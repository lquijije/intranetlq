
var table_gb = null;

$(document).ready(function () {

    getTomas();

    var fec_1 = $('#f_i').val();
    var now_1 = new Date(fec_1);
    now_1.setTime(now_1.getTime() + 1 * 24 * 60 * 60 * 1000);

    var fec_2 = $('#f_f').val();
    var now_2 = new Date(fec_2);
    now_2.setTime(now_2.getTime() + 1 * 24 * 60 * 60 * 1000);

    $('#reportrange').daterangepicker({
        startDate: now_1,
        endDate: now_2
    },
            function (start, end) {
                $('#f_i').val(start.format("YYYY-MM-DD"));
                $('#f_f').val(end.format("YYYY-MM-DD"));
                $('#reportrange span').html(start.format('MMMM D YYYY') + ' - ' + end.format('MMMM D YYYY'));
                getTomas();
            }
    );

    $('.excel_btn').click(function (event) {
        var datos = "f_i=" + $('#f_i').val() + "&f_f=" + $('#f_f').val();
        $.download('co_5001/exExc', datos);
        event.preventDefault();
    });

    $('.ref_pag').click(function (event) {
        getTomas();
        event.preventDefault();
    });

    $('.retPaini').click(function (event) {
        getTomas();
        event.preventDefault();
    });

    $('.ge_inform').click(function (event) {
        seInforme();
        event.preventDefault();
    });

});

function procesarArticulos(c_b, c_l) {
    if (confirm("Desea procesar los siguientes productos.") == true) {
        dataLoading();
        $.post('co_5001/proProductos',
                {c_b: c_b, c_l: c_l},
                function (b) {
                    var c = eval('(' + b + ')');
                    if (parseInt(c.co_err) == 0) {
                        setTimeout(function () {
                            window.location = window.location.href;
                        }, 1000);
                        errorGeneral(conf_hd.tx.t_1, 'Toma procesada correctamente.', 'success');
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    }
}

function grabarArticulos(c_b, c_l) {

    if (va_sess()) {
        var solc = [];
        var table_ = $('#detArt').DataTable();
        var naju = table_.$("input[id='option_naju[]']:checked");
        var ajus = table_.$("input[id='option_ajus[]']:checked");
        var revi = table_.$("input[id='option_revi[]']:checked");

        $(ajus).each(function () {
            var itemR = {};
            itemR["c_s"] = $(this).val();
            itemR["e_s"] = 'A';
            solc.push(itemR);
        });

        $(naju).each(function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'N';
            solc.push(itemA);
        });

        $(revi).each(function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'R';
            solc.push(itemA);
        });

        if (!$.isEmptyObject(solc)) {
            var r = confirm("Desea grabar los siguientes productos.");
            if (r == true) {
                dataLoading();
                $.post('co_5001/savProductos',
                        {s_s: JSON.stringify(solc), c_b: c_b, c_l: c_l},
                        function (b) {
                            var c = eval('(' + b + ')');
                            if (parseInt(c.co_err) == 0) {
                                setTimeout(function () {
                                    window.location = window.location.href;
                                }, 1000);
                                errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_5, 'success');
                            } else {
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                            }
                            removedataLoading();
                        }).fail(function (p) {
                    removedataLoading();
                    errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });
            }
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Seleccione uno o m&aacute;s productos a procesar.', 'danger');
        }
    } else {
        redirectIntranet();
    }
}

function getTomas() {

    $('.desPanel').show();
    $('.desPanelDet').hide();
    $('#pieAdmin').html('');
    $('.detAud').html('');
    if (va_sess()) {

        var url = $('#txt_urlGe').val();
        dataLoading();
        $.post('co_5001/reDas',
                {f_i: $('#f_i').val(), f_f: $('#f_f').val()},
                function (b) {
                    var c = eval('(' + b + ')');
                    var ct_tm = 0;

                    if (!$.isEmptyObject(c.pie)) {

                        $.each(c.pie, function (e, d) {
                            ct_tm += d.data;
                        });

                        $('#tot_cli').html(ct_tm);

                        $.plot("#pieAdmin", c.pie, {
                            series: {
                                pie: {
                                    show: true,
                                    radius: 1,
                                    label: {
                                        show: true,
                                        radius: 1,
                                        threshold: 0.1,
                                        formatter: labelFormatter,
                                        background: {
                                            opacity: 0.7,
                                            color: '#333'
                                        }
                                    }
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            legend: {
                                show: true,
                                labelFormatter: function (label, series, ab) {
                                    label = label.split('*|*');
                                    return '<div style="font-size:8pt;padding:2px;"><a href="javascript:void(0)" onclick="getDetToma(' + "'" + label[0] + "'" + ',' + "'" + label[1] + "'" + ',' + "'" + series.color + "'" + ',0,' + "''" + ')">' + label[1] + '</a></div>';
                                }
                            }
                        });

                        if (!$.isEmptyObject(c.usu)) {
                            $.each(c.usu, function (e, q) {
                                $('.detAud').append(q.dat);
                                if (!$.isEmptyObject(q.pie)) {
                                    $.plot("#pie" + q.c, q.pie, {
                                        series: {
                                            pie: {
                                                show: true,
                                                radius: 1,
                                                label: {
                                                    show: true,
                                                    radius: 1,
                                                    threshold: 0.1,
                                                    formatter: labelFormatter,
                                                    background: {
                                                        opacity: 0.7,
                                                        color: '#333'
                                                    }
                                                }
                                            }
                                        },
                                        grid: {
                                            hoverable: true
                                        },
                                        legend: {
                                            show: true,
                                            labelFormatter: function (label, series) {
                                                label = label.split('*|*');
                                                //onclick="getDetToma(' + "'" + label[0] + "'" + ',' + "'" + label[1] + "'" + ',' + "'" + series.color + "'" + ',' + q.c + ',' + "' - " + q.groupeddata[0].no_aud + "'" + ');"
                                                return '<div style="font-size:8pt;padding:2px;"><a style="color: rgb(51, 51, 51);" href="javascript:void(0)" >' + label[1] + '</a></div>';
                                            }
                                        }
                                    });
                                } else {
                                    $('#pieAge').html('<h4 class="text-blanco text-center">' + conf_hd.tx.t_6 + '</h4>');
                                }
                            });
                        }

                    } else {
                        $('#pieAdmin').html('<h4 class="text-blanco text-center">' + conf_hd.tx.t_6 + '</h4>');
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });

    } else {
        redirectIntranet();
    }
}

function getDetToma(s, t, p, x, y) {

    if (va_sess()) {

        $('#titleTelf').html(t + y);

        $('#tabAud').DataTable({
            bDestroy: true,
            sAjaxDataProp: "data",
            ajax: {
                url: "co_5001/reLisT",
                type: "POST",
                data: {f_i: $('#f_i').val(), f_f: $('#f_f').val(), c_e: s, c_u: x},
                dataSrc: ""
            },
            columns: [
                {title: 'Bodega', "data": "no_bdg", sClass: "text-center"},
                {title: 'Listado', "data": "co_lis", sClass: "text-right"},
                {title: 'Descripci&oacute;n', "data": "tx_des", sClass: "text-left"},
                {title: 'Fecha', "data": "fe_gen", sClass: "text-center"},
                {title: 'Tipo', "data": "co_tip", sClass: "text-center"},
                {title: 'Items', "data": "ca_ite", sClass: "text-right"},
                {title: 'Acci&oacute;n', "data": "html", sClass: "text-center"}
            ]
        });

        $('#tabAud th').css({background: p, color: "#FFF"});
        $('.modal-header').css({background: p});

        newModal('DetallToma', '65', '65', '');

    } else {
        redirectIntranet();
    }
}


function delToma(bdg, list) {
    if (va_sess()) {
        if (confirm('Desea eliminar la toma ?') == true) {
            dataLoading();
            $.post('co_5001/delToma',
                    {a_l: bdg, c_t: list},
                    function (c) {
                        c = eval('(' + c + ')');
                        if (c.co_err == 0) {
                            quitarmodalGeneral('DetallToma', '');
                            errorGeneral(conf_hd.tx.t_1, 'Toma eliminada con exito.', 'success');
                            getTomas();
                        } else {
                            errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                        }
                        removedataLoading();
                    }).fail(function (p) {
                removedataLoading();
                errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
            });
        }
    } else {
        redirectIntranet();
    }
}

function viewDetToma(c_b, c_l, fl, c_e) {

    if (va_sess()) {

        dataLoading();
        $.post('co_5001/reToma',
                {c_b: c_b, c_l: c_l, c_a: fl, c_e: c_e},
                function (b) {
                    var c = eval('(' + b + ')');
                    if ($.isEmptyObject(c.err)) {
                        if (!$.isEmptyObject(c.inf)) {
                            if (!$.isEmptyObject(c.art)) {
                                $('.desPanel').hide();
                                $('.desPanelDet').show();
                                quitarmodalGeneral('DetallToma', '');
                                $('#va_tot_pvp').html(c.inf.va_tot_pvp);
                                $('#va_tot_cos').html(c.inf.va_tot_cos);
                                $('#va_tot_dif_pvp').html(c.inf.va_tot_dif_pvp);
                                $('#va_tot_dif_cos').html(c.inf.va_tot_dif_cos);
                                $('#countif').html(c.inf.countif);
                                $('#ubica_2').html(c.inf.ubica_2);

                                $('#tab_pa_1').html('<table id="detArt" class="table table-bordered TFtable"></table><div class="clearfix"></div>');
                                var countVco = 0;
                                var table_ = $('#detArt').DataTable({
                                    data: c.art.data,
                                    columns: c.art.columns,
                                    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                        var to_est = typeof (aData.to_est) == "undefined" ? 0 : aData.to_est;
                                        if (aData.va_con != 0) {
                                            $('td', nRow).css('background', 'rgb(188, 255, 198)');
                                        }
                                        if (to_est != 0 && c.inf.da_ecv > 0) {
                                            countVco++;
                                            $('td', nRow).css('background', 'rgb(255, 164, 164)');
                                        }
                                    }
                                });

                                $('#tab_pa_1').append(c.btn);

                                $('#ck_ajus').toggle(
                                        function () {
                                            table_.$("input[id='option_naju[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_.$("input[id='option_revi[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_.$("input[id='option_ajus[]']").each(function () {
                                                $(this).attr('checked', true);
                                            });
                                        }, function () {
                                    table_.$("input[id='option_ajus[]']").each(function () {
                                        $(this).attr('checked', false);
                                    });
                                }
                                );

                                $('#ck_naju').toggle(
                                        function () {
                                            table_.$("input[id='option_ajus[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_.$("input[id='option_revi[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_.$("input[id='option_naju[]']").each(function () {
                                                $(this).attr('checked', true);
                                            });
                                        }, function () {
                                    table_.$("input[id='option_naju[]']").each(function () {
                                        $(this).attr('checked', false);
                                    });
                                }
                                );

                                $('#ck_revi').toggle(
                                        function () {
                                            table_.$("input[id='option_ajus[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_.$("input[id='option_naju[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_.$("input[id='option_revi[]']").each(function () {
                                                $(this).attr('checked', true);
                                            });
                                        }, function () {
                                    table_.$("input[id='option_revi[]']").each(function () {
                                        $(this).attr('checked', false);
                                    });
                                }
                                );

                                $('#btn_check_f_art').click(function (a, b) {
                                    var cmb = $("#cmb_pendientes").val();
                                    if ($(this).is(':checked')) {
                                        viewDetToma(c_b, c_l, 1, c_e);
                                    } else {
                                        viewDetToma(c_b, c_l, 0, c_e);
                                    }
                                });

                                if (countVco > 0) {
                                    $('#btn_check_vco').click(function (a, b) {
                                        if ($(this).is(':checked')) {
                                            $("input[name='option_vc[]']").prop('checked', true);
                                        } else {
                                            $("input[name='option_vc[]']").prop('checked', false);
                                        }
                                    });
                                }

                                $('.consToma').click(function (event) {
                                    event.preventDefault();
                                });

                                if (!$.isEmptyObject(c.tab_ave)) {
                                    $('#ave_va_tot_pvp').html(c.tab_ave.inf.va_tot_pvp);
                                    $('#ave_va_tot_cos').html(c.tab_ave.inf.va_tot_cos);
                                    $('#ave_countif').html(c.tab_ave.inf.countif);
                                    $('#ave_ubica_2').html(c.tab_ave.inf.ubica_2);

                                    $('#tab_pa_2').html('<table id="detAve" class="table table-bordered TFtable"></table><div class="clearfix"></div>');
                                    var table_ave = $('#detAve').DataTable({
                                        data: c.tab_ave.art.data,
                                        columns: c.tab_ave.art.columns
                                    });
                                    $('#tab_pa_2').append(c.tab_ave.btn);
                                    $('.dt_clear').on('click', function () {
                                        table_ave.search('').columns().search('').draw();
                                    });

                                    $('#ck_ajus_ave').toggle(
                                            function () {
                                                table_ave.$("input[id='option_naju_ave[]']").each(function () {
                                                    $(this).attr('checked', false);
                                                });
                                                table_ave.$("input[id='option_revi_ave[]']").each(function () {
                                                    $(this).attr('checked', false);
                                                });
                                                table_ave.$("input[id='option_ajus_ave[]']").each(function () {
                                                    $(this).attr('checked', true);
                                                });
                                            }, function () {
                                        table_ave.$("input[id='option_ajus_ave[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                    }
                                    );

                                    $('#ck_naju_ave').toggle(
                                            function () {
                                                table_ave.$("input[id='option_ajus_ave[]']").each(function () {
                                                    $(this).attr('checked', false);
                                                });
                                                table_ave.$("input[id='option_revi_ave[]']").each(function () {
                                                    $(this).attr('checked', false);
                                                });
                                                table_ave.$("input[id='option_naju_ave[]']").each(function () {
                                                    $(this).attr('checked', true);
                                                });
                                            }, function () {
                                        table_ave.$("input[id='option_naju_ave[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                    }
                                    );

                                    $('#ck_revi_ave').toggle(
                                            function () {
                                                table_ave.$("input[id='option_ajus_ave[]']").each(function () {
                                                    $(this).attr('checked', false);
                                                });
                                                table_ave.$("input[id='option_naju_ave[]']").each(function () {
                                                    $(this).attr('checked', false);
                                                });
                                                table_ave.$("input[id='option_revi_ave[]']").each(function () {
                                                    $(this).attr('checked', true);
                                                });
                                            }, function () {
                                        table_ave.$("input[id='option_revi_ave[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                    }
                                    );
                                }
                            } else {
                                errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                            }

                        } else {
                            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                        }

                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });

    } else {
        redirectIntranet();
    }

}

function labelFormatter(a, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;border-radius: 3px;'> " + Math.round(series.percent) + "%</div>";
}

function consArt(info, cod_art, des_art, cad_para) {

    var co_lis = cad_para.split('-');

    if (va_sess()) {

        info = decodeURIComponent(info);
        info = $.parseJSON(info);

        if (!$.isEmptyObject(info) && !$.isEmptyObject(cod_art) && !$.isEmptyObject(cad_para)) {

            $('#nomCentr').html(info.no_alm);
            $('#nomToma').html(co_lis[1]);
            $('#desTipo').html(info.ds_tip);
            $('#co_art').html(cod_art);
            $('#ds_art').html(des_art);

            retDetUbicaciones(cod_art, cad_para);


        }
    } else {
        redirectIntranet();
    }
}

function retDetUbicaciones(cod_art, cad_para) {
    $.post('co_5001/consultaArtToma',
            {c_a: cod_art, c_p: cad_para},
            function (c) {
                c = eval('(' + c + ')');
                if ($.isEmptyObject(c.err)) {
                    $('#cont_lis_det').html(c.data);
                    newModal('DetArt', 75, 65, '');
                } else {
                    errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                }
                removedataLoading();
            }).fail(function (p) {
        removedataLoading();
        errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
    });
}

function grabarArticulosAveriados(c_b, c_l) {

    if (va_sess()) {
        var solc = [];
        var table_ = $('#detAve').DataTable();
        var naju = table_.$("input[id='option_naju_ave[]']:checked");
        var ajus = table_.$("input[id='option_ajus_ave[]']:checked");
        var revi = table_.$("input[id='option_revi_ave[]']:checked");

        $(ajus).each(function () {
            var itemR = {};
            itemR["c_s"] = $(this).val();
            itemR["e_s"] = 'A';
            solc.push(itemR);
        });

        $(naju).each(function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'N';
            solc.push(itemA);
        });

        $(revi).each(function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'R';
            solc.push(itemA);
        });

        if (!$.isEmptyObject(solc)) {
            var r = confirm("Desea grabar los siguientes productos.");
            if (r == true) {
                dataLoading();
                $.post('co_5001/savProductosAveriado',
                        {s_s: JSON.stringify(solc), c_b: c_b, c_l: c_l},
                        function (b) {
                            var c = eval('(' + b + ')');
                            if (parseInt(c.co_err) == 0) {
                                setTimeout(function () {
                                    window.location = window.location.href;
                                }, 1000);
                                errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_5, 'success');
                            } else {
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                            }
                            removedataLoading();
                        }).fail(function (p) {
                    removedataLoading();
                    errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });
            }
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Seleccione uno o m&aacute;s productos a procesar.', 'danger');
        }
    } else {
        redirectIntranet();
    }
}

function fn_generarInforme(c_b, c_l) {
    $('#txt_mail').val('');
    $('#c_b').val(c_b);
    $('#c_l').val(c_l);
    $('#cont_mail').val('');
    $("input[name='format_informe']").prop('checked', false);
    newModal('GeInforme', 50, 50, '');
    $('#modalGeInforme').css('z-index', '1042');
    $('#fadeGeInforme').css('z-index', '1041');
}

function seInforme() {
    var c_b = $('#c_b').val();
    var c_l = $('#c_l').val();
    var mail = $('#txt_mail').val();
    var cnt_mail = $('#cont_mail').val();
    var informe = $("#cmb_tip").val();

    if (va_sess()) {
        if (!$.isEmptyObject(mail)) {
            var arrMail = mail.split(',');
            for (var a = 0; a < arrMail.length; a++) {
                if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(arrMail[a])) {
                    errorGeneral(conf_hd.tx.t_1, 'El email ' + arrMail[a] + ' es incorrecto', 'danger');
                    return false;
                }
            }
            dataLoading();
            $.post('co_5001/snInforme',
                    {c_b: c_b, c_l: c_l, e_i: mail, t_i: informe, c_m: cnt_mail},
                    function (c) {
                        c = eval('(' + c + ')');
                        if (c.co_err == 0) {
                            quitarmodalGeneral('GeInforme', '');
                            errorGeneral(conf_hd.tx.t_1, 'Informe enviado con exito.', 'success');
                        } else {
                            errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                        }
                        removedataLoading();
                    }).fail(function (p) {
                removedataLoading();
                errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
            });
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Debe de ingresar un email.', 'danger');
        }
    } else {
        redirectIntranet();
    }
}