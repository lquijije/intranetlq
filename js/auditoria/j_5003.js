
$(function () {
    $('#txt_fec_ini').datetimepicker({
        format: 'Y/m/d',
        lang: 'es',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txt_fec_fin').val() ? $('#txt_fec_fin').val() : false
            });
        },
        timepicker: false,
        closeOnDateSelect: true
    });

    $('#txt_fec_fin').datetimepicker({
        format: 'Y/m/d',
        lang: 'es',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#txt_fec_ini').val() ? $('#txt_fec_ini').val() : false
            });
        },
        timepicker: false,
        closeOnDateSelect: true
    });

    $(".seTom").click(function (e) {
        getTomas();
        e.preventDefault();
    });

    $('.ge_inform').click(function (e) {
        seInforme();
        e.preventDefault();
    });
});


function getTomas() {
    $('#detalleTomas').html('');
    var fe_ini = $('#txt_fec_ini').val();
    var fe_fin = $('#txt_fec_fin').val();
    var co_bdg = $('#cmb_almacen').val();
    if (va_sess()) {
        if (co_bdg !== 'NNN') {
            if (!$.isEmptyObject(fe_ini) && !$.isEmptyObject(fe_fin)) {
                dataLoading();
                $.post('co_5003/retTom',
                        {f_d: fe_ini, f_h: fe_fin, c_b: co_bdg},
                        function (b) {
                            var c = eval('(' + b + ')');
                            if ($.isEmptyObject(c.err)) {
                                if (!$.isEmptyObject(c.data)) {
                                    $('#detalleTomas').html('<table id="T_detToma" class="table table-bordered TFtable"></table><div class="clearfix"></div>');
                                    $('#T_detToma').DataTable({
                                        data: c.data.data,
                                        columns: c.data.columns
                                    });
                                } else {
                                    errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                                }
                            } else {
                                errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                            }
                            removedataLoading();
                        }).fail(function (p) {
                    removedataLoading();
                    errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });
            } else {
                errorGeneral(conf_hd.tx.t_1, 'Ingrese un rango de fechas.', 'danger');
            }
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Seleccione una bodega', 'danger');
        }
    } else {
        redirectIntranet();
    }
}

function sendMail(c_b, c_s) {
    if (va_sess()) {
        var mail = prompt("Ingrese su correo electronico.", "");
        if (!$.isEmptyObject(mail)) {
            if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(mail)) {
                dataLoading();
                $.post('co_5003/senMai',
                        {c_b: c_b, c_s: c_s, e_m: mail},
                        function (b) {
                            var c = eval('(' + b + ')');
                            if (parseInt(c.co_err) == 0) {
                                errorGeneral(conf_hd.tx.t_1, 'Informe enviado con exito.', 'success');
                            } else {
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                            }
                            removedataLoading();
                        }).fail(function (p) {
                    removedataLoading();
                    errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });
            } else {
                errorGeneral(conf_hd.tx.t_1, 'El email ' + mail + ' es incorrecto', 'danger');
                return false;
            }
        }
    } else {
        redirectIntranet();
    }
}

function fn_generarInforme(c_b, c_l) {
    $('#txt_mail').val('');
    $('#c_b').val(c_b);
    $('#c_l').val(c_l);
    $('#cont_mail').val('');
    $("input[name='format_informe']").prop('checked', false);
    newModal('GeInforme', 50, 50, '');
    $('#modalGeInforme').css('z-index', '1042');
    $('#fadeGeInforme').css('z-index', '1041');
}

function seInforme() {
    var c_b = $('#c_b').val();
    var c_l = $('#c_l').val();
    var mail = $('#txt_mail').val();
    var cnt_mail = $('#cont_mail').val();
    var informe = $("#cmb_tip").val();

    if (va_sess()) {
        if (!$.isEmptyObject(mail)) {
            var arrMail = mail.split(',');
            for (var a = 0; a < arrMail.length; a++) {
                if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(arrMail[a])) {
                    errorGeneral(conf_hd.tx.t_1, 'El email ' + arrMail[a] + ' es incorrecto', 'danger');
                    return false;
                }
            }
            dataLoading();
            $.post('co_5003/snInforme',
                    {c_b: c_b, c_l: c_l, e_i: mail, t_i: informe, c_m: cnt_mail},
                    function (c) {
                        c = eval('(' + c + ')');
                        if (c.co_err == 0) {
                            quitarmodalGeneral('GeInforme', '');
                            errorGeneral(conf_hd.tx.t_1, 'Informe enviado con exito.', 'success');
                        } else {
                            errorGeneral(conf_hd.tx.t_1, c.tx_err, 'error');
                        }
                        removedataLoading();
                    }).fail(function (p) {
                removedataLoading();
                errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
            });
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Debe de ingresar un email.', 'danger');
        }
    } else {
        redirectIntranet();
    }
}