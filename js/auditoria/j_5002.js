var table_gb = null;

$(document).ready(function () {

    $('.desPanel').show();
    $('.desPanelDet').hide();

    $('.retPaini').click(function (event) {
        window.location = window.location.href;
        event.preventDefault();
    });
    $('.ge_inform').click(function (event) {
        seInforme();
        event.preventDefault();
    });
});

function grabarArticulos(c_b, c_l) {

    if (va_sess()) {
        var solc = [];
        var table_ = $('#detArt').DataTable();
        var cobr = table_.$("input[id='option_cobr[]']:checked");
        var ajus = table_.$("input[id='option_ajus[]']:checked");
        var revi = table_.$("input[id='option_revi[]']:checked");

        $(ajus).each(function () {
            var itemR = {};
            itemR["c_s"] = $(this).val();
            itemR["e_s"] = 'A';
            solc.push(itemR);
        });

        $(cobr).each(function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'C';
            solc.push(itemA);
        });

        $(revi).each(function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'R';
            solc.push(itemA);
        });

        if (!$.isEmptyObject(solc)) {
            var r = confirm("Desea grabar los siguientes productos.");
            if (r == true) {
                dataLoading();
                $.post('co_5002/savProductos',
                        {s_s: JSON.stringify(solc), c_b: c_b, c_l: c_l},
                        function (b) {
                            var c = eval('(' + b + ')');
                            if (parseInt(c.co_err) == 0) {
                                setTimeout(function () {
                                    window.location = window.location.href;
                                }, 2000);
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'success');
                            } else {
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                            }
                            removedataLoading();
                        }).fail(function (p) {
                    removedataLoading();
                    errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });
            }
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Seleccione uno o m&aacute;s productos a procesar.', 'danger');
        }
    } else {
        redirectIntranet();
    }
}

function grabarArticulosAveriados(c_b, c_l) {

    if (va_sess()) {
        var solc = [];
        var table_ = $('#detAve').DataTable();
        var cobr = table_.$("input[id='option_cobr_ave[]']:checked");
        var ajus = table_.$("input[id='option_ajus_ave[]']:checked");
        var revi = table_.$("input[id='option_revi_ave[]']:checked");

        $(ajus).each(function () {
            var itemR = {};
            itemR["c_s"] = $(this).val();
            itemR["e_s"] = 'A';
            solc.push(itemR);
        });

        $(cobr).each(function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'C';
            solc.push(itemA);
        });

        $(revi).each(function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'R';
            solc.push(itemA);
        });

        if (!$.isEmptyObject(solc)) {
            var r = confirm("Desea grabar los siguientes productos.");
            if (r == true) {
                dataLoading();
                $.post('co_5002/savProductosAveriados',
                        {s_s: JSON.stringify(solc), c_b: c_b, c_l: c_l},
                        function (b) {
                            var c = eval('(' + b + ')');
                            if (parseInt(c.co_err) == 0) {
                                setTimeout(function () {
                                    window.location = window.location.href;
                                }, 2000);
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'success');
                            } else {
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                            }
                            removedataLoading();
                        }).fail(function (p) {
                    removedataLoading();
                    errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });
            }
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Seleccione uno o m&aacute;s productos a procesar.', 'danger');
        }
    } else {
        redirectIntranet();
    }
}

function procesarArticulos(c_b, c_l) {
    if (confirm("Desea procesar los siguientes productos.") == true) {
        dataLoading();
        $.post('co_5002/proProductos',
                {c_b: c_b, c_l: c_l},
                function (b) {
                    var c = eval('(' + b + ')');
                    if (parseInt(c.co_err) == 0) {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'success');
                        setTimeout(function () {
                            window.location = window.location.href;
                        }, 2000);
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    }
}

function getDetToma(s, t, p, x, y) {

    if (va_sess()) {

        $('#titleTelf').html(t + y);

        $('#tabAud').DataTable({
            bDestroy: true,
            sAjaxDataProp: "data",
            ajax: {
                url: "co_5002/reLisT",
                type: "POST",
                data: {f_i: $('#f_i').val(), f_f: $('#f_f').val(), c_e: s, c_u: x},
                dataSrc: ""
            },
            columns: [
                {title: 'Bodega', "data": "no_bdg", sClass: "text-center"},
                {title: 'Listado', "data": "co_lis", sClass: "text-right"},
                {title: 'Fecha', "data": "fe_gen", sClass: "text-center"},
                {title: 'Tipo', "data": "co_tip", sClass: "text-center"},
                {title: 'Items', "data": "ca_ite", sClass: "text-right"},
                {title: 'Acci&oacute;n', "data": "html", sClass: "text-center"}
            ]
        });

        $('#tabAud th').css({background: p, color: "#FFF"});
        $('.modal-header').css({background: p});

        newModal('DetallToma', '65', '65', '');

    } else {
        redirectIntranet();
    }
}

function viewDetToma(c_b, c_l, fl) {

    if (va_sess()) {

        dataLoading();
        $.post('co_5002/reToma',
                {c_b: c_b, c_l: c_l, c_a: fl},
                function (b) {
                    var c = eval('(' + b + ')');

                    if ($.isEmptyObject(c.err)) {

                        if (!$.isEmptyObject(c.inf)) {

                            $('.desPanel').hide();
                            $('.desPanelDet').show();

                            $('#va_tot_pvp').html(c.inf.va_tot_pvp);
                            $('#va_tot_cos').html(c.inf.va_tot_cos);
                            $('#va_tot_dif_pvp').html(c.inf.va_tot_dif_pvp);
                            $('#va_tot_dif_cos').html(c.inf.va_tot_dif_cos);
                            $('#countif').html(c.inf.countif);
                            $('#ubica_2').html(c.inf.ubica_2);

                            $('#tab_pa_1').html('<table id="detArt" class="table table-bordered TFtable"></table><div class="clearfix"></div>');

                            var table_ = $('#detArt').DataTable({
                                data: c.art.data,
                                columns: c.art.columns,
                                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                    var to_est = typeof (aData.to_est) == "undefined" ? 0 : aData.to_est;
                                    if (aData.va_con != 0) {
                                        $('td', nRow).css('background', 'rgb(188, 255, 198)');
                                    }
                                    if (to_est != 0 && c.inf.da_ecv > 0) {
                                        $('td', nRow).css('background', 'rgb(255, 164, 164)');
                                    }
                                }
                            });

                            $('#tab_pa_1').append(c.btn);

                            $('#ck_ajus').toggle(
                                    function () {
                                        table_.$("input[id='option_cobr[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                        table_.$("input[id='option_revi[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                        table_.$("input[id='option_ajus[]']").each(function () {
                                            $(this).attr('checked', true);
                                        });
                                    }, function () {
                                table_.$("input[id='option_ajus[]']").each(function () {
                                    $(this).attr('checked', false);
                                });
                            }
                            );

                            $('#ck_cobr').toggle(
                                    function () {
                                        table_.$("input[id='option_ajus[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                        table_.$("input[id='option_revi[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                        table_.$("input[id='option_cobr[]']").each(function () {
                                            $(this).attr('checked', true);
                                        });
                                    }, function () {
                                table_.$("input[id='option_cobr[]']").each(function () {
                                    $(this).attr('checked', false);
                                });
                            }
                            );

                            $('#ck_revi').toggle(
                                    function () {
                                        table_.$("input[id='option_ajus[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                        table_.$("input[id='option_cobr[]']").each(function () {
                                            $(this).attr('checked', false);
                                        });
                                        table_.$("input[id='option_revi[]']").each(function () {
                                            $(this).attr('checked', true);
                                        });
                                    }, function () {
                                table_.$("input[id='option_revi[]']").each(function () {
                                    $(this).attr('checked', false);
                                });
                            }
                            );

                            $('#btn_check_f_art').click(function (a, b) {
                                var cmb = $("#cmb_pendientes").val();
                                if ($(this).is(':checked')) {
                                    viewDetToma(c_b, c_l, 1);
                                } else {
                                    viewDetToma(c_b, c_l, 0);
                                }
                            });


                            if (!$.isEmptyObject(c.tab_ave)) {
                                $('#ave_va_tot_pvp').html(c.tab_ave.inf.va_tot_pvp);
                                $('#ave_va_tot_cos').html(c.tab_ave.inf.va_tot_cos);
                                $('#ave_countif').html(c.tab_ave.inf.countif);
                                $('#ave_ubica_2').html(c.tab_ave.inf.ubica_2);

                                $('#tab_pa_2').html('<table id="detAve" class="table table-bordered TFtable"></table><div class="clearfix"></div>');
                                var table_ave = $('#detAve').DataTable({
                                    data: c.tab_ave.art.data,
                                    columns: c.tab_ave.art.columns
                                });
                                $('.dt_clear').on('click', function () {
                                    table_ave.search('').columns().search('').draw();
                                });
                                $('#tab_pa_2').append(c.tab_ave.btn);

                                $('#ck_ajus_ave').toggle(
                                        function () {
                                            table_ave.$("input[id='option_cobr_ave[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_ave.$("input[id='option_revi_ave[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_ave.$("input[id='option_ajus_ave[]']").each(function () {
                                                $(this).attr('checked', true);
                                            });
                                        }, function () {
                                    table_ave.$("input[id='option_ajus_ave[]']").each(function () {
                                        $(this).attr('checked', false);
                                    });
                                }
                                );

                                $('#ck_naju_ave').toggle(
                                        function () {
                                            table_ave.$("input[id='option_ajus_ave[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_ave.$("input[id='option_revi_ave[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_ave.$("input[id='option_cobr_ave[]']").each(function () {
                                                $(this).attr('checked', true);
                                            });
                                        }, function () {
                                    table_ave.$("input[id='option_naju_ave[]']").each(function () {
                                        $(this).attr('checked', false);
                                    });
                                }
                                );

                                $('#ck_revi_ave').toggle(
                                        function () {
                                            table_ave.$("input[id='option_ajus_ave[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_ave.$("input[id='option_cobr_ave[]']").each(function () {
                                                $(this).attr('checked', false);
                                            });
                                            table_ave.$("input[id='option_revi_ave[]']").each(function () {
                                                $(this).attr('checked', true);
                                            });
                                        }, function () {
                                    table_ave.$("input[id='option_revi_ave[]']").each(function () {
                                        $(this).attr('checked', false);
                                    });
                                }
                                );
                            }

                        } else {
                            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                        }

                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });

    } else {
        redirectIntranet();
    }

}

function labelFormatter(a, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;border-radius: 3px;'> " + Math.round(series.percent) + "%</div>";
}

function consArt(info, cod_art, des_art, cad_para) {

    var co_lis = cad_para.split('-');

    if (va_sess()) {

        info = decodeURIComponent(info);
        info = $.parseJSON(info);

        if (!$.isEmptyObject(info) && !$.isEmptyObject(cod_art) && !$.isEmptyObject(cad_para)) {

            $('#nomCentr').html(info.no_alm);
            $('#nomToma').html(co_lis[1]);
            $('#desTipo').html(info.ds_tip);
            $('#co_art').html(cod_art);
            $('#ds_art').html(des_art);

            retDetUbicaciones(cod_art, cad_para);

            newModal('DetArt', 75, 65, '');

        }
    } else {
        redirectIntranet();
    }
}

function fn_generarInforme(c_b, c_l) {
    $('#txt_mail').val('');
    $('#c_b').val(c_b);
    $('#c_l').val(c_l);
    $('#cont_mail').val('');
    $("input[name='format_informe']").prop('checked', false);
    newModal('GeInforme', 50, 50, '');
    $('#modalGeInforme').css('z-index', '1042');
    $('#fadeGeInforme').css('z-index', '1041');
}

function seInforme() {
    var c_b = $('#c_b').val();
    var c_l = $('#c_l').val();
    var mail = $('#txt_mail').val();
    var cnt_mail = $('#cont_mail').val();
    var informe = $("#cmb_tip").val();

    if (va_sess()) {
        if (!$.isEmptyObject(mail)) {
            var arrMail = mail.split(',');
            for (var a = 0; a < arrMail.length; a++) {
                if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(arrMail[a])) {
                    errorGeneral(conf_hd.tx.t_1, 'El email ' + arrMail[a] + ' es incorrecto', 'danger');
                    return false;
                }
            }
            dataLoading();
            $.post('co_5002/snInforme',
                    {c_b: c_b, c_l: c_l, e_i: mail, t_i: informe, c_m: cnt_mail},
                    function (c) {
                        c = eval('(' + c + ')');
                        if (c.co_err == 0) {
                            quitarmodalGeneral('GeInforme', '');
                            errorGeneral(conf_hd.tx.t_1, 'Informe enviado con exito.', 'success');
                        } else {
                            errorGeneral(conf_hd.tx.t_1, c.tx_err, 'error');
                        }
                        removedataLoading();
                    }).fail(function (p) {
                removedataLoading();
                errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
            });
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Debe de ingresar un email.', 'danger');
        }
    } else {
        redirectIntranet();
    }
}