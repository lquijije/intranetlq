
function viewSolic(tipo,co_soli,activi,pp){
    
    var url_1 = $('#txt_urlGe').val();
    $('#modalViewSolicit > div > div > div').removeClass();
    var tp = tipo === conf_hd.sol[0].type?conf_hd.sol[0].name:conf_hd.sol[1].name;
    var cl = tipo === conf_hd.sol[0].type?conf_hd.sol[0].bg+" "+conf_hd.sol[0].color:conf_hd.sol[1].bg+" "+conf_hd.sol[1].color;
    var datos = "c_s="+co_soli;    
    var estado = '';
    
    $('#modalViewSolicit > div > div > div').addClass(cl);
    $('#id_Viewsolic').val(co_soli);  
    $('#ti_Viewsolic').val(tipo);  
    $('.titleSol').html(tp+" #"+co_soli); 
    $('.iconViewSolict').removeClass();
    $('.titleViewSolict').html('');
    $('.contViewSolict').html('');
    $('.cancelSolict').removeClass('disp_none');
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_101/retCodSolicit", 
        data: datos,
        success : function (returnData) {
            var data = eval('('+returnData+')'); 
            removedataLoading();
            if(!$.isEmptyObject(data.sol)) {
                $.each(data.sol,function(e,dato){
                    estado = dato.co_estado;
                    console.log(dato.co_empleado);
                    console.log(parseInt($('#txt_ceGe').val()));
                    if(parseInt(dato.co_empleado) != parseInt($('#txt_ceGe').val())){
                        $('.cancelSolict').addClass('disp_none');
                    }
                    $('.iconViewSolict').addClass("fa "+dato.icon);
                    $('.titleViewSolict').html(dato.tx_titulo);
                    $('.contViewSolict').html(dato.tx_descripcion_detallada);
                    
                    $('._ing_sol').html(dato.nomb_empl_ing);
                    $('._ing_cco_sol').html(dato.nomb_ccosto_ing);
                    $('._ing_fec_sol').html(dato.fe_ingreso);
                    
                    if (dato.co_estado === 'C' || dato.co_estado === 'F'){
                        $('.send-container').hide();
                        $('.finSolict').hide();
                        $('.phone').css('top','0');
                        $('.cancelSolict').hide();
                    } else {
                        $('.finSolict').show();
                        $('.cancelSolict').show();
                        $('.send-container').show();
                        $('.phone').css('margin-top','56px');
                        var $alto = $('.modal-content').height();
                        $('.phone').css('height','350px');
                    }
                });
                if (activi){
                    
                    if (estado === 'C' || estado === 'F'){
                        $('.savePorceTiempo').hide();
                    } else {
                        $('.savePorceTiempo').show();
                    }
                    
                    if(tipo === conf_hd.sol[0].type){
                        $('.time_asig').show();
                    } else {
                        $('#time_asig_s').removeClass('spa-7');
                        $('#time_asig_s').addClass('spa-10');
                        $('.time_asig').hide();
                    }
                    
                    if(!$.isEmptyObject(data.asi)){
                        var asi = '';
                        $.each(data.asi,function(e,dasi){
                            asi = asi +'<li>';
                                asi = asi +'<b>'+dasi.des_area +' - '+dasi.nom_empl+' </b><br>Progreso ('+dasi.val_pro+')%';
                            asi = asi +'</li>';
                        });
                        $('.list_asig').append(asi);
                    }
                    
                    var arr  = activi.split('//');
                    $('#id_ViewActi').val(arr[0]);
                    $('#tipo_sol').val(tipo);
                    $('#text_des_act').html(arr[1]);
                    if(arr[2].toString().trim() != ''){
                        $('#cmb_unidad').val(arr[2]);
                    }
                    $('#txt_tiempo').val(arr[3]);
                    var a = arr[4] !== ''?arr[4]:0;

                    $("#porcentaje").ionRangeSlider({
                        grid: true,
                        min: 0,
                        max: 100,
                        from: a,
                        prettify_enabled: false,
                        prefix: "%" 
                    });

                    var slider = $("#porcentaje").data("ionRangeSlider");

                    slider.update({
                        grid: true,
                        min: 0,
                        max: 100,
                        from: a,
                        prettify_enabled: false,
                        prefix: "%" 
                    });
                    
                }
            }
            
            retCodSolictComent(co_soli,'idComentsSolic',pp); 
            
            newModal('ViewSolicit','75','75','form_ViewSolicit'); 
            $('#modalViewSolicit').css('z-index','1042');
            $('#fadeViewSolicit').css('z-index','1041');

        }
    });
}

function retCodSolictComent(i,div,pp){
    var id_dep = $('#id_dpto').val();
    var url_1 = $('#txt_urlGe').val();
    $.ajax({
        type : 'POST',
        async: false,
        url : url_1+"procesos/helpdesk/co_101/retCodSolictComent", 
        data: "c_s="+i+"&c_d="+id_dep+"&p_p="+pp,
        success : function (returnData) {
            var data = eval(returnData); 
            $('#'+div).html('');
            if (data){ 
                $.each(data,function(e,dato){
                    var $_a = $('#'+div);
                    var text = dato.co_tipo === conf_hd.sol[0].type?conf_hd.sol[0].name:conf_hd.sol[1].name;
                    if (dato.fl_responder === '1'){
                        var row = '<div class="activity">';
                            row = row+'<img class="activity_img" src="'+dato.img+'">';
                            row = row+'<div class="activity__message">';
                                row = row+'<div class="activity__message__header">';
                                    row = row+'<h4>'+dato.apellidos+" "+dato.nombres+'</h4>';
                                    row = row+'<div style="color: #949494;font-size: 12px;">'+text+' #'+dato.co_solicitud+' - '+dato.tx_titulo+'</div>';
                                    row = row+'<span class="span_right">'+dato.fe_comentario+'</span>';
                                row = row+'</div>';
                                row = row+'<p>'+dato.tx_descripcion+'</p>';
                                row = row+'<span class="timeChat"></span>';
                            row = row+'</div>';
                        row = row+'<div>';
                        $_a.append(row);
                    }
                });
            }

        }
    });
}

function saveComentSolic(msg,cod_sol,a){
    var url_1 = $('#txt_urlGe').val();
    var datos = "m_g="+msg+"&f_p="+a;
    datos = datos+"&c_s="+cod_sol;
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_101/guardarComent", 
        data: datos
    });
    //window.location.reload();
}