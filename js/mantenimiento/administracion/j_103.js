
$(function () {
    getAllNoticia();
    $(".btnN").button().click(function () {
        $('#txt_accion').val('I');
        $('#id_noticia').val('');
        newModal('Noticias', '70', '75', 'form_noticias');
    });
    $(".resetForm").button().click(function () {
        resetNoticia();
    });
    $(".saveForm").button().click(function () {
        saveNoticia();
    });
});

function resetNoticia() {
    $('#id_perfil').val('');
    CKEDITOR.instances['txt_descripcion'].setData('');
    quitarmodalGeneral('Noticias', 'form_noticias');
}

function consulNoticia(accion, id) {
    $('#txt_accion').val(accion);
    $('#id_noticia').val(id);
    newModal('Noticias', '70', '75', 'form_noticias');
    getNoticia(id);
}

function getNoticia(id) {
    if (va_sess()) {
        dataLoading();
        $.post('co_103/consulta',
                {id: id},
                function (c) {
                    c = eval('(' + c + ')');
                    if ($.isEmptyObject(c.err)) {
                        $('#txt_titulo').val(c.dat.ds_not);
                        CKEDITOR.instances['txt_descripcion'].setData(c.dat.ds_des);
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}

function deleteNoticia(id) {
    if (va_sess()) {
        dataLoading();
        $.post('co_103/deleteNoticia',
                {id: id},
                function (c) {
                    c = eval('(' + c + ')');
                    if ($.isEmptyObject(c.err)) {
                        getAllNoticia();
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_5, 'success');
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}

function getAllNoticia() {
    if (va_sess()) {
        dataLoading();
        $.post('co_103/getNoticias',
                {a_l: 1},
                function (c) {
                    c = eval('(' + c + ')');
                    if ($.isEmptyObject(c.err)) {
                        $('#contTableNoticias').html(c.dat);
                        //checkPublic();
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    $('#table_Noticias').dataTable();
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}

function saveNoticia() {
    if (va_sess()) {
        var acc = $('#txt_accion').val();
        var id = $('#id_noticia').val();
        var tit = $('#txt_titulo').val();
        var des = CKEDITOR.instances['txt_descripcion'].getData();

        if ($.isEmptyObject(tit)) {
            errorGeneral(conf_hd.tx.t_1, 'El campo "Titulo" esta vacio', 'danger');
            return false;
        }

        if ($.isEmptyObject(des)) {
            errorGeneral(conf_hd.tx.t_1, 'El campo "Descripci&oacute;n" esta vacio', 'danger');
            return false;
        }
        dataLoading();
        $.post('co_103/guardar',
                {acc: acc, id: id, tit: tit, des: des},
                function (c) {
                    c = eval('(' + c + ')');
                    if ($.isEmptyObject(c.err)) {
                        $('#id_noticia').val('');
                        quitarmodalGeneral('Noticias', 'form_noticias');
                        getAllNoticia();
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_5, 'success');
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                    $('#table_Noticias').dataTable();
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}

function publicarNoticia(id) {
    if (va_sess()) {
        var acc = $("#ck_" + id).is(':checked') ? 'P' : 'I';
        dataLoading();
        $.post('co_103/publicarNoticia',
                {id: id, acc: acc},
                function (c) {
                    c = eval('(' + c + ')');
                    if ($.isEmptyObject(c.err)) {
                        getAllNoticia();
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}
