$(function() {
    getAllUsers();
    $( ".btnN" ).button().click(function(){
        $('#id_user').val('');
        $('#txt_accion').val('I'); 
        $('#txt_cod_emp').removeAttr('readonly');
        $('#txt_nom_user').removeAttr('readonly');
        newModal('Users','50','60','form_users');
    });
    
    $( ".resetForm" ).button().click(function(){
        resetUsers();
    });
    
    $( ".saveForm" ).button().click(function(){
        saveUsers();
    });
    
}); 

function resetUsers(){
    $('#id_user').val('');
    $('#txt_accion').val(''); 
    quitarmodalGeneral('Users','form_users');
}

function consulUsers(accion,id){
    $('#txt_accion').val(accion);
    $('#id_user').val(id);
    newModal('Users','50','60','form_users');
    getUsers(id);
}

function saveUsers(){
        var url_1 = $('#txt_urlGe').val();
        var accion = $('#txt_accion').val();
        var id = $('#id_user').val();
        var permisos = [];
        
        
        if ($('#txt_cod_emp').val().trim() === ''){
            errorGeneral("Sistema Intranet",'El campo "Cod. de Empleado" esta vacio','danger'); 
            return false;
        }
        
        if ($('#txt_nom_user').val().trim() === ''){
            errorGeneral("Sistema Intranet",'El campo "Nombre de Usuario" esta vacio','danger'); 
            return false;
        }
        
        if ($('#txt_pass').val().trim() === ''){
            errorGeneral("Sistema Intranet",'El campo "Clave" esta vacio','danger'); 
            return false;
        }
        
        if ($('#txt_cod_resp').val().trim() === ''){
            errorGeneral("Sistema Intranet",'El campo "Cod. Responsable" esta vacio','danger'); 
            return false;
        }
        
        $("input[name='cb_w[]']:checked").each( function () {
            permisos.push($(this).val());
        });
        
        var pass = $('#txt_pass').val();
        
        dataLoading();
        
        var datos = "accion="+accion ;
        datos = datos+"&c_e="+$('#txt_cod_emp').val();
        datos = datos+"&c_r="+$('#txt_cod_resp').val();
        datos = datos+"&n_e="+$('#txt_nom_emp').val();
        datos = datos+"&a_e="+$('#txt_ape_emp').val();
        datos = datos+"&n_u="+$('#txt_nom_user').val();
        datos = datos+"&e_l="+$('#txt_email').val();
        datos = datos+"&co_e="+$('#txt_cod_empresa').val();
        datos = datos+"&co_c="+$('#txt_cod_cc').val();
        datos = datos+"&c_a="+pass;
        datos = datos+"&p_s="+permisos;
        datos = datos+"&i_d="+id;
        
        $.ajax({
            type : 'POST',
            url : url_1+"mantenimiento/administracion/co_102/guardar", 
            data: datos,
            success : function (returnData) {
            var data =returnData.trim();
            removedataLoading();
            if(data.trim()==="OK"){ 
                $('#id_user').val('');
                quitarmodalGeneral('Users','form_users');
                getAllUsers();
                errorGeneral("Sistema Intranet",'Registro grabado correctamente','success'); 
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }

         }
    });

}

function getUsers(id){
    var url_1 = $('#txt_urlGe').val();

    $.ajax({

        type : 'POST',
        async: false,
        url : url_1+"mantenimiento/administracion/co_102/consulta", 
        data: "id="+id,
        success : function (returnData) {
            var data = eval(returnData); 

            if (data){ 
                $i = 0;
                $.each(data,function(e,dato){
                    if($i === 0){
                        //txt_cod_resp
                        $('#txt_cod_emp').attr('readonly','readonly');
                        $('#txt_nom_user').attr('readonly','readonly');
                        $('#txt_cod_emp').val(dato.c_e);
                        $('#txt_nom_emp').val(dato.n_s);
                        $('#txt_ape_emp').val(dato.a_s);
                        $('#txt_email').val(dato.e_l);
                        $('#txt_nom_user').val(dato.u_o);
                        $('#txt_pass').val(dato.c_l);
                        consultaResponsable(dato.c_r);
                    }
                    
                    $("#cb_w"+dato.p_s.trim()).prop('checked',true);
                    $i++;
                });
            }

        }
    });

} 

function deleteUsers(id){
    var url_1 = $('#txt_urlGe').val();
    $.ajax({
        type : 'POST',
        url : url_1+"mantenimiento/administracion/co_102/deleteUsers", 
        data: "id="+id,            
        success : function (returnData) {
            var data =returnData.trim();
            if (data.trim() === 'OK'){
                getAllUsers()
                errorGeneral("Sistema Intranet",'Registro eliminado correctamente','success'); 
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }

        }
    });    
}

function getAllUsers() {

    var url1 = $('#txt_urlGe').val();
    
    $.ajax({
        type : 'POST',
        async: false,
        url : url1+'mantenimiento/administracion/co_102/getUsers',
        
        success : function (returnData) {
                var data = eval(returnData); 
                
                $('#contTableUsers').html('');
                var row = '<table id="table_perfil" class="table table-bordered table-hover">';
                        row = row+'<thead>';
                        row = row+'<th>Código</th>';
                        row = row+'<th>Usuario</th>';
                        row = row+'<th>Nombre</th>';
                        row = row+'<th>Centro Costo</th>';
                        row = row+'<th>Estado</th>';
                        row = row+'<th>Modificar</th>';
                        row = row+'</thead>';
                        row = row+'<tbody>';
                        
                if (data){
                
                    $.each(data,function(e,dato){
                        row = row+'<tr>';
                        row = row+'<td>'+dato.c_p+'</td>';
                        row = row+'<td>'+dato.user+'</td>';
                        row = row+'<td>'+dato.nombre+'</td>';
                        row = row+'<td>'+dato.n_ccos+'</td>';
                        row = row+'<td>'+dato.estado+'</td>';
                        row = row+'<td><center><a href="javascript:void(0)" onclick="consulUsers('+"'U'"+','+"'"+dato.c_p+"'"+')">Modificar</a></center></td>';
                    });
                    
                }

                row = row+'</tbody>';
                row = row+'</table>';
                
                $('#contTableUsers').append(row);
                
        }
           
    });
    
    $('#table_perfil').dataTable();
}

function getTreeview(id){
    
    var url = $('#txt_urlGe').val();
    newModal('Treeview','30','80','form_Treeview');
    $('#modalTreeview').css('z-index','1042');
    $('#fadeTreeview').css('z-index','1041');
    retornaTreeview(id,url)

}

function retornaTreeview(id,url){
    $("#TV").html('');
    $.ajax({
        type : 'POST',
        url : url+"mantenimiento/administracion/co_102/getTreeview", 
        data: "id="+id,
        success : function (returnData) {
            var data =returnData.trim();  
            $("#TV").html(data);
            treeview.expandAll();   
            $("#form_Treeview :input").prop("disabled", true);
        }
    });        
}

function consultaEmpleado(cod_emp){
    var url = $('#txt_urlGe').val();
    var accion = $('#txt_accion').val();
    
    if ($('#txt_cod_emp').val().trim() === ''){
        $('#txt_cod_emp').val('');
        $('#txt_nom_emp').val('');
        $('#txt_ape_emp').val('');
        $('#txt_cod_empresa').val('');
        $('#txt_cod_cc').val('');
        return false;
    }
    
    if(accion.trim() === 'I'){
        $.ajax({
            type : 'POST',
            url : url+"mantenimiento/administracion/co_102/retEmpleado", 
            data: "c_p="+cod_emp,
            success : function (returnData) {
                if (returnData.trim().substr(0,2) === "OK"){
                    var data = eval(returnData.trim().substr(3));
                    if (data){
                        $.each(data,function(e,dato){
                            $('#txt_nom_emp').val(dato.nombre);
                            $('#txt_ape_emp').val(dato.apellido);
                            $('#txt_cod_empresa').val(dato.cod_empresa);
                            $('#txt_cod_cc').val(dato.cod_ccosto);
                        });
                    }
                } else {
                    $('#txt_cod_empresa').val('');
                    $('#txt_cod_cc').val('');
                    $('#txt_cod_emp').val('');
                    $('#txt_nom_emp').val('');
                    $('#txt_ape_emp').val('');
                    errorGeneral("Sistema Intranet",returnData,'danger');
                }
            }
        });
    }
    
}

function consultaResponsable(cod_emp){
    
    var url = $('#txt_urlGe').val();
    
    if (cod_emp.trim() === ''){
        $('#txt_cod_resp').val('');
        $('#txt_nom_resp').val('');
        $('#txt_ape_resp').val('');
        return false;
    }
    
    $.ajax({
        type : 'POST',
        url : url+"mantenimiento/administracion/co_102/retResponsable", 
        data: "c_r="+cod_emp,
        success : function (returnData) {
            if (returnData.trim().substr(0,2) === "OK"){
                var data = eval(returnData.trim().substr(3));
                if (data){
                    $.each(data,function(e,dato){
                        $('#txt_cod_resp').val(cod_emp);
                        $('#txt_nom_resp').val(dato.nombre);
                        $('#txt_ape_resp').val(dato.apellido);
                    });
                }
            } else {
                $('#txt_cod_resp').val('');
                $('#txt_nom_resp').val('');
                $('#txt_ape_resp').val('');
                errorGeneral("Sistema Intranet",returnData,'danger');
            }
        }
    });
    
}