
var option_count = 0;
$(function() {
    getAllPerfil();
    $( ".btnN" ).button().click(function(){
        //$("#form_perfiles :input").prop("disabled", true);
        $('#txt_accion').val('I');
        newModal('Perfiles','60','80','form_perfiles');
    });
    
    $( ".resetForm" ).button().click(function(){
        resetPerfil();
    });
    
    $( ".saveForm" ).button().click(function(){
        savePerfil();
    });
    
    $( ".add_option" ).button().click(function(){
        add_option();
    });
    
    $( ".resetFormOpciones" ).button().click(function(){
        resetOption();
    });
    
    $( ".saveFormOpciones" ).button().click(function(){
        saveEspecialOption();
    });
    
}); 

function resetPerfil(){
    $('#id_perfil').val('');
    quitarmodalGeneral('Perfiles','form_perfiles');
}

function resetOption(){
    option_count = 0;
    $('#table_body_option').html('');
    quitarmodalGeneral('opciones_especiales','form_opciones_especiales');
    $('#id_optionMenu').val('');
}

function consulPerfil(accion,id){
    $('#txt_accion').val(accion);
    $('#id_perfil').val(id);
    newModal('Perfiles','60','80','form_perfiles');
    getPerfil(id);
}

function savePerfil(){
        var url_1 = $('#txt_urlGe').val();
        var accion = $('#txt_accion').val();
        var id = $('#id_perfil').val();
        var treview_valor=$("#treview_valor").val();
        var arbol=treview_valor.split('|');
        var tamanio=arbol.length;
        var widgets = [];
        var postarbol = '';
        if ($('#txt_nombre').val().trim() === ''){
            errorGeneral("Sistema Intranet",'El campo "Nombre" esta vacio','danger'); 
            return false;
        }
        
        var datos = "accion="+accion ;
        $("input[name='cb_w[]']:checked").each( function () {
            widgets.push($(this).val());
        });
        
        
        dataLoading();
        for(var i=0; i<tamanio;i++ ){
            if(typeof($("#"+arbol[i]).val()) != "undefined"){
                var chequeado=$("#"+arbol[i]).prop("checked");
                if (chequeado === true){
                    var qAq = arbol[i].replace(/[cb_]/g,"");
                    if($("#iesp_"+qAq).length){
                        postarbol = postarbol+qAq+"|*|"+$('#iesp_'+qAq).val()+"/*/";
                    } else {
                        postarbol = postarbol+qAq+"|*|/*/";
                    }
                } 
           }
        }   
           
        datos = datos+"&nombre="+$('#txt_nombre').val();
        datos = datos+"&postarbolP="+decodeURIComponent(postarbol.substring(0, postarbol.length-3));
        datos = datos+"&id="+id;
        datos = datos+"&widgets="+widgets;
        
        $.ajax({
            type : 'POST',
            url : url_1+"mantenimiento/administracion/co_101/guardar", 
            data: datos,
            success : function (returnData) {
            var data =returnData.trim();
            removedataLoading();
            if(data.trim()==="OK"){ 
                treeview.collapseAll();
                $('#id_perfil').val('');
                quitarmodalGeneral('Perfiles','form_perfiles');
                getAllPerfil();
                errorGeneral("Sistema Intranet",'Registro grabado correctamente','success'); 
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }

         }
        });

}

function getPerfil(id){
    var url_1 = $('#txt_urlGe').val();

    $.ajax({

        type : 'POST',
        async: false,
        url : url_1+"mantenimiento/administracion/co_101/consulta", 
        data: "id="+id,
        success : function (returnData) {
            var data = eval(returnData); 

            if (data){ 
                var i = 0;
                $.each(data,function(e,dato){
                    if (i === 0){
                        $('#txt_nombre').val(dato.descripcion);
                        var a = eval(dato.dptos);
                        if (a){
                            $.each(a,function(e,d){
                                $("#cb_dpto"+d).prop('checked',true);
                            });
                        }
                    }
                    i++;
                    $("#cb_w"+dato.menu.trim()).prop('checked',true);
                });
            }

            consulta_menuPerfil(id,url_1); 

        }
    });

} 

function consulta_menuPerfil(id,url){

    $.ajax({
        type : 'POST',
        url : url+"mantenimiento/administracion/co_101/consulta_menuPerfil", 
        data: "id="+id,            
        success : function (returnData) {
            var data =returnData.trim();
            var arbol= data.split('|*|');
            var tamanio=arbol.length;

            for(var i=1; i<tamanio;i++ ){
                var datos= arbol[i].split('-');
                var q = datos[0].replace(/[cb_]/g,"");

                $("#cb_"+q).prop('checked',true);

                if (datos[1].trim() !== ''){
                    
                    $('#iesp_'+q).val(datos[1]);

                }

            }   

        }
    });    

}

function deletePerfil(id){
    var url_1 = $('#txt_urlGe').val();
    var r = confirm("Desea Eliminar el Registro.");
    if (r === true) {
        $.ajax({
            type : 'POST',
            url : url_1+"mantenimiento/administracion/co_101/deletePerfil", 
            data: "id="+id,            
            success : function (returnData) {
                var data =returnData.trim();
                if (data.trim() === 'OK'){
                    getAllPerfil()
                    errorGeneral("Sistema Intranet",'Registro eliminado correctamente','success'); 
                } else {
                    errorGeneral("Sistema Intranet",data,'danger'); 
                }

            }
        });   
    }  
}

function getAllPerfil() {

    var url1 = $('#txt_urlGe').val();
    
    $.ajax({
        type : 'POST',
        async: false,
        url : url1+'mantenimiento/administracion/co_101/getPerfiles',
        
        success : function (returnData) {
                var data = eval(returnData); 
                
                $('#contTablePerfil').html('');
                var row = '<table id="table_perfil" class="table table-bordered table-hover">';
                        row = row+'<thead>';
                        row = row+'<th>Perfil</th>';
                        row = row+'<th>Modificar</th>';
                        row = row+'<th>Eliminar</th>';
                        row = row+'</thead>';
                        row = row+'<tbody>';
                
                if (data){
                    $.each(data,function(e,dato){
                        row = row+'<tr>';
                        row = row+'<td>'+dato.descripcion+'</td>';
                        row = row+'<td><center><a href="javascript:void(0)" onclick="consulPerfil('+"'U'"+','+"'"+dato.codigo+"'"+')">Modificar</a></center></td>';
                        row = row+'<td><center><a href="javascript:void(0)" onclick="deletePerfil('+"'"+dato.codigo+"'"+')">Eliminar</a></center></td>';
                    });
                    
                }

                row = row+'</tbody>';
                row = row+'</table>';
                
                $('#contTablePerfil').append(row);
                
        }
           
    });
    
    $('#table_perfil').dataTable();
}

function openEspecialOption(id){
    
    newModal('opciones_especiales','30','50','form_opciones_especiales');
    $('#modalopciones_especiales').css('z-index','1042');
    $('#fadeopciones_especiales').css('z-index','1041');
    option_count = 0;
    $('#table_body_option').html('');
    $('#id_optionMenu').val(id);
    
}

function add_option(){
    option_count++;
    
    var a = $('#txt_op_id').val();
    var c = $('#txt_op_val').val();
    
    var b = '<tr>';
        b = b +'<td><input type="text" id="option_esp['+option_count+'][ident]" name="option_esp['+option_count+'][ident]" class="form-control" value="'+a+'"></td>';
        b = b +'<td><input type="text" id="option_esp['+option_count+'][valor]" name="option_esp['+option_count+'][valor]" class="form-control" value="'+c+'"></td>';
        b = b +'</tr>';
    
    $('#table_body_option').append(b);
    $('#txt_op_id').val('');
    $('#txt_op_val').val('');
}

function saveEspecialOption(){
    
    var id = $('#id_optionMenu').val();
    var count = option_count;
    var cadena = ''; 
    for (var i = 1; i < count+1; i++){
        cadena= cadena+document.getElementById("option_esp["+i+"][ident]").value+":"+document.getElementById("option_esp["+i+"][valor]").value+"|";
    }
    $('#iesp_'+id).val(cadena.substring(0, cadena.length-1));
    resetOption();
    
}

