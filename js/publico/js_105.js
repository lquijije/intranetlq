$(function () {
    $(".searchInventar").button().click(function () {
        getInventar();
    });
});

function getInventar() {
    var url_1 = $('#txt_urlGe').val();

    if ($('#txt_descr').val().trim() !== '') {
        if ($('#txt_descr').val().length < 5) {
            $('#tableConsultaInvent').dataTable().fnClearTable();
            errorGeneral("Sistema", 'El campo Descripción debe tener por lo menos 5 caracteres', 'danger');
            return false;
        }
    }

    if ($('#txt_descr').val().trim() === '') {
        errorGeneral("Sistema", 'El campo descripción se encuentra vacio', 'danger');
        return false;
    }

    var datos = "i_des=" + $('#txt_descr').val();
    $.ajax({
        type: 'POST',
        data: datos,
        url: url_1 + 'publico/co_105/getInventar',

        success: function (returnData) {

            if (returnData.trim().substr(0, 2) === "OK") {
                var data = eval(returnData.trim().substr(2));
                removedataLoading();

                $('#contTable').html('');

                var row = '<table id="tableConsultaInvent" class="table table-bordered">';
                row = row + '<thead>';
                row = row + '<th>Código</th>';
                row = row + '<th>Descripción</th>';
                row = row + '<th>Fabrica</th>';
                row = row + '<th></th>';
                row = row + '</thead>';
                row = row + '<tbody>';

                if (data) {
                    var i = 0;
                    $.each(data, function (e, dato) {
                        i++;
                        row = row + '<tr id="art_' + i + '">';
                        row = row + '<td>' + dato.cod_art + '</td>';
                        row = row + '<td>' + dato.ds_art + '</td>';
                        row = row + '<td>' + dato.co_fab + '</td>';
                        row = row + '<td class="centro">';
                        row = row + '<button type="button" class="btn btn-circle btn-danger" onclick="modalArtInvent(' + "'" + dato.cod_art + "'" + ',' + "'" + dato.ds_art.replace(/['"]/g, "") + "'" + ')"><i class="text-blanco text-size-1 fa fa-plus"></i></button>';
                        row = row + '</td>';
                    });
                }
                row = row + '</tbody>';
                row = row + '</table>';

                $('#contTable').append(row);
                $('#tableConsultaInvent').dataTable();
            } else {
                removedataLoading();
                $('#tableConsultaInvent').dataTable().fnClearTable();
                errorGeneral("Sistema", returnData, 'danger');
            }


        },
        //timeout: 10000,
        error: function (html) {
            removedataLoading();
            errorGeneral("Sistema", 'No hay conexion por favor intente nuevamente', 'danger');
            return false;
        }

    });
}

function modalArtInvent(cod_art, ds_art) {
    dataLoading();
    $.post('co_105/retDataArt',
            {i_art: cod_art},
            function (c) {
                c = eval('(' + c + ')');
                if ($.isEmptyObject(c.err)) {
                    $('#prodGeAl').html(cod_art + ' - ' + ds_art);
                    $('#DatProdInvet').html(c.dat);
                    newModal('ProdInvet', '60', '80', '');
                } else {
                    errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                }
                removedataLoading();
            }).fail(function (p) {
        removedataLoading();
        errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
    });
}