
$(function() {
    inicio();
    $( ".searchCI" ).button().click(function(){
        getPersonas('C');
    });
    
    $( ".searchName" ).button().click(function(){
        getPersonas('N');
    });
   
}); 
function inicio(){
    $('#detallePersonas').html('');
    var row = '<table id="T_docPersonas" class="table table-bordered table-hover">';
    row = row+'<thead>';
    row = row+'<th>Cédula</th>';
    row = row+'<th>Nombre</th>';
    row = row+'<th>Nacimiento</th>';
    row = row+'<th>Conyuge</th>';
    row = row+'<th>Domicilio</th>';
    row = row+'<th>Padre</th>';
    row = row+'<th>Madre</th>';
    row = row+'</thead>';
    row = row+'<tbody>';
    row = row+'</tbody>';
    row = row+'</table>';

    $('#detallePersonas').append(row);
    $('#T_docPersonas').dataTable();
}
function getPersonas(para){ 
    
    var url1 = $('#txt_urlGe').val();
    var ci = $('#txt_ci').val();
    var name = $('#txt_name').val();
    
    if (para.trim() === 'C'){
        
        if (ci.trim() === ''){
            errorGeneral("Sistema Intranet",'Campo Cédula esta vacio','danger');
            return false;
        }
        
        if (!validaCedula(ci)){
            return false;
        }
        
    } else if (para.trim() === 'N'){
        
        if (name.trim() === ''){
            errorGeneral("Sistema Intranet",'Campo Nombre esta vacio','danger');
            return false;
        }
    }
    
    var datos = "c_i="+ci;
    datos = datos + "&n_e="+name;
    datos = datos + "&t_p="+para;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'publico/co_103/retPersonas',
        data: datos,
        success : function (returnData) {
            
                removedataLoading();
                
                $('#detallePersonas').html('');
                var row = '<table id="T_docPersonas" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Cédula</th>';
                row = row+'<th>Nombre</th>';
                row = row+'<th>Nacimiento</th>';
                row = row+'<th>Conyuge</th>';
                row = row+'<th>Domicilio</th>';
                row = row+'<th>Padre</th>';
                row = row+'<th>Madre</th>';
                row = row+'</thead>';
                row = row+'<tbody>';
                    
                if (returnData.trim().substr(0,2) === "OK"){

                    var data = eval(returnData.trim().substr(3));

                    if (data){
                        $('#txt_ci').val('');
                        $.each(data,function(e,dato){
                            row = row+'<tr>';
                            row = row+'<td>'+dato.cedula+'</td>';
                            row = row+'<td>'+dato.nombres+'</td>';
                            row = row+'<td>'+dato.fech_nacim+'</td>';
                            row = row+'<td>'+dato.nomb_conyu+'</td>';
                            row = row+'<td>'+dato.domi_calle+'</td>';
                            row = row+'<td>'+dato.nombre_padre+'</td>';
                            row = row+'<td>'+dato.nombre_madre+'</td>';
                            row = row+'</tr>';
                        });
                    }  
                    
                } else {
                    if(returnData.trim() === ''){
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    } else {  
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                    }
                }
                
                row = row+'</tbody>';
                row = row+'</table>';

                $('#detallePersonas').append(row);
                $('#T_docPersonas').dataTable();
                
            }
    });

}
