
$(function() {
    inicio();
    $( ".searchAuto" ).button().click(function(){
        getAutos();
    });
}); 


function inicio(){
    $('#detalleAuto').html('');
    var row = '<table id="T_docAuto" class="table table-bordered table-hover">';
    row = row+'<thead>';
    row = row+'<th></th>';
    row = row+'<th>Identificación</th>';
    row = row+'<th>Propietario</th>';
    row = row+'<th>Placa</th>';
    row = row+'<th>Marca</th>';
    row = row+'</thead>';
    row = row+'<tbody>';
    row = row+'</tbody>';
    row = row+'</table>';

    $('#detalleAuto').append(row);
    $('#T_docAuto').dataTable();

}

function changeTip(){
    $('#txt_search').val('');
}

function getAutos(){ 
    
    var url1 = $('#txt_urlGe').val();
    var tipo = $('#tipo').val();
    var buscar = $('#txt_search').val();
    
    if (tipo.trim() === 'P'){
        
        if (buscar.trim() === ''){
            errorGeneral("Sistema Intranet",'Campo esta vacio','danger');
            return false;
        }
        
//        if (!validaCedula(buscar)){
//            return false;
//        }
        
    } else if (tipo.trim() === 'I'){
        
        if (buscar.trim() === ''){
            errorGeneral("Sistema Intranet",'Campo esta vacio','danger');
            return false;
        }
    }
    
    dataLoading();
                
    $('#detalleAuto').html('');
    var row = '<table id="T_docAuto" class="table table-bordered table-hover">';
    row = row+'<thead>';
    row = row+'<th></th>';
    row = row+'<th>Identificación</th>';
    row = row+'<th>Propietario</th>';
    row = row+'<th>Placa</th>';
    row = row+'<th>Marca</th>';
    row = row+'</thead>';
    row = row+'<tbody>';
    row = row+'</tbody>';
    row = row+'</table>';

    $('#detalleAuto').append(row);
    
    var dt = $('#T_docAuto').DataTable({
        "serverSide": true,
        "ajax": {
            'type': 'POST',
            'url': url1+'publico/co_104/retAutos',
            'data': {
               t_p: tipo,
               b_r: buscar
            },
            "dataSrc": function ( a ) {
                if(a.recordsFiltered >= 1){
                    return a.data;
                } else {
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger');
                    return false;
                }
            }   
        },
        "fnDrawCallback": function( oSettings ) {
            removedataLoading();
        },
        "columns": [
            {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
            },
            { "data": "identificacion" },
            { "data": "propietario" },
            { "data": "placa" },
            { "data": "marca" }
        ],
        "order": [[1, 'asc']]
    });
    
    
    var detailRows = [];
 
    $('#T_docAuto tbody').on( 'click', 'tr td:first-child', function () {
        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if (row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();
            detailRows.splice( idx, 1 );
        } else {
            tr.addClass('details');
            row.child( format( row.data() ) ).show();
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    });
    
    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td:first-child').trigger( 'click' );
        });
    });
    
}

function format ( d ) {
   return '<div style="padding:15px;display: inline-block;"><div class="spa-4">'+
            '<div class="spa-12 line">Clase: '+d.clase+'</div>'+
            '<div class="spa-12 line">Marca: '+d.marca+'</div>'+
            '<div class="spa-12 line">Modelo: '+d.modelo+'</div>'+
            '<div class="spa-12 line">Color: '+d.color+'</div>'+
          '</div>'+
          '<div class="spa-4">'+
            '<div class="spa-12 line">Chasis: '+d.chasis+'</div>'+
            '<div class="spa-12 line">Motor: '+d.motor+'</div>'+
            '<div class="spa-12 line">Tonelaje: '+d.tonelaje+'</div>'+
          '</div>'+
          '<div class="spa-4">'+
            '<div class="spa-12 line">Año: '+d.annio+'</div>'+
            '<div class="spa-12 line">País: '+d.pais+'</div>'+
            '<div class="spa-12 line">Servicio: '+d.servicio+'</div>'+
            '<div class="spa-12 line">Cantón: '+d.canton+'</div>'+
          '</div><div>';
}
