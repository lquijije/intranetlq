jQuery.download = function (url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        //send request
        jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
                .appendTo('body').submit().remove();
    }
    ;
};

if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^[\s\xA0]+|[\s\xA0]+$/g, '');
    };
}

$.xhrPool = [];
$.xhrPool.abortAll = function () {
    $(this).each(function (idx, jqXHR) {
        jqXHR.abort();
    });
    $.xhrPool = [];
};

$.ajaxSetup({
    beforeSend: function (jqXHR) {
        $.xhrPool.push(jqXHR);
    },
    complete: function (jqXHR) {
        var index = $.xhrPool.indexOf(jqXHR);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
    }
});

(function ($) {

    var xhrPool = [];
    $(document).ajaxSend(function (e, jqXHR, options) {
        xhrPool.push(jqXHR);
    });
    $(document).ajaxComplete(function (e, jqXHR, options) {
        xhrPool = $.grep(xhrPool, function (x) {
            return x != jqXHR
        });
    });
    var abort = function () {
        $.each(xhrPool, function (idx, jqXHR) {
            console.log('Abort');
            jqXHR.abort();
        });
    };

//        var oldbeforeunload = window.onbeforeunload;
//        window.onbeforeunload = function() {
//            var r = oldbeforeunload ? oldbeforeunload() : undefined;
//            if (r == undefined) {
//                console.log('Abort');
//                abort();
//            }
//            return r;
//        }
})($);

(function ($) {
    $.fn.extend({
        postlink: function (options) {
            var defaults = {'bandera': false};
            options = $.extend(defaults, options);
            return this.each(function () {
                $(this).click(function (e) {
                    var frm = $("<form>");
                    frm.attr({'action': $(this).attr('href'), 'method': 'post'});
                    if (options.csrf_protected) {
                        frm.append("<input name='user' value='" + $('#txt_ceGe').val() + "'>");
                    }
                    frm.appendTo("body");
                    frm.attr('target', '_blank').submit();
                    e.preventDefault();
                });
            });
        }
    });
})($);

$(document).ready(function () {

    $(".postlink").postlink({'bandera': true});

    var windowWidth = $(window).height();
    //alert(windowWidth)
    //getPorcentajes();
    $(".refrescar").button().click(function () {
        getPorcentajes();
    });

    $('.openMenu').button().click(function () {
        $("#menuEmergente").css("right", '0%');
    });

    $('.closeMenu').button().click(function () {
        closeMenu();
    });

    $('.ajaxAbort').click(function () {
        $.xhrPool.abortAll();
    });

    $('.save_pass_new').button().click(function () {
        val_pas_new();
    });

    if (navigator.sayswho === 'MSIE 9' || navigator.sayswho === 'IE 9') {

        $('.dashboard').css('animation', 'none');
        $('.dashboard').css('opacity', '1');
        $('.dashboard').css('margin', 'auto');
        $('.dashboard').css('transform', 'none');

    }

    $('#cmb_clima_ge').on('change', function () {
        var arr = $('#cmb_clima_ge option:selected').attr('data-label');
        arr = $.parseJSON(arr);

        var ra = '';
        var ro = '';

        if (arr) {

            ra = ra + '<div class="spa-6">' + arr.ic_cod + '</div>';
            ra = ra + '<div class="spa-6"><span>' + arr.co_tem + '<span class="gr_cen">&#186;C</span></span></div>';
            ra = ra + '<div class="spa-12 gr_nom">' + arr.co_tex + '</div>';
            ro = ro + '<div style="font-weight: bold;">Viento = ' + arr.ve_vie + ' km/h</div>';
            ro = ro + '<div style="font-weight: bold;">Humedad = ' + arr.at_hum + '%</div>';

            $('.clima_ico').html(ra);
            $('.clima_vie').html(ro);
        }

    });

});

function closeMenu() {
    $("#menuEmergente").css("right", '-20%');
    $('#sidebar-menu').css('display', 'block');
    $('#txt_searchGe').val('');
}

numeroG = 0;
contadorError = 0;
topCnt = 0;
var nav4 = window.Event ? true : false;

var message;

function errorGeneral(titulo, message, css_class) {
    contadorError++;

    $(document).keyup(function (event) {
        if (event.which === 27)
        {
            remueveErrorGe(contadorError);
        }
    });

    var icon = '';
    if (css_class.trim() === 'success') {
        icon = 'check';
    } else if (css_class.trim() === 'danger') {
        icon = 'ban';
    } else if (css_class.trim() === 'info') {
        icon = 'comment';
    }

    var box = document.createElement('div');
    box.setAttribute("id", "alertGeneral-" + contadorError + "");
    box.setAttribute("class", "alertGeneral alert alert-" + css_class + " alert-dismissable");

    if (contadorError === 1) {
        box.style.top = '0%';
    } else {
        topCnt += 15;
        box.style.top = topCnt + '%';
    }
    box.innerHTML = '<i class="fa fa-' + icon + '"></i> <button type="button" onClick="remueveErrorGe(' + "'" + contadorError + "'" + ')" id="cerrar_error" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4>' + titulo + '</h4>' + message + '';
    document.body.appendChild(box);

    var b = contadorError;
    setTimeout(function () {
        remueveErrorGe(b);
    }, 10000);

}

function remueveErrorGe(a) {

    if ($("#alertGeneral-" + a + "").length > 0) {
        $("#alertGeneral-" + a + "").remove();
        contadorError = 0;
        topCnt = 0;
    }

}

function dataGeneralLoading(id) {
    var b = '<div id="fade' + id + '" class="overlay">';
    b = b + '<div id="load' + id + '" class="loading-img"></div>';
    $("#" + id).append(b);
}

function removeGeneralLoading(id) {
    $("#fade" + id).remove();
    $("#load" + id).remove();
}

function dataLoading() {
    var load = $('#loading').css('display', 'block');
}

function removedataLoading() {
    var load = $('#loading').css('display', 'none')
}

function isNumero(evt) {
    var key = nav4 ? evt.which : evt.keyCode;
    return (key <= 13 || (key >= 45 && key <= 57));
}

function isNumeric(evt) {
    var key = nav4 ? evt.which : evt.keyCode;
    return (key <= 13 || key == 44 ||key == 46 || (key >= 48 && key <= 57));
}

function validaMail(e) {
    if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/.test(e)) {
        return true;
    } else if (e.trim() === "") {
        return false;
    } else {
        errorGeneral('Sistema', "Correo " + e + " es Incorrecto.", 'danger');
        return false;
    }
}

function isDecimal(e, field) {
    key = e.keyCode ? e.keyCode : e.which
    // backspace
    if (key === 8)
        return true
    // 0-9
    if (key > 47 && key < 58) {
        if (field.value === "")
            return true
        regexp = /.[0-9]{9}$/
        return !(regexp.test(field.value))
    }
    // .
    if (key === 46) {
        if (field.value === "")
            return false
        regexp = /^[0-9]+$/
        return regexp.test(field.value)
    }
    if (key === 9)
        return true;
    // other key
    return false;
}

function roundTo(num) {
    return +(Math.round(num + "e+2") + "e-2");
}

function retMes(a) {
    var monthNames = new Array();
    monthNames[1] = "Enero";
    monthNames[2] = "Febrero";
    monthNames[3] = "Marzo";
    monthNames[4] = "Abril";
    monthNames[5] = "Mayo";
    monthNames[6] = "Junio";
    monthNames[7] = "Julio";
    monthNames[8] = "Agosto";
    monthNames[9] = "Septiembre";
    monthNames[10] = "Octubre";
    monthNames[11] = "Noviembre";
    monthNames[12] = "Diciembre";
    return monthNames[a];
}

function retFechaActual(format) {
    fecha = new Date();
    var monthNames = new Array();
    monthNames[1] = "Enero";
    monthNames[2] = "Febrero";
    monthNames[3] = "Marzo";
    monthNames[4] = "Abril";
    monthNames[5] = "Mayo";
    monthNames[6] = "Junio";
    monthNames[7] = "Julio";
    monthNames[8] = "Agosto";
    monthNames[9] = "Septiembre";
    monthNames[10] = "Octubre";
    monthNames[11] = "Noviembre";
    monthNames[12] = "Diciembre";
    var curr_date = fecha.getDate();
    var curr_month = fecha.getMonth();
    curr_month = curr_month + 1;
    var curr_year = fecha.getFullYear();
    var curr_min = fecha.getMinutes();
    var curr_hr = fecha.getHours();
    var curr_sc = fecha.getSeconds();
    if (curr_month.toString().length == 1)
        curr_month = '0' + curr_month;
    if (curr_date.toString().length == 1)
        curr_date = '0' + curr_date;
    if (curr_hr.toString().length == 1)
        curr_hr = '0' + curr_hr;
    if (curr_min.toString().length == 1)
        curr_min = '0' + curr_min;

    if (format == 1)//dd-mm-yyyy
    {
        return curr_date + "-" + curr_month + "-" + curr_year;
    } else if (format == 2)//yyyy-mm-dd
    {
        return curr_year + "/" + curr_month + "/" + curr_date;
    } else if (format == 3)//dd/mm/yyyy
    {
        return curr_date + "/" + curr_month + "/" + curr_year;
    } else if (format == 4)// MM/dd/yyyy HH:mm:ss
    {
        return curr_date + " de " + monthNames[parseInt(curr_month)] + " del " + curr_year + " " + curr_hr + ":" + curr_min + ":" + curr_sc;
    } else if (format == 5)// MM/dd/yyyy HH:mm:ss
    {
        return curr_month + "/" + curr_date + "/" + curr_year;
    }
}

function formato_fechaDMA(fecha) {
    if (/^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/.test(fecha)) {
        return (true);
    } else if (fecha.trim() === "") {
        return (false)
    } else {
        errorGeneral('Sistema', "El formato de la fecha es dd/MM/AA")
        return (false)
    }
}

function num_format(num) {
    var out = parseFloat(Math.round(num * 100) / 100).toFixed(2);
    return out;
}

function retornaNumeroDecimales(valor, numerodecimales, multiplicadecimales) {
    var tempo = parseFloat(valor) * parseFloat(multiplicadecimales)
    valor = roundTo(parseFloat(tempo)) / parseFloat(multiplicadecimales);
    valor = valor.toFixed(numerodecimales);
    valor = formato_numero(valor, numerodecimales, ".", "");
    return valor;
}

function formato_numero(numero, decimales, separador_decimal, separador_miles) { // v2007-08-06
    numero = parseFloat(numero);
    if (isNaN(numero)) {
        return "";
    }

    if (decimales !== undefined) {
        // Redondeamos
        numero = numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero = numero.toString().replace(".", separador_decimal !== undefined ? separador_decimal : ",");

    if (separador_miles) {
        // AÃ±adimos los separadores de miles
        var miles = new RegExp("(-?[0-9]+)([0-9]{3})");
        while (miles.test(numero)) {
            numero = numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}

function validaFechaDDMMAAAA(fecha, separador) {
    //var dtCh= "/";
    var dtCh = separador;
    var minYear = 1900;
    var maxYear = 2100;
    function isInteger(s) {
        var i;
        for (i = 0; i < s.length; i++) {
            var c = s.charAt(i);
            if (((c < "0") || (c > "9")))
                return false;
        }
        return true;
    }
    function stripCharsInBag(s, bag) {
        var i;
        var returnString = "";
        for (i = 0; i < s.length; i++) {
            var c = s.charAt(i);
            if (bag.indexOf(c) == -1)
                returnString += c;
        }
        return returnString;
    }
    function daysInFebruary(year) {
        return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
    }
    function DaysArray(n) {
        for (var i = 1; i <= n; i++) {
            this[i] = 31
            if (i == 4 || i == 6 || i == 9 || i == 11) {
                this[i] = 30
            }
            if (i == 2) {
                this[i] = 29
            }
        }
        return this
    }
    // correcta m/d/Y
    // sistema d/M/y
    function isDate(dtStr) {
        var daysInMonth = DaysArray(12)
        var pos1 = dtStr.indexOf(dtCh)
        var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
        var tipo_fecha = $('#txt_tipo_fecha').val();
        //if (tipo_fecha.trim() === '1'){
        var strDay = dtStr.substring(0, pos1)
        var strMonth = dtStr.substring(pos1 + 1, pos2)
        var strYear = dtStr.substring(pos2 + 1)
        //}else{
        //var strMonth=dtStr.substring(0,pos1)
        //var strDay=dtStr.substring(pos1+1,pos2)
        //var strYear=dtStr.substring(pos2+1)                    
        //}
        strYr = strYear
        if (strDay.charAt(0) == "0" && strDay.length > 1)
            strDay = strDay.substring(1)
        if (strMonth.charAt(0) == "0" && strMonth.length > 1)
            strMonth = strMonth.substring(1)
        for (var i = 1; i <= 3; i++) {
            if (strYr.charAt(0) == "0" && strYr.length > 1)
                strYr = strYr.substring(1)
        }
        month = parseInt(strMonth)
        day = parseInt(strDay)
        year = parseInt(strYr)
        if (pos1 == -1 || pos2 == -1) {
            return false
        }
        if (strMonth.length < 1 || month < 1 || month > 12) {
            return false
        }
        if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
            return false
        }
        if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
            return false
        }
        if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
            return false
        }
        return true
    }
    if (isDate(fecha)) {
        return true;
    } else {
        return false;
    }
}

function manual_ayuda(url) {
    var controlador = $('#txt_idFormulario').val();
    window.open(url + 'manuales/' + controlador + '.pdf');
    //close();
}

function deletRow(a) {
    var row = a.split('-');
    alert("qqqqqq");
    var count = row.length;
    for (var i = 0; i < count; i++) {
        $('#D-' + row[i] + '').remove();
    }
}

function newModal(nameModal, ancho, alto, form) {
    $("#fade" + nameModal + "").remove();
    var Top = 100 - parseInt(alto);
    var diTop = Top / 2;

    $(document).keyup(function (event) {
        if (event.which === 27)
        {
            quitarmodalGeneral(nameModal, form);
        }
    });

    var a = document.createElement('div');
    a.setAttribute("id", "fade" + nameModal);
    a.setAttribute("class", "fadebox");
    $("body").append(a);
    $("#fade" + nameModal).css('display', 'block');
    $("#modal" + nameModal).css('top', diTop + '%');
    $("#modal" + nameModal).css('bottom', diTop + '%');
    $("#modal" + nameModal).css('width', ancho + '%');
    $("#modal" + nameModal).css('height', alto + '%');
    $("#modal" + nameModal).css('display', 'block');

}

function animatequitarModal(formulario) {
    $("#modal" + formulario + "").hide();
    $("#fade" + formulario + "").remove();
}

function quitarmodalGeneral(formulario, form) {
    animatequitarModal(formulario);
    if (form.trim() !== '') {
        $('#' + form)[0].reset();
    }
}

function validaCedula(cedula) {

    if (cedula.length == 10) {
        var digito_region = cedula.substring(0, 2);
        if (digito_region >= 1 && digito_region <= 24) {
            var ultimo_digito = cedula.substring(9, 10);
            var pares = parseInt(cedula.substring(1, 2)) + parseInt(cedula.substring(3, 4)) + parseInt(cedula.substring(5, 6)) + parseInt(cedula.substring(7, 8));
            var numero1 = cedula.substring(0, 1);
            var numero1 = (numero1 * 2);

            if (numero1 > 9) {
                var numero1 = (numero1 - 9);
            }
            var numero3 = cedula.substring(2, 3);
            var numero3 = (numero3 * 2);
            if (numero3 > 9) {
                var numero3 = (numero3 - 9);
            }
            var numero5 = cedula.substring(4, 5);
            var numero5 = (numero5 * 2);
            if (numero5 > 9) {
                var numero5 = (numero5 - 9);
            }
            var numero7 = cedula.substring(6, 7);
            var numero7 = (numero7 * 2);
            if (numero7 > 9) {
                var numero7 = (numero7 - 9);
            }
            var numero9 = cedula.substring(8, 9);
            var numero9 = (numero9 * 2);
            if (numero9 > 9) {
                var numero9 = (numero9 - 9);
            }
            var impares = numero1 + numero3 + numero5 + numero7 + numero9;
            var suma_total = (pares + impares);
            var primer_digito_suma = String(suma_total).substring(0, 1);
            var decena = (parseInt(primer_digito_suma) + 1) * 10;
            var digito_validador = decena - suma_total;
            if (digito_validador == 10)
                var digito_validador = 0;
            if (digito_validador == ultimo_digito) {
                return true;
            } else {
                errorGeneral('Sistema Intranet', 'La cedula:' + cedula + ' es incorrecta', 'danger');
                return false;
            }
        } else {
            errorGeneral('Sistema Intranet', 'Esta cedula no pertenece a ninguna region', 'danger');
            return false;
        }
    } else {
        errorGeneral('Sistema Intranet', 'Esta cedula tiene menos de 10 Digitos', 'danger');
        return false;
    }
}

function getPorcentajes() {

    $('#graph_porcentaje').html('');
    var url1 = $('#txt_urlGe').val();
    var fec_ini = $('#fec_ini').val();
    var fec_fin = $('#fec_fin').val();

    var datos = "fec_ini=" + fec_ini;
    datos = datos + "&fec_fin=" + fec_fin;
    dataGeneralLoading('w_1');
    $.ajax({
        type: 'POST',
        url: url1 + "general/principal/retPorcentajes",
        data: datos,
        success: function (returnData) {
            removeGeneralLoading('w_1');
            var data = eval(returnData);
            var row = '';
            var text = '';
            if (data) {
                var i = 0;
                $.each(data, function (a, dato) {
                    if (!dato.sms) {
                        if (dato.tipo === 'F') {
                            text = 'Fact.(%)';
                        } else if (dato.tipo === 'E') {
                            text = 'Ret.(%)';
                        } else if (dato.tipo === 'V') {
                            text = 'Nc.(%)';
                        } else if (dato.tipo === 'G') {
                            text = 'Gr.(%)';
                        }

                        var text_1 = dato.aprobados === 0 ? 0 : dato.aprobados;
                        var text_2 = dato.pendientes === 0 ? 0 : dato.pendientes;
                        var text_3 = dato.faltantes === 0 ? 0 : dato.faltantes;

//                            var text_1 = dato.aprobados === 0?'':dato.aprobados;
//                            var text_2 = dato.pendientes === 0?'':dato.pendientes;
//                            var text_3 = dato.faltantes === 0?'':dato.faltantes;

                        var t_1 = text_1;
                        var t_2 = text_2;
                        var t_3 = text_3;

                        if (text_1 >= 1 && text_1 <= 3) {
                            t_1 = 4;
                            if (text_2 > text_1 && text_2 > text_3) {
                                t_2 = t_2 - 3;
                            } else if (text_3 > text_1 && text_3 > text_2) {
                                t_3 = t_3 - 3;
                            }
                        }

                        if (text_2 >= 1 && text_2 <= 3) {
                            t_2 = 4;
                            if (text_1 > text_2 && text_1 > text_3) {
                                t_1 = t_1 - 3;
                            } else if (text_3 > text_1 && text_3 > text_2) {
                                t_3 = t_3 - 3;
                            }
                        }

                        if (text_3 >= 1 && text_3 <= 3) {
                            t_3 = 4;
                            if (text_1 > text_2 && text_1 > text_3) {
                                t_1 = t_1 - 3;
                            } else if (text_2 > text_1 && text_2 > text_3) {
                                t_2 = t_2 - 3;
                            }
                        }

                        //console.log(t_1+t_2+t_3);

                        row = row + '<tr>';
                        row = row + '<td class="izquierda" style="font-size: 15px;">' + text + '</td>';
                        row = row + '<td>';
                        row = row + '<a href="' + url1 + 'procesos/facturacion/co_101?tp=' + dato.tipo.trim() + '&fi=' + fec_ini.trim() + '&ff=' + fec_fin.trim() + '">';
                        row = row + '<div class="cont_chart">';
                        var v_1 = text_1 > 0 ? text_1 : '';
                        var v_2 = text_2 > 0 ? text_2 : '';
                        var v_3 = text_3 > 0 ? text_3 : '';
                        row = row + '<div data-original-title="' + text_1 + '% Aprobado" class="graph-bar-1" style="width:' + t_1 + '%;" data-toggle="tooltip" data-placement="top" title=""><span class="inner-p">' + v_1 + '</span></div>';
                        row = row + '<div data-original-title="' + text_2 + '% Pendiente" class="graph-bar-2" style="width:' + t_2 + '%;" data-toggle="tooltip" data-placement="top" title=""><span class="inner-p">' + v_2 + '</span></div>';
                        row = row + '<div data-original-title="' + text_3 + '% Erroneos" class="graph-bar-3" style="width:' + t_3 + '%;" data-toggle="tooltip" data-placement="top" title=""><span class="inner-p">' + v_3 + '</span></div>';
                        row = row + '</div>';
                        row = row + '</a>';
                        row = row + '</td>';
                        row = row + '</tr>';
                    } else {
                        row = row + '<tr>';
                        row = row + '<td></td>';
                        row = row + '<td class="centro">' + dato.sms + '</td>';
                        row = row + '</tr>';
                    }

                });

                $.each(data, function (posicion, dato) {
                    if (dato.error) {
                        errorGeneral('Sistema Intranet', dato.error, 'danger');
                        return false;
                    }
                });

            }

            $('#graph_porcentaje').append(row);
            $("[data-toggle='tooltip']").tooltip();
            $('#fecConsulta').text(retFechaActual(4));
        },
        timeout: 50000,
        error: function (jqXHR, exception) {
            removeGeneralLoading('w_1');
            if (jqXHR.status === 0) {
                errorGeneral("Error [0]", 'No hay conexion.\n Verifique la conexion.', 'danger');
            } else if (jqXHR.status == 404) {
                errorGeneral("Error [404]", 'Página no encontrada', 'danger');
            } else if (jqXHR.status == 500) {
                errorGeneral("Error [500]", 'Error interno del servidor', 'danger');
            } else if (exception === 'parsererror') {
                errorGeneral("Error", 'Error al retornar JSON', 'danger');
            } else if (exception === 'timeout') {
                errorGeneral("Error", 'Tiempo de respuesta', 'danger');
            } else if (exception === 'abort') {
                errorGeneral("Error", 'Petición abortada', 'danger');
            } else {
                errorGeneral("Error", jqXHR.responseText, 'danger');
            }
        }
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getVentas() {
    //retVentas
    $('#contW2').html('');
    var url1 = $('#txt_urlGe').val();
    var fec_ini = $('#txt_fec_Ge').val();
    var datos = "f_i=" + fec_ini;
    dataGeneralLoading('w_2');
    $.ajax({
        type: 'POST',
        url: url1 + "general/principal/retVentas",
        data: datos,
        success: function (returnData) {
            removeGeneralLoading('w_2');
            var arreglo = returnData.split('/*/');
            var data = eval(arreglo[0]);
            var arrFecha = fec_ini.split('|');
            if (data) {
                var dataChart = [];
                var i = 0;
                var actual = '';
                var valida = 0;
                var valAnio, valAnio_1, valAnio_2 = 0;
                $.each(data, function (posicion, dato) {
                    //console.log(dato)
                    valida += Math.round(parseInt(dato.venta)) + Math.round(parseInt(dato.vtapy)) + Math.round(parseInt(dato.cobpy));
                    if (dato.tipo === 'ANIO') {
                        valAnio = dato.venta;
                        valAnio_1 = dato.vtapy;
                        valAnio_2 = dato.cobpy;
                    }

                    if (dato.tipo !== 'ANIO') {
                        dataChart.push([i, dato.venta]);
                        i++;
                    }

                    if (dato.tipo === 'ACTUAL') {
                        var porc = roundTo((dato.venta * 100) / valAnio - 100);
                        var porcRemp = String(Math.round(porc)).replace(/[-]/g, "");
                        var updown = String(porc).substring(0, 1) === '-' ? 'fa-arrow-down' : 'fa-arrow-up';
                        //console.log(Math.round(porc));
                        actual = actual + '<tr>';
                        actual = actual + '<td rowspan="3">';
                        actual = actual + '<div class="spa-12" style="position: relative;">';
                        actual = actual + '<div id="flot-placeholder" style="font-size: 12px; color:#FFF;height: 80px; width: 100%; left: 20px; padding: 10px; position: absolute;"></div>';
                        actual = actual + '</div>';
                        actual = actual + '</td>';
                        actual = actual + '<td class="derecha text-yellow" style="font-size: 23px;"><a href="' + url1 + 'procesos/ventas/co_108?f=' + arrFecha[0].trim() + '&h=' + arrFecha[1].trim() + '" style="color:yellow">$' + formato_numero(roundTo(dato.venta), 0, ".", ",") + ' <span style="font-size:10px;">VTA</span><a></td>';
                        actual = actual + '<td style="font-size: 23px;" class="derecha">' + porcRemp + '%</td>';
                        actual = actual + '<td style="font-size: 23px;"><i class="fa ' + updown + '" data-toggle="tooltip" title="$' + formato_numero(roundTo(valAnio), 0, ".", ",") + '"></i></td>';
                        actual = actual + '</tr>';

                        var porc = roundTo((dato.vtapy * 100) / valAnio_1 - 100);
                        var porcRemp = String(Math.round(porc)).replace(/[-]/g, "");
                        var updown = String(porc).substring(0, 1) === '-' ? 'fa-arrow-down' : 'fa-arrow-up';
                        actual = actual + '<tr style="font-size: 12px;">';
                        actual = actual + '<td class="derecha text-yellow">$' + formato_numero(roundTo(dato.vtapy), 0, ".", ",") + ' <span style="font-size:10px;">TCP</span></td>';
                        actual = actual + '<td class="derecha">' + porcRemp + '%</td>';
                        actual = actual + '<td><i class="fa ' + updown + '" title="$' + formato_numero(roundTo(valAnio_1), 0, ".", ",") + '"></i></td>';
                        actual = actual + '</tr>';

                        var porc = roundTo((dato.cobpy * 100) / valAnio_2 - 100);
                        var porcRemp = String(Math.round(porc)).replace(/[-]/g, "");
                        var updown = String(porc).substring(0, 1) === '-' ? 'fa-arrow-down' : 'fa-arrow-up';
                        actual = actual + '<tr style="font-size: 12px;">';
                        actual = actual + '<td class="derecha text-yellow">$' + formato_numero(roundTo(dato.cobpy), 0, ".", ",") + ' <span style="font-size:10px;">COB</span></td>';
                        actual = actual + '<td class="derecha">' + porcRemp + '%</td>';
                        actual = actual + '<td><i class="fa ' + updown + '" title="$' + formato_numero(roundTo(valAnio_2), 0, ".", ",") + '"></i></td>';
                        actual = actual + '</tr>';
                    }

                });
                if (valida > 0) {
                    $('#contW2').append(actual);
                    var dataset = [{data: dataChart, color: "#FFF"}];
                    var options = {
                        series: {
                            bars: {
                                show: true
                            }
                        },
                        bars: {
                            align: "center",
                            barWidth: 0.6
                        },
                        xaxis: {
                            font: {
                                size: 11,
                                weight: "bold",
                                color: "#FFF"
                            },
                            ticks: eval(arreglo[1])
                        },
                        yaxis: {
                            ticks: ''
                        },
                        grid: {
                            borderWidth: 0,
                            hoverable: true
                        }
                    };

                    $.plot($("#flot-placeholder"), dataset, options);
                    $("#flot-placeholder").UseTooltip();
                    $('#fecConsultaVentas').text(retFechaActual(4));
                } else {
                    errorGeneral('Sistema Intranet', 'No hay datos en la busqueda realizada', 'danger');
                    return false;
                }
            }
        }

    });
}

function gd(year, month, day) {
    return new Date(year, month, day).getTime();
}

var previousPoint = null, previousLabel = null;


$.fn.UseTooltip = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (item) {
            if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                previousPoint = item.dataIndex;
                previousLabel = item.series.label;
                $("#tooltipFlot").remove();

                var x = item.datapoint[0];
                var y = item.datapoint[1];

                var color = item.series.color;

                showTooltip(item.pageX,
                        item.pageY,
                        color,
                        "<strong>$" + formato_numero(roundTo(y), 0, ".", ",") + "</strong>");
            }
        } else {
            $("#tooltipFlot").remove();
            previousPoint = null;
        }
    });
};

$.fn.UseTooltip2 = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (item) {
            //console.log(item)
            var tit = '';
            if (item.series.data[0][0] == 0) {
                tit = 'Pendiente';
            } else if (item.series.data[0][0] == 1) {
                tit = 'Enviado';
            } else if (item.series.data[0][0] == 2) {
                tit = 'Cancelado';
            }
            tit = '';
            if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                previousPoint = item.dataIndex;
                previousLabel = item.series.label;
                //console.log(previousLabel)
                $("#tooltipFlot").remove();

                var x = item.datapoint[0];
                var y = item.datapoint[1];

                var color = item.series.color;

                showTooltip(item.pageX,
                        item.pageY,
                        color,
                        "<strong>" + tit + "" + formato_numero(roundTo(y), 0, ".", ",") + "</strong>");
            }
        } else {
            $("#tooltipFlot").remove();
            previousPoint = null;
        }
    });
};

//0995573927 KG

function showTooltip(x, y, color, contents) {
    $('<div id="tooltipFlot">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 30,
        left: x - 10,
        border: '2px solid ' + color,
        padding: '3px',
        'font-size': '9px',
        'border-radius': '5px',
        'background-color': '#fff',
        'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        opacity: 0.9
    }).appendTo("body").fadeIn(100);
}

function loadPagePrinc() {

    if ($("#w_2").length) {
        getVentas();
    }
//        if($("#w_1").length){
//            getPorcentajes();
//        }
//        
//        var currentRequest = null;
//        function widget_1(a){
//            var url = $('#txt_urlGe').val();
//            return currentRequest = $.post(url+"general/principal/retVentas", { f_i:a });
//        }
//
//        function widget_2(a,b){
//            var url = $('#txt_urlGe').val();
//            return $.post(url+"general/principal/retPorcentajes", { fec_ini:a,fec_fin:b });
//        }
//        
//        $.when(widget_2('2015-02-21','2015-02-23'),widget_1('2015/02/23 | 10:00')).done(function(c){
//            $("span#firstName").html(c);
//        });
    //$.when(getVentas(),getPorcentajes());
}


function isEmptySis(a) {

    var array = a.split(':');
    var count = array.length;
    var p = 0;
    var c = '';

    for (var i = 0; i < count; i++) {
        var arr = array[i].split('|');
        var b = arr[0].toString().trim() === '' || arr[0].toString().trim() === '0' || arr[0].toString().trim() === 'NNN' || arr[0].toString().trim() === 'null' ? 0 : 1;
        if (b === 0) {
            p++;
            c = c + '&#9679; ' + arr[1] + '<br>';
        }
    }

    if (p > 0) {
        errorGeneral('Sistema Intranet', 'Los Siguientes Campos se encuentran vacios: <br>' + c + '', 'danger');
        return false;
    } else {
        return true;
    }

}



navigator.sayswho = (function () {
    var ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/);
        if (tem != null)
            return 'Opera ' + tem[1];
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null)
        M.splice(1, 1, tem[1]);
    return M.join(' ');
})();

$(document).keyup(function (event) {
    if (event.which === 27) {
        closeMenu();
    }
});

function val_pas_new() {
    document.getElementById("f_login").submit();
}

function va_sess() {
    var ban = true;
    var url1 = $('#txt_urlGe').val();
    $.ajax({
        type: 'POST',
        async: false,
        url: url1 + "general/error_general/valSess",
        success: function (res) {
            if (parseInt(res) === 0) {
                ban = false;
            }
        }
    });
    return ban;
}

function redirectIntranet() {
    var url = $('#txt_urlGe').val();
    window.location.href = url;
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

function secondToHour(seconds)
{
    var date = new Date(1970, 0, 1);
    date.setSeconds(seconds);
    return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
}
