
$(document).ready(function () {

    getAgente();

    var fec_1 = $('#f_i').val();
    var now_1 = new Date(fec_1);
    now_1.setTime(now_1.getTime() + 1 * 24 * 60 * 60 * 1000);

    var fec_2 = $('#f_f').val();
    var now_2 = new Date(fec_2);
    now_2.setTime(now_2.getTime() + 1 * 24 * 60 * 60 * 1000);

    $('#reportrange').daterangepicker({
        startDate: now_1,
        endDate: now_2
    },
    function (start, end) {
        $('#f_i').val(start.format("YYYY-MM-DD"));
        $('#f_f').val(end.format("YYYY-MM-DD"));
        $('#reportrange span').html(start.format('MMMM D YYYY') + ' - ' + end.format('MMMM D YYYY'));
        getAgente();
    }
    );

    $('.sav_a').click(function (event) {
        saveAsignacion();
        event.preventDefault();
    });

    $('.excel_btn').click(function (event) {
        var datos = "f_i=" + $('#f_i').val() + "&f_f=" + $('#f_f').val();
        $.download('co_4002/exExc', datos);
        event.preventDefault();
    });

});

function saveAsignacion() {

    var users = $("input[name='option_usu[]']:checked");
    var cnt = $("#cnt_cli").val();
    var tipo = $("#cmb_tip").val();
    var array = [];

    if (va_sess()) {

        if ($(users).length <= 0) {

            errorGeneral(conf_hd.tx.t_1, 'Seleccione uno o m&aacute;s colaboradores.', 'danger');

        } else {

            if ($.isEmptyObject(cnt)) {

                errorGeneral(conf_hd.tx.t_1, 'Debe ingresar la cantidad a asignar', 'danger');

            } else {

                $(users).each(function () {
                    array.push($(this).val());
                });

                dataLoading();
                $.post('co_4002/svAsig',
                        {d_t: JSON.stringify(array), c_a: cnt, t_p: tipo},
                function (c) {
                    c = eval('(' + c + ')');
                    if (!$.isEmptyObject(c)) {
                        if (c.co_err == 0) {
                            $('#cnt_cli').val('');
                            $("input[name='option_usu[]']").prop('checked', false);
                            getAgente();
                            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_8, 'success');
                        } else {
                            errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                        }
                    } else {
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
                    removedataLoading();
                    errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });

            }

        }

    } else {
        redirectIntranet();
    }
}

function getAgente() {
    $('#pieAdmin').html('');
    $('.detUsu').html('');
    var tot_cli = 0;
    var url = $('#txt_urlGe').val();
    if (va_sess()) {
        dataLoading();
        $.post('co_4002/reDas',
                {f_i: $('#f_i').val(), f_f: $('#f_f').val()},
        function (b) {
            var c = eval('(' + b + ')');

            if (!$.isEmptyObject(c.data)) {

                if (!$.isEmptyObject(c.data.pie)) {

                    $.plot("#pieAdmin", c.data.pie, {
                        series: {
                            pie: {
                                show: true,
                                radius: 1,
                                label: {
                                    show: true,
                                    radius: 1,
                                    threshold: 0.1,
                                    formatter: labelFormatter,
                                    background: {
                                        opacity: 0.7,
                                        color: '#333'
                                    }
                                }
                            }
                        },
                        grid: {
                            hoverable: true
                        },
                        legend: {
                            show: true
                        }
                    });

                    $.each(c.data.pie, function (e, y) {
                        tot_cli += y.data;
                    });

                } else {
                    $('#pieAdmin').html('<h4 class="text-blanco text-center">' + conf_hd.tx.t_6 + '</h4>');
                }

                $('#tot_cli').html(tot_cli);

                if (!$.isEmptyObject(c.data.usu)) {

                    $.each(c.data.usu, function (e, q) {

                        var p = '';
                        p = p + '<div class="spa-3">';
                        p = p + '<div class="box">';
                        p = p + '<div class="box-header text-center">';
                        p = p + '<img src="' + url + 'lib/loadImgNominaCredencial.php?ce=' + q.co_usu + '" class="img-circle" width="85" height="85">';
                        p = p + '</div>';
                        p = p + '<div class="box-body text-center">';
                        p = p + '<h6><b>' + q.no_usu + '</b></h6>';
                        p = p + '<h6>' + q.no_car + '</h6>';
                        p = p + '<div id="pie' + q.co_usu + '" class="des_box" style="height:80px;"></div>';
                        p = p + '</div>';
                        p = p + '<div class="box-footer text-center">';
                        p = p + '<div class="col-xs-6">';
                        p = p + '<i class="fa fa-check color_age"></i> <span class="text-blanco">' + q.ca_fin + '</span>';
                        p = p + '</div>';
                        p = p + '<div class="col-xs-6">';
                        p = p + '<i class="fa fa-exclamation color_age"></i> <span class="text-blanco">' + q.ca_pen + '</span>';
                        p = p + '</div>';
                        p = p + '<div class="clearfix"></div>';
                        p = p + '</div>';
                        p = p + '</div>';
                        p = p + '</div>';

                        $('.detUsu').append(p);


                        if (!$.isEmptyObject(q.pie)) {

                            $.plot("#pie" + q.co_usu, q.pie, {
                                series: {
                                    pie: {
                                        show: true,
                                        radius: 1,
                                        label: {
                                            show: true,
                                            radius: 1,
                                            threshold: 0.1,
                                            formatter: labelFormatter,
                                            background: {
                                                opacity: 0.7,
                                                color: '#333'
                                            }
                                        }
                                    }
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                }

                            });
                        } else {
                            $('#pieAge').html('<h4 class="text-blanco text-center">' + conf_hd.tx.t_6 + '</h4>');
                        }

                    });
                } else {

                }
            } else {
                errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
            }
            removedataLoading();
        }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });

    } else {
        redirectIntranet();
    }
}

function cambioAsignado(cod, nom, data) {
    data = decodeURIComponent(data);
    data = $.parseJSON(data);

    $('.sav_c').removeAttr('onclick');

    if (va_sess()) {
        if (cod > 0 && !$.isEmptyObject(nom) && !$.isEmptyObject(data)) {
            $('#titleUser').html(nom);
            var p = '<ul class="list-unstyled">';
            $.each(data, function (e, a) {
                if (a.co_est == 'A' || a.co_est == 'E' || a.co_est == 'P') {
                    p = p + '<li>';
                    p = p + '<input type="radio" id="ce_' + a.co_est + '" name="radio_est[]" value="' + a.co_est + '"> ' + a.ds_est;
                    p = p + '</li>';
                }
            });
            p = p + '</ul>';

            $('.sav_c').attr('onclick', 'saveCambioAsig(' + cod + ');');
            $('#est_usu').html(p);
            newModal('Users', '40', '60', '');
        }
    } else {
        redirectIntranet();
    }

}

function saveCambioAsig(co_usu) {

    var estado = $("input[name='radio_est[]']").is(':checked') ? $("input[name='radio_est[]']:checked").attr('value') : '';
    var usuario = $("input[name='radio_usu[]']").is(':checked') ? $("input[name='radio_usu[]']:checked").attr('value') : 0;

    if (!$.isEmptyObject(estado)) {
        if (usuario > 0) {
            dataLoading();
            $.post('co_4002/svCambioAsig',
                    {c_u: co_usu, u_c: usuario, e_s: estado, f_i: $('#f_i').val(), f_f: $('#f_f').val()},
            function (c) {
                c = eval('(' + c + ')');
                if (!$.isEmptyObject(c)) {
                    if (c.co_err == 0) {
                        quitarmodalGeneral('Users', '');
                        $('.sav_c').removeAttr('onclick');
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_8, 'success');
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                } else {
                    errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                }
                removedataLoading();
            }).fail(function (p) {
                removedataLoading();
                errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
            });
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Seleccione el usuario', 'danger');
        }
    } else {
        errorGeneral(conf_hd.tx.t_1, 'Seleccione el estado', 'danger');
    }

}

function labelFormatter(a, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;border-radius: 3px;'>" + Math.round(series.percent) + "%</div>";
}