
$(document).ready(function () {

    getAgente();
    $('.new_reg').hide();

    $('.form_hour').datetimepicker({
        datepicker: false,
        format: 'H:i',
        closeOnDateSelect: true
    });

    $(".con_cli").click(function (event) {
        updateClient('C');
        event.preventDefault();
    });

    $(".incon_cli").click(function (event) {
        updateClient('I');
        event.preventDefault();
    });

    $(".age_cli").click(function (event) {
        updateClient('A');
        event.preventDefault();
    });

    $(".btn_tomacli").click(function (event) {
        obtenerCliente();
        event.preventDefault();
    });

    $(".new_t").click(function (event) {
        $('.new_reg').show();
        $('.oc_reg').hide();
        $('#nu_tel').focus();
        $('#nu_tel').val('');
        $('#cmb_area_te').val('');
        $('#cmb_tip').val('');
        $('#ex_tel').val('');
        event.preventDefault();
    });

    $(".clo_t").click(function (event) {
        $('.new_reg').hide();
        $('.oc_reg').show();
        event.preventDefault();
    });

    $('#form_new_phone').bind('submit', function () {

        var item = {};

        item["co_acc"] = 'I';
        item["co_cli"] = $('#co_cli').val();
        item["co_cam"] = $('#co_cam').val();
        item["fe_car"] = $('#fe_car').val();
        item["cb_tip"] = $('#cmb_tip').val();
        item["ar_tel"] = $('#cmb_area_te').val();
        item["nu_tel"] = $('#nu_tel').val();
        item["ex_tel"] = $('#ex_tel').val();
        item["co_reg"] = $('#cmb_tg').val();

        if (!$.isEmptyObject(item["co_cli"])) {
            if (!$.isEmptyObject(item["nu_tel"]) && !$.isEmptyObject(item["cb_tip"])) {
                if (va_sess()) {
                    dataLoading();
                    $.post('co_4001/svTel',
                            {dt: item},
                    function (c) {
                        c = eval('(' + c + ')');
                        if (!$.isEmptyObject(c)) {
                            if (c.co_err == 0) {
                                retTelf(item["co_cli"], '');
                                cerr_new();
                                errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_8, 'success');
                            } else {
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                            }
                        } else {
                            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                        }
                        removedataLoading();
                    }).fail(function (p) {
                        removedataLoading();
                        errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                    });
                } else {
                    redirectIntranet();
                }
            } else {
                errorGeneral(conf_hd.tx.t_1, "Campo area o numero se encuentran vacios", 'danger');
            }
        } else {
            errorGeneral(conf_hd.tx.t_1, "Debe de seleccionar un cliente", 'danger');
        }

        return false;

    });

});

function resetControl() {
    $('#cmb_tip').val('01');
    $('#cmb_area_te').val('');
    $('#nu_tel').val('');
    $('#ex_tel').val('');
    $('#txt_comen').val('');
    $('#detET').html('');
    $('#co_cli').val('');
    $('#fe_car').val('');
    $('#co_cam').val('');
    $('#co_ci').val('');
    $('#txt_hora').val('');
    $('.ced_cli').html('');
    $('.nom_cli').html('');
    $('.ges_cli').html('');
    $('.deu_cli').html('');
    $('.dia_ven').html('');
}

function obtenerCliente() {
    resetControl();
    if (va_sess()) {

        $.post('co_4001/tomCli',
                {a: 1},
        function (c) {
            c = eval('(' + c + ')');
            if (!$.isEmptyObject(c)) {
                if (c.err.co_err == 0) {
                    getAgente();
                    if (!$.isEmptyObject(c.cab)) {
                        modalTelf(JSON.stringify(c.cab));
                    }
                    errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_8, 'success');
                } else {
                    errorGeneral(conf_hd.tx.t_1, c.err.tx_err, 'danger');
                }
            } else {
                errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
            }
            removedataLoading();
        }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });

    } else {
        redirectIntranet();
    }
}

function getAgente() {
    if (va_sess()) {

        $('.des_box').html('');
        dataLoading();
        $.post('co_4001/reAge',
                {ba: 1},
        function (b) {
            var c = eval('(' + b + ')');
            if (!$.isEmptyObject(c.data)) {
                if (!$.isEmptyObject(c.data.pie)) {
                    $.plot("#pieAge", c.data.pie, {
                        series: {
                            pie: {
                                //innerRadius: 0.5,
                                show: true,
                                radius: 1,
                                label: {
                                    show: true,
                                    radius: 1,
                                    threshold: 0.1,
                                    formatter: labelFormatter,
                                    background: {
                                        opacity: 0.7,
                                        color: '#333'
                                    }
                                }
                            }
                        },
                        grid: {
                            hoverable: true
                        },
                        legend: {
                            show: true,
                            labelFormatter: legendFormatter1
                        }

                    });
                } else {
                    $('#pieAge').html('<h4 class="text-blanco text-center">' + conf_hd.tx.t_6 + '</h4>');
                }

                if (!$.isEmptyObject(c.data.pro)) {
                    $('.p1_').html(c.data.pro.ba_pro);
                    $('.p2_').html(c.data.pro.mi_pro);
                    $('.p3_').html(c.data.pro.ma_pro);
                }

                if (!$.isEmptyObject(c.data.bar.data[0])) {

                    $.plot("#barAge", c.data.bar.data, {
                        grid: {
                            borderWidth: 1,
                            borderColor: "#DDD",
                            tickColor: "#DDD",
                            hoverable: true
                        },
                        series: {
                            bars: {
                                show: true,
                                barWidth: 0.1,
                                align: "center"
                            }
                        },
                        xaxis: {
                            ticks: c.data.bar.ticks
                        }
                    });

                } else {
                    $('#barAge').html('<h4 class="text-center">' + conf_hd.tx.t_6 + '</h4>');
                }

            } else {
                errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
            }
            removedataLoading();
        }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });

    } else {
        redirectIntranet();
    }
}

function getClien(t, p) {

    if (va_sess()) {

        $('#titleTelf').html(t);

        $('#tabCli').DataTable({
            bDestroy: true,
            sAjaxDataProp: "data",
            ajax: {
                url: "co_4001/reCli",
                type: "POST",
                data: {"c_e": t.substring(0, 1)},
                dataSrc: ""
            },
            columns: [
                {title: 'C&eacute;dula', "data": "id_cli", sClass: "text-center"},
                {title: 'Cliente', "data": "no_cli"},
                {title: 'Gesti&oacute;n', "data": "no_ges", sClass: "text-center"},
                {title: 'Deuda', "data": "va_deu", sClass: "text-right"},
                {title: 'Hora', "data": "fe_age", sClass: "text-right"},
                {title: 'Estado', "data": "ds_est", sClass: "text-center"},
                {title: 'Acci&oacute;n', "data": "html", sClass: "text-center"}
            ],
            order: [[1, "asc"]]
        });

        $('#tabCli th').css({background: p, color: "#FFF"});
        $('.modal-header').css({background: p});

        newModal('DetallTelf', '65', '90', '');

    } else {
        redirectIntranet();
    }
}

function modalTelf(a) {

    if (va_sess()) {
        console.log(a)
        var datos = $.parseJSON(a);

        resetControl();

        if (!$.isEmptyObject(datos.cab)) {

            $('.ced_cli').html(datos.cab.id_cli);
            $('.nom_cli').html(datos.cab.no_cli);
            $('.ges_cli').html(datos.cab.no_ges);
            $('.deu_cli').html(datos.cab.va_deu);
            $('.dia_ven').html(datos.cab.di_ven);

            $('#co_cli').val(datos.cab.co_cli);
            $('#fe_car').val(datos.cab.fe_car);
            $('#co_cam').val(datos.cab.co_cam);
            $('#co_ci').val(datos.cab.id_cli);
            $('#txt_comen').val(datos.cab.tx_obs);

            retTelf(datos.cab.co_cli, datos.cab.co_est);

            quitarmodalGeneral('DetallTelf', '');

        }
    } else {
        redirectIntranet();
    }
}

function retTelf(co_cli, co_est) {

    var p = '<h3 class="ti_tipo">Tel&eacute;fonos Cliente</h3>';
    $('#detET').html('');
    var item = {};
    var fe_car = $('#fe_car').val();
    var co_cam = $('#co_cam').val();

    item["co_cli"] = co_cli;
    item["co_cam"] = co_cam;
    item["fe_car"] = fe_car;
    item["co_est"] = co_est;

    $.post('co_4001/reTel',
            {dt: item},
    function (c) {
        c = eval(c);
        if (!$.isEmptyObject(c)) {

            $.each(c, function (e, f) {

                if (f.co_reg == 'RF') {
                    p = p + '<div class="clearfix"></div><h3 class="ti_tipo" style="margin-top:10px;">Tel&eacute;fonos Referencia</h3>';
                }

                $.each(f.groupeddata, function (e, s) {
                    var tipo_ = '';

                    if (s.co_sec > 0) {

                        if (f.co_reg == 'RF') {
                            tipo_ = 'Trabajo' + ' - ' + s.no_par + ' - ' + s.no_ref + ' ' + s.ap_ref;
                        } else {
                            tipo_ = s.no_tip;
                        }

                        p = p + '<div class="formWrap tel-' + s.co_sec + '">';

                        p = p + '<div class="col-xs-1">';
                        p = p + '<div class="group">';
                        p = p + '<input type="text" id="are_' + s.co_sec + '" value="' + s.co_are + '" maxlength="2" onkeyPress="return isNumero(event,this);">';
                        p = p + '<input type="hidden" id="reg_' + s.co_sec + '" value="' + f.co_reg + '">';
                        p = p + '<input type="hidden" id="se_ref_' + s.co_sec + '" value="' + s.se_ref + '">';
                        p = p + '<input type="hidden" id="no_ref_' + s.co_sec + '" value="' + s.no_ref + '">';
                        p = p + '<input type="hidden" id="ap_ref_' + s.co_sec + '" value="' + s.ap_ref + '">';
                        p = p + '<input type="hidden" id="par_' + s.co_sec + '" value="' + s.co_par + '">';
                        p = p + '<input type="hidden" id="ciu_' + s.co_sec + '" value="' + s.co_ciu + '">';
                        p = p + '<input type="hidden" id="pro_' + s.co_sec + '" value="' + s.co_pro + '">';

                        p = p + '<label for="name">Area</label>';
                        p = p + '<div class="bar"></div>';
                        p = p + '</div>';
                        p = p + '</div>';

                        p = p + '<div class="col-xs-6">';
                        p = p + '<div class="group">';
                        p = p + '<input type="text" id="txt_' + s.co_sec + '" value="' + s.co_cel + '"  maxlength="10" onkeyPress="return isNumero(event,this);">';
                        p = p + '<input type="hidden" id="tip_' + s.co_sec + '" value="' + s.co_tip + '">';
                        p = p + '<label for="name">' + tipo_ + '</label>';
                        p = p + '<div class="bar"></div>';
                        p = p + '</div>';
                        p = p + '</div>';

                        p = p + '<div class="col-xs-5">';
                        p = p + '<div class="group">';
                        p = p + '<button type="button" class="btn btn-circle btn-success btn_dos sin_acc" onclick="updateTelef(' + "'U'" + ',' + s.co_sec + ');"><i class="text-blanco fa fa-edit"></i></button>';
                        p = p + '<button type="button" class="btn btn-circle btn-danger btn_era sin_acc" onclick="updateTelef(' + "'D'" + ',' + s.co_sec + ');"><i class="text-blanco fa fa-minus"></i></button>';
                        p = p + '<input type="text" id="ext_' + s.co_sec + '" value="' + s.ex_tel + '" maxlength="5" onkeyPress="return isNumero(event,this);">';
                        p = p + '<label for="name">Extensi&oacute;n</label>';
                        p = p + '<div class="bar"></div>';
                        p = p + '</div>';
                        p = p + '</div>';

                        p = p + '</div>';

                    }

                });

            });

            $('#detET').html(p);

            if (co_est == 'C') {
                $('.sin_acc').hide();
            }

        } else {
            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
        }
        removedataLoading();
    }).fail(function (p) {
        removedataLoading();
        errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
    });
}

function updateTelef(co_est, co_sec) {

    if (co_est == 'D') {
        if (confirm(conf_hd.tx.t_9) == false) {
            return false;
        }
    }

    if (va_sess()) {

        var co_cli = $('#co_cli').val();
        var fe_car = $('#fe_car').val();
        var co_cam = $('#co_cam').val();

        if (!$.isEmptyObject(co_cli) && !$.isEmptyObject(fe_car) && !$.isEmptyObject(co_cam) && !$.isEmptyObject(co_est)) {

            var item = {};

            item["co_est"] = co_est;
            item["co_cli"] = co_cli;
            item["co_cam"] = co_cam;
            item["fe_car"] = fe_car;
            item["co_sec"] = co_sec;
            item["co_reg"] = $('#reg_' + co_sec).val();
            item["ar_tel"] = $('#are_' + co_sec).val();
            item["ti_tel"] = $('#tip_' + co_sec).val();
            item["nu_tel"] = $('#txt_' + co_sec).val();
            item["ex_tel"] = $('#ext_' + co_sec).val();
            item["se_ref"] = $('#se_ref_' + co_sec).val();
            item["no_ref"] = $('#no_ref_' + co_sec).val();
            item["ap_ref"] = $('#ap_ref_' + co_sec).val();
            item["co_par"] = $('#par_' + co_sec).val();
            item["co_ciu"] = $('#ciu_' + co_sec).val();
            item["co_pro"] = $('#pro_' + co_sec).val();


            $.post('co_4001/upTel',
                    {dt: item},
            function (c) {
                c = eval('(' + c + ')');
                if (!$.isEmptyObject(c)) {
                    if (c.co_err == 0) {
                        retTelf(co_cli, '');
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_8, 'success');
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                } else {
                    errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                }
                removedataLoading();
            }).fail(function (p) {
                removedataLoading();
                errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
            });

        } else {
            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_7, 'danger');
        }

    } else {
        redirectIntranet();
    }

}

function updateClient(co_est) {

    if (va_sess()) {

        var co_cli = $('#co_cli').val();
        var fe_car = $('#fe_car').val();
        var co_cam = $('#co_cam').val();
        var co_ci = $('#co_ci').val();

        if (!$.isEmptyObject(co_cli) && !$.isEmptyObject(fe_car) && !$.isEmptyObject(co_cam) && !$.isEmptyObject(co_est)) {

            var item = {};

            item["co_est"] = co_est;
            item["co_cli"] = co_cli;
            item["co_cam"] = co_cam;
            item["fe_car"] = fe_car;
            item["co_ci"] = co_ci;
            item["tx_hor"] = $('#txt_hora').val();
            item["tx_com"] = $('#txt_comen').val();

            if (co_est == 'A' && item["tx_hor"] == '') {
                errorGeneral(conf_hd.tx.t_1, 'Debe ingresar la hora', 'danger');
                return false;
            }

            $.post('co_4001/upCli',
                    {dt: item},
            function (c) {
                c = eval('(' + c + ')');
                if (!$.isEmptyObject(c)) {
                    if (c.co_err == 0) {
                        resetControl();
                        getAgente();
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_8, 'success');
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                } else {
                    errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                }
                removedataLoading();
            }).fail(function (p) {
                removedataLoading();
                errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
            });

        } else {
            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_7, 'danger');
        }

    } else {
        redirectIntranet();
    }

}

function labelFormatter(a, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;border-radius: 3px;'>" + Math.round(series.percent) + "%</div>";
}
function legendFormatter1(label, series) {
    return '<div style="font-size:8pt;text-align:center;padding:2px;"><a href="javascript:void(0)" onclick="getClien(' + "'" + label + "'" + ',' + "'" + series.color + "'" + ')">' + label + '</a></div>';
}
;