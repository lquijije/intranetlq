$(iniciar);
// Inicio del documento
function iniciar(){
	$(document).keyup(function (event) {
        //Tecla Escape
        if (event.which === 27)
        {
        	var nameModal = $('#hdMdlAct').val();
        	if(nameModal!="")        		quitarModal();
        }
    });
	// CLICK Boton <Buscar Bodega despacho>
	$(".btAplFil").button().click(agrBodDesp);
	//Click check Por Despachar / Modificar Despachado
	$('input[type=radio]').on('change',evtRadioCheck);
	agrBodDesp();
	//retTablaPedidosPorDespachar();
}

function agrBodDesp(){
	$('#hdMdlAct').val('BodPed');
	newModal('BodPed','25','50','form_bodped');
	$('#table_pedido').dataTable().fnDestroy();
	$('#table_pedido').dataTable({
		"bPaginate"	: false, 
		"bFilter"	: false, 
		"bInfo" 	: false,
		"sScrollY"	: $('#dvBdPCont').height()-$('#dvBdPBar').height()-50, 
		"sScrollX"	: "95%", 
		"sScrollXInner": "95%", 
		"order": []
	});
}

function limpiar(){
	$('.lbBodDesp').text('');
	$('#table_grid_datos tbody tr').remove();
	$('#rdPorDesp').prop('checked',true);
}

//evento evtRadioCheck
function evtRadioCheck(e){
	var cdBodDesp = $('#hdBodSal').val();
	if(cdBodDesp!=''){
		retTablaPedidosPorDespachar();
	}
}

//Establece el codigo de bodega que despacha
function setBod(cdBod,dsBod){
	$('.lbBodDesp').text(dsBod.substring(0,19));
	$('#hdBodSal').val(cdBod);
	retTablaPedidosPorDespachar();
}


function retTablaPedidosPorDespachar(){

	var url1 		= $('#txt_urlGe').val();
	var datos 		= "";
	var estado 		= (($('#rdPorDesp').is(':checked'))?'P':'D');

	$('.contDatos').html('<button class="btn btn-lg" style="background-color:#428bca;color:white;">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	datos = datos+"c_bs="  + $('#hdBodSal').val();
	datos = datos+"&c_es=" + estado;
	datos = datos+"&c_zb=" + '0';
	datos = datos+"&c_im=" + '0';
	datos = datos+"&c_pd=" + '';
	$.ajax({
		type : 'POST',
		url  : url1+"inventario/DespacharMercaderia/retTablaPedidosPorDesp", 
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				$('.contDatos').html('');
				var data = eval(returnData.trim().substr(3));
				var row = ' <table id="table_grid_datos" class="table table-bordered text-size-0-2 no-padding no-margin text-bold">';
				row = row+ '<thead>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Zona#</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Picking#</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Nº Ped</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Solicita</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Items</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Fecha_Pedido</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Pedido Por</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Motivo</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Observacion</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Usuario</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Impreso</th>'
				row = row+ '</thead>';
				row = row + '<tbody>';
				if (data){
					$.each(data,function(e,dato){
						row = row+'<tr>';
						row = row+'<td style="text-align:left">'	+dato.zona+'</td>';
						row = row+'<td style="text-align:left">'	+dato.picking+'</td>';
						row = row+'<td style="text-align:center">'
						+'<a href="#" data-toggle="tooltip" title="Ver Detalle" onclick="verDetallePed('
						+"'"+dato.co_noped+"',"
						+"'"+dato.co_bode_e+"',"
						+"'"+dato.id_picking+"'"
						+')">'+dato.co_noped
						+'</a>'
						+'</td>'
						row = row+'<td style="text-align:left">'	+dato.bodent+'</td>';
						row = row+'<td style="text-align:right">' 	+dato.items+'</td>';
						row = row+'<td style="text-align:center">' 	+dato.fe_pedido+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.no_pedido_por+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.no_motivo+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.ds_comentario+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.tx_nombre+'</td>';
						row = row+'<td style="text-align:right">' 	+dato.pedido_impreso+'</td>';
						//row = row+'<td style="text-align:center">' 	+dato.id_picking+'</td>';
						row = row+'</tr>';
					});	
				}
				row = row + '</tbody>';
				row = row + '<tfloat>';
				row = row + '</tfloat>';
				row = row + '</table>';
				$('.contDatos').append(row);
				$('#table_grid_datos').dataTable({  
					"bPaginate"	: false, 
					"bFilter"	: false, 
					"bInfo" 	: false,
					"sScrollY"	: '45vh', 
					"sScrollX"	: "95%", 
					"sScrollXInner": "95%", 
					"order": []	,
					"columnDefs": [ {
						"targets": [0,1,2,3,4,5,6,7,8,9,10],
						"orderable": false
					} ]			        
				});
			}else {
				$('.contDatos').html(returnData);
			}
		}
	});
}

function verDetallePed(codPed,bodEnt,pick){
	$('#hdMdlAct').val('DetPed');
	newModal('DetPed','60','60','form_detped');
	var url1 		= $('#txt_urlGe').val();
	var datos 		= "";

	$('.contTableDetPed').html('<button class="btn btn-lg" style="background-color:#428bca;color:white;">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	datos = datos+"c_cp=" + codPed;
	datos = datos+"&c_bs=" + $('#hdBodSal').val();
	datos = datos+"&c_be=" + bodEnt;
	datos = datos+"&c_pk=" + pick;

	$.ajax({
		type : 'POST',
		url  : url1+"inventario/DespacharMercaderia/retTablaDetallePedido", 
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				$('.contTableDetPed').html('');
				var data = eval(returnData.trim().substr(3));
				var row = ' <table id="table_grid_detalle" class="table table-bordered text-size-0-2 no-padding no-margin text-bold">';
				row = row+ '<thead>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Código</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Artículo</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Cant.Ped.</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Cant.Desp.</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Bulto</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Fábrica</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Cant.x.Picking</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Cant.Encont</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Tiendas Piden</th>'
				row = row+ '</thead>';
				row = row + '<tbody>';
				if (data){
					$.each(data,function(e,dato){
						row = row+'<tr>';
						row = row+'<td style="text-align:left">'	+dato.co_articulo+'</td>';
						row = row+'<td style="text-align:left">'	+dato.ds_articulo+'</td>';
						row = row+'<td style="text-align:left">'	+dato.cantpedido+'</td>';
						row = row+'<td style="text-align:right">' 	+dato.cantdespachado+'</td>';
						row = row+'<td style="text-align:center">' 	+dato.bulto_despachado+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.co_fabrica+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.cantidad_pedida+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.cantidad_pickeado+'</td>';
						row = row+'<td style="text-align:left"></td>';
						row = row+'</tr>';
					});	
				}
				row = row + '</tbody>';
				row = row + '<tfloat>';
				row = row + '</tfloat>';
				row = row + '</table>';
				$('.contTableDetPed').append(row);
				$('#table_grid_detalle').dataTable({  
					"bPaginate"	: false, 
					"bFilter"	: false, 
					"bInfo" 	: false,
					"sScrollY"	: '45vh', 
					"sScrollX"	: "95%", 
					"sScrollXInner": "95%", 
					"order": []	,
					"columnDefs": [ {
						"targets": [0,1,2,3,4,5,6,7,8],
						"orderable": false
					} ]			        
				});
			}else {
				$('.contTableDetPed').html(returnData);
			}
		}
	});
}


//Oculta el modal activo
function quitarModal(){
	var modals = $('#hdMdlAct').val().split('>');
	var lenModals = modals.length;
	var nameModal = modals[lenModals-1];
	modals.splice(modals.indexOf(lenModals),1);
	var resModals = modals.join('>');

	$('#hdMdlAct').val(resModals);
	$('.contTableBusqueda').html('');
	$('#hdModTrn').val('');
	quitarmodalGeneral(nameModal, '');
}