var arrArt = [];
var headArt = '<thead>'
+'<th style="background-color:#428bca; color:white; font-size:1.2em;">Código</th>'
+'<th style="background-color:#428bca; color:white; font-size:1.2em;"></th>'
+'<th style="background-color:#428bca; color:white; font-size:1.2em;">Descripción</th>'
+'<th style="background-color:#428bca; color:white; font-size:1.2em;">Cant.</th>'
+'<th style="background-color:#428bca; color:white; font-size:1.2em;">PVP</th>'
+'<th style="background-color:#428bca; color:white; font-size:1.2em;">Subtotal</th>'
+'</thead>';
var headAmo = '<thead>'
+'<th style="background-color:#428bca; color:white; font-size:1.2em;">#Cuota</th>'
+'<th style="background-color:#428bca; color:white; font-size:1.2em;">Valor</th>'
+'</thead>';

//Document Ready
$(iniciar);
//FUNCION Inicial load del document
function iniciar(){
	$('.txCdAr').focus();
	$("input[type=text]").css("text-transform","uppercase");
	//****************************** 	EVENTOS  **********************************
	// KEY PRESS de todos los <input texts>
	$("input[type=text]").keyup(evtInput);
	// KEY DOWN Solo Numeros
	$(".txCant").keydown(soloNumeros);
	// Lost Focus de codigo de articulo
	$(".txCdAr").focusout(function() {
		//$( ".txCant" ).focus();
	});
	// CLICK Boton <Busqueda>
	$('.btMainBus').click(cargarBusqueda);
	$( ".btBusB" ).button().click(buscarArticulo);
	// CLICK Boton <Limpiar>
	$('.btMainLimp').on('click', function(e){
		confirmDialog("Desea Limpiar la pantalla?", function(){
			resetear();
		});
	});
	// CLICK Boton <Agregar>
	$( ".btMainAgr" ).button().click(agrArticulo);
	// CLICK Boton <Procesar>
	$( ".btProc" ).button().click(retTablaAmortiza);
}

// Resetea la Pantalla y elimina los grids
function resetear(){
	limpiar();
	arrArt = [];
	$('.contTableArtic').html('<table id="table_grid_articulos" class="table table-bordered text-size-0-5 text-bold">'+ headArt+'</table>');
	$('.contTableAmort').html('<table id="table_grid_cuotas"    class="table table-bordered text-size-0-5 text-bold">'+ headAmo+'</table>');
}

//Limpia los controles txt y labels
function limpiar(){
	$('input[type=text]').val('');
	$('#hdCodigo').val('');
	$('#hdMdlAct').val('');
	$("label[class*='lb']").text('');
	$('.txCdAr').focus();
}

//Evento que controla todos los KEYS PRESS de los input texts
function evtInput(e){
	var isTxBusBar = $(this).hasClass('txBusB');
	var isTxCdAr   = $(this).hasClass('txCdAr');
	var isTxCant   = $(this).hasClass('txCant');
	if(e.keyCode==13){
		if(isTxBusBar){
			buscarArticulo();
		}
		if(isTxCdAr){
			var codArt = $(this).val();
			retDetalleArt(codArt);
		}
		if(isTxCant){
			agrArticulo();
		}
	}
}

//Carga la pantalla inicial de los articulos similares, lanza el div modal busqueda.
function cargarBusqueda(){
	$('#hdMdlAct').val('Busqueda');
	$('.contTableBusqueda').html('');
	$('.txBusB').val('');
	newModal('Busqueda','50','60','form_busqueda');
	$('.txBusB').focus();
}

//Carga los articulos buscados en el modal de busqueda
function buscarArticulo(){
	
	var datos = "";
	var busq  = $('.txBusB').val();
	var url1 = $('#txt_urlGe').val();
	
	if(busq=="")	return;
	if(busq.replace(/%/,"")=="")	return;
	
	$('.contTableBusqueda').html('<button class="btn btn-lg bg-primary">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	if(busq.indexOf("%")>-1){
		busq = busq.replace(/%/,"||");
	}
	datos = datos+"c_bu="+busq; 
	$.ajax({
		type : 'POST',
		url  : url1+'inventario/CalcValorCuotasTcp/retTablaBusqueda',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				$('.contTableBusqueda').html('');
				var row = '<table id="table_busqueda" class="table table-bordered table-success text-size-0-5 no-padding no-margin text-bold">';
				row = row+'<thead>';
				row = row+'<th style="background-color:#428bca; color:white;">Código</th>';
				row = row+'<th style="background-color:#428bca; color:white;">Descripción</th>';
				row = row+'<th style="background-color:#428bca; color:white;">PVP+IVA</th>';
				row = row+'<th style="background-color:#428bca; color:white;">Fecha Oferta</th>';
				row = row+'<th style="background-color:#428bca; color:white;">%Desc.</th>';
				row = row+'<th style="background-color:#428bca; color:white;">PVP_Oferta</th>';
				row = row+'</thead>';
				row = row+'<tbody>';

				if (data){
					
					$.each(data,function(e,dato){
						row = row+'<tr>';
						row = row+'<td><a href="javascript:void(0)" onclick="quitarModal();retDetalleArt('+"'"+dato.codigo+"'"+')"><span class="text-size-0-5">'+dato.codigo+'</span></td>';
						row = row+'<td style="text-align:left">'+dato.descripcion+'</td>';
						row = row+'<td style="text-align:right">'+parseFloat(dato.pvpiva).toFixed(2)+'</td>';
						row = row+'<td style="text-align:center">'+dato.fe_ohasta+'</td>';
						row = row+'<td style="text-align:right">'+dato.descuento+'%'+'</td>';
						row = row+'<td style="text-align:right">'+parseFloat(dato.pvp_oferta_iva).toFixed(2)+'</td>';
						row = row+'</tr>';
					});

				}

				row = row+'</tbody>';
				row = row+'</table>';

				$('.contTableBusqueda').append(row);
				$('#table_busqueda').dataTable({
					"language": {
						"info": "_MAX_ Artículo(s) encontrado(s)"
					},
					"bPaginate"	: false, 
					"bFilter"	: false, 
					"bInfo" 	: true,
					"sScrollY"	: $('#dvBusCont').height()-$('#dvBusBar').height()-$('#dvBusSearch').height()-$('#dvBusFoot').height()-110, 
					"sScrollX"	: "95%", 
					"sScrollXInner": "95%", 
					"order": []
				} );
			}else{
				$('.contTableBusqueda').html(returnData);
			}
		}

	});
}

// Carga la informacion de un artículo en pantalla.
function retDetalleArt(codigo){
	var url = $('#txt_urlGe').val();
	var datos = "";
	if(codigo){
		datos = datos+"c_cd="+codigo;
		$.ajax({
			type : 'POST',
			url  : url+"inventario/CalcValorCuotasTcp/retConsArt", 
			data : datos,
			success : function (returnData) {
				if (returnData.trim().substr(0,2) === "OK"){
					var data = eval(returnData.trim().substr(3));
					if (data){
						$('#hdCodigo').val(codigo.toUpperCase());
						$.each(data,function(e,dato){
							$('.txCdAr').val(dato.co_articulo);
							$('.lbDsArt').text(dato.ds_articulo);
							if(dato.oferta_activa=="1"){
								$('.lbPvpIva').text("$ "+parseFloat(dato.pvp_oferta_iva).toFixed(2));
								$('.lbOfert').text("Precio Oferta");
							}else{
								$('.lbPvpIva').text("$ "+parseFloat(dato.pvpiva).toFixed(2));
								$('.lbOfert').text("Precio Normal");
							}
						});
						$('.txCant').focus();
					}else {
						errorGeneral("Sistema Intranet",returnData,'danger');
					}
				}else {
					limpiar();
					errorGeneral("Sistema Intranet",returnData,'danger');
				}
			}
		});
	}
}

//Cuando un artículo esta cargado y se ha ingreado una cantidad, agraga este artículo al grid de totales.
function agrArticulo(){
	var codArt = $('#hdCodigo').val();
	var desArt = $('.lbDsArt').text();
	var canArt = parseInt($('.txCant').val());
	var pvpArt = $('.lbPvpIva').text().replace('$','');
	var agrRow = true;


	if(codArt==""){
		errorGeneral("Sistema Intranet","Debe ingresar un artículo","danger");
		$('.txCdAr').focus();
		return;
	}

	if(canArt<=0){
		errorGeneral("Sistema Intranet","Debe ingresar una cantidad válida","danger");
		$('.txCant').focus();
		return;
	}

	$.each(arrArt,function(e,dato){
		if(dato.codigo == codArt){
			agrRow=false;
			dato.cantidad = (parseInt(dato.cantidad) + parseInt(canArt));
			dato.subtotal = parseFloat(parseInt(dato.cantidad)*parseFloat(pvpArt)).toFixed(2);
		}
	});

	if(agrRow){
		arrArt.push({
			codigo 			: codArt,
			descripcion		: desArt,
			cantidad		: canArt,
			pvp				: pvpArt,
			subtotal 		: parseFloat(parseInt(canArt)*parseFloat(pvpArt)).toFixed(2)
		});
	}
	actualizaTablaArt();
}

function quitarArticulo(codigo){
	var elem = -1; 
	$.each(arrArt,function(e,dato){
		if(dato.codigo == codigo){
			elem = arrArt.indexOf(dato);
		}
	});
	if(elem>=0){
		confirmDialog("Desea eliminar el producto "+arrArt[elem]["codigo"]+"-"+arrArt[elem]["descripcion"]+" ?", function(e){
			arrArt.splice(elem,1);	
			actualizaTablaArt();
		});
	}
	
}


function actualizaTablaArt(){
	var row = "";
	var totItems = 0;
	var totFact  = 0;

	if(arrArt.length==0){
		resetear();
		return;
	}

	row = row + '<table id="table_grid_articulos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
	row = row + headArt;
	row = row + '<tbody>';

	$.each(arrArt,function(e,dato){
		row = row+'<tr>';
		row = row+'<td style="text-align:center;">'+dato.codigo+'</td>';
		row = row+'<td style="text-align:center;">'
		+'<a href="javascript:void(0)" onclick="quitarArticulo('+"'"+dato.codigo+"'"+')"><i class="fa fa-trash-o"></i></a>'+'</td>';
		row = row+'<td style="text-align:left;">'+dato.descripcion+'</td>';
		row = row+'<td style="text-align:right;">'+dato.cantidad+'</td>';
		row = row+'<td style="text-align:right;">'+dato.pvp+'</td>';
		row = row+'<td style="text-align:right;">'+dato.subtotal+'</td>';
		row = row+'</tr>';
		totItems = totItems + parseInt(dato.cantidad);
		totFact  = totFact  + parseFloat(dato.subtotal);
	});
	row = row + '</tbody>';
	row = row + '<tfloat>';
	row = row + '</tfloat>';
	row = row + '</table>';
	row = row + '<label class="form-control text-bold text-blue text-size-1-25" style="text-align:right;padding-right:45px;">TOTAL ---> $'+parseFloat(totFact).toFixed(2)+'</label>';
	$('.contTableArtic').html(row);
	$('#table_grid_articulos').dataTable({   
		"language": {
			"info": "Total _MAX_ artículo(s), "+totItems+" item(s)"
		},      
		"bPaginate" : false,
		"bFilter"   : false,          
		"bInfo"     : true,         
		"sScrollY"  : 200,
		"sScrollX"  : "95%",          
		"sScrollXInner": "95%",          
		"order": []
	} );     
	limpiar(); 
	$('.txCap').val(parseFloat(totFact).toFixed(2));
	if(arrArt.length>0){
		retTablaAmortiza();
	}
}

// Carga la informacion de la tabla de amortización.
function retTablaAmortiza(){
	var url   = $('#txt_urlGe').val();
	var datos = "";
	var monto = parseFloat($('.txCap').val());
	if(isNaN(monto)){
		return;
	}
	if(monto<=0){
		return;
	}
	datos = datos+"c_mt="+monto;
	$.ajax({
		type : 'POST',
		url  : url+"inventario/CalcValorCuotasTcp/retTablaAmortiza", 
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				$('.contTableAmort').html('');
				var row = '<table id="table_grid_cuotas" class="table table-bordered table-success text-size-0-5 no-padding no-margin text-bold">';
				row = row+ headAmo;
				row = row+ '</thead>';
				if (data){
					$.each(data,function(e,dato){
						row = row+'<tr>';
						row = row+'<td style="text-align:left">'+dato.cuota+'</td>';
						row = row+'<td style="text-align:right">'+parseFloat(dato.valor).toFixed(2)+'</td>';
						row = row+'</tr>';
					});
					row = row+'</tbody>';
					row = row+'</table>';

					$('.contTableAmort').append(row);
					$('#table_grid_cuotas').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: 200, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []
					} );
				}else {
					errorGeneral("Sistema Intranet",returnData,'danger');
				}
			}else {
				errorGeneral("Sistema Intranet",returnData,'danger');
			}
		}
	});
}

//Permite filtrar solo numeros en las cajas de texto
function soloNumeros(e) {
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
		(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
		(e.keyCode >= 35 && e.keyCode <= 40)) 
	{
		return;
	}
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
}

//Oculta el modal activo
function quitarModal(){
	var nameModal = $('#hdMdlAct').val();
	$('#hdMdlAct').val('');
	$('.contTableBusqueda').html('');
	quitarmodalGeneral(nameModal, '');
}

//Funcion que permite controlar los confirm dialog.
function confirmDialog(message, onConfirm){
	var fClose = function(){
		modal.modal("hide");
		$("#confirmOk").off('click', onConfirm);
	};
	$(document).keyup(function (e) {
		if (e.which === 27)
		{
			modal.modal("hide");
			$("#confirmOk").off('click', onConfirm);
		}
	});

	var modal = $("#confirmModal");
	modal.modal("show");
	$("#confirmMessage").empty().append(message);
	$("#confirmOk").one('click', onConfirm);
	$("#confirmOk").one('click', fClose);
	$("#confirmClose").one('click', fClose);
	$("#confirmCancel").one("click", fClose);
}


// Esto no lo voy a usar pero ahi queda el ejemplo de manejo de Tables
// $("#table_grid_articulos tbody tr").each(function (index){
// 	var duplicado = false;
// 	var cantAct = 0;
// 	var pvpAct  = 0;
// 	$(this).children("td").each(function (index2) 
// 	{
// 		if(index2==0){	
// 			if($(this).text()==codArt)
// 				duplicado = true;
// 			else
// 				duplicado = false;
// 		}
// 		if(index2==2)	cantAct = parseInt($(this).text());
// 		if(index2==3)	pvpAct  = parseFloat($(this).text());
// 		if(duplicado && index2==2){
// 			$(this).text((cantAct+parseInt(canArt)));
// 			cantAct = parseInt($(this).text());
// 			agrRow = false;
// 		}
// 		if(index2==4)	$(this).text(parseFloat(cantAct*pvpAct).toFixed(2));
// 	});

// });