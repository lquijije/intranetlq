$(function () {
    getArtCourier();
});

function getArtCourier() {
    if (va_sess()) {
//        var table_ = $('#tabCourier').dataTable({
//            "sAjaxSource": "co_3003/artCou",
//            "bDeferRender": true
//        });
        dataLoading();
        $.post('co_3003/artCou',
                {c_b: 1},
                function (b) {
                    var c = eval('(' + b + ')');
                    if (c.co_err == 0) {
                        if (!$.isEmptyObject(c.data)) {
                            $('#tab_pa_1').html('<table id="tabCourier" class="table table-bordered TFtable"></table><div class="clearfix"></div>');
                            var table_ = $('#tabCourier').DataTable({
                                data: c.data.data,
                                columns: c.data.columns,
                                aoColumnDefs: [{bSortable: false, aTargets: [0]}],
                                "bDeferRender": true
                            });
                            $('#ck_art').toggle(
                                    function () {
                                        table_.$("input[name='option_art[]']").each(function () {
                                            $(this).attr('checked', true);
                                        });
                                    }, function () {
                                table_.$("input[name='option_art[]']").each(function () {
                                    $(this).attr('checked', false);
                                });
                            }
                            );
                            $('#tab_pa_1').append(c.data.btn);
                        } else {
                            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                        }
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}

function sav_art() {

    if (va_sess()) {

        var oTable = $('#tabCourier').dataTable();
        var art = [];

        $("input[name='option_art[]']:checked", oTable.fnGetNodes()).each(function (index, elem) {
            var itemR = {};
            itemR["co_a"] = $(this).val();
            itemR["un_m"] = $(this).closest('tr').find('select.cmb_lo_').val();
            itemR["la_a"] = $(this).closest('tr').find('input.larg_').val();
            itemR["an_a"] = $(this).closest('tr').find('input.anch_').val();
            itemR["al_a"] = $(this).closest('tr').find('input.alto_').val();
            itemR["pe_a"] = $(this).closest('tr').find('input.peso_').val();
            itemR["un_p"] = $(this).closest('tr').find('select.cmb_pe_').val();
            art.push(itemR);
        });

        if (!$.isEmptyObject(art)) {
            var r = confirm("Desea procesar los siguientes productos.");
            if (r == true) {
                //dataLoading();
                $.post('co_3003/proArts',
                        {s_s: JSON.stringify(art)},
                        function (b) {
                            var c = eval('(' + b + ')');
                            if (parseInt(c.co_err) == 0) {
                                setTimeout(function () {
                                    window.location = window.location.href;
                                }, 1000);
                                errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_5, 'success');
                            } else {
                                errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                            }
                            removedataLoading();
                        }).fail(function (p) {
                    removedataLoading();
                    errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });
            }
        } else {
            errorGeneral(conf_hd.tx.t_1, 'Seleccione uno o m&aacute;s productos a procesar.', 'danger');
        }
    } else {
        redirectIntranet();
    }
}