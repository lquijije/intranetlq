var arrayPedidos = [];
//Document Ready
$(iniciar);
// Inicio del documento
function iniciar(){
	$(document).keyup(function (event) {
        //Tecla Escape
        if (event.which === 27)
        {
        	var nameModal = $('#hdMdlAct').val();
        	if(nameModal!="")        		quitarModal();
        }
    });
    agrBodDesp();
	//Cargo los tooltips
	$('[data-toggle="tooltip"]').tooltip();
    //Click check Salida / Llegada de Camión
    $('input[type=radio]').on('change',evtRadioCheck);
    //Click check Salida / Llegada de Camión
    $('input[type=radio]').on('click',function(e){
    	var idRadAct = $('#hdRadAct').val();
    	if( idRadAct!= $(this).attr('id')){
    		var hayPedido = (($('#hdCodPed').val()!="")?true:false);
    		if(hayPedido){
    			e.preventDefault();
    			confirmDialog("Se limpiará la pantalla, desea continuar?", function(e){
    				limpiar();
    				var radAct = $('#hdRadAct').val();
    				var radNew = ((radAct=='chkBySal')?'#chkByLle':'#chkBySal')
    				$(radNew).prop('checked',true);
    				$(radNew).trigger('change');
    			});
    		}
    	}
    });
    // KEY PRESS Buscar Transportista
    $('.txTranB').keyup(function (event) {
    	if (event.which === 13){
    		retTablaTransportistas();
    	}
    });
    // CLICK Boton Agregar Pedido
    $('.btToolAgrPed').click(agrPedido);
	// CLICK Boton <Buscar Bodega despacho>
	$( ".btBDesp" ).button().click(agrBodDesp);
	// CLICK Boton <Buscar Pedido Pendiente>
	$( ".btNPed" ).button().click(agrPedidoPendiente);
	// CLICK BOTON Limpiar Pantalla
	$('.btToolLimp').button().click(function(){
		confirmDialog("Desea limpiar la pantalla ?", function(e){
			limpiar();
		});
	});
	// CLICK Boton Agregar Transportista
	$('.btTrBus').button().click(agrTransport);
	// CLICK Boton Buscar Transportista
	$('.btTranB').button().click(retTablaTransportistas);
	// CLICK Boton Nuevo Transportista
	$('.btTranN').button().click(nuevTran);
	// CLICK Boton Grabar Transportista
	$('.btSav').button().click(savTransp);
	$('.btSave').button().click(savTransaccion);
}

//evento evtRadioCheck
function evtRadioCheck(e){
	var idRadio = $(this).attr("id");
	$('#hdRadAct').val(idRadio);
	//e.preventDefault();
	//Click Opcion Salida
	if(idRadio=='chkBySal'){
		$('.salCamCap').text('Salida Cam:');
	}
	//Click Opcion Llegada
	if(idRadio=='chkByLle'){
		$('.salCamCap').text('Llegada Cam:');
	}	

}
//Limpia la pantalla
function limpiar(){
	$("label[class*='lb']").text('');
	$('.btBDesp').removeAttr('disabled');
	$('#hdMdlAct').val('');
	$('#hdCodBod').val('');
	$('#hdCodPed').val('');
	$('#hdModTrn').val('');
	$('#totItems').html('');
	$('#hdStockB').val('');
	$('#hdTipBod').val('');
	$('#hdBodLle').val('');
	//$('#chkBySal').prop('checked',true);
	$('.lbFchSalCam').text($('#hdFechSL').val());
	$('#table_grid_datos tbody').remove();
	$('#table_grid_pedido tbody').remove();
	arrayPedidos = [];
}

//Agrega bodega de despacho o bodega que recibe
function agrBodDesp(){
	$('#hdMdlAct').val('BodPed');
	newModal('BodPed','25','50','form_bodped');
	$('#table_pedido').dataTable().fnDestroy();
	$('#table_pedido').dataTable({
		"bPaginate"	: false, 
		"bFilter"	: false, 
		"bInfo" 	: false,
		"sScrollY"	: $('#dvBdPCont').height()-$('#dvBdPBar').height()-50, 
		"sScrollX"	: "95%", 
		"sScrollXInner": "95%", 
		"order": []
	});
}

//Una vez elegida la bodega aqui se cargan los pedidos pendientes con  fecha salida = null
function agrPedidoPendiente(){
	var codBod = $('#hdCodBod').val();
	if(codBod==''){
		errorGeneral("Sistema Intranet","Ingrese Primero la bodega que despacha","danger");
		agrBodDesp();
		return;
	}
	$('#hdMdlAct').val('PedPen');
	newModal('PedPen','40','50','form_pedpen');
	retPedidoPendiente();
}

//Carga los pedidos pendientes en el modal de pedidos
function retPedidoPendiente(){
	var codBod 		= $('#hdCodBod').val();
	var url1 		= $('#txt_urlGe').val();
	var esSalida 	= $('#chkBySal').is(':checked');
	var datos 		= "";
	
	$('.contTablePedPen').html('<button class="btn btn-lg" style="background-color:#428bca;color:white;">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	datos = datos+"c_cb=" +codBod;
	datos = datos+"&c_sl="+((esSalida)?'1':'0');
	datos = datos+"&c_ch="+'0';// TODO Es chofer si tiene permiso especial tipo H
	datos = datos+"&c_cp=";
	$.ajax({
		type : 'POST',
		url  : url1+"inventario/MarcaSalidaLlegadaCamiones/retTablaPedidosPendientes", 
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				$('.contTablePedPen').html('');
				var data = eval(returnData.trim().substr(3));
				var row = '<table id="table_ped_pend" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
				row = row+ '<thead>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Código</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Solicita</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Fecha Ped.</th>'
				row = row+ '</thead>';
				row = row + '<tbody>';
				if (data){
					$.each(data,function(e,dato){
						var no_ped = dato.co_noped+((dato.id_picking!='0')?'-'+dato.id_picking:'');

						row = row+'<tr>';
						row = row+'<td style="text-align:right"><a href="javascript:void(0)" onclick="quitarModal();busSalEnt('+"'"+dato.co_noped+"','"+dato.id_picking+"'"+')" style="text-decoration:underline;">'+no_ped+'</a></td>';
						row = row+'<td style="text-align:left">'	+dato.descripcion+'</td>';
						row = row+'<td style="text-align:center">' 	+dato.fe_pedido+'</td>';
						row = row+'</tr>';
					});	
				}
				row = row + '</tbody>';
				row = row + '<tfloat>';
				row = row + '</tfloat>';
				row = row + '</table>';
				$('.contTablePedPen').append(row);
				$('#table_ped_pend').dataTable({
					"bPaginate"	: false, 
					"bFilter"	: false, 
					"bInfo" 	: false,
					"sScrollY"	: $('#dvPedPenCont').height()-$('#dvPedPenBar').height()-$('#dvPedPenTit').height()-55, 
					"sScrollX"	: "95%", 
					"sScrollXInner": "95%", 
					"order": []				        
				});
			}else {
				$('.contTablePedPen').html(returnData);
			}
		}
	});
}

//Establece el codigo de bodega que despacha
function setBod(cdBod,dsBod){
	$('.lbBodDesp').text(dsBod.substring(0,19));
	$('#hdCodBod').val(cdBod);
	agrPedidoPendiente();
}

//Cuando se escoge un pedido pendiente se carga la infromación en la pantalla.
function busSalEnt(cdPed,idPick){
	$('#hdCodPed').val(cdPed);
	$('.lbNoPed').text(cdPed);
	var codBod 		= $('#hdCodBod').val();
	var url1 		= $('#txt_urlGe').val();
	var esSalida 	= $('#chkBySal').is(':checked');
	var datos 		= "";

	datos = datos+"&c_se=" + codBod;
	datos = datos+"&c_cp=" + cdPed;
	datos = datos+"&c_es=" + 'D';
	datos = datos+"&c_tp=" + ((esSalida)?'1':'2');
	datos = datos+"&c_pk=" + idPick;
	$.ajax({
		type : 'POST',
		url  : url1+"inventario/MarcaSalidaLlegadaCamiones/retTablaSalidaEntradaPed", 
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				$('.contDatos').html('');
				var data = eval(returnData.trim().substr(3));
				var row = '<table id="table_grid_datos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
				row = row+ '<thead>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Código</th>	'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Grupo</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Artículo</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Cant.Ped.</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Cant.Desp.</th>'
				row = row+ '</thead>';
				row = row + '<tbody>';
				if (data){
					var numItems = 0 ;
					$.each(data,function(e,dato){
						
						$('.lbPedPor').text(dato.no_pedido_por);
						$('.lbFchPed').text(dato.fe_pedido);
						$('.lbFchDesp').text(dato.fe_despachado);
						$('#hdBodLle').val(dato.co_bode_e);
						$('#hdStockB').val(dato.stock_basico);
						$('#hdTipBod').val(dato.tipo_bodega);

						numItems += 1;

						row = row+'<tr>';
						row = row+'<td style="text-align:center">'	+dato.co_articulo+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.ds_grupo+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.ds_articulo+'</td>';
						row = row+'<td style="text-align:right">' 	+dato.cantpedido+'</td>';
						row = row+'<td style="text-align:right">' 	+dato.cantdespachado+'</td>';
						row = row+'</tr>';
					});	
					$('.btBDesp').attr('disabled','disabled');
					$('#hdNumIte').val(numItems);
				}
				row = row + '</tbody>';
				row = row + '<tfloat>';
				row = row + '</tfloat>';
				row = row + '</table>';
				$('.contDatos').append(row);
				$('#table_grid_datos').dataTable({
					"bPaginate"	: false, 
					"bFilter"	: false, 
					"bInfo" 	: false,
					"sScrollY"	: 100,
					"sScrollX"	: "95%", 
					"sScrollXInner": "95%", 
					"order": []				        
				});
			}else {
				errorGeneral("Sistema Intranet",returnData,'danger');
			}
		}
	});
}

// Agrega un pedido seleccionado para grabar
function agrPedido(){
	var pedDup   = false;
	var pedBodEq = false;
	var noPed 	 = $('#hdCodPed').val();
	var bdEnt 	 = $('#hdBodLle').val();

	$.each(arrayPedidos,function(e,item){
		// Si pedido ya fue ingresado
		if(noPed == item.no_pedido){
			pedDup = true;
		}
		// Si la bodega de llegada es diff a la bodega de salisa de los pedidos
		if(bdEnt != item.co_bode_e){
			pedBodEq = true;
		}
	});

	if(pedDup){
		errorGeneral('Sistema Intranet','El pedido '+noPed+' ya ha sido ingresado','danger');
		return false;
	}
	if(pedBodEq){
		errorGeneral('Sistema Intranet','Esta tratando de consolidar pedidos','danger');
		return false;
	}

	if(noPed!=''){
		arrayPedidos.push({
			no_pedido		: $('#hdCodPed').val(),
			co_bode_s		: $('#hdCodBod').val(),
			co_bode_e		: $('#hdBodLle').val(),
			ds_bode_s		: $('.lbBodDesp').text(),
			pedid_por		: $('.lbPedPor').text(),
			num_items 		: $('#hdNumIte').val(),
			num_llega		: '0',
			salida			: '',
			stock_bas		: $('#hdStockB').val(),
			tipBodLle		: $('#hdTipBod').val()
		});	
		setPedidosFromArray();
		resetAgrPedido();
	}
}

function resetAgrPedido(){
	$('#hdCodPed').val('');
	$('.lbNoPed').text('');
	$('.lbFchDesp').text('');
	$('.lbFchPed').text('');
	$('.lbPedPor').text('');
	$('#lbPedPor').text('');
	$('#table_grid_datos tbody tr').remove();
}

function setPedidosFromArray(){
	var hayPedidos = ((arrayPedidos.length>0)?true:false);
	if(hayPedidos){
		$('.contPedidos').html('');
		var row = '<table id="table_grid_pedido" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
		row = row+ '<thead>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Nº Pedido</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Cod.Bod.</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Nombre Bodega</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Pedido Por</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Items</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;">Nº Llegada</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Cant.Desp.</th>'
		row = row+ '</thead>';
		row = row + '<tbody>';
		$.each(arrayPedidos,function(e,dato){
			row = row+'<tr>';
			row = row+'<td style="text-align:center">'	+dato.no_pedido+'</td>';
			row = row+'<td style="text-align:right">' 	+dato.co_bode_s+'</td>';
			row = row+'<td style="text-align:left">' 	+dato.ds_bode_s+'</td>';
			row = row+'<td style="text-align:left">' 	+dato.pedid_por+'</td>';
			row = row+'<td style="text-align:right">' 	+dato.num_items+'</td>';
			row = row+'<td style="text-align:right">' 	+dato.num_llega+'</td>';
			row = row+'<td style="text-align:right">' 	+dato.salida+'</td>';
			row = row+'</tr>';
		});	
		row = row + '</tbody>';
		row = row + '<tfloat>';
		row = row + '</tfloat>';
		row = row + '</table>';
		$('.contPedidos').append(row);
		$('#table_grid_pedido').dataTable({
			"bPaginate"	: false, 
			"bFilter"	: false, 
			"bInfo" 	: false,
			"sScrollY"	: 100,
			"sScrollX"	: "95%", 
			"sScrollXInner": "95%", 
			"order": [],
			"footerCallback": function ( row, data, start, end, display ) {
				var api = this.api(), data;
				var intVal = function ( i ) {
					return typeof i === 'string' ?
					i.replace(/[\$,]/g, '')*1 :
					typeof i === 'number' ?
					i : 0;
				};
				total = api
				.column( 4 )
				.data()
				.reduce( function (a, b) {
					return intVal(a) + intVal(b);
				}, 0 );
				$('#totItems').html(' Total Items: '+ total);
			}				        
		});
	}	
}
//Agregar Transportista
function agrTransport(){
	$('#hdMdlAct').val('Transp');
	setTran('','','','');
	newModal('Transp','60','50','form_transp');
	$('.contTableTran').html('');
	$('.txTranB').val('');
	$('.txTranB').focus();
}

//Carga los transportistas
function retTablaTransportistas(){
	var url1 		= $('#txt_urlGe').val();
	var datos 		= "";
	var busq		= $('.txTranB').val();
	if(busq.indexOf("%")>-1){
		busq = busq.replace(/%/,"||");
	}

	$('.contTableTran').html('<button class="btn btn-lg" style="background-color:#428bca;color:white;">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	datos = datos+"c_bus=" + busq;
	$.ajax({
		type : 'POST',
		url  : url1+"inventario/MarcaSalidaLlegadaCamiones/retTablaTransportistas", 
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				$('.contTableTran').html('');
				var data = eval(returnData.trim().substr(3));
				var row = '<table id="table_transp" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
				row = row+ '<thead>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Céd./RUC</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;"></th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;"></th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Nombres</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Dirección</th>'
				row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Placa</th>'
				row = row+ '</thead>';
				row = row + '<tbody>';
				if (data){
					$.each(data,function(e,dato){
						row = row+'<tr>';
						row = row+'<td style="text-align:center">'	+dato.id+'</td>';
						row = row+'<td style="text-align:center">'
						+'<a href="#" data-toggle="tooltip" title="Escoger" onclick="quitarModal();confTran('
						+"'"+dato.id+"',"
						+"'"+dato.nombres+"',"
						+"'"+dato.direccion+"',"
						+"'"+dato.placa+"'"
						+')"><i class="fa fa-check"></i>'
						+'</a>'
						+'</td>'
						row = row+'<td style="text-align:center">'
						+'<a href="#" data-toggle="tooltip" title="Editar" onclick="editTran('
						+"'"+dato.id+"',"
						+"'"+dato.nombres+"',"
						+"'"+dato.direccion+"',"
						+"'"+dato.placa+"'"
						+')"><i class="fa fa-pencil-square-o"></i>'
						+'</a>'
						+'</td>'
						row = row+'<td style="text-align:left">'	+dato.nombres+'</td>';
						row = row+'<td style="text-align:left">' 	+dato.direccion+'</td>';
						row = row+'<td style="text-align:center">' 	+dato.placa+'</td>';
						row = row+'</tr>';
					});	
				}
				row = row + '</tbody>';
				row = row + '<tfloat>';
				row = row + '</tfloat>';
				row = row + '</table>';
				$('.contTableTran').append(row);
				$('#table_transp').dataTable({
					"language": {
						"info": "Total _MAX_ registro(s)"
					},  
					"bPaginate"	: false, 
					"bFilter"	: false, 
					"bInfo" 	: true,
					"sScrollY"	: $('#dvTranCont').height()-$('#dvTranSearch').height()-$('#dvTranBar').height()-$('#dvTranTit').height()-95, 
					"sScrollX"	: "95%", 
					"sScrollXInner": "95%", 
					"order": []				        
				});
			}else {
				$('.contTableTran').html(returnData);
			}
		}
	});
}

// Click Nuevo Transportista
function nuevTran(){
	$('#hdModTrn').val('nuev');
	$('#hdMdlAct').val('Transp>EdtTrn');
	newModal('EdtTrn','40','25','form_edttrn');
	$('.btSav').html('<i class="fa fa-floppy-o"></i> Guardar');
	$('#spEdtTrnBar').html('<i class="fa fa-file-o"></i> Nuevo Transportista ');
	$('.btCan').attr('class','btn btn-default btCan');
	setTran('','','','');
	$('.txSvTrRuc').focus();
}

// Click confirmar transportista
function confTran(id,nom,dir,pla){
	$('#hdModTrn').val('conf');
	$('#hdMdlAct').val('EdtTrn');
	newModal('EdtTrn','40','25','form_edttrn');
	$('.btSav').html('<i class="fa fa-floppy-o"></i> Guardar y Continuar');
	$('#spEdtTrnBar').html('<i class="fa fa-check"></i> Confirme los datos del transporte ');
	$('.btCan').attr('class','hide btn btn-default btCan');
	setTran(id,nom,dir,pla);
	$('.txSvTrPla').focus();
}
//Establece los datos del transportista en la pantalla principal, o en la pantalla de confirmacion de datos.
function setTran(id,nom,dir,pla){
	if($('#hdModTrn').val()!=''){
		$('.txSvTrNom').val(nom);
		$('.txSvTrRuc').val(id);
		$('.txSvTrPla').val(pla);
		$('.txSvTrDir').val(dir);
	}else{
		$('.lbTrNom').text(nom);
		$('.lbTrRuc').text(id);
		$('.lbTrPla').text(pla);
		$('.lbTrDir').text(dir);
	}
}

//Cuando presiona el boton editar transportista
function editTran(id,nom,dir,pla){
	$('#hdModTrn').val('edit');
	$('#hdMdlAct').val('Transp>EdtTrn');
	newModal('EdtTrn','40','25','form_edttrn');
	$('.btSav').html('<i class="fa fa-floppy-o"></i> Guardar');
	$('#spEdtTrnBar').html('<i class="fa fa-pencil-square-o"></i> Editar datos del transportista ');
	$('.btCan').attr('class','btn btn-default btCan');
	setTran(id,nom,dir,pla);
	$('.txSvTrPla').focus();
}

//Cuando presiona el boton Guardar Datos del transportista
function savTransp(){
	var url_1 = $('#txt_urlGe').val();
	var accion = $('#txt_accion').val();
	var id = $('#id_user').val();
	var datos = '';
	var permisos = [];

	id    = $('.txSvTrRuc').val().trim().toUpperCase();
	nomb  = $('.txSvTrNom').val().trim().toUpperCase();
	plac  = $('.txSvTrPla').val().trim().toUpperCase();
	dire  = $('.txSvTrDir').val().trim().toUpperCase();

	if (nomb === ''){
		errorGeneral("Sistema Intranet",'El campo "Nombre:" esta vacio','danger'); 
		return false;
	}

	if (id === ''){
		errorGeneral("Sistema Intranet",'El campo "RUC:" esta vacio','danger'); 
		return false;
	}

	if (plac === ''){
		errorGeneral("Sistema Intranet",'El campo "Placa:" esta vacio','danger'); 
		return false;
	}

	if (dire === ''){
		errorGeneral("Sistema Intranet",'El campo "Dirección:" esta vacio','danger'); 
		return false;
	}

	datos = datos+"c_id="+id;
	datos = datos+"&c_no="+nomb;
	datos = datos+"&c_pl="+plac;
	datos = datos+"&c_di="+dire;
	
	$.ajax({
		type : 'POST',
		url : url_1+"inventario/MarcaSalidaLlegadaCamiones/guardarTransp", 
		data: datos,
		success : function (returnData) {
			var data =returnData.trim();
			if(data.trim()==="OK"){ 
				if($('#hdModTrn').val()=='conf'){
					$('#hdModTrn').val('');
					setTran(id,nomb,dire,plac);	
				}
				if($('#hdModTrn').val()=='edit'){
					setRegTableTransp(id,nomb,dire,plac);
				}
				if($('#hdModTrn').val()=='nuev'){
					addRegTableTransp(id,nomb,dire,plac);
				}
				errorGeneral("Sistema Intranet",'Transportista grabado correctamente','success'); 
				quitarModal();
			} else {
				errorGeneral("Sistema Intranet",data,'danger'); 
			}
		}
	});
}

//Cuando se modifican los datos del transportista aqui se actualiza el registro de la tabla de busqueda
function setRegTableTransp(id,nomb,dire,plac){
	var encontro = false;
	$("#table_transp tbody tr").each(function (ixRow){
		$(this).children("td").each(function (ixCol) 
		{
			if(ixCol==0){	
				if($(this).text()==id){
					encontro = true;
				}
			}	
			if(encontro){
				if(ixCol==1){
					$(this).html('<a href="#" data-toggle="tooltip" title="Escoger" onclick="quitarModal();confTran('
						+"'"+id+"',"
						+"'"+nomb+"',"
						+"'"+dire+"',"
						+"'"+plac+"'"
						+')"><i class="fa fa-check"></i>'
						+'</a>');
				}
				if(ixCol==2){
					$(this).html('<a href="#" data-toggle="tooltip" title="Editar" onclick="editTran('
						+"'"+id+"',"
						+"'"+nomb+"',"
						+"'"+dire+"',"
						+"'"+plac+"'"
						+')"><i class="fa fa-pencil-square-o"></i>'
						+'</a>');
				}
				if(ixCol==3)
					$(this).text(nomb);
				if(ixCol==4)
					$(this).text(dire);
				if(ixCol==5)
					$(this).text(plac);
			}
		});
		if(encontro)
			return false;
	});
}

//Cuando se modifican los datos del transportista aqui se actualiza el registro de la tabla de busqueda
function addRegTableTransp(id,nomb,dire,plac){
	var table = $('#table_transp');
	var row = '';
	if(!table.html()){
		row = '<table id="table_transp" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
		row = row+ '<thead>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Céd./RUC</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;"></th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;"></th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Nombres</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Dirección</th>'
		row = row+ '<th style="background-color:#428bca; color:white; font-size:1.2em;">Placa</th>'
		row = row+ '</thead>';
		row = row + '<tbody>';
		row = row + '</tbody>';
		row = row + '</table>';
		$('.contTableTran').append(row);
	}else{
		$('#table_transp').dataTable().fnDestroy();
	}
	row ='';
	row = '<tr>';
	row = row+'<td style="text-align:center">'	+id+'</td>';
	row = row+'<td style="text-align:center">'
	+'<a href="#" data-toggle="tooltip" title="Escoger" onclick="quitarModal();confTran('
	+"'"+id+"',"
	+"'"+nomb+"',"
	+"'"+dire+"',"
	+"'"+plac+"'"
	+')"><i class="fa fa-check"></i>'
	+'</a>'
	+'</td>'
	row = row+'<td style="text-align:center">'
	+'<a href="#" data-toggle="tooltip" title="Editar" onclick="editTran('
	+"'"+id+"',"
	+"'"+nomb+"',"
	+"'"+dire+"',"
	+"'"+plac+"'"
	+')"><i class="fa fa-pencil-square-o"></i>'
	+'</a>'
	+'</td>'
	row = row+'<td style="text-align:left">'	+nomb+'</td>';
	row = row+'<td style="text-align:left">' 	+dire+'</td>';
	row = row+'<td style="text-align:center">' 	+plac+'</td>';
	row = row+'</tr>';
	$('#table_transp tbody tr').remove();
	$('#table_transp tbody').append(row);
	$('#table_transp').dataTable({
		"language": {
			"info": "Total _MAX_ registro(s)"
		},  
		"bPaginate"	: false, 
		"bFilter"	: false, 
		"bInfo" 	: true,
		"sScrollY"	: $('#dvTranCont').height()-$('#dvTranSearch').height()-$('#dvTranBar').height()-$('#dvTranTit').height()-95, 
		"sScrollX"	: "95%", 
		"sScrollXInner": "95%", 
		"order": []				        
	});
}

// Guardar la transaccion de salida y llegada de camiones
function savTransaccion(){

	confAgregaPedido();
}

function confAgregaPedido(){
	var pedActual  = $('.lbNoPed').text();
	if(pedActual!=''){
		confirmDialog('Desea ingresar el pedido actual '+ pedActual +' ?', function(e){
			agrPedido();
			if(!validacionGeneral()){
				return;
			}
			return;
		});
	}
	else{
		if(!validacionGeneral()){
			return;
		}
	}
}

function validacionGeneral(){
	
	var hayPedidos = ((arrayPedidos.length>0)?true:false);
	var hayTranspo = (($('.lbTrRuc').text()!='')?true:false);

	//Validación de ingreso de pedido
	if(!hayPedidos){
		var codBod 	= $('#hdCodBod').val();
		errorGeneral('Sistema Intranet','Debe agregar pedidos','danger');
		if(codBod==''){
			agrBodDesp();
			return false;
		}

		if(pedActual==''){
			agrPedidoPendiente();
			return false;
		}
		return false;
	}

	//Validacion de existencia de transportista
	if(!hayTranspo){
		errorGeneral('Sistema Intranet','Debe Ingresar un transportista','danger');
		agrTransport();
		return false;
	}
	validacionPrevia();
	return true;
}

function validacionPrevia(){
	// var retorno = false;

	$.each(arrayPedidos,function(e,item){
		var codPed = item['no_pedido'].split('-')[0];
		var bodLle = item['co_bode_e'];
		var bodSal = item['co_bode_s'];
		var picking = item['no_pedido'].split('-')[1];
		var url_1 = $('#txt_urlGe').val();
		var datos = "";
		if(!picking){
			picking = '0';
		}
		datos = datos+"c_pd="+codPed+"&c_bl="+bodLle+"&c_bs="+bodSal+"&c_pk="+picking;

		$.ajax({
			type : 'POST',
			url : url_1+"inventario/MarcaSalidaLlegadaCamiones/retPedValidaLlegadaPrevia", 
			data: datos,
			success : function (returnData) {
				if(returnData.trim()!=''){
					var msg = returnData.trim().split('-');
					if(msg[0]=='0'){
						salidaTrx1Blomba();
					}
					else{
						errorGeneral('Sistema Intranet',msg[1],'danger');
						return;	
					}
				}else{
					return;
				}			
			}
		});
	});
}

function salidaTrx1Blomba(){
	var esSalida 	= $('#chkBySal').is(':checked');
	var blomba_salida = '';
	var url_1 = $('#txt_urlGe').val();
	var datos = '';
	var tipBod = arrayPedidos[0]['tipBodLle'];
	if(esSalida){
		if(tipBod!='B'){
			//Verifica blomba
			datos = datos+"c_pe="+arrayPedidos[0]['no_pedido']+"&c_be="+arrayPedidos[0]['co_bode_e']+"&c_bs="+arrayPedidos[0]['co_bode_s'];
			$.ajax({
				type : 'POST',
				url : url_1+"inventario/MarcaSalidaLlegadaCamiones/retBlomba", 
				data: datos,
				success : function (returnData) {
					var data = returnData.trim();
					blomba_salida = returnData.trim();
				}
			});
		}
		inputDialog('Ingrese el # de blomba: ', function(e){
			var numBlomba = $('#inputText').val().trim();
			if(numBlomba!=''){
				//Seguir con el proceso de grabar
				$('#hdBlomba').val(numBlomba);
				grabarSalida();
				return;	
			}else{
				errorGeneral('Sistema Intranet','No ha ingresado # de blomba, no se puede continuar','danger');
			}
		});
		return;
	}
}

function grabarSalida(){
	var numPed = arrayPedidos.length;
	var count = 0;
	$.each(arrayPedidos,function(e,item){
		var codPed = item['no_pedido'].split('-')[0];	
		var bodLle = item['co_bode_e'];
		var bodSal = item['co_bode_s'];
		var picking = item['no_pedido'].split('-')[1];
		var esSalid = ($('#chkBySal').is(':checked'))?'1':'0';
		var codBlom	= $('#hdBlomba').val();
		var autoriz = '21182'; // Autorización de salida de camión
		var retorno = false;
		var url_1 = $('#txt_urlGe').val();
		var datos = "";
		if(!picking){
			picking = '0';
		}
		datos = datos+'c_ch='+autoriz+'&c_pd='+codPed+'&c_bl='+bodLle+'&c_bs='+bodSal+'&c_pk='+picking+'&c_sl='+esSalid+'&c_bm='+codBlom;
		$.ajax({
			type : 'POST',
			url : url_1+"inventario/MarcaSalidaLlegadaCamiones/savPedidoLlegada", 
			data: datos,
			success : function (returnData) {
				if(returnData.trim()!=''){
					if(returnData.trim().substr(0,2) === "OK"){
						count++;
						if(numPed==count){
							errorGeneral('Sistema Intranet','Salida de camion OK','success');
							limpiar();
							return true;	
						}						
					}else{
						errorGeneral('Sistema Intranet',returnData.trim(),'danger');
						return false;
					}	
				}else{
					return false;
				}			
			}
		});
	});	
}

//Oculta el modal activo
function quitarModal(){
	var modals = $('#hdMdlAct').val().split('>');
	var lenModals = modals.length;
	var nameModal = modals[lenModals-1];
	modals.splice(modals.indexOf(lenModals),1);
	var resModals = modals.join('>');

	$('#hdMdlAct').val(resModals);
	$('.contTableBusqueda').html('');
	$('#hdModTrn').val('');
	quitarmodalGeneral(nameModal, '');
}

//Funcion que permite controlar los confirm dialog.
function confirmDialog(message, onConfirm){
	var fClose = function(){
		modal.modal("hide");
		$("#confirmOk").off('click', onConfirm);
	};
	$(document).keyup(function (e) {
		if (e.which === 27)
		{
			modal.modal("hide");
			$("#confirmOk").off('click', onConfirm);
		}
	});

	var modal = $("#confirmModal");
	modal.modal("show");
	$("#confirmMessage").empty().append(message);
	$("#confirmOk").one('click', onConfirm);
	$("#confirmOk").one('click', fClose);
	$("#confirmClose").one('click', fClose);
	$("#confirmCancel").one("click", fClose);
}

function inputDialog(message, onConfirm){
	var fClose = function(){
		modal.modal("hide");
		$("#inputOk").off('click', onConfirm);
	};
	$(document).keyup(function (e) {
		if (e.which === 27)
		{
			modal.modal("hide");
			$("#inputOk").off('click', onConfirm);
		}
	});

	var modal = $("#inputModal");
	modal.modal("show");
	$("#inputLabel").text(message);
	$("#inputText").val('');
	$("#inputText").focus();
	$("#inputOk").one('click', onConfirm);
	$("#inputOk").one('click', fClose);
	$("#inputClose").one('click', fClose);
	$("#inputCancel").one("click", fClose);
}