//Document Ready
$(iniciar);
// variables GLOBALES
var grupo = -1;
var linea = -1;
var sublinea = -1;
var categoria = -1;
var subcategoria = -1;
var defaulec = -1;
var tableRotPl ="";
var tableRotPy ="";
var heighDvRot = 0.85;
var heighTbRot = 0.75;
var defHtmlTblRot = '<table id="table_rotacion_py" class="table no-padding no-margin text-bold">'+
'<thead>'+
'<th style="background-color:#001f3f; color:white; font-size:1em;">Descripción</th>'+
'<th style="background-color:#001f3f; color:white; font-size:1em;">Saldo</th>'+
'<th style="background-color:#001f3f; color:white; font-size:1em;">Rot360</th>'+
'<th style="background-color:#001f3f; color:white; font-size:1em;">Rot90</th>'+
'</thead>'+
'</table>';

//FUNCION Inicial load del document
function iniciar(){
	//INICIALIZAR LA PANTALLA PRINCIPAL
	limpiar();
	$(".txCodArt").focus();
	//Todas los inputs text a Mayuscula
	$("input[type=text]").css("text-transform","uppercase");
	//Seteo el select Grupo por default
	var sel = $('.cbGru');
	consultaEstructuraCom(sel,grupo);
	//Cargo los tooltips
	$('[data-toggle="tooltip"]').tooltip();

	//****************************** 	EVENTOS  **********************************
	// KEY PRESS de todos los <input texts>
	$("input[type=text]").keyup(evtInput);
	// CHANGE de todos los <selects>
	$( "select" ).change(evtSelect);
	//Evitar que se oculte menu de consulta inventario
	$('#mnConInv').on("click.bs.dropdown", function (e) {
		e.stopPropagation();
	});
	//Click de los tabs
	$('[data-toggle="tab"]').on('shown.bs.tab', evtTabs);
	// KEY PRESS del documento completo para avenzar, retroceder y limpiar
	$(document).keyup(function (event) {
		//Tecla direccional Atras
		if (event.which === 37)				retAnterior();
		//Tecla direccional Adelante
        if (event.which === 39)	        	retAdelante();
        //Tecla Escape
        if (event.which === 27)
        {
        	var nameModal = $('#hdMdlAct').val();
        	if(nameModal!="")        		quitarModal();
        	else  				       		resetPantalla();
        }
    });
    // KEY DOWN txtFiltros Valor
    $(".txFil1a").keydown(soloNumeros);		$(".txFil3b").keydown(soloNumeros);
    $(".txFil1b").keydown(soloNumeros);		$(".txFil4a").keydown(soloNumeros);
    $(".txFil2a").keydown(soloNumeros);		$(".txFil4b").keydown(soloNumeros);
    $(".txFil2b").keydown(soloNumeros);		$(".txFil5a").keydown(soloNumeros);
    $(".txFil3a").keydown(soloNumeros);		$(".txFil5b").keydown(soloNumeros);    
	//BOTON ADELANTE
	$('.btAde').click( function(){
		retAdelante();
	});
	//BOTON ATRAS
	$('.btAnt').click( function(){
		retAnterior();
	});
	// RESIZE DOCUMENT
	$(window).on('resize', redimensionar);
	// CLICK Boton <AGREGAR FILTRO>
	$('.btFilAgr').click(cargarFiltro);
	// CLICK Boton <QUITAR FILTRO>
	$('.btFilQui').click(quitarFiltro);
	// CLICK ALL CHECKBOXS
	$('input[type=checkbox]').change(evtCheckBox);
	// CLICK Boton <Aplicar Filtros>
	$('.btAplFil').click(aplicarFiltros);
    // CLICK Boton <Similares>
    $('.btSim').click(cargarSimilares);
    $( ".btSimB" ).button().click(buscarSimilares);
    // CLICK Boton <Ficha Tecnica>
    $('.btFic').click(cargarFicha);
    // CLICK Boton <Ficha Tecnica>
    $('.btPer').click(cargarPercha);
    // CLICK Boton <Forecast>
    $('.btFor').click(cargarForecast);
    // CLICK Boton <Consulta de Factura>
    $('.btFac').click(cargarFactura);
    // CLICK Boton <Busqueda de factura>
    $('.btFacBus').click(retTablaFactura);
     //CLICK Boton copiar en el portapapeles del form Consulta Factura
    $('.btFacCli').click(function(){
    	copiarEnPortaPapeles('contTableFactura');
    });
    // CLICK Boton <Consulta de Pedidos a Bodega>
    $('.btPed').click(cargarPedido);
    // CLICK Boton <Busqueda de pedido>
    $('.btPedBus').click(retTablaPedido);
    //CLICK Boton copiar en el portapapeles del form Consulta Pedido
    $('.btPedCli').click(function(){
    	copiarEnPortaPapeles('contTablePedido');
    });
    // CLICK Boton <Consulta de Movimientos>
    $('.btMov').click(cargarMovimiento);
    // CLICK Boton <Busqueda de Movimientos>
    $('.btMovBus').click(retTablaMovimiento);
    //CLICK Boton copiar en el portapapeles del form Consulta Pedido
    $('.btMovCli').click(function(){
    	copiarEnPortaPapeles('contTableMov');
    });
    //CLICK Boton Impresion Movimiento
    $('.btMovImp').click(function(){
    	genMovPDF();
	});
	//CLICK Boton Movimientos Historicos
	$('.btMovHis').click(retHistMov);
}

// Evento que captura cual tab se dio clic 
function evtTabs(e){
	var target = $(e.target).attr("href")
	// Tab PYCCA 
	if(target=="#1a"){
		renTablesRotacion("#table_rotacion_py");
	}
	//Tab POLIPAPEL
	if(target=="#2a"){
		renTablesRotacion("#table_rotacion_pl");
	}
}

// Evento que captura los checks de los filtros y del menú de opciones
function evtCheckBox() {
	var checked = false;
	var idCheck = $(this).attr("id");

	if ($(this).is(':checked'))  {
		checked = true;
	} 
	//Si son los checks del form de filtros de busqueda
	if(idCheck.indexOf("chkFil")>-1){
		var idSec = "";
		idSec = idCheck.substr(6);
		evtCheckBoxFil(idSec,checked);
	}
	// Si son los checks del menú de opciones
	else if(idCheck.indexOf("chk")>-1){
		if(idCheck=="chkBdVir"){
			$('#hdBodVir').val((checked)?"1":"0");
			shBodegaVirtual();
		}
		if(idCheck=="chkBdSal"){
			$('#hdBodSal').val((checked)?"1":"0");
			if(retActiveMainTab()=="1")
				renTablesRotacion("#table_rotacion_py");
			else
				renTablesRotacion("#table_rotacion_pl");
		}
		if(idCheck=="chkByAnt"){
			$('#hdAntigu').val((checked)?"1":"0");
			resetPantalla();
		}
		if(idCheck=="chkMovTot"){
			if(checked){
				var rowCount = $('#table_movimiento tr').length-1;
				$('#table_movimiento').dataTable().fnDestroy();
				var table = $('#table_movimiento').dataTable({
								"bPaginate"	: false, 
								"bFilter"	: false, 
								"bInfo" 	: false,
								"sScrollY"	: $('#dvMovCont').height()-$('#dvMovBar').height()-$('#dvMovBus').height()-$('#dvMovTit').height()-51, 
								"sScrollX"	: "95%", 
								"sScrollXInner": "95%", 
								"order": [[2,"asc"]],
		        				"responsive": true,
						        drawCallback: function (settings) {
						            var api = this.api();
						            var rows = api.rows({ page: 'all' }).nodes();
						            var last = '';
						            var cant = 0;
						            api.column(2, { page: 'all' }).data().each(function (group, i) {

						            	if(last==''){
						            		last = group;
						            	}
						                if (last !== group ) {
						                	var grp = last.replace(/ /gi,'_');
						                    $(rows).eq(i-cant).before(
						                        '<tr class="group" style="background-color:#BAF3E3;">'+
						                        	'<td colspan=1 style="text-align:center;" class="details">'+
						                        		'<a href="javascript:void(0)" onclick="shDetailMov('+"'"+grp+"'"+')">'+
						                        			'<i id="btMovGrp'+grp+'" class="fa fa-plus"></i>'+
						                        	    '</a>'+
						                        	'</td>'+
						                        	'<td colspan="10" style="text-align:left;">'+last+' ('+cant+')'+'</td>'+
						                        '</tr>'
						                    );

						                    last = group;
						                    cant = 0;
						                }
						                cant = cant + 1;
						                if(rowCount==(i+1)){
						                	var grp = group.replace(/ /gi,'_');
						                    $(rows).eq(i+1-cant).before(
						                        '<tr class="group" style="background-color:#BAF3E3;">'+
						                        	'<td colspan=1 style="text-align:center;" class="details">'+
						                        		'<a href="javascript:void(0)" onclick="shDetailMov('+"'"+grp+"'"+')">'+
						                        			'<i id="btMovGrp'+grp+'" class="fa fa-plus"></i>'+
						                        	    '</a>'+
						                        	'</td>'+
						                        	'<td colspan="10" style="text-align:left;">'+ group	+' ('+cant+')'+'</td>'+
						                        '</tr>'
						                    );
						                }
						            });
						        }
							});
					$('#table_movimiento tbody td.data').hide();

			}else{
				$('#table_movimiento').dataTable().fnDestroy();
				retTablaMovimiento();
			}
		}
	}
}

function shDetailMov(grupo){
	var isOpen = !$('#btMovGrp'+grupo).hasClass('fa-plus');
	if(isOpen){
		$('#btMovGrp'+grupo).attr("class","fa fa-plus");
		$('#table_movimiento tbody td.grp'+grupo).hide();
	}else{
		$('#btMovGrp'+grupo).attr("class","fa fa-minus");
		$('#table_movimiento tbody td.grp'+grupo).show();	
	}
	
}

// Evento que controla cuando se trabaja con los checks de filtros de consulta
function evtCheckBoxFil(idSec,checked){
	var idCheck = "#chkFil"+idSec;
	var idSel	= ".cbFil"+idSec;
	var idTxA	= ".txFil"+idSec+"a";
	var idTxB	= ".txFil"+idSec+"b";
	//Si se elige un filtro se cargan las condiciones >,<,>=, etc..
	if(checked===true){
		$(idSel).append('<option value="1">=</option>');
		$(idSel).append('<option value="2">>=</option>');
		$(idSel).append('<option value="3">></option>');
		$(idSel).append('<option value="4"><</option>');
		$(idSel).append('<option value="5"><=</option>');
		$(idSel).append('<option value="6">ENTRE</option>');
		$(idTxA).removeAttr('disabled');
		$(idTxB).removeAttr('disabled');
		$(idTxA).focus();
	}
	else{
		$(idSel).children().remove().end();
		$(idTxA).val('');
		$(idTxB).val('');
		$(idTxA).attr('disabled', 'disabled');
		$(idTxB).attr('disabled', 'disabled');
		$("#dvFilTx"+idSec+"a").attr("class","col-sm-6");
		$("#dvFilTx"+idSec+"b").attr("class","col-sm-3 hide");
	}
}

//Evento que controla el KEY PRESS de los <input texts>
function evtInput(e){
	var isCodArt = $(this).hasClass('txCodArt')?true:false;
	var isDesArt = $(this).hasClass('txDesArt')?true:false;
	var isCodFab = $(this).hasClass('txCodFab')?true:false;
	var isUpc 	 = $(this).hasClass('txUpc')?true:false;

	if(e.keyCode==13){
		if(isCodArt || isDesArt || isCodFab || isUpc){	
			var tipBusAct = $('#hdTipBus').val();
			if(tipBusAct==5){
				$('select').css('background-color', 'white');
			}
			var txtValorBuscar = $(this).val().trim().toUpperCase();
			$('#hdTipBus').val('');
			$('#hdCodigo').val('');
			$('#hdCoLike').val('');
			$('#hdDescri').val('');
			$('#hdCodFab').val('');
			$('#hdCodUpc').val('');
			$('#hdTipBus').val('');
			var c_gr = 0;
			var tipo = 0;

			if(isCodArt){
				tipo = 1;
				$('#hdCoLike').val($(this).val());
			}
			if(isDesArt){
				tipo = 2;
				$('#hdDescri').val($(this).val());
			}
			if(isCodFab){
				tipo = 3;
				$('#hdCodFab').val($(this).val());
			}
			if(isUpc){
				tipo = 4;
				$('#hdCodUpc').val($(this).val());
			}
			retArticulo(tipo,txtValorBuscar,c_gr);
		}
		if($(this).hasClass('txSimB')){
			buscarSimilares();
		}
		if($(this).hasClass('txFacBus')){
			retTablaFactura();
		}
	}
	if(e.keyCode==8 || e.keyCode==46){
		if(isCodArt || isDesArt || isCodFab || isUpc){
			if($(this).val().trim()==''){
				resetPantalla();
			}
		}
	}
}
//Evento que controla el CHANGE de los <selects>
function evtSelect(e){
	//Si dio click en un cbo de la pantalla de filtros de condiciones.
	if($(this).attr("class").indexOf("cbFil")>-1){
		var idSec	= $(this).attr("class").substr(18);
		var selected = $(this).find(":selected").val();
		$(".txFil"+idSec+"a").val('');
		$(".txFil"+idSec+"b").val('');
		if(selected=="6"){ //Si dio click en la opcion "ENTRE"
			$("#dvFilTx"+idSec+"a").attr("class","col-sm-3");
			$("#dvFilTx"+idSec+"b").attr("class","col-sm-3");
		}else{
			$("#dvFilTx"+idSec+"a").attr("class","col-sm-6");
			$("#dvFilTx"+idSec+"b").attr("class","col-sm-3 hide");
		}
		$(".txFil"+idSec+"a").focus();
		return;
	}

	if($(this).attr("class").indexOf("cbPed")>-1){
		
		var isBus = $(this).hasClass('cbPedBus')?true:false;
		var isAlm = $(this).hasClass('cbPedAlm')?true:false;
		var selected = $(this).find(":selected").val();	
		if(isBus){
			if(selected=="6"){ //Si dio click en la opcion "ENTRE"
				$('.dvPedTxEnt').attr("class","col-sm-2 dvPedTxEnt");
			}else{
				$('.dvPedTxEnt').attr("class","col-sm-2 hide dvPedTxEnt");
			}
			$(".txPedBus1").val('');
			$(".txPedBus2").val('');
			$(".txPedBus1").focus();
		}
		if(isAlm){
			//TODO: SELECCIONO PEDIDOS DE UN ALMACEN

		}
		return;
	}

	if($(this).attr("class").indexOf("cbMov")>-1){
		
		var isTip = $(this).hasClass('cbMovTip')?true:false;
		var isAlm = $(this).hasClass('cbMovAlm')?true:false;
		var selected = $(this).find(":selected").val();	
		if(isTip){
			
		}
		if(isAlm){
			//TODO: SELECCIONO PEDIDOS DE UN ALMACEN

		}
		return;
	}
	//Cuando son los combos de la estructura comercial
	var selected = $(this).find(":selected").val();
	var isGru = $(this).hasClass('cbGru')?true:false;
	var isLin = $(this).hasClass('cbLin')?true:false;
	var isSbl = $(this).hasClass('cbSbl')?true:false;
	var isCat = $(this).hasClass('cbCat')?true:false;
	var isSbc = $(this).hasClass('cbSbc')?true:false;

	if(isGru || isLin || isSbl || isCat)	$('.cbSbc').children().remove().end();
	if(isGru || isLin || isSbl) 			$('.cbCat').children().remove().end();
	if(isGru || isLin) 						$('.cbSbl').children().remove().end();
	if(isGru) 								$('.cbLin').children().remove().end();

	if(selected==0){
		//Cuando selecciono un valor de cualquier combo y es igual a "TODAS"
		var c_gr = 0;
		var tipo = 5;
		var txtValorBuscar = "";
		if(!isGru){
			$('select').css('background-color', 'white');
			txtValorBuscar = " WHERE co_grupo = " + $('.cbGru').find(":selected").val() + " ";
			$('.cbGru').css('background-color', '#AFE1F7');
			if(isSbl || isCat || isSbc){
				txtValorBuscar += " AND co_linea = " + $('.cbLin').find(":selected").val() + " ";	
				$('.cbLin').css('background-color', '#AFE1F7');
			}
			if(isCat || isSbc){
				txtValorBuscar += " AND co_sublinea = " + $('.cbSbl').find(":selected").val() + " ";	
				$('.cbSbl').css('background-color', '#AFE1F7');
			}
			if(isSbc){
				txtValorBuscar += " AND co_categoria = " + $('.cbCat').find(":selected").val() + " ";
				$('.cbCat').css('background-color', '#AFE1F7');
			}
			$('#hdEstCom').val(txtValorBuscar);
			retArticulo(tipo,txtValorBuscar,c_gr);
			$('.txCodArt').focus();
		}
		return false;
	}
		//Cuando selecciono un valor de cualquier combo que no sea "TODAS"

		var sel = null;
		var def = -1;

		if(isGru){
			sel = $('.cbLin');
			def = defaulec;
		}
		if(isLin){
			sel = $('.cbSbl');	
			def = defaulec;
		}
		if(isSbl){
			sel = $('.cbCat');
			def = defaulec;
		}
		if(isCat){
			sel = $('.cbSbc');
			def = defaulec;
		}
		if(isSbc){
			//TODO: EJECUTAR SP QUE TRAE ARTICULOS CON UNA ESTRUCTURA COMERCIAL DEFINIDA
			var txtValorBuscar 	= " WHERE co_grupo 			= " + $('.cbGru').find(":selected").val() + " ";
			txtValorBuscar 		+= "  AND co_linea 			= " + $('.cbLin').find(":selected").val() + " ";
			txtValorBuscar 		+= "  AND co_sublinea 		= " + $('.cbSbl').find(":selected").val() + " ";
			txtValorBuscar 		+= "  AND co_categoria 		= " + $('.cbCat').find(":selected").val() + " ";
			txtValorBuscar 		+= "  AND co_subcategoria 	= " + $('.cbSbc').find(":selected").val() + " ";
			var tipo = 5;
			$('#hdEstCom').val(txtValorBuscar);
			retArticulo(tipo,txtValorBuscar,0);
			$('select').css('background-color', '#AFE1F7');
			$('.txCodArt').focus();
		}
		consultaEstructuraCom(sel,def);
	}

// Blanquea o limpia todos los controles de la pantalla
function limpiar(){
	$(".txCodArt").val('');				$('.cbGru').children().remove().end();
	$(".txDesArt").val('');				$('.cbLin').children().remove().end();
	$(".txCodFab").val('');				$('.cbSbl').children().remove().end();
	$(".txUpc").val('');				$('.cbCat').children().remove().end();
	$("p").text('');					$('.cbSbc').children().remove().end();
	$("h4[id*='h']").text('');
	$("label[class*='lb']").text('');
	$(".tbRot").find("tr:gt(0)").remove();
	$(".imgArt").attr("src","");
	$('.dvPed').css("display","none");
	$('#hdTipBus').val('');				$('#hdTipBus').val('');
	$('#hdCodigo').val('');				$('#hdEstCom').val('');
	$('#hdCoLike').val('');				$('#hdMdlAct').val('');
	$('#hdDescri').val('');				$('#hOfeDes').text('');
	$('#hdCodFab').val('');				$('#hOfeHas').text('');
	$('#hdCodUpc').val('');				$('#hPerDes').text('');
	$('.lbOfe').text('');
	$('.contTablePyRot').html(defHtmlTblRot);
	$('.contTablePolRot').html(defHtmlTblRot);
	$('select').css('background-color', 'white');
	heighDvRot = 0.85;
	heighTbRot = 0.75;
	$('.contTablePyRot').css("height",$('.dvInfCont').height()*heighDvRot);
	$('.contTablePolRot').css("height",$('.dvInfCont').height()*heighDvRot);
	tableRotPl = "";
	tableRotPy = "";
}

//Genera el html de las tablas de rotacion de saldos y las crea en el document
function renTablesRotacion(idTable){
	var tbpy = (idTable=="#table_rotacion_py")?true:false;
	var cont = (tbpy)?".contTablePyRot":".contTablePolRot";
	var data = (tbpy)?tableRotPy:tableRotPl;
	var shBodSal = ($('#hdBodSal').val()=="1")?true:false;
	var pnlPed = ($('.dvPed').css('display')=="none")?false:true;

	if(pnlPed){
		heighDvRot = 0.77;
		heighTbRot = 0.65;
	}else{
		heighDvRot = 0.85;
		heighTbRot = 0.75;
	}

	if(data!=""){
		$(cont).html('');
		$(cont).append(data);
		shBodegaVirtual();
		if(shBodSal){
			shBodegaSaldo(idTable);	
		}
		$(idTable).dataTable({
			"autoWidth" : true,
			"bPaginate"	: false, 
			"bFilter"	: false, 
			"bInfo" 	: false,
			"sScrollY"	: $('.dvInfCont').height()*heighTbRot, 
			"sScrollX"	: "95%", 
			"sScrollXInner": "95%", 
			"order": [],
			"columnDefs": [ {
				"targets": [0,1,2,3,4],
				"orderable": false
			} ]
		});
		$('.contTablePyRot').css("height",$('.dvInfCont').height()*heighDvRot);
		$('.contTablePolRot').css("height",$('.dvInfCont').height()*heighDvRot);
	}else{
		$(cont).html(defHtmlTblRot);
	}	
}

//Permite ocultar las filas de la tabla de rotacion de saldos 
//cuando el saldo de alguna bodega es 0
function shBodegaSaldo(idTable){
	$(idTable+" tbody tr").each(function (index){
		var saldo = 0;
		var isAV  = false; 
		$(this).children("td").each(function (index2) 
		{
			if(index2==1){	
				saldo = parseInt($(this).text());
			}
			if(index2==3){
				isAV = $(this).css('color') == 'rgb(255, 0, 0)';
			}
		});
		if(saldo==0 && !isAV){
			$(this).hide();
		}
	});
}

//Permite ocultar/mostrar la columna de bodega virtual en la
//tabla de rotacion de saldos.
function shBodegaVirtual(){
	var show = ($('#hdBodVir').val()=="1")?true:false;
	if(show){
		$('#table_rotacion_py td:nth-child(5),th:nth-child(5)').show();
		$('#table_rotacion_pl td:nth-child(5),th:nth-child(5)').show();
	}else{
		$('#table_rotacion_py td:nth-child(5),th:nth-child(5)').hide();
		$('#table_rotacion_pl td:nth-child(5),th:nth-child(5)').hide();
	}
}

//Cuando se aumenta o disminuye el zoom o se redimensiona el 
//browser la pantalla principal se contrae o ensancha
function redimensionar(){
	var screenWidth = window.innerWidth;
	if(screenWidth<1120){
		//TODO Resolucion antigua 
	}else if(screenWidth<1400){
		$('#secCa').attr("class","content invoice");
	}else if(screenWidth<1600){
		$('#secCa').attr("class","content invoice2");
	}else if(screenWidth<1800){
		$('#secCa').attr("class","content invoice3");
	}else{
		$('#secCa').attr("class","content invoice4");
	}
}

//Oculta el modal activo
function quitarModal(){
	var modals = $('#hdMdlAct').val().split('>');
	var lenModals = modals.length;
	var nameModal = modals[lenModals-1];
	modals.splice(modals.indexOf(lenModals),1);
	var resModals = modals.join('>');

	$('#hdMdlAct').val(resModals);
	$('#hdClkHis').val('0');
	$('.contTableForecast').html('');
	$('.contTableSimilares').html('');
	$('.contTablePercha').html('');
	$('.contTableFicha').html('');
	$('.contTableFactura').html('');
	quitarmodalGeneral(nameModal, '');
}

//Funcion que retorna la estructurta comercial de grupo, linea, sublinea, categoria y subcategoria
function consultaEstructuraCom(sel,defval){

	var url = $('#txt_urlGe').val();
	var c_gr = $('.cbGru').find(":selected").val();
	var c_li = $('.cbLin').find(":selected").val();
	var c_sl = $('.cbSbl').find(":selected").val();
	var c_ca = $('.cbCat').find(":selected").val();
	var c_sc = $('.cbSbc').find(":selected").val();

	if(linea>0) c_li=linea;
	if(sublinea>0) c_sl=sublinea;
	if(categoria>0) c_ca=categoria;
	if(subcategoria>0) c_sc=subcategoria;

	var datos = "";
	if(c_gr) datos = datos+"c_gr="+c_gr; 
	if(c_li) datos = datos+"&c_li="+c_li;
	if(c_sl) datos = datos+"&c_sl="+c_sl;
	if(c_ca) datos = datos+"&c_ca="+c_ca;   
	if(c_sc) datos = datos+"&c_sc="+c_sc;

	$.ajax({
		type : 'POST',
		url : url+"inventario/ConsultaArticulo/retEstructCom", 
		data: datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if (data){
					$.each(data,function(e,dato){
						sel.append('<option value="' + dato.valor + '">' + dato.mostrar + "</option>'");
					});
					sel.val(defval);
				}
			} else {
				sel.children().remove().end();
				errorGeneral("Sistema Intranet",returnData,'danger');
			}
		}
	});
}

//Funcion que retorna el codigo y la descripcion de un articulo.
//tipo: 1: Por Codigo.
//		2: Por descripcion.
//		3: Por Codigo de fabrica.
//		4: Por codigo UPC.
//		5: Por Estructura Comercial
//valor: valor de busqueda dependiendo del tipo.
function retArticulo(tipo,valor){
	var url 	= $('#txt_urlGe').val();
	var datos 	= "";
	var fl_an 	= $('#hdAntigu').val();
	var filtro 	= $('#hdFilSql').val();
	
	if(tipo>0){
		if(valor.indexOf("%")>-1){
			valor = valor.replace(/%/,"||");
		}
		datos = datos+"c_tp="+tipo; 
		datos = datos+"&c_vl="+valor; 
		datos = datos+"&c_an="+fl_an;
		datos = datos+"&c_fl="+filtro;

		$.ajax({
			type : 'POST',
			url : url+"inventario/ConsultaArticulo/retFiltros2", 
			data: datos,
			success : function (returnData) {
				if (returnData.trim().substr(0,2) === "OK"){
					var data = eval(returnData.trim().substr(3));
					if (data){
						$.each(data,function(e,dato){
							$('#hdTipBus').val(tipo);
							retDetalleArt(dato.codigo);
						});
					}
				} else {
					resetPantalla();
					errorGeneral("Sistema Intranet",returnData,'danger');
				}
			}
		});
	}
}



//Funcion que carga la informacion del articulo anterior
function retAdelante(){
	var url = $('#txt_urlGe').val();
	var tipBus = $('#hdTipBus').val();
	var codArt = $('#hdCodigo').val();
	var codLik = $('#hdCoLike').val();
	var desArt = $('#hdDescri').val();
	var codFab = $('#hdCodFab').val();
	var codUpc = $('#hdCodUpc').val();
	var estCom = $('#hdEstCom').val();
	var fl_an  = $('#hdAntigu').val();
	var filtro = $('#hdFilSql').val();

	var datos = "";
	if(tipBus!=''){
		datos = datos+"c_tb="+tipBus; 
		datos = datos+"&c_ca="+codArt; 
		datos = datos+"&c_cl="+codLik; 
		datos = datos+"&c_da="+desArt; 
		datos = datos+"&c_cf="+codFab; 
		datos = datos+"&c_cu="+codUpc; 
		datos = datos+"&c_ec="+estCom; 
		datos = datos+"&c_an="+fl_an;
		datos = datos+"&c_fl="+filtro;

		$.ajax({
			type : 'POST',
			url : url+"inventario/ConsultaArticulo/retAdelante", 
			data: datos,
			success : function (returnData) {
				if (returnData.trim().substr(0,2) === "OK"){
					var data = eval(returnData.trim().substr(3));
					if (data){
						$.each(data,function(e,dato){
							retDetalleArt(dato.codigo);
						});
					}
				} else {
					errorGeneral("Sistema Intranet",returnData,'danger');
				}
			}
		});
	}
}

//Funcion que carga la informacion del articulo siguiente
function retAnterior(){
	var url = $('#txt_urlGe').val();
	var tipBus = $('#hdTipBus').val();
	var codArt = $('#hdCodigo').val();
	var codLik = $('#hdCoLike').val();
	var desArt = $('#hdDescri').val();
	var codFab = $('#hdCodFab').val();
	var codUpc = $('#hdCodUpc').val();
	var estCom = $('#hdEstCom').val();
	var fl_an  = $('#hdAntigu').val();
	var filtro = $('#hdFilSql').val();

	var datos = "";
	if(tipBus!=''){
		datos = datos+"c_tb="+tipBus; 
		datos = datos+"&c_ca="+codArt; 
		datos = datos+"&c_cl="+codLik; 
		datos = datos+"&c_da="+desArt; 
		datos = datos+"&c_cf="+codFab; 
		datos = datos+"&c_cu="+codUpc; 
		datos = datos+"&c_ec="+estCom; 
		datos = datos+"&c_an="+fl_an;
		datos = datos+"&c_fl="+filtro;

		$.ajax({
			type : 'POST',
			url : url+"inventario/ConsultaArticulo/retAnterior", 
			data: datos,
			success : function (returnData) {
				if (returnData.trim().substr(0,2) === "OK"){
					var data = eval(returnData.trim().substr(3));
					if (data){
						$.each(data,function(e,dato){
							retDetalleArt(dato.codigo);
						});
					}
				} else {
					errorGeneral("Sistema Intranet",returnData,'danger');
				}
			}
		});
	}
}

//Funcion que resetea la pantalla
function resetPantalla(){
	limpiar();
	$('.txCodArt').focus();
	var sel = $('.cbGru');
	consultaEstructuraCom(sel,-1);
}

//Funcion que carga la rotacion del producto en una tabla
function retRotacion() {
	var datos = "";
	var url1 = $('#txt_urlGe').val();
	var codArt = $('#hdCodigo').val();
	datos = datos+"c_cd="+codArt;
	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retRotacion',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var datos = JSON.parse(returnData.trim().substr(3));
				var rotacion=datos.rotacion;
				var pedidos=datos.pedidos;

				$('.contTablePyRot').html('');
				$('.contTablePolRot').html('');
				var tbPY = '<table id="table_rotacion_py" class="table table-bordered text-size-0-2 no-padding no-margin">';
				var tbPL = '<table id="table_rotacion_pl" class="table table-bordered text-size-0-2 no-padding no-margin">';
				var rowBlue ="";
				var headTb = "";
				headTb = headTb+'<thead>';
				headTb = headTb+'<th style="background-color:#001f3f; color:white; font-size:1.2em;">Descripción</th>';
				headTb = headTb+'<th style="background-color:#001f3f; color:white; font-size:1.2em;">Saldo</th>';
				headTb = headTb+'<th style="background-color:#001f3f; color:white; font-size:1.2em;">Rot360</th>';
				headTb = headTb+'<th style="background-color:#001f3f; color:white; font-size:1.2em;">Rot90</th>';
				headTb = headTb+'<th style="background-color:#001f3f; color:white; font-size:1.2em;">Virt.</th>';
				headTb = headTb+'</thead>';
				headTb = headTb+'<tbody>';
				tbPY = tbPY + headTb;
				tbPL = tbPL + headTb;
				var saldoTotal = 0;
				var totRot360 = 0;
				var totRot90 = 0;
				var rot360Mayor =0;

				if (rotacion){
					var estiloAv 	= "";
					var estiloBC 	= "";
					var estiloAl 	= "";
					var estiloSal	= "";
					var estiloAvRed = "";					
					var rot360 		= 0;
					var rot90 		= 0;

					$.each(rotacion,function(e,dato){
						saldoTotal+=parseInt(dato.saldo);
						rot360=parseFloat((dato.rotdiaria!="AV")?dato.rotdiaria:0)*30.5;
						//Si el tipo es BODEGA
						if(dato.tipo=="B"){
							estiloBC = "color:#f012be;";
							rot90 = parseFloat(dato.reservado);
						}else{
							estiloBC = "";
							rot90 		= parseFloat(parseFloat(dato.rot90)*30.5);
							totRot360 	= totRot360 + rot360;
							totRot90 	= totRot90 	+ rot90;
							rot360Mayor = rot360Mayor + parseFloat(dato.rot360);
						}
						//Si el almacen es CENTRO
						if(dato.co_bodega=="4"){
							estiloAl = "background-color:#5bc0de;";
						}else{
							estiloAl = "";
						}

						if(dato.saldo<0){
							estiloSal = "color:red;";
						}else{
							estiloSal = "";
						}	
						//Si es bodegas de averiados
						if(dato.rotdiaria!="AV"){
							estiloAv 	= "";
							estiloAvRed = "";
						}else{
							rowBlue ="";
							rowBlue = rowBlue+'<tr style="background-color:#033D66;">';
							rowBlue = rowBlue+'<td>'+'</td>';
							rowBlue = rowBlue+'<td>'+'</td>';
							rowBlue = rowBlue+'<td>'+'</td>';
							rowBlue = rowBlue+'<td>'+'</td>';
							rowBlue = rowBlue+'<td>'+'</td>';
							rowBlue = rowBlue+'</tr>';
							tbPY = tbPY + rowBlue;
							estiloAv = "background-color:#C0DDF1;";
							estiloAvRed ="color:red;";
						}
						if(dato.empresa_tipo=="Y"){	
							tbPY = tbPY+'<tr style="'+estiloAv+estiloAl+'color:#001f3f">';
							tbPY = tbPY+'<td>'+dato.descripcion+'</td>';
							tbPY = tbPY+'<td style = "'+estiloSal+'text-align:right;">'+dato.saldo+'</td>';
							tbPY = tbPY+'<td style="text-align:right;">'+rot360.toFixed(2)+'</td>';
							tbPY = tbPY+'<td style="'+estiloBC+estiloAvRed+'text-align:right;">'+rot90.toFixed(2)+'</td>';
							tbPY = tbPY+'<td style="text-align:right;">'+dato.virtual+'</td>';
							tbPY = tbPY+'</tr>';
						}
						if(dato.empresa_tipo=="P" || dato.tipo=="B"){	
							tbPL = tbPL+'<tr style="'+estiloAv+estiloAl+'">';
							tbPL = tbPL+'<td>'+dato.descripcion+'</td>';
							tbPL = tbPL+'<td style = "'+estiloSal+'text-align:right;">'+dato.saldo+'</td>';
							tbPL = tbPL+'<td style="text-align:right">'+rot360.toFixed(2)+'</td>';
							tbPL = tbPL+'<td style="'+estiloBC+estiloAvRed+'text-align:right;">'+rot90.toFixed(2)+'</td>';
							tbPL = tbPL+'<td style="text-align:right;">'+dato.virtual+'</td>';
							tbPL = tbPL+'</tr>';
						}
					});
					//Si es proveedor PYCCA llena los pedidos
					$('.lbPedTot').text(pedidos.total);
					$('.lbBodPICA').text(pedidos.bc);
					$('.lbTienPICA').text(pedidos.tienda);

					if(pedidos.tipo!==""){
						$('.lbObs').text($('.lbObs').text()+" tipo="+pedidos.tipo);	
					}
					//Sumatoria de los saldos y rotaciones
					$('.lbSalTo').text(saldoTotal);
					$('.lbRotTo360').text(totRot360.toFixed(2));
					$('.lbRotTo90').text(totRot90.toFixed(2));
					$('.lbRotMay').text(rot360Mayor.toFixed(2));
				}

				tbPY = tbPY+'</tbody>';
				tbPY = tbPY+'</table>';
				tbPL = tbPL+'</tbody>';
				tbPL = tbPL+'</table>';
				tableRotPl = tbPL;
				tableRotPy = tbPY;
				if(retActiveMainTab()=="1")
					renTablesRotacion("#table_rotacion_py");
				else
					renTablesRotacion("#table_rotacion_pl");
			}
		}
	});
}

//Invoca al formulario Modal de Filtros de consultas
function cargarFiltro(){
	resetPantalla();
	$('#hdMdlAct').val('Filtros');
	abrirModal('Filtros','40','45','form_filtros');
	$('#modalFiltros').css("height",$('#dvFilBar').height()+$('#dvFilCont').height()+60);
	var arrFil = [ false,  false, false, false, false, false];
	$.each( arrFil, function( i, l ){
		var id=i+1;
		if(!$("#chkFil"+id).is(':checked')){
			evtCheckBoxFil(id,false);
		}
	});
}

//Elimina el filtro de busqueda
function quitarFiltro(){
	evtCheckBoxFil("1",false);
	evtCheckBoxFil("2",false);
	evtCheckBoxFil("3",false);
	evtCheckBoxFil("4",false);
	evtCheckBoxFil("5",false);
	evtCheckBoxFil("6",false);
	$('input[id*=chkFil]').prop('checked', false);
	$('.filCon').text('');
	$('#hdFilSql').val('');
}

//Establece el filtro para la busqueda de productos
function aplicarFiltros(){
	var arrFil = [ false,  false, false, false, false, false];
	var condFilSql = "";
	var condFil = "";

	$.each( arrFil, function( i, l ){
		var id=i+1;
		if($("#chkFil"+id).is(':checked')){
			var camp = $("#chkFil"+id).attr("name");
			var cond = $(".cbFil"+id).find(":selected").text();
			var val1 = $(".txFil"+id+"a").val().toUpperCase();
			var val2 = "";
			var betw = false;
			if(val1==""){
				errorGeneral("Sistema Intranet","Debe ingresar un valor","info");
				$(".txFil"+id+"a").focus();
				return;
			}
			if(cond.indexOf("ENTRE")>-1){
				cond = cond.replace("ENTRE","BETWEEN");
				betw = true;
			}
			if(id==6)	val1 = "''"+val1+"''";
			if(!betw){
				condFilSql += " AND ("+camp+" "+cond+" "+val1+") ";
			}else{
				val2 = $(".txFil"+id+"b").val().toUpperCase();
				if(val2==""){
					errorGeneral("Sistema Intranet","Debe ingresar un valor","info");
					$(".txFil"+id+"b").focus();
					return;
				}
				if(id==6)	val2 = "''"+val2+"''";
				condFilSql += " AND ("+camp+" "+cond+" "+val1+" AND "+val2+") ";
			}
		}
	});
	condFil = condFilSql.replace(/BETWEEN/gi,"entre");
	condFil = condFil.replace(/AND/gi," y ");
	condFil = condFil.replace("DATEDIFF(NOW(),a.fe_ult_factura)","Antiguedad");
	condFil = condFil.replace(/a\./gi,"");
	condFil = condFil.replace(/r\./gi,"");
	condFil = condFil.replace(/k\./gi,"");
	condFil = condFil.replace(/s\./gi,"");
	condFil = condFil.replace(/pvpiva/gi,"PVP+Iva");
	condFil = condFil.replace(/\(rotdiaria\*30\.5\)/gi,"Rotación Mensual Almacén");
	condFil = condFil.replace(/ds_ranking/gi,"Ranking");
	condFil = condFil.replace(/saldo/gi,"Saldo");
	condFil = condFil.replace(/saldo_total/gi,"Saldo Total");
	condFil = condFil.substr(3);
	$('.filCon').text(condFil);
	$('#hdFilSql').val(condFilSql);
	quitarModal();
}

/*********************************************************************************************************/
/******************************** S I M I L A R E S ******************************************************/
/*********************************************************************************************************/
//Carga la pantalla inicial de los articulos similares, lanza el div modal similares.
function cargarSimilares(){

	var tipBusAct 	= $('#hdTipBus').val();
	var txCodArt 	= $('#hdCodigo').val();
	var txCodFab 	= $('.txCodFab').val();
	var dsArt 		= $('.txDesArt').val();

	var tit 		= "";

	if(txCodArt==''){
		errorGeneral("Sistema Intranet","Debe ingresar el código del articulo",'info');    		
		return false;
	}

	$('#hdMdlAct').val('Similares');
	abrirModal('Similares','80','70','form_similares');
	switch(tipBusAct)
	{
		case "1"  	:
		case "4"	:
		case "2"	:
		case "5"	:
		tit = txCodArt+" - "+dsArt;
		break;
		case "3"	:
		tit = "Fábrica: "+txCodFab
		break;
	}	
	$('#txSmTit').text(tit);
	$('.txSimB').val('');
	$('.txSimB').focus();
	$('.contTableSimilares').html('<button class="btn btn-lg bg-red">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');
	retTablaSimilares();
}
//Llena el grid de los articulos similares.
function retTablaSimilares() {
	var datos = "";
	var tipBus = $('#hdTipBus').val();
	var codArt = $('#hdCodigo').val();
	var desArt = $('#hdDescri').val();
	var codFab = $('#hdCodFab').val();
	var codUpc = $('#hdCodUpc').val();
	var estCom = $('#hdEstCom').val();
	var filtro = $('#hdFilSql').val();
	var url1 = $('#txt_urlGe').val();
	
	if(tipBus!=''){
		datos = datos+"c_tb="+tipBus; 
		datos = datos+"&c_ca="+codArt; 
		datos = datos+"&c_da="+desArt; 
		datos = datos+"&c_cf="+codFab; 
		datos = datos+"&c_cu="+codUpc; 
		datos = datos+"&c_ec="+estCom;
		datos = datos+"&c_fl="+filtro;

		$.ajax({
			type : 'POST',
			url  : url1+'inventario/ConsultaArticulo/retTablaSimilares',
			data : datos,
			success : function (returnData) {
				if (returnData.trim().substr(0,2) === "OK"){
					var data = eval(returnData.trim().substr(3));

					$('.contTableSimilares').html('');
					var row = '<table id="table_similares" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#e74343; color:white;">Código</th>';
					row = row+'<th style="background-color:#e74343; color:white;">Descripción</th>';
					row = row+'<th style="background-color:#e74343; color:white;">PVP</th>';
					row = row+'<th style="background-color:#e74343; color:white;">PVP+IVA</th>';
					row = row+'<th style="background-color:#e74343; color:white;">Saldo</th>';
					row = row+'<th style="background-color:#e74343; color:white;">Fch.Oferta</th>';
					row = row+'<th style="background-color:#e74343; color:white;">%Desc.</th>';
					row = row+'<th style="background-color:#e74343; color:white;">PVP_Oferta</th>';
					row = row+'<th style="background-color:#e74343; color:white;">Fábrica</th>';
					row = row+'<th style="background-color:#e74343; color:white;">Upc</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						var estilo = '';
						$.each(data,function(e,dato){
							if(dato.codigo==codArt)	{estilo=' style="background-color:#f56954; font-weight: bold; color:white;"';}
							else {estilo = '';}
							row = row+'<tr'+estilo+'>';
							row = row+'<td ><a style="text-align:right;text-decoration: underline;color:#F3170E;" href="javascript:void(0)" onclick="escogerSimilar('+"'"+dato.codigo+"'"+')"><span class="text-size-0-5">'+dato.codigo+'</span></td>';
							row = row+'<td style="text-align:left">'+dato.descripcion+'</td>';
							row = row+'<td style="text-align:right">'+parseFloat(dato.pvp).toFixed(2)+'</td>';
							row = row+'<td style="text-align:right">'+parseFloat(dato.pvpiva).toFixed(2)+'</td>';
							row = row+'<td style="text-align:right">'+dato.saldo+'</td>';
							row = row+'<td style="text-align:center">'+dato.fe_ohasta+'</td>';
							row = row+'<td style="text-align:right">'+dato.descuento+'%'+'</td>';
							row = row+'<td style="text-align:right">'+parseFloat(dato.pvp_oferta_iva).toFixed(2)+'</td>';
							row = row+'<td style="text-align:left">'+dato.co_fabrica+'</td>';
							row = row+'<td style="text-align:left">'+dato.upc+'</td>';
							row = row+'</tr>';
						});

					}

					row = row+'</tbody>';
					row = row+'</table>';

					$('.contTableSimilares').append(row);
					$('#table_similares').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvSimCont').height()-$('#dvSimBar').height()-$('#dvSimTit').height()-$('#dvSimSearch').height()-50, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []
					} );
				}else{
					$('.contTableSimilares').html(returnData);
				}
			}
		});
	}
}
//Evento que sucede cuando el usuario busca una lista de articulos en el form de similares .
function buscarSimilares(){
	$('.contTableSimilares').html('<button class="btn btn-lg bg-red">'+
				'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
				'</button>');
	var datos = "";
	var busq  = $('.txSimB').val();
	var url1 = $('#txt_urlGe').val();
	var codArt = $('#hdCodigo').val();
	if(busq.indexOf("%")>-1){
		busq = busq.replace(/%/,"||");
	}
	datos = datos+"c_bu="+busq; 
	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retBuscarSimilares',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				$('.contTableSimilares').html('');
				var row = '<table id="table_similares" class="table table-bordered table-success text-size-0-5 no-padding no-margin text-bold">';
				row = row+'<thead>';
				row = row+'<th style="background-color:#e74343; color:white;">Código</th>';
				row = row+'<th style="background-color:#e74343; color:white;">Descripción</th>';
				row = row+'<th style="background-color:#e74343; color:white;">PVP</th>';
				row = row+'<th style="background-color:#e74343; color:white;">PVP+IVA</th>';
				row = row+'<th style="background-color:#e74343; color:white;">Saldo</th>';
				row = row+'<th style="background-color:#e74343; color:white;">Fecha Oferta</th>';
				row = row+'<th style="background-color:#e74343; color:white;">%Desc.</th>';
				row = row+'<th style="background-color:#e74343; color:white;">PVP_Oferta</th>';
				row = row+'<th style="background-color:#e74343; color:white;">Fábrica</th>';
				row = row+'<th style="background-color:#e74343; color:white;">Upc</th>';
				row = row+'</thead>';
				row = row+'<tbody>';

				if (data){
					var estilo = '';
					$.each(data,function(e,dato){
						if(dato.codigo==codArt)	{estilo=' style="background-color:#f56954; font-weight: bold; color:white;"';}
						else {estilo = '';}
						row = row+'<tr'+estilo+'>';
						row = row+'<td><a style="text-align:right;text-decoration: underline;color:#F3170E;" href="javascript:void(0)" onclick="escogerSimilar('+"'"+dato.codigo+"'"+')"><span class="text-size-0-5">'+dato.codigo+'</span></td>';
						row = row+'<td style="text-align:left">'+dato.descripcion+'</td>';
						row = row+'<td style="text-align:right">'+parseFloat(dato.pvp).toFixed(2)+'</td>';
						row = row+'<td style="text-align:right">'+parseFloat(dato.pvpiva).toFixed(2)+'</td>';
						row = row+'<td style="text-align:right">'+dato.saldo+'</td>';
						row = row+'<td style="text-align:center">'+dato.fe_ohasta+'</td>';
						row = row+'<td style="text-align:right">'+dato.descuento+'%'+'</td>';
						row = row+'<td style="text-align:right">'+parseFloat(dato.pvp_oferta_iva).toFixed(2)+'</td>';
						row = row+'<td style="text-align:left">'+dato.co_fabrica+'</td>';
						row = row+'<td style="text-align:left">'+dato.upc+'</td>';
						row = row+'</tr>';
					});

				}

				row = row+'</tbody>';
				row = row+'</table>';

				$('.contTableSimilares').append(row);
				$('#table_similares').dataTable({
					"bPaginate"	: false, 
					"bFilter"	: false, 
					"bInfo" 	: false,
					"sScrollY"	: $('#dvSimCont').height()-$('#dvSimBar').height()-$('#dvSimTit').height()-$('#dvSimSearch').height()-50, 
					"sScrollX"	: "95%", 
					"sScrollXInner": "95%", 
					"order": []
				} );
			}else{
				$('.contTableSimilares').html(returnData);
			}
		}

	});
}
//Cuando se elige un codigo de la lista de similares se carga en la pantalla pincipal el articulo elegido.
function escogerSimilar(codigo){
	retDetalleArt(codigo);
	quitarmodalGeneral('Similares','form_similares');
}
/*********************************************************************************************************/
/******************************** F I C H A   T E C N I C A **********************************************/
/*********************************************************************************************************/
//Invoca al formulario modal de FICHA TECNICA
function cargarFicha(){
	var txCodArt = $('#hdCodigo').val();
	var dsArt 	 = $('.txDesArt').val();
	if(txCodArt==''){
		errorGeneral("Sistema Intranet","Debe ingresar el código del articulo",'info');    		
		return false;
	}
	$('#hdMdlAct').val('Ficha');
	abrirModal('Ficha','50','60','form_ficha');
	$('#txFicTit').text(txCodArt+" - "+dsArt);
	$('.contTableFicha').html('<button class="btn btn-lg bg-navy">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');
	retTablaFicha();
}

//Carga la informacion de Ficha Tecnica del Producto
function retTablaFicha(){
	var datos 	= "";
	var codArt 	= $('#hdCodigo').val();
	var url1 	= $('#txt_urlGe').val();

	datos = datos+"c_ca="+codArt; 

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaFicha',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if(data){
					$('.contTableFicha').html('');
					var row = '<table id="table_ficha" class="table table-bordered table-hover text-size-0-5 no-padding no-margin text-bold text-primary">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#001f3f; color:white;"></th>';
					row = row+'<th style="background-color:#001f3f; color:white;">Aspecto</th>';
					row = row+'<th style="background-color:#001f3f; color:white;">Valor</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						$.each(data,function(e,dato){
							row = row+'<tr>';
							row = row+'<td style="text-align:right">'+dato.codigo+'</td>';
							row = row+'<td style="text-align:left">'+dato.descripcion+'</td>';
							row = row+'<td style="text-align:left">'+dato.atributo+'</td>';
							row = row+'</tr>';
						});

					}

					row = row+'</tbody>';
					row = row+'</table>';

					$('.contTableFicha').append(row);
					$('#table_ficha').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvFicCont').height()-$('#dvFicBar').height()-$('#dvFicTit').height()-52, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []
					} );

				}
			}else{
				$('.contTableFicha').html(returnData);
			}
		}

	});
}
/*********************************************************************************************************/
/******************************************** P E R C H A S **********************************************/
/*********************************************************************************************************/
//Invoca al formulario modal de PERCHAS
function cargarPercha(){
	var txCodArt = $('#hdCodigo').val();
	var dsArt 	 = $('.txDesArt').val();
	if(txCodArt==''){
		errorGeneral("Sistema Intranet","Debe ingresar el código del articulo",'info');    		
		return false;
	}
	$('#hdMdlAct').val('Percha');
	abrirModal('Percha','40','40','form_percha');
	$('#txPerTit').text(txCodArt+" - "+dsArt);
	$('#spPerBar').text("Consulta de ubicación en perchas de arículo "+txCodArt);
	$('.contTablePercha').html('<button class="btn btn-lg bg-teal">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');
	retTablaPercha();
}

//Carga la informacion de perchas del producto
function retTablaPercha(){
	var datos 	= "";
	var codArt 	= $('#hdCodigo').val();
	var url1 	= $('#txt_urlGe').val();

	datos = datos+"c_ca="+codArt; 

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaPercha',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if(data){
					$('.contTablePercha').html('');
					var row = '<table id="table_percha" class="table table-bordered table-hover text-size-0-5 no-padding no-margin text-bold text-primary">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#39cccc; color:white;">Bodega</th>';
					row = row+'<th style="background-color:#39cccc; color:white;">Zona</th>';
					row = row+'<th style="background-color:#39cccc; color:white;">Columna</th>';
					row = row+'<th style="background-color:#39cccc; color:white;">Percha</th>';
					row = row+'<th style="background-color:#39cccc; color:white;">Nivel</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						$.each(data,function(e,dato){
							row = row+'<tr>';
							row = row+'<td style="text-align:left">'+dato.bodega+'</td>';
							row = row+'<td style="text-align:center">'+dato.zona+'</td>';
							row = row+'<td style="text-align:center">'+dato.columna+'</td>';
							row = row+'<td style="text-align:center">'+dato.percha+'</td>';
							row = row+'<td style="text-align:center">'+dato.nivel+'</td>';
							row = row+'</tr>';
						});

					}

					row = row+'</tbody>';
					row = row+'</table>';

					$('.contTablePercha').append(row);
					$('#table_percha').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvPerCont').height()-$('#dvPerBar').height()-$('#dvPerTit').height()-51, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []
					} );

				}
			}else{
				$('.contTablePercha').html(returnData);
			}
		}

	});
}
/*********************************************************************************************************/
/******************************************** F O R E C A S T ********************************************/
/*********************************************************************************************************/
//Invoca al formulario modal de FORECAST
function cargarForecast(){
	var txCodArt = $('#hdCodigo').val();
	var dsArt 	 = $('.txDesArt').val();
	if(txCodArt==''){
		errorGeneral("Sistema Intranet","Debe ingresar el código del articulo",'info');    		
		return false;
	}
	$('#hdMdlAct').val('Forecast');
	abrirModal('Forecast','50','60','form_forecast');
	$('#txForTit').text(txCodArt+" - "+dsArt);
	$('.contTableForecast').html('<button class="btn btn-lg bg-orange">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');
	retTablaForecast();
}

//Carga la informacion del forecast del producto
function retTablaForecast(){
	var datos = "";
	var codArt = $('#hdCodigo').val();
	var url1 = $('#txt_urlGe').val();
	datos = datos+"c_ca="+codArt; 

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaForecast',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				var importante = "";
				if(data){
					$('.contTableForecast').html('');
					var row = '<table id="table_forecast" class="table table-bordered table-hover text-size-0-5 no-padding no-margin text-bold text-primary">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#ff851b; color:white;">Mes</th>';
					row = row+'<th style="background-color:#ff851b; color:white;">Forecast</th>';
					row = row+'<th style="background-color:#ff851b; color:white;">Und. Pedidas</th>';
					row = row+'<th style="background-color:#ff851b; color:white;">Und. Recibidas</th>';
					row = row+'<th style="background-color:#ff851b; color:white;">Und. Vendidas</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						$.each(data,function(e,dato){
							row = row+'<tr>';
							row = row+'<td style="text-align:left">'+dato.anio+"-"+dato.mes+'</td>';
							row = row+'<td style="text-align:right">'+dato.forecast+'</td>';
							row = row+'<td style="text-align:right">'+dato.pedido+'</td>';
							row = row+'<td style="text-align:right">'+dato.recibido+'</td>';
							row = row+'<td style="text-align:right">'+dato.vendido+'</td>';
							row = row+'</tr>';
							importante = dato.importante;
						});
						if(importante=="Si"){
							$('#dvForImp').css("display","inline");
						}else{
							$('#dvForImp').css("display","none");
						}
					}

					row = row+'</tbody>';
					row = row+'</table>';

					$('.contTableForecast').append(row);
					$('#table_forecast').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvForCont').height()-$('#dvForBar').height()-$('#dvForTit').height()-51, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []
					} );

				}
			}else{
				$('.contTableForecast').html(returnData);
			}
		}

	});
}
/*********************************************************************************************************/
/******************************************** F A C T U R A S ********************************************/
/*********************************************************************************************************/
//Invoca al formulario modal de FACTURAS
function cargarFactura(){
	var txCodArt = $('#hdCodigo').val();
	var dsArt 	 = $('.txDesArt').val();
	if(txCodArt==''){
		errorGeneral("Sistema Intranet","Debe ingresar el código del articulo",'info');    		
		return false;
	}
	$('#hdMdlAct').val('Factura');
	abrirModal('Factura','80','70','form_factura');
	$('#txFacTit').text(txCodArt+" - "+dsArt);
	
	$('#txFacFeIni').val($('#hdFacFDe').val());
	$('#txFacFeFin').val($('#hdFacFHa').val());
	$('.txFacBus').val('');
	$('.txFacBus').focus();
	$('#txFacFeIni').datetimepicker({
		format: 'Y-m-d',
		lang: 'es',
		onShow: function (ct) {
			this.setOptions({
				maxDate: $('#txFacFeFin').val() ? $('#txFacFeFin').val() : false
			});
		},
		timepicker			: false,
		closeOnDateSelect	: true
	});
	$('#txFacFeFin').datetimepicker({
		format: 'Y-m-d',
		lang: 'es',
		onShow: function (ct) {
			this.setOptions({
				maxDate: $('#txFacFeIni').val() ? $('#txFacFeIni').val() : false
			});
		},
		timepicker			: false,
		closeOnDateSelect	: true
	});

	retTablaFactura();
}

//Carga la informacion de faturas asociadas al producto en una tabla
function retTablaFactura(){
	var datos 	= "";
	var codFact = $('.txFacBus').val();
	var codArt  = $('#hdCodigo').val();
	var feDesde = $('#txFacFeIni').val();
	var feHasta = $('#txFacFeFin').val();
	var url1 	= $('#txt_urlGe').val();

	if(codFact.indexOf("%")>-1){
		codFact = codFact.replace(/%/,"||");
	}

	datos = datos+"c_cf="+codFact; 
	datos = datos+"&c_ca="+codArt;
	datos = datos+"&c_fd="+feDesde;
	datos = datos+"&c_fh="+feHasta;
	
	$('.contTableFactura').html('<button class="btn btn-lg bg-green">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaFactura',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if(data){
					$('.contTableFactura').html('');
					var row = '<table id="table_factura" class="table table-bordered table-hover text-size-0-5 no-padding no-margin text-bold text-black">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#00539F; color:white;">No.Fact.</th>';
					row = row+'<th style="background-color:#00539F; color:white;">Proveedor</th>';
					row = row+'<th style="background-color:#00539F; color:white;">Fecha Ing.</th>';
					row = row+'<th style="background-color:#00539F; color:white;">Cantidad</th>';
					row = row+'<th style="background-color:#00539F; color:white;">Costo M/E</th>';
					row = row+'<th style="background-color:#00539F; color:white;">Moneda</th>';
					row = row+'<th style="background-color:#00539F; color:white;">Costo USD</th>';
					row = row+'<th style="background-color:#00539F; color:white;">Fabricante</th>';
					row = row+'<th style="background-color:#00539F; color:white;">Fábrica</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						$.each(data,function(e,dato){
							row = row+'<tr>';
							row = row+'<td style="text-align:right;text-decoration: underline;"><a href="#" rel="popover" data-trigger="focus" data-popover-content="#list-popover" class="'+
							dato.co_factura+"|"+
							dato.secu_pedi+"|"+
							dato.consolidado+"|"+
							dato.tipo_factura+"|"+
							dato.nume_pedi+"|"+
							dato.co_bodega_pica+"|"+
							dato.codi_clie+
							'"><span class="text-size-0-5">'+dato.co_factura+'</span></a></td>';
							row = row+'<td style="text-align:right">'+dato.no_proveedor+'</td>';
							row = row+'<td style="text-align:right">'+dato.fe_ingreso+'</td>';
							row = row+'<td style="text-align:right">'+dato.cantidad+'</td>';
							row = row+'<td style="text-align:right">'+parseFloat(dato.costo_m_e).toFixed(2)+'</td>';
							row = row+'<td style="text-align:right">'+dato.ds_moneda+'</td>';
							row = row+'<td style="text-align:right">'+parseFloat(dato.costo_suc).toFixed(2)+'</td>';
							row = row+'<td style="text-align:right">'+dato.co_fabrica+'</td>';
							row = row+'<td style="text-align:right">'+dato.co_fabricante+'</td>';
							row = row+'</tr>';
						});
					}

					row = row+'</tbody>';
					row = row+'</table>';
					$('.contTableFactura').append(row);
					$('#table_factura').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvFacCont').height()-$('#dvFacBar').height()-$('#dvFacBus').height()-$('#dvFacTit').height()-51, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []
					} );
					$('[rel="popover"]').popover({
						html: true,
						content: function () {
							var datos = $(this).attr("class");
							var arr = datos.split("|");
							$('#tbRowFac1').text(arr[0]);
							$('#tbRowFac2').text(arr[1]);
							$('#tbRowFac3').text(arr[2]);
							$('#tbRowFac4').text(arr[3]);
							$('#tbRowFac5').text(arr[4]);
							$('#tbRowFac6').text(arr[5]);
							$('#tbRowFac7').text(arr[6]);
							var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
							return clone;
						}
					});
				}
			}else{
				$('.contTableFactura').html(returnData);
			}
		}

	});
}

/*********************************************************************************************************/
/********************************************  P E D I D O S  ********************************************/
/*********************************************************************************************************/
//Invoca al formulario modal de PEDIDOS
function cargarPedido(){
	var txCodArt = $('#hdCodigo').val();
	var dsArt 	 = $('.txDesArt').val();
	if(txCodArt==''){
		errorGeneral("Sistema Intranet","Debe ingresar el código del articulo",'info');    		
		return false;
	}
	$('#hdMdlAct').val('Pedido');
	abrirModal('Pedido','80','70','form_pedido');
	$('#txPedTit').text(txCodArt+" - "+dsArt);
	$('#txPedFeIni').val($('#hdPedFDe').val());
	$('#txPedFeFin').val($('#hdPedFHa').val());
	$('.txPedBus1').val('');
	$('.txPedBus1').focus();
	$('.cbPedBus').val(0).trigger('change');
	$('.cbPedAlm').val(0).trigger('change');
	$('#txPedFeIni').datetimepicker({
		format: 'Y-m-d',
		lang: 'es',
		onShow: function (ct) {
			this.setOptions({
				maxDate: $('#txPedFeIni').val() ? $('#txPedFeIni').val() : false
			});
		},
		timepicker			: false,
		closeOnDateSelect	: true 
	});
	$('#txPedFeFin').datetimepicker({
		format: 'Y-m-d',
		lang: 'es',
		onShow: function (ct) {
			this.setOptions({
				maxDate: $('#txPedFeFin').val() ? $('#txPedFeFin').val() : false
			});
		},
		timepicker			: false,
		closeOnDateSelect	: true
	});
	retTablaPedido();
}

//Carga la informacion de faturas asociadas al producto en una tabla
function retTablaPedido(){
	var datos 	= "";
	var codBod = $('.cbPedAlm').find(":selected").val();
	var codArt = $('#hdCodigo').val();
	var feDesd = $('#txPedFeIni').val();
	var feHast = $('#txPedFeFin').val();
	var condic = getCondicionPedido();

	var url1   = $('#txt_urlGe').val();

	datos = datos+"c_cb="+codBod; 
	datos = datos+"&c_ca="+codArt;
	datos = datos+"&c_fd="+feDesd;
	datos = datos+"&c_fh="+feHast;
	datos = datos+"&c_cn="+condic;

	$('.contTablePedido').html('<button class="btn btn-lg bg-yellow">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaPedido',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if(data){
					var estado  = "";
					var bckgColorRow = "";

					$('.contTablePedido').html('');
					var row = '<table id="table_pedido" class="table table-bordered table-hover text-size-0-5 no-padding no-margin text-bold">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#d55c23; color:white;">No.Pedido</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Tipo</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Estado</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Pedido A</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Pedido Por</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Cant.Ped.</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Fecha Ped.</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Tiempo Picking</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Salida Camión</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Llegada Camión</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Recibido</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Tiempo Total</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						$.each(data,function(e,dato){
							switch(dato.st_estado){
								case "P": 	estado = "Pendiente";
											bckgColorRow = "background-color:#FFCCD0";
										break;
								case "D": 	estado = "Recolectado";
											bckgColorRow = "background-color:#FAFFD8";
										break;
								case "C": 	estado = "Consolidado";
											bckgColorRow = "background-color:#3977B5;";
										break;
								case "R": 	estado = "Recibido";	
											bckgColorRow = "background-color:#B9DFBE";
										break;
								case "A": 	estado = "Anulado";
											bckgColorRow = "";
										break;
								case "O": 	estado = "Pendi.Consolidado";
											bckgColorRow = "";
										break;
								case "I": 	estado = "Reclamo";
											bckgColorRow = "";
										break;
								case "F": 	estado = "Pedido Manual";
											bckgColorRow = "background-color:#B7DBFF";
										break;
								case "L": 	estado = "Pedido Liquidado";
											bckgColorRow = "background-color:#3977B5;";
										break;
							}
							row = row+'<tr style="'+bckgColorRow+'">';
							row = row+  '<td style = "text-align:right;">'+
											'<a style="color:#d55c23;text-decoration: underline;" href="javascript:void(0)"'+
												' onclick="detallePedido('+"'"	+	dato.codigo 	
																		  +"','"+ dato.co_bode_s	
																		  +"','"+ dato.co_bode_e	
																		  +"','"+ dato.id_picking	
																		  +"','"+dato.bodeen+"'"+')">'+
												'<span class="text-size-0-5">'
												+dato.codigo+
												'</span>'+
											'</a>'+
										'</td>';
							row = row+'<td style="text-align:center">'+dato.tipo+'</td>';
							row = row+'<td style="text-align:right">' +estado+'</td>';
							row = row+'<td style="text-align:right">' +dato.bodesa+'</td>';
							row = row+'<td style="text-align:right">' +dato.bodeen+'</td>';
							row = row+'<td style="text-align:right">' +dato.cantpedido+'</td>';
							row = row+'<td style="text-align:right">' +dato.fe_pedido+'</td>';
							row = row+'<td style="text-align:right">' +dato.despacho+'</td>';
							row = row+'<td style="text-align:right">' +dato.camion_sale+'</td>';
							row = row+'<td style="text-align:right">' +dato.camion_llega+'</td>';
							row = row+'<td style="text-align:right">' +dato.recibido+'</td>';
							row = row+'<td style="text-align:right">' +dato.tiempo_total+'</td>';
							row = row+'</tr>';
						});
					}

					row = row+'</tbody>';
					row = row+'</table>';
					$('.contTablePedido').append(row);
					$('#table_pedido').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvPedCont').height()-$('#dvPedBar').height()-$('#dvPedBus').height()-$('#dvPedTit').height()-51, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []
					} );
				}
			}else{
				$('.contTablePedido').html(returnData);
			}
		}

	});
}

function detallePedido(pedido,bodSal,bodEnt,idPick,nombBodEnt){
	$('#hdMdlAct').val('Pedido>DetPedido');
	abrirModal('DetPedido','70','60','form_detpedido');
	$('.lbDetPedNP').text(pedido);
	$('.lbDetPedDE').text(nombBodEnt);
	$('.lbDetPedPP').text('');
	$('.lbDetPedPN').text('');
	$('.lbDetPedRQ').text('');
	$('.lbDetPedFD').text('');
	$('.lbDetPedCM').text('');
	$('.contTableDetPedido').html('<button class="btn btn-lg bg-yellow">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	var datos 	= "";
	var codPed = pedido.split('-');
	var cdBodS = bodSal;
	var cdBodE = bodEnt;
	var idPick = idPick;

	var url1   = $('#txt_urlGe').val();

	datos = datos+"c_cp="+codPed[0]; 
	datos = datos+"&c_bs="+cdBodS;
	datos = datos+"&c_be="+cdBodE;
	datos = datos+"&c_pk="+idPick;

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaDetallePedido',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if(data){

					$('.contTableDetPedido').html('');
					var row = '<table id="table_det_pedido" class="table table-bordered table-hover table-striped text-size-0-5 no-padding no-margin text-bold">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#d55c23; color:white;">Código</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Línea</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Artículo</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Pedido</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Despacho</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Recibido</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Fábrica</th>';
					row = row+'<th style="background-color:#d55c23; color:white;">Nom.Bulto</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						$.each(data,function(e,dato){
							$('.lbDetPedPP').text(dato.co_usuario);
							$('.lbDetPedPN').text(dato.tx_nombre);
							$('.lbDetPedRQ').text(dato.no_pedido_por);
							$('.lbDetPedFD').text(dato.fe_recepcion);
							$('.lbDetPedCM').text(dato.nota);
							row = row+'<tr>';
							row = row+'<td style="text-align:center">'+dato.co_articulo+'</td>';
							row = row+'<td style="text-align:left">'  +dato.ds_grupo+'</td>';
							row = row+'<td style="text-align:left">'  +dato.ds_articulo+'</td>';
							row = row+'<td style="text-align:right">' +dato.cantpedido+'</td>';
							row = row+'<td style="text-align:right">' +dato.cantdespachado+'</td>';
							row = row+'<td style="text-align:right">' +dato.cantrecepcion+'</td>';
							row = row+'<td style="text-align:right">' +dato.co_fabrica+'</td>';
							row = row+'<td style="text-align:right">' +dato.no_bulto+'</td>';
							row = row+'</tr>';
						});
					}

					row = row+'</tbody>';
					row = row+'</table>';
					$('.contTableDetPedido').append(row);
					$('#table_det_pedido').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvDetPedCont').height()-$('#dvDetPedBar').height()-$('#dvDetPedCab').height()-51, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []
					} );
				}
			}else{
				$('.contTableDetPedido').html(returnData);
			}
		}

	});
}

function getCondicionPedido(){
	var retcond = "";
	var camp = "a.co_noped";
	var cond = $(".cbPedBus").find(":selected").text();
	var val1 = $(".txPedBus1").val().toUpperCase();
	var val2 = $(".txPedBus2").val().toUpperCase();
	var betw = false;
	if(val1!=""){
		if(cond.indexOf("Entre")>-1){
			cond = cond.replace("Entre","BETWEEN");
			betw = true;
		}
		if(!betw){
			retcond += " AND ("+camp+" "+cond+" "+val1+") ";
		}else{
			if(val2==""){
				errorGeneral("Sistema Intranet","Debe ingresar un valor","info");
				$(".txPedBus2").focus();
				return retcond;
			}
			retcond += " AND ("+camp+" "+cond+" "+val1+" AND "+val2+") ";
		}
	}
	return retcond;
}


/*********************************************************************************************************/
/************************************** M O V I M I E N T O S ********************************************/
/*********************************************************************************************************/
//Invoca al formulario modal de MOVIMIENTOS
function cargarMovimiento(){
	var txCodArt = $('#hdCodigo').val();
	var dsArt 	 = $('.txDesArt').val();
	if(txCodArt==''){
		errorGeneral("Sistema Intranet","Debe ingresar el código del articulo",'info');    		
		return false;
	}
	$('#hdMdlAct').val('Movimiento');
	abrirModal('Movimiento','80','70','form_movimiento');
	$('#txMovTit').text(txCodArt+" - "+dsArt);
	$('#txMovFeIni').val($('#hdMovFDe').val());
	$('#txMovFeFin').val($('#hdFacFHa').val());
	$('.cbMovAlm').val(0).trigger('change');
	$('.cbMovTip').val(0).trigger('change');
	$('#chkMovTot').prop('checked', false);
	$('#txMovFeIni').datetimepicker({
		format: 'Y-m-d',
		lang: 'es',
		onShow: function (ct) {
			this.setOptions({
				maxDate: $('#txMovFeIni').val() ? $('#txMovFeIni').val() : false
			});
		},
		timepicker			: false,
		closeOnDateSelect	: true 
	});
	$('#txMovFeFin').datetimepicker({
		format: 'Y-m-d',
		lang: 'es',
		onShow: function (ct) {
			this.setOptions({
				maxDate: $('#txMovFeFin').val() ? $('#txMovFeFin').val() : false
			});
		},
		timepicker			: false,
		closeOnDateSelect	: true
	});
	$('#hdClkHis').val('0');
	retTablaMovimiento();
}

function retTablaMovimiento(){
	var datos 	= "";
	var codBod = $('.cbMovAlm').find(":selected").val();
	var tipMov = $('.cbMovTip').find(":selected").val();
	var codArt = $('#hdCodigo').val();
	var feDesd = $('#txMovFeIni').val();
	var feHast = $('#txMovFeFin').val();
	var condic = '';

	var url1   = $('#txt_urlGe').val();
 	
 	if(codBod!="0"){
 		condic = condic + " AND (a.co_bodega_entra = "+codBod+" or a.co_bodega_sale = "+codBod+" ) ";
 	}

 	if(tipMov!="0"){
 		condic = condic + " AND a.co_tipo = ''"+tipMov+"'' ";
 	}

	datos = datos+"&c_ca="+codArt;
	datos = datos+"&c_fd="+feDesd;
	datos = datos+"&c_fh="+feHast;
	datos = datos+"&c_cn="+condic;
	
	$('#chkMovTot').prop('checked', false);

	$('.contTableMov').html('<button class="btn btn-lg bg-green2">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaMovimiento',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if(data){
					var estado  = "";
					var bckgColorRow = "";
					$('#hdClkHis').val('0');
					$('.contTableMov').html('');
					var row = '<table id="table_movimiento" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#5C954A; color:white;">Tipo</th>';
					row = row+'<th style="background-color:#5C954A; color:white;"></th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Movimiento</th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Cant.</th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Fecha</th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Caja/Doc.</th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Entra</th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Sale</th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Usuario</th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Nombre Usuario</th>';
					row = row+'<th style="background-color:#5C954A; color:white;">Cargo</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						$.each(data,function(e,dato){
							var enlace ="";
							if(dato.co_bodega_sale!=""){
								switch(dato.co_tipo_mov){
									case "FC":
									case "DE":
									case "FM":
									case "L" :
										enlace ='<a href="javascript:void(0)"'+
												'onclick="retTablaCostoVenta('+"'"+dato.fe_movto+"'"
												+",'"+codArt+"'"
												+",'"+dato.co_bodega_sale+"'"
												+",'"+dato.co_nota+"'"
												+",'"+dato.co_tipo_mov+"'"
												+')"'+'><i class="fa fa-search-plus"></i></a>';
									break;
									case "1" :
										enlace ='<a href="javascript:void(0)"'+
												'onclick="genIngFacPDF('+"'"+dato.co_nota+"'"+')"'+'><i class="fa fa-bar-chart"></i></a>';
									break;
								}
							}
							
							row = row+'<tr>';
							row = row+'<td style="text-align:center" class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">'	+dato.co_tipo_mov+'</td>';
							row = row+'<td style="text-align:center" class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">'	+enlace+'</td>';
							row = row+'<td style="text-align:left" 	 class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">' 	+dato.ds_tipo_mov+'</td>';
							row = row+'<td style="text-align:right"  class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">'	+parseFloat(dato.cantidad).toFixed(0)+'</td>';
							row = row+'<td style="text-align:right"  class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">' 	+dato.fe_movto+'</td>';
							row = row+'<td style="text-align:right"  class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">' 	+dato.co_nota+'</td>';
							row = row+'<td style="text-align:left" 	 class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">' 	+dato.bode_entra+'</td>';
							row = row+'<td style="text-align:left" 	 class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">' 	+dato.bode_sale+'</td>';
							row = row+'<td style="text-align:right"  class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">' 	+dato.co_usuario+'</td>';
							row = row+'<td style="text-align:left" 	 class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">' 	+dato.tx_nombre+'</td>';
							row = row+'<td style="text-align:right"  class="data grp'+dato.ds_tipo_mov.replace(/ /gi,'_')+'">' 	+dato.tx_cargo+'</td>';
							row = row+'</tr>';
						});
					}

					row = row+'</tbody>';
					row = row+'</table>';
					$('.contTableMov').append(row);
					$('#table_movimiento').dataTable({
						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvMovCont').height()-$('#dvMovBar').height()-$('#dvMovBus').height()-$('#dvMovTit').height()-51, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": []				        
					});
				}
			}else{
				$('.contTableMov').html(returnData);
			}
		}

	});
}

function retHistMov(){
	var txCodArt = $('#hdCodigo').val();
	var datos 	 = "";
	var url1     = $('#txt_urlGe').val();
	var clickHis = $('#hdClkHis').val();

	if(clickHis=="1"){
		return;
	}

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaHisMov',
		data : "c_ca="+txCodArt,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if(data){
					var tablaMov = $('#table_movimiento').html();
					var row = "";
					if(!tablaMov){
						$('.contTableMov').html('');
						row = '<table id="table_movimiento" class="table table-bordered table-hover text-size-0-5 no-padding no-margin text-bold">';
						row = row+'<thead>';
						row = row+'<th style="background-color:#5C954A; color:white;">Tipo</th>';
						row = row+'<th style="background-color:#5C954A; color:white;"></th>';
						row = row+'<th style="background-color:#5C954A; color:white;">Movimiento</th>';
						row = row+'<th style="background-color:#5C954A; color:white;">cantidad</th>';
						row = row+'<th style="background-color:#5C954A; color:white;">fecha</th>';
						row = row+'<th style="background-color:#5C954A; color:white;">Caja/Doc.</th>';
						row = row+'<th style="background-color:#5C954A; color:white;">Entra</th>';
						row = row+'<th style="background-color:#5C954A; color:white;">Sale</th>';
						row = row+'<th style="background-color:#5C954A; color:white;">Usuario</th>';
						row = row+'<th style="background-color:#5C954A; color:white;">Nombre Usuario</th>';
						row = row+'<th style="background-color:#5C954A; color:white;">Cargo</th>';
						row = row+'</thead>';
						row = row+'<tbody>';
						row = row+'</tbody>';
						row = row+'</table>';
						$('.contTableMov').append(row);
					}

					row = "";
					$.each(data,function(e,dato){
						row = row+'<tr>';
						row = row+'<td style="background-color:#F5EDB2;text-align:center">'	+dato.co_tipo_mov+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:center">	</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:left">' 	+dato.ds_tipo_mov+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:right">'	+parseFloat(dato.cantidad).toFixed(0)+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:right">' 	+dato.fe_movto+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:right">' 	+dato.co_nota+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:left">' 	+dato.bode_entra+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:left">' 	+dato.bode_sale+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:right">' 	+dato.co_usuario+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:left">' 	+dato.tx_nombre+'</td>';
						row = row+'<td style="background-color:#F5EDB2;text-align:right">' 	+dato.tx_cargo+'</td>';
						row = row+'</tr>';
					})
					$('#table_movimiento tbody').append(row);
					if(!tablaMov){
						$('#table_movimiento').dataTable({
							"bPaginate"	: false, 
							"bFilter"	: false, 
							"bInfo" 	: false,
							"sScrollY"	: $('#dvMovCont').height()-$('#dvMovBar').height()-$('#dvMovBus').height()-$('#dvMovTit').height()-51, 
							"sScrollX"	: "95%", 
							"sScrollXInner": "95%", 
							"order": []
						});
					}
					$('#hdClkHis').val('1');
				}
			}else{
				errorGeneral("Sistema Intranet",returnData,"info");
			}
		}

	});
}

function retTablaCostoVenta(fdes,cart,cbod,ccaj,tmov){
	var txCodArt = $('#hdCodigo').val();
	var dsArt 	 = $('.txDesArt').val();
	var datos 	= "";
	var url1   = $('#txt_urlGe').val();

	$('#hdMdlAct').val('Movimiento>CostoVta');
	abrirModal('CostoVta','60','50','form_costovta');
	$('#txCvtTit').text("Documentos de "+txCodArt+" - "+dsArt);

	datos = datos+"&c_fd="+fdes;
	datos = datos+"&c_fh="+fdes;
	datos = datos+"&c_ca="+cart;
	datos = datos+"&c_cb="+cbod;
	datos = datos+"&c_cc="+ccaj;
	datos = datos+"&c_tm="+tmov;

	$('.contTableCvt').html('<button class="btn btn-lg bg-aqua">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	$.ajax({
		type : 'POST',
		url  : url1+'inventario/ConsultaArticulo/retTablaXlsCostoVentasPvp',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				if(data){
					var estado  = "";
					var bckgColorRow = "";

					$('.contTableCvt').html('');
					var row = '<table id="table_costo_venta" class="table table-bordered table-hover text-size-0-5 no-padding no-margin text-bold">';
					row = row+'<thead>';
					row = row+'<th style="background-color:#00c0ef; color:white;">Tienda</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">Tipo</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">Fecha</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">Bodega</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">Caja</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">Numero</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">Cantidad</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">PVP Unit.</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">PVP+Iva</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">%Desc</th>';
					row = row+'<th style="background-color:#00c0ef; color:white;">Subtotal</th>';
					row = row+'</thead>';
					row = row+'<tbody>';

					if (data){
						$.each(data,function(e,dato){							
							row = row+'<tr>';
							row = row+'<td style="text-align:center">'	+dato.descripcion+'</td>';
							row = row+'<td style="text-align:center">'	+dato.tipo+'</td>';
							row = row+'<td style="text-align:center">' 	+dato.fe_venta+'</td>';
							row = row+'<td style="text-align:right">'	+dato.co_bodega+'</td>';
							row = row+'<td style="text-align:right">' 	+dato.co_caja+'</td>';
							row = row+'<td style="text-align:right">' 	+dato.no_documento+'</td>';
							row = row+'<td style="text-align:right">' 	+dato.cantidad+'</td>';
							row = row+'<td style="text-align:right">' 	+parseFloat(dato.pvp_unit).toFixed(2)+'</td>';
							row = row+'<td style="text-align:right">' 	+parseFloat(dato.valor_pvp).toFixed(2)+'</td>';
							row = row+'<td style="text-align:right">' 	+((dato.valor_descuento!="")?parseFloat(dato.valor_descuento).toFixed(2):"")+'</td>';
							row = row+'<td style="text-align:right">' 	+parseFloat(dato.venta_pika).toFixed(2)+'</td>';
							row = row+'</tr>';
						});
					}

					row = row+'</tbody>';
					row = row+'<tfoot>';
					row = row+'<tr>';
					row = row+'<th colspan="3" style="text-align:right">Grand Total:</th>';
					row = row+'<th style="text-align:right" id="fTotal"></th>';
					row = row+'</tr>';
					row = row+'</tfoot>';
					row = row+'</table>';
					var total;
					$('.contTableCvt').append(row);
					$('#table_costo_venta').dataTable({

						"bPaginate"	: false, 
						"bFilter"	: false, 
						"bInfo" 	: false,
						"sScrollY"	: $('#dvCvtCont').height()-$('#dvCvtBar').height()-$('#dvCvtTit').height()-51, 
						"sScrollX"	: "95%", 
						"sScrollXInner": "95%", 
						"order": [],
						"footerCallback": function ( row, data, start, end, display ) {
							var api = this.api(), data;
				            // Remove the formatting to get integer data for summation
				            var intVal = function ( i ) {
				                return typeof i === 'string' ?
				                    i.replace(/[\$,]/g, '')*1 :
				                    typeof i === 'number' ?
				                        i : 0;
				            };
				            // Total over all pages
				            total = api
				                .column( 10 )
				                .data()
				                .reduce( function (a, b) {
				                    return intVal(a) + intVal(b);
				                }, 0 );
				             // Update footer
					            $('#fTotal').html(
					                '$'+ total 
					            );
						}
					} );
					//alert(total);
				}
			}else{
				$('.contTableCvt').html(returnData);
			}
		}

	});
}

//Selecciona con el mouse el contenido de un elemento
function selectElementContents(el) {
	var body = document.body, range, sel;
	if (document.createRange && window.getSelection) {
		range = document.createRange();
		sel = window.getSelection();
		sel.removeAllRanges();
		try {
			range.selectNodeContents(el);
			sel.addRange(range);
		} catch (e) {
			range.selectNode(el);
			sel.addRange(range);
		}
	} else if (body.createTextRange) {
		range = body.createTextRange();
		range.moveToElementText(el);
		range.select();
	}
}

function abrirModal(nameModal, ancho, alto, form) {
    $("#fade" + nameModal + "").remove();
    var Top = 100 - parseInt(alto);
    var diTop = Top / 2;
    var a = document.createElement('div');
    a.setAttribute("id", "fade" + nameModal);
    a.setAttribute("class", "fadebox");
    $("body").append(a);
    $("#fade" + nameModal).css('display', 'block');
    $("#modal" + nameModal).css('top', diTop + '%');
    $("#modal" + nameModal).css('bottom', diTop + '%');
    $("#modal" + nameModal).css('width', ancho + '%');
    $("#modal" + nameModal).css('height', alto + '%');
    $("#modal" + nameModal).css('display', 'block');

}

//Permite filtrar solo numerros en las cajas de texto
function soloNumeros(e) {
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
		(e.keyCode >= 35 && e.keyCode <= 40)) {
		return;
}
if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	e.preventDefault();
}
}

//Retorna 	1: Si el Tab Pycca esta activo
//  		2: Si el tab Polipapel esta activo
function retActiveMainTab(){
	var tab = "1";
	if($('#2a').hasClass('active')){
		tab = "2";
	}
	return tab;
}

//Funcion para copiar el contenido de una tabla en el portapapeles
function copiarEnPortaPapeles(content){
	selectElementContents(document.getElementById(content));
    	if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
    		document.execCommand("copy");
    		errorGeneral("Sistema Intranet","¡¡ Copiado en el portapapeles !!","info")
    	}
}

function genMovPDF(){
    var url1 = $('#txt_urlGe').val();
    var codBod = $('.cbMovAlm').find(":selected").val();
	var tipMov = $('.cbMovTip').find(":selected").val();
	var codArt = $('#hdCodigo').val();
	var feDesd = $('#txMovFeIni').val();
	var feHast = $('#txMovFeFin').val();
	var clickHis = $('#hdClkHis').val();

	var condic = '';
 	
 	if(codBod!="0"){
 		condic = condic + " AND (a.co_bodega_entra = "+codBod+" or a.co_bodega_sale = "+codBod+" ) ";
 	}

 	if(tipMov!="0"){
 		condic = condic + " AND a.co_tipo = ''"+tipMov+"'' ";
 	}

 	var params = [];
	params.push({
			c_codart 		: codArt,
			c_fdesde		: feDesd,
			c_fhasta		: feHast,
			c_condic		: condic,
			c_histor		: clickHis
		});
	
	var mapForm = document.createElement("form");
	mapForm.target = "_blank";    
	mapForm.method = "POST";
	mapForm.action = url1+'inventario/ConsultaArticulo/genMovPDF/PDF-'+codArt;
	var mapInput = document.createElement("input");
	mapInput.type = "text";
	mapInput.name = "parameter";
	mapInput.value = JSON.stringify(params);
	mapForm.appendChild(mapInput);
	document.body.appendChild(mapForm);
	mapForm.submit();
	document.body.removeChild(mapForm);
}

function genIngFacPDF(cdFact){
    var url1 = $('#txt_urlGe').val();
	var codProv = $('#hdProvee').val();
	window.open(url1+'inventario/ConsultaArticulo/genIngFacPDF/'+cdFact+'/'+codProv+'/Factura '+cdFact);
}

//Funcion que retorna el detalle de un articulo dependiendo del codigo
function retDetalleArt(codigo){
	var url = $('#txt_urlGe').val();
	var datos = "";
	if(codigo){
		datos = datos+"c_cd="+codigo;
		$.ajax({
			type : 'POST',
			url  : url+"inventario/ConsultaArticulo/retConsArt", 
			data : datos,
			success : function (returnData) {
				if (returnData.trim().substr(0,2) === "OK"){
					var data = eval(returnData.trim().substr(3));
					if (data){
						$.each(data,function(e,dato){
							var tipBus = $('#hdTipBus').val();

							//Lleno los selects de la estructura comercial
							defaulec=dato.co_linea;
							$('.cbGru').val(dato.co_grupo).trigger('change');
							linea = dato.co_linea;
							defaulec=dato.co_sublinea;
							$('.cbLin').val(dato.co_linea).trigger('change');
							sublinea = dato.co_sublinea;
							defaulec=dato.co_categoria;
							$('.cbSbl').val(dato.co_sublinea).trigger('change');
							categoria = dato.co_categoria;
							defaulec=dato.co_subcategoria;
							$('.cbCat').val(dato.co_categoria).trigger('change');

							//Seteo el estilo del ranking
							if(dato.ds_ranking=='A' || dato.ds_ranking=='AA' || dato.ds_ranking=='AAA') 
								$('#pRank').attr("class"," text-size-2 text-bold no-padding no-margin text-blue");
							if(dato.ds_ranking=='B') $('#pRank').attr("class","text-size-2 text-bold no-padding no-margin text-success");
							if(dato.ds_ranking=='C') $('#pRank').attr("class","text-size-2 text-bold no-padding no-margin text-black");
							if(dato.ds_ranking=='D') $('#pRank').attr("class","text-size-2 text-bold no-padding no-margin text-orange");
							if(dato.ds_ranking=='E') $('#pRank').attr("class","text-size-2 text-bold no-padding no-margin text-red");
							if(dato.ds_ranking=='F') $('#pRank').attr("class","text-size-2 text-bold no-padding no-margin text-red");
							//Lleno información basica del producto
							$('.txCodArt').val(dato.co_articulo);
							$('.txDesArt').val(dato.ds_articulo);
							$('.imgArt').attr("src","../lib/loadImg.php?codigo="+dato.co_articulo.substring(0,5)+".jpg");
							$('.txCodFab').val(dato.co_fabrica);
							$('.txUpc').val(dato.upc);
							$('.lbOfe').text("$ "+parseFloat(dato.pvp_oferta_iva).toFixed(2));								
							$('#hOfeDes').text(dato.fe_odesde);
							$('#hOfeHas').text(dato.fe_ohasta);
							$('#hdProvee').val(dato.co_proveedor);
							$('#pPvp').text("$"+parseFloat(dato.pvp).toFixed(2));
							$('#pPvpIva').text("$"+parseFloat(dato.pvpiva).toFixed(2));
							$('#hPerDes').text(dato.descuento+" %");
							$('#pRank').text(dato.ds_ranking);
							$('.lbFecCre').text(dato.fe_creacion);
							$('.lbEmp').text(dato.empaque);
							$('.lbUltFac').text(dato.co_factura);
							$('.lbUndIng').text(dato.und_factura);
							$('.lbFecUltIng').text(dato.fe_ult_factura);
							$('.lbIngTot').text('('+dato.canti+') '+dato.sumatotal);
							$('.lbCosMonExt').text(parseFloat(dato.costo_m_e).toFixed(2)+" USD");
							$('.lbUltCos').text(parseFloat(dato.costo_suc).toFixed(2)+" USD");
							$('.lbCosPro').text(parseFloat(dato.costop).toFixed(2)+" USD");
							$('.lbBasCom').text(parseFloat(dato.base_comercial).toFixed(2)+" USD");
							$('.lbFab').text(dato.co_fabricante);
							$('.lbPro').text(dato.proveedor+" - "+dato.pais);
							$('.lbObs').text(dato.ds_observacion);
							$('.lbGar').text(dato.garantia);

							//Si es de oferta cambio el estilo del precio
							if(dato.oferta_activa=='1'){
								$('.lbOfe').attr("class","text-size-1-25 label text-bold text-strong-red lbOfe");
								$('#pPvpIva').attr("class","text-primary text-size-2 no-padding no-margin");
								$('.dvOfe').attr("class","text-size-1 label label-warning dvOfe");
							}
							else{
								$('.lbOfe').attr("class","text-size-1-25 label text-blue lbOfe");
								$('#pPvpIva').attr("class","text-blue text-size-2 text-bold no-padding no-margin");
								$('.dvOfe').attr("class","text-size-1 label label-info dvOfe");
							}
							//Reseteo las variables globales auxiliares
							grupo 		= -1;
							linea 		= -1;
							sublinea 	= -1;
							categoria 	= -1;
							subcategoria = -1;
							defaulec 	= -1;
							//Guardo el tipo de busqueda que hizo el usuario
							$('#hdCodigo').val($('.txCodArt').val());
							//Si el proveedor es PICA Plasticos mostrar info de pedidos
							if(dato.co_proveedor=="990001243001"){
								$('.dvPed').css("display","block");
							}else{
								$('.dvPed').css("display","none");
							}
							//Consulto la rotacion del articulo
							retRotacion();
						});
					}else {
						errorGeneral("Sistema Intranet",returnData,'danger');
					}
				}
			}
		});
	}
}