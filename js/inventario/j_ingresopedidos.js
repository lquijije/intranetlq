var arrArt = [];

$(iniciar);
// Inicio del documento
function iniciar(){
	$(document).keyup(function (event) {
        //Tecla Escape
        if (event.which === 27)
        {
        	var nameModal = $('#hdMdlAct').val();
        	if(nameModal!="")        		quitarModal();
        }
    });
    // KEY PRESS de todos los <input texts>
    $("input[type=text]").keyup(evtInput);
    //Click de los tabs
    $('[data-toggle="tab"]').on('shown.bs.tab', evtTabs);
    $(".txCodArt").focusout(function() {
    	var cdArt = $('.txCodArt').val();
    	retConsArt(cdArt);
    });
    // CLICK Boton <Buscar Bodega Pide>
    $(".btBPide").button().click(agrBodPide);
	// CLICK Boton <Buscar Motivo traslado>
	$(".btMotTra").button().click(agrMotivo);
	// CLICK Boton <Buscar Bodega A pedir>
	$(".btBodA").button().click(agrBodA);
	// CLICK Boton <Buscar articulo>
	$(".btBusArt").button().click(agrArt);
	// CLICK Boton Buscar artículo 
	$( ".btBusB" ).button().click(buscarArticulo);
	// CLICK Boton Ingresar artículo para pedido
	$( ".btIng" ).button().click(ingArticulo);
	// CLICK Boton para grabar el pedido
	$( ".btProc" ).button().click(grabaPedido);
	// Todos los inputs con mayuscula
	$("input[type=text]").css("text-transform","uppercase");
	// CLICK Aprobar prepedido
	$('.btApr').button().click(fnAprobar);
	// KEY DOWN Solo Numeros
	$(".txCant").keydown(soloNumeros);
	// Por defaul se pide la bodega que despacha o a quien se pide.
	agrBodPide();
}

//Evento que controla todos los KEYS PRESS de los input texts
function evtInput(e){
	var isTxBusBar = $(this).hasClass('txBusB');
	var isTxCdAr   = $(this).hasClass('txCodArt');
	var isTxCant   = $(this).hasClass('txCant');
	if(e.keyCode==13){
		if(isTxBusBar){
			buscarArticulo();
		}
		if(isTxCdAr){
			var cdArt = $('.txCodArt').val();
			retConsArt(cdArt);
		}
		if(isTxCant){
			ingArticulo();
		}
	}
}

function limpiar(){
	$("label[class*='lb']").text('');
	$("input[type=text]").val('');
	$('#hdBodPid').val('');
	$('#hdBodA').val('');
	$('#hdMotivo').val('');
	$('#hdTipBod').val('');
	$('#hdDspAr').val('');
	$('#hdSalArt').val('');
	$('#table_lst_pedidos tbody tr').remove();
	$('#table_lst_pedidos').dataTable().fnDestroy();
	$('#taObs').val('');
	arrArt = [];
}

// Evento que captura cual tab se dio clic 
function evtTabs(e){
	var target = $(e.target).attr("href")
	// Tab Pedidos 
	if(target=="#pnlPed"){
		var regPed = $('#table_pedidos tbody tr').length;
		if(regPed>0){
			$('#table_pedidos').dataTable().fnDestroy();
			$('#table_pedidos').dataTable({
				"bPaginate"	: false, 
				"bFilter"	: false, 
				"bInfo" 	: false,
				"sScrollY"	: '20vh', 
				"sScrollX"	: "95%", 
				"sScrollXInner": "95%", 
				"order": []
			} );
		}
	}
}

function fnAprobar(){
	$('#hdMdlAct').val('AprPrePed');
	newModal('AprPrePed','60','80','form_aprpreped');
	$('.txAprBus').focus();
}

//Oculta el modal activo
function quitarModal(){
	var modals = $('#hdMdlAct').val().split('>');
	var lenModals = modals.length;
	var nameModal = modals[lenModals-1];
	modals.splice(modals.indexOf(lenModals),1);
	var resModals = modals.join('>');
	$('#hdMdlAct').val(resModals);
	quitarmodalGeneral(nameModal, '');
}

//Establece el codigo de bodega que despacha
function setBodPide(cdBod,dsBod){
	$('.lbBodPide').text(dsBod.substring(0,19));
	$('#hdBodPid').val(cdBod);
	if($('#hdMotivo').val()==''){
		agrMotivo();
	}
	retTraslados();
	retDetBod();
	retConsBodeGyeUio(cdBod,1);
}

function agrBodPide(){
	$('#hdMdlAct').val('BodPide');
	newModal('BodPide','25','50','form_bodpide');
	$('#table_pedido').dataTable().fnDestroy();
	$('#table_pedido').dataTable({
		"bPaginate"	: false, 
		"bFilter"	: false, 
		"bInfo" 	: false,
		"sScrollY"	: $('#dvBdPCont').height()-$('#dvBdPBar').height()-50, 
		"sScrollX"	: "95%", 
		"sScrollXInner": "95%", 
		"order": []
	});
}


function agrMotivo(){
	$('#hdMdlAct').val('Motivo');
	newModal('Motivo','25','40','form_motivo');
	$('#table_motivo').dataTable().fnDestroy();
	$('#table_motivo').dataTable({
		"bPaginate"	: false, 
		"bFilter"	: false, 
		"bInfo" 	: false,
		"sScrollY"	: $('#dvMotCont').height()-$('#dvMotBar').height()-50, 
		"sScrollX"	: "95%", 
		"sScrollXInner": "95%", 
		"order": [],
		"columnDefs": [ {
			"targets": [0,1],
			"orderable": false
		} ]	
	});
}

//Establece el codigo de bodega que despacha
function setMot(cdMot,dsMot){
	$('.lbMotTra').text(dsMot);
	$('#hdMotivo').val(cdMot);
	if($('#hdBodA').val()==''){
		agrBodA();
	}
	if(cdMot==1){
		$('#taObs').attr("style","resize: none;height: 50px;");
		$('#dvTipFact').attr("class","row");
	}else{
		$('#taObs').attr("style","resize: none;height: 85px;");
		$('#dvTipFact').attr("class","row hide");
	}
}

function agrBodA(){
	$('#hdMdlAct').val('BodA');
	newModal('BodA','25','50','form_boda');
	$('#table_bod_gen').dataTable().fnDestroy();
	$('#table_bod_gen').dataTable({
		"bPaginate"	: false, 
		"bFilter"	: false, 
		"bInfo" 	: false,
		"sScrollY"	: $('#dvBdACont').height()-$('#dvBdABar').height()-50, 
		"sScrollX"	: "95%", 
		"sScrollXInner": "95%", 
		"order": []
	});
}

//Establece el codigo de bodega que despacha
function setBodA(cdBod,dsBod){
	$('.lbBodA').text(dsBod);
	$('#hdBodA').val(cdBod);
	$('.txPedPor').focus();
	retConsBodeGyeUio(cdBod,2);
	if($('.txCodArt').val()!=''){
		retConsDisponibleArt($('.txCodArt').val());
	}
}

function agrArt(){
	$('#hdMdlAct').val('BusqArt');
	$('.contTableBusqueda').html('');
	$('.txBusB').val('');
	newModal('BusqArt','50','60','form_busqarrt');
	$('.txBusB').focus();
}

// Carga la informacion de un artículo en pantalla.
function retConsArt(codigo){
	var url     = $('#txt_urlGe').val();
	var datos   = "";
	var bodPide = $('#hdBodPid').val();
	var bodA 	= $('#hdBodA').val();
	var codMot  = $('#hdMotivo').val();
	if(bodPide == ''){
		errorGeneral("Sistema Intranet",'Debe ingresar la bodega que pide','danger');
		return;
	}

	if(bodA == ''){
		errorGeneral("Sistema Intranet",'Debe ingresar la bodega a quien pide','danger');
		return;
	}
	if(codMot==''){
		errorGeneral("Sistema Intranet",'Debe ingresar un motivo','danger');
		return;
	}

	if(codigo){
		datos = datos+"c_cd="+codigo;
		$.ajax({
			type : 'POST',
			url  : url+"inventario/IngresoPedidos/retConsArt", 
			data : datos,
			success : function (returnData) {
				if (returnData.trim().substr(0,2) === "OK"){
					var data = eval(returnData.trim().substr(3));
					if (data){
						$.each(data,function(e,dato){
							$('.txCodArt').val(dato.co_articulo);
							$('.lbDesArt').text(dato.ds_articulo);
							$('#hdCodFab').val(dato.co_fabrica);
							retConsDisponibleArt(dato.co_articulo);
						});
						$('.txCant').focus();
					}else {
						errorGeneral("Sistema Intranet",returnData,'danger');
					}
				}else {
					errorGeneral("Sistema Intranet",returnData,'danger');
				}
			}
		});
	}
}

//Carga los articulos buscados en el modal de busqueda
function buscarArticulo(){
	
	var datos = "";
	var busq  = $('.txBusB').val();
	var url1 = $('#txt_urlGe').val();
	
	if(busq=="")	return;
	if(busq.replace(/%/,"")=="")	return;
	
	$('.contTableBusqueda').html('<button class="btn btn-lg bg-primary">'+
		'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...'+
		'</button>');

	if(busq.indexOf("%")>-1){
		busq = busq.replace(/%/,"||");
	}

	datos = datos+"c_bu="+busq; 
	$.ajax({
		type : 'POST',
		url  : url1+'inventario/IngresoPedidos/retBusqArt',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				$('.contTableBusqueda').html('');
				var row = '<table id="table_busqueda" class="table table-bordered table-success text-size-0-5 no-padding no-margin text-bold">';
				row = row+'<thead>';
				row = row+'<th style="background-color:#428bca; color:white;">Código</th>';
				row = row+'<th style="background-color:#428bca; color:white;">Descripción</th>';
				row = row+'<th style="background-color:#428bca; color:white;">PVP+IVA</th>';
				row = row+'<th style="background-color:#428bca; color:white;">Fecha Oferta</th>';
				row = row+'<th style="background-color:#428bca; color:white;">%Desc.</th>';
				row = row+'<th style="background-color:#428bca; color:white;">PVP_Oferta</th>';
				row = row+'</thead>';
				row = row+'<tbody>';

				if (data){
					
					$.each(data,function(e,dato){
						row = row+'<tr>';
						row = row+'<td><a href="javascript:void(0)" onclick="quitarModal();setDetArt('+"'"+dato.codigo+"','"+dato.descripcion+"'"+')"><span class="text-size-0-5">'+dato.codigo+'</span></td>';
						row = row+'<td style="text-align:left">'+dato.descripcion+'</td>';
						row = row+'<td style="text-align:right">'+parseFloat(dato.pvpiva).toFixed(2)+'</td>';
						row = row+'<td style="text-align:center">'+dato.fe_ohasta+'</td>';
						row = row+'<td style="text-align:right">'+dato.descuento+'%'+'</td>';
						row = row+'<td style="text-align:right">'+parseFloat(dato.pvp_oferta_iva).toFixed(2)+'</td>';
						row = row+'</tr>';
					});

				}

				row = row+'</tbody>';
				row = row+'</table>';

				$('.contTableBusqueda').append(row);
				$('#table_busqueda').dataTable({
					"language": {
						"info": "_MAX_ Artículo(s) encontrado(s)"
					},
					"bPaginate"	: false, 
					"bFilter"	: false, 
					"bInfo" 	: true,
					"sScrollY"	: $('#dvBusCont').height()-$('#dvBusBar').height()-$('#dvBusSearch').height()-$('#dvBusFoot').height()-110, 
					"sScrollX"	: "95%", 
					"sScrollXInner": "95%", 
					"order": []
				} );
			}else{
				$('.contTableBusqueda').html(returnData);
			}
		}

	});
}

// Carga la informacion de un artículo en pantalla.
function setDetArt(cod,desc){
	retConsArt(cod);
}

//Carga los articulos buscados en el modal de busqueda
function retTraslados(){
	
	var datos = "";
	var bodent  = $('#hdBodPid').val();
	var url1 = $('#txt_urlGe').val();

	datos = datos+"c_be="+bodent; 
	$.ajax({
		type : 'POST',
		url  : url1+'inventario/IngresoPedidos/retTraslados',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = eval(returnData.trim().substr(3));
				$('.contPed').html('');
				var row = '<table id="table_pedidos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
				row = row+'<thead>';
				row = row+'<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Nº_Pedido</th>';
				row = row+'<th style="background-color:#428bca; color:white; font-size:1em;">Entra</th>';
				row = row+'<th style="background-color:#428bca; color:white; font-size:1em;">Sale</th>';
				row = row+'<th style="background-color:#428bca; color:white; font-size:1em;">Fecha_Ped.</th>';
				row = row+'<th style="background-color:#428bca; color:white; font-size:1em;">Pedido Por.</th>';
				row = row+'<th style="background-color:#428bca; color:white; font-size:1em;">Motivo</th>';
				row = row+'<th style="background-color:#428bca; color:white; font-size:1em;">Observación</th>';
				row = row+'<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Usuario</th>';
				row = row+'</thead>';
				row = row+'<tbody>';

				if (data){
					
					$.each(data,function(e,dato){
						row = row+'<tr>';
						row = row+'<td style="text-align:left">'+dato.co_noped+'</td>';
						row = row+'<td style="text-align:left">'+dato.bodent+'</td>';
						row = row+'<td style="text-align:left">'+dato.bodsal+'</td>';
						row = row+'<td style="text-align:left">'+dato.fe_pedido+'</td>';
						row = row+'<td style="text-align:left">'+dato.no_pedido_por+'</td>';
						row = row+'<td style="text-align:left">'+dato.no_motivo+'</td>';
						row = row+'<td style="text-align:left">'+dato.ds_comentario+'</td>';
						row = row+'<td style="text-align:left">'+dato.co_usuario+'</td>';
						row = row+'</tr>';
					});

				}

				row = row+'</tbody>';
				row = row+'</table>';

				$('.contPed').append(row);
			}
		}
	});
}

//Consulta si puede pedir
function retDetBod(){
	
	var datos = "";
	var bodent  = $('#hdBodPid').val();
	var url1 = $('#txt_urlGe').val();
	

	datos = datos+"c_be="+bodent; 
	$.ajax({
		type : 'POST',
		url  : url1+'inventario/IngresoPedidos/retStockBasico',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = JSON.parse(returnData.trim().substr(3));
				if (data){
					$('.lbPuedePed').text(data.puede_pedir);	
				}
			}else{
				errorGeneral("Sistema Intranet",returnData,'danger');
			}
		}

	});
}

//Consulta los tipos de bodega
function retConsBodeGyeUio(codBod,codOpt){
	
	var datos = "";
	var url1 = $('#txt_urlGe').val();
	

	datos = datos+"c_be="+codBod;
	$.ajax({
		type : 'POST',
		url  : url1+'inventario/IngresoPedidos/retConsBodeGyeUio',
		data : datos,
		success : function (returnData) {
			if (returnData.trim().substr(0,2) === "OK"){
				var data = JSON.parse(returnData.trim().substr(3));
				if (data){
					if(codOpt == 1){
						$('.lbTiBodPid').text(data.tipo_bodega);
					}
					if(codOpt == 2){
						$('.lbTiBodA').text(data.tipo_bodega);
					}
				}
			}else{
				errorGeneral("Sistema Intranet",returnData,'danger');
			}
		}
	});
}

function ingArticulo(){
	var cdMot   = $('#hdMotivo').val();
	var numFact = $('.txFactNum').val();
	var bodPide = $('#hdBodPid').val();
	var bodA    = $('#hdBodA').val();
	var codArt  = $('.txCodArt').val();
	var canArt  = parseInt($('.txCant').val());
	var isPrePed= $('#chkPre').is(':checked');
	var tipBodP = $('.lbTiBodPid').text();
	var tipBodA = $('.lbTiBodA').text();
	var artDsp  = parseInt($('#hdDspArt').val());
	var artSal  = parseInt($('#hdSalArt').val());
	var regArt  = $('#table_lst_pedidos tbody tr').length;
	var pedCont = $('#hdPedCon').val();	
	var pedidas = $('#hdPedida').val();	

	if(cdMot==''){
		errorGeneral("Sistema Intranet",'Debe ingresar un motivo','danger');
		agrMotivo();
		return;
	}

	if(cdMot==1 && numFact==''){
		errorGeneral("Sistema Intranet",'Debe ingresar el número de factura','danger');
		$('.txFactNum').focus();
		return;	
	}

	if(cdMot==1 && numFact!=''){
		// TODO: Validar el numero de factura si no return
	}

	if(bodPide == ''){
		errorGeneral("Sistema Intranet",'Debe ingresar la bodega que pide','danger');
		agrBodPide();
		return;
	}

	if(bodA == ''){
		errorGeneral("Sistema Intranet",'Debe ingresar la bodega a quien pide','danger');
		agrBodA();
		return;
	}

	if(codArt == ''){
		errorGeneral("Sistema Intranet",'Debe ingresar código del artículo','danger');
		$('.txCodArt').focus();
		return;
	}

	if(canArt == ''){
		errorGeneral("Sistema Intranet",'Debe ingresar cantidad del artículo','danger');
		$('.txCant').focus();
		return;
	}

	if(bodPide == bodA){
		errorGeneral("Sistema Intranet",'Bodega Origen no puede ser la misma bodega destino, revise por favor.','danger');
		return;
	}

	if(!isPrePed && cdMot!=1){
		errorGeneral("Sistema Intranet",'ATENCION: Pedido deberá ingresarlo como Prepedido.','danger');
		return;
	}

	if(!(	(bodA==18 || bodA==20 || bodA==44 || bodA==46 || bodA==47) || 
		(bodPide==18 || bodPide==20 || bodPide==44 || bodPide==14 || bodPide==46 || bodPide==47) ||
		(codArt.substr(0,1)=='Z')
		)
		){
		if(cdMot!=1 && false){// este false es por permiso especial
			errorGeneral("Sistema Intranet",'ATENCION: Pedidos manuales deshabilitados. EXCEPTO los F4, los cuales se registran como Motivo de Traslado: VENTA','danger');
			return;
		}
	}

	if(isPrePed && !(tipBodP=='A' && tipBodA=='A') && false){// este false es por permiso especial
		if(tipBodA!='V' && (tipBodA=='A' || tipBodP=='A')){
			errorGeneral("Sistema Intranet",'ATENCION: Prepedidos habilitado solo para juguetes o entre almacenes.','danger');
			return;
		}
	}

	if(codArt!='H03025' && codArt!='H03047' && codArt!='H03036'){
		if( (artDsp<canArt && artSal>=canArt) || artSal<=0 ){
			errorGeneral("Sistema Intranet",'La cantidad solicitada excede a la existente en la bodega ','danger');
			return;	
		}else if(artDsp<canArt && artSal<canArt){
			errorGeneral("Sistema Intranet",'La cantidad solicitada excede a la existente en la bodega ','danger');
			return;	
		}
	}
	if(regArt>=30){
		errorGeneral("Sistema Intranet",'Cantidad máxima de items por pedido es 30. Los items restantes ingréselos en otro pedido por favor ','danger');
		return;
	}
	
	if(pedCont>0){
		confirmDialog('El Articulo ya existe en '+pedCont+' Pedidos Pendientes, por '+pedidas+' unidades'+'</br>'+'Desea Realizar el pedido del artículo.?', function(){
			agrArticulo();
		});
	}else{
		agrArticulo();
	}
}

// Carga la informacion del saldo y disponible de un artículo.
function retConsDisponibleArt(codigo){
	var url = $('#txt_urlGe').val();
	var datos = "";
	if(codigo){
		datos = datos+"c_ca="+codigo;
		datos = datos+"&c_be="+$('#hdBodPid').val();
		datos = datos+"&c_bs="+$('#hdBodA').val();;
		$.ajax({
			type : 'POST',
			url  : url+"inventario/IngresoPedidos/retExistenciaItem", 
			data : datos,
			success : function (returnData) {
				if (returnData.trim().substr(0,2) === "OK"){
					var data = JSON.parse(returnData.trim().substr(3));
					if (data){
						$.each(data,function(e,dato){
							$('#hdDspArt').val(dato.disponible);	
							$('#hdSalArt').val(dato.saldo);	
							$('#hdPedCon').val(dato.v_contped);	
							$('#hdPedida').val(dato.v_pedidas);	
						});
					}
				}else{
					errorGeneral("Sistema Intranet",returnData,'danger');
				}
			}
		});
	}
}


//Permite filtrar solo numeros en las cajas de texto
function soloNumeros(e) {
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
		(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
		(e.keyCode >= 35 && e.keyCode <= 40)) 
	{
		return;
	}
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
}

//Cuando un artículo esta cargado y se ha ingreado una cantidad, agraga este artículo al grid de totales.
function agrArticulo(){

	var codArt = $('.txCodArt').val();
	var desArt = $('.lbDesArt').text();
	var canArt = parseInt($('.txCant').val());
	var codFab = $('#hdCodFab').val();
	var agrRow = true;


	if(codArt==""){
		errorGeneral("Sistema Intranet","Debe ingresar un artículo","danger");
		$('.txCodArt').focus();
		return;
	}

	if(canArt<=0){
		errorGeneral("Sistema Intranet","Debe ingresar una cantidad válida","danger");
		$('.txCant').focus();
		return;
	}

	$.each(arrArt,function(e,dato){
		if(dato.codigo == codArt){
			agrRow=false;
			dato.cantidad = parseInt(canArt);
		}
	});

	if(agrRow){
		arrArt.push({
			codigo 			: codArt,
			descripcion		: desArt,
			cantidad		: canArt,
			cod_fabrica		: codFab
		});
	}
	actualizaTablaArt();
	$('.txCodArt').val('');
	$('.lbDesArt').text('');
	$('.txCant').val('');
	$('.txCodArt').focus();
}

function actualizaTablaArt(){
	var row = "";
	var totItems = 0;
	var totFact  = 0;

	
	row = row + '<table id="table_lst_pedidos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">';
	row = row + '<thead>';
	row = row + '<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Código</th>';
	row = row + '<th style="background-color:#428bca; color:white; font-size:1em;"></th>';
	row = row + '<th style="background-color:#428bca; color:white; font-size:1em;">Artículo</th>';
	row = row + '<th style="background-color:#428bca; color:white; font-size:1em;">Cantidad</th>';
	row = row + '<th style="background-color:#428bca; color:white; font-size:1em;">Error</th>';
	row = row + '<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Fábrica</th>';
	row = row + '<thead>';
	row = row + '<tbody>';

	$.each(arrArt,function(e,dato){
		row = row+'<tr>';
		row = row+'<td style="text-align:center;">'+dato.codigo+'</td>';
		row = row+'<td style="text-align:center;">'
		+'<a href="javascript:void(0)" onclick="quitarArticulo('+"'"+dato.codigo+"'"+')"><i class="fa fa-trash-o"></i></a>'+'</td>';
		row = row+'<td style="text-align:left;">'+dato.descripcion+'</td>';
		row = row+'<td style="text-align:right;">'+dato.cantidad+'</td>';
		row = row+'<td style="text-align:right;"></td>';
		row = row+'<td style="text-align:right;">'+dato.cod_fabrica+'</td>';
		row = row+'</tr>';
	});
	row = row + '</tbody>';
	row = row + '</table>';
	$('.contLstPed').html(row);
	if(arrArt.length>0){	
		$('#table_lst_pedidos').dataTable({        
			"bPaginate" : false,
			"bFilter"   : false,          
			"bInfo"     : false,         
			"sScrollY"  : '20vh',
			"sScrollX"  : "95%",          
			"sScrollXInner": "95%",          
			"order": []
		} );     
	}
}

function quitarArticulo(codArt){
	var elem = -1; 
	//alert(JSON.stringify(arrArt));
	$.each(arrArt,function(e,dato){
		if(dato.codigo == codArt){
			elem = arrArt.indexOf(dato);
		}
	});
	if(elem>=0){
		confirmDialog("Desea eliminar el producto "+arrArt[elem]["codigo"]+"-"+arrArt[elem]["descripcion"]+" ?", function(e){
			arrArt.splice(elem,1);	
			actualizaTablaArt();
		});
	}
}

//Funcion que permite controlar los confirm dialog.
function confirmDialog(message, onConfirm){
	var fClose = function(){
		modal.modal("hide");
		$("#confirmOk").off('click', onConfirm);
	};
	$(document).keyup(function (e) {
		if (e.which === 27)
		{
			modal.modal("hide");
			$("#confirmOk").off('click', onConfirm);
		}
	});

	var modal = $("#confirmModal");
	modal.modal("show");
	$("#confirmMessage").empty().append(message);
	$("#confirmOk").one('click', onConfirm);
	$("#confirmOk").one('click', fClose);
	$("#confirmClose").one('click', fClose);
	$("#confirmCancel").one("click", fClose);
}

function grabaPedido(){
	var cdMot   = $('#hdMotivo').val();
	var bodPide = $('#hdBodPid').val();
	var bodA    = $('#hdBodA').val();
	var isPrePed= $('#chkPre').is(':checked');
	var pedPor  = $('.txPedPor').val();
	var obs		= $('#taObs').val().trim().toUpperCase();
	var url 	= $('#txt_urlGe').val();
	var arrCab 	= [];

	arrCab.push({
			bod_entra 		: bodPide,
			bod_sale		: bodA,
			pedido_por		: pedPor,
			motivo			: cdMot,
			comentario		: obs,
			estado			: ((isPrePed)?'F':'P')
		});

	var detPed = JSON.stringify(arrArt);
	var cabPed = JSON.stringify(arrCab);

	$.ajax({
		type: 'POST',
		url: url+"inventario/IngresoPedidos/grabaPedido",
		data: {'cabPedidos':cabPed,'detPedidos': detPed},
		success: function(returnData) {
			if (returnData.trim().substr(0,1) === "0"){
				var msg = returnData.trim().substr(2);
				errorGeneral('Sistema Intranet',msg,'success');	
				limpiar();
			}else{
				errorGeneral('Sistema Intranet',returnData,'success');	
			}
		}
	});
}