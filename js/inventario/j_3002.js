

$(document).ready(function() {
    
    var sp_2 = document.getElementById('sp_2');
    var so_0 = document.getElementById('so_0');
    var so_2 = document.getElementById('so_2');
    var t_2 = document.getElementById('t_2');
    var err = document.getElementById('err');
    sp_2.volume = 1;
    so_0.volume = 1;
    so_2.volume = 1;
    t_2.volume = 1;
    err.volume = 1;

    sp_2.play();
    resetControlU();
    
    $('#form_upc').bind('submit', function() {
        
        if(va_sess()){
            var a = $('#cod_upc').val();
            if(!$.isEmptyObject(a)){
                if(parseInt(a.length) > 6){
                    $('#cod_upc').focus();
                    $('.cnt_art').html('');
                    dataLoading();
                    $.post('co_3002/reArt',
                    {c_a:a},
                    function(b){
                        removedataLoading();
                        var c = eval('('+b+')');
                        if(c.err == ''){
                            if(c.data){
                                sp_2.play();
                                resetControlU();
                                $('.cnt_art').html(c.data);
                            } else {
                                t_2.play();
                                errorGeneral(conf_hd.tx.t_1,conf_hd.tx.t_2,'danger');
                                resetControlU();
                                setTimeout(function(){ sp_2.play();},2000);
                            }
                        } else {
                            err.play();
                            errorGeneral(conf_hd.tx.t_1,c.err,'danger');
                            resetControlU();
                            setTimeout(function(){ sp_2.play();},2000);
                        }
                    }).fail(function(p) {
                        err.play();
                        removedataLoading();
                        errorGeneral(conf_hd.tx.t_1,"ERROR:"+p.statusText+" - "+p.statusText,'danger');
                        resetControlU();
                        setTimeout(function(){ sp_2.play();},2000);
                        
                    });
                } else {
                    so_2.play();
                    errorGeneral(conf_hd.tx.t_1,"C&oacute;digo UPC incorrecto: "+a,'danger');
                    resetControlU();
                    setTimeout(function(){ sp_2.play();},2000);
                }
            }
        } else {
            redirectIntranet();
        }
        return false;
    });
    
}); 

function resetControlU(){
    $('#cod_upc').val('');
    $('#cod_upc').focus();
    setInterval(function(){
        $('#cod_upc').focus();
    }, 100);
}
