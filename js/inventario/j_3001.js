

$(document).ready(function() {
    
    var sp_1 = document.getElementById('sp_1');
    var sp_2 = document.getElementById('sp_2');
    var sp_3 = document.getElementById('sp_3');
    var so_0 = document.getElementById('so_0');
    var so_1 = document.getElementById('so_1');
    var so_2 = document.getElementById('so_2');
    var so_99 = document.getElementById('so_99');
    var t_2 = document.getElementById('t_2');
    var err = document.getElementById('err');
    sp_1.volume = 1;
    sp_2.volume = 1;
    sp_3.volume = 1;
    so_0.volume = 1;
    so_1.volume = 1;
    so_2.volume = 1;
    so_99.volume = 1;
    t_2.volume = 1;
    err.volume = 1;

    $('.f_2').css('display','none');
    sp_1.play();
    resetControlP();
    
    $('#form_pyc').bind('submit', function() {
        
        if(va_sess()){
            var a = $('#cod_pyc').val();
            if(!$.isEmptyObject(a)){
                if(parseInt(a.length) == 6){
                    $('#cod_upc').focus();
                    $('.f_1').css('display','none');
                    $('.f_2').css('display','block');
                    $('.cnt_art').html('');
                    dataLoading();
                    $.post('co_3001/reArt',
                    {c_a:a},
                    function(b){
                        removedataLoading();
                        var c = eval('('+b+')');
                        if(c.err == ''){
                            if(c.data){
                                sp_2.play();
                                resetControlU();
                                $('.cnt_art').html(c.data);
                            } else {
                                t_2.play();
                                errorGeneral(conf_hd.tx.t_1,conf_hd.tx.t_2,'danger');
                                resetControlP();
                                setTimeout(function(){ sp_1.play();},2000);
                            }
                        } else {
                            err.play();
                            errorGeneral(conf_hd.tx.t_1,c.err,'danger');
                            resetControlP();
                            setTimeout(function(){ sp_1.play();},2000);
                        }
                    }).fail(function(p) {
                        err.play();
                        removedataLoading();
                        errorGeneral(conf_hd.tx.t_1,"ERROR:"+p.statusText+" - "+p.statusText,'danger');
                        resetControlP();
                        setTimeout(function(){ sp_1.play();},2000);
                        
                    });
                } else {
                    sp_3.play();
                    errorGeneral(conf_hd.tx.t_1,"C&oacute;digo PYCCA incorrecto: "+a,'danger');
                    resetControlP();
                    setTimeout(function(){ sp_1.play();},2000);
                }
            }
        } else {
            redirectIntranet();
        }
        return false;
    });
    
    $('#form_upc').bind('submit', function() {
        var y = $('#cod_upc').val();
        var x = $('#cod_pyc').val();
        if(!$.isEmptyObject(x) && !$.isEmptyObject(y)){
            if(parseInt(y.length) > 6){
                if(va_sess()){
                    dataLoading();
                    $.post('co_3001/svUpc',
                        {c_u:y,c_a:x},
                        function(b){
                            removedataLoading();
                            var c = eval('('+b+')');
                            if(c.co_err == 0){
                                so_1.play();
                                resetControlP();
                                errorGeneral(conf_hd.tx.t_1,c.tx_err,'success');
                                setTimeout(function(){ sp_1.play();},2100);
                            } else {
                                err.play();
                                resetControlU();
                                errorGeneral(conf_hd.tx.t_1,c.tx_err,'danger');
                                setTimeout(function(){ sp_2.play();},2100);
                            }
                        }).fail(function(p) {
                            err.play();
                            removedataLoading();
                            errorGeneral(conf_hd.tx.t_1,"ERROR:"+p.statusText+" - "+p.statusText,'danger');
                        });
                } else {
                    redirectIntranet();
                }
            } else {
                so_2.play();
                resetControlU();
                errorGeneral(conf_hd.tx.t_1,"C&oacute;digo UPC incorrecto: "+y,'danger');
                setTimeout(function(){ sp_2.play();},2100);
            }
        }
        return false;
    });
    
//    $(document).keypress(function(e) {
//        if (e.which != 13) {
//            return false;
//        }
//    });

}); 

function resetControlP(){
    $('#cod_pyc').val('');
    $('#cod_upc').val('');
    $('.cnt_art').html('');
    $('.f_1').css('display','block');
    $('.f_2').css('display','none');
    $('#cod_pyc').focus();
    $('form input[text]').blur();
    setInterval(function(){
        $('#cod_pyc').focus();
    }, 100);
}

function resetControlU(){
    $('#cod_upc').val('');
    $('#cod_upc').focus();
    $('.f_1').css('display','none');
    $('.f_2').css('display','block');
     $('form input[text]').blur();
    setInterval(function(){
        $('#cod_upc').focus();
    }, 100);
}
