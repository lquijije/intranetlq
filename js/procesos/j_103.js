
$(document).ready(function() {
    var i = 1;
    var a = 1;
    var urlGe = $('#txt_urlGe').val();
    getDocCompras();
    
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            })
        },
        timepicker:false
    });
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            })
        },
        timepicker:false
    });
    
    $( ".searchAprobar" ).button().click(function(){
        getDocCompras();
    });
    
    $( ".aprobar" ).button().click(function(){
        aprueba();
    });
    
    $("#ck_apro").button().click(function(){
        if(i === 1){
            $("input[id='option_apro[]']").prop('checked', true);
            i++;
        } else if(i === 2){
            $(this).prop('checked', false);
            $("input[id='option_apro[]']").prop('checked', false);
            i = 1;
        }
    });
    
    $("#ck_rech").button().click(function(){
        if(a === 1){
            $("input[id='option_rech[]']").prop('checked', true);
            a++;
        } else if(a === 2){
            $(this).prop('checked', false);
            $("input[id='option_rech[]']").prop('checked', false);
            a = 1;
        }
    });

});

function getDocCompras(){ 
    
    var url1 = $('#txt_urlGe').val();
    var clase='';
    var datos = "tp="+$('#cmb_estado').val();
    datos = datos + "&f_i="+$('#txt_fec_ini').val();
    datos = datos + "&f_f="+$('#txt_fec_fin').val();
    
    if (!validaBusqueda()){
        return false;
    }
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'general/c_general_compras/retDocCompra',
        data: datos,
        success : function (returnData) {
                removedataLoading();       
                var data = eval(returnData); 
                $('#tableAprobar').html('');
                
                var row = '<table id="T_Requisicion" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Fecha</th>';
                row = row+'<th>Estado</th>';
                row = row+'<th>Dispositivo</th>';
                row = row+'<th>Destino/Motivo</th>';
                row = row+'<th>Cantidad</th>';
                row = row+'<th>Proveedor</th>';
                row = row+'<th>Precio</th>';
                row = row+'<th><input type="radio" name="ck_apro" id="ck_apro" /> Aprobar</th>';
                row = row+'<th><input type="radio" name="ck_apro" id="ck_rech" /> Rechazar</th>';
                row = row+'</thead>';
                row = row+'<tbody>';
                
                if (data){ 
                    $.each(data,function(e,dato){
                        if (dato.bd_status.trim() === 'A'){
                            clase = 'aprobada';
                        } else if (dato.bd_status.trim() === 'N'){
                            clase = 'negada';
                        } else {
                            clase = '';
                        }   
                        row = row+'<tr class="'+clase+'" id="row_'+dato.co_solicitud+'">';
                        row = row+'<td class="centro">'+dato.fe_ingreso+'</td>';
                        row = row+'<td class="centro">'+dato.nom_estado+'</td>';
                        row = row+'<td>'+dato.tx_dispositivo+'</td>';
                        row = row+'<td>'+dato.tx_destino+'</td>';
                        row = row+'<td class="centro">'+dato.va_cantidad+'</td>';
                        row = row+'<td>'+dato.tx_proveedor+'</td>';
                        row = row+'<td class="derecha">'+dato.va_precio+'</td>';
                        if (dato.bd_status.trim() === 'G'){
                            row = row+'<td><center><input type="radio" name="ck_'+dato.co_solicitud+'" id="option_apro[]" value="'+dato.co_solicitud+'"></center></td>';
                            row = row+'<td><center><input type="radio" name="ck_'+dato.co_solicitud+'" id="option_rech[]" value="'+dato.co_solicitud+'"></center></td>';
                        } else {
                            row = row+'<td></td>';
                            row = row+'<td></td>';
                        }
                        row = row+'</tr>';
                    });
                }  
                
                row = row+'</tbody>';
                row = row+'</table>';
                
                $('#tableAprobar').append(row);
                $('#T_Requisicion').dataTable();
            }
           
    });
    
    
}

function aprueba(){
    
    var apro = [];
    var rech = [];
    var url1 = $('#txt_urlGe').val();
    var aprobados = $("input[id='option_apro[]']:checked");
    var rechazados = $("input[id='option_rech[]']:checked");
    
    $(aprobados).each( function () {
        apro.push($(this).val());
    });
    
    $(rechazados).each( function () {
        rech.push($(this).val());
    });
    
//    if (parseInt($(rechazados).length) <= 0){
//        errorGeneral("Sistema Intranet",'Seleccione por lo menos una requisición.','danger');
//        return false;
//    }
//    return false;

    if($(aprobados).length <= 0){
        errorGeneral("Sistema Intranet",'Seleccione por lo menos una requisición.','danger');
        return false;
    } 
    
    var datos = "tp=G";
    datos = datos + "&tc=A";
    datos = datos + "&d_r="+apro;
    
    var datos1 = "tp=G";
    datos1 = datos1 + "&tc=N";
    datos1 = datos1 + "&d_r="+rech;
    
    dataLoading();
    
    if($(aprobados).length > 0){
        fn_aprobados(url1,datos);
    }
    
    if($(rechazados).length > 0){
        fn_negados(url1,datos1);
    }
    
}

function fn_aprobados(url1,datos){
    $.ajax({
        type : 'POST',
        async: false,
        url : url1+'general/c_general_compras/updateDocCompra',
        data: datos,
        success : function (returnData) {
            var data =returnData.trim(); 
            removedataLoading();         
            if(data.trim()==="OK"){
                errorGeneral("Sistema Intranet",'Transacción con exito','success'); 
                getDocCompras();
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }
        }
    });
}

function fn_negados(url1,datos){
    $.ajax({
        type : 'POST',
        async: false,
        url : url1+'general/c_general_compras/updateDocCompra',
        data: datos,
        success : function (returnData) {
            var data =returnData.trim(); 
            removedataLoading();         
            if(data.trim()==="OK"){
                //errorGeneral("Sistema Intranet",'Negados con exito','success'); 
                getDocCompras();
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }
        }
    });
}