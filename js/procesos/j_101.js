
$(function() {
    
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            })
        },
        timepicker:false
    });
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            })
        },
        timepicker:false
    });
    
    $('#tableIngReq').dataTable();
    
    $( ".btnN" ).button().click(function(){
        newModal('Requisicion','40','60','form_ing_req');
    });
    
    $( ".resetRequi" ).button().click(function(){
        quitarmodalGeneral('Requisicion','form_ing_req');
    });
    
    $( ".searchRequi" ).button().click(function(){
        getDocCompras();
    });
    
    $( ".aprueba" ).button().click(function(){
        aprobardocumentos('I','C');
    });
    
}); 

$(document).ready(function() {
    getDocCompras();
    var urlGe = $('#txt_urlGe').val();
    $( ".saveRequi").button().click(function(){
        saveRequisicion('I',urlGe);
    });
});

function saveRequisicion(accion,urlGe){
    
    var datos = "act="+accion;
    datos = datos + "&dis="+$('#txt_dispositivo').val();
    datos = datos + "&des="+$('#txt_destino').val();
    datos = datos + "&can="+$('#txt_cantidad').val();
    
    if (!validaCampos()){
        return false;
    }
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : urlGe+"procesos/requisicion/c_101/guardar", 
        data: datos,
        success : function (returnData) { 
            var data =returnData.trim(); 
            removedataLoading();         
            if(data.trim()==="OK"){
                quitarmodalGeneral('Requisicion','form_ing_req');
                errorGeneral("Sistema Intranet",'Requisición grabada con exito','success'); 
                getDocCompras()
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }
        }
    });

}

function validaCampos(){
    
    if ($("#txt_dispositivo").val().trim() === '') {
        errorGeneral("Sistema Intranet",'Campo Dispositivo esta vacio','danger');
        return false;
    }
    
    if ($("#txt_destino").val().trim() === '') {
        errorGeneral("Sistema Intranet",'Campo Destino esta vacio','danger');
        return false;
    }

    if ($("#txt_cantidad").val().trim() === '') {
        errorGeneral("Sistema Intranet",'Campo Cantidad esta vacio','danger');
        return false;
    }
    
    return true;
}

function getDocCompras(){ 
    
    var url1 = $('#txt_urlGe').val();
    var clase='';
    var datos = "tp="+$('#cmb_estado').val();
    datos = datos + "&f_i="+$('#txt_fec_ini').val();
    datos = datos + "&f_f="+$('#txt_fec_fin').val();
    
    if (!validaBusqueda()){
        return false;
    }
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'general/c_general_compras/retDocCompra',
        data: datos,
        success : function (returnData) {
                removedataLoading();
                var data = eval(returnData); 
                $('#tableRequisicion').html('');
                
                var row = '<table id="T_Requisicion" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Fecha</th>';
                row = row+'<th>Estado</th>';
                row = row+'<th>Dispositivo</th>';
                row = row+'<th>Destino/Motivo</th>';
                row = row+'<th>Cantidad</th>';
                row = row+'<th>Cotizar</th>';
                row = row+'</thead>';
                row = row+'<tbody>';
                
                if (data){ 
                    $.each(data,function(e,dato){
                        if (dato.bd_status.trim() === 'A'){
                            clase = 'aprobada';
                        } else if (dato.bd_status.trim() === 'N'){
                            clase = 'negada';
                        } else {
                            clase = '';
                        }  
                        row = row+'<tr class="'+clase+'" id="row_'+dato.co_solicitud+'">';
                        row = row+'<td class="centro">'+dato.fe_ingreso+'</td>';
                        row = row+'<td class="centro">'+dato.nom_estado+'</td>';
                        row = row+'<td>'+dato.tx_dispositivo+'</td>';
                        row = row+'<td>'+dato.tx_destino+'</td>';
                        row = row+'<td class="centro">'+dato.va_cantidad+'</td>';
                        if (dato.bd_status.trim() === 'I'){
                            row = row+'<td><center><input type="checkbox" id="ck_'+dato.co_solicitud+'" name="option_req[]" value="'+dato.co_solicitud+'"></center></td>';
                        } else {
                            row = row+'<td></td>';
                        }
                        row = row+'</tr>';
                    });
                }  
                
                row = row+'</tbody>';
                row = row+'</table>';
                
                $('#tableRequisicion').append(row);
                $('#T_Requisicion').dataTable();
            }
           
    });
    
    
}