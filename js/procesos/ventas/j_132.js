var contGe = 0;

function aprobar_solic() {

    var solc = [];
    var url1 = $('#txt_urlGe').val();
    var aprobados = null;
    var rechazados = null;

    aprobados = $("input[id='option_apro[]']:checked");
    rechazados = $("input[id='option_rech[]']:checked");

    $(aprobados).each(function () {
        var itemA = {};
        itemA["c_s"] = $(this).val();
        itemA["e_s"] = 'A';
        itemA["t_s"] = encodeURIComponent($('#tx_a' + $(this).context.name.trim().replace(/[ck_]/g, "")).val());
        solc.push(itemA);
    });

    $(rechazados).each(function () {
        var itemR = {};
        itemR["c_s"] = $(this).val();
        itemR["e_s"] = 'R';
        itemR["t_s"] = encodeURIComponent($('#tx_a' + $(this).context.name.trim().replace(/[ck_]/g, "")).val());
        solc.push(itemR);
    });

    if (!$.isEmptyObject(solc)) {
        var r = confirm("Desea procesar los siguientes registros.");
        if (r == true) {
            var datos = "s_s=" + JSON.stringify(solc);
            dataLoading();
            $.ajax({
                type: 'POST',
                url: url1 + "procesos/ventas/co_132/aprSol",
                data: datos,
                success: function (retData) {
                    removedataLoading();
                    var data = eval('(' + retData + ')');
                    if (parseInt(data.co_err) == 0) {
                        getSolis();
                        errorGeneral("Sistema Intranet", data.tx_err, 'success');
                    } else {
                        errorGeneral("Sistema Intranet", data.tx_err, 'danger');
                    }
                }
            });
        }
    } else {
        errorGeneral("Sistema Intranet", 'Debe Seleccionar una solicitud.', 'danger');
    }
}

function viewSolic(emp, id) {

    $('#view_Soli_cab').html('');
    $('#titleDoc').html('');

    var url1 = $('#txt_urlGe').val();
    var datos = "i_s=" + id + "&i_e=" + emp;
    dataLoading();
    $.ajax({
        type: 'POST',
        url: url1 + "procesos/ventas/co_132/viSol",
        data: datos,
        success: function (retData) {
            removedataLoading();
            var data = eval('(' + retData + ')');
            if (data) {
                $.each(data, function (a, dato) {
                    $('#titleDoc').html('Orden de Compra: ' + id);
                    $('#view_Soli_cab').html(dato.text);
                });
            }
        }
    });

    newModal('DetallSolic', '70', '95', 'form_DetallSolic');
}

function getSolis() {

    if (va_sess()) {
        var url1 = $('#txt_urlGe').val();
        $('#tab_Aut').DataTable({
            "bDestroy": true,
            "sAjaxDataProp": "data",
            "ajax": {
                "url": url1 + 'procesos/ventas/co_132/reSol',
                "type": "POST",
                "dataSrc": ""
            },
            "columns": [
                {title: 'Código', "data": "co_sol"},
                {title: 'Tipo', "data": "no_ti_aut"},
                {title: 'Almac&eacute;n', "data": "ds_bdg"},
                {title: 'Fecha', "data": "de_sol"},
                {title: 'Comentario', "data": "ms_sol"},
                {title: 'Factura', "data": "_link"},
                {title: 'Fecha Factura', "data": "fe_fac"},
                {title: 'Estado', "data": "no_es_sol"},
                {title: '<input type="radio" name="ck_apro" id="ck_apro" /> Aprobar', "data": "ap_sol"},
                {title: '<input type="radio" name="ck_apro" id="ck_rech" /> Negar', "data": "ne_sol"},
                {title: 'Comentario', "data": "ap_tex"}
            ],
            "aLengthMenu": [
                [100, 150, -1],
                [100, 150, "Todos"]
            ]
        });
    } else {
        redirectIntranet();
    }
}

function viewFact(sri, co_bdg, co_caj, nu_doc) {

    if (va_sess()) {

        dataLoading();

        $.post('co_132/viFac',
                {c_b: co_bdg, c_c: co_caj, n_d: nu_doc},
        function (c) {

            c = eval('(' + c + ')');

            if ($.isEmptyObject(c.err)) {

                if (!$.isEmptyObject(c.fac)) {

                    $('#nuFact').html(sri + '-' + co_caj + '-' + nu_doc + ' | Fecha: ' + c.fac[0].fe_fac);
                    //$('#nuFact').html(sri + '-' + co_caj + '-' + nu_doc + ' Fecha: ' + c.fac[0].fe_fac + ' Cliente: ' + c.fac[0].co_cli);

                    var row = '<table id="detSob" class="table table-bordered TFtable">';
                    row = row + '<thead>';
                    row = row + '<tr>';
                    row = row + '<th>Art&iacute;culo</th>';
                    row = row + '<th>Descripci&oacute;n</th>';
                    row = row + '<th>Cantidad</th>';
                    row = row + '<th>PVP</th>';
                    row = row + '</tr>';
                    row = row + '</thead>';
                    row = row + '<tbody>';

                    $.each(c.fac, function (f, art) {
                        row = row + '<tr>';
                        row = row + '<td>' + art.co_art + '</td>';
                        row = row + '<td>' + art.ds_art + '</td>';
                        row = row + '<td class="text-right">' + art.va_can + '</td>';
                        row = row + '<td class="text-right">' + art.va_pvp + '</td>';
                        row = row + '</tr>';
                    });

                    row = row + '</tbody>';
                    row = row + '</table>';

                    $('#view_fact').html(row);

                    newModal('DetFact', '70', '60', '');

                } else {
                    errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                }

            } else {
                errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
            }

            removedataLoading();

        }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });

    } else {
        redirectIntranet();
    }
}

$(document).ready(function () {

    getSolis();
    var i = 1;
    var a = 1;

    $(".a_sol").button().click(function () {
        aprobar_solic();
    });

    $("#ck_apro").button().click(function () {
        if (i === 1) {
            $("input[id='option_apro[]']").prop('checked', true);
            i++;
        } else if (i === 2) {
            $(this).prop('checked', false);
            $("input[id='option_apro[]']").prop('checked', false);
            i = 1;
        }
    });

    $("#ck_rech").button().click(function () {
        if (a === 1) {
            $("input[id='option_rech[]']").prop('checked', true);
            a++;
        } else if (a === 2) {
            $(this).prop('checked', false);
            $("input[id='option_rech[]']").prop('checked', false);
            a = 1;
        }
    });

}); 