
function _s_fact(){
    if(va_sess()){
        
        var nu_doc = $('#nu_doc').val();
        var aa = nu_doc.split('-');
        
        var cb_ = aa[0];
        var cj_ = aa[1];
        var nd_ = aa[2];
        
        if(cb_.trim() === ''){
            $('#co_al').focus();
            errorGeneral("Sistema Intranet",'Ingrese el # de almac&eacute;n','danger');
            return false;
        }
        
        if(cj_.trim() === ''){
            $('#co_ca').focus();
            errorGeneral("Sistema Intranet",'Ingrese el # de caja','danger');
            return false;
        }
        
        if(nd_.trim() === ''){
            $('#co_do').focus();
            errorGeneral("Sistema Intranet",'Ingrese el # de documento','danger');
            return false;
        }
        
        dataLoading();

        var datos = "c_a="+cb_+"&c_c="+cj_+"&c_d="+nd_;
        var url_1 = $('#txt_urlGe').val();

        $.ajax({
            type : 'POST',
            url : url_1+"procesos/ventas/co_127/ve_fac", 
            data: datos,
            success : function (returnData) {
                removedataLoading();
                var data = eval('('+returnData+')');
                if (data.data){ 
                    $('#id__fact').html(cb_+"-"+cj_+"-"+pad(nd_,9));
                    $('._i_cli').html(data.data.co_cl);
                    $('._n_cli').html(data.data.no_cl);
                    $('._t_cli').html(data.data.te_cl);
                    $('._f_com').html(data.data.fe_ve);
                    newModal('GenCup','60','50','');
                } else {
                    errorGeneral("Sistema Intranet",data.error,'danger'); 
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

function _u_cup(){
    if(va_sess()){
        
        var nu_doc = $('#nu_doc').val();
        var aa = nu_doc.split('-');
        
        var cb_ = aa[0];
        var cj_ = aa[1];
        var nd_ = aa[2];
        
        if(cb_.trim() === ''){
            $('#co_al').focus();
            errorGeneral("Sistema Intranet",'Ingrese el # de almac&eacute;n','danger');
            return false;
        }
        
        if(cj_.trim() === ''){
            $('#co_ca').focus();
            errorGeneral("Sistema Intranet",'Ingrese el # de caja','danger');
            return false;
        }
        
        if(nd_.trim() === ''){
            $('#co_do').focus();
            errorGeneral("Sistema Intranet",'Ingrese el # de documento','danger');
            return false;
        }
        
        dataLoading();

        var datos = "c_a="+cb_+"&c_c="+cj_+"&c_d="+nd_;
        var url_1 = $('#txt_urlGe').val();

        $.ajax({
            type : 'POST',
            url : url_1+"procesos/ventas/co_127/va_cup", 
            data: datos,
            success : function (returnData) {
                removedataLoading();
                var data = eval('('+returnData+')');
                //console.log(data.data);
                if (data.data == 0){ 
                    errorGeneral("Sistema Intranet","Orden generada con exito.",'success'); 
                    quitarmodalGeneral('GenCup','');
                    $('#nu_doc').val('');
                } else {
                    errorGeneral("Sistema Intranet",data.error,'danger'); 
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

$(function() {
    
    $( ".sea_f" ).button().click(function(){
        _s_fact();
    });
    
    $( ".val_c" ).button().click(function(){
        _u_cup();
    });
    
});
