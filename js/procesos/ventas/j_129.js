
$(function () {

    consultaCalendario();

    $('#tabCamp').DataTable();

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function (html) {
        var switchery = new Switchery(html, {size: 'small'});
        var changeCheckbox = document.querySelector('#enable_job');
        changeCheckbox.onchange = function () {
            var url1 = $('#txt_urlGe').val();
            var id_job = $('#i_j').val();
            var check = 0;
            if (changeCheckbox.checked) {
                check = 1;
            }

            if (va_sess()) {
                $.ajax({
                    type: 'POST',
                    url: url1 + "procesos/ventas/co_129/up_job",
                    data: "ck=" + check + "&i_j=" + id_job,
                    success: function (returnData) {
                        var datta = eval('(' + returnData + ')');
                        if (datta.data) {
                            if (datta.data.co_err != 0) {
                                errorGeneral("Sistema Intranet", datta.data.tx_err, 'danger');
                            }
                        } else {
                            errorGeneral("Sistema Intranet", returnData, 'danger');
                            return false;
                        }
                    }
                });
            } else {
                redirectIntranet();
            }

        };


    });

    $('.form_datetime').datetimepicker({
        format: 'Y/m/d',
        lang: 'es',
        timepicker: false,
        closeOnDateSelect: true
    });

    $(".convertText").click(function () {
        convertColumnExcel();
    });

    $(".add_p").click(function () {
        addOptionParametros();
    });

    $(".add_c").click(function () {
        addOptionCanales();
    });

    $(".add_pl").click(function () {
        addOptionPlanificacion();
    });

    $(".sav_r").click(function () {
        saveReg();
    });

    $(".p_res").click(function () {
        sumresdate(false);
    });

    $(".p_sum").click(function () {
        sumresdate(true);
    });

    $(".ini_job").click(function () {
        iniJob();
    });
    $(".new_camp").click(function () {
        $('#co_acc').val('I');
        $('#reg_para').html('');
        $('#contCanalAsig').html('');
        $('#reg_plani').html('');
        $('#name_reg').html("Nueva");
        newModal('ModReg', '60', '75', 'form_ModReg');
    });

    $('#cmb_can_pla').select2({
        width: '100%',
        placeholder: 'Canales'
    });

    $('#cmb_pri_pla').select2({
        width: '100%',
        placeholder: 'Prioridades'
    });

});

function iniJob() {
    if (va_sess()) {

        var q = 0;
        var url1 = $('#txt_urlGe').val();

        dataGeneralLoading('porc_ge');

        $.ajax({
            type: 'POST',
            async: false,
            url: url1 + "procesos/ventas/co_129/in_job",
            success: function (returnData) {
                ///errorGeneral("Sistema Intranet",returnData,'danger');
            }
        });

        var inter = setInterval(function () {
            q++;
            $.ajax({
                type: 'POST',
                async: false,
                url: url1 + "procesos/ventas/co_129/st_job",
                success: function (returnData) {
                    var data = eval('(' + returnData + ')');
                    if (data) {

                        $('#job_ini').html(data.run_Requested_date);
                        $('#job_ter').html(data.stop_execution_date);
                        $('#job_dem').html(secondToHour(data.ElapsedSeconds));
                        $('#job_men').html(data.last_outcome_message);

                        if (data.stop_execution_date.trim() != '') {
                            removeGeneralLoading('porc_ge');
                            clearInterval(inter);
                            return false;
                        }
                    } else {
                        removeGeneralLoading('porc_ge');
                        clearInterval(inter);
                        errorGeneral("Sistema Intranet", returnData, 'danger');
                        return false;
                    }
                }
            });
        }, 1000);


    } else {
        redirectIntranet();
    }
}

function saveReg() {

    if (va_sess()) {

        var urlGe = $('#txt_urlGe').val();
        var c_r = $('#co_reg').val();
        var d_r = $('#txt_des').val();
        var c_e = $('#cmb_est').val();

        var a = d_r + '|' + 'Descripción' + ':';
        a = a + c_e + '|' + 'Estado';

        var json_param = [];
        var json_detalle = [];
        var json_canal = [];
        var item = {};

        if (isEmptySis(a)) {

            var item = {};
            item['c_r'] = c_r;
            item['d_r'] = d_r;
            item['c_e'] = c_e;

            $('#reg_para tr').each(function () {

                var item_p = {};
                var co = $(this).find('.tab_co_para').html();
                var va = $(this).find('.tab_va_para').html();

                if (co != null && va != null) {
                    item_p["co"] = co;
                    item_p["va"] = va;
                    json_param.push(item_p);
                }

            });

            $('#contCanalAsig li').each(function () {

                var item_c = {};
                var co_c = parseInt($(this).attr('id').replace(/[opt_cana_]/g, ""));
                var me_c = $('#txt_men_can_' + co_c).val();

                if (co_c != null && me_c != null) {
                    item_c["co"] = co_c;
                    item_c["me"] = me_c;
                    json_canal.push(item_c);
                }

            });

            $('#reg_plani tr').each(function () {

                var item_d = {};
                var d_tx = $(this).find('.tab_tx_pla').html();
                var d_ca = $(this).find('.tab_cm_can').val();
                var d_pr = $(this).find('.tab_cm_pri').html();
                var d_es = $(this).find('.tab_es_pla').html();

                if (d_tx != null && d_ca != null && d_pr != null && d_es != null) {
                    item_d["d_tx"] = d_tx;
                    item_d["d_ca"] = d_ca;
                    item_d["d_pr"] = d_pr;
                    item_d["d_es"] = d_es;
                    json_detalle.push(item_d);
                }

            });

            dataLoading();

            var datos = 'c_a=' + JSON.stringify(item);
            datos = datos + '&d_p=' + JSON.stringify(json_param);
            datos = datos + '&d_c=' + JSON.stringify(json_canal);
            datos = datos + '&d_d=' + JSON.stringify(json_detalle);
            datos = datos + '&e_t=' + $('#co_acc').val();

            $.ajax({
                type: 'POST',
                url: urlGe + "procesos/ventas/co_129/sv_reg",
                data: datos,
                success: function (returnData) {
                    removedataLoading();
                    var datta = eval('(' + returnData + ')');
                    if (datta.error == '') {
                        if (datta.data.co_err == 0) {
                            $('#co_reg').val('');
                            $('#form_ModReg')[0].reset();
                            errorGeneral("Sistema Intranet", "Registro Grabado Con Exito", 'success');
                            quitarmodalGeneral('ModReg', 'form_ModReg');
                        } else {
                            errorGeneral("Sistema Intranet", datta.data.tx_err, 'danger');
                        }
                    } else {
                        errorGeneral("Sistema Intranet", datta.error, 'danger');
                        return false;
                    }
                }
            });
        }
    } else {
        redirectIntranet();
    }
}

function sumresdate(bool) {

    var d = new Date($('#fe_ini').val());
    d.setTime(d.getTime() + 1 * 24 * 60 * 60 * 817);
    if (bool) {
        d.setMonth((d.getMonth()) + 1);
    } else {
        d.setMonth((d.getMonth()) - 1);
    }
    $('#fe_ini').val(d.getFullYear() + "-" + pad((d.getMonth() + 1), 2) + "-" + pad(d.getDate(), 2));
    consultaCalendario();
}

function consultaCalendario() {

    if (va_sess()) {

        var da = $('#fe_ini').val();
        //console.log(da)
        var now = new Date(da);
        now.setTime(now.getTime() + 1 * 24 * 60 * 60 * 817);
        var curr_month = now.getMonth();
        curr_month = curr_month + 1;
        var curr_year = now.getFullYear();

        $('.tit_cal').html(retMes(curr_month) + "/" + curr_year);

//        console.log(now)
//        console.log(curr_year)
//        console.log(curr_month)

        var url1 = $('#txt_urlGe').val();
        var datos = "c_a=" + curr_year + "&c_m=" + curr_month;

        dataGeneralLoading('plani_tra');

        $.ajax({
            type: 'POST',
            url: url1 + 'procesos/ventas/co_129/retCalendar',
            data: datos,
            success: function (returnData) {
                removeGeneralLoading('plani_tra');
                var datos = eval('(' + returnData + ')');

                var row = '';
                var pp = '';

                if (!$.isEmptyObject(datos)) {

                    $.each(datos, function (e, d2) {

                        row = row + '<div class="media">';
                        row = row + '<div class="media-left"><div class="media-cal"><span>' + d2.groupeddata[0].fe_dia + '</span><span>' + d2.groupeddata[0].no_dia + '</span></div></div>';
                        row = row + '<div class="media-body" style="width: 100%;">';

                        $.each(d2.groupeddata, function (e, dg) {
                            var color = '';
                            if (dg.es_fec == 'I') {
                                color = 'style="color:#F00"';
                            }
                            row = row + '<h4 class="media-heading" ' + color + '>' + dg.ds_cam + '</h4>';
                            row = row + '<p ' + color + '>' + dg.ds_can + ' - ' + dg.ds_pri + '</p>';
                        });

                        row = row + '</div>';
                        row = row + '</div>';

                    });

                    $('#plani_tra').html(row);

//                    $('#calendar').fullCalendar({
//                        header: {
//				left: 'prev,next today',
//				center: 'title',
//                                right: 'month,agendaWeek,agendaDay'
//			},
//                        lang: 'es',
//                        events: datos
//                    });

                } else {
                    errorGeneral("Sistema Intranet", returnData, 'danger');
                }
            }

        });

    } else {
        redirectIntranet();
    }
}

function viewReg(c_r) {

    if (va_sess()) {

        $('#co_acc').val('U');
        $('#co_reg').val(c_r);

        var url1 = $('#txt_urlGe').val();
        var datos = "c_r=" + c_r;

//        $('#m_n1').addClass('active');
//        $('#tab1').addClass('active');
//        $('#tab2').removeClass('active');
//        $('#m_n2').removeClass('active');
//        $('#m_n3').removeClass('active');
//        $('#m_n4').removeClass('active');

        dataLoading();

        $.ajax({
            type: 'POST',
            url: url1 + 'procesos/ventas/co_129/retReg',
            data: datos,
            success: function (returnData) {
                $('#reg_para').html('');
                $('#contCanalAsig').html('');
                $('#reg_plani').html('');
                $('#reg_fech').html('');
                removedataLoading();
                var datos = eval('(' + returnData + ')');
                if ($.isEmptyObject(datos.err)) {
                    if (!$.isEmptyObject(datos.res_1)) {
                        $('#name_reg').html(datos.res_1.co_cam + " - " + datos.res_1.ds_cam);
                        $('#txt_des').val(datos.res_1.ds_cam);
                        $('#cmb_est').val(datos.res_1.es_cam);
                        if (!$.isEmptyObject(datos.res_2)) {
                            $('#reg_para').html(datos.res_2);
                        }
                        if (!$.isEmptyObject(datos.res_3)) {
                            $('#contCanalAsig').html(datos.res_3);
                        }
                        if (!$.isEmptyObject(datos.res_4)) {
                            $('#reg_plani').html(datos.res_4);
                        }
                        if (!$.isEmptyObject(datos.res_5)) {
                            $('#reg_fech').html(datos.res_5);
                        }
                        resetSelectCan();
                        newModal('ModReg', '60', '75', 'form_ModReg');
                    } else {
                        errorGeneral("Sistema Intranet", datos.err, 'danger');
                    }
                } else {
                    errorGeneral("Sistema Intranet", datos.err, 'danger');
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

/*//////////////////////////////////////////////////////////////////////////*/

function addOptionParametros() {

    var co_par = $('#cmb_param').val();
    var de_par = $('#cmb_param option:selected').text();
    var va_par = $('#txt_param').val();
    var a = co_par + '|' + 'Parámetro' + ':';
    a = a + va_par + '|' + 'Valor';

    if (isEmptySis(a)) {

        if (validaParametros(co_par)) {

            var row = '<tr id="opt_para_' + co_par + '">';
            row = row + '<td><p class="tab_co_para">' + co_par + '</p></td>';
            row = row + '<td>' + de_par + '</td>';
            row = row + '<td class="derecha"><p class="tab_va_para">' + va_par + '</p></td>';
            row = row + '<td><center><button type="button" onclick="removeOptionParametros(' + co_par + ')" class="btn btn-circle btn-danger"><i class="text-blanco fa fa-minus"></i></button></center></td>';
            row = row + '</tr>';

            $('#reg_para').append(row);
            $('#cmb_param').val('NNN');
            $('#txt_param').val('');

        }
    }

}

function validaParametros(co_par) {
    var bandera = true;
    $('#reg_para tr').each(function () {
        if (parseInt(co_par) == parseInt($(this).context.id.replace(/[opt_para_]/g, ""))) {
            errorGeneral("Sistema Intranet", "El Parámetro <b>" + $('#cmb_param option:selected').text() + "</b> ya se encuentra ingresado", "danger");
            bandera = false;
        }
    });

    return bandera;
}

function removeOptionParametros(id) {

    if (confirm('Esta seguro de que desea eliminar este registro ?')) {
        $('#opt_para_' + id).remove();
    }

}

/*//////////////////////////////////////////////////////////////////////////*/

function addOptionCanales() {

    var co_can = $('#cmb_canal').val();
    var de_can = $('#cmb_canal option:selected').text();
    var va_can = $('#txt_men_ad_can').val();
    var a = co_can + '|' + 'Canal' + ':';
    a = a + va_can + '|' + 'Mensaje';

    if (isEmptySis(a)) {

        if (validaCanales(co_can)) {

            var can = '<li class="ui-state-default" id="opt_cana_' + co_can + '">';
            can = can + '<div class="external-event">';
            can = can + '<div style="position: relative;">';
            can = can + '<button title="Eliminar Canal" type="button" class="btn btn-circle btn-danger" onclick="removeOptionCanales(' + co_can + ')" style="position: absolute; right: 0;"><i class="text-blanco text-size-1 fa fa-close"></i></button>';
            can = can + '<b>' + de_can + '</b>';
            can = can + '<textarea name="txt_men_can_' + co_can + '" value="" id="txt_men_can_' + co_can + '" class="form-control" style="height:50px;">' + va_can + '</textarea>';
            can = can + '</div>';
            can = can + '<div class="clearfix"></div>';
            can = can + '</div>';
            can = can + '</li>';

            $('#contCanalAsig').append(can);
            $('#cmb_canal').val('NNN');
            $('#txt_men_ad_can').val('');
            resetSelectCan();

        }

    }

}

function resetSelectCan() {
    var sel = '<select name="cmb_can_pla" id="cmb_can_pla" class="form-control" multiple="">';
    $('#contCanalAsig li').each(function () {
        var co_c = parseInt($(this).attr('id').replace(/[opt_cana_]/g, ""));
        if (co_c != null) {
            sel = sel + '<option value="' + co_c + '">' + $("#cmb_canal option[value='" + co_c + "']").text() + '</option>';
        }
    });
    sel = sel + '</select>';
    $('#div_can_det').html(sel);
    $('#cmb_can_pla').select2({
        width: '100%',
        placeholder: 'Canales'
    });
}

function removeOptionCanales(id) {

    if (confirm('Esta seguro de que desea eliminar este registro ?')) {
        $('#opt_cana_' + id).remove();
        resetSelectCan();
    }

}

function validaCanales(co_can) {

    var bandera = true;

    $('#contCanalAsig li').each(function () {
        if (parseInt(co_can) == parseInt($(this).attr('id').replace(/[opt_cana_]/g, ""))) {
            errorGeneral("Sistema Intranet", "El Canal <b>" + $('#cmb_canal option:selected').text() + "</b> ya se encuentra ingresado", "danger");
            bandera = false;
        }
    });

    return bandera;

}

/*//////////////////////////////////////////////////////////////////////////*/

function addOptionPlanificacion() {

    var tx_can = '';

    $("#cmb_can_pla :selected").each(function (i, sel) {
        tx_can = tx_can + $(sel).text() + ",";
    });

    var bandera = false;

    $('#contCanalAsig li').each(function () {
        bandera = true;
    });

    if (bandera) {

        var tx_pla = $('#txt_plani').val();
        var cm_can = $('#cmb_can_pla').val();
        var cm_pri = $('#cmb_pri_pla').val();
        var es_pla = $('#cmb_est_pla').val();

        var a = tx_pla + '|' + 'Dias de desplazamiento' + ':';
        a = a + cm_can + '|' + 'Canal';
        //a = a + cm_pri + '|' + 'Prioridad';

        if (isEmptySis(a)) {

            if (validaPlanificacion(tx_pla)) {

                var pla = '<tr id="opt_plan_' + tx_pla + '">';
                pla = pla + '<td><p class="tab_tx_pla">' + tx_pla + '</p></td>';
                pla = pla + '<td>' + tx_can.substring(0, (tx_can.length) - 1) + '<input type="hidden" class="tab_cm_can" value="' + cm_can + '"></td>';
                pla = pla + '<td><p class="tab_cm_pri">' + cm_pri + '</p></td>';
                pla = pla + '<td><p class="tab_es_pla">' + es_pla + '</p></td>';
                pla = pla + '<td><center><button type="button" onclick="removeOptionPlanificacion(' + tx_pla + ')" class="btn btn-circle btn-danger add_option"><i class="text-blanco fa fa-minus"></i></button></center></td>';
                pla = pla + '</tr>';

                $('#reg_plani').append(pla);
                $('#txt_plani').val('');
                $('#cmb_can_pla').select2('val', '');
                $('#cmb_pri_pla').select2('val', '');
                $('#cmb_est_pla').val('A');

            }
        }

    } else {
        errorGeneral("Sistema Intranet", "Para poder planificar necesita ingresar 1 o mas canales.", "danger");
    }

}

function validaPlanificacion(co_par) {
    var bandera = true;
    $('#reg_plani tr').each(function () {
        if (parseInt(co_par) == parseInt($(this).context.id.replace(/[opt_plan_]/g, ""))) {
            errorGeneral("Sistema Intranet", "El Parámetro '" + co_par + "' ya se encuentra ingresado", "danger");
            bandera = false;
        }
    });
    return bandera;
}

function removeOptionPlanificacion(id) {
    if (confirm('Esta seguro de que desea eliminar este registro ?')) {
        $('#opt_plan_' + id).remove();
    }
}

function convertColumnExcel() {
    var xl = $('#textarea_copy').val();
    var fe = $('#txt_fec_ini').val();
    var c_c = $('#co_reg').val();
    var ar = [];
    if (!$.isEmptyObject(xl)) {
        if (!$.isEmptyObject(fe)) {
            var string = '[["' + xl.replace(/\n+$/i, '').replace(/\n/g, '"],["').replace(/\t/g, '","') + '"]]';
            var json_e = eval(string);
            var countCol = Object.keys(json_e[0]).length;
            if (countCol === 1) {
                $.each(json_e, function (e, dato) {
                    ar.push(dato[0])
                });
                dataLoading();
                $.post('co_129/snExcel',
                        {d_c: JSON.stringify(ar), d_f: fe, c_c: c_c},
                        function (c) {
                            c = eval('(' + c + ')');
                            if (c.co_err == 0) {
                                $('#textarea_copy').val('');
                                $('#txt_fec_ini').val('');
                                quitarmodalGeneral('ModReg', 'form_ModReg');
                                errorGeneral("Sistema Intranet", c.tot + ' registros ingresados con exito.', 'success');
                            } else {
                                errorGeneral("Sistema Intranet", c.tx_err, 'danger');
                            }
                            removedataLoading();

                        }).fail(function (p) {
                    removedataLoading();
                    errorGeneral("Sistema Intranet", "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
                });
            } else {
                errorGeneral("Sistema Intranet", 'Solo puede ingresar 1 columna.', 'danger');
            }
        } else {
            errorGeneral("Sistema Intranet", 'El campo fecha se encuentra vacio', 'danger');
        }
    } else {
        errorGeneral("Sistema Intranet", 'Campo de datos se encuentra vacio.', 'danger');
    }
}