function sendSolis() {

    if (va_sess()) {

        var url1 = $('#txt_urlGe').val();
        var tx_obs = $('#tx_obs').val();
        var a = tx_obs + '|' + 'Observación';

        if ($('#cmb_tip').val().trim() == 'NNN') {
            errorGeneral("Sistema Intranet", "No puede realizar esta acción porque no tiene tipos de autorización asignados.", "danger");
            return false;
        }

        if (isEmptySis(a)) {

            dataLoading();
            var datos = 't_o=' + encodeURIComponent(tx_obs);
            datos = datos + '&t_t=' + $('#cmb_tip').val();
            datos = datos + '&f_f=' + $('#txt_fec_doc').val();
            datos = datos + '&c_s=' + $('#txt_sr').val();
            datos = datos + '&c_a=' + $('#co_alm').val();
            datos = datos + '&c_c=' + $('#txt_cj').val();
            datos = datos + '&c_d=' + $('#txt_dc').val();

            $.ajax({
                type: 'POST',
                url: url1 + 'procesos/ventas/co_131/senSo',
                data: datos,
                success: function (returnData) {
                    removedataLoading();
                    var datta = eval('(' + returnData + ')');
                    if (datta.data.co_err === 0) {
                        resetPage();
                        getSolis();
                        quitarmodalGeneral('DetFact', '');
                        errorGeneral("Sistema Intranet", "Registro Grabado Con Exito", 'success');
                    } else {
                        errorGeneral("Sistema Intranet", datta.data.tx_err, 'danger');
                    }
                }
            });
        }
    } else {
        redirectIntranet();
    }
}

function getFact(a, b, c, d) {
    if (va_sess()) {
        if ($('#cmb_tip').val().trim() == 'NNN') {
            errorGeneral("Sistema Intranet", "No puede realizar esta acción porque no tiene tipos de autorización asignados.", "danger");
            return false;
        }
        var url1 = $('#txt_urlGe').val();
        var datos = "c_b=" + a + "&c_c=" + b + "&c_d=" + c + "&f_f=" + d;
        var num_fact = pad($('#txt_sr').val(), 3).toString() + '-' + b.toString() + '-' + c.toString();
        $('#id__fact').html(num_fact);
        dataLoading();
        $.ajax({
            type: 'POST',
            data: datos,
            url: url1 + 'procesos/ventas/co_131/retDetFact',
            success: function (returnData) {
                removedataLoading();
                var data = eval('(' + returnData + ')');
                if (data.err == '') {
                    if (data.dat) {
                        $('#cntDetFac').html(data.dat);
                        newModal('DetFact', '60', '55', 'form_DetFact');
                    } else {
                        errorGeneral("Sistema Intranet", 'No hay datos en la busqueda realizada', 'danger');
                    }
                } else {
                    errorGeneral("Sistema Intranet", data.err, 'danger');
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

function passEmpr(a, b) {
    $('#co_alm').val(a);
    $('#co_sr').val(b);
    document.getElementById("f_solici").submit();
}

function getSolis() {
    if (va_sess()) {
        var url1 = $('#txt_urlGe').val();
        $('#tab_Aut').DataTable({
            "bDestroy": true,
            "sAjaxDataProp": "data",
            "ajax": {
                "url": url1 + 'procesos/ventas/co_131/retSol',
                "type": "POST",
                "data": {"c_a": $('#co_alm').val()},
                "dataSrc": ""
            },
            "columns": [
                {title: 'Código', "data": "co_sol"},
                {title: 'Tipo', "data": "no_ti_aut"},
                {title: 'Fecha', "data": "de_sol"},
                {title: 'Comentario', "data": "ms_sol"},
                {title: 'Factura', "data": "nu_fac"},
                {title: 'Estado', "data": "no_es_sol"}
            ]
        });
    } else {
        redirectIntranet();
    }
}

function resetPage() {
    $('#txt_fec_doc').val('');
    $('#txt_cj').val('');
    $('#txt_dc').val('');
    $('#tx_obs').val('');
}

$(function () {

    getSolis();

    $('.form_datetime').datetimepicker({
        format: 'Y/m/d',
        lang: 'es',
        timepicker: false,
        closeOnDateSelect: true
    });

    $(".sea_f").click(function () {
        var cb_ = $('#co_alm').val();
        var cj_ = $('#txt_cj').val();
        var nd_ = $('#txt_dc').val();
        var ff_ = $('#txt_fec_doc').val();

        var a = cb_ + '|' + '# De almacén' + ':';
        a = a + cj_ + '|' + '# De caja' + ':';
        a = a + nd_ + '|' + '# De documento' + ':';
        a = a + ff_ + '|' + 'Fecha del documento';

        if (isEmptySis(a)) {
            getFact(cb_, cj_, nd_, ff_);
        }
    });

    $(".seaGet").click(function () {
        getSolis();
    });


});
