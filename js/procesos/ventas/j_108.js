var icount = 0;

$(function() {
   
    $( ".searchDatVent" ).button().click(function(){
        getDatVentas();
    });
    
    $('#txt_fec_Ge').datetimepicker({
        format:'Y/m/d | H:i',
        lang:'es',
        hour: 19,
        minute: 00 
    });
}); 

function getDatVentas(){
    var fi = $('#txt_fec_Ge').val();
    
    if (fi.trim() === ''){
        errorGeneral("Sistema Intranet",'Campo Fecha Inicial esta vacio','danger');
        return false;
    }
    
    var url1 = $('#txt_urlGe').val();
    var datos = "f_i="+fi;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/ventas/co_108/reDatVentas',
        data: datos,
        success : function (returnData) {
            
                removedataLoading();
                    
                if (returnData.trim().substr(0,2) === "OK"){
                    $('#placeholder').empty();
                    $('#tableVentasDiarias').empty();
                    $('#ContVentPri').css('border','none');
                    var row = '<table class="TFtable">';
                    row = row+'<thead>';
                    row = row+'<tr>';
                    row = row+'<th rowspan="2">Almacén</th>';
                    row = row+'<th rowspan="2">Hora</th>';
                    row = row+'<th>Hoy</th>';
                    row = row+'<th colspan="2">1 Semana</th>';
                    row = row+'<th colspan="4">1 Año</th>';
                    row = row+'</tr>';
                    
                    var data = returnData.trim().substr(3);
                    var arra = data.split('/*/');
                    var array_1 = eval(arra[0]);
                    var array_2 = eval(arra[1]);
                    //console.log(eval(arra[1]));
                    //return false;
                
                    if (array_2){ 
                        var Sumventa =0,Sumventa_07=0,Sumpor_07=0,Sumventa_365=0,Sumpor_365=0,Sumventa_365xDia=0,Sumpor_365xDia = 0;
                        var now = new Date();
                        var fecha = now.customFormat("#DDD# #D# | #MMM# #YY#"); 
                        //console.log(fecha);
                        $.each(array_1,function(e,dat){
                            row = row+'<tr>';
                            row = row+'<th>'+fecha+'</th>';
                            row = row+'<th>'+dat.semana+'</th>';
                            row = row+'<th>%</th>';
                            row = row+'<th>'+dat.anio+'</th>';
                            row = row+'<th>%</th>';
                            row = row+'<th>'+dat.anioDia+'</th>';
                            row = row+'<th>%</th>';
                            row = row+'</tr>';
                            row = row+'</thead>';
                            row = row+'<tbody>';
                        });
                        var fec = fi.split('|');
                        $.each(array_2,function(e,dato){
                            Sumventa+=parseInt(dato.venta);
                            Sumventa_07+=parseInt(dato.venta_07);
                            Sumventa_365+=parseInt(dato.venta_365);
                            Sumventa_365xDia+=parseInt(dato.venta_365xDia);
                            row = row+'<tr id="id_'+dato.co_bodega+'" >';
                            row = row+'<td>'+dato.descripcion+'</td>';
                            row = row+'<td class="centro">'+fec[1]+'</td>';
                            row = row+'<td class="derecha">'+formato_numero(roundTo(dato.venta),0,".",",")+'</td>';
                            row = row+'<td class="derecha">'+formato_numero(roundTo(dato.venta_07),0,".",",")+'</td>';
                            row = row+'<td class="derecha">'+dato.por_07+'</td>';
                            row = row+'<td class="derecha">'+formato_numero(roundTo(dato.venta_365),0,".",",")+'</td>';
                            row = row+'<td class="derecha">'+dato.por_365+'</td>';
                            row = row+'<td class="derecha">'+formato_numero(roundTo(dato.venta_365xDia),0,".",",")+'</td>';
                            row = row+'<td class="derecha">'+dato.por_365xDia+'</td>';
                            row = row+'</tr>';
                        });
                        
                        row = row+'</tbody>';
                        row = row+'<tfoot>';
                        row = row+'<tr>';
                        row = row+'<td>Totales: </td>';
                        row = row+'<td class="centro">'+fec[1]+'</td>';
                        row = row+'<td class="derecha">'+formato_numero(roundTo(Sumventa),0,".",",")+'</td>';
                        row = row+'<td class="derecha">'+formato_numero(roundTo(Sumventa_07),0,".",",")+'</td>';
                        Sumpor_07 = (Sumventa*100/Sumventa_07)-100;
                        row = row+'<td class="derecha">'+formato_numero(roundTo(Sumpor_07),2,".",",")+'</td>';
                        row = row+'<td class="derecha">'+formato_numero(roundTo(Sumventa_365),0,".",",")+'</td>';
                        Sumpor_365 = (Sumventa*100/Sumventa_365)-100;
                        row = row+'<td class="derecha">'+formato_numero(roundTo(Sumpor_365),2,".",",")+'</td>';
                        row = row+'<td class="derecha">'+formato_numero(roundTo(Sumventa_365xDia),0,".",",")+'</td>';
                        Sumpor_365xDia = (Sumventa*100/Sumventa_365xDia)-100;
                        row = row+'<td class="derecha">'+formato_numero(roundTo(Sumpor_365xDia),2,".",",")+'</td>';
                        row = row+'</tr>';
                        row = row+'</tfoot>';
                        row = row+'</table>';
                    }  
                    
                } else {
                    $('#tableVentasDiarias').empty();
                    $('#placeholder').empty();
                    var row_1 = '<div style="margin-left: auto;margin-right: auto; width: 500px; text-align: center;padding-top: 130px;">';
                    row_1 = row_1+'<h1 id="textChart" style="font-weight: bold;color:#C9C9C9;">'+returnData+'</h1>';
                    row_1 = row_1+'<i class="fa fa-table" style="color:#DCDCDC;font-size: 10em;"></i>';
                    row_1 = row_1+'</div>';
                    $('#placeholder').html(row_1);
                    $('#ContVentPri').css('border','5px dashed #FFF');
                    $('#placeholder').css('height','500');
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                    return false;
                    
                }
                
                $('#placeholder').css('height','0');
                $('#tableVentasDiarias').append(row);
            }
    });

}
