$(function () {

    var fec_1 = $('#f_i').val();
    var now_1 = new Date(fec_1);
    now_1.setTime(now_1.getTime() + 1 * 24 * 60 * 60 * 1000);

    var fec_2 = $('#f_f').val();
    var now_2 = new Date(fec_2);
    now_2.setTime(now_2.getTime() + 1 * 24 * 60 * 60 * 1000);

    $('#reportrange').daterangepicker({
        startDate: now_1,
        endDate: now_2
    },
            function (start, end) {
                $('#f_i').val(start.format("YYYY-MM-DD"));
                $('#f_f').val(end.format("YYYY-MM-DD"));
                $('#reportrange span').html(start.format('MMMM D YYYY') + ' - ' + end.format('MMMM D YYYY'));
                document.getElementById("form_mon_robot").submit();
            }
    );

    $(".sea_c").button().click(function () {
        searchContac();
    });

    $(".pie_call").button().click(function () {
        retCalls();
    });

//    $(".pie_call").bind("plotclick", function (event, pos, item) {
//        
//    });

    setInterval(function () {
        $.download('/ipycca/procesos/ventas/co_128', "f_i=" + $('#f_i').val() + "&f_f=" + $('#f_f').val());
    }, 1200000);

    $(".ref_pag").button().click(function () {
        $.download('/ipycca/procesos/ventas/co_128', "f_i=" + $('#f_i').val() + "&f_f=" + $('#f_f').val());
    });

});

function exportExcel(co_can) {
    //exportExcel(<?php echo (int)$r2_Data[$i]['co_can']; ?>)
    var datos = "c_c=" + co_can + "&f_i=" + $('#f_i').val() + "&f_f=" + $('#f_f').val();
    $.download('/ipycca/procesos/ventas/co_128/exportExcel', datos);
}

function retCalls() {

    var url1 = $('#txt_urlGe').val();
    var f_i = $('#f_i').val();
    var f_f = $('#f_f').val();

    var datos = "f_i=" + f_i + "&f_f=" + f_f;

    //$('#tabl_concurre').html('');

    $.ajax({
        type: 'POST',
        url: url1 + 'procesos/ventas/co_128/retCalls',
        data: datos,
        success: function (series) {

            var da = eval('(' + series + ')');

            if (!$.isEmptyObject(da.ch_1)) {

                $('#canvas_call_det').html('<canvas id="canvas_call_det_chart" height="120"></canvas>');
                $(".leg_cha").remove();
                $('#name_call').html("Desde " + f_f + " hasta " + f_f);
                newModal('ModCall', '60', '55', '');

                var row = '';
                var data_1 = {
                    labels: da.ch_1.ticks,
//                    labelsFilter: function (index) {
//                        return (index+1) % 10 !== 0;
//                    },
                    datasets: da.ch_1.datos
                };
                var ctx_1 = document.getElementById('canvas_call_det_chart').getContext('2d');
                var myLineChart_1 = new Chart(ctx_1).LineAlt(data_1, {
                    pointDotRadius: 3,
                    pointHitDetectionRadius: 0.1,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,0.15)",
                    scaleGridLineWidth: 1,
                    responsive: true,
                    animationEasing: "easeOutBounce",
                    legendTemplate: '<div class="leg_cha"><ul class="list-unstyled">'
                            + '<% for (var i=0; i<datasets.length; i++) { %>'
                            + '<li style="float: left; margin-right: 10px;">'
                            + '<span style=\"display: inline-block;width: 10px;height: 10px;border: 1px solid #404040;background-color:<%=datasets[i].strokeColor%>\"></span>'
                            + ' <% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                            + '</li>'
                            + '<% } %>'
                            + '</ul></div>'
                });

                var legendHolder1 = document.createElement('div');
                legendHolder1.innerHTML = myLineChart_1.generateLegend();
                myLineChart_1.chart.canvas.parentNode.parentNode.appendChild(legendHolder1.firstChild);

                var sumT = 0;
                row = row + '<table class="table">';
                row = row + '<tr>';
                row = row + '<th colspan="2">Resumen</th>';
                row = row + '</tr>';

                $.each(da.re_2, function (e, dt) {
                    sumT += parseInt(dt.ca_dial);
                    row = row + '<tr>';
                    row = row + '<td>' + dt.ou_dial + '</td>';
                    row = row + '<td class="derecha">' + dt.ca_dial + '</td>';
                    row = row + '</tr>';
                });
                row = row + '<tr>';
                row = row + '<td style="font-weight:bold;">Total</td>';
                row = row + '<td class="derecha">' + sumT + '</td>';
                row = row + '</tr>';
                row = row + '</table>';

                $('#canvas_call_res').html(row);

            } else {
                //$('.ch_ro_1').remove();
            }

        }
    });

}

function searchContac() {

    var urlGe = $('#txt_urlGe').val();
    var c_b = $('#txt_search').val();
    var a = c_b + '|' + 'Busqueda';

    if (isEmptySis(a)) {

        dataLoading();

        $.ajax({
            type: 'POST',
            url: urlGe + 'procesos/ventas/co_128/seaNum',
            data: 'c_b=' + c_b,
            success: function (returnData) {

                removedataLoading();

                var datos = eval('(' + returnData + ')');

                if (!$.isEmptyObject(datos)) {

                    var row = '';

                    $('#name_bus').html(c_b);
                    console.log(datos);

                    $('._co_cli').html(datos[0].co_cli);
                    $('._ce_cli').html(datos[0].ce_cli);
                    $('._no_cli').html(datos[0].ap_cli + " " + datos[0].no_cli);

                    $('._di_ven').html(datos[0].va_dia);
                    $('._pa_min').html('$' + datos[0].va_min);
                    $('._pa_ven').html('$' + datos[0].va_ven);
                    $('._va_sal').html('$' + datos[0].va_sal);
                    $('._es_cab').html(datos[0].no_est_cab);

                    $.each(datos, function (e, da) {
                        row = row + '<tr>';
                        row = row + '<td>' + da.ds_cam + '</td>';
                        row = row + '<td>' + da.co_sec + '</td>';
                        row = row + '<td>' + da.co_con + '</td>';
                        row = row + '<td>' + da.ds_can + '</td>';
                        row = row + '<td>' + da.no_pro + '</td>';
                        row = row + '<td>' + da.tx_men + '</td>';
                        row = row + '<td>' + da.no_est_det + '</td>';
                        row = row + '<td>' + da.va_dia + '</td>';
                        row = row + '<td>' + da.va_pre + '</td>';
                        row = row + '<td>' + da.fe_env + '</td>';
                        row = row + '<td>' + da.tx_res + '</td>';
                        row = row + '<td>' + da.fe_gen + '</td>';
                        row = row + '</tr>';
                    });

                    $('#reg_busq').html(row);

                    newModal('ModBus', '80', '70', '');

                } else {
                    errorGeneral("Sistema Intranet", "Sin datos en la busqueda realizada.", 'danger');
                }

            }

        });

    }

}
