$(function() {
    retCanales();
    
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function(html) {
        var switchery = new Switchery(html,{ size: 'small'});
        var changeCheckbox = document.querySelector('#enable_job');
        changeCheckbox.onchange = function() {
            var url1     = $('#txt_urlGe').val();
            var id_job   = $('#i_j').val();
            var check = 0;
            if(changeCheckbox.checked){
                check = 1;
            }

            if(va_sess()){
                 $.ajax({
                    type : 'POST',
                    url : url1+"procesos/ventas/co_128/up_job", 
                    data: "ck="+check+"&i_j="+id_job,
                    success : function (returnData) { 
                        var datta = eval('('+returnData+')');
                        if (datta.data){
                            if (datta.data.co_err != 0){
                                errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                            }
                        } else {
                            errorGeneral("Sistema Intranet",returnData,'danger');
                            return false;
                        }
                    }
                });
            } else {
                redirectIntranet();
            }

        };
        
    });

    $(".enable_prov").change(function() {
        
        var url1     = $('#txt_urlGe').val();
        var check = 0;
        
        if(this.checked) {
            check = 1;
        }
        
        if(va_sess()){
            $.ajax({
               type : 'POST',
               url : url1+"procesos/ventas/co_128/up_prov", 
               data: "ck="+check+"&c_p="+$(this).attr('data-label'),
               success : function (returnData) { 
                   var datta = eval('('+returnData+')');
                   if (datta.data){
                       if (datta.data.co_err != 0){
                           errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                       }
                   } else {
                       errorGeneral("Sistema Intranet",returnData,'danger');
                       return false;
                   }
               }
           });
       } else {
           redirectIntranet();
       }
        
    });
    
    $(".enable_camp").change(function() {
        
        var url1     = $('#txt_urlGe').val();
        var check = 'I';
        
        if(this.checked) {
            check = 'A';
        }
        
        if(va_sess()){
            $.ajax({
               type : 'POST',
               url : url1+"procesos/ventas/co_128/up_camp", 
               data: "ck="+check+"&c_c="+$(this).attr('data-label'),
               success : function (returnData) { 
                   var datta = eval('('+returnData+')');
                   if (datta.data){
                       if (datta.data.co_err != 0){
                           errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                       }
                   } else {
                       errorGeneral("Sistema Intranet",returnData,'danger');
                       return false;
                   }
               }
           });
       } else {
           redirectIntranet();
       }
        
    });

    $('.leftPage').click(function( e ){
        e.preventDefault();
        $("#slide").animate({left:"-100%"},"fast");
    }); 
    
    $('.rightPage').click(function( e ){
        e.preventDefault();
        $("#slide").animate({left:"0"},"fast");
    }); 
    
    $('.reportrange').datetimepicker({
        format:'Y-m-d',
        lang:'es',
        timepicker:false,
        closeOnDateSelect:true,
        onSelectDate:function(ct,$i){
            ct.dateFormat('Y-m-d');
            $('#f_i').val(ct.dateFormat('Y-m-d'));
            $('#f_f').val(ct.dateFormat('Y-m-d'));
            $i.datetimepicker('destroy');
            //$('#reportrange span').html(start.format('MMMM ,D YYYY') + ' - ' + end.format('MMMM ,D YYYY'));
            document.getElementById("form_mon_robot").submit();
        }
    });
    
    $( ".sea_c" ).button().click(function(){
        searchContac();
    });
    
    $( ".ini_job" ).button().click(function(){
        iniJob();
    });
    
    $("#cmb_chart_lines").change(function() {
        retCanales();
    });
    
    $(".det_call").change(function() {
        retCalls();
    });

    $(".pie_call").bind("plotclick", function (event, pos, item) {
        retCalls();
    });

    setInterval(function(){ 
        $.download('/ipycca/procesos/ventas/co_128',"f_i="+$('#f_i').val()+"&f_f="+$('#f_f').val());
    }, 1200000);
        
        
}); 

function exportExcel(co_camp){
    var datos = "c_c="+co_camp+"&f_f="+$('#f_f').val(); 
    $.download('/ipycca/procesos/ventas/co_128/exportExcel',datos);
}

function iniJob(){
    if(va_sess()){
        
        var q = 0;
        var url1 = $('#txt_urlGe').val();
        
        dataGeneralLoading('panels-wrap');
        
        $.ajax({
            type : 'POST',
            async: false,
            url : url1+"procesos/ventas/co_128/in_job", 
            success : function (returnData) { 
                ///errorGeneral("Sistema Intranet",returnData,'danger');
            }
        });
        
        var inter = setInterval(function(){
            q++;
            $.ajax({
                type : 'POST',
                async: false,
                url : url1+"procesos/ventas/co_128/st_job", 
                success : function (returnData) { 
                    var data = eval('('+returnData+')');
                    if(data){
                        
                        $('#job_ini').html(data.run_Requested_date);
                        $('#job_ter').html(data.stop_execution_date);
                        $('#job_dem').html(secondToHour(data.ElapsedSeconds));
                        $('#job_men').html(data.last_outcome_message);
                        
                        if(data.stop_execution_date.trim() != ''){
                            removeGeneralLoading('panels-wrap');
                            clearInterval(inter);
                            return false;
                        }
                    } else {
                        removeGeneralLoading('panels-wrap');
                        clearInterval(inter);
                        errorGeneral("Sistema Intranet",returnData,'danger');
                        return false;
                    }
                }
            });
        },1000);
        
        
    } else {
        redirectIntranet();
    }
}

function retCalls(){
    
    var url1 = $('#txt_urlGe').val();
    var f_f = $('#f_f').val();
    
    var datos = "f_f="+f_f;
    
    //$('#tabl_concurre').html('');
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/ventas/co_128/retCalls',
        data: datos,
        success : function (series) {
            
            var da = eval('('+series+')');
            
            if(!$.isEmptyObject(da.ch_1)) {
                
                $('#canvas_call_det').html('<canvas id="canvas_call_det_chart" height="120"></canvas>');
                $(".leg_cha").remove();
                $('#name_call').html(f_f);
                newModal('ModCall','60','55','');
                
                var row = '';
                var data_1 = {
                    labels : da.ch_1.ticks,
//                    labelsFilter: function (index) {
//                        return (index+1) % 10 !== 0;
//                    },
                    datasets : da.ch_1.datos
                };
                var ctx_1 = document.getElementById('canvas_call_det_chart').getContext('2d');
                var myLineChart_1 = new Chart(ctx_1).LineAlt(data_1,{
                    pointDotRadius : 3,
                    pointHitDetectionRadius : 0.1,
                    scaleShowGridLines : true,
                    scaleGridLineColor : "rgba(0,0,0,0.15)",
                    scaleGridLineWidth : 1,
                    responsive: true,
                    animationEasing : "easeOutBounce",
                    legendTemplate : '<div class="leg_cha"><ul class="list-unstyled">'
                    +'<% for (var i=0; i<datasets.length; i++) { %>'
                    +'<li style="float: left; margin-right: 10px;">'
                    +'<span style=\"display: inline-block;width: 10px;height: 10px;border: 1px solid #404040;background-color:<%=datasets[i].strokeColor%>\"></span>'
                    +' <% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                    +'</li>'
                    +'<% } %>'
                    +'</ul></div>'
                });

                var legendHolder1 = document.createElement('div');
                legendHolder1.innerHTML = myLineChart_1.generateLegend();
                myLineChart_1.chart.canvas.parentNode.parentNode.appendChild(legendHolder1.firstChild);
                
                var sumT = 0;
                row = row + '<table class="table">';
                row = row + '<tr>';
                row = row + '<th colspan="2">Resumen</th>';
                row = row + '</tr>';
                
                $.each(da.re_2,function(e,dt){
                    sumT+=parseInt(dt.ca_dial);
                    row = row + '<tr>';
                        row = row + '<td>'+dt.ou_dial+'</td>';
                        row = row + '<td class="derecha">'+dt.ca_dial+'</td>';
                    row = row + '</tr>';
                });
                row = row + '<tr>';
                        row = row + '<td style="font-weight:bold;">Total</td>';
                        row = row + '<td class="derecha">'+sumT+'</td>';
                    row = row + '</tr>';
                row = row + '</table>';
                
                $('#canvas_call_res').html(row);

            } else {
                //$('.ch_ro_1').remove();
            }
            
        }
    });
                
}

function retCanales(){
    
    var url1 = $('#txt_urlGe').val();
    var co_ca = $('#cmb_chart_lines').val();
    var f_f = $('#f_f').val();
    
    var datos = "c_a="+co_ca+"&f_f="+f_f;
    
    $('#tabl_concurre').html('');
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/ventas/co_128/retCanales',
        data: datos,
        success : function (series) {
            
            $('#canvas_ca_det').html('<canvas id="canvas_ca_det_chart" height="52"></canvas>');
            $('#canvas_ca_tot').html('<canvas id="canvas_ca_tot_chart" height="52"></canvas>');
            $(".leg_cha").remove();
            
            var da = eval('('+series+')');
            
            if(!$.isEmptyObject(da.ch_1)) {

                var data_1 = {
                    labels : da.ch_1.ticks,
                    labelsFilter: function (index) {
                        return (index+1) % 10 !== 0;
                    },
                    datasets : da.ch_1.datos
                };
                var ctx_1 = document.getElementById('canvas_ca_det_chart').getContext('2d');
                var myLineChart_1 = new Chart(ctx_1).LineAlt(data_1,{
                    pointDotRadius : 1.5,
                    pointHitDetectionRadius : 0.1,
                    scaleShowGridLines : true,
                    scaleGridLineColor : "rgba(0,0,0,0.15)",
                    scaleGridLineWidth : 1,
                    responsive: true,
                    animationEasing : "easeOutBounce",
                    legendTemplate : '<div class="leg_cha"><ul class="list-unstyled">'
                    +'<% for (var i=0; i<datasets.length; i++) { %>'
                    +'<li style="float: left; margin-right: 10px;">'
                    +'<span style=\"display: inline-block;width: 10px;height: 10px;border: 1px solid #404040;background-color:<%=datasets[i].strokeColor%>\"></span>'
                    +' <% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                    +'</li>'
                    +'<% } %>'
                    +'</ul></div>'
                });

                var legendHolder1 = document.createElement('div');
                legendHolder1.innerHTML = myLineChart_1.generateLegend();
                myLineChart_1.chart.canvas.parentNode.parentNode.appendChild(legendHolder1.firstChild);

            } else {
                //$('.ch_ro_1').remove();
            }
            
            if(!$.isEmptyObject(da.ch_2)) {
                            
                var data_2 = {
                    labels : da.ch_2.ticks,
                    labelsFilter: function (index) {
                        return (index+1) % 10 !== 0;
                    },
                    datasets : da.ch_2.datos
                };
                var ctx_2 = document.getElementById('canvas_ca_tot_chart').getContext('2d');
                var myLineChart_2 = new Chart(ctx_2).LineAlt(data_2,{
                    pointDotRadius : 0.2,
                    pointHitDetectionRadius : 0.1,
                    scaleShowGridLines : true,
                    scaleGridLineColor : "rgba(0,0,0,0.15)",
                    scaleGridLineWidth : 1,
                    responsive: true,
                    animationEasing : "easeOutBounce",
                    legendTemplate : '<div class="leg_cha"><ul class="list-unstyled">'
                    +'<% for (var i=0; i<datasets.length; i++) { %>'
                    +'<li style="float: left; margin-right: 10px;">'
                    +'<span style=\"display: inline-block;width: 10px;height: 10px;border: 1px solid #404040;background-color:<%=datasets[i].strokeColor%>\"></span>'
                    +' <% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                    +'</li>'
                    +'<% } %>'
                    +'</ul></div>'
                });

                var legendHolder1 = document.createElement('div');
                legendHolder1.innerHTML = myLineChart_2.generateLegend();
                myLineChart_2.chart.canvas.parentNode.parentNode.appendChild(legendHolder1.firstChild);
                
                if(!$.isEmptyObject(da.ch_3)) {
                    
                    var row  =  '<table class="tab_concurrencia">';
                    row = row + '<thead>';
                    row = row + '<tr>';

                    $.each(da.ch_3,function(e,dt){
                        row = row + '<th style="background:'+da.ch_2.datos[0].pointColor+';">'+dt.call+'</th>';
                    });

                    row = row + '</tr>';
                    row = row + '</thead>';
                    row = row + '<tbody>';
                    row = row + '<tr>';

                    $.each(da.ch_3,function(e,db){
                        row = row + '<td style="border: 2px solid '+da.ch_2.datos[0].pointColor+';">'+db.veces+'</td>';
                    });

                    row = row + '</tr>';
                    row = row + '</tbody>';
                    row = row + '</table>';

                    $('#tabl_concurre').html(row);

                }
                
            } else {
                $('.ch_ro_2').remove();
            }
            
        }
    });
                
}

function viewReg(c_r,acc){
    
    if(va_sess()){

        var url1 = $('#txt_urlGe').val();
        $('.fe_hora').val('00:00');
        var datos = "c_r="+c_r;
        $('#co_reg').val(c_r);
        
        $('#m_n1').addClass('active');
        $('#tab1').addClass('active');
        $('#m_n2').removeClass('active');
        $('#m_n3').removeClass('active');
        $('#tab2').removeClass('active');
        $('#tab3').removeClass('active');

        dataLoading();

        $.ajax({
            type : 'POST',
            url : url1+'procesos/ventas/co_128/retReg',
            data: datos,
            success : function (returnData) {
                
                removedataLoading();
                
                var datos = eval('('+returnData+')');
                
                if(!$.isEmptyObject(datos.res_1)) {

                    var row = '';
                    var can = '';

                    $('#name_reg').html(datos.res_1.co_reg+" - "+datos.res_1.ds_reg);
                    $('#txt_des').html(datos.res_1.ds_reg);
                    $('#cmb_dia').html(datos.res_1.va_dia);
                    $('#cmb_int').html(datos.res_1.va_int);
                    $('#cmb_top').html(datos.res_1.va_top);
                    $('#cmb_est').html(datos.res_1.co_est);
                    
                    if(!$.isEmptyObject(datos.res_2)) {
                        $.each(datos.res_2,function(e,d2){
                            row = row + '<tr id="opt_para_'+d2.co_par+'">';
                            row = row + '<td><p class="tab_co_para">'+d2.co_par+'</p></td>';
                            row = row + '<td>'+d2.ds_par+'</td>';
                            row = row + '<td class="derecha"><p class="tab_va_para">'+d2.va_par+'</p></td>';
                            row = row + '</tr>';
                        });
                    }
                    
                    $('#reg_para').html(row);

                    if(!$.isEmptyObject(datos.res_3)) {
                        $.each(datos.res_3,function(e,d3){
                            can = can + '<li class="ui-state-default" id="opt_cana_'+d3.co_can+'">';
                            can = can + '<div class="external-event">';
                            can = can + '<div style="position: relative;">';
                            can = can + '<b>'+d3.ds_can+'</b></br>';
                            can = can + '<label>'+d3.me_can+'</label>';
                            can = can + '</div>';
                            can = can + '<div class="clearfix"></div>';
                            can = can + '</div>';
                            can = can + '</li>';
                        });
                    }
                    
                    $('#contCanalAsig').html(can);
                    
                    if(!$.isEmptyObject(datos.res_4)) {
                        $.each(datos.res_4,function(e,d4){
                            $('#box_fe_hora_'+d4.di_hor).removeClass('box menu-color-8');
                            $('#box_fe_hora_'+d4.di_hor).addClass('box menu-color-1');
                            $('#fe_hora_'+d4.di_hor+'_1').val(d4.fe_des);
                            $('#fe_hora_'+d4.di_hor+'_2').val(d4.fe_has);
                        });
                    }

                    newModal('ModReg','60','75','form_ModReg');

                } else {
                    errorGeneral("Sistema Intranet",datos.err,'danger');
                }
            }

        });

    } else {
        redirectIntranet();
    }
}

function searchContac (){
    
    var urlGe = $('#txt_urlGe').val();
    var c_b = $('#txt_search').val();
    var f_f = $('#f_f').val();
    var a = c_b+'|'+'Busqueda';

    if(isEmptySis(a)){
        
        dataLoading();
        
        $.ajax({
            type : 'POST',
            url : urlGe+'procesos/ventas/co_128/seaNum',
            data: 'c_b='+c_b+'&f_f='+f_f,
            success : function (returnData) {
                
                removedataLoading();
                
                var datos = eval('('+returnData+')');
                
                if(!$.isEmptyObject(datos)) {
                    
                    var row = '';
                    
                    $('#name_bus').html(c_b);
                    console.log(datos);
                    
                    $('._co_cli').html(datos[0].co_cli);
                    $('._ce_cli').html(datos[0].ce_cli);
                    $('._no_cli').html(datos[0].ap_cli +" "+datos[0].no_cli);
                    
                    $('._di_ven').html(datos[0].va_dia);
                    $('._pa_min').html('$'+datos[0].va_min);
                    $('._pa_ven').html('$'+datos[0].va_ven);
                    $('._va_sal').html('$'+datos[0].va_sal);
                    $('._es_cab').html(datos[0].no_est_cab);
                    
                    $.each(datos,function(e,da){
                        row = row + '<tr>';
                        row = row + '<td>'+da.ds_cam+'</td>';
                        row = row + '<td>'+da.co_sec+'</td>';
                        row = row + '<td>'+da.co_con+'</td>';
                        row = row + '<td>'+da.ds_can+'</td>';
                        row = row + '<td>'+da.no_pro+'</td>';
                        row = row + '<td>'+da.tx_men+'</td>';
                        row = row + '<td>'+da.no_est_det+'</td>';
                        row = row + '<td>'+da.va_dia+'</td>';
                        row = row + '<td>'+da.va_pre+'</td>';
                        row = row + '<td>'+da.fe_env+'</td>';
                        row = row + '<td>'+da.tx_res+'</td>';
                        row = row + '</tr>';
                    });
                    
                    $('#reg_busq').html(row);
                    
                    newModal('ModBus','80','70','');

                } else {
                    errorGeneral("Sistema Intranet","Sin datos en la busqueda realizada.",'danger');
                }
                
            }
            
        });
        
    }
    
}
