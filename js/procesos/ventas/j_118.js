
function getDocProv(){
    
    var url_1 = $('#txt_urlGe').val();
    var fi = $('#txt_fec_ini').val();
    var ff = $('#txt_fec_fin').val();
    var tip = $('#cmb_prov').val();
    
    if (tip.trim() === 'NNN'){
        errorGeneral("Sistema Intranet",'Debe seleccionar un proveedor','danger');
        return false;
    }
    
    if (fi.trim() === ''){
        errorGeneral("Sistema Intranet",'Campo Fecha Inicial esta vacio','danger');
        return false;
    }
    
    if (ff.trim() === ''){
        errorGeneral("Sistema Intranet",'Campo Fecha Final esta vacio','danger');
        return false;
    }
    
    var datos = "f_i="+fi+"&f_f="+ff+"&t_p="+tip;
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url_1+'procesos/ventas/co_118/reDoc',
        data: datos,
        success : function (retData) {
            
            removedataLoading();
            $('#table_con_ind').html('');
            $('#total').hide();
            
            retData = eval('('+retData+')');
            
            var sum_a = 0,sum_b = 0,sum_c = 0;
            
            if(!$.isEmptyObject(retData.data)) {

                var row = '<table id="T_docCI" class="table table-bordered table-hover">';
                row = row+'<thead style="background: #2E8BEF;color: #FFF;">';
                row = row+'<th>Fecha Doc</th>';
                row = row+'<th>Factura</th>';
                row = row+'<th>Cod. Art&iacute;culo</th>';
                row = row+'<th></th>';
                row = row+'<th># Serie</th>';
                row = row+'<th>Fecha Envio</th>';
                row = row+'<th>Fecha Estimada</th>';
                row = row+'<th>Fecha Cancelaci&oacute;n</th>';
                row = row+'<th>Estado</th>';
                row = row+'<th></th>';
                row = row+'</thead>';
                row = row+'<tbody>';

                $.each(retData.data,function(e,dat){
                    
                    if(dat.cod_esta === 'A'){
                        sum_a+=parseFloat(dat.val_tot);
                    } else if(dat.cod_esta === 'E'){
                        sum_b+=parseFloat(dat.val_tot);
                    } else if(dat.cod_esta === 'C'){
                        sum_c+=parseFloat(dat.val_tot);
                    }
                    
                    row = row+'<tr>';
                    row = row+'<td class="centro">'+dat.fec_tra+'</td>';
                    row = row+'<td class="centro">'+dat.num_fac+'</td>';
                    row = row+'<td class="centro">'+dat.cod_art+'</td>';
                    row = row+'<td class="centro">'+dat.des_art+'</td>';
                    row = row+'<td>'+dat.num_ser+'</td>';
                    row = row+'<td class="centro">'+dat.fec_env+'</td>';
                    row = row+'<td class="centro '+dat.cla_esti+'">'+dat.fec_esti+'</td>';
                    row = row+'<td class="centro">'+dat.fec_canc+'</td>';
                    row = row+'<td class="centro">'+dat.nom_esta+'</td>';
                    row = row+'<td class="centro"><button type="button" class="btn btn-circle btn-success" onclick="_v_det('+"'"+encodeURIComponent(JSON.stringify(dat)).replace(/['"]/g,"-")+"'"+')"><i class="text-blanco text-size-1 fa fa-plus"></i></button>';
                    row = row+'</tr>';
                });
                
                row = row+'</tbody>';
                row = row+'</table>';
                
                $('.tot_pen').html(formato_numero(roundTo(sum_a),2,".",","));
                $('.tot_env').html(formato_numero(roundTo(sum_b),2,".",","));
                $('.tot_can').html(formato_numero(roundTo(sum_c),2,".",","));

                $('#table_con_ind').html(row);
                $('#total').show();
                
                $('#T_docCI').dataTable();

            } else {
                if(retData.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    return false;
                } else {
                    errorGeneral("Sistema Intranet",retData,'danger'); 
                    return false;
                }
            }

        }
    });

}

function _v_det(data){
    data = decodeURIComponent(data);
    var d = $.parseJSON(data);
    var r = '',s = '';
    
    if(!$.isEmptyObject(d)) {
        $('#reemGeAl').html(d.num_fac);
        $('._i_cli').html(d.ide_cli);
        $('._n_cli').html(d.nom_cli);
        $('._f_fac').html(d.fec_tra);
        
        r = r +'<tr>';
            r = r +'<td>'+d.cod_art+'</td>';
            r = r +'<td>'+d.des_art+'</td>';
            r = r +'<td>'+d.num_ser+'</td>';
            r = r +'<td class="derecha">'+d.val_cuo+'</td>';
            r = r +'<td class="derecha">'+d.val_tot+'</td>';
        r = r +'</tr>';
        
        $('._f_env').html(d.fec_env);
        $('._u_env').html(d.nomb_user_env);
        $('._f_can').html(d.fec_canc);
        $('._u_can').html(d.nomb_user_can);
        $('._f_est').html(d.fec_esti);
        
        $('#det_art_fac').html(r);
        
        if (d.cod_esta.toString().trim() === 'A'){
            s = s +'<button type="button" class="btn btn-danger cancelSolict pull-right sendEnvi" onclick="_s_upd('+"'E'"+','+"'"+d.cod_bdg+"'"+','+"'"+d.cod_caj+"'"+','+"'"+d.num_doc+"'"+','+"'"+d.cod_tra+"'"+','+"'"+d.tip_doc+"'"+')" style="margin-right: 10px;">Enviar Documento</button>';
        } else if (d.cod_esta.toString().trim() === 'E'){
            s = s +'<button type="button" class="btn btn-success finSolict pull-right sendConf" onclick="_s_upd('+"'C'"+','+"'"+d.cod_bdg+"'"+','+"'"+d.cod_caj+"'"+','+"'"+d.num_doc+"'"+','+"'"+d.cod_tra+"'"+','+"'"+d.tip_doc+"'"+')">Confirmado</button>';
            s = s +'<button type="button" class="btn btn-danger cancelSolict pull-right sendEnvi" onclick="_s_upd('+"'E'"+','+"'"+d.cod_bdg+"'"+','+"'"+d.cod_caj+"'"+','+"'"+d.num_doc+"'"+','+"'"+d.cod_tra+"'"+','+"'"+d.tip_doc+"'"+')" style="margin-right: 10px;">Enviar Documento</button>';
        }
        
        $('.buttons').html(s);
        newModal('ViewDet','75','50','');
    }
}

function _s_upd(a,b,c,d,e,f){
    
    dataLoading();
    
    var datos = "e_d="+a+"&c_b="+b+"&c_c="+c+"&n_d="+d+"&c_t="+e+"&t_t="+f;
    var url_1 = $('#txt_urlGe').val();
    
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/ventas/co_118/updaEstd", 
        data: datos,
        success : function (retData) {
            removedataLoading();
            if (retData.trim().substr(0,2) === "OK"){
                var resp = retData.trim().substr(3);
                quitarmodalGeneral('ViewDet','');
                errorGeneral("Sistema Intranet",resp,'success'); 
                getDocProv();
            } else {
                errorGeneral("Sistema Intranet",retData,'danger'); 
            }
        }
    });
}

$(function() {
    
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function(ct){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            });
        },
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            });
        },
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $( ".searchDoc" ).button().click(function(){
        getDocProv();
    });
    
});
