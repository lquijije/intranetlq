
function saveTicket(a,b){
    if(va_sess()){
        $('#viewSave').html('');

        var c = b == 'U'? a : 1;
        var j = '';
        var k = [];
        var l = [];

        for (var p=c; p<=a; p++){

            var m = {};
            var g = $("input[name='plato_"+p+"']:checked");
            var h = $("input[name='acomp_"+p+"']:checked");

            $(g).each( function () {
                var fec = $('#fech_'+p).val();
                var now = new Date(fec);
                now.setTime(now.getTime()+1*24*60*60*1000);
                var myDate = now.customFormat("#DDDD# #DD#"); 

                j = j + '<h4 style="font-weight: bold;">Para el d&iacute;a '+myDate+' usted va a comprar:</h4>';
                j = j + '<div class="box box-primary" style="padding:10px;"><i class="fa fa-ticket"></i> ' +$(this).attr('data_label');

                m["id"] = $(this).attr('id');            
                m["fecha"] = fec;            
                m["text_1"] = $(this).attr('data_label');
                if($('#json_'+p).val().toString().trim() !== 'null'){
                    l.push($.parseJSON($('#json_'+p).val()));
                }
            });

            m["text_2"] = '';

            $(h).each( function () {
                m["text_2"] = $(this).attr('id');
                if(!$.isEmptyObject(m["text_2"])){
                    j = j +' CON '+$(this).attr('id');
                }
            });

            if(!$.isEmptyObject(m["id"])) {
                k.push(m);
            }

            j = j + '</div>';

        }

        j = j + 'Si est&aacute; seguro(a) presione Aceptar, caso contrario presione Cancelar'; 
        j = j + '<div class="spa-12" style="margin-top:10px;">';
        j = j + '<button type="button" class="btn btn-danger pull-right" onclick="quitarmodalGeneral('+"'Ticket',''"+');" style="margin-left:10px;"><i class="fa fa-times"></i> Cancelar</button>'; 
        j = j + '<button type="button" class="btn btn-success pull-right" onclick="sendMenu('+"'"+encodeURIComponent(JSON.stringify(k)).replace(/['"]/g,"-")+"'"+');"><i class="fa fa-save"></i> Aceptar</button></div>';

        if ($.isEmptyObject(k)){
            errorGeneral('Sistema Intranet','Debe seleccionar un tipo de plato','danger');
            return false;
        }

        viewtick(JSON.stringify(l));

        $('#viewSave').append(j);
    } else {
        redirectIntranet();
    }
    
}

function sendMenu(data){
    
    quitarmodalGeneral('Ticket','');
    data = decodeURIComponent(data);
    data = JSON.stringify(data);
    
    var url1 = $('#txt_urlGe').val();
    var datos = 'j_n='+data;
    if(va_sess()){
        dataLoading();
        $.ajax({
            type :'POST',
            url :url1+"procesos/comedor/co_116/s_tic", 
            data:datos,
            success : function (retData){
                removedataLoading();
                var d = eval('('+retData+')');
                if(d.err == ''){
                    if(parseInt(d.data.co_err) == 0){
                        errorGeneral("Sistema Intranet",'Grabado correctamente','success');
                        window.location = window.location.href;
                    } else {
                        errorGeneral("Sistema Intranet",d.data.tx_err,'danger');
                    }
                } else {
                    errorGeneral("Sistema Intranet",d.err,'danger');
                }
            }
        });
    } else {
        redirectIntranet();
    }
    
}

function viewtick(data){
    
    $('#viewSave').html('');
    $('#viewTick').html('');
    var datos = $.parseJSON(data);
    var row = '';
    
    $.each(datos,function(e,dato){
        row = row + '<h4 style="font-weight: bold;">Para el d&iacute;a '+dato.fecha+' usted tiene comprado:</h4>';
        $.each(dato.item,function(e,da){
            row = row + '<div class="box" style="padding:10px;background:#1DC096;color:#FFF;"><i class="fa fa-ticket"></i> ' +da.des;
            if(da.aco !== ''){ row = row +' CON '+da.aco; }
            row = row + '</div>';
        });
    });
    
    $('#viewTick').append(row);
    newModal('Ticket','40','55','');
    
}
    
$(function() {
    $(".helpCome" ).button().click(function(){
        newModal('Help','60','95','');
    });
});