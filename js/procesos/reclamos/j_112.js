var i_count = 0;

$(function() {
    
    $(".searchCI").click(function() {
        getReclamos('');
    });
    
    $(".add_option" ).click(function() {
        addOption('');
    });
    
    
    $('#form_ViewDoc').bind('submit', function() {
        var msgText = $('#area_ingre').val();
        $('#area_ingre').val('');
        if (msgText != ''){
            addOption(msgText);
            saveACC($('#dp_to').val(),$('#co_re').val(),msgText);
        }
        return false;
    });
    
    getReclamos(1);
    
}); 

function getReclamos(pass){
    
    var cod_cli = $('#txt_ci').val();
    var url1 = $('#txt_urlGe').val();
    
    if (cod_cli.toString().trim() === '' && pass === ''){
        errorGeneral("Sistema Intranet",'Debe ingresar la identificaci&oacute;n','danger'); 
        return false;
    }
    
    var datos = "c_c="+cod_cli;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/reclamos/co_112/retReclamos',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();
            
            var data = eval('('+returnData+')');
            $('#detalleReclamos').html('');    

            var row = '<table id="T_DocPen" class="table table-bordered table-hover">';
            row = row+'<thead>';
            row = row+'<th>Identificaci&oacute;n</th>';
            row = row+'<th>Cliente</th>';
            row = row+'<th>Almac&eacute;n</th>';
            row = row+'<th>Reclamo</th>';
            row = row+'<th>Tipo</th>';
            row = row+'<th>Descripcion</th>';
            row = row+'<th>Fecha</th>';
            row = row+'<th>Estado</th>';
            row = row+'<th></th>';
            row = row+'</thead>';
            row = row+'<tbody>';
                
            if (!$.isEmptyObject(data.data)){ 
                $.each(data.data,function(e,dato){
                    
                    var tipo = dato.tip_rcl === 'B'?'Bien':'Servicio';
                    var estaDO = dato.est_rcl === 'I'?'INGRESADA':'FINALIZADA';
                    
                    row = row+'<tr>';
                    row = row+'<td>'+dato.id_cli+'</td>';
                    row = row+'<td>'+dato.nom_cli+'</td>';
                    row = row+'<td>'+dato.nom_alm+'</td>';
                    row = row+'<td>'+dato.cod_rcl+'</td>';
                    row = row+'<td>'+tipo+'</td>';
                    row = row+'<td>'+dato.des_abr+'</td>';
                    row = row+'<td>'+dato.fec_rcl+'</td>';
                    row = row+'<td>'+estaDO+'</td>';
                    row = row+'<td class="centro"><button type="button" onclick="viewRec('+"'"+cod_cli+"'"+','+"'"+dato.cod_rcl+"'"+','+"'"+dato.nom_alm+"'"+','+"'"+dato.cod_alm+"'"+')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                    row = row+'</tr>';
                    
                });

            } else {            
                
                if(data.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
            
            row = row+'</tbody>';
            row = row+'</table>';
            
            $('#detalleReclamos').append(row);
            
            $('#T_DocPen').dataTable();

        }
    });

}

function viewRec(ci,cr,a,b){
    
    $('#table_cont_det').empty();
    $('#cont_cab').empty();
    $('#name_pro').html(a);
    $('#num_doc_fac').html(cr);
    $('#dp_to').val(b);
    $('#co_re').val(cr);
    
    var url1 = $('#txt_urlGe').val();
    var datos = "c_c="+ci+"&c_r="+cr+"&c_b="+b;
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/reclamos/co_112/viewReclamo',
        data: datos,
        success : function (returnData) {
            
                removedataLoading();
                
                var data = eval('('+returnData+')');
                
                if (!$.isEmptyObject(data.cab)) {
                    
                    $('#cont_cab').append(data.cab);
                    if (!$.isEmptyObject(data.det)) {
                        $.each(data.det,function(e,dato){
                            addOption(dato.des_det);
                        });
                    }
                } else {
                    
                    if(data.cab === null){
                        errorGeneral('Sistema Intranet','No se encuentran datos del reclamo.<br>Comuniquese con sistemas','danger');
                        return false;
                    } else {
                        errorGeneral('Sistema Intranet',returnData,'danger');
                        return false;
                    }

                }
                
                newModal('ViewDoc','75','70','form_ViewDoc');
                
            }
    });

}

function addOption(des){
    
    if ($('#area_ingre').val().toString().trim() === '' && des === ''){
        errorGeneral('Sistema Intranet','No ha ingresado la observaci&oacute;n de la acci&oacute;n','danger');
        return false;
    }
    
    i_count++;
    
//    if (i_count === 1){
//        $('#table_cont_det').empty();
//    }
    
    var descrip = des === ''?$('#area_ingre').val():des;
    
    var tab = '';
    tab = tab+'<tr id="option-'+i_count+'">';
    tab = tab+'<td class="obs_acc">'+descrip+'</td>';
//    tab = tab+'<td class="centro">';
//    tab = tab+'<button type="button" onclick="removeOption('+i_count+')" class="btn btn-circle btn-danger add_option"><i class="text-blanco fa fa-minus"></i></button>';
//    tab = tab+'</td>';
    tab = tab+'</tr>';
    
    $('#table_cont_det').append(tab);
    $('#area_ingre').val('');
    
}

function endReclamo(a,b){
    
    if(confirm('Esta seguro que desea finalizar el reclamo #'+b+' ?')){ 
        dataLoading();
        var url1 = $('#txt_urlGe').val();  
        var datos = 'c_b='+a+'&c_r='+b;

        $.ajax({
            type : 'POST',
            url : url1+'procesos/reclamos/co_112/finReclamo',
            data: datos,
            success : function (returnData) {

                removedataLoading();

                var data = eval('('+returnData+')');

                if (data.data){

                    $.each(data,function(e,dat){
                        if (dat.a === 2){
                            errorGeneral("Sistema Intranet",dat.b,'success');
                            i_count = 0;
                            getReclamos();
                            quitarmodalGeneral('ViewDoc','form_ViewDoc');
                        } else {
                            errorGeneral("Sistema Intranet",dat.b,'danger');
                            return false;
                        }
                    });

                } else {
                    errorGeneral("Sistema Intranet",returnData,'danger');
                    return false;
                }

            }
        });
    }

}

function saveACC(alm,id,des){
            
    var url1 = $('#txt_urlGe').val();
        
    dataLoading();
        
    var datos = 'd_e='+des+'&c_a='+alm+'&c_r='+id;

    $.ajax({
        type : 'POST',
        url : url1+'procesos/reclamos/co_112/saveAcciones',
        data: datos,
        success : function (returnData) {

            removedataLoading();

            var data = eval('('+returnData+')');

            if (data.data){

                $.each(data,function(e,dat){
                    if (dat.a != 2){
                        errorGeneral("Sistema Intranet",dat.b,'danger');
                        return false;
                    }
                });

            } else {
                errorGeneral("Sistema Intranet",returnData,'danger');
                return false;
            }

        }
    });
        
}

function removeOption(id) {
    
    if(confirm('Esta seguro de que desea eliminar este registro ?')){ 
        $('#option-'+id).remove();
    }
    
}

function getPDF(a,b){
    var url1 = $('#txt_urlGe').val();
    window.open(url1+'procesos/reclamos/co_112/viewPDF/'+a+'/'+b+'/Reclamo '+b);
}
