
var __cont = 0;

$(function() {
    
    $( "#cmb_tipo" ).change(function() {
        if($('#txt_identificacion').val().toString().trim() !== ''){
            
            $('#Pass_det_art').empty();
            $('#txt_desc_serv').html('');
            $('#txt_num_doc').val('');
            
            if (this.value.trim() === 'B'){
                $("#ifS").css('display','none');
                $("#ifB").css('display','block');
                $("#ifA").css('display','block');
            } else if (this.value.trim() === 'S'){ 
                $("#ifB").css('display','none');
                $("#ifA").css('display','none');
                $("#ifS").css('display','block');
            } else {
                $("#ifB").css('display','none');
                $("#ifA").css('display','none');
                $("#ifS").css('display','none');
            }
            
        } else {
            $( "#cmb_tipo" ).val('NNN');
            errorGeneral("Sistema Intranet",'Debe ingresar la identificaci&oacute;n','danger'); 
            return false;
        }
        
    });    
    
    $(".searchClient").click(function() {
        getCliente();
    });
    
    $(".busFact").click(function() {
        $('#txt_sear_ci').val($('#txt_identificacion').val());
        newModal('Fact','65','50','form_Fact');
        $('#T_regFact').dataTable().fnClearTable();
    });
    
    $(".filtroFact").click(function() {
        searchFact();
    });
    
    $(".passArt").click(function() {
        passArt();
    });
    
    $(".saveReclamo").click(function() {
        saveReclamo();
    });
    
    $('.form_datetime').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('.form_hour').datetimepicker({
        datepicker:false,
        format:'H:i',
        closeOnDateSelect:true
    });
    
    
}); 

function getCliente(){
    
    var cod_cli = $('#txt_identificacion').val();
    var url1 = $('#txt_urlGe').val();
    
    if (cod_cli.trim() === ''){
        errorGeneral("Sistema Intranet",'Debe ingresar la identificaci&oacute;n','danger'); 
        return false;
    }
    
    var datos = "c_c="+cod_cli;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/reclamos/co_111/retClient',
        data: datos,
        success : function (returnData) {
            removedataLoading();
            var data = eval('('+returnData+')');
            if (data.data){ 
                $.each(data.data,function(e,dato){
                    $('#txt_nomb_ape').val(dato.nom_cli);
                    $('#txt_direccion').val(dato.dir_cli);
                    $('#txt_convencional').val(dato.tel_cli);
                    $('#txt_celular').val(dato.tel_cli);
                    $('#txt_mail').val(dato.mail_cli);
                });
            } else {
                if(data.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    $('#form_reclamos_ge')[0].reset();
                    return false;
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                    return false;
                }
            }

        }
    });

}

function saveReclamo(){
    
    var url1 = $('#txt_urlGe').val();
    var cod_bdg = $('#cod_emp').val();
    var id = $('#txt_identificacion').val();
    var nom_cli = $('#txt_nomb_ape').val();
    var dir_cli = $('#txt_direccion').val();
    var conv_cli = $('#txt_convencional').val();
    var celu_cli = $('#txt_celular').val();
    var mail_cli = $('#txt_mail').val();
    var tipo = $('#cmb_tipo').val();
    var num_fac = $('#txt_num_doc').val();
    var des_serv = $('#txt_desc_serv').val();
    var fec_recl = $('#txt_fec_doc').val();
    var hora = $('#txt_hora').val();
    var desc_clara = $('#txt_desc_clara').val();
    var desc_prete = $('#txt_pretende').val();
    
    var json_cabecera = [];
    var json_art = [];
    
    var a = id+'|'+'Identificaci&oacute;n'+':';
    a = a + nom_cli+'|'+'Nombre del Cliente'+':';
    a = a + dir_cli+'|'+'Direcci&oacute;n del Cliente'+':';
    if (tipo.toString().trim() === 'S'){
        a = a + des_serv+'|'+'Descripci&oacute;n del servicio'+':';
    } else if (tipo.toString().trim() === 'B'){
        a = a + num_fac+'|'+'No. De Factura'+':';
    } else {
        a = a + tipo+'|'+'Tipo'+':';
    }
    a = a + fec_recl+'|'+'Fecha del Inconveniente'+':';
    a = a + hora+'|'+'Hora del Inconveniente'+':';
    a = a + desc_clara+'|'+'Descripción clara y consica del reclamo'+':';
    a = a + desc_prete+'|'+'Resultado que pretende obtener con el reclamo';
    
    if(!$("#bit_mail").is(':checked') && !$("#bit_convencional").is(':checked') && !$("#bit_celular").is(':checked')){
        errorGeneral("Sistema Intranet",'Debe de Seleccionar por lo menos una forma que desea recibir su respuesta.','danger');
        return false;
    }
    
    if($("#bit_mail").is(':checked') && mail_cli === ''){
        errorGeneral("Sistema Intranet",'Debe de ingresar la direcci&oacute;n de correo electronico.','danger');
        return false;
    }
    
    if(!validaMail(mail_cli)){
        return false;
    }
    
    if(isEmptySis(a)){
        
        var item = {};
        item["id"]=id;
        item["nom_cli"]=nom_cli;
        item["dir_cli"]=dir_cli;
        item["conv_cli"]=conv_cli;
        item["celu_cli"]=celu_cli;
        item["mail_cli"]=mail_cli;
        item["tipo"]=tipo;
        item["num_fac"]=num_fac;
        item["des_serv"]=des_serv;
        item["fec_recl"]=fec_recl;
        item["hora"]=hora;
        item["desc_clara"]=desc_clara;
        item["desc_prete"]=desc_prete;
        if($("#bit_mail").is(':checked')){item["bit_mail"]=1;} else {item["bit_mail"]=0;}
        if($("#bit_convencional").is(':checked')){item["bit_conv"]=1;} else {item["bit_conv"]=0;}
        if($("#bit_celular").is(':checked')){item["bit_celu"]=1;} else {item["bit_celu"]=0;}
        
        json_cabecera.push(item);
        
        dataLoading();
        
        $('#getArti tr').each(function (){

            var item = {};
            var fec = $(this).find('.cod_ART').html();
            var por = $(this).find('.rec_ART').val();

            if(fec != null && por != null){
                
                item["cod_art"] = fec;            
                item["rec_art"] = por;

                json_art.push(item); 

            }

        });
        
        var datos = 'c_a='+JSON.stringify(json_cabecera)+'&d_a='+JSON.stringify(json_art);
        
        $.ajax({
            type : 'POST',
            url : url1+'procesos/reclamos/co_111/saveReclamo',
            data: datos,
            success : function (returnData) {

                removedataLoading();              
                var datta = eval('('+returnData+')');
                
                if (datta.data){
                    
                    $.each(datta,function(e,dat){
                        if (dat.a === 2){
                            errorGeneral("Sistema Intranet",dat.b,'success');
                            $('#form_reclamos_ge')[0].reset();
                            $('#Pass_det_art').empty();
                            window.open(url1+'procesos/reclamos/co_113/viewPDF/'+cod_bdg+'/'+dat.c+'/Reclamo '+dat.c);
                            return false;
                        } else {
                            errorGeneral("Sistema Intranet",dat.b,'danger');
                            return false;
                        }
                    });

                } else {
                    
                    errorGeneral("Sistema Intranet",returnData,'danger');
                    return false;
                }

            }
        });
    }
    
}

function passEmpr(a,b){
    
    $('#cod_emp').val(a);
    $('#id_emp').val(b);
    document.getElementById("f_solch").submit();
    
}

function searchFact(){
    
    var ci = $('#txt_sear_ci').val();
    var mes = $('#cmb_mes').val();
    var anio = $('#cmb_anio').val();
    var url1 = $('#txt_urlGe').val();
    var datos = "c_i="+ci+"&m_s="+mes+"&a_o="+anio;
    
    if (ci.trim() === ''){
        errorGeneral("Sistema Intranet",'Ingrese la Identificaci&oacute;n','danger'); 
        return false;
    }
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/reclamos/co_111/retFact',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();
            $('#T_regFact').dataTable().fnClearTable();
            
            var data = eval('('+returnData+')');

            if (data.data){

                $('#tableFact').empty();
                var row = '<table id="T_regFact" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Almac&eacute;n</th>';
                row = row+'<th>Tipo de Documento</th>';
                row = row+'<th># Documento</th>';
                row = row+'<th>Valor Transacci&oacute;n</th>';
                row = row+'<th>Fecha</th>';
                row = row+'<th></th>';
                row = row+'</thead>';
                row = row+'<tbody>';

                $.each(data.data,function(e,dato){
                    row = row+'<tr>';
                    row = row+'<td>'+dato.des_bod+'</td>';
                    row = row+'<td>'+dato.tip_nom+'</td>';
                    row = row+'<td>'+pad(dato.cod_bod,3)+'-'+dato.num_caj+'-'+dato.num_doc+'</td>';
                    row = row+'<td>'+dato.val_doc+'</td>';
                    row = row+'<td>'+dato.fec_doc+'</td>';
                    row = row+'<td class="centro"><button type="button" onclick="viewDoc('+"'"+dato.tip_doc+"'"+','+"'"+dato.cod_bod+"'"+','+"'"+dato.num_caj+"'"+','+"'"+dato.num_doc+"'"+')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                    row = row+'</tr>';
                });

                row = row+'</tbody>';
                row = row+'</table>';

                $('#tableFact').append(row);
                $('#T_regFact').dataTable();

            } else {
                if(data.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }

        }
    });

}

function pad (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function viewDoc(a,b,c,d){
    __cont = 0;
    var url1 = $('#txt_urlGe').val();
    var datos = "t_d="+a+"&c_i="+b+"&c_c="+c+"&c_d="+d;
    var num_fact = pad(b,3);
    $('#id__fact').html(num_fact.toString()+'-'+c.toString()+'-'+d.toString());
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/reclamos/co_111/retDetFact',
        data: datos,
        success : function (returnData) {
            
                removedataLoading();
                
                $('#det_articulos').empty();
                
                var data = eval('('+returnData+')');

                if (data.data){

                    var row = '';
                    var fact = num_fact.toString()+'-'+c.toString()+'-'+d.toString();
                    $.each(data.data,function(e,dato){                            
                        __cont++;
                        row = row+'<tr id="option_'+__cont+'">';
                        row = row+'<td>'+dato.cod_art+'</td>';
                        row = row+'<td>'+dato.des_art+'</td>';
                        row = row+'<td>'+dato.cat_art+'</td>';
                        row = row+'<td>'+dato.val_art+'</td>';
                        row = row+'<td class="centro"><input type="checkbox" id="'+__cont+'" name="opt_art[]" value="'+dato.cod_art+'" desc="'+dato.des_art+'"></td>';
                        row = row+'</tr>';
                    });

                    $('#det_articulos').append(row);
                    $('#botonServicios').append("<button type='button' class='btn btn-success' onclick='passArt("+'"'+fact+'"'+")' style='position: absolute; right: 0;margin: 0.2% 0.6%;'><i class='fa fa-check'></i> Seleccionar Art&iacute;culos</button>");
                    
                    newModal('DetFact','50','45','form_DetFact');
                    $('#modalDetFact').css('z-index','1042');
                    $('#fadeDetFact').css('z-index','1041');
    
                } else {
                    if(data.data === null){
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    } else {  
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                    }
                }
                
                
            }
    });
}

function passArt(fact){
    
    var select = $("input[name='opt_art[]']:checked");
    if($(select).length <= 0){
        errorGeneral("Sistema Intranet",'Seleccione los art&iacute;culos','danger');
        return false;
    }    
    
    var ara = '';
    $('#Pass_det_art').empty();
    $('#txt_num_doc').val(fact);
    
    $(select).each( function () {
        ara = ara +'<tr>';
        ara = ara +'<td class="cod_ART">'+$(this).val()+'</td>';
        ara = ara +'<td>'+$(this).attr('desc')+'</td>';
        ara = ara +'<td><textarea class="form_control rec_ART" value="" style="width: 100%;"></textarea></td>';
        ara = ara +'</tr>';
    });
    
    $('#Pass_det_art').append(ara);
    quitarmodalGeneral('Fact','form_Fact');
    quitarmodalGeneral('DetFact','form_DetFact');
}