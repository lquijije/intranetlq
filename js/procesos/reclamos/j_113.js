
$(function() {
    
    $(".searchCI").click(function() {
        getReclamos();
    });
   
}); 

function getReclamos(){
    
    var cod_cli = $('#txt_ci').val();
    var url1 = $('#txt_urlGe').val();
    
    if (cod_cli.toString().trim() === ''){
        errorGeneral("Sistema Intranet",'Debe ingresar la identificaci&oacute;n','danger'); 
        return false;
    }
    
    var datos = "c_c="+cod_cli;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/reclamos/co_113/retReclamos',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();
            
            $('#detalleReclamos').empty();
            
            var data = eval('('+returnData+')');
            
            if (!$.isEmptyObject(data.data)){
                
                var row = '<table id="T_DocPen" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Cliente</th>';
                row = row+'<th>Almac&eacute;n</th>';
                row = row+'<th>Reclamo</th>';
                row = row+'<th>Tipo</th>';
                row = row+'<th>Descripcion</th>';
                row = row+'<th>Fecha</th>';
                row = row+'<th>Estado</th>';
                row = row+'<th></th>';
                row = row+'</thead>';
                row = row+'<tbody>';

                $.each(data.data,function(e,dato){
                    var tipo = dato.tip_rcl === 'B'?'Bien':'Servicio';
                    var estaDO = dato.est_rcl === 'I'?'INGRESADA':'FINALIZADA';
                    
                    row = row+'<tr>';
                    row = row+'<td>'+dato.nom_cli+'</td>';
                    row = row+'<td>'+dato.nom_alm+'</td>';
                    row = row+'<td>'+dato.cod_rcl+'</td>';
                    row = row+'<td>'+tipo+'</td>';
                    row = row+'<td>'+dato.des_abr+'</td>';
                    row = row+'<td>'+dato.fec_rcl+'</td>';
                    row = row+'<td>'+estaDO+'</td>';
                    row = row+'<td class="centro"><button type="button" onclick="viewRec('+"'"+cod_cli+"'"+','+"'"+dato.cod_rcl+"'"+','+"'"+dato.nom_alm+"'"+','+"'"+dato.cod_alm+"'"+')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                    row = row+'</tr>';
                });

                row = row+'</tbody>';
                row = row+'</table>';

                $('#detalleReclamos').append(row);
                $('#T_DocPen').dataTable();

            } else {
                if(data.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    return false;
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                    return false;
                }
            }

        }
    });

}

function viewRec(ci,cr,a,b){
    
    $('#cont_cab').empty();
    $('#name_pro').html(a);
    $('#num_doc_fac').html(cr);
    
    var url1 = $('#txt_urlGe').val();
    var datos = "c_c="+ci+"&c_r="+cr+"&c_b="+b;
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/reclamos/co_113/viewReclamo',
        data: datos,
        success : function (data) {
            
            removedataLoading();
            
            data = eval('('+data+')');
            
            if(data.str){
                
                $('#cont_cab').append(data.str);
                newModal('ViewDoc','75','70','form_ViewDoc');
                
            } else {
            
                if(data.str === null){
                    errorGeneral("Sistema Intranet",'No se puede encontrar los datos del reclamo.<br>Comuniquese con sistemas','danger'); 
                    return false;
                } else {
                    errorGeneral("Sistema Intranet",data,'danger'); 
                    return false;
                }
                
            }
            
            
        }
    });

}

function getPDF(a,b){
    var url1 = $('#txt_urlGe').val();
    
    window.open(url1+'procesos/reclamos/co_113/viewPDF/'+a+'/'+b+'/Reclamo '+b);
    
}
