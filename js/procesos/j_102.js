
$(document).ready(function() {
    
    var urlGe = $('#txt_urlGe').val();
    getDocCompras();
    
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            })
        },
        timepicker:false
    });
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            })
        },
        timepicker:false
    });
    
    $( ".resetCotiza" ).button().click(function(){
        quitarmodalGeneral('Cotizar','form_cotiz');
    });
    
    $( ".searchCotiza" ).button().click(function(){
        getDocCompras();
    });
    
    $( ".cotizar" ).button().click(function(){
        aprobardocumentos('C','G');
    });
    
    $( ".saveCotiza").button().click(function(){
        saveDocCotiz('C',urlGe);
    });
});

function saveCotiza(id){
    $('#txt_cod_cot').val(id);
    newModal('Cotizar','40','60','form_ing_req');
}

function saveDocCotiz(accion,urlGe){
    
    var datos = "act="+accion;
    datos = datos + "&id="+$('#txt_cod_cot').val();
    datos = datos + "&pro="+$('#txt_proveedor').val();
    datos = datos + "&pre="+$('#txt_precio').val();
    
    if (!validaCampos()){
        return false;
    }
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : urlGe+"procesos/requisicion/c_102/guardar", 
        data: datos,
        success : function (returnData) { 
            var data =returnData.trim(); 
            removedataLoading();         
            if(data.trim()==="OK"){
                quitarmodalGeneral('Cotizar','form_cotiz');
                errorGeneral("Sistema Intranet",'Cotización grabada con exito','success'); 
                getDocCompras();
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }
        }
    });

}

function validaCampos(){
    
    if ($("#txt_proveedor").val().trim() === '') {
        errorGeneral("Sistema Intranet",'Campo Proveedor esta vacio','danger');
        return false;
    }
    
    if ($("#txt_precio").val().trim() === '') {
        errorGeneral("Sistema Intranet",'Campo Precio esta vacio','danger');
        return false;
    }
    
    return true;
}

function getDocCompras(){ 
    
    var url1 = $('#txt_urlGe').val();
    var clase='';
    var datos = "tp="+$('#cmb_estado').val();
    datos = datos + "&f_i="+$('#txt_fec_ini').val();
    datos = datos + "&f_f="+$('#txt_fec_fin').val();
    
    if (!validaBusqueda()){
        return false;
    }
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'general/c_general_compras/retDocCompra',
        data: datos,
        success : function (returnData) {
                removedataLoading();         
                var data = eval(returnData); 
                $('#tableCotizar').html('');
                var row = '<table id="T_Requisicion" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Fecha</th>';
                row = row+'<th>Estado</th>';
                row = row+'<th>Dispositivo</th>';
                row = row+'<th>Destino/Motivo</th>';
                row = row+'<th>Cantidad</th>';
                row = row+'<th>Proveedor</th>';
                row = row+'<th>Precio</th>';
                row = row+'<th>Cotizar</th>';
                row = row+'<th>Enviar</th>';
                row = row+'</thead>';
                row = row+'<tbody>';
                
                if (data){ 
                    $.each(data,function(e,dato){
                        if (dato.bd_status.trim() === 'A'){
                            clase = 'aprobada';
                        } else if (dato.bd_status.trim() === 'N'){
                            clase = 'negada';
                        } else {
                            clase = '';
                        }   
                        row = row+'<tr class="'+clase+'" id="row_'+dato.co_solicitud+'">';
                        row = row+'<td class="centro">'+dato.fe_ingreso+'</td>';
                        row = row+'<td class="centro">'+dato.nom_estado+'</td>';
                        row = row+'<td>'+dato.tx_dispositivo+'</td>';
                        row = row+'<td>'+dato.tx_destino+'</td>';
                        row = row+'<td class="centro">'+dato.va_cantidad+'</td>';
                        row = row+'<td>'+dato.tx_proveedor+'</td>';
                        row = row+'<td class="derecha">'+dato.va_precio+'</td>';
                        
                        if (dato.bd_status.trim() === 'C'){
                            row = row+'<td><center><a class="text-blue" href="#" onclick="saveCotiza('+dato.co_solicitud+')">Cotizar</a></center></td>';
                        } else {
                            row = row+'<td></td>';
                        }
                        
                        if (dato.bd_status.trim() === 'C' && dato.tx_proveedor.trim() !== '' && dato.va_precio.trim()){
                            row = row+'<td><center><input type="checkbox" id="ck_'+dato.co_solicitud+'" name="option_req[]" value="'+dato.co_solicitud+'"></center></td>';
                        } else {
                            row = row+'<td></td>';
                        }
                        
                        row = row+'</tr>';
                    });
                }  
                
                row = row+'</tbody>';
                row = row+'</table>';
                
                $('#tableCotizar').append(row);
                $('#T_Requisicion').dataTable();
            }
           
    });
    
    
}

