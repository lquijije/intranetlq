
var __cont = 0;

function saveGara(){
    
    var url1 = $('#txt_urlGe').val();
    var id_ = $('#txt_ide').val();
    var cb_ = $('#co_alm').val();
    var cj_ = $('#txt_cj').val();
    var nd_ = $('#txt_dc').val();
    var co_ap = $('#txt_c_a').val();
    var gr_ap = $('#txt_g_a').val();
    var co_ag = $('#txt_c_g').val();
    var ma_el = $('#txt_m_a').val();
    var mo_el = $('#txt_mo_a').val();
    var nu_se = $('#txt_s_n_a').val();
    var ma_il = $('#txt_mail').val();
    
    var a = nd_+'|'+'# De Documento'+':';
    a = a + cb_+'|'+'# De Documento'+':';
    a = a + cj_+'|'+'# De Documento'+':';
    a = a + id_+'|'+'Identificaci&oacute;n'+':';
    a = a + co_ap+'|'+'C&oacute;digo del Art&iacute;culo'+':';
    a = a + co_ag+'|'+'C&oacute;digo de la Garant&iacute;a'+':';
    a = a + nu_se+'|'+'Numero de Serie';
    
    if(isEmptySis(a)){
        
        if(gr_ap === 4 && ma_el === '' || mo_el === ''){
            errorGeneral("Sistema Intranet",'Debe de ingresar la marca y modelo del Electrodomestico','danger');
            return false;
        }

        if($("#bit_mail").is(':checked') && ma_il === ''){
            errorGeneral("Sistema Intranet",'Debe de ingresar la direcci&oacute;n de correo electronico.','danger');
            return false;
        } 

        if (ma_il != ''){  
            if(!validaMail(ma_il)){
                return false;
            }
        }
        
        var item = {};
        item["id"]=id_;
        item["cb_"]=cb_;
        item["cj_"]=cj_;
        item["nd_"]=nd_;
        item["co_ap"]=co_ap;
        item["co_ag"]=co_ag;
        item["ma_el"]=ma_el;
        item["mo_el"]=mo_el;
        item["nu_se"]=nu_se;
        item["ma_il"]=ma_il;
        if($("#bit_mail").is(':checked')){item["bit_mail"]=1;} else {item["bit_mail"]=0;}
        
        dataLoading();
        
        var datos = 'c_a='+JSON.stringify(item);
        
        $.ajax({
            type : 'POST',
            url : url1+'procesos/garantia/co_124/saveGa',
            data: datos,
            success : function (returnData) {
                removedataLoading();              
                var datta = eval('('+returnData+')');
                
                if (datta.data){
                    if (datta.data.co_err === 0){
                        $('#txt_g_a').val();
                        $('#f_new_ga')[0].reset();
                        quitarmodalGeneral('NewGa','f_new_ga');
                        errorGeneral("Sistema Intranet","Registro Grabado Con Exito",'success');
                        retConJS();
                    } else {
                        errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                    }
                    //window.open(url1+'procesos/reclamos/co_113/viewPDF/'+cod_bdg+'/'+dat.c+'/Reclamo '+dat.c);
                } else {
                    errorGeneral("Sistema Intranet",returnData,'danger');
                    return false;
                }

            }
        });
    }
    
}

function searchFact(){
    
    var ci = $('#txt_sear_ci').val();
    var mes = $('#cmb_mes').val();
    var anio = $('#cmb_anio').val();
    var url1 = $('#txt_urlGe').val();
    var datos = "c_i="+ci+"&m_s="+mes+"&a_o="+anio;
    
    if (ci.trim() === ''){
        errorGeneral("Sistema Intranet",'Ingrese la Identificaci&oacute;n','danger'); 
        return false;
    }
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/garantia/co_124/retFact',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();
            $('#T_regFact').dataTable().fnClearTable();
            
            var data = eval('('+returnData+')');

            if (data.data){

                $('#tableFact').empty();
                var row = '<table id="T_regFact" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Almac&eacute;n</th>';
                row = row+'<th>Tipo de Documento</th>';
                row = row+'<th># Documento</th>';
                row = row+'<th>Valor Transacci&oacute;n</th>';
                row = row+'<th>Fecha</th>';
                row = row+'<th></th>';
                row = row+'</thead>';
                row = row+'<tbody>';

                $.each(data.data,function(e,dato){
                    row = row+'<tr>';
                    row = row+'<td>'+dato.des_bod+'</td>';
                    row = row+'<td>'+dato.tip_nom+'</td>';
                    row = row+'<td>'+pad(dato.num_sri,3)+'-'+dato.num_caj+'-'+dato.num_doc+'</td>';
                    row = row+'<td>'+dato.val_doc+'</td>';
                    row = row+'<td>'+dato.fec_doc+'</td>';
                    row = row+'<td class="centro"><button type="button" onclick="getFact('+"'"+dato.cod_bod+"'"+','+"'"+dato.num_caj+"'"+','+"'"+dato.num_doc+"'"+',1)" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                    row = row+'</tr>';
                });

                row = row+'</tbody>';
                row = row+'</table>';

                $('#tableFact').append(row);
                $('#T_regFact').dataTable();

            } else {
                if(data.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
        }
    });
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function passArt(d){
    quitarmodalGeneral('Fact','form_Fact');
    var select = $("input[name='opt_art[]']:checked");
    if($(select).length <= 0){
        errorGeneral("Sistema Intranet",'Seleccione los art&iacute;culos','danger');
        return false;
    }    
    
    $(select).each( function () {
        var data = decodeURIComponent($(this).attr('data-label'));
        var datos = $.parseJSON(data);

        if(datos.cod_gru === 8 && datos.cod_lin === 3){
            $('#cmb_ti_con').val(datos.tim_pro);
            $('#txt_c_g').val(datos.cod_art);
            $('#txt_d_g').val(datos.des_art);
            $('#txt_p_g').val(datos.val_art);
        } else {
            if(datos.cod_gru === 4){
                errorGeneral("Sistema Intranet",'Debe de ingresar Marca y Modelo del Electrodomestico','info');
            }
            $('#txt_g_a').val(datos.cod_gru);
            $('#txt_c_a').val(datos.cod_art);
            $('#txt_d_a').val(datos.des_art);
            $('#cmb_ti_ga').val(datos.tim_pro);
            $('#txt_p_c_a').val(datos.val_art);
        }
    });
    quitarmodalGeneral('DetFact','form_DetFact');
    
    if (d === 1){
        var cb_ = $('#co_alm').val();
        var cj_ = $('#txt_cj').val();
        var nd_ = $('#txt_dc').val();
        var cg_ = $('#txt_c_g').val();
        var vg_ = $('#txt_p_g').val();
        if(cj_.trim() === ''){
            errorGeneral("Sistema Intranet",'Ingrese el # de caja','danger');
            return false;
        }
        if(nd_.trim() === ''){
            errorGeneral("Sistema Intranet",'Ingrese el # de documento','danger');
            return false;
        }
        if(cj_.trim() === ''){
            errorGeneral("Sistema Intranet",'Ingrese el # de caja','danger');
            return false;
        }
        if(cg_.trim() === '' || vg_.trim() === ''){
            errorGeneral("Sistema Intranet",'Debe de escoger una garant&iacute;a','danger');
            return false;
        }
        getFact(cb_,cj_,nd_,2);
    }
}

function getFact(a,b,c,d){
    __cont = 0;
    $('#co_alm').val(a);
    $('#txt_cj').val(b);
    $('#txt_dc').val(c);
    var url1 = $('#txt_urlGe').val();
    var datos = "t_d=F&c_i="+a+"&c_c="+b+"&c_d="+c;
    var num_fact = pad($('#co_sr').val(),3);
    $('#id__fact').html(num_fact.toString()+'-'+b.toString()+'-'+c.toString());
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/garantia/co_124/retDetFact',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();

            var data = eval('('+returnData+')');

            if (data.dt_1 && data.dt_2){

                var row = '';
                var fact = num_fact.toString()+'-'+b.toString()+'-'+c.toString();
                $.each(data.dt_2,function(e,dat){
                    $('#txt_ide').val(dat.ide);
                    $('#txt_nomb_ape').val(dat.nom);
                    $('#txt_direccion').val(dat.dir);
                    $('#txt_convencional').val(dat.cel);
                    $('#txt_celular').val(dat.cel);
                    $('#txt_mail').val(dat.cor);
                });
                
                $.each(data.dt_1,function(e,dato){   
                    __cont++;
                    row = row+'<tr id="option_'+__cont+'">';
                    row = row+'<td>'+dato.cod_art+'</td>';
                    row = row+'<td>'+dato.des_art+'</td>';
                    row = row+'<td class="derecha">'+dato.cat_art+'</td>';
                    row = row+'<td class="derecha">'+dato.val_art+'</td>';
                    row = row+'<td class="centro">';   
                    
                    var vali = dato.cod_gru.toString() + dato.cod_lin.toString();
                    
                    if(parseInt(vali) === 83 && d === 1){
                        if (data.dt_3){
                            $.each(data.dt_3,function(e,con){ 
                                if(con.cod_art_gar === dato.cod_art){
                                    row = row+'<a href="javascript:void(0)">Ver |</a>';
                                }
                            });
                        } else {
                            row = row+'<input type="radio" name="opt_art[]" value="'+dato.cod_art+'" data-label="'+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+'">';
                        }
                    } else {
                        if(d === 2 && parseInt(vali) != 83){
                            row = row+'<input type="radio" name="opt_art[]" value="'+dato.cod_art+'" data-label="'+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+'">';
                        }
                    }
                    row = row+'</td>';
                    row = row+'</tr>';
                });

                var des = d === 1?'Garant&iacute;a':'Art&iacute;culo';
                $('#det_articulos').html(row);
                $('#botonServicios').html("<button type='button' class='btn btn-success' onclick='passArt("+d+")' style='position: absolute; right: 0;margin: 0.2% 0.6%;'><i class='fa fa-check'></i> Seleccionar "+des+"</button>");

                newModal('DetFact','60','55','form_DetFact');
                $('#modalDetFact').css('z-index','1043');
                $('#fadeDetFact').css('z-index','1042');

            } else {
                if(!data.dt_1 && !data.dt_2){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
        }
    });
}

function passEmpr(a,b){
    
    $('#co_alm').val(a);
    $('#co_sr').val(b);
    document.getElementById("f_garan").submit();
    
}

function busAvan(){
    $('#txt_sear_ci').val($('#txt_identificacion').val());
    newModal('Fact','65','50','form_Fact');
    $('#modalFact').css('z-index','1042');
    $('#fadeFact').css('z-index','1041');
    $('#T_regFact').dataTable().fnClearTable();
};

function retConJS(){
    
    var fi = $('#txt_fec_ini').val();
    var ff = $('#txt_fec_fin').val();
    var url1 = $('#txt_urlGe').val();
    var datos = "fi="+fi+"&ff="+ff;
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/garantia/co_124/retCon',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();
            
            var data = eval('('+returnData+')');

            $('#detContra').html('');
            var row = '<table id="tabContr" class="table table-bordered TFtable">';
            row = row+'<thead>';
            row = row+'<tr>';
            row = row+'<th>Co. Contrato</th>';
            row = row+'<th># De Factura</th>';
            row = row+'<th>Identificaci&oacute;n</th>';
            row = row+'<th>Cliente</th>';
            row = row+'<th>Fecha Contrato</th>';
            row = row+'<th></th>';
            row = row+'</tr>';
            row = row+'</thead>';
            row = row+'<tbody>';

            if (data){
                $.each(data,function(e,dato){
                    row = row+'<tr>';
                    row = row+'<td>'+dato.co_con+'</td>';
                    row = row+'<td>'+dato.de_sri+'-'+dato.co_caj+'-'+dato.nu_doc+'</td>';
                    row = row+'<td>'+dato.id_cli+'</td>';
                    row = row+'<td>'+dato.no_cli+'</td>';
                    row = row+'<td>'+dato.fe_gar+'</td>';
                    row = row+'<td class="centro"><button type="button" onclick="viewCont('+"'"+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+"'"+')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                    row = row+'</tr>';
                });
            } else {
                if(data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
            row = row+'</tbody>';
            row = row+'</table>';

            $('#detContra').html(row);
            $('#tabContr').dataTable();
            
        }
    });
}

function viewCont(data){
    data = decodeURIComponent(data);
    var da = $.parseJSON(data);
    
    if (da){
        newModal('ViewCon','60','80','');
        
        $('#id__contr').html(da.co_con);
        $('._i_cli').html(da.id_cli);
        $('._n_cli').html(da.no_cli);
        $('._d_cli').html(da.di_cli);
        $('._t_cli').html(da.tl_cli);
        $('._e_cli').html(da.em_env);
        $('._f_cli').html(da.de_sri+'-'+da.co_caj+'-'+da.nu_doc);
        $('._c_art').html(da.co_art);
        $('._d_art').html(da.ds_art);
        $('._n_ser').html(da.nu_ser);
        $('._p_con').html(da.va_art);
        $('._ma_art').html(da.ds_mar);
        $('._mo_art').html(da.ds_mod);
        $('._c_art_ga').html(da.co_art_ga);
        $('._d_art_ga').html((parseInt(da.ds_art_ga)*12)+' Meses');
        $('._du_art_ga').html(da.pr_art_gar);
        $('._pe_art_ga').html(da.va_art_gar);
    }
}
    
$(function() {
    
    $('.form_datetime').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('.form_hour').datetimepicker({
        datepicker:false,
        format:'H:i',
        closeOnDateSelect:true
    });
    
    $(".new_g").click(function() {
        newModal('NewGa','85','75','form_NewGa');
    });
    
    $(".sea_f").click(function() {
        $('#txt_g_a').val();
        var cb_ = $('#co_alm').val();
        var cj_ = $('#txt_cj').val();
        var nd_ = $('#txt_dc').val();
        if(cj_.trim() === ''){
            errorGeneral("Sistema Intranet",'Ingrese el # de caja','danger');
            return false;
        }
        if(nd_.trim() === ''){
            errorGeneral("Sistema Intranet",'Ingrese el # de documento','danger');
            return false;
        }
        getFact(cb_,cj_,nd_,1);
    });
    
    $(".cl_fr_1").click(function() {
        $('#f_new_ga')[0].reset();
        quitarmodalGeneral('DetFact','f_new_ga');
    });
    
    $(".res_g").click(function() {
        $('#f_new_ga')[0].reset();
    });
    
    $(".sav_g").click(function() {
        saveGara();
    });
    
    $(".filtroFact").click(function() {
        searchFact();
    });
    
    $(".seaCont").click(function() {
        retConJS();
    });
    $('#tabContr').dataTable();
}); 
