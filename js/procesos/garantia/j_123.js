
var __cont = 0;

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function retConJS(){
    
    var ti = $('#bu_sea').val();
    var ci = $('#ci_txt').val();
    var url1 = $('#txt_urlGe').val();
    var datos = "ci="+ci+"&ti="+ti;
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/garantia/co_123/retCon',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();
            
            var data = eval('('+returnData+')');

            $('#detContra').html('');
            var row = '<table id="tabContr" class="table table-bordered TFtable">';
            row = row+'<thead>';
            row = row+'<tr>';
            row = row+'<th>Co. Contrato</th>';
            row = row+'<th># De Factura</th>';
            row = row+'<th>Identificaci&oacute;n</th>';
            row = row+'<th>Cliente</th>';
            row = row+'<th>Fecha Contrato</th>';
            row = row+'<th></th>';
            row = row+'</tr>';
            row = row+'</thead>';
            row = row+'<tbody>';
            
            if (data.data){
                $.each(data.data,function(e,dato){
                    row = row+'<tr>';
                    row = row+'<td>'+dato.co_con+'</td>';
                    row = row+'<td>'+dato.de_sri+'-'+dato.co_caj+'-'+dato.nu_doc+'</td>';
                    row = row+'<td>'+dato.id_cli+'</td>';
                    row = row+'<td>'+dato.no_cli+'</td>';
                    row = row+'<td>'+dato.fe_gar+'</td>';
                    row = row+'<td class="centro"><button type="button" onclick="viewCont('+"'"+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+"'"+')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                    row = row+'</tr>';
                });
            } else {
                if(data.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
            row = row+'</tbody>';
            row = row+'</table>';

            $('#detContra').html(row);
            $('#tabContr').dataTable();
            
        }
    });
}

function viewCont(data){
    var url1 = $('#txt_urlGe').val();
    data = decodeURIComponent(data);
    var da = $.parseJSON(data);
    
    if (da){
        newModal('ViewCon','60','65','');
        
        $('#id__contr').html(da.co_con);
        $('._i_cli').html(da.id_cli);
        $('._n_cli').html(da.no_cli);
        $('._d_cli').html(da.di_cli);
        $('._t_cli').html(da.tl_cli);
        $('._e_cli').html(da.em_env);
        $('._f_cli').html(da.de_sri+'-'+da.co_caj+'-'+da.nu_doc);
        $('._c_art').html(da.co_art);
        $('._d_art').html(da.ds_art);
        $('._n_ser').html(da.nu_ser);
        $('._p_con').html(da.va_art);
        $('._ma_art').html(da.ds_mar);
        $('._mo_art').html(da.ds_mod);
        $('._c_art_ga').html(da.co_art_ga);
        $('._d_art_ga').html(da.ds_art_ga);
        $('._du_art_ga').html((parseInt(da.pr_art_gar)*12)+' Meses');
        $('._pe_art_ga').html(da.va_art_gar);
        
        $('#a_h_11').attr('href',url1+'procesos/garantia/co_123/ge_pdf1/'+da.co_con+'/1/Certificado '+da.co_con);
        $('#a_h_22').attr('href',url1+'procesos/garantia/co_123/ge_pdf2/'+da.co_con+'/1/Contrato '+da.co_con);
        
    }
}
    
$(function() {
    
    $(".seaCont").click(function() {
        retConJS();
    });
    
    $('#tabContr').dataTable();
    
    $("#bu_sea").change(function(e) {
        $('#ci_txt').val('');
        if (this.value.toString().trim() === '3'){
            $('.fallback').attr('placeholder','___-___-_________');
            $('.fallback').mask("000r000r000000000", {
                translation: {
                    'r': {
                        pattern: /[\/]/, 
                        fallback: '-'
                    }, 
                    placeholder: "__-__-____"
                }
            });
        } else {
            $('.fallback').unmask();
            $('.fallback').removeAttr('placeholder');
        }
    });
    
}); 
