
var __cont = 0;

function saveGara(){
    if(va_sess()){
        var url1 = $('#txt_urlGe').val();
        var id_ = $('#txt_ide').val();
        var nom_ = $('#txt_nomb_ape').val();
        var dir_ = $('#txt_direc').val();
        var tel_ = $('#txt_conve').val();
        var ma_il = $('#txt_mail').val();
        var cb_ = $('#co_alm').val();
        var cj_ = $('#txt_cj').val();
        var nd_ = $('#txt_dc').val();
        var co_ap = $('#txt_c_a').val();
        var gr_ap = $('#txt_g_a').val();
        var co_ag = $('#txt_c_g').val();
        var ma_el = $('#txt_m_a').val();
        var mo_el = $('#txt_mo_a').val();
        var nu_se = $('#txt_s_n_a').val();
        var ti_pr = $('#cmb_ti_pro').html();
        var ti_ga = $('#cmb_ti_gar').html();
        var pr_ap = $('#txt_p_c_a').val();
        var pr_ag = $('#txt_p_g').val();
        var us_pos = $('#co_up').val();
        var fe_fac = $('#fe_fa').val();
        var sec_fac = $('#sec_fa').val();

        var a = nd_+'|'+'# De Documento'+':';
        a = a + cb_+'|'+'# De Documento'+':';
        a = a + cj_+'|'+'# De Documento'+':';
        a = a + id_+'|'+'Identificaci&oacute;n'+':';
        a = a + nom_+'|'+'Nombres y Apellidos'+':';
        a = a + dir_+'|'+'Direcci&oacute;n'+':';
        a = a + tel_+'|'+'Tel&eacute;fono Convencional'+':';
        a = a + co_ap+'|'+'C&oacute;digo del Art&iacute;culo'+':';
        a = a + co_ag+'|'+'C&oacute;digo de la Garant&iacute;a';

        if(isEmptySis(a)){

    //        if(gr_ap === 4 && ma_el === '' || mo_el === ''){
    //            errorGeneral("Sistema Intranet",'Debe de ingresar la marca y modelo del Electrodomestico','danger');
    //            return false;
    //        }

            if($("#bit_mail").is(':checked') && ma_il === ''){
                errorGeneral("Sistema Intranet",'Debe de ingresar la direcci&oacute;n de correo electronico.','danger');
                return false;
            } 

            if (ma_il != ''){  
                if(!validaMail(ma_il)){
                    return false;
                }
            }

            var item = {};
            item["id"]=id_;
            item["no_"]=nom_;
            item["di_"]=dir_;
            item["te_"]=tel_;
            item["cb_"]=cb_;
            item["cj_"]=cj_;
            item["nd_"]=nd_;
            item["co_ap"]=co_ap;
            item["pr_ap"]=pr_ap;
            item["co_ag"]=co_ag;
            item["pr_ag"]=pr_ag;
            item["ma_el"]=ma_el.replace(/[&]/g,"-");
            item["mo_el"]=mo_el.replace(/[&]/g,"-");
            item["nu_se"]=nu_se.replace(/[&]/g,"-");
            item["ma_il"]=ma_il;
            item["ti_ga"]=ti_ga;
            item["ti_pr"]=ti_pr;
            item["us_pos"]=us_pos;
            item["fe_fac"]=fe_fac;
            item["sec_fac"]=sec_fac;
            
            if($("#bit_mail").is(':checked')){item["bit_mail"]=1;} else {item["bit_mail"]=0;}

            dataLoading();
            //console.log(item);
            var datos = 'c_a='+JSON.stringify(item);

            $.ajax({
                type : 'POST',
                url : url1+'procesos/garantia/co_122/saveGa',
                data: datos,
                success : function (returnData) {
                    removedataLoading();              
                    var datta = eval('('+returnData+')');

                    if (datta.data.id_gar){
                        if (datta.data.co_err === 0){
                            $('#cmb_ti_pro').html('0');
                            $('#cmb_ti_gar').html('0');
                            $('#txt_g_a').val('');
                            $('#f_new_ga')[0].reset();
                            errorGeneral("Sistema Intranet","Registro Grabado Con Exito\nContrato#"+datta.data.id_gar+" ",'success');
                            var ro = '<object height="1" width="1" data="'+url1+'procesos/garantia/co_123/ge_pdf1/'+datta.data.id_gar+'/1/Certificado'+datta.data.id_gar+'" type="application/pdf"></object>';
                            $('#_pdf').html(ro);
                        } else {
                            errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                        }
                    } else {
                        errorGeneral("Sistema Intranet",returnData,'danger');
                        return false;
                    }
                }
            });
        }
    } else {
        redirectIntranet();
    }
}

function searchFact(){
    if(va_sess()){
        var ci = $('#txt_sear_ci').val();
        var mes = $('#cmb_mes').val();
        var anio = $('#cmb_anio').val();
        var url1 = $('#txt_urlGe').val();
        var datos = "c_i="+ci+"&m_s="+mes+"&a_o="+anio;

        if (ci.trim() === ''){
            errorGeneral("Sistema Intranet",'Ingrese la Identificaci&oacute;n','danger'); 
            return false;
        }

        dataLoading();
        $.ajax({
            type : 'POST',
            url : url1+'procesos/garantia/co_122/retFact',
            data: datos,
            success : function (returnData) {

                removedataLoading();
                $('#T_regFact').dataTable().fnClearTable();

                var data = eval('('+returnData+')');

                if (data.data){

                    $('#tableFact').empty();
                    var row = '<table id="T_regFact" class="table table-bordered table-hover">';
                    row = row+'<thead>';
                    row = row+'<th>Almac&eacute;n</th>';
                    row = row+'<th>Tipo de Documento</th>';
                    row = row+'<th># Documento</th>';
                    row = row+'<th>Valor Transacci&oacute;n</th>';
                    row = row+'<th>Fecha</th>';
                    row = row+'<th></th>';
                    row = row+'</thead>';
                    row = row+'<tbody>';

                    $.each(data.data,function(e,dato){
                        row = row+'<tr>';
                        row = row+'<td>'+dato.des_bod+'</td>';
                        row = row+'<td>'+dato.tip_nom+'</td>';
                        row = row+'<td>'+pad(dato.num_sri,3)+'-'+dato.num_caj+'-'+dato.num_doc+'</td>';
                        row = row+'<td class="derecha">'+dato.val_doc+'</td>';
                        row = row+'<td>'+dato.fec_doc+'</td>';
                        row = row+'<td class="centro"><button type="button" onclick="getFact('+"'"+dato.cod_bod+"'"+','+"'"+dato.num_caj+"'"+','+"'"+dato.num_doc+"'"+',1)" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                        row = row+'</tr>';
                    });

                    row = row+'</tbody>';
                    row = row+'</table>';

                    $('#tableFact').append(row);
                    $('#T_regFact').dataTable();

                } else {
                    if(data.data === null){
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    } else {  
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                    }
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

function passArt(data){
    
    var select = $("input[name='opt_art[]']:checked");
    if($(select).length <= 0){
        errorGeneral("Sistema Intranet",'Seleccione la Garant&iacute;a','danger');
        return false;
    }
    
    data = decodeURIComponent(data);
    var datos = $.parseJSON(data);
    
    $('#sec_fa').val('');
    quitarmodalGeneral('Fact','form_Fact');
    
    $(select).each( function () {
        
        var gar_sel = $(this).attr('value').trim();
        var art_ref = $(this).attr('data-label').trim();
        var sec_gar = $(this).attr('data-sec').trim();
        if(art_ref != ''){
            if(datos){
                $.each(datos,function(e,dato){   
                    if(dato.cod_art === gar_sel && dato.cod_sec == sec_gar){
                        $('#sec_fa').val(dato.cod_sec);
                        $('#co_up').val(dato.cod_user_ref);
                        $('#cmb_ti_gar').html(dato.tim_pro);
                        $('#txt_c_g').val(dato.cod_art);
                        $('#txt_d_g').val(dato.des_art);
                        $('#txt_p_g').val(dato.val_art);
                    } else if(dato.cod_art === art_ref){
                        if(dato.cod_gru === 4){
                            errorGeneral("Sistema Intranet",'Debe de ingresar Marca y Modelo del Electrodomestico','info');
                        }
                        $('#txt_g_a').val(dato.cod_gru);
                        $('#txt_c_a').val(dato.cod_art);
                        $('#txt_d_a').val(dato.des_art);
                        $('#cmb_ti_pro').html(dato.tim_pro);
                        $('#txt_p_c_a').val(dato.val_art);
                        $('#txt_m_a').val(dato.mar_art);
                        $('#txt_mo_a').val(dato.mod_art);
                    }

                });
            }
        } else {
            errorGeneral("Sistema Intranet",'C&oacute;digo de Garant&iacute;a no hace referencia a ningun art&iacute;culo de la factura.','danger');
            return false;
        }
        
        quitarmodalGeneral('DetFact','form_DetFact');
        
    });
}

function getFact(a,b,c){
    if(va_sess()){
        __cont = 0;
        $('#fe_fa').val();
        $('#sec_fa').val();
        $('#co_alm').val(a);
        $('#txt_cj').val(b);
        $('#txt_dc').val(c);
        var url1 = $('#txt_urlGe').val();
        var datos = "t_d=F&c_i="+a+"&c_c="+b+"&c_d="+c;
        var num_fact = pad($('#co_sr').val(),3);
        $('#id__fact').html(num_fact.toString()+'-'+b.toString()+'-'+c.toString());
        dataLoading();

        $.ajax({
            type : 'POST',
            data: datos,
            url : url1+'procesos/garantia/co_122/retDetFact',
            success : function (returnData) {
                removedataLoading();

                var data = eval('('+returnData+')');

                if (data.err == ''){
                    if (data.dt_2){
                        $('#fe_fa').val(data.fe_f);
                        var row = '';
                        var fact = num_fact.toString()+'-'+b.toString()+'-'+c.toString();

                        if(data.dt_1){
                            $('#txt_ide').val(data.dt_1.ide_cli);
                            $('#txt_nomb_ape').val(data.dt_1.nom_cli);
                            $('#txt_direc').val(data.dt_1.dir_cli);
                            $('#txt_conve').val(data.dt_1.cel_cli);
                            $('#txt_celular').val(data.dt_1.cel_cli);
                            $('#txt_mail').val(data.dt_1.mail_cli);
                        }

                        $.each(data.dt_2,function(e,dato){   
                            __cont++;
                            row = row+'<tr id="option_'+__cont+'">';
                            row = row+'<td>'+dato.cod_art+'</td>';
                            row = row+'<td>'+dato.des_art+'</td>';
                            row = row+'<td class="derecha">'+dato.can_art+'</td>';
                            row = row+'<td class="derecha">'+dato.val_art+'</td>';
                            row = row+'<td class="centro">';   

                            var vali = dato.cod_gru.toString() + dato.cod_lin.toString();

                            if(parseInt(vali) === 83){
                                if (dato.co_cont > 0){
                                    row = row+'<a href="javascript:void(0)" onclick="viewCont('+"'"+dato.co_cont+"'"+')">Ver</a>';
                                } else {
                                    row = row+'<input type="radio" name="opt_art[]" value="'+dato.cod_art+'" data-label="'+dato.cod_art_ref+'" data-sec="'+dato.cod_sec+'">';
                                }
                            } 
                            row = row+'</td>';
                            row = row+'</tr>';
                        });

                        $('#det_articulos').html(row);
                        $('#botonServicios').html('<button type="button" class="btn btn-success" onclick="passArt('+"'"+encodeURIComponent(JSON.stringify(data.dt_2)).replace(/['"]/g,"-")+"'"+')" style="position: absolute; right: 0;margin: 0.2% 0.6%;"><i class="fa fa-check"></i> Seleccionar Garant&iacute;a</button>');

                        newModal('DetFact','60','55','form_DetFact');
                        $('#modalDetFact').css('z-index','1043');
                        $('#fadeDetFact').css('z-index','1042');
                    } else {
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    }
                    
                } else {
                    errorGeneral("Sistema Intranet",data.err,'danger'); 
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

function passEmpr(a,b,c){
    $('#co_alm').val(a);
    $('#co_sr').val(b);
    $('#co_ip').val(c);
    document.getElementById("f_garan").submit();
}

function busAvan(){
    $('#txt_sear_ci').val($('#txt_identificacion').val());
    newModal('Fact','65','50','form_Fact');
    $('#modalFact').css('z-index','1042');
    $('#fadeFact').css('z-index','1041');
    $('#T_regFact').dataTable().fnClearTable();
};

function viewCont(cc){
    
    if(va_sess()){
        var url1 = $('#txt_urlGe').val();
        var datos = "cc="+cc;

        if (cc.trim() === ''){
            errorGeneral("Sistema Intranet",'Error al Obtener datos de Contrato','danger'); 
            return false;
        }

        dataLoading();
        $.ajax({
            type : 'POST',
            url : url1+'procesos/garantia/co_122/retCont',
            data: datos,
            success : function (returnData) {

                removedataLoading();

                var data = eval('('+returnData+')');

                if (data.data){

                    $.each(data.data,function(e,da){
                        newModal('ViewCon','60','65','');
                        $('#modalViewCon').css('z-index','1043');
                        $('#fadeViewCon').css('z-index','1042');

                        $('#id__contr').html(da.co_con);
                        $('._i_cli').html(da.id_cli);
                        $('._n_cli').html(da.no_cli);
                        $('._d_cli').html(da.di_cli);
                        $('._t_cli').html(da.tl_cli);
                        $('._e_cli').html(da.em_env);
                        $('._f_cli').html(da.de_sri+'-'+da.co_caj+'-'+da.nu_doc);
                        $('._c_art').html(da.co_art);
                        $('._d_art').html(da.ds_art);
                        $('._n_ser').html(da.nu_ser);
                        $('._p_con').html(da.va_art);
                        $('._ma_art').html(da.ds_mar);
                        $('._mo_art').html(da.ds_mod);
                        $('._c_art_ga').html(da.co_art_ga);
                        $('._d_art_ga').html(da.ds_art_ga);
                        $('._du_art_ga').html((parseInt(da.pr_art_gar)*12)+' Meses');
                        $('._pe_art_ga').html(da.va_art_gar);

                        $('#a_h_11').attr('href',url1+'procesos/garantia/co_123/ge_pdf1/'+da.co_con+'/1/Certificado '+da.co_con);
                        $('#a_h_22').attr('href',url1+'procesos/garantia/co_123/ge_pdf2/'+da.co_con+'/1/Contrato '+da.co_con);

                    });

                } else {
                    if(data.data === null){
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    } else {  
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                    }
                }
            }
        });
    } else {
        redirectIntranet();
    }
}
    
function getCli(){
    
    if(va_sess()){
        var cod_cli = $('#txt_ide').val();
        var url1 = $('#txt_urlGe').val();

        if (cod_cli.trim() === ''){
            errorGeneral("Sistema Intranet",'Debe ingresar la identificaci&oacute;n','danger'); 
            return false;
        }

        var datos = "c_c="+cod_cli;
        dataLoading();
        $.ajax({
            type : 'POST',
            url : url1+'procesos/garantia/co_122/retCli',
            data: datos,
            success : function (returnData) {
                removedataLoading();
                var data = eval('('+returnData+')');
                //console.log(data.data)
                if (data.data){ 
                    $('#txt_nomb_ape').val(data.data.nom_cli);
                    $('#txt_direc').val(data.data.dir_cli);
                    $('#txt_conve').val(data.data.tel_cli);
                    $('#txt_celular').val(data.data.tel_cli);
                    $('#txt_mail').val(data.data.mail_cli);
                } else {
                    if(data.data === null){
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                        $('#txt_nomb_ape').val('');
                        $('#txt_direc').val('');
                        $('#txt_conve').val('');
                        $('#txt_celular').val('');
                        $('#txt_mail').val('');
                        return false;
                    } else {  
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                        return false;
                    }
                }

            }
        });
    } else {
        redirectIntranet();
    }
}

$(function() {
    
    $(".sea_f").click(function() {
        $('#txt_g_a').val();
        var cb_ = $('#co_alm').val();
        var cj_ = $('#txt_cj').val();
        var nd_ = $('#txt_dc').val();
        if(cj_.trim() === ''){
            errorGeneral("Sistema Intranet",'Ingrese el # de caja','danger');
            return false;
        }
        if(nd_.trim() === ''){
            errorGeneral("Sistema Intranet",'Ingrese el # de documento','danger');
            return false;
        }
        getFact(cb_,cj_,nd_);
    });
    
    $(".cl_fr_1").click(function() {
        $('#f_new_ga')[0].reset();
        quitarmodalGeneral('DetFact','f_new_ga');
    });
    
    $(".res_g").click(function() {
        $('#f_new_ga')[0].reset();
    });
    
    $(".sav_g").click(function() {
        saveGara();
    });
    
    $(".filtroFact").click(function() {
        searchFact();
    });
    
    $(".sea_c").click(function() {
        getCli();
    });
    
}); 
