var contGe = 0;
    
$(document).ready(function() {
    
    var i = 1;
    var a = 1; 
    
    $('#table_solicitudes').dataTable({
        "aLengthMenu": [
             [100,150, -1],
             [100,150, "Todos"]
         ]
     });

    $(".apr_soli" ).button().click(function(){
        aprobar_solic('','','');
    });
    
    $("#ck_apro").button().click(function(){
        if(i === 1){
            $("input[id='option_apro[]']").prop('checked', true);
            i++;
        } else if(i === 2){
            $(this).prop('checked', false);
            $("input[id='option_apro[]']").prop('checked', false);
            i = 1;
        }
    });

    $("#ck_rech").button().click(function(){
        if(a === 1){
            $("input[id='option_rech[]']").prop('checked', true);
            a++;
        } else if(a === 2){
            $(this).prop('checked', false);
            $("input[id='option_rech[]']").prop('checked', false);
            a = 1;
        }
    });
    
}); 


function aprobar_solic(sol,tip){
    
    var emp = $('#c_emp').val();
    var apro = [];
    var rech = [];
    var url1 = $('#txt_urlGe').val();
    var aprobados = null;
    var rechazados = null;
    var countError = 0;
    var countOK = 0;

    aprobados = $("input[id='option_apro[]']:checked");
    rechazados = $("input[id='option_rech[]']:checked");

    $(aprobados).each( function () {
        apro.push($(this).val());
    });

    $(rechazados).each( function () {
        rech.push($(this).val());
    });

    if(!$.isEmptyObject(apro) || !$.isEmptyObject(rech)){

        var r = confirm("Desea procesar los siguientes registros.");
        if (r == true) {  

            var datos = "i_e="+emp+"&s_a="+apro+"&s_n="+rech;
            dataLoading();
            $.ajax({
                type : 'POST',
                url : url1+"procesos/compras/co_121/aprSol", 
                data: datos,
                success : function (retData){             
                    removedataLoading();    
                    var data = eval(retData);
                    var str = '' ,str_1 = '';

                    if(data){
                        $.each(data,function(a,dato){
                            if(dato.cod_err === '0'){
                                countOK++;
                                str_1 = str_1+'&#9679; '+dato.cod_sol+'<br>';
                                $('#id_'+dato.cod_sol).remove();
                            } else {
                                countError++;
                                str = str+'&#9679; '+dato.cod_sol+'<br>';
                            }
                        });

                        if (countError >= 1){
                            errorGeneral("Sistema Intranet",'Solicitudes no Procesadas: <br>'+str,'danger');
                        }

                        if (countOK >= 1){
                            errorGeneral("Sistema Intranet",'Solicitudes Procesadas Correctamente','success');
                        }
                    }
                }
            });

        }

    } else {
        errorGeneral("Sistema Intranet",'Debe Seleccionar una solicitud.','danger'); 
    }
}

function viewSolic(emp,id){
    
    $('#view_Soli_cab').html(''); 
    $('#titleDoc').html('');
    
    var url1 = $('#txt_urlGe').val();
    var datos = "i_s="+id+"&i_e="+emp;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+"procesos/compras/co_121/viSol", 
        data: datos,
        success : function (retData){             
            removedataLoading();    
            var data = eval('('+retData+')');
            if(data){
                $.each(data,function(a,dato){
                    $('#titleDoc').html('Orden de Compra: '+id);
                    $('#view_Soli_cab').html(dato.text);
                });
            }
        }
    });
    
    newModal('DetallSolic','70','95','form_DetallSolic');
}