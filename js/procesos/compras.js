function validaBusqueda(){
    
    if ($("#cmb_estado").val().trim() === '') {
        errorGeneral("Sistema Intranet",'Campo Estado esta vacio','danger');
        return false;
    }
    
    var fechaInicial = $("#txt_fec_ini").val();
    var fechaFinal = $("#txt_fec_fin").val();
    
    var arregloFI=fechaInicial.split('/');
    var arregloFF=fechaFinal.split('/');
    
    var compFechaFI = arregloFI[0]+arregloFI[1]+arregloFI[2];
    var compFechaFF = arregloFF[0]+arregloFF[1]+arregloFF[2];
    
    if ((fechaInicial.trim() === "") || (fechaFinal.trim() === "") ){
        errorGeneral("Sistema Intranet","Verifique que los campos fecha inicio y fecha final tengan datos",'danger');
        return false;
    }
    
    if( compFechaFI > compFechaFF){
        errorGeneral("Sistema Intranet","Verifique que la fecha inicial sea menor a la fecha final",'danger');
        return false;
    }
    
    return true;
}

function aprobardocumentos(tipo,cambia_tipo){
    
    var docu = [];
    var url1 = $('#txt_urlGe').val();
    var select = $("input[name='option_req[]']:checked");
    
    if($(select).length <= 0){
        errorGeneral("Sistema Intranet",'Seleccione una requisición para poder enviarla.','danger');
        return false;
    }    
    
    $(select).each( function () {
        docu.push($(this).val());
    });
    
    var datos = "tp="+tipo;
    datos = datos + "&tc="+cambia_tipo;
    datos = datos + "&d_r="+docu;
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        async: false,
        url : url1+'general/c_general_compras/updateDocCompra',
        data: datos,
        success : function (returnData) {
            var data =returnData.trim(); 
            removedataLoading();         
            if(data.trim()==="OK"){
                errorGeneral("Sistema Intranet",'Envio con exito','success'); 
                getDocCompras();
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }
        }
           
    });
    
}