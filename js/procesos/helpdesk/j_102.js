
var option_count = 0;

$(function() {
    
    $( "ul#contUsersAsigSelect" ).sortable({
        placeholder: "portlet-placeholder ui-corner-all"
    });
    
    $('#txt_fec_top').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        minDate: new Date(),
        timepicker:false
    });
    
    $( "#SlidersImg" ).disableSelection();
    
    $('#areaUserSelect').select2({ 
        width: '100%',
        placeholder: 'Seleccionar Usuario'
    });
    
    $('#areaUserSelect').on("change", function(e) {
          $('#cod_Empl_Asig').val(this.value);
    });
    
    var fec_1 =  $('#f_i').val();
    var now_1 = new Date(fec_1);
    now_1.setTime(now_1.getTime()+1*24*60*60*1000);
    
    var fec_2 =  $('#f_f').val();
    var now_2 = new Date(fec_2);
    now_2.setTime(now_2.getTime()+1*24*60*60*1000);
    
    $('#reportrange').daterangepicker({
            startDate: now_1,
            endDate: now_2,
            dateLimit: {days:365},
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Consultar',
                fromLabel: 'Desde',
                toLabel: 'Hasta',
                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                firstDay: 1
            }
        },
        
        function(start,end){
            $('#f_i').val(start.format("YYYY-MM-DD"));
            $('#f_f').val(end.format("YYYY-MM-DD"));
            $('#reportrange span').html(start.format('MMMM ,D YYYY') + ' - ' + end.format('MMMM ,D YYYY'));
            document.getElementById("f_help").submit();
        }
    );
    
    $( ".add_option" ).button().click(function(){
        var b = $('#areaUserSelect option:selected').val();
        if(b.trim() === 'NN'){
            errorGeneral("Sistema Intranet",'Debe seleccionar un usuario','danger'); 
            return false;
        }
        option_count++;
        var a = option_count;
        var c = $('#areaUserSelect option:selected').text();
        var d = $('#ObsevaUsua').val();
        var e = 'I';
        addUserAsig(a,b,c,d,e,'');
    });
    
    $( ".actualizaActivi" ).button().click(function(){
        actualizActividades();
    });
    
    $( ".excel_btn" ).button().click(function(){
        exportExcel();
    });
    
}); 

function actualizActividades(){
    var url = $('#txt_urlGe').val();
    var order = []; 
    $('#contUsersAsigSelect li').each( function(e) {
        order.push( $(this).attr('id')  + '=' + ( $(this).index() + 1 ) );
    });
    var positions = order.join('*//*');

    var datos = "order="+positions;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url+"procesos/helpdesk/co_102/actualizaPrioridadActividad", 
        data: datos,
        success : function (returnData) { 
            removedataLoading();
            errorGeneral("Sistema Intranet",returnData,'success'); 
        }
    });
}

function addUserAsig(a,b,c,d,e,f){
    
    var z = '<tr id="option-'+a+'">';
        z = z +'<td>';
        z = z +'<b class="spa-2">Usuario:</b> <div id="option_user['+a+'][nameUser]" name="option_user['+a+'][nameUser]" class="spa-10">'+c+'</div>';
        z = z +'<b class="spa-2">Observación:</b> <div id="option_user['+a+'][obsUser]" name="option_user['+a+'][obsUser]"  class="spa-10">'+d+'</div>';
        z = z +'</td>';
        z = z +'<td><button type="button" class="btn btn-circle btn-success add_option" onclick="remove_detalle('+a+')"><i class="text-blanco fa fa-minus"></i></button></td>';
        z = z +"<input type='hidden' id='option_user["+a+"][accion]' name='option_user["+a+"][accion]' value='"+e+"'></td>";
        z = z +"<input type='hidden' id='option_user["+a+"][coAct]' name='option_user["+a+"][coAct]' value='"+f+"'></td>";
        z = z +"<input type='hidden' id='option_user["+a+"][idUser]' name='option_user["+a+"][idUser]' value='"+b+"'></td>";
        z = z +'</tr>';
    
    $('#detalleAsigUsers').append(z);
    
    if(a === 1){
        var btn = '<button type="button" class="btn btn-success saveAsign">Guardar</button>';
        $('#cont_asig').append(btn);
        $( ".saveAsign" ).button().click(function(){
            saveAsignaciones();
        });
    }
    $('#ObsevaUsua').val('');
    
}

function coincidencia(a){
    
//    if(coincidencia($('#cod_Empl_Asig').val())){
//        errorGeneral("Sistema Intranet",'No puede ingresar dos veces el mismo usuario','danger'); 
//        return false;
//    }
//    
    var cantidad=option_count;
    var d="";
    a=a.trim();
    for(var i=1;i<=cantidad;i++){
        d= document.getElementById('option_user['+i+'][idUser]').value;
        if(d.trim() == a)
           return true; 
    }
    return false;
}   

function remove_detalle(id) {
    
    if(confirm('Esta seguro de que desea eliminar este registro ?')){ 
        
        if (document.getElementById('option_user['+id+'][accion]').value === 'U'){
            var url_1 = $('#txt_urlGe').val();
            var datos = "c_s="+$('#hid_cod_soli').val(); 
            datos = datos +'&c_e='+document.getElementById('option_user['+id+'][idUser]').value;
            datos = datos +'&c_a='+document.getElementById('option_user['+id+'][coAct]').value;
            $.ajax({
                type : 'POST',
                url : url_1+"procesos/helpdesk/co_102/UpdateAsig", 
                data: datos,
                success : function (returnData){
                    errorGeneral("Sistema Intranet",returnData,'success'); 
                }
            });
        } else {
            errorGeneral("Sistema Intranet",'Registro Eliminado Correctamente','success'); 
        }
        
        $('#option-'+id).remove();
        var cantidad=option_count;
        var cont = 0;
        for(var i=1;i<=cantidad;i++){
            if (document.getElementById('option_user['+i+'][idUser]')){
                cont++;
            }
        }

        if(cont === 0){
            option_count=0;
            $('#cont_asig').html('');
        }

        cont=0;
    }
}

function getSolicit(a,b,dpto){
    var text = a === 'S'?'Asignar':'Por Vencer';
    var url1 = $('#txt_urlGe').val();
    var tipo_soli = b === conf_hd.sol[0].name?conf_hd.sol[0].type:conf_hd.sol[1].type;
    var cl = b === conf_hd.sol[0].name?conf_hd.sol[0].bg+" "+conf_hd.sol[0].color:conf_hd.sol[1].bg+" "+conf_hd.sol[1].color;
    var datos = "t_p="+a;  
    datos = datos+"&t_s="+tipo_soli; 
    datos = datos+"&d_o="+dpto;   
    
    $('#modalSolicit > div > div > div').removeClass();
    $('#modalSolicit > div > div > div').addClass(cl);
    
    $('#titleDoc').html(text+" ("+b+'s)');
    newModal('Solicit','75','60','form_Solicit');
    dataLoading();
    
    $.ajax({
        type : 'POST',
        async: false,
        data: datos,
        url : url1+'procesos/helpdesk/co_102/retTipoSolicit',
        
        success : function (returnData) {
            var data = eval('('+returnData+')'); 
            removedataLoading();
            $('#tableSolicit').html('');
            var row = '<table id="inner_table_solic" class="table table-bordered table-hover">';
                row = row+'<thead style="background: #DDD;">';
                row = row+'<th>Cod. Solicitud</th>';
                row = row+'<th>Tipo</th>';
                row = row+'<th>Titulo</th>';
                row = row+'<th>Estado</th>';
                row = row+'<th>Fecha de Ingreso</th>';
                row = row+'<th>Acción</th>';
                row = row+'</thead>';
                row = row+'<tbody>';
            if (data.data){
                $.each(data.data,function(e,dato){
                    var funcion =  'viewSolic('+"'"+dato.co_tipo+"'"+','+dato.co_solicitud+','+"''"+',1)';
                    var funcionI =  'asigSolic('+dato.co_solicitud+','+"'"+b+"'"+')';
                    var linKK = a === 'P'?funcion:funcionI;
                    row = row+'<tr>';
                    row = row+'<td>'+dato.co_solicitud+'</td>';
                    row = row+'<td>'+dato.nom_tipo+'</td>';
                    row = row+'<td>'+dato.tx_titulo+'</td>';
                    row = row+'<td>'+dato.nom_estado+'</td>';
                    row = row+'<td class="centro">'+dato.fe_ingreso+'</td>';
                    row = row+'<td class="centro"><a href="javascript:void(0)" onclick="'+linKK+'">'+text+'</a></td>';
                    row = row+'</tr>';
                });

            } else {
                errorGeneral("Sistema Intranet",returnData,'success'); 
            }

            row = row+'</tbody>';
            row = row+'</table>';

            $('#tableSolicit').append(row);
        }
           
    });
    
    $('#inner_table_solic').dataTable();
    $('.esx').removeClass();
}

function asigSolic(co_solic,tipo){
    
    var t = '';
    if(tipo === conf_hd.sol[0].name){
        t = conf_hd.sol[0].type;
        $('#ObsevaUsua').show();
    } else {
        t = conf_hd.sol[1].type;
        $('#ObsevaUsua').hide();
    }
    $('#areaUserSelect').select2({ 
        width: '100%',
        placeholder: 'Seleccionar Usuario'
    });
    option_count = 0;
    $('#cont_asig').html('');
    $('#detalleAsigUsers').html('');
    newModal('AsigSolicit','95','85','form_AsigSolicit'); 
    $('#modalAsigSolicit').css('z-index','1042');
    $('#fadeAsigSolicit').css('z-index','1041');
    var url_1 = $('#txt_urlGe').val();
    var datos = "c_s="+co_solic;    
    $('#hid_cod_soli').val(co_solic);
    $('#hid_tip_soli').val(t);
    $('#titleAsigSol').html(tipo+" #"+co_solic);
    $('#iconAsigSolict').removeClass();
    $('#titleAsigSolict').html('');
    $('#contAsigSolict').html('');
    $('input[name="star"]').attr('checked',false);
    
    var cl = tipo === conf_hd.sol[0].name?conf_hd.sol[0].bg+" "+conf_hd.sol[0].color:conf_hd.sol[1].bg+" "+conf_hd.sol[1].color;
    $('#modalAsigSolicit > div > div > div').removeClass();
    $('#modalAsigSolicit > div > div > div').addClass(cl);
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_101/retCodSolicit", 
        data: datos,
        success : function (returnData) {
            var data = eval('('+returnData+')'); 
            removedataLoading();
            if(!$.isEmptyObject(data.sol)) {
                $.each(data.sol,function(e,dato){
                    
                    $('#iconAsigSolict').addClass("fa "+dato.icon);
                    $('#titleAsigSolict').html(dato.tx_titulo);
                    $('#contAsigSolict').html(dato.tx_descripcion_detallada);
                    $('#txt_fec_top').val(dato.fe_tope);
                    $('#star-'+dato.prioridad_usuario).prop("checked", true);
                    
                    $('._ing_sol').html(dato.nomb_empl_ing);
                    $('._ing_cco_sol').html(dato.nomb_ccosto_ing);
                    $('._ing_fec_sol').html(dato.fe_ingreso);
                    
                });
                
                retAsignacionSolicitud(url_1,datos);
                retCodSolictComent(co_solic,'idComentsSolic_1',1); 
            }
        }
    });
}

function asigUsers(co_emple,id_dpt){
    
    var url_1 = $('#txt_urlGe').val();
    var datos = "c_e="+co_emple+'&i_d='+id_dpt;    
    $('#contUsersAsigSelect').html('');
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_102/retActiUser", 
        data: datos,
        success : function (returnData) {
            var data = eval(returnData); 
            removedataLoading();
            if (data){ 
                var i = 0;
                var solici = [];
                $.each(data,function(e,dato){
                    //console.log(dato);
                    if (i === 0){
                        var img = '<img class="img_circle" src="'+url_1+'img/fotos/'+dato.co_empleado+'.JPG">';
                        $('#imgUserAsig').html(img);
                        $('#titleUser').html(dato.nombre);
                    }
                    i++;
                    solici.push(dato.co_solicitud);
                    var row = '<li class="ui-state-default" id="'+dato.co_solicitud+'-'+dato.co_actividad+'">';

                    var classs = dato.fl_siendo_atendido === 1?'border_green':'border_gris';
                    var activo_color = dato.fl_siendo_atendido === 1?'label-success':'label-danger';
                    
                    row = row+'<div class="external-event '+classs+'">';
                    row = row+'<div class="ibox">';
                    row = row+'<div class="ibox-content">';

                    row = row+'<div class="m-b-md" style="position: relative;">';
                    row = row+'<button title="Ver Solicitud" type="button" class="btn btn-circle btn-success" onclick="asigSolic('+"'"+dato.co_solicitud+"'"+','+"'"+dato.tipo+"'"+');" style="position: absolute; right: 23px;margin: 0.2% auto;"><i class="text-blanco text-size-1 fa fa-plus"></i></button>';
                    row = row+'<button title="Finalizar Asignación" type="button" class="btn btn-circle btn-warning" onclick="_f_asig('+"'"+dato.co_solicitud+"'"+','+"'"+dato.tipo+"'"+','+id_dpt+','+dato.co_actividad+','+co_emple+');" style="position: absolute; right: 0;margin: 0.2% auto;"><i class="text-blanco text-size-1 fa fa-check"></i></button>';
                    row = row+' <b>'+dato.tipo+' #'+dato.co_solicitud+' - <i class="fa '+dato.icon+'"></i> '+dato.tx_titulo+'</b>';
                    row = row+'</div>';
                    
                    row = row+'<div class="row" style="padding: 16px;">';
                        row = row+'<div class="col-xs-12">';
                            row = row+dato.tx_detalle;
                            row = row+'<dl class="dl-horizontal">';
                                row = row+'<dt>Tiempo Estimado:</dt> <dd><input type="text" id="range'+dato.co_solicitud+'-'+dato.co_actividad+'" value="" name="range'+dato.co_solicitud+'-'+dato.co_actividad+'" /></dd>';
                            row = row+'</dl>';
                        row = row+'</div>';
                        
                        row = row+'<div class="col-xs-4">';
                            row = row+'<dl class="dl-horizontal">';
                                row = row+'<dt>Fecha Solicitud:</dt><dd>'+dato.fe_ingreso+'</dd>';
                            row = row+'</dl>';
                        row = row+'</div>';
                        
                        row = row+'<div class="col-xs-4">';
                            row = row+'<dl class="dl-horizontal">';
                                row = row+'<dt>Fecha Asignación:</dt><dd>'+dato.fe_asignacion+'</dd>';
                            row = row+'</dl>';
                        row = row+'</div>';
                        
                        row = row+'<div class="col-xs-4">';
                            row = row+'<dl class="dl-horizontal">';
                                row = row+'<dt>Fecha Ingreso:</dt><dd>'+dato.fe_ingreso+'</dd>';
                            row = row+'</dl>';
                        row = row+'</div>';
                        
                    row = row+'</div>';
                    
                    row = row+'<div class="row">';
                        row = row+'<div class="col-lg-5">';
                        row = row+'</div>';
                    row = row+'</div>';


                    row = row+'</div>';
                    row = row+'</div>';
                    row = row+'<div class="clearfix"></div></div>';

                    row = row+'</li>';
                    var slider = $("#range"+dato.co_solicitud+'-'+dato.co_actividad).data("ionRangeSlider");
                    slider && slider.destroy();
                    
                    $('#contUsersAsigSelect').append(row);
                    $("#range"+dato.co_solicitud+'-'+dato.co_actividad).ionRangeSlider({
                        min: 0,
                        max: 100,
                        from: dato.va_progreso,
                        prefix: "%",
                        disable: true
                    });
                });
                
                newModal('Users','50','90','form_Users'); 
                
            } else {
                errorGeneral("Sistema Intranet",'No hay actividades o ya se encuentran finalizadas','success'); 
            }
        }
    });
}

function _f_asig(co_sol,ti_sol,co_dep,co_act,co_emp){
    var r = confirm("Desea finalizar esta actividad ?");
    if (r == true) {  
        
        var url_1 = $('#txt_urlGe').val();
        var datos = "c_s="+co_sol;
        datos = datos+"&c_e="+co_emp;
        datos = datos+"&t_s="+ti_sol;
        datos = datos+"&i_d="+co_dep;
        datos = datos+"&c_a="+co_act;
        datos = datos+"&p_e="+100;
        datos = datos+"&t_o=0";
        datos = datos+"&u_d=H";

        dataLoading();
        $.ajax({
            type : 'POST',
            url : url_1+"procesos/helpdesk/co_103/savePorcTiempActi", 
            data: datos,
            success : function (returnData) {
                var data =returnData.trim();
                removedataLoading();
                if(data.trim()==="OK"){ 
                    window.location = window.location.href;
                } else {
                    errorGeneral("Sistema Intranet",data,'danger'); 
                }
            }
        });
    }
}

function retAsignacionSolicitud(a,b){
    $.ajax({
        type : 'POST',
        url : a+"procesos/helpdesk/co_102/retSoliAsig", 
        data: b,
        success : function (returnData) {
            var data = eval(returnData); 
            removedataLoading();
            if (data){ 
                $.each(data,function(e,dato){
                    option_count++;
                    addUserAsig(option_count,dato.co_asignado,dato.nombres,dato.tx_detalle,'U',dato.co_actividad) 
                });
            }
        }
    });
}

function saveAsignaciones(){
    var url_1 = $('#txt_urlGe').val();
    var cantidad=option_count;
    var asig = [];
    var count = 0;
    //var prio = $("input[name='star']:checked").attr('id');
    var prio = $("input[name='star']").is(':checked')?$("input[name='star']:checked").attr('id'):0;
    var fec_top = $("#txt_fec_top").val();
    
    for(var i=1;i<=cantidad;i++){
        if (document.getElementById('option_user['+i+'][idUser]')){
            if (document.getElementById('option_user['+i+'][accion]').value === 'I'){
                count++;
                asig.push(document.getElementById('option_user['+i+'][idUser]').value+"/*/"+document.getElementById('option_user['+i+'][obsUser]').innerHTML);
            }
        }
    }
    //if (count >= 1){
        dataLoading();
        var datos = "d_e="+asig.join('|'); 
            datos = datos+"&c_s="+$('#hid_cod_soli').val()+"&t_s="+$('#hid_tip_soli').val()+"&i_d="+$('#dpto').val()+"&p_d="+prio+"&f_e="+fec_top; 
        $.ajax({
            type : 'POST',
            url : url_1+"procesos/helpdesk/co_102/saveAsignaciones", 
            data: datos,
            success : function (returnData){
                removedataLoading();
                option_count = 0;
                $('#detalleAsigUsers').html('');
                $('#cont_asig').html('');
                var datos = "c_s="+$('#hid_cod_soli').val();    
                retAsignacionSolicitud(url_1,datos);
                errorGeneral("Sistema Intranet",returnData,'success'); 
            }
        });
//    } else {
//        errorGeneral("Sistema Intranet",'Grabado Correctamente','success'); 
//    }
    count=0;
}

function passDpto(a){
    $('#dpto').val(a);
    document.getElementById("f_help").submit();
}

function exportExcel(){
    var datos = "c_d="+$('#dpto').val()+"&f_i="+$('#f_i').val()+"&f_f="+$('#f_f').val(); 
    $.download('/ipycca/procesos/helpdesk/co_102/exportExcel',datos);
}