$(function() {
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            })
        },
        timepicker:false
    });
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            })
        },
        timepicker:false
    });
    
    $( ".btnN" ).button().click(function(){
        $('#txt_accion').val('I');
        $('#txt_co_estado').val('');
        $('#id_co_solic').val('');
        $('#newTitle').html('Nueva Solicitud');
        newModal('Solicit','75','80','form_Solicit');
    });
    
    $( ".resetForm" ).button().click(function(){
        resetSolici();
    });
    
    $( ".sendForm" ).button().click(function(){
        saveSolici('A');
    });
    
    $( ".saveForm" ).button().click(function(){
        saveSolici('I');
    });

    $( ".cancelSolict" ).button().click(function(){
        var id = $('#id_Viewsolic').val();
        cancelSolict(id);
    });
    
    $( ".add_option" ).button().click(function(){
        var b = $('#areaUserSelect option:selected').val();
        if(b.trim() === 'NN'){
            errorGeneral("Sistema Intranet",'Debe seleccionar un usuario','danger'); 
            return false;
        }
        option_count++;
        var a = option_count;
        var c = $('#areaUserSelect option:selected').text();
        var d = $('#ObsevaUsua').val();
        var e = 'I';
        addUserAsig(a,b,c,d,e,'');
    });
    
    $( ".savePorceTiempo" ).button().click(function(){
        saveActPorTiempo();
    });
    
}); 

function saveActPorTiempo(){
    
    var tip = $('#tipo_sol').val();
    if(($('#txt_tiempo').val().toString().trim() === '' || $('#txt_tiempo').val().toString().trim() === '0') && tip === conf_hd.sol[0].type){
        errorGeneral("Sistema Intranet",'El campo Tiempo se encuentra vacio','danger'); 
        return false;
    }
    
    if($('#cmb_unidad').val().toString().trim() === '' && tip === conf_hd.sol[0].type){
        errorGeneral("Sistema Intranet",'El campo Unidad de Tiempo se encuentra vacio','danger'); 
        return false;
    }
    
    var url_1 = $('#txt_urlGe').val();
    var datos = "c_s="+$('#id_Viewsolic').val();
    datos = datos+"&t_s="+tip;
    datos = datos+"&i_d="+$('#dpto').val();
    datos = datos+"&c_a="+$('#id_ViewActi').val();
    datos = datos+"&p_e="+$('#porcentaje').val();
    datos = datos+"&t_o="+$('#txt_tiempo').val();
    datos = datos+"&u_d="+$('#cmb_unidad').val();
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_103/savePorcTiempActi", 
        data: datos,
        success : function (returnData) {
            var data =returnData.trim();
            removedataLoading();
            if(data.trim()==="OK"){ 
                window.location = window.location.href;
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }
        }
    });
}

function passDpto(a){
    $('#dpto').val(a);
    document.getElementById("f_help").submit();
}

function modalAllActivi(depart){
    newModal('ViewAllSolicit','80','75','form_ViewAllSolicit');
    getAllActiv(depart);
}

function getAllActiv(depart) {
    var url1 = $('#txt_urlGe').val();
    var datos = "f_i="+$('#txt_fec_ini').val();  
    datos = datos+"&f_f="+$('#txt_fec_fin').val(); 
    datos = datos+"&c_d="+depart; 
    dataLoading();
    $.ajax({
        type : 'POST',
        data: datos,
        url : url1+'procesos/helpdesk/co_103/getAllActivt',
        success : function (returnData) {
            var data = eval(returnData); 
            removedataLoading();
            var clase = '';
            $('#tableAllSolicit').html('');
            var row = '<table id="tableAll" class="table table-bordered">';
                row = row+'<thead style="background: #DDD;">';
                row = row+'<th>Cod. Solicitud</th>';
                row = row+'<th>Tipo</th>';
                row = row+'<th>Titulo</th>';
                row = row+'<th>Observación</th>';
                row = row+'<th>Fecha de Ingreso</th>';
                row = row+'<th>Acción</th>';
                row = row+'</thead>';
                row = row+'<tbody>';

            if (data){
                $.each(data,function(e,dato){
                    clase = parseInt(dato.fl_siendo_atendido) ===1?'enCurso':'';
                    var funcion =  'viewSolic('+"'"+dato.co_tipo+"'"+','+dato.co_solicitud+','+"'"+dato.co_actividad+"//"+dato.tx_detalle+"//"+dato.co_unidad_tiempo+"//"+dato.nu_tiempo_estimado+"//"+dato.va_progreso+"'"+',1)';

                    row = row+'<tr class="'+clase+'">';
                    row = row+'<td>'+dato.co_solicitud+'</td>';
                    row = row+'<td>'+dato.tipo+'</td>';
                    row = row+'<td>'+dato.tx_titulo+'</td>';
                    row = row+'<td>'+dato.tx_detalle+'</td>';
                    row = row+'<td class="centro">'+dato.fe_ingreso+'</td>';
                    row = row+'<td class="centro"><a href="javascript:void(0)" onclick="'+funcion+'">Visualizar Solicitud</a></td>';
                    row = row+'</tr>';
                });
            }

            row = row+'</tbody>';
            row = row+'</table>';

            $('#se_solic').val('');  
            $('#tableAllSolicit').append(row);
            
            $('#tableAll').dataTable();
            $('.esx').removeClass();
        }
        
           
    });
    
}

function getSolicit(a,b,dpto){
    
    if(a === 'P'){
        $('#m_n1').remove();
        $('#tab1').remove();
        $('#m_n2').addClass('active');
        $('#tab2').addClass('active');
    }
    
    var text = a === 'S'?'Asignar':'Por Vencer';
    var url1 = $('#txt_urlGe').val();
    var tipo_soli = b === conf_hd.sol[0].name?conf_hd.sol[0].type:conf_hd.sol[1].type;
    var cl = b === conf_hd.sol[0].name?conf_hd.sol[0].bg+" "+conf_hd.sol[0].color:conf_hd.sol[1].bg+" "+conf_hd.sol[1].color;
    var datos = "t_p="+a;  
    datos = datos+"&t_s="+tipo_soli; 
    datos = datos+"&d_o="+dpto;   
    
    $('#modalSolicit > div > div > div').removeClass();
    $('#modalSolicit > div > div > div').addClass(cl);
    
    $('#titleDoc').html(text+" ("+b+')');
    newModal('Solicit','75','60','form_Solicit');
    dataLoading();
    
    $.ajax({
        type : 'POST',
        async: false,
        data: datos,
        url : url1+'procesos/helpdesk/co_103/retTipoSolicit',
        
        success : function (returnData) {
            
            var data = eval('('+returnData+')'); 
            removedataLoading();
            $('#tableSolicit').html('');
            var row = '<table id="inner_table_solic" class="table table-bordered table-hover">';
                row = row+'<thead style="background: #DDD;">';
                row = row+'<th>Cod. Solicitud</th>';
                row = row+'<th>Tipo</th>';
                row = row+'<th>Titulo</th>';
                row = row+'<th>Estado</th>';
                row = row+'<th>Fecha de Ingreso</th>';
                row = row+'<th>Acción</th>';
                row = row+'</thead>';
                row = row+'<tbody>';
            if (data.data){
                $.each(data.data,function(e,dato){
                    var funcion =  'viewSolic('+"'"+dato.co_tipo+"'"+','+dato.co_solicitud+','+"''"+',1)';
                    var funcionI =  'asigSolic('+dato.co_solicitud+','+"'"+b+"'"+')';
                    var linKK = a === 'P'?funcion:funcionI;
                    row = row+'<tr>';
                    row = row+'<td>'+dato.co_solicitud+'</td>';
                    row = row+'<td>'+dato.nom_tipo+'</td>';
                    row = row+'<td>'+dato.tx_titulo+'</td>';
                    row = row+'<td>'+dato.nom_estado+'</td>';
                    row = row+'<td class="centro">'+dato.fe_ingreso+'</td>';
                    row = row+'<td class="centro"><a href="javascript:void(0)" onclick="'+linKK+'">'+text+'</a></td>';
                    row = row+'</tr>';
                });

            } else {
                errorGeneral("Sistema Intranet",returnData,'success'); 
            }

            row = row+'</tbody>';
            row = row+'</table>';

            $('#tableSolicit').append(row);
            
//                var data = eval('('+returnData+')'); 
//                removedataLoading();
//                $('#tableSolicit').html('');
//                
//                var row = '<table id="inner_table_solic" class="table table-bordered table-hover">';
//                    row = row+'<thead style="background: #DDD;">';
//                    row = row+'<th>Cod. Solicitud</th>';
//                    row = row+'<th>Tipo</th>';
//                    row = row+'<th>Titulo</th>';
//                    row = row+'<th>Estado</th>';
//                    row = row+'<th>Fecha de Ingreso</th>';
//                    row = row+'<th>Acción</th>';
//                    row = row+'</thead>';
//                    row = row+'<tbody>';
//                        
//                if (data){
//                    $.each(data,function(e,dato){
//                        var funcion =  'viewSolic('+"'"+dato.co_tipo+"'"+','+dato.co_solicitud+','+"''"+',1)';
//                        var funcionI =  'asigSolic('+dato.co_solicitud+','+"'"+b+"'"+')';
//                        var linKK = a === 'P'?funcion:funcionI;
//                        row = row+'<tr>';
//                        row = row+'<td>'+dato.co_solicitud+'</td>';
//                        row = row+'<td>'+dato.nom_tipo+'</td>';
//                        row = row+'<td>'+dato.tx_titulo+'</td>';
//                        row = row+'<td>'+dato.nom_estado+'</td>';
//                        row = row+'<td class="centro">'+dato.fe_ingreso+'</td>';
//                        row = row+'<td class="centro"><a href="javascript:void(0)" onclick="'+linKK+'">'+text+'</a></td>';
//                        row = row+'</tr>';
//                    });
//                    
//                }
//
//            row = row+'</tbody>';
//            row = row+'</table>';
//
//            $('#tableSolicit').append(row);

        }
           
    });
    
    $('#inner_table_solic').dataTable();
    $('.esx').removeClass();
}

function asigSolic(co_solic,tipo){
    var t = '';
    if(tipo === conf_hd.sol[0].name){
        t = conf_hd.sol[0].type;
        $('#ObsevaUsua').show();
    } else {
        t = conf_hd.sol[1].type;
        $('#ObsevaUsua').hide();
    }
    option_count = 0;
    $('#cont_asig').html('');
    $('#detalleAsigUsers').html('');
    var url_1 = $('#txt_urlGe').val();
    var datos = "c_s="+co_solic;    
    $('#hid_cod_soli').val(co_solic);
    $('#hid_tip_soli').val(t);
    $('#titleAsigSol').html(tipo+" #"+co_solic);
    $('#iconAsigSolict').removeClass();
    $('#titleAsigSolict').html('');
    $('#contAsigSolict').html('');
    
    var cl = tipo === conf_hd.sol[0].name?conf_hd.sol[0].bg+" "+conf_hd.sol[0].color:conf_hd.sol[1].bg+" "+conf_hd.sol[1].color;
    $('#modalAsigSolicit > div > div > div').removeClass();
    $('#modalAsigSolicit > div > div > div').addClass(cl);
    
    newModal('AsigSolicit','95','85','form_AsigSolicit'); 
    $('#modalAsigSolicit').css('z-index','1042');
    $('#fadeAsigSolicit').css('z-index','1041');
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_101/retCodSolicit", 
        data: datos,
        success : function (returnData) {
            var data = eval('('+returnData+')'); 
            removedataLoading();
            if(!$.isEmptyObject(data.sol)) {
                $.each(data.sol,function(e,dato){
                    $('#iconAsigSolict').addClass("fa "+dato.icon);
                    $('#titleAsigSolict').html(dato.tx_titulo);
                    $('#contAsigSolict').html(dato.tx_descripcion_detallada);
                    $('._ing_sol').html(dato.nomb_empl_ing);
                    $('._ing_cco_sol').html(dato.nomb_ccosto_ing);
                    $('._ing_fec_sol').html(dato.fe_ingreso);
                });
                retAsignacionSolicitud(url_1,datos);
            }
        }
    });
}

function retAsignacionSolicitud(a,b){
    $.ajax({
        type : 'POST',
        url : a+"procesos/helpdesk/co_103/retSoliAsig", 
        data: b,
        success : function (returnData) {
            var data = eval(returnData); 
            removedataLoading();
            if (data){ 
                $.each(data,function(e,dato){
                    option_count++;
                    addUserAsig(option_count,dato.co_asignado,dato.nombres,dato.tx_detalle,'U',dato.co_actividad) 
                });
                
                window.location = window.location.href;
            }
        }
    });
}

function saveAsignaciones(){
    var url_1 = $('#txt_urlGe').val();
    var cantidad=option_count;
    var asig = [];
    var count = 0;
    for(var i=1;i<=cantidad;i++){
        if (document.getElementById('option_user['+i+'][idUser]')){
            if (document.getElementById('option_user['+i+'][accion]').value === 'I'){
                count++;
                asig.push(document.getElementById('option_user['+i+'][idUser]').value+"/*/"+document.getElementById('option_user['+i+'][obsUser]').innerHTML);
            }
        }
    }
    if (count >= 1){
        dataLoading();
        var datos = "d_e="+asig.join('|'); 
            datos = datos+"&c_s="+$('#hid_cod_soli').val()+"&t_s="+$('#hid_tip_soli').val()+"&i_d="+$('#dpto').val(); 
        $.ajax({
            type : 'POST',
            url : url_1+"procesos/helpdesk/co_103/saveAsignaciones", 
            data: datos,
            success : function (returnData){
                removedataLoading();
                option_count = 0;
                $('#detalleAsigUsers').html('');
                $('#cont_asig').html('');
                var datos = "c_s="+$('#hid_cod_soli').val();    
                retAsignacionSolicitud(url_1,datos);
                errorGeneral("Sistema Intranet",returnData,'success');
                window.location = window.location.href;
            }
        });
    } else {
        errorGeneral("Sistema Intranet",'Grabado Correctamente','success'); 
    }
    count=0;
}

function addUserAsig(a,b,c,d,e,f){
    
    var z = '<tr id="option-'+a+'">';
        z = z +'<td>';
        z = z +'<b class="spa-2">Usuario:</b> <div id="option_user['+a+'][nameUser]" name="option_user['+a+'][nameUser]" class="spa-10">'+c+'</div>';
        z = z +'<b class="spa-2">Observación:</b> <div id="option_user['+a+'][obsUser]" name="option_user['+a+'][obsUser]"  class="spa-10">'+d+'</div>';
        z = z +'</td>';
        z = z +'<td><button type="button" class="btn btn-circle btn-success add_option" onclick="remove_detalle('+a+')"><i class="text-blanco fa fa-minus"></i></button></td>';
        z = z +"<input type='hidden' id='option_user["+a+"][accion]' name='option_user["+a+"][accion]' value='"+e+"'></td>";
        z = z +"<input type='hidden' id='option_user["+a+"][coAct]' name='option_user["+a+"][coAct]' value='"+f+"'></td>";
        z = z +"<input type='hidden' id='option_user["+a+"][idUser]' name='option_user["+a+"][idUser]' value='"+b+"'></td>";
        z = z +'</tr>';
    
    $('#detalleAsigUsers').append(z);
    
    if(a === 1){
        var btn = '<button type="button" class="btn btn-success saveAsign">Guardar</button>';
        $('#cont_asig').append(btn);
        $( ".saveAsign" ).button().click(function(){
            saveAsignaciones();
        });
    }
    $('#ObsevaUsua').val('');
    
}

function remove_detalle(id) {
    
    if(confirm('Esta seguro de que desea eliminar este registro ?')){ 
        
        if (document.getElementById('option_user['+id+'][accion]').value === 'U'){
            var url_1 = $('#txt_urlGe').val();
            var datos = "c_s="+$('#hid_cod_soli').val(); 
            datos = datos +'&c_e='+document.getElementById('option_user['+id+'][idUser]').value;
            datos = datos +'&c_a='+document.getElementById('option_user['+id+'][coAct]').value;
            $.ajax({
                type : 'POST',
                url : url_1+"procesos/helpdesk/co_103/UpdateAsig", 
                data: datos,
                success : function (returnData){
                    errorGeneral("Sistema Intranet",returnData,'success'); 
                }
            });
        } else {
            errorGeneral("Sistema Intranet",'Registro Eliminado Correctamente','success'); 
        }
        
        $('#option-'+id).remove();
        var cantidad=option_count;
        var cont = 0;
        for(var i=1;i<=cantidad;i++){
            if (document.getElementById('option_user['+i+'][idUser]')){
                cont++;
            }
        }

        if(cont === 0){
            option_count=0;
            $('#cont_asig').html('');
        }

        cont=0;
    }
}

function enCurso(tipo,id,idSoli){    
    
    var datos = "i_d="+id+"&i_s="+idSoli+"&t_p="+tipo; 
    var url_1 = $('#txt_urlGe').val();
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_103/enCurso", 
        data: datos,
        success : function (returnData) {
            var data =returnData.trim();
            removedataLoading();
            if(data.trim()==="OK"){ 
                errorGeneral("Sistema Intranet",'Actividad se encuentra en Curso','success'); 
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }
            //window.location = window.location.href;
        }
    });
    
}