$(function() {
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            })
        },
        timepicker:false
    });
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            })
        },
        timepicker:false
    });
    
    $( ".btnN" ).button().click(function(){
        $('#txt_accion').val('I');
        $('#txt_co_estado').val('');
        $('#id_co_solic').val('');
        $('#newTitle').html('Nueva Solicitud');
        newModal('Solicit','75','80','form_Solicit');
    });
    
    $( ".resetForm" ).button().click(function(){
        resetSolici();
    });
    
    $( ".sendForm" ).button().click(function(){
        saveSolici('A');
    });
    
    $( ".saveForm" ).button().click(function(){
        saveSolici('I');
    });

    $( ".cancelSolict" ).button().click(function(){
        var id = $('#id_Viewsolic').val();
        cancelSolict(id);
    });
    
    $( ".finSolict" ).button().click(function(){
        var id = $('#id_Viewsolic').val();
        finSolict(id);
    });
    
}); 

function modalAllSolicit(depart){
    newModal('ViewAllSolicit','80','75','form_ViewAllSolicit');
    getAllSolici(depart);
}

function getAllSolici(depart) {
    var url1 = $('#txt_urlGe').val();
    var datos = "c_d="+depart;  
    datos = datos+"&f_i="+$('#txt_fec_ini').val();  
    datos = datos+"&f_f="+$('#txt_fec_fin').val();  
    datos = datos+"&c_b="+$('#se_solic').val();  
    dataLoading();
    $.ajax({
        type : 'POST',
        data: datos,
        url : url1+'procesos/helpdesk/co_101/getAllSolicit',
        
        success : function (returnData) {
            var data = eval(returnData); 
            removedataLoading();
            $('#tableAllSolicit').html('');
            var row = '<table id="tableAll" class="table table-bordered table-hover dataTable no-footer">';
                row = row+'<thead style="background:#DDD;">';
                row = row+'<th>Cod. Solicitud</th>';
                row = row+'<th>Tipo</th>';
                row = row+'<th>Titulo</th>';
                row = row+'<th>Estado</th>';
                row = row+'<th>Fecha de Ingreso</th>';
                row = row+'<th>Acción</th>';
                row = row+'</thead>';
                row = row+'<tbody>';
                //viewSolic('T',6)
            if (data){
                $.each(data,function(e,dato){
                    var funcion =  'viewSolic('+"'"+dato.co_tipo+"'"+','+dato.co_solicitud+','+"''"+',0)';
                    var funcionI =  'consulSolici('+dato.co_solicitud+','+"'"+dato.co_estado+"'"+')';
                    var linKK = dato.co_estado === 'I'?funcionI:funcion;
                    var text = dato.co_estado === 'I'?'Modificar':'Visualizar';

                    row = row+'<tr>';
                    row = row+'<td>'+dato.co_solicitud+'</td>';
                    row = row+'<td>'+dato.tx_descripcion+'</td>';
                    row = row+'<td>'+dato.tx_titulo+'</td>';
                    row = row+'<td>'+dato.nom_estado+'</td>';
                    row = row+'<td class="centro">'+dato.fe_ingreso+'</td>';
                    row = row+'<td class="centro"><a href="javascript:void(0)" onclick="'+linKK+'">'+text+'</a></td>';
                    row = row+'</tr>';
                });
            }
            row = row+'</tbody>';
            row = row+'</table>';
            $('#se_solic').val('');  
            $('#tableAllSolicit').append(row);
            $('#tableAll').dataTable();
            $('.esx').removeClass();
        }
    });
}

function passDpto(a){
    $('#dpto').val(a);
    document.getElementById("f_help").submit();
}

function cancelSolict(id){
    
    var r = confirm("Desea anular la solicitud");
    var url_1 = $('#txt_urlGe').val();
    var d = $('#dpto').val();
    var e = $('#ti_Viewsolic').val();
    var datos = "c_s="+id+"&i_d="+d+"&t_s="+e;
    
    if (r == true) {  
        dataLoading();
        $.ajax({
            type : 'POST',
            url : url_1+"procesos/helpdesk/co_101/cancelSolicit", 
            data: datos,
            success : function (returnData) {
                var data =returnData.trim();
                removedataLoading();
                if(data.trim()==="OK"){ 
                    quitarmodalGeneral('ViewSolicit','form_ViewSolicit');
                    errorGeneral("Sistema Intranet",'Registro Anulado con exito','success'); 
                } else {
                    errorGeneral("Sistema Intranet",data,'danger'); 
                }
                window.location = window.location.href;
            }
        });
    } 
}

function consulSolici(id,est){
    $('#txt_accion').val('U');
    $('#txt_co_estado').val(est);
    $('#id_co_solic').val(id);
    newModal('Solicit','75','80','form_Solicit');
    $('#modalSolicit').css('z-index','1042');
    $('#fadeSolicit').css('z-index','1041');
    getSolici(id);
}

function getSolici(id){
    var url_1 = $('#txt_urlGe').val();
    var datos = "c_s="+id;    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_101/retCodSolicit", 
        data: datos,
        success : function (returnData) {
            var data = eval('('+returnData+')'); 
            removedataLoading();
            if(!$.isEmptyObject(data.sol)) {
                $.each(data.sol,function(e,dato){
                    $('#cmb_tipo').val(dato.co_tipo);
                    var tp = dato.co_tipo === conf_hd.sol[0].type?conf_hd.sol[0].name:conf_hd.sol[1].name;
                    $('#newTitle').html('Modificar '+tp+" #"+id);
                    ch_tp(dato.co_tipo);
                    $('#txt_title').val(dato.tx_titulo);
                    CKEDITOR.instances['txt_contenido'].setData(dato.tx_descripcion_detallada);
                });
            }
        }
    });
} 

function ch_tp(a){
    
    if(a.trim() === conf_hd.sol[1].type){
        $('.saveForm').hide();
        $('.sendForm').show();
    } else if(a.trim() === conf_hd.sol[0].type){
        $('.saveForm').show();
        $('.sendForm').show();
    } else {
        $('.saveForm').hide();
    }
    
}

function resetSolici(){
    $('#id_co_solic').val('');
    $('#txt_title').val('');
    CKEDITOR.instances['txt_contenido'].setData('');
    quitarmodalGeneral('Solicit','form_Solicit');
}

function saveSolici(estado){
    var url_1 = $('#txt_urlGe').val();
    var accion = $('#txt_accion').val();
    var idDpto = $('#id_dpto').val();
    var tipo = $('#cmb_tipo').val();;
    var cod_estado = $('#txt_co_estado').val();
    var contenido = CKEDITOR.instances['txt_contenido'].getData();
    var condicion = tipo === conf_hd.sol[0].type && estado ==='A'?'P':estado;
    
    if ($('#cmb_tipo').val().trim() === 'S'){
        errorGeneral("Sistema Intranet",'Seleccione un tipo de Solicitud','danger'); 
        return false;
    }
    
    if ($('#txt_title').val().trim() === ''){
        errorGeneral("Sistema Intranet",'El campo "Titulo" esta vacio','danger'); 
        return false;
    }
    
    if (contenido.trim() === ''){
        errorGeneral("Sistema Intranet",'El campo "Descripción" esta vacio','danger'); 
        return false;
    }

    dataLoading();

    var datos = "a_n="+accion;
    datos = datos+"&t_s="+tipo;
    datos = datos+"&t_o="+$('#txt_title').val();
    datos = datos+"&d_n="+encodeURIComponent(contenido);
    datos = datos+"&i_d="+idDpto;
    datos = datos+"&e_o="+condicion;
    datos = datos+"&c_e="+cod_estado;
    datos = datos+"&b_a="+estado;
    datos = datos+"&c_s="+$('#id_co_solic').val();

    $.ajax({
        type : 'POST',
        url : url_1+"procesos/helpdesk/co_101/guardar", 
        data: datos,
        success : function (returnData) {
            var data =returnData.trim();
            removedataLoading();
            if(data.trim()==="OK"){ 
                $('#id_perfil').val('');
                resetSolici();
                errorGeneral("Sistema Intranet",'Registro grabado correctamente','success'); 
            } else {
                errorGeneral("Sistema Intranet",data,'danger'); 
            }
            window.location = window.location.href;
        }
    });
}

function finSolict(id){
    
    var r = confirm("Desea finalizar la solicitud");
    var url_1 = $('#txt_urlGe').val();
    var d = $('#dpto').val();
    var e = $('#ti_Viewsolic').val();
    var datos = "c_s="+id+"&i_d="+d+"&t_s="+e;
    
    if (r == true) {  
        dataLoading();
        $.ajax({
            type : 'POST',
            url : url_1+"procesos/helpdesk/co_101/finSolicit", 
            data: datos,
            success : function (returnData) {
                var data =returnData.trim();
                removedataLoading();
                if(data.trim()==="OK"){ 
                    quitarmodalGeneral('ViewSolicit','form_ViewSolicit');
                    errorGeneral("Sistema Intranet",'Solicitud finalizada con exito','success'); 
                } else {
                    errorGeneral("Sistema Intranet",data,'danger'); 
                }
                window.location = window.location.href;
            }
        });
    } 
}