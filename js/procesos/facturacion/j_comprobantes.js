
$(function () {
    $('.se_enviados').click(function (a) {
        getEnviados();
    });
    $('.cl_enviados').click(function (a) {
        $('#cmb_estados').val('00');
        $('#cmb_documentos').val('00');
        $('#tx_identificacion').val('');
        $('#tx_estable').val('');
        $('#tx_pemision').val('');
        $('#tx_secuencial').val('');
    });
    $('.se_recibidos').click(function (a) {
        getRecibidos();
    });
    $('.cl_recibidos').click(function (a) {
        $('#cmb_estados_rec').val('00');
        $('#cmb_documentos_rec').val('00');
        $('#tx_identificacion_rec').val('');
        $('#tx_estable_rec').val('');
        $('#tx_pemision_rec').val('');
        $('#tx_secuencial_rec').val('');
    });
});

function getRecibidos() {
    if (va_sess()) {
        var c_an = $('#cmb_anio_rec').val();
        var c_me = $('#cmb_meses_rec').val();
        var c_di = $('#cmb_dia_rec').val();
        var c_au = $('#cmb_estados_rec').val();
        var c_do = $('#cmb_documentos_rec').val();
        var c_id = $('#tx_identificacion_rec').val();
        var c_es = $('#tx_estable_rec').val();
        var c_pe = $('#tx_pemision_rec').val();
        var c_se = $('#tx_secuencial_rec').val();
        var c_co = $('#ck_contabilizado').is(':checked') ? 1 : 0;
        $('#contRecibidos').html('');
        $('#contRecibidosTot').html('');
        $('#pieDocRec').html('');
        $('#pieConRec').html('');
        dataLoading();
        $.post('co_comprobantes/reRecibidos',
                {c_an: c_an, c_me: c_me, c_au: c_au, c_do: c_do, c_es: c_es, c_pe: c_pe, c_se: c_se, c_di: c_di, c_id: c_id, c_co: c_co},
                function (b) {
                    var c = eval('(' + b + ')');
                    if ($.isEmptyObject(c.err)) {
                        $('#contRecibidosTot').html(c.tot);
                        $('#contRecibidos').html('<div class="scrollBar"><table id="tabRecibidos" class="table tabDocEmi table-bordered TFtable floatThead"></table></div>');
                        if (!$.isEmptyObject(c.dat)) {
                            var $table = $('#tabRecibidos').dataTable({
                                bJQueryUI: true,
                                data: c.dat.data,
                                columns: c.dat.columns
                            });
                            $table.floatThead({
                                scrollContainer: function ($table) {
                                    var $wrapper = $table.closest('.dataTables_wrapper');
                                    $wrapper.css({'overflow': 'auto', 'height': '600px'});
                                    return $wrapper;
                                }
                            });
                            if (!$.isEmptyObject(c.con)) {
                                $.plot("#pieConRec", c.con, {
                                    series: {
                                        pie: {
                                            show: true,
                                            radius: 1,
                                            label: {
                                                show: true,
                                                radius: 1,
                                                threshold: 0.1,
                                                formatter: labelFormatter,
                                                background: {
                                                    opacity: 0.7,
                                                    color: '#333'
                                                }
                                            }
                                        }
                                    },
                                    grid: {
                                        hoverable: true
                                    },
                                    legend: {
                                        show: true,
                                        labelFormatter: function (label, series, ab) {
                                            label = label.split('*|*');
                                            return '<div style="font-size:8pt;padding:2px;"><a href="javascript:void(0)" onclick="getDetToma(' + "'" + label[0] + "'" + ',' + "'" + label[1] + "'" + ',' + "'" + series.color + "'" + ',0,' + "''" + ')">' + label[1] + '</a></div>';
                                        }
                                    }
                                });
                            }
                            if (!$.isEmptyObject(c.doc)) {
                                $.plot("#pieDocRec", c.doc, {
                                    series: {
                                        pie: {
                                            show: true,
                                            radius: 1,
                                            label: {
                                                show: true,
                                                radius: 1,
                                                threshold: 0.1,
                                                formatter: labelFormatter,
                                                background: {
                                                    opacity: 0.7,
                                                    color: '#333'
                                                }
                                            }
                                        }
                                    },
                                    grid: {
                                        hoverable: true
                                    },
                                    legend: {
                                        show: true,
                                        labelFormatter: function (label, series, ab) {
                                            label = label.split('*|*');
                                            return '<div style="font-size:8pt;padding:2px;"><a href="javascript:void(0)" onclick="getDetToma(' + "'" + label[0] + "'" + ',' + "'" + label[1] + "'" + ',' + "'" + series.color + "'" + ',0,' + "''" + ')">' + label[1] + '</a></div>';
                                        }
                                    }
                                });
                            }
                        } else {
                            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                        }
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}

function tableEmitidos() {
//    $.get('co_comprobantes/reEnviados', {c_an: 2016, c_me: 12, c_di: 10}, function (c) {
//        $('#contEmitidos').html('<div class="scrollBar"><table id="tabEmitidos" class="table tabDocEmi table-bordered TFtable TFtable1 floatThead"></table></div>');
//        c = eval('(' + c + ')');
//        var $table = $('#tabEmitidos').dataTable({
//            data: c.data,
//            columns: c.columns
//        });
//        $table.floatThead({
//            scrollContainer: function ($table) {
//                var $wrapper = $table.closest('.dataTables_wrapper');
//                $wrapper.css({'overflow': 'auto', 'height': '600px'});
//                return $wrapper;
//            }
//        });
//    })
}

function getEnviados() {
    if (va_sess()) {
        var c_an = $('#cmb_anio').val();
        var c_me = $('#cmb_meses').val();
        var c_di = $('#cmb_dia').val();
        var c_au = $('#cmb_estados').val();
        var c_do = $('#cmb_documentos').val();
        var c_id = $('#tx_identificacion').val();
        var c_es = $('#tx_estable').val();
        var c_pe = $('#tx_pemision').val();
        var c_se = $('#tx_secuencial').val();
        $('#contEmitidos').html('');
        $('#contEmitidosTot').html('');
        $('#pieAut').html('');
        $('#pieDoc').html('');
        dataLoading();
        $.ajaxSetup({
            timeout: 120000
        });
        $.post('co_comprobantes/reEnviados',
                {c_an: c_an, c_me: c_me, c_au: c_au, c_do: c_do, c_es: c_es, c_pe: c_pe, c_se: c_se, c_di: c_di, c_id: c_id},
                function (b) {
                    var c = eval('(' + b + ')');
                    if ($.isEmptyObject(c.err)) {
                        $('#contEmitidosTot').html(c.tot);
                        $('#contEmitidos').html('<div class="scrollBar"><table id="tabEmitidos" class="table tabDocEmi table-bordered TFtable TFtable1 floatThead"></table></div>');

                        if (!$.isEmptyObject(c.dat)) {
                            var $table = $('#tabEmitidos').dataTable({
                                bJQueryUI: true,
                                data: c.dat.data,
                                columns: c.dat.columns
                            });
                            $table.floatThead({
                                scrollContainer: function ($table) {
                                    var $wrapper = $table.closest('.dataTables_wrapper');
                                    $wrapper.css({'overflow': 'auto', 'height': '600px'});
                                    return $wrapper;
                                }
                            });

                            if (!$.isEmptyObject(c.aut)) {
                                $.plot("#pieAut", c.aut, {
                                    series: {
                                        pie: {
                                            show: true,
                                            radius: 1,
                                            label: {
                                                show: true,
                                                radius: 1,
                                                threshold: 0.1,
                                                formatter: labelFormatter,
                                                background: {
                                                    opacity: 0.7,
                                                    color: '#333'
                                                }
                                            }
                                        }
                                    },
                                    grid: {
                                        hoverable: true
                                    },
                                    legend: {
                                        show: true,
                                        labelFormatter: function (label, series, ab) {
                                            label = label.split('*|*');
                                            return '<div style="font-size:8pt;padding:2px;"><a href="javascript:void(0)" onclick="getDetToma(' + "'" + label[0] + "'" + ',' + "'" + label[1] + "'" + ',' + "'" + series.color + "'" + ',0,' + "''" + ')">' + label[1] + '</a></div>';
                                        }
                                    }
                                });

                            }
                            if (!$.isEmptyObject(c.doc)) {
                                $.plot("#pieDoc", c.doc, {
                                    series: {
                                        pie: {
                                            show: true,
                                            radius: 1,
                                            label: {
                                                show: true,
                                                radius: 1,
                                                threshold: 0.1,
                                                formatter: labelFormatter,
                                                background: {
                                                    opacity: 0.7,
                                                    color: '#333'
                                                }
                                            }
                                        }
                                    },
                                    grid: {
                                        hoverable: true
                                    },
                                    legend: {
                                        show: true,
                                        labelFormatter: function (label, series, ab) {
                                            label = label.split('*|*');
                                            return '<div style="font-size:8pt;padding:2px;"><a href="javascript:void(0)" onclick="getDetToma(' + "'" + label[0] + "'" + ',' + "'" + label[1] + "'" + ',' + "'" + series.color + "'" + ',0,' + "''" + ')">' + label[1] + '</a></div>';
                                        }
                                    }
                                });
                            }
                        } else {
                            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                        }
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}

function labelFormatter(a, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;border-radius: 3px;'> " + Math.round(series.percent) + "%</div>";
}