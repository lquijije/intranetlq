var icount = 0;

$(function() {
    
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            });
        },
        timepicker:false,
        closeOnDateSelect:true
    });
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            });
        },
        timepicker:false,
        closeOnDateSelect:true
    });

    $( ".searchDocu" ).button().click(function(){
        getDocuError($('#cmb_tipo').val());
    });
    
    $( ".btn-process" ).button().click(function(){
        getXmlProcess();
    });
   
}); 

function validaFecha(fec_ini,fec_fin){
    var fi = fec_ini.trim().replace(/[/-]/g,"").replace(/\ /g,"");
    var ff = fec_fin.trim().replace(/[/-]/g,"").replace(/\ /g,"");
    if (fi > ff){
        errorGeneral("Sistema Intranet",'La Fecha inicial es mayor que la fecha final','danger'); 
        return false;
    } else {
        return true;
    }
}

function getDocuError(tipo){
    var fi = $('#txt_fec_ini').val();
    var ff = $('#txt_fec_fin').val();
    var al = $('#cmb_almacen').val();
    
    if (!validaFecha(fi,ff)){
        return false;
    }
    
    if (fi.trim() === ''){
        errorGeneral("Sistema Intranet",'Campo Fecha Inicial esta vacio','danger');
        return false;
    }
    
    if (ff.trim() === ''){
        errorGeneral("Sistema Intranet",'Campo Fecha Final esta vacio','danger');
        return false;
    }
    
    if (al === null){
        errorGeneral("Sistema Intranet",'Seleccione un Almacén','danger');
        return false;
    }
    
    if (al.toString() !== 'nnn'){
        if (al.toString().substr(0,3) === "nnn"){
            errorGeneral("Sistema Intranet",'Si va a seleccionar varios almacenes no puede tener seleccionada la opción "TODOS"','danger');
            return false;
        }
    }
    
    var url1 = $('#txt_urlGe').val();
    var datos = "tp="+tipo;
    datos = datos + "&a_l="+al;
    datos = datos + "&f_i="+fi;
    datos = datos + "&f_f="+ff;
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/facturacion/co_101/retDocuError',
        data: datos,
        success : function (returnData) {
            
                removedataLoading();
                
                $('#detalleDocumentos').html('');                        
                $('#totales').html('');

                var row = '<table id="T_docElectronicos" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Almacén</th>';
                row = row+'<th>Estados</th>';
                row = row+'<th>Cantidad</th>';
                row = row+'<th>Acción</th>';
                row = row+'</thead>';
                row = row+'<tbody>';
                    
                if (returnData.trim().substr(0,2) === "OK"){
                    var dt = returnData.trim().substr(3);
                    var td = dt.split('/*/');
                    var data_1 = eval(td[0]);
                    var data_2 = eval(td[1]);
                    
                    if (data_1){ 
                        var i = 0;
                        var num_docs = 0,base_0 = 0,base_12 = 0,iva_12 = 0;
                        
                        $.each(data_1,function(e,dato){
                            //
                            num_docs+= dato.cantidad;
                            base_0+= parseFloat(dato.base_0);
                            base_12+= parseFloat(dato.base_12);
                            iva_12+= parseFloat(dato.val_iva);
                            
                            i++;
                            row = row+'<tr id="id_'+i+'" class="'+dato.clase+'">';
                            if(dato.tipo_cnx.trim() === 'S'){
                                row = row+'<td class="centro">'+dato.de_bodega+' (SQL)</td>';
                            } else {
                                row = row+'<td class="centro">'+dato.de_bodega+'</td>';
                            }
                            row = row+'<td>'+dato.estado+'</td>';
                            row = row+'<td class="centro">'+dato.cantidad+'</td>';
                            row = row+'<td class="centro"><a href="javascript:void(0)"  id="a2"  onclick="getDocuErrorDetalle('+"'"+fi+"'"+','+"'"+ff+"'"+','+"'"+tipo+"'"+','+"'"+dato.co_bodega+"'"+','+"'"+dato.id_ip_address+"'"+','+"'"+dato.estado+"'"+','+"'"+dato.tipo_cnx+"'"+','+"'"+dato.cantidad+"'"+','+"'"+i+"'"+')">Detalle</a></td>';
                            row = row+'</tr>';
                        });
                        
                        var tab = '';
                        tab = tab+'<table class="table table-bordered" style="background:#FFF;">';
                        tab = tab+'<thead>';
                        if (tipo === 'F' || tipo === 'V'){
                            tab = tab+'<th>N° de Comprobantes Emitidos</th>';
                            tab = tab+'<th>BASE 0%</th>';
                            tab = tab+'<th>BASE 12%</th>';
                            tab = tab+'<th>IVA 12%</th>';
                            tab = tab+'</thead>';
                            tab = tab+'<tbody>';
                            tab = tab+'<tr>';
                            tab = tab+'<td class="centro">'+formato_numero(roundTo(num_docs),0,".",",")+'</td>';
                            tab = tab+'<td class="centro">'+formato_numero(roundTo(base_0),2,".",",")+'</td>';
                            tab = tab+'<td class="centro">'+formato_numero(roundTo(base_12),2,".",",")+'</td>';
                            tab = tab+'<td class="centro">'+formato_numero(roundTo(iva_12),2,".",",")+'</td>';
                            tab = tab+'</tr>';
                        } else if(tipo === 'E'){
                            tab = tab+'<th>N° de Comprobantes Emitidos</th>';
                            tab = tab+'<th>Total Retenido</th>';
                            tab = tab+'</thead>';
                            tab = tab+'<tbody>';
                            tab = tab+'<tr>';
                            tab = tab+'<td class="centro">'+formato_numero(roundTo(num_docs),0,".",",")+'</td>';
                            tab = tab+'<td class="centro">'+formato_numero(roundTo(base_0),2,".",",")+'</td>';
                            tab = tab+'</tr>';
                        } else if (tipo === 'G'){
                            tab = tab+'<th>N° de Comprobantes Emitidos</th>';
                            tab = tab+'</thead>';
                            tab = tab+'<tbody>';
                            tab = tab+'<tr>';
                            tab = tab+'<td class="centro">'+formato_numero(roundTo(num_docs),0,".",",")+'</td>';
                            tab = tab+'</tr>';
                        }
                        tab = tab+'</tbody>';
                        tab = tab+'</table>';
                        $('#totales').append(tab);
                    }  
                    
                    if (data_2.toString().trim() !== ''){
                        $.each(data_2,function(e,da){
                            if (da.error){
                                errorGeneral("Sistema Intranet",da.error,'danger'); 
                            }
                        });
                    }
                    
                } else {
                    var error = eval(returnData.trim().substr(3));
                    if (error.toString().trim() !== ''){
                        $.each(error,function(e,da){
                            if (da.error){
                                errorGeneral("Sistema Intranet","SIN CONEXION EN ALMACÉN:<br>"+da.error,'danger'); 
                            } else {
                                errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                            }
                        });
                    } else {
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                    }
                }
                
                row = row+'</tbody>';
                row = row+'</table>';

                $('#detalleDocumentos').append(row);
                $('#T_docElectronicos').dataTable({
                    "order": [[ 1, "desc" ]]
                });
                
            }
    });

}

function getDocuErrorDetalle(fec_ini,fec_fin,tip,almacen,ip,estado,tip_con,cantidad,id){
    
    if (estado.trim() === 'AUTORIZADO'){
        if (!validaFecha(fec_ini,fec_fin)){
            return false;
        }
        
        if (parseInt(cantidad) > 2000){
            errorGeneral("Sistema Intranet",'La cantidad del detalle('+cantidad+') a mostrar es mayor a la permitida(2000)','danger'); 
            return false; 
        }

    }
    
    var url1 = $('#txt_urlGe').val();
    var datos = "tp="+tip;
    datos = datos + "&f_i="+fec_ini;
    datos = datos + "&f_f="+fec_fin;
    datos = datos + "&a_l="+almacen;
    datos = datos + "&i_p="+ip;
    datos = datos + "&est="+estado;
    datos = datos + "&t_c="+tip_con;
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/facturacion/co_101/retDocuErrorDetalle',
        data: datos,
        
        success : function (returnData) {
            
                removedataLoading();
                
                if (returnData.trim().substr(0,2) === "OK"){
                    
                    $('#detalleDocuElect').html('');
                    var row = '<table id="T_docElectDetall" class="table table-bordered table-hover">';
                    row = row+'<thead>';
                    row = row+'<th>Fecha Transacción</th>';
                    row = row+'<th>#Documento</th>';
                    row = row+'<th>Valor</th>';
                    row = row+'<th>Cliente</th>';
                    row = row+'<th>Fe Envio</th>';
                    row = row+'<th>De Envio</th>';
                    row = row+'<th>Fe Consulta</th>';
                    row = row+'<th>#Autorización</th>';
                    row = row+'<th>Observación</th>';
                    row = row+'<th>Acción</th>';
                    row = row+'</thead>';
                    row = row+'<tbody>';

                    var dato = returnData.trim().substr(3);
                    dato = eval('('+dato+')');
                    
                    if(!$.isEmptyObject(dato.error)) {
                         errorGeneral("Sistema Intranet",dato.error,'danger'); 
                         return false;
                    } else {
                        
                        $('#titleDoc').text("("+dato.data[0].tip_doc+") "+estado);
                        
                        $.each(dato.data,function(e,d){
                            
                            row = row+'<tr class="'+d.cod_cla+'">';
                            row = row+'<td class="centro">'+d.fec_tra+'</td>';
                            row = row+'<td class="centro">'+d.doc_doc+'</td>';
                            row = row+'<td class="derecha">'+d.val_tra+'</td>';
                            row = row+'<td class="centro">'+d.cod_cli+'</td>';
                            row = row+'<td class="centro">'+d.fec_env+'</td>';
                            row = row+'<td class="centro">'+d.det_env+'</td>';
                            row = row+'<td class="centro">'+d.fec_con+'</td>';
                            row = row+'<td class="centro">'+d.num_aut+'</td>';
                            row = row+'<td class="centro">'+d.det_con+'</td>';
                            var objectPas = d.cod_bdg+"/*/"+d.cod_caj+"/*/"+d.tip_doc+"/*/"+d.num_doc+"/*/"+d.det_xml+"/*/"+d.cod_est+"/*/"+d.doc_doc+"/*/"+d.cod_cli;
                            var objectPas2 = d.doc_doc+"/*/"+ip+"/*/"+tip;
                            if (d.cod_est.trim() === 'AUTORIZADO' || d.cod_est.trim() === 'ENCOLADO EN CIMA' || d.cod_est.trim() === 'XML NO GENERADO - COMUNIQUE A SISTEMAS'){
                                row = row+'<td class="centro"><a href="javascript:void(0)" onclick="viewXML('+"'"+encodeURIComponent(objectPas)+"'"+')">XML </a></td>';
                            } else if(d.cod_est.trim() === 'NOVEDAD CON XML - COMUNIQUE A SISTEMAS'){ 
                                row = row+'<td></td>'; 
                            } else {
                                //row = row+'<td class="centro"><a href="javascript:void(0)" onclick="viewXML('+"'"+encodeURIComponent(objectPas)+"'"+')">XML </a>| <a href="javascript:void(0)" onclick="updateDocElect('+"'"+encodeURIComponent(objectPas2)+"'"+')">Reprocesar</a></td>';
                                row = row+'<td class="centro"><a href="javascript:void(0)" onclick="viewXML('+"'"+encodeURIComponent(objectPas)+"'"+')">XML </a></td>';
                            }
//                          else if (){
//                              row = row+'<td class="centro"><a href="javascript:void(0)" onclick="updateDocElect('+"'"+objectPas2+"'"+')">Procesar</a></td>';
//                          } 
                            row = row+'</tr>';

                        });

                    }
                    
                    row = row+'</tbody>';
                    row = row+'</table>';

                    $('#detalleDocuElect').append(row);
                    $('#T_docElectDetall').dataTable({
                        aLengthMenu: [
                            [25, 50, 100, 200, -1],
                            [25, 50, 100, 200, "Todos"]
                        ]
                    });
                    
                    newModal('DetallDocts','95','75','form_DetallDocts');
                    
                } else {
                    if(returnData.trim() === ''){
                        $('#id_'+id).remove();
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    } else {  
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                    }
                }
                

            }
    });
    
}


function viewXML(dato){
    var datos = decodeURIComponent(dato);
    var arreglo =datos.split('/*/');
    newModal('DetallXML','65','85','form_DetallXML');
    $('#modalDetallXML').css('z-index','1042');
    $('#fadeDetallXML').css('z-index','1041');
    $('#titleDocXML').text(arreglo[6]+" ("+arreglo[5]+") ");
    
    var string = arreglo[4].trim().replace('<![CDATA[','');
        string = string.replace(']]>','');
    var xmlData = string
    var editor = ace.edit('editor');
        editor.setValue(xmlData); 
        editor.setTheme("ace/theme/solarized_light");
        editor.getSession().setMode("ace/mode/xml");
        //editor.setReadOnly(true);
        
    //    try {
    //        icount = 0;
    //        traverseXmlDoc($xml);
    //    } catch (err) {
    //        alert(err.message);
    //    }
    //    
    //    alert(icount)

}

function traverseXmlDoc(xmlDoc, initialMarginLeft) {
    var $xmlDocObj, $xmlDocObjChildren, $contentDiv;
    
    $contentDiv = $('#contentXML');
    if ($contentDiv.length === 0) {
        throw new Error('No se ha encontrado el objeto contentXML');
    }

    $xmlDocObj = $(xmlDoc);
    $xmlDocObjChildren = $(xmlDoc).children();

    $xmlDocObjChildren.each(function(index, Element){
        var $currentObject = $(this),
            childElementCount = Element.childElementCount,
            currentNodeType = $currentObject.prop('nodeType'),
            currentNodeName = $currentObject.prop('nodeName'),
            currentTagName = $currentObject.prop('tagName'),
            currentTagText = $currentObject.text();
            
            if (childElementCount > 0) {
                traverseXmlDoc($currentObject, initialMarginLeft);
            } else {
                $contentDiv.append($('<div>', {
                    'class': 'spa-3',
                    'html': '<label>'+currentTagName+'</label>: ' + currentTagText
                }));
                
                //detalleProduct
            }
    });
}

function getXmlProcess(){
    var editor = ace.edit('editor');
    alert(editor.getValue())
}

function  updateDocElect(data){
    alert(data)
    return false;
    var url1 = $('#txt_urlGe').val();
    var datos = "data="+data;
    datos = datos + "&x_l=N";
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/facturacion/co_101/modificaDoc',
        data: datos,
        success : function (returnData) {
            
                removedataLoading();
                
                if (returnData.trim().substr(0,2) === "OK"){
                    errorGeneral("Sistema Intranet",'Documento procesado con exito','success'); 
                } else {
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
                
            }
    });
    
}