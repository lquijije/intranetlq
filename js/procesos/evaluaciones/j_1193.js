
$(function() {
    $('#tab_met').dataTable();
    $( ".newPregu" ).button().click(function(){
        $('#co_acc').val('I');
        $('#co_met').val('');
        $('#cmb_est').attr('disabled',true);
        newModal('NewMet','40','30','form_NewMet');
    });
    
    $( ".savMet" ).button().click(function(){
        svMet();
    });
     
}); 

function svMet(){
    
    var urlGe = $('#txt_urlGe').val();
    var d_m = $('#txt_des_met').val();
    var e_m = $('#cmb_est').val();
    var c_m = $('#co_met').val();
    
    var a = d_m+'|'+'Nombre del M&eacute;todo';
    
    if(isEmptySis(a)){

        var datos = "_acc="+$('#co_acc').val();
        datos = datos + '&d_m='+d_m;
        datos = datos + '&e_m='+e_m;
        datos = datos + '&c_m='+c_m;

        dataLoading();

        $.ajax({
            type : 'POST',
            url : urlGe+"procesos/evaluaciones/co_1193/sv_tra", 
            data: datos,
            success : function (returnData) {                 
                removedataLoading();      
                errorGeneral("Sistema Intranet",returnData,'danger');   
                quitarmodalGeneral('NewMet','form_NewMet');
                window.location = window.location.href;
            }
        });

    }
}

function viewPre(c_m,acc){
    
    var url1 = $('#txt_urlGe').val();
    var datos = "c_m="+c_m;
    $('#co_met').val('');
    
    dataLoading();

    $.ajax({
        type : 'POST',
        url : url1+'procesos/evaluaciones/co_1193/retMet',
        data: datos,
        success : function (returnData) {
            removedataLoading();
            var datos = eval('('+returnData+')'); 
            if(!$.isEmptyObject(datos.data)) {

                $.each(datos.data,function(e,dt){
                    $('#co_met').val(c_m);
                    $('#txt_des_met').val(dt.ds_met);
                    $('#cmb_est').val(dt.es_met);
                });

                $('#cmb_est').attr('disabled',false);
                newModal('NewMet','40','30','form_NewMet');

                $('#co_acc').val(acc);

            } else {
                errorGeneral("Sistema Intranet",datos.err,'danger');
            }
        }

    });
    
    
}