
$(function() {
    $('#tab_pregu').dataTable();
    $( ".newPregu" ).button().click(function(){
        $('#co_acc').val('I');
        $('#co_pre').val('');
        $('#cmb_est').attr('disabled',true);
        newModal('NewPre','40','50','form_NewPre');
    });
    
    $( ".savPre" ).button().click(function(){
        svPre();
    });
     
    GoTree();
}); 

function GoTree(){
    
    var url1 = $('#txt_urlGe').val();
    $("#jstree").jstree({
        'core' : {
            'data' : {
                "url" : url1+"procesos/evaluaciones/co_1192/retTrev",
                "dataType" : "json"
            }
        },
        "checkbox" : {
            "three_state" : false
        },
        "plugins" : [ "checkbox" ]
    }).bind("loaded.jstree", function (event, data) {
            $(this).jstree("open_all");
        })   ;      
    
}

function svPre(){
    
    var urlGe = $('#txt_urlGe').val();
    var d_p = $('#txt_des_pre').val();
    var c_m = $('#cmb_met').val();
    var e_p = $('#cmb_est').val();
    var c_p = $('#co_pre').val();
    
    var a = d_p+'|'+'Descripción de la Pregunta'+':';
    a = a + c_m+'|'+'M&eacutetodo';
    
    if(isEmptySis(a)){

        var datos = "_acc="+$('#co_acc').val();
        datos = datos + '&d_p='+d_p;
        datos = datos + '&c_m='+c_m;
        datos = datos + '&e_p='+e_p;
        datos = datos + '&c_p='+c_p;

        dataLoading();

        $.ajax({
            type : 'POST',
            url : urlGe+"procesos/evaluaciones/co_1192/sv_tra", 
            data: datos,
            success : function (returnData) {                 
                removedataLoading();      
                errorGeneral("Sistema Intranet",returnData,'success');   
                quitarmodalGeneral('NewPre','form_NewPre');
                //window.location = window.location.href;
            }
        });

    }
}

function viewPre(c_p,acc){
    
    var url1 = $('#txt_urlGe').val();
    var datos = "c_p="+c_p;
    $('#co_pre').val('');
    
    dataLoading();

    $.ajax({
        type : 'POST',
        url : url1+'procesos/evaluaciones/co_1192/retPre',
        data: datos,
        success : function (returnData) {
            removedataLoading();
            var datos = eval('('+returnData+')'); 
            if(!$.isEmptyObject(datos.data)) {

                $.each(datos.data,function(e,dt){
                    $('#co_pre').val(c_p);
                    $('#txt_des_pre').val(dt.ds_pre);
                    $('#cmb_met').val(dt.co_met);
                    $('#cmb_est').val(dt.es_pre);
                });

                $('#cmb_est').attr('disabled',false);
                newModal('NewPre','40','50','form_NewPre');

                $('#co_acc').val(acc);

            } else {
                errorGeneral("Sistema Intranet",datos.err,'danger');
            }
        }

    });
    
    
}