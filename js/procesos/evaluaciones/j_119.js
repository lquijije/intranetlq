
$(function() {
    
    $("#organisation").orgChart({
        container: $("#main"), 
        interactive: true, 
        fade: true, 
        speed: 'slow',
        nodeClicked : function(node){
            //console.log(node)
        }
//        nodeText:alert(this.id)
//        showLevels:2,
//        copyData: true
    });
    
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            })
        },
        timepicker:false
    });
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            })
        },
        timepicker:false
    });
    
    $('#table_tot_eva').dataTable();
    
    $( ".new_eva" ).button().click(function(){
        newModal('EvaCre','65','80','form_EvaCre');
    });
    
}); 
