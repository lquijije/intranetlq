
$(function() {
    $( ".saveEvaPreg " ).button().click(function(){
        $_savPregs();
    });
    
    $('#tab_Resp input').on('change', function() {
        var arr = $(this).context.id.split('_');
        if ($('#option-'+arr[1]).length > 0){
            $('#option-'+arr[1]).removeClass();
            $('#option-'+arr[1]).addClass("success");
        }
    });
}); 

function evaDes(a,b,c){
    
    if (c.trim() === 'A'){
        
        var j = '<h4>Desea empezar <span style="font-weight: bold;">'+b.trim()+'</span>, caso contrario presione Cancelar.</h4>'; 
        j = j + '<div class="spa-12" style="margin-top:10px;">';
        j = j + '<button type="button" class="btn btn-danger pull-right" onclick="quitarmodalGeneral('+"'Eva',''"+');" style="margin-left:10px;"><i class="fa fa-times"></i> Cancelar</button>'; 
        j = j + '<button type="button" class="btn btn-success pull-right" onclick="_e_p('+a+','+"'"+c+"'"+');"><i class="fa fa-save"></i> Aceptar</button></div>';
        $('#pasEva').html(j);
        newModal('Eva','40','30','');
        
    } else {
        _e_p(a,c);
    }
    
}

function _e_p(a,b){
    var url = $('#txt_urlGe').val();
    var frm = $("<form>");
    frm.attr({'action':url+'procesos/evaluaciones/co_1191/takEva', 'method': 'post'});
    frm.append("<input type='hidden' name='co_ev' value='"+a+"'>");
    frm.append("<input type='hidden' name='es_ev' value='"+b+"'>");
    frm.appendTo("body");
    frm.submit();
}

function $_savPregs(){
    
    var c_e = $('#co_eva_preg').val();
    var count = $('#tab_preg_asig tr').length;
    var urlGe = $('#txt_urlGe').val();
    var arr_preg = [];
    var preg_resp = [];
    
    if (count > 0){
        $('#tab_preg_asig tr').each(function (){
            if ($(this).context.id.trim().length > 0){
                arr_preg.push(parseInt($(this).context.id.trim().replace(/[option-]/g,"")));
            }
        });
    }
    
    $.each(arr_preg,function(e,dt){
        var item = {};
        item["p"] = dt;            
        item["r"] = $('input:radio[name=ck_'+dt+']').is(':checked') ?$('input:radio[name=ck_'+dt+']').filter(":checked").val():0;
        preg_resp.push(item);
    });
    
    dataLoading();

    var datos = 'c_e='+c_e;
    datos = datos +'&d_p='+JSON.stringify(preg_resp);

    $.ajax({
        type : 'POST',
        url : urlGe+"procesos/evaluaciones/co_1191/sv_tra", 
        data: datos,
        success : function (returnData) {

            removedataLoading();      
            errorGeneral("Sistema Intranet",returnData,'success'); 

        }
    });
    
}

function _c_e(a){
    var url1 = $('#txt_urlGe').val();
    var datos = "c_e="+a;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/evaluaciones/co_1191/ckEva',
        data: datos,
        success : function (returnData) {
            removedataLoading();
            var datos = eval('('+returnData+')'); 
            if(!$.isEmptyObject(datos.data)) {
                $.each(datos.data,function(e,dt){
                    if ($('#opt_'+dt.c_p+'_'+dt.c_r).length > 0){
                        $('#option-'+dt.c_p).addClass("success");
                        $('#opt_'+dt.c_p+'_'+dt.c_r).prop("checked", true);
                    }
                });
            } 
        }
    });
}