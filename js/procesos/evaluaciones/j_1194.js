var i_count = 0;

$(function() {
    
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function(ct){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            });
        },
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            });
        },
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('#tab_pregu').dataTable();
    
    $('#tb_pr_as').dataTable( {
        "paging":   false,
        "searching": false,
        "info":     false,
        "columnDefs": [
            { "width": "20%", "targets": 0 }
        ],
        "createdRow" : function( row, data, index ) {
            if( data.hasOwnProperty("id") ) {
                row.id = "option-" + data.id;
            }       
        }
    } );
    
    $( ".newPregu" ).button().click(function(){
        $('#co_acc').val('I');
        $('#co_eva').val('');
        $('#cmb_est').attr('disabled',true);
        newModal('NewEva','60','70','form_NewEva');
    });
    
    $( ".addPregu " ).button().click(function(){
        addPregu();
    });
//    
    $( ".seaPregu" ).button().click(function(){
        searchPregu();
    });
    
    $( ".savEva" ).button().click(function(){
        svEva();
    });
    GoTree();
}); 

function GoTree(){
    
    var url1 = $('#txt_urlGe').val();
    $("#jstree").jstree({
        'core' : {
            'data' : {
                "url" : url1+"procesos/evaluaciones/co_1194/retTrev",
                "dataType" : "json"
            }
        },
        "checkbox" : {
            "three_state" : false
        }//,"plugins" : [ "checkbox" ]
    }).bind("loaded.jstree", function (event, data) {
        $(this).jstree("open_all");
    });      
    
}

function resetEval(){
    CKEDITOR.instances['txt_cont_mail'].setData('');
    quitarmodalGeneral('NewEva','form_NewEva');
}

function viewEva(c_e,acc){
    
    var url1 = $('#txt_urlGe').val();
    var datos = "c_e="+c_e;
    $('#co_eva').val('');
    
    var table = $('#tb_pr_as').DataTable();
    table.clear().draw();
                    
    dataLoading();

    $.ajax({
        type : 'POST',
        url : url1+'procesos/evaluaciones/co_1194/retEva',
        data: datos,
        success : function (returnData) {
            removedataLoading();
            var datos = eval('('+returnData+')'); 
            if(!$.isEmptyObject(datos.d_ev)) {

                $.each(datos.d_ev,function(e,dt){
                    $('#co_eva').val(c_e);
                    $('#txt_eva_tit_1').val(dt.e_t1);
                    $('#txt_eva_tit_2').val(dt.e_t2);
                    $('#txt_eva_des').val(dt.e_de);
                    $('#txt_fec_ini').val(dt.e_fi);
                    $('#txt_fec_fin').val(dt.e_ff);
                    $('#cmb_per').val(dt.e_pe);
                    $('#cmb_est').val(dt.e_es);
                });
                
                $.each(datos.d_pr,function(e,dp){
                    var table = $('#tb_pr_as').DataTable();
                    var data = [dp.p_dm,'<p>'+dp.p_cp+'</p>',dp.p_dp,'<center><button type="button" onclick="removeOption('+dp.p_cp+')" class="btn btn-circle btn-danger add_option"><i class="text-blanco fa fa-minus"></i></button><center>'];
                    data.id = dp.p_cp;
                    table.row.add(data).draw();
                });

                $('#cmb_est').attr('disabled',false);
                newModal('NewEva','60','70','form_NewEva');

                $('#co_acc').val(acc);

            } else {
                errorGeneral("Sistema Intranet",datos.err,'danger');
            }
        }

    });
    
}

function svEva(){
    
    var urlGe = $('#txt_urlGe').val();
    var et_1 = $('#txt_eva_tit_1').val();
    var et_2 = $('#txt_eva_tit_2').val();
    var e_d = $('#txt_eva_des').val();
    var f_i = $('#txt_fec_ini').val();
    var f_f = $('#txt_fec_fin').val();
    var e_p = $('#cmb_per').val();
    var e_e = $('#cmb_est').val();
    var e_c = $('#co_eva').val();
    
    var a = et_1+'|'+'T&iacute;tulo'+':';
    a = a + et_2+'|'+'Sub T&iacute;tulo'+':';
    a = a + e_d+'|'+'Descripci&oacute;n'+':';
    a = a + f_i+'|'+'Fecha Inicial'+':';
    a = a + f_f+'|'+'Fecha Final'+':';
    a = a + e_p+'|'+'Periodo';
    
    var arr_preg = [];
    
    var count = $('#tab_preg_asig tr').length;
    
    if (count > 0){
        $('#tab_preg_asig tr').each(function (){
            if ($(this).context.id.trim().length > 0){
                arr_preg.push(parseInt($(this).context.id.trim().replace(/[option-]/g,"")));
            }
        });
    }
    
    if(isEmptySis(a)){
        
        if(!$.isEmptyObject(arr_preg)){
            
            var datos = "_acc="+$('#co_acc').val();
            datos = datos + '&et_1='+et_1;
            datos = datos + '&et_2='+et_2;
            datos = datos + '&e_d='+e_d;
            datos = datos + '&f_i='+f_i;
            datos = datos + '&f_f='+f_f;
            datos = datos + '&e_p='+e_p;
            datos = datos + '&e_e='+e_e;
            datos = datos + '&e_c='+e_c;
            datos = datos + '&a_p='+arr_preg;

            dataLoading();
    
            $.ajax({
                type : 'POST',
                url : urlGe+"procesos/evaluaciones/co_1194/sv_tra", 
                data: datos,
                success : function (returnData) {                 
                    removedataLoading();      
                    errorGeneral("Sistema Intranet",returnData,'danger');   
                    quitarmodalGeneral('NewEva','form_NewEva');
                    var table = $('#tb_pr_as').DataTable();
                    table.clear().draw();
                }
            });

        } else {
            
        }
    }
}

function searchPregu(){
    
    var i_m = $('#cmb_met').val();
    
    if (i_m.trim() != 'NNN'){
        
        var url1 = $('#txt_urlGe').val();
        var datos = "i_m="+i_m;

        dataLoading();

        $.ajax({
            type : 'POST',
            url : url1+'procesos/evaluaciones/co_1194/retPre',
            data: datos,
            success : function (returnData) {
                removedataLoading();
                var datos = eval('('+returnData+')'); 
                if(!$.isEmptyObject(datos.data)) {
                    var bo = '';
                    $.each(datos.data,function(e,dt){
                        bo = bo +'<tr>';
                        bo = bo +'<td>'+dt.co_pre+'</td>';
                        bo = bo +'<td>'+dt.ds_pre+'</td>';
                        bo = bo +'<td><center><input type="checkbox" id="ck_'+dt.co_pre+'" name="option_pre[]" value="'+dt.co_pre+'" data-label="'+dt.ds_pre.replace(/['"]/g," ")+'" ></center></td>';
                        bo = bo +'</tr>';
                    });
                    
                    $('#tab_preguntas').html(bo);
                    
                    $('#cmb_est').attr('disabled',false);
                    newModal('ViewPre','40','50','');
                    $('#modalViewPre').css('z-index','1042');
                    $('#fadeViewPre').css('z-index','1041');

                } else {
                    errorGeneral("Sistema Intranet",datos.err,'danger');
                }
            }

        });
        
    } else {
        errorGeneral("Sistema Intranet","Debe seleccionar un m&eacute;todo",'danger');
    }
    
}

function addPregu(){
    
    var tab = '';
    var err = 0;
    var tx_err = '';
    
    var select = $("input[name='option_pre[]']:checked");
    
    if($(select).length <= 0){
        errorGeneral("Sistema Intranet",'Seleccione una o m&aacute;s pregunta para poder añadirla.','danger');
        return false;
    }   
    
    var arr = [];
    
    $('#tab_preg_asig tr').each(function (){
        arr.push($(this).context.id.trim());
    });
    
    $(select).each( function () {
        var idSel = $(this).val();
        var desSel = $(this).attr('data-label');

        if(!!~$.inArray("option-"+idSel,arr)){
            tx_err = tx_err +'<br>#'+idSel;
            err++;
        } else {
            var table = $('#tb_pr_as').DataTable();
            var data = [$('#cmb_met :selected').text(),'<p>'+idSel+'</p>',desSel,'<center><button type="button" onclick="removeOption('+idSel+')" class="btn btn-circle btn-danger add_option"><i class="text-blanco fa fa-minus"></i></button><center>'];
            data.id = idSel;
            table.row.add(data).draw();
        }

    });
    
    if(err >= 1){
        errorGeneral("Sistema Intranet",'Las siguientes preguntas ya estan ingresadas:'+tx_err,'danger');
    }
    
}

function removeOption(id){
    
    if(confirm('Esta seguro de que desea eliminar este registro ?')){ 
        $('#tb_pr_as').dataTable().fnDestroy();
        $('#option-'+id).remove();
        $('#tb_pr_as').dataTable({
            "paging":   false,
            "searching": false,
            "info":     false,
            "columnDefs": [
                { "width": "20%", "targets": 0 }
            ],
            "createdRow" : function( row, data, index ) {
                if( data.hasOwnProperty("id") ) {
                    row.id = "option-" + data.id;
                }       
            }
        });
    }
    
}
