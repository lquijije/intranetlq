
var _cont = 0;

function procTurnos() {

    if (va_sess()) {

        var a = 0;
        var ce_cos = [];
        var url1 = $('#txt_urlGe').val();
        var cmb = $('#cmb_fec').val();
        cmb = cmb.split('|');
        var fe_in = cmb[0];
        var fe_fi = cmb[1];
        var select = $("input[name='option_tur[]']:checked");
        var m_l = $("#ck_mail").is(':checked') ? 1 : 0;

        if ($(select).length <= 0) {
            errorGeneral("Sistema Intranet", 'Seleccione una o m&aacute;s centros de costo.', 'danger');
            return false;
        }

        $(select).each(function () {
            ce_cos.push($(this).val());
        });

        var r = confirm("Desea procesar los turnos de este centro de costo ?");

        if (r == true) {

            dataLoading();

            var datos = 'c_c=' + JSON.stringify(ce_cos) + '&f_i=' + fe_in + '&f_f=' + fe_fi + '&m_l=' + m_l;

            $.ajax({
                type: 'POST',
                url: url1 + 'procesos/nomina/co_126/procTu',
                data: datos,
                success: function (returnData) {
                    removedataLoading();
                    var datta = eval('(' + returnData + ')');

                    if (datta.data) {
                        if (datta.data.co_err == 0) {
                            errorGeneral("Sistema Intranet", "Turnos Procesados Con Exito", 'success');
                            $.download('/ipycca/procesos/nomina/co_126', "cmb_fec=" + $('#cmb_fec').val());
                        } else if (datta.data.co_err == 1) {
                            errorGeneral("Sistema Intranet", datta.data.tx_err, 'danger');
                            $.download('/ipycca/procesos/nomina/co_126', "cmb_fec=" + $('#cmb_fec').val());
                        } else {
                            errorGeneral("Sistema Intranet", datta.data, 'danger');
                        }
                    } else {
                        errorGeneral("Sistema Intranet", returnData, 'danger');
                        return false;
                    }
                }
            });
        }
    } else {
        redirectIntranet();
    }
}

function viewPersonTurn(c_bdg) {

    var url1 = $('#txt_urlGe').val();
    var cmb = $('#cmb_fec').val();
    cmb = cmb.split('|');
    var fe_in = cmb[0];
    var fe_fi = cmb[1];
    var a = 0;
    var b = 0;
    var row = '';
    $('.fg').html('');
    var turnos = [];

    dataLoading();

    $.ajax({
        type: 'POST',
        async: false,
        url: url1 + "procesos/nomina/co_126/getTu",
        success: function (returnData) {
            turnos = returnData;
        }
    });

    var jturnos = eval(eval('(' + turnos + ')'));

    if (va_sess()) {

        dataLoading();
        _cont = 0;

        $('#id__turn').html($("#" + c_bdg).find('.nom_alm').html());

        var datos = 'c_c=' + c_bdg + '&f_i=' + fe_in + '&f_f=' + fe_fi;

        $.ajax({
            type: 'POST',
            url: url1 + 'procesos/nomina/co_126/getEmplBg',
            data: datos,
            success: function (returnData) {
                removedataLoading();
                var datta = eval('(' + returnData + ')');

                if (!$.isEmptyObject(datta)) {

                    var i = 0;
                    $('#tab_turns').html('');

                    $.each(datta, function (e, da) {
                        console.log(da)
                        i++;
                        var activ = '';
                        da.co_turn > 0 && da.co_sali == 0 ? a++ : 0;
                        da.co_turn == 0 ? activ = 'class="warning"' : '';
                        da.co_turn > 0 ? activ = 'class="success"' : '';
                        da.co_sali > 0 ? activ = 'class="danger"' : '';
                        da.em_proc == 0 && da.co_sali == 0 ? b++ : 0;

                        row = row + '<tr id="' + da.co_empl + '" ' + activ + '>';
                        row = row + '<td>' + i + '</td>';
                        row = row + '<td>' + da.co_empl + '</td>';
                        row = row + '<td style="width: 15%;">' + da.ap_empl + ' ' + da.no_empl + '</td>';
                        row = row + '<td style="width: 10%;">' + da.no_carg + '</td>';
                        row = row + '<td style="width: 10%;" class="centro">';

                        if (da.co_turn == 0) {
                            row = row + '<select id="cmb_' + da.co_empl + '" class="form-control cmb">';
                            $.each(jturnos, function (e, de) {
                                row = row + de.ds_turn;
                                row = row + '<option  value="' + de.co_turn + '">' + de.ds_turn + '</option>';
                            });
                            row = row + '</select>';
                        } else if (da.em_proc == 1) {
                            row = row + '<div>Procesado</div>';
                            row = row + da.no_turn;
                        } else {
                            row = row + '<select id="cmb_' + da.co_empl + '" class="form-control cmb">';
                            $.each(jturnos, function (e, de) {
                                var selec = '';
                                row = row + de.ds_turn;
                                if (da.co_turn == de.co_turn) {
                                    selec = 'selected';
                                }
                                row = row + '<option  value="' + de.co_turn + '" ' + selec + '>' + de.ds_turn + '</option>';
                            });
                            row = row + '</select>';
                        }

//                        if (da.em_proc == 1){
//                        } else if (da.co_sali == 0){
//                            
//                        } else {
//                            row = row+'<button type="button" class="btn btn-circle btn-danger" onclick="deleTurnos('+c_bdg+','+da.co_empl+');" style="margin-right: 5px;">';
//                            row = row+'<i class="text-blanco text-size-1 fa fa-minus"></i>';
//                            row = row+'</button>';
//                            row = row+'Eliminar Colaborador';
//                            row = row+'<h6 style="margin: 0px 0px 0px 30px;">(Salida o Cambio de Almac&eacute;n)</h6>';
//                        }
                        row = row + '</td>';
                        row = row + '</tr>';

                    });
                    _cont = a;
                    $('#count_colaModal').html(i);
                    $('#count_asigModal').html(a);
                    $('#tab_turns').html(row);
                    $('.fg').html('<button type="button" class="btn btn-success" onclick="saveTurnos(' + c_bdg + ');"><i class="fa fa-save"></i> Grabar</button>');
                    newModal('DetTurn', '60', '80', '');
                } else {
                    errorGeneral("Sistema Intranet", "Sin datos en busqueda realiza.", 'danger');
                }
            }
        });
    } else {
        redirectIntranet();
    }


}

function closeMoTurno() {
    quitarmodalGeneral('DetTurn', '');
    $('#count_colaModal').html(0);
    $('#count_asigModal').html(0);
    $('#tab_turns').html('');
}

function deleTurnos(co_cco, co_en) {

    if (va_sess()) {

        var url1 = $('#txt_urlGe').val();
        var cmb = $('#cmb_fec').val();
        cmb = cmb.split('|');
        var fe_in = cmb[0];
        var fe_fi = cmb[1];

        dataLoading();

        var datos = 'c_c=' + co_cco + '&f_i=' + fe_in + '&f_f=' + fe_fi + '&c_e=' + co_en;

        $.ajax({
            type: 'POST',
            url: url1 + 'procesos/nomina/co_126/deleTu',
            data: datos,
            success: function (returnData) {
                removedataLoading();
                var datta = eval('(' + returnData + ')');

                if (datta.data) {
                    if (datta.data.co_err === 0) {
                        $('#' + co_en).remove();
                        _cont--;
                        $('#count_colaModal').html(_cont);
                        errorGeneral("Sistema Intranet", "Registro Eliminado Con Exito", 'success');
                    } else {
                        errorGeneral("Sistema Intranet", datta.data.tx_err, 'danger');
                    }
                } else {
                    errorGeneral("Sistema Intranet", returnData, 'danger');
                    return false;
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

function saveTurnos(co_cco) {

    if (va_sess()) {

        var json_ = [];
        var url1 = $('#txt_urlGe').val();
        var cmb = $('#cmb_fec').val();
        var cmb1 = cmb.split('|');
        var fe_in = cmb1[0];
        var fe_fi = cmb1[1];

        $('#tab_turns tr').each(function () {
            if ($('#cmb_' + parseInt($(this).context.id.trim())).length > 0) {
                var item = {};
                item["c_u"] = parseInt($(this).context.id.trim());
                item["c_t"] = parseInt($('#cmb_' + parseInt($(this).context.id.trim())).val());
                json_.push(item);
            }
        });

        if (!$.isEmptyObject(json_)) {
            dataLoading();
            var datos = 'c_c=' + co_cco + '&f_i=' + fe_in + '&f_f=' + fe_fi;
            datos = datos + '&d_d=' + JSON.stringify(json_);

            $.ajax({
                type: 'POST',
                url: url1 + 'procesos/nomina/co_126/saveTu',
                data: datos,
                success: function (returnData) {
                    removedataLoading();
                    var datta = eval('(' + returnData + ')');

                    if (datta.data) {
                        if (datta.data.co_err === 0) {
                            closeMoTurno('DetTurn', 'form_DetTurn');
                            errorGeneral("Sistema Intranet", "Registro Grabado Con Exito", 'success');
                            $.download('/ipycca/procesos/nomina/co_126', "cmb_fec=" + cmb);
                        } else if (datta.data.co_err === 1) {
                            errorGeneral("Sistema Intranet", datta.data.tx_err, 'danger');
                        } else {
                            errorGeneral("Sistema Intranet", datta.data, 'danger');
                        }
                    } else {
                        errorGeneral("Sistema Intranet", returnData, 'danger');
                        return false;
                    }
                }
            });

        } else {
            errorGeneral("Sistema Intranet", "Todos los colaboradores se encuentran procesados.", 'danger');
            return false;
        }

    } else {
        redirectIntranet();
    }

}

$(function () {

    $(".ref_pag").click(function () {
        $.download('/ipycca/procesos/nomina/co_126', "cmb_fec=" + $('#cmb_fec').val());
    });

    $(".pro_t").click(function () {
        procTurnos();
    });

    $(".tu_print").click(function () {
        var url1 = $('#txt_urlGe').val();
        var cmb = $('#cmb_fec').val();
        cmb = cmb.split('|');
        var fe_in = cmb[0];
        var fe_fi = cmb[1];
        window.open(url1 + 'procesos/nomina/co_126/ge_pdf/' + fe_in + '/' + fe_fi + '/Turnos ');
    });

});
