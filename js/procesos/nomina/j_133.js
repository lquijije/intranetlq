function getCol() {

    $('#c_empl').val('');
    $('#c_empr').val('');

    if (va_sess()) {
        resetControl();
        var c_e = $('#txt_ce').val();
        var pt = $('#ed_let').val();
        var url1 = $('#txt_urlGe').val();

        if (c_e.trim() === '') {
            errorGeneral(conf_hd.tx.t_1, 'Debe ingresar un c&oacute;digo.', 'danger');
            return false;
        }

        $('#c_empl').val(c_e);
        var datos = "c_e=" + c_e;
        dataLoading();
        $.ajax({
            type: 'POST',
            url: url1 + 'procesos/nomina/co_133/geCo',
            data: datos,
            success: function (returnData) {
                removedataLoading();
                var data = eval('(' + returnData + ')');
                if (data.res_1) {

                    var cargo = '';

                    $('.no_emp').html(data.res_1.no_emp);
                    $('.no_col').html(data.res_1.no_col + " " + data.res_1.ap_col);
                    $('.ce_col').html(data.res_1.ce_col);
                    $('.no_car').html(data.res_1.no_car);
                    $('.no_cco').html(data.res_1.co_cco + " " + data.res_1.no_cco);
                    $('#c_empr').val(data.res_1.co_emp);

                    if (data.res_1.co_emp == 1) {
                        $('._tEmpr').html('PLASTICOS INDUSTRIALES ');
                        $('._taIMG').html('<img src="' + url1 + '/img/credencial_1.jpg" width="308">');
                        $('._taN').addClass('font_emp1');
                        $('._taE').addClass('font_emp1');
                        $('._taN').css('color', '#000');
                        $('._taE').css('color', '#FFF');

                    } else if (data.res_1.co_emp == 27) {
                        cargo = !$.isEmptyObject(data.res_2) ? data.res_2.ta_car : '';
                        $('._tEmpr').html('PYCCA S.A. ');
                        $('._taIMG').html('<img src="' + url1 + '/img/credencial_27.jpg" width="308">');
                        $('._taN').removeClass('font_emp1');
                        $('._taE').removeClass('font_emp1');
                        $('._taN').css('color', '#FFF');
                    }

                    if (parseInt(data.res_1.x) > 0) {
                        var img = $("<img />").attr('src', url1 + 'lib/loadImgNomina.php?ce=' + c_e).attr('height', data.res_1.y).attr('width', data.res_1.x).load(function () {
                            $("._taI").html(img);
                        });
                    }

                    if (data.res_2) {
                        $('#ed_nom').val(data.res_2.ta_nom);
                        //$('#ed_car').val(data.res_2.ta_car);
                        $('#ed_car').val(data.res_1.no_car);
                        $('#ed_ciu').val(data.res_2.ta_ciu);

                        $('._taN').html(data.res_2.ta_nom);
                        $('._taN').css('font-size', (parseInt(pt) + 4) + "pt !important;");
                        $('._taC').html(cargo);
                        $('._taE').html('COD: ' + c_e);
                        $('._taD').html(data.res_2.di_ciu + " " + data.res_2.te_ciu);
                    }

                    if (data.res_3) {
                        var se_log = '<ul>';
                        $.each(data.res_3, function (e, dt) {
                            se_log = se_log + "<li>" + dt.fe_emi + "</li>";
                        });
                        se_log = se_log + '</ul>';
                        $('#fe_log').html(se_log);
                    }

                } else {
                    if (data.res_1 === null) {
                        errorGeneral(conf_hd.tx.t_1, 'No hay datos en la busqueda realizada', 'danger');
                    } else {
                        errorGeneral(conf_hd.tx.t_1, returnData, 'danger');
                    }
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

function getRep() {
    if (va_sess()) {
        var f_i = $('#txt_fec_ini').val();
        var f_f = $('#txt_fec_fin').val();
        var c_d = $('#ck_descuento').is(':checked') ? 1 : 0;

        if ($.isEmptyObject(f_i) && $.isEmptyObject(f_f)) {
            errorGeneral(conf_hd.tx.t_1, "Debe de ingresar un rango de fechas.", 'danger');
            return false;
        }
        dataLoading();
        $.post('co_133/geRe',
                {f_i: f_i, f_f: f_f, c_d: c_d},
                function (c) {
                    c = eval('(' + c + ')');
                    if ($.isEmptyObject(c.err)) {
                        if (!$.isEmptyObject(c.dat)) {
                            $('#sumReport').html(c.sum);
                            $('#tabReport').html('<table id="detRep" class="table table-bordered table-striped table-mc-teal"></table>');
                            var table_ = $('#detRep').DataTable({
                                data: c.dat.data,
                                columns: c.dat.columns,
                                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                    if (aData.fl != 0) {
                                        $('td', nRow).css('background', 'rgb(188, 255, 198)');
                                    }
                                },
                            });
                        } else {
                            errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                        }
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();

                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}

function resetControl() {
    $('.no_emp').html('');
    $('.no_col').html('');
    $('.ce_col').html('');
    $('.no_car').html('');
    $('.no_cco').html('');
    $('._taI').html('');
    $('._taD').html('');
    $('._taN').html('');
    $('._taC').html('');
    $('._taE').html('');
    $('#ed_nom').val('');
    $('#ed_car').val('');
    $('#ed_let').val('11');
    $('#ed_ciu').val('GYE');
    $('#fe_log').html('');
    $('#enable_dol').prop('checked', false);
}

function savePrint() {
    if (va_sess()) {
        var url1 = $('#txt_urlGe').val();
        var c_co = $('#c_empl').val();
        var c_em = $('#c_empr').val();
        var n_em = $('#ed_nom').val();
        var c_ci = $('#ed_ciu').val();
        var n_ci = $("#ed_ciu option:selected").text();
        var t_le = $('#ed_let').val();
        var c_ca = $('#ed_car').val();

        var a = c_co + '|' + 'C&oacute;digo de Empleado' + ':';
        a = a + c_em + '|' + 'C&oacute;digo de Empresa' + ':';
        a = a + n_em + '|' + 'Nombre de Empleado' + ':';
        a = a + c_ci + '|' + 'Ciudad' + ':';
        a = a + t_le + '|' + 'Tama&ntilde;o Letra' + ':';
        a = a + c_ca + '|' + 'Cargo';

        if (isEmptySis(a)) {

            var item = {};
            item["c_co"] = c_co;
            item["c_em"] = c_em;
            item["n_em"] = n_em;
            item["c_ci"] = c_ci;
            item["n_ci"] = n_ci;
            item["t_le"] = t_le;
            item["c_ca"] = c_ca;
            if ($("#enable_dol").is(':checked')) {
                item["dol_cre"] = 1;
            } else {
                item["dol_cre"] = 0;
            }

            dataLoading();
            var datos = 'c_a=' + JSON.stringify(item);

            $.ajax({
                type: 'POST',
                url: url1 + 'procesos/nomina/co_133/svCr',
                data: datos,
                success: function (returnData) {
                    removedataLoading();
                    var datta = eval('(' + returnData + ')');
                    if (datta) {
                        if (datta.co_err == 0) {
                            resetControl();
                            errorGeneral(conf_hd.tx.t_1, "Registro Grabado Con Exito", 'success');
                            var ro = '<object height="1" width="1" data="' + url1 + 'procesos/nomina/co_133/geCr/' + c_co + '" type="application/pdf"></object>';
                            $('#_pdf').html(ro);
                        } else {
                            errorGeneral(conf_hd.tx.t_1, datta.tx_err, 'danger');
                        }
                    } else {
                        errorGeneral(conf_hd.tx.t_1, returnData, 'danger');
                        return false;
                    }
                }
            });
        }
    } else {
        redirectIntranet();
    }
}

$(function () {
    $("#ed_let").change(function () {
        var pt = $('#ed_let').val();
        $('._taN').attr('style', 'font-size:' + (parseInt(pt) + 4) + "pt !important;");
    });

    $("#ed_ciu").change(function () {
        if (va_sess()) {

            var c_c = $(this).val();
            var url1 = $('#txt_urlGe').val();
            var datos = "c_c=" + c_c;

            $.ajax({
                type: 'POST',
                url: url1 + 'procesos/nomina/co_133/geCi',
                data: datos,
                success: function (returnData) {
                    removedataLoading();
                    var data = eval('(' + returnData + ')');
                    if (data) {
                        $.each(data, function (e, dt) {
                            $('._taD').html(dt.di_ciu + " " + dt.te_ciu);
                        });
                    } else {
                        if (data != null) {
                            errorGeneral(conf_hd.tx.t_1, returnData, 'danger');
                        }
                    }
                }
            });
        } else {
            redirectIntranet();
        }
        $('._taD').html('');
    });

    $(".sea_r").click(function () {
        getRep();
    });

    $(".sea_e").click(function () {
        getCol();
    });

    $("#ed_nom").keypress(function () {
        var $this = $(this);
        window.setTimeout(function () {
            $("._taN").text($this.val());
        }, 0);
    });

    $("#ed_car").keypress(function () {
        var $this = $(this);
        window.setTimeout(function () {
            $("._taC").text($this.val());
        }, 0);
    });

    $('.leftPage').click(function (e) {
        e.preventDefault();
        $("#slide").animate({left: "-100%"}, "fast");
    });

    $('.rightPage').click(function (e) {
        e.preventDefault();
        $("#slide").animate({left: "0"}, "fast");
    });

    $('.p_cre').click(function (e) {
        savePrint();
    });

    $('#txt_fec_ini').datetimepicker({
        format: 'Y/m/d',
        lang: 'es',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txt_fec_fin').val() ? $('#txt_fec_fin').val() : false
            });
        },
        timepicker: false,
        closeOnDateSelect: true
    });
    $('#txt_fec_fin').datetimepicker({
        format: 'Y/m/d',
        lang: 'es',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#txt_fec_ini').val() ? $('#txt_fec_ini').val() : false
            });
        },
        timepicker: false,
        closeOnDateSelect: true
    });

});
