
var _cont = 0;

function procTurnos(){
    
    if(va_sess()){
        var a = 0;
        var url1 = $('#txt_urlGe').val();
        var co_cco = $('#co_cco').val();
        var fe_in = $('#fe_in').val();
        var fe_fi = $('#fe_fi').val();

        if($('.sin_asignar').length > 0){
            errorGeneral("Sistema Intranet","Faltan colaboradores por asignar.",'danger');
            return false;
        }
        
        if($('.sin_asignar').length > 0){
            errorGeneral("Sistema Intranet","Faltan colaboradores por asignar.",'danger');
            return false;
        }

//        if(parseInt($('#count_cola').html()) != parseInt($('#count_asig').html())){
//            errorGeneral("Sistema Intranet","Faltan colaboradores por asignar.",'danger');
//            return false;
//        }
//        
//        $('#tab_turns tr').each(function (){
//            if ($('#cmb_'+parseInt($(this).context.id.trim())).length > 0){
//                a++;
//            }
//        });
//        
//        if(a === 0){
//            errorGeneral("Sistema Intranet","Todos los colaboradores se encuentran procesados.",'danger');
//            return false;
//        }

        var r = confirm("Desea procesar los turnos de este centro de costo ?");

        if (r === true) {
            
            dataLoading();

            var datos = 'c_c='+co_cco+'&f_i='+fe_in+'&f_f='+fe_fi;

            $.ajax({
                type : 'POST',
                url : url1+'procesos/nomina/co_126_1/procTu',
                data: datos,
                success : function (returnData) {
                    removedataLoading();              
                    var datta = eval('('+returnData+')');

                    if (datta.data){
                        if (datta.data.co_err === 0){
                            errorGeneral("Sistema Intranet","Turnos Procesados Con Exito",'success');
                            $.download('/ipycca/procesos/nomina/co_126_1',"co_bdg="+$('#co_bdg').val()+"&co_cco="+$('#co_cco').val());
                        } else if (datta.data.co_err === 1){
                            errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                        } else {
                            errorGeneral("Sistema Intranet",datta.data,'danger');
                        }
                    } else {
                        errorGeneral("Sistema Intranet",returnData,'danger');
                        return false;
                    }
                }
            });
        }
    } else {
        redirectIntranet();
    }
}

function saveTurnos(){
    
    if(va_sess()){

        var json_ = [];
        var url1 = $('#txt_urlGe').val();
        var co_cco = $('#co_cco').val();
        var fe_in = $('#fe_in').val();
        var fe_fi = $('#fe_fi').val();

        if($('.danger').length > 0){
            errorGeneral("Sistema Intranet","Debe de Eliminar los Colaboradores",'danger');
            return false;
        }

        $('#tab_turns tr').each(function (){
            if ($('#cmb_'+parseInt($(this).context.id.trim())).length > 0){
                var item = {};
                item["c_u"] = parseInt($(this).context.id.trim());      
                item["c_t"] = parseInt($('#cmb_'+parseInt($(this).context.id.trim())).val());      
                json_.push(item);
            }
        });

        if(!$.isEmptyObject(json_)){
            dataLoading();
            var datos = 'c_c='+co_cco+'&f_i='+fe_in+'&f_f='+fe_fi;
            datos = datos + '&d_d='+JSON.stringify(json_);

            $.ajax({
                type : 'POST',
                url : url1+'procesos/nomina/co_126_1/saveTu',
                data: datos,
                success : function (returnData) {
                    removedataLoading();              
                    var datta = eval('('+returnData+')');

                    if (datta.data){
                        if (datta.data.co_err === 0){
                            closeMoTurno('DetTurn','form_DetTurn');
                            errorGeneral("Sistema Intranet","Registro Grabado Con Exito",'success');
                            $.download('/ipycca/procesos/nomina/co_126_1',"co_bdg="+$('#co_bdg').val()+"&co_cco="+$('#co_cco').val());
                        } else if (datta.data.co_err === 1){
                            errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                        } else {
                            errorGeneral("Sistema Intranet",datta.data,'danger');
                        }
                    } else {
                        errorGeneral("Sistema Intranet",returnData,'danger');
                        return false;
                    }
                }
            });

        } else {
            errorGeneral("Sistema Intranet","Todos los colaboradores se encuentran procesados.",'danger');
            return false;
        }
    
    } else {
        redirectIntranet();
    }
    
}

function deleTurnos(a){
    
    if(va_sess()){
        var url1 = $('#txt_urlGe').val();
        var co_cco = $('#co_cco').val();
        var fe_in = $('#fe_in').val();
        var fe_fi = $('#fe_fi').val();
        var co_en = a.parent().parent('tr')[0].id;

        dataLoading();
        var datos = 'c_c='+co_cco+'&f_i='+fe_in+'&f_f='+fe_fi+'&c_e='+co_en;

        $.ajax({
            type : 'POST',
            url : url1+'procesos/nomina/co_126_1/deleTu',
            data: datos,
            success : function (returnData) {
                removedataLoading();              
                var datta = eval('('+returnData+')');

                if (datta.data){
                    if (datta.data.co_err === 0){
                        $('#'+co_en).remove();
                        _cont--;
                        $("#count_cola").html(_cont);
                        errorGeneral("Sistema Intranet","Registro Eliminado Con Exito",'success');
                    } else {
                        errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                    } 
                } else {
                    errorGeneral("Sistema Intranet",returnData,'danger');
                    return false;
                }
            }
        });
    } else {
        redirectIntranet();
    }
}

function passEmpr(a,b,c){
    $('#co_bdg').val(a);
    $('#co_cco').val(b);
    document.getElementById("f_turnos").submit();
}
    
function countAsig(){
    var a = $('.success').length;
    $('#count_asig').html(a);
}

function viewPersonTurn(tipo,co_turn,data){

    var datos = $.parseJSON(data);
    if(datos){
        
        $('#id__turn').html(co_turn);
        
        var i = 0;
        var a = 0;
        var b = 0;
        var url1 = $('#txt_urlGe').val();
        var row = '';
        var turnos = [];
        
        dataLoading();
        
        $.ajax({
            type : 'POST',
            async: false,
            url : url1+"procesos/nomina/co_126_1/getTu", 
            success : function (returnData) { 
                turnos = returnData;        
            }
        });
        
        var jturnos = eval(eval('('+turnos+')'));
                
        if(!$.isEmptyObject(jturnos)) { 
            
            var row = '';
            
            $.each(datos,function(e,da){
                //console.log(da)
                i++;
                var activ = '';
                co_turn > 0 && da.co_sali == 0?a++:0;
                tipo == 'S' && co_turn > 0 ? activ = 'class="warning"':'';
                tipo == 'A' && co_turn > 0 ? activ = 'class="success"':'';
                da.co_sali > 0?activ = 'class="danger"':'';
                
                da.em_proc == 0 && da.co_sali == 0?b++:0;

                row = row+'<tr id="'+da.co_empl+'" '+activ+'>';
                row = row+'<td style="width: 5%;">'+i+'</td>';
                row = row+'<td style="width: 5%;">'+da.co_empl+'</td>';
                row = row+'<td>'+da.ap_empl+' '+da.no_empl+'</td>';
                row = row+'<td>'+da.no_carg+'</td>';
                row = row+'<td class="centro">';

                if (da.em_proc == 1){ 
                    row = row+'<div>Procesado</div>';
                    $.each(jturnos,function(e,de){
                        if(co_turn == de.co_turn){
                            row = row+de.ds_turn;
                        }
                    });
                } else if (da.co_sali == 0){
                    row = row+'<select id="cmb_'+da.co_empl+'" class="form-control cmb">';
                    $.each(jturnos,function(e,de){
                        var selec = '';
                        row = row+de.ds_turn;
                        if(co_turn == de.co_turn){
                            selec = 'selected';
                        }
                            row = row+'<option  value="'+de.co_turn+'" '+selec+'>'+de.ds_turn+'</option>';
                    });
                    row = row+'</select>';
                } else {
                    row = row+'<button type="button" class="btn btn-circle btn-danger eli_u" style="margin-right: 5px;">';
                    row = row+'<i class="text-blanco text-size-1 fa fa-minus"></i>';
                    row = row+'</button>';
                    row = row+'Eliminar Colaborador';
                    row = row+'<h6 style="margin: 0px 0px 0px 30px;">(Salida o Cambio de Almac&eacute;n)</h6>';
                }
                row = row+'</td>';
                row = row+'</tr>';
//                console.log(i)
//                console.log(a)
            });
            if(tipo == 'P'){
               $('.sav_t').remove();
            }
            $('#count_colaModal').html(i);
            $('#count_asigModal').html(a);
            $('#tab_turns').html(row);
            
            newModal('DetTurn','60','80','form_DetTurn');
        } else {
            errorGeneral("Sistema Intranet",turnos,'danger');
        }
        removedataLoading();
    }
}

function closeMoTurno(){
    quitarmodalGeneral('DetTurn','form_DetTurn');
    $('#count_colaModal').html(0);
    $('#count_asigModal').html(0);
    $('#tab_turns').html('');
}

$(function() {
    
    $(document).on('change', '.cmb', function() {
        var val = $(this);
        var co = val.context.id.trim().replace(/[cmb_]/g,"");
        //console.log(parseInt(val.context.value.trim()))
        if(parseInt(val.context.value.trim()) != 0){
            $('#'+co).addClass('warning');
        } else {
            $('#'+co).removeClass('warning');
        }
        countAsig();
    });
    
    $(document).on('change', '.cmb_upt', function() {
        var val = $(this);
        var tip;
        var co = val.context.id.trim();
        tip = co.indexOf("a") > 0 ?0:1;
        co = tip == 0?co.replace(/[cmb_turn_a_]/g,""):co.replace(/[cmb_turn_u_]/g,"");
        if(parseInt(val.context.value.trim()) != 0){
            if(va_sess()){
                var url1 = $('#txt_urlGe').val();
                var co_cco = $('#co_cco').val();
                var fe_in = $('#fe_in').val();
                var fe_fi = $('#fe_fi').val();

                dataLoading();
                var datos = 'c_c='+co_cco+'&f_i='+fe_in+'&f_f='+fe_fi+'&c_ti='+tip+'&c_tu='+co+'&a_tu='+parseInt(val.context.value.trim());

                $.ajax({
                    type : 'POST',
                    url : url1+'procesos/nomina/co_126_1/updaTu',
                    data: datos,
                    success : function (returnData) {
                        removedataLoading();              
                        var datta = eval('('+returnData+')');

                        if (datta.data){
                            if (datta.data.co_err === 0){
                                errorGeneral("Sistema Intranet","Registros actualizados con exito.",'success');
                                $.download('/ipycca/procesos/nomina/co_126_1',"co_bdg="+$('#co_bdg').val()+"&co_cco="+$('#co_cco').val());
                            } else {
                                errorGeneral("Sistema Intranet",datta.data.tx_err,'danger');
                            } 
                        } else {
                            errorGeneral("Sistema Intranet",returnData,'danger');
                            return false;
                        }
                    }
                });
            } else {
                redirectIntranet();
            }
        }
    });
    
    _cont = parseInt($("#count_cola").html());
    
    $(".eli_u").click(function() {
        deleTurnos($(this));
    });

    $(".sav_t").click(function() {
        saveTurnos();
    });
    
    $(".pro_t").click(function() {
        procTurnos();
    });
    
    $(".tu_print").click(function() {
        var url1 = $('#txt_urlGe').val();
        var co_cco = $('#co_cco').val();
        var fe_in = $('#fe_in').val();
        var fe_fi = $('#fe_fi').val();
        window.open(url1+'procesos/nomina/co_126_1/ge_pdf/'+fe_in+'/'+fe_fi+'/'+co_cco+'/Turnos '+co_cco);
    });
    
    $('.invoice4').removeClass('content');
}); 
