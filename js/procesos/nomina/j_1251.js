
function getSolis(){
    
    var co_e = $('#cmb_empr').val();
    
    if(co_e != 'NNN'){
        if(va_sess()){
            var url1 = $('#txt_urlGe').val();
            $('#tab_Aut').DataTable({
                bDestroy: true,
                sAjaxDataProp: "data",
                ajax: {
                    url    : url1+'procesos/nomina/co_1251/reSol',
                    type   : "POST",
                    "data"   : {"c_e": co_e},
                    dataSrc: ""
                },
                columns: [
                    {title  : '#Solicitud',data : "co_sol_link", sClass: "text-center"},
                    {title  : 'Centro',data : "al_sol"},
                    {title  : 'Fecha',data : "fe_ini"},
                    {title  : 'Ingresado por',data : "no_usu"},
                    {title  : 'Deudor',data : "de_sol"},
                    {title  : 'Motivo',data : "mo_sol"},
                    {title  : 'Tipo',data : "de_con"},
                    {title  : 'Valor',data : "mo_pre", sClass: "text-right"},
                    {title  : 'Cuotas',data : "de_cuo", sClass: "text-right"},
                    {title  : 'Deuda Vigente',data : "de_ant", sClass: "text-right"},
                    {title  : '<input type="radio" name="ck_apro" id="ck_apro" /> Aprobar',data : "ap_sol"},
                    {title  : '<input type="radio" name="ck_apro" id="ck_rech" /> Negar',data : "ne_sol"}
                ],
                aLengthMenu: [
                    [100,150, -1],
                    [100,150, "Todos"]
                ],
                order: [[ 3, "desc" ]]
            });

        } else {
            redirectIntranet();
        }
    } else {
        errorGeneral(conf_hd.tx.t_4,c.tx_err,'danger');
    }
    
}

function asigJsonSoli(cd,gh){
    var obs = $('#tx_obs').val();
    var solc = [];
    var itemR = {};
    itemR["c_s"] = cd;
    itemR["e_s"] = gh;
    solc.push(itemR);
    procSolic(solc,obs);
}

function procSolic(solc,obs){
    var co_e = $('#cmb_empr').val();
    if(va_sess()){
        if(!$.isEmptyObject(solc)){
            var r = confirm("Desea procesar los siguientes registros.");
            if (r == true) {  
                dataLoading();
                $.post('co_1251/apPre',
                {s_s:JSON.stringify(solc),o_s:obs,c_e:co_e},
                function(b){
                    removedataLoading();
                    var c = eval('('+b+')');
                    if(parseInt(c.co_err) == 0){
                        getSolis();
                        errorGeneral(conf_hd.tx.t_1,c.tx_err,'success');
                    } else {
                        errorGeneral(conf_hd.tx.t_1,c.tx_err,'danger');
                    }
                });
            }
        } else {
            errorGeneral(conf_hd.tx.t_1,conf_hd.tx.t_3,'danger'); 
        }
    } else {
        redirectIntranet();
    }
}

function aprobar_solic(){
    
    var solc = [];
    var aprobados = null;
    var rechazados = null;

    aprobados = $("input[id='option_apro[]']:checked");
    rechazados = $("input[id='option_rech[]']:checked");

    $(aprobados).each( function () {
        var itemA = {};
        itemA["c_s"] = $(this).val();            
        itemA["e_s"] = 'A';
        solc.push(itemA);
    });

    $(rechazados).each( function () {
        var itemR = {};
        itemR["c_s"] = $(this).val();            
        itemR["e_s"] = 'R';
        solc.push(itemR);
    });
    
    procSolic(solc,'');
   
}

function viewSolic(p,f){
    if(va_sess()){ 
        dataLoading();
        $.post('co_1251/rePre',
        {c_e:p,c_p:f},
        function(b){
            removedataLoading();
            var c = eval('('+b+')');
            if(c.err == ''){
                if(c.data){
                    $('#titleDoc').html(c.tite);
                    $('#view_Soli').html(c.data);
                    newModal('DetallSolic','70','75','');
                } else {
                    errorGeneral(conf_hd.tx.t_1,conf_hd.tx.t_2,'danger');
                }
            } else {
                errorGeneral(conf_hd.tx.t_1,c.err,'danger');
            }
        });
    } else {
        redirectIntranet();
    }
}
    
$(document).ready(function() {
    
    getSolis();
    
    $("#cmb_empr").change(function() {
        if (this.value.trim() != 'NNN'){
            getSolis();
        } 
    });   
    
    $(".a_sol" ).click(function(){
        aprobar_solic();
    });
    
    $("#ck_apro").toggle(
        function() {
            $("input[id='option_apro[]']").prop('checked', true);
        }, function() {
            $("input[id='option_apro[]']").prop('checked', false);
        }
    );
    
    $("#ck_rech").toggle(
        function() {
            $("input[id='option_rech[]']").prop('checked', true);
        }, function() {
            $("input[id='option_rech[]']").prop('checked', false);
        }
    );
    
}); 