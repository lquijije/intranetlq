$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
        
});

$(function() {
    
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
    
    $('#tab_promo').dataTable();
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function(ct){
            this.setOptions({
                maxDate:$('#txt_fec_fin').val()?$('#txt_fec_fin').val():false
            });
        },
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('#txt_fec_fin').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        onShow:function( ct ){
            this.setOptions({
                minDate:$('#txt_fec_ini').val()?$('#txt_fec_ini').val():false
            });
        },
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $( ".newPromo" ).button().click(function(){
        $('#co_acc').val('I');
        $('#co_pro').val('');
        $('#txt_co_art').attr('readonly',false);
        $('#cmb_est').attr('disabled',true);
        newModal('NewPro','55','50','form_NewPro');
    });
    
    $( ".savPro" ).button().click(function(){
        svProArt();
    });
    
}); 

function svProArt(){
    
    var urlGe = $('#txt_urlGe').val();
    var c_a = $('#txt_co_art').val();
    var d_a = $('#txt_ds_art').val();
    var p_i = $('#txt_pr_ini').val();
    var p_t = $('#txt_pr_top').val();
    var c_s = $('#txt_cn_sha').val();
    var f_i = $('#txt_fec_ini').val();
    var f_f = $('#txt_fec_fin').val();
    var e_p = $('#cmb_est').val();
    var c_p = $('#co_pro').val();
    
    var a = c_a+'|'+'Codigo del Articulo'+':';
    a = a + d_a+'|'+'Descripci&oacute;n'+':';
    a = a + p_i+'|'+'Precio Inicial'+':';
    a = a + p_t+'|'+'Precio Tope'+':';
    a = a + c_s+'|'+'Cantidad Total de Share'+':';
    a = a + f_i+'|'+'Fecha Inicial'+':';
    a = a + f_f+'|'+'Fecha Final';
    
    
    if(isEmptySis(a)){
        var item = {};
        item['_acc'] = $('#co_acc').val();
        item['c'] = c_a;
        item['d'] = d_a;
        item['p_i'] = p_i;
        item['p_t'] = p_t;
        item['c_s'] = c_s;
        item['f_i'] = f_i;
        item['f_f'] = f_f;
        item['e_p'] = e_p;
        item['c_p'] = c_p;
        
        var formdata = new FormData();
        var img_pro = document.getElementById("img_post");
        var img_fac = document.getElementById("img_face");
        var pro_img = img_pro.files[0];
        var fac_img = img_fac.files[0];
        
//        if (pro_img === undefined){
//            errorGeneral('Sistema Intranet','Debe ingresar una imagen para la promoción.','danger');
//            return false; 
//        }
//        
//        if (img_fac === undefined){
//            errorGeneral('Sistema Intranet','Debe ingresar una imagen para facebook.','danger');
//            return false; 
//        }
        
        if (formdata) {
            
            dataLoading();
            
            formdata.append("data",JSON.stringify(item));
            formdata.append("pro_img", pro_img);
            formdata.append("fac_img", fac_img);
        
            $.ajax({
                type : 'POST',
                url : urlGe+"procesos/pyccacom/co_120/sv_tra", 
                data: formdata,
                processData: false,
                contentType: false,
                success : function (returnData) {                 
                    removedataLoading();         
                    var data =  eval('('+returnData+')'); 
                    if(!$.isEmptyObject(data.data)) {
                        errorGeneral("Sistema Intranet",data.data.txt_err,'danger');
                        window.location = window.location.href;
                    } else {
                        errorGeneral("Sistema Intranet",'Error al Actualizar','danger');
                    }
                }
            });
        }
    }
}

function getArt(ca){ 
    if (!$('#txt_co_art').is('[readonly]')) {
        if(ca.trim() != ''){

            var url1 = $('#txt_urlGe').val();
            var datos = "c_a="+ca;

            dataLoading();

            $.ajax({
                type : 'POST',
                url : url1+'procesos/pyccacom/co_120/retArt',
                data: datos,
                success : function (returnData) {
                    removedataLoading();
                    var datos = eval('('+returnData+')'); 
                    if(!$.isEmptyObject(datos.data)) {
                        $('#txt_ds_art').val(datos.data.ds_art);
                        $('#txt_pr_ini').val(datos.data.pr_art);
                    } else {
                        $('#txt_co_art').val('');
                        errorGeneral("Sistema Intranet",datos.err,'danger');
                    }
                }

            });

        }
    }
}

function viewPro(c_p,acc){
    
    var url1 = $('#txt_urlGe').val();
    var datos = "c_p="+c_p;
    $('#co_pro').val('');
    
    dataLoading();

    $.ajax({
        type : 'POST',
        url : url1+'procesos/pyccacom/co_120/retPro',
        data: datos,
        success : function (returnData) {
                removedataLoading();
                var datos = eval('('+returnData+')'); 
                if(!$.isEmptyObject(datos.data)) {
                    
                    $.each(datos.data,function(e,dt){
                        $('#co_pro').val(c_p);
                        $('#txt_co_art').val(dt.co_a);
                        $('#txt_ds_art').val(dt.ds_a);
                        $('#txt_pr_ini').val(dt.pr_i);
                        $('#txt_pr_top').val(dt.pr_b);
                        $('#txt_cn_sha').val(dt.me_s);
                        $('#txt_fec_ini').val(dt.fe_i);
                        $('#txt_fec_fin').val(dt.fe_f);
                        $('#cmb_est').val(dt.es_p);
                        
                        document.getElementById("post_img").innerHTML = ['<a class="fancybox" href="',"http://preciorecord.pycca.com/img/articulos/"+dt.im_p,'" title="',dt.im_p,'"><img class="example-image" width="100px" height="100px" src="',"http://preciorecord.pycca.com/img/articulos/"+dt.im_p,'" title="',dt.im_p, '"/></a>'].join('');
                        $("#img_post_name").val(dt.im_p);
                        
                        document.getElementById("face_img").innerHTML = ['<a class="fancybox" href="',"http://preciorecord.pycca.com/img/articulos/face/"+dt.im_f,'" title="',dt.im_f,'"><img class="example-image" width="100px" height="100px" src="',"http://preciorecord.pycca.com/img/articulos/face/"+dt.im_f,'" title="',dt.im_f, '"/></a>'].join('');
                        $("#img_face_name").val(dt.im_f);
                    });
                    
                    if(acc.trim() === 'C'){

                    } else if(acc.trim() === 'U'){
                        $('#txt_co_art').attr('readonly',true);
                        $('#cmb_est').attr('disabled',false);
                        newModal('NewPro','55','50','form_NewPro');
                    }

                    $('#co_acc').val(acc);
                    
                } else {
                    $('#txt_co_art').val('');
                    errorGeneral("Sistema Intranet",datos.err,'danger');
                }
            }

    });
    
    
}