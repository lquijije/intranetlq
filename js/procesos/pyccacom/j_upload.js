

//$(function () {
//
//    $(".btn-upload :file").change(function (event) {
//        $("#addFile").html('');
//        if (this.files.length < 11) {
//            $.each(this.files, function (a, b) {
//                var reader = new FileReader();
//                reader.onload = (function (theFile) {
//                    return function (e) {
//                        if (fielArray.indexOf(theFile.type) >= 0) {
//                            $("#addFile").append('<div class="col-xs-2"style="margin:0px;background: #FFF; height: 150px; border: 3px solid rgb(9, 61, 108) !important;"><a class="example-image-link" href="' + e.target.result + '" data-lightbox="example-set" title="' + escape(theFile.name) + '"><img class="example-image" src="' + e.target.result + '" title="' + escape(theFile.name) + '"/></a><div>');
//                        } else {
//                            errorGeneral(conf_hd.tx.t_1, 'Formato de archivo incorrecto en la imagen' + escape(theFile.name) + ' ', 'danger');
//                        }
//                    };
//                })(this);
//                reader.readAsDataURL(this);
//            });
//        } else {
//            $("#file_img").val('');
//            errorGeneral(conf_hd.tx.t_1, 'Solo puede ingresar maximo 10 imagenes.', 'danger');
//        }
//
//    });
//
//});


function fileread(file) {
    var fielArray = ["image/png", "image/jpeg", "image/jpg"];
    var files = $('#file_img').prop('files');
    var formdata = new FormData();
    if (files.length < 11) {
        if (!$.isEmptyObject(files)) {
            $.each(files, function (a, b) {
                if (fielArray.indexOf(files[a].type) >= 0) {
                    formdata.append("fi_up[]", files[a]);
                } else {
                    errorGeneral(conf_hd.tx.t_1, 'Formato de archivo incorrecto en la imagen' + escape(files[a].name) + ' ', 'danger');
                }
            });
            dataLoading();
            $.ajax({
                url: "co_upload/saveImgs",
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (b) {
                    var c = eval('(' + b + ')');
                    if ($.isEmptyObject(c.error)) {
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_5, 'success');
                        window.location = window.location.href;
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                    removedataLoading();
                }
            });
        }
    } else {
        errorGeneral(conf_hd.tx.t_1, 'Solo puede ingresar maximo 10 imagenes.', 'danger');
    }
}

