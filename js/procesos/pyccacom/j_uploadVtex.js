
$(function () {
    $(".searchInventar").click(function () {
        getInventar();
    });
});

function getInventar() {
    $('#contTable').html('');
    var tipo = $('#cmb_tipo').val();
    var buscar = $('#txt_busqueda').val();
    if ($.isEmptyObject(buscar)) {
        errorGeneral(conf_hd.tx.t_1, 'El campo busqueda se encuentra vacio.', 'danger');
        return false;
    }

    if (buscar.length < 4) {
        errorGeneral(conf_hd.tx.t_1, 'La busqueda debe de tener minimo 5 caracteres.', 'danger');
        return false;
    }

    if (va_sess()) {

        var table_ = $('#contTable').DataTable({
            responsive: true,
            bDestroy: true,
            sAjaxDataProp: "dat",
            ajax: {
                url: 'co_uploadVtex/consultaArt',
                type: 'POST',
                data: {c_t: tipo, c_b: buscar},
                dataSrc: function (a) {
                    if (!$.isEmptyObject(a.dat)) {
                        return a.dat;
                    } else {
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_2, 'danger');
                        return false;
                    }
                }
            },
            columns: [
                {title: 'C&oacute;digo', "data": "_link", sClass: "text-center"},
                {title: 'Descripci&oacute;n', "data": "ds_art"},
                {title: 'Fabrica', "data": "co_fab"}
            ]
        });
        $('.dt_clear').on('click', function () {
            table_.search('').columns().search('').draw();
        });
    } else {
        redirectIntranet();
    }

}

function consArticulo(co_art) {
    if (va_sess()) {
        $.post('co_uploadVtex/retDArt',
                {c_art: co_art},
                function (c) {
                    c = eval('(' + c + ')');
                    if ($.isEmptyObject(c.err)) {
                        $('#imgsArt').html(c.dat);
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });

    } else {
        redirectIntranet();
    }
}

function modalArtInvent(a) {
    if (va_sess()) {
        var datos = $.parseJSON(a);
        $('#hiArt').val(datos.co_articulo);
        $('#desArt').html(datos.co_articulo + ' - ' + datos.ds_articulo);
        consArticulo(datos.co_articulo);
        newModal('ArtFind', '60', '90', '');

    } else {
        redirectIntranet();
    }
}

function fileread(file) {
    var fielArray = ["image/png", "image/jpeg", "image/jpg"];
    var files = $('#file_img').prop('files');
    var co_art = $('#hiArt').val();
    var formdata = new FormData();
    if (files.length < 11) {
        if (!$.isEmptyObject(files)) {
            $.each(files, function (a, b) {
                if (fielArray.indexOf(files[a].type) >= 0) {
                    formdata.append("fi_up[]", files[a]);
                } else {
                    errorGeneral(conf_hd.tx.t_1, 'Formato de archivo incorrecto en la imagen' + escape(files[a].name) + ' ', 'danger');
                }
            });
            dataLoading();
            formdata.append("co_art", co_art);
            $.ajax({
                url: "co_uploadVtex/saveImgs",
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (b) {
                    var c = eval('(' + b + ')');
                    if ($.isEmptyObject(c.error)) {
                        consArticulo(co_art);
                        errorGeneral(conf_hd.tx.t_1, conf_hd.tx.t_5, 'success');
                    } else {
                        errorGeneral(conf_hd.tx.t_1, c.tx_err, 'danger');
                    }
                    removedataLoading();
                }
            });
        }
    } else {
        errorGeneral(conf_hd.tx.t_1, 'Solo puede ingresar maximo 10 imagenes.', 'danger');
    }
}

function delFil(fl) {
    if (va_sess()) {
        dataLoading();
        $.post('co_uploadVtex/dFil',
                {fl: fl},
                function (c) {
                    c = eval('(' + c + ')');

                    var sal = fl.split('-');
                    consArticulo(sal[0]);
                    if (!$.isEmptyObject(c.err)) {
                        errorGeneral(conf_hd.tx.t_1, c.err, 'danger');
                    }
                    removedataLoading();
                }).fail(function (p) {
            removedataLoading();
            errorGeneral(conf_hd.tx.t_1, "ERROR:" + p.statusText + " - " + p.statusText, 'danger');
        });
    } else {
        redirectIntranet();
    }
}