
var __cont = 0;

$(function() {
    
    $( "#cmb_tipo" ).change(function() {
        if($('#txt_identificacion').val().toString().trim() !== ''){
            
            $('#Pass_det_art').empty();
            $('#txt_desc_serv').html('');
            $('#txt_num_doc').val('');
            
            if (this.value.trim() === 'B'){
                $("#ifS").css('display','none');
                $("#ifB").css('display','block');
                $("#ifA").css('display','block');
            } else if (this.value.trim() === 'S'){ 
                $("#ifB").css('display','none');
                $("#ifA").css('display','none');
                $("#ifS").css('display','block');
            } else {
                $("#ifB").css('display','none');
                $("#ifA").css('display','none');
                $("#ifS").css('display','none');
            }
            
        } else {
            $( "#cmb_tipo" ).val('NNN');
            errorGeneral("Sistema Intranet",'Debe ingresar la identificaci&oacute;n','danger'); 
            return false;
        }
        
    });    
    
    $(".searchClient").click(function() {
        getCliente();
    });
    
    $(".busFact").click(function() {
        $('#txt_sear_ci').val($('#txt_identificacion').val());
        newModal('Fact','65','50','form_Fact');
        $('#T_regFact').dataTable().fnClearTable();
    });
    
    $(".filtroFact").click(function() {
        searchFact();
    });
    
    $(".passArt").click(function() {
        passArt();
    });
    
    $(".saveReclamo").click(function() {
        saveReclamo();
    });
    
    $('.form_datetime').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('.form_hour').datetimepicker({
        datepicker:false,
        format:'H:i',
        closeOnDateSelect:true
    });
    
    
}); 

function getCliente(){
    
    var cod_cli = $('#txt_identificacion').val();
    var url1 = $('#txt_urlGe').val();
    
    if (cod_cli.trim() === ''){
        errorGeneral("Sistema Intranet",'Debe ingresar la identificaci&oacute;n','danger'); 
        return false;
    }
    
    var datos = "c_c="+cod_cli;
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/averiados/co_8800/retClient',
        data: datos,
        success : function (returnData) {
            removedataLoading();
            var data = eval('('+returnData+')');
            if (data.data){ 
                $.each(data.data,function(e,dato){
                    $('#txt_nomb_ape').val(dato.nom_cli);
                    $('#txt_direccion').val(dato.dir_cli);
                    $('#txt_convencional').val(dato.tel_cli);
                    $('#txt_celular').val(dato.tel_cli);
                    $('#txt_mail').val(dato.mail_cli);
                });
            } else {
                if(data.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    $('#form_reclamos_ge')[0].reset();
                    return false;
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                    return false;
                }
            }

        }
    });

}

function saveReclamo(){
    		
				
    var url1 = $('#txt_urlGe').val();
    var cod_bdg = $('#cod_emp').val();
    var id_cliente = $('#txt_identificacion').val();
    var nom_cli = $('#txt_nomb_ape').val();
    var direccion = $('#txt_direccion').val();
    var telf_conve = $('#txt_convencional').val();
    var telf_celu = $('#txt_celular').val();
    var email = $('#txt_mail').val();
    var tipo = $('#cmb_tipo').val();
    var num_fac = $('#txt_num_doc').val();
    var arti_deja = $('#txt_desc_serv').val();
    var fech_reclamo = $('#txt_fec_doc').val();
    var hora = $('#txt_hora').val();
    var descripcion = $('#txt_desc_clara').val();
    var otros_problema = $('#otros_problema').val();
    var otros_entrega = $('#otros_entrega').val();
    
    var objetivo = $('#txt_pretende').val();
    var fech_termino = $('#txt_fec_doc').val();
	var json_cabecera = [];
    var json_art = [];
    
    var a = id_cliente+'|'+'Identificaci&oacute;n'+':';
    a = a + nom_cli+'|'+'Nombre del Cliente'+':';
    a = a + direccion+'|'+'Direcci&oacute;n del Cliente'+':';
    if (tipo.toString().trim() === 'S'){
        a = a + des_serv+'|'+'Descripci&oacute;n del servicio'+':';
    } else if (tipo.toString().trim() === 'B'){
        a = a + num_fac+'|'+'No. De Factura'+':';
    } else {
        a = a + tipo+'|'+'Tipo'+':';
    }
    a = a + fech_reclamo+'|'+'Fecha del Inconveniente'+':';
    a = a + hora+'|'+'Hora del Inconveniente'+':';
    a = a + descripcion+'|'+'Descripción clara y consica del reclamo'+':';
    a = a + objetivo+'|'+'Resultado que pretende obtener con el reclamo';
    
    if(!$("#bit_mail").is(':checked') && !$("#bit_convencional").is(':checked') && !$("#bit_celular").is(':checked')){
        errorGeneral("Sistema Intranet",'Debe de Seleccionar por lo menos una forma que desea recibir su respuesta.','danger');
        return false;
    }
    
    if($("#bit_mail").is(':checked') && email === ''){
        errorGeneral("Sistema Intranet",'Debe de ingresar la direcci&oacute;n de correo electronico.','danger');
        return false;
    }
    
    if(!validaMail(email)){
        return false;
    }
    
    if(isEmptySis(a)){
        
        var item = {};
        item["id_cliente"]=id_cliente;
        item["nom_cli"]=nom_cli;
        item["direccion"]=direccion;
        item["telf_conve"]=telf_conve;
        item["telf_celu"]=telf_celu;
        item["email"]=email;
        item["tipo"]=tipo;
        item["num_fac"]=num_fac;
        item["arti_deja"]=arti_deja;
        item["fech_reclamo"]=fech_reclamo;
        item["hora"]=hora;
	item["fech_termino"]=fech_termino;
        item["descripcion"]=descripcion;
        item["objetivo"]=objetivo;
        item["bit_mail"] = $("#bit_mail").is(':checked') ? 1:0;
        item["bit_conv"] = $("#bit_convencional").is(':checked') ? 1:0;
        item["bit_celular"] = $("#bit_celular").is(':checked') ? 1:0;
		
        item["no_enci"] = $("#no_enci").is(':checked') ? 1:0;
        item["pin_carga"] = $("#pin_carga").is(':checked') ? 1:0;
        item["se_descarga"] = $("#se_descarga").is(':checked') ? 1:0;
        item["se_caliente"] = $("#se_caliente").is(':checked') ? 1:0;
        item["se_apaga"] = $("#se_apaga").is(':checked') ? 1:0;

        item["tactil_nofunciona"] = $("#tactil_nofunciona").is(':checked') ? 1:0;
        item["audio_nfunc"] = $("#audio_nfunc").is(':checked') ? 1:0;
        item["tec_nfunc"] = $("#tec_nfunc").is(':checked') ? 1:0;
        item["micro_nfunc"] = $("#micro_nfunc").is(':checked') ? 1:0;
        item["se_baja"] = $("#se_baja").is(':checked') ? 1:0;
        item["no_wifi"] = $("#no_wifi").is(':checked') ? 1:0;

        item["msj_error"] = $("#msj_error").is(':checked') ? 1:0;
        item["disp_bloq"] = $("#disp_bloq").is(':checked') ? 1:0;
        item["pant_bloq"] = $("#pant_bloq").is(':checked') ? 1:0;
        item["desprog"] = $("#desprog").is(':checked') ? 1:0;
        item["no_sistoper"] = $("#no_sistoper").is(':checked') ? 1:0;
        item["otros_problema"]=otros_problema;

        item["pant_rota"] = $("#pant_rota").is(':checked') ? 1:0;
        item["carc_golp"] = $("#carc_golp").is(':checked') ? 1:0;
        item["carc_raya"] = $("#carc_raya").is(':checked') ? 1:0;
        item["disp_moja"] = $("#disp_moja").is(':checked') ? 1:0;
        item["disp_mani"] = $("#disp_mani").is(':checked') ? 1:0;
        item["endi"] = $("#endi").is(':checked') ? 1:0;

        item["dispo"] = $("#dispo").is(':checked') ? 1:0;
        item["bateria"] = $("#bateria").is(':checked') ? 1:0;
        item["cargador"] = $("#cargador").is(':checked') ? 1:0;
        item["cables"] = $("#cables").is(':checked') ? 1:0;
        item["tapa_post"] = $("#tapa_post").is(':checked') ? 1:0;
        item["cable_poder"] = $("#cable_poder").is(':checked') ? 1:0;

        item["cont_remo"] = $("#cont_remo").is(':checked') ? 1:0;
        item["audifono"] = $("#audifono").is(':checked') ? 1:0;
        item["caja_orig"] = $("#caja_orig").is(':checked') ? 1:0;
        item["manual"] = $("#manual").is(':checked') ? 1:0;
        item["otros_entrega"]=otros_entrega;

        json_cabecera.push(item);
        dataLoading();
       
        $('#getArti tr').each(function (){

            var item = {};
            var fec = $(this).find('.cod_ART').html();
			//var desc = $(this).find('.desc').html();
            var por = $(this).find('.modelo_ART').val();
	    var serie = $(this).find('.serie_ART').val();
			
            if(fec != null && por != null){
                item["cod_art"] = fec;            
		//item["desc"] = desc;
                item["modelo_art"] = por;
		item["serie"] = serie;

                json_art.push(item); 

            }
        });
        
        var datos = 'c_a='+JSON.stringify(json_cabecera)+'&d_a='+JSON.stringify(json_art);
        
        $.ajax({
            type : 'POST',
            url : url1+'procesos/averiados/co_8800/saveReclamo',
            data: datos,
            success : function (returnData) {

                removedataLoading();              
                var datta = eval('('+returnData+')');
                
                if (datta.data){
                    
                    $.each(datta,function(e,dat){
                        if (dat.a === 2){
                            errorGeneral("Sistema Intranet",dat.b,'success');
                            $('#form_reclamos_ge')[0].reset();
                            $('#Pass_det_art').empty();
                            window.open(url1+'procesos/averiados/co_8803/viewPDF/'+cod_bdg+'/'+dat.c+'/Reclamo '+dat.c);
                            return false;
                        } else {
                            errorGeneral("Sistema Intranet",dat.b,'danger');
                            return false;
                        }
                    });

                } else {
                    
                    errorGeneral("Sistema Intranet",returnData,'danger');
                    return false;
                }

            }
        });
    }
    
}

function passEmpr(a,b){
    
    $('#cod_emp').val(a);
    $('#id_emp').val(b);
    document.getElementById("f_solch").submit();
    
}

function searchFact(){
    
    var ci = $('#txt_sear_ci').val();
    var mes = $('#cmb_mes').val();
    var anio = $('#cmb_anio').val();
    var url1 = $('#txt_urlGe').val();
    var datos = "c_i="+ci+"&m_s="+mes+"&a_o="+anio;
    
    if (ci.trim() === ''){
        errorGeneral("Sistema Intranet",'Ingrese la Identificaci&oacute;n','danger'); 
        return false;
    }
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/averiados/co_8800/retFact',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();
            $('#T_regFact').dataTable().fnClearTable();
            
            var data = eval('('+returnData+')');

            if (data.data){

                $('#tableFact').empty();
                var row = '<table id="T_regFact" class="table table-bordered table-hover">';
                row = row+'<thead>';
                row = row+'<th>Almac&eacute;n</th>';
                row = row+'<th>Tipo de Documento</th>';
                row = row+'<th># Documento</th>';
                row = row+'<th>Valor Transacci&oacute;n</th>';
                row = row+'<th>Fecha</th>';
                row = row+'<th></th>';
                row = row+'</thead>';
                row = row+'<tbody>';

                $.each(data.data,function(e,dato){
                    row = row+'<tr>';
                    row = row+'<td>'+dato.des_bod+'</td>';
                    row = row+'<td>'+dato.tip_nom+'</td>';
                    row = row+'<td>'+pad(dato.cod_bod,3)+'-'+dato.num_caj+'-'+dato.num_doc+'</td>';
                    row = row+'<td>'+dato.val_doc+'</td>';
                    row = row+'<td>'+dato.fec_doc+'</td>';
                    row = row+'<td class="centro"><button type="button" onclick="viewDoc('+"'"+dato.tip_doc+"'"+','+"'"+dato.cod_bod+"'"+','+"'"+dato.num_caj+"'"+','+"'"+dato.num_doc+"'"+')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                    row = row+'</tr>';
                });

                row = row+'</tbody>';
                row = row+'</table>';

                $('#tableFact').append(row);
                $('#T_regFact').dataTable();

            } else {
                if(data.data === null){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }

        }
    });

}

function pad (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function viewDoc(a,b,c,d){
    __cont = 0;
    var url1 = $('#txt_urlGe').val();
    var datos = "t_d="+a+"&c_i="+b+"&c_c="+c+"&c_d="+d;
    var num_fact = pad(b,3);
    $('#id__fact').html(num_fact.toString()+'-'+c.toString()+'-'+d.toString());
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/averiados/co_8800/retDetFact',
        data: datos,
        success : function (returnData) {
            
                removedataLoading();
                
                $('#det_articulos').empty();
                
                var data = eval('('+returnData+')');

                if (data.data){

                    var row = '';
                    var fact = num_fact.toString()+'-'+c.toString()+'-'+d.toString();
                    $.each(data.data,function(e,dato){                            
                        __cont++;
                        row = row+'<tr id="option_'+__cont+'">';
                        row = row+'<td>'+dato.cod_art+'</td>';
                        row = row+'<td>'+dato.des_art+'</td>';
                        row = row+'<td>'+dato.cat_art+'</td>';
                        row = row+'<td>'+dato.val_art+'</td>';
                        row = row+'<td class="centro"><input type="radio" id="'+__cont+'" name="opt_art[]" value="'+dato.cod_art+'" desc="'+dato.des_art+'"></td>';
                        row = row+'</tr>';
                    });

                    $('#det_articulos').append(row);
                    $('#botonServicios').append("<button type='button' class='btn btn-success' onclick='passArt("+'"'+fact+'"'+")' style='position: absolute; right: 0;margin: 0.2% 0.6%;'><i class='fa fa-check'></i> Seleccionar Art&iacute;culos</button>");
                    
                    newModal('DetFact','50','45','form_DetFact');
                    $('#modalDetFact').css('z-index','1042');
                    $('#fadeDetFact').css('z-index','1041');
    
                } else {
                    if(data.data === null){
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    } else {  
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                    }
                }
                
                
            }
    });
}

function passArt(fact){
    
    var select = $("input[name='opt_art[]']:checked");
    if($(select).length <= 0){
        errorGeneral("Sistema Intranet",'Seleccione los art&iacute;culos','danger');
        return false;
    }    
    
    var ara = '';
    $('#Pass_det_art').empty();
    $('#txt_num_doc').val(fact);
    
    $(select).each( function () {
        ara = ara +'<tr>';
        ara = ara +'<td class="cod_ART">'+$(this).val()+'</td>';
        ara = ara +'<td>'+$(this).attr('desc')+'</td>';
        ara = ara +'<td><textarea class="form_control modelo_ART" value="" style="width: 100%;"></textarea></td>';
		ara = ara +'<td><textarea class="form_control serie_ART" value="" style="width: 100%;"></textarea></td>';
		ara = ara +'</tr>';
    });
    
    $('#Pass_det_art').append(ara);
    quitarmodalGeneral('Fact','form_Fact');
    quitarmodalGeneral('DetFact','form_DetFact');
}