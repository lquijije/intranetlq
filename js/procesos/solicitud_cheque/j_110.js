var contGe = 0;

$(function() {
    
    $(".convertText" ).button().click(function(){
        convert();
    });
    
    $("#cmb_empr").change(function() {
        if (this.value.trim() !== 'NNN'){
            var url_1 = $('#txt_urlGe').val();
            retAproSoliChe(this.value,url_1);
        } 
    });    
    
    $(".apr_soli").button().click(function(){
        arrSolic();
    });
    
    if ($("#cmb_empr").length){
        var emp = $("#cmb_empr").val();
        if(emp.toString().trim() !== '' || emp.toString().trim() !== 'NNN'){
           retAproSoliChe(emp,$('#txt_urlGe').val());
        }
    }
    
}); 

function retAproSoliChe(empresa,url_1){
    
    $('#table_solic').html('');
    var datos = "cod_emp="+empresa;
                
    if(va_sess()){
        dataLoading();
        $.ajax({
            type : 'POST',
            url : url_1+"procesos/solicitud_cheque/co_110/reSol", 
            data: datos,
            success : function (returnData){ 
                removedataLoading();     
                var data = eval('('+returnData+')'); 
                if(data.err == ''){
                    if(data.solic != null){

                        var tabl = '<table id="table_solicitudes" class="table table-bordered TFtable">';
                        tabl = tabl+'<thead>';
                        tabl = tabl+'<tr>';
                        tabl = tabl+'<th>No. Solicitud</th>';
                        tabl = tabl+'<th>Tipo Doc.</th>';
                        tabl = tabl+'<th>No. Documento</th>';
                        tabl = tabl+'<th>Fecha Solic.</th>';
                        tabl = tabl+'<th>Centro Costo</th>';
                        tabl = tabl+'<th>Responsable</th>';
                        tabl = tabl+'<th>Proveedor</th>';
                        tabl = tabl+'<th>Valor</th>';
                        tabl = tabl+'<th><input type="radio" name="ck_apro" id="ck_apro" /> Aprobar</th>';
                        tabl = tabl+'<th><input type="radio" name="ck_apro" id="ck_rech" /> Negar</th>';
                        tabl = tabl+'</tr>';
                        tabl = tabl+'</thead>';
                        tabl = tabl+'<tbody>';

                        $.each(data.solic,function(a,dato){
                            contGe++;
                            tabl = tabl+'<tr>';
                            tabl = tabl+'<td class="centro"><a href="javascript:void(0)" onclick="viewSolic('+dato.num_sol+','+"'"+empresa+"'"+')" class="link" style="text-decoration: underline;">'+dato.num_sol+'</a></td>';
                            tabl = tabl+'<td>'+dato.nom_doc+'</td>';
                            tabl = tabl+'<td>'+dato.num_doc+'</td>';
                            tabl = tabl+'<td class="centro">'+dato.fecha+'</td>';
                            tabl = tabl+'<td><a href="javascript:void(0)" class="link">('+dato.cod_cec+') </a>'+dato.nom_cec+'</td>';
                            tabl = tabl+'<td>'+dato.nom_emp+'</td>';
                            tabl = tabl+'<td>'+dato.cod_pro+'</td>';
                            tabl = tabl+'<td class="derecha">'+formato_numero(roundTo((parseFloat(dato.valor)+parseFloat(dato.val_var)+parseFloat(dato.val_iva))),2,".",",")+'</td>';
                            tabl = tabl+'<td><center><input type="radio" name="ck_'+contGe+'" id="option_apro[]" value="'+dato.num_sol+'" data-label="'+dato.tip_doc+'"></center></td>';
                            tabl = tabl+'<td><center><input type="radio" name="ck_'+contGe+'" id="option_rech[]" value="'+dato.num_sol+'" data-label="'+dato.tip_doc+'"></center></td>';
                            tabl = tabl+'</tr>';
                        });

                        tabl = tabl+'</tbody>';
                        tabl = tabl+'</table>';

                        $('#table_solic').html(tabl);
                        $('#table_solicitudes').dataTable({"paging":   false,
                            "searching": false,
                            "info":     false,
                            "aLengthMenu": [
                                [200,300,450, -1],
                                [200,300,450, "Todos"]
                            ]
                        });
                        cambia();

                    } else {
                        $('#aproba_solic').html('');
                        errorGeneral("Sistema Intranet",'No existen solicitudes por aprobar.','danger'); 
                    }
                } else {
                    errorGeneral("Sistema Intranet",data.err,'danger'); 
                }
            }
        });
    } else {
        redirectIntranet();
    }
    
}

function cambia(){
    
    var i = 1;
    var a = 1; 
    $("#ck_apro").button().click(function(){
        if(i === 1){
            $("input[id='option_apro[]']").prop('checked', true);
            i++;
        } else if(i === 2){
            $(this).prop('checked', false);
            $("input[id='option_apro[]']").prop('checked', false);
            i = 1;
        }
    });
    
    $("#ck_rech").button().click(function(){
        if(a === 1){
            $("input[id='option_rech[]']").prop('checked', true);
            a++;
        } else if(a === 2){
            $(this).prop('checked', false);
            $("input[id='option_rech[]']").prop('checked', false);
            a = 1;
        }
    });
}

function arrSolic(){

    var solc = [];
    var emp = $("#cmb_empr").val();
    var aprobados = null;
    var rechazados = null;
    
    if(va_sess()){
        aprobados = $("input[id='option_apro[]']:checked");
        rechazados = $("input[id='option_rech[]']:checked");

        $(aprobados).each( function () {
            var itemA = {};
            itemA["c_s"] = $(this).val();
            itemA["e_s"] = 'A';
            itemA["t_s"] = $(this).attr('data-label');
            solc.push(itemA);
        });

        $(rechazados).each( function () {
            var itemR = {};
            itemR["c_s"] = $(this).val();
            itemR["e_s"] = 'R';
            itemR["t_s"] = $(this).attr('data-label');
            solc.push(itemR);
        });
        procSolic(emp,solc,'');
    } else {
        redirectIntranet();
    }
    
}

function asigJsonSoli(ab,cd,ef,gh){
    var obs = $('#p').val();
    var solc = [];
    var itemR = {};
    itemR["c_s"] = cd;
    itemR["e_s"] = gh;
    itemR["t_s"] = ef;
    solc.push(itemR);
    procSolic(ab,solc,obs);
}

function procSolic(emp,solc,obs){
    var url1 = $('#txt_urlGe').val();
    if(va_sess()){
        if(!$.isEmptyObject(solc)){
            var r = confirm("Desea procesar los siguientes registros.");
            if (r == true) {  
                var datos = "i_e="+emp+"&d_s="+JSON.stringify(solc)+"&o_s="+encodeURIComponent(obs);
                dataLoading();
                $.ajax({
                    type : 'POST',
                    url : url1+"procesos/solicitud_cheque/co_110/apSol",
                    data: datos,
                    success : function (retData){             
                        removedataLoading();    
                        var data = eval('('+retData+')');
                        if(parseInt(data.co_err) == 0){
                            $('#view_Soli_cab').html(''); 
                            $('#titleDoc').html('');
                            quitarmodalGeneral('DetallSolic','form_DetallSolic');
                            retAproSoliChe($("#cmb_empr").val().toString().trim(),url1);
                            errorGeneral("Sistema Intranet",data.tx_err,'success');
                        } else {
                            errorGeneral("Sistema Intranet",data.tx_err,'danger');
                        } 
                    }
                });
            }
        } else {
            errorGeneral("Sistema Intranet",'Debe Seleccionar una solicitud.','danger'); 
        }
    } else {
        redirectIntranet();
    }
}

function viewSolic(id,emp){
    
    $('#view_Soli_cab').html(''); 
    $('#titleDoc').html('');

    if(va_sess()){
        var url1 = $('#txt_urlGe').val();
        var datos = "i_s="+id+"&i_e="+emp;
        dataLoading();
        $.ajax({
            type : 'POST',
            url : url1+"procesos/solicitud_cheque/co_110/viSol", 
            data: datos,
            success : function (retData){             
                removedataLoading();    
                var data = eval('('+retData+')');
                if(!data.Err){
                    $('#titleDoc').html('Empresa: '+data.empr+' / Solicitud: '+id +' / Status: '+data.status);
                    $('#view_Soli_cab').html(data.soli);
                    newModal('DetallSolic','70','95','form_DetallSolic');
                } else {
                    errorGeneral("Sistema Intranet",data.Err,'danger'); 
                }
            }
        });
    } else {
        redirectIntranet();
    }
    
}