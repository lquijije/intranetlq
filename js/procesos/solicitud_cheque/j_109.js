var countCecos = 0;
var i_count_ge = 0;
var i_count_plan_pago = 0;
var total_ge = 0;

function exitModal(){
    $('#T_regProveed').dataTable().fnClearTable();
    quitarmodalGeneral('Provee','form_Provee');
}

function getProveedor(){
    
    var ruc = $('#txt_search_ruc').val();
    var name = $('#txt_search_name').val();
    var url1 = $('#txt_urlGe').val();
    var datos = "ruc="+ruc+"&name="+name;
    
    if (ruc.trim() === '' && name.trim() === ''){
        errorGeneral("Sistema Intranet",'Debe ingresar por lo menos el "RUC" o el "Nombre" ','danger'); 
        return false;
    }

    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/solicitud_cheque/co_109/retProvee',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();
            $('#T_regProveed').dataTable().fnClearTable();

            if (returnData.trim().substr(0,2) === "OK"){

                var data = eval(returnData.trim().substr(3));
                if (data){ 
                    $('#tableProveed').empty();
                    var row = '<table id="T_regProveed" class="table table-bordered table-hover">';
                    row = row+'<thead>';
                    row = row+'<th>Ruc</th>';
                    row = row+'<th>Nombre</th>';
                    row = row+'<th>Dirección</th>';
                    row = row+'<th>Correo</th>';
                    row = row+'</thead>';
                    row = row+'<tbody>';

                    $.each(data,function(e,dato){
                        //console.log(encodeURIComponent(JSON.stringify(dato)))
                        row = row+'<tr>';
                        row = row+'<td class="izquierda"><a href="javascript:void(0)" onclick="passProv('+"'"+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+"'"+')" class="link">'+dato.ruc_prov+'</a></td>';
                        row = row+'<td class="izquierda">'+dato.nom_prov+'</td>';
                        row = row+'<td class="izquierda">'+dato.dir_prov+'</td>';
                        row = row+'<td class="izquierda">'+dato.mail_prov+'</td>';
                        row = row+'</tr>';
                    });

                    row = row+'</tbody>';
                    row = row+'</table>';

                    $('#tableProveed').append(row);
                    $('#T_regProveed').dataTable();
                }  

            } else {
                if(returnData.trim() === ''){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
                
        }
    });

}

function getDocPen(){
    
    var ruc = $('#ruc_prov').text();
    var tip_doc = $('#cmb_tip_doc').val();
    var co_e = $('#cod_emp').val();
    var url1 = $('#txt_urlGe').val();
    var datos = "ruc="+ruc+"&co_e="+co_e;
    
    if (ruc.trim() === ''){
        errorGeneral("Sistema Intranet",'No ha seleccionado un Proveedor','danger'); 
        return false;
    }
    
    if (parseInt(tip_doc.trim()) != 58){
        errorGeneral("Sistema Intranet",'El tipo debe de ser 58 - Facturas Electronicas','danger'); 
        return false;
    }
    
    newModal('DocPen','75','70','form_DocPen');
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/solicitud_cheque/co_109/retDocPen',
        data: datos,
        success : function (returnData) {
            
                removedataLoading();
                $('#T_DocPen').dataTable().fnClearTable();
                
                if (returnData.trim().substr(0,2) === "OK"){
                    
                    var data = eval(returnData.trim().substr(3));
                    if (data){ 
                        $('#tableDocPen').empty();
                        var row = '<table id="T_DocPen" class="table table-bordered table-hover">';
                        row = row+'<thead>';
                        row = row+'<th>Fecha Emisión</th>';
                        row = row+'<th>Numero Documento</th>';
                        row = row+'<th>Estado</th>';
                        row = row+'<th>Autorización</th>';
                        row = row+'<th>Total</th>';
                        row = row+'<th></th>';
                        row = row+'</thead>';
                        row = row+'<tbody>';
                        
                        $.each(data,function(e,dato){
                            //console.log(encodeURIComponent(JSON.stringify(dato)))
                            row = row+'<tr>';
                            
                            var art = '<div style="padding:5px;display: inline-block;">';
                            art = art +'<div class="spa-12 line">Aut: '+dato.e+'</div>';
                            art = art +'<div class="spa-12 line">Fecha: '+dato.f+'</div>';
                            art = art +'<div class="spa-12">Clave: '+dato.g+'</div>';
                            art = art +'</div>';

                            row = row+'<td class="centro">'+dato.h+'</td>';
                            row = row+'<td class="centro"><a href="javascript:void(0)" onclick="passDoc('+"'"+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+"'"+')" class="link">'+dato.a+"-"+dato.b+"-"+dato.c+'</a></td>';
                            row = row+'<td class="centro">'+dato.d+'</td>';
                            row = row+'<td class="izquierda">'+art+'</td>';
                            row = row+'<td class="derecha">'+dato.m+'</td>';
                            row = row+'<td class="centro"><button type="button" onclick="viewDoc('+"'"+dato.a+"'"+','+"'"+dato.b+"'"+','+"'"+dato.c+"'"+','+"'"+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+"'"+')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></td>';
                            
                            row = row+'</tr>';
                        });
                        
                        row = row+'</tbody>';
                        row = row+'</table>';
                        
                        $('#tableDocPen').append(row);
                        $('#T_DocPen').dataTable();
                    }  
                    
                } else {
                    if(returnData.trim() === ''){
                        errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                    } else {  
                        errorGeneral("Sistema Intranet",returnData,'danger'); 
                    }
                }
                
                
            }
    });

}

function viewDoc(a,b,c,d){
    
    var ruc = $('#ruc_prov').text();
    var co_e = $('#cod_emp').val();
    var url1 = $('#txt_urlGe').val();
    var datos = "ruc="+ruc+"&co_e="+co_e+"&e_o="+a+"&p_n="+b+"&s_l="+c;
    
    $('#name_pro').html($('#name_prov').text());
    $('#num_doc_fac').html(a+"-"+b+"-"+c);
    
    if (ruc.trim() === ''){
        errorGeneral("Sistema Intranet",'No ha seleccionado un Proveedor','danger'); 
        return false;
    }
    
    newModal('ViewDoc','60','80','form_ViewDoc');
    $('#modalViewDoc').css('z-index','1042');
    $('#fadeViewDoc').css('z-index','1041');
    
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url1+'procesos/solicitud_cheque/co_109/retDoc',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();

            $('#cont_det_pro').empty();
            $('#cont_imp').empty();
            $('#cont_cab').empty();

            if (returnData.trim().substr(0,2) === "OK"){
                var data = eval(returnData.trim().substr(3));
                if (data){
                    $.each(data,function(e,dato){
                        var btn = '<button type="button" onclick="passDoc('+"'"+d+"'"+','+"'"+encodeURIComponent(JSON.stringify(dato.val_imp)).replace(/['"]/g,"-")+"'"+')" class="btn btn-success" style="float: right;">Procesar</button>';
                        $('#cont_cab').append(dato.cab);
                        $('#cont_det_pro').append(dato.pro);
                        $('#cont_imp').append(dato.imp);
                        $('#cont_total').html(dato.tot);
                        $('#btn_doc').html(btn);
                    });
                }
            } else {
                if(returnData.trim() === ''){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
        }
    });
}

function passDoc(data,arr_imp){
    
    data = decodeURIComponent(data);
    //console.log(data);
    arr_imp = decodeURIComponent(arr_imp);
    //console.log(arr_imp);

    var datos = $.parseJSON(data);
    var datos_impu = $.parseJSON(arr_imp);
    var sub = 0;
    var vari = 0;
    var val_imp = 0;
    
    $.each(datos_impu,function(e,dato){
        
        if (parseInt(dato.c) === 12){
            sub+=parseFloat(dato.f);
            $('#cmb_impu_doc').val('12');
        } else {
            vari+=parseFloat(dato.f);
        }
    });
    
    $('#txt_num_doc').val(datos.a+""+datos.b+""+datos.c);
    $('#txt_fec_doc').val(datos.f);
    $('#txt_subt_doc').val(sub);
    $('#txt_vari_doc').val(vari);
    $('#txt_aut_sri').val(datos.e);
    
    baseImp();
    totIva();

    quitarmodalGeneral('DocPen','form_DocPen');
    quitarmodalGeneral('ViewDoc','form_ViewDoc');
    
    baseImp();
    totIva();
}

function baseImp(){    
    
    var Tot = 0 , Bas = 0 ,TotDesc = 0 ,TotImp;
    var Subt = $('#txt_subt_doc').val().toString().trim() === ''?0:$('#txt_subt_doc').val();
    var Adic = $('#txt_adic_doc').val().toString().trim() === ''?0:$('#txt_adic_doc').val();
    var Porcdesc = $('#txt_porc_desc_doc').val().toString().trim() === ''?0:$('#txt_porc_desc_doc').val();
    TotDesc = (parseFloat(Subt) + parseFloat(Adic)) * parseInt(Porcdesc) / 100;
    $('#txt_desc_doc').val(TotDesc);
    Bas = parseFloat(Subt) + parseFloat(Adic) - parseFloat(TotDesc);
    $('#txt_base_imp_doc').val(Bas);
    var Vari = $('#txt_vari_doc').val().toString().trim() === ''?0:$('#txt_vari_doc').val();
    var PorcImp = $('#cmb_impu_doc').val().toString().trim() === ''?0:$('#cmb_impu_doc').val();
    TotImp = Bas * parseInt(PorcImp) /100;
    $('#txt_res_impu_doc').val(TotImp);
    Tot = parseFloat(Bas) + parseFloat(Vari) + TotImp;
    $('#txt_tot_doc').val(Tot);

}

function totIva(){    
    
    var tot = 0;
    var basImp = $('#txt_base_imp_doc').val().toString().trim() === ''?0:$('#txt_base_imp_doc').val();
    tot = parseFloat(basImp) * 12 /100;
    $('#txt_res_impu_doc').val(formato_numero((tot),2,".",","));

}

function passEmpr(a,b){
    
    $('#cod_emp').val(a);
    $('#id_emp').val(b);
    document.getElementById("f_solch").submit();
    
}

function passProv(data){
    
    data = decodeURIComponent(data);
    var d = $.parseJSON(data);
    var url1 = $('#txt_urlGe').val();
    var datos = "p_i="+d.plan_imp;
    var bandera = true;
    
    $.ajax({
        type : 'POST',
        async: false,
        url : url1+"procesos/solicitud_cheque/co_109/retPlaImp", 
        data: datos,
        success : function (returnData) { 
            if (returnData.trim().substr(0,2) === "OK"){
                var data = eval('('+returnData.trim().substr(3)+')');
                if (data){
                    $.each(data,function(e,da){
                        $('#cmb_plan_imp').append('<option value="'+da.b+'">'+da.a+'</option>');
                    });
                }
            } else {
                bandera = false;
                $('#cmb_plan_imp').append('<option value="NNN">No tiene plan de impuesto</option>');
            }
        }
    });
    
    if (bandera === false){
        var r = confirm("Este proveedor no tiene plan de impuestos y deberia comunicarlo a contabilidad \n\nDesea de todos modos utilizarlo ? ");
        if (r == false) { 
            return false;
        } 
    }
        
    $('#ruc_prov').text(d.ruc_prov); 
    $('#name_prov').text(d.nom_prov); 
    $('#dir_prov').text(d.dir_prov); 
    $('#bene_prov').text(d.bene_prov); 
    $('#tip_prov').val(d.tip_prov); 
        
    exitModal();
    
}

function pass_cuent(k){
    
    var a = $('#cmb_tipo_cont').val();
    var b = $('#cmb_cen_cost').val();
    var c = $('#tip_cuent').val();
    var d = $('#cmb_cuentas').val();
    var d_d = $('#cmb_cuentas :selected').text();
    var e = $('#cmb_activoPasivo').val();
    var e_d = $('#cmb_activoPasivo :selected').text();
    var f = $('#txt_mont_1').val();
    var g = $('#txt_mont_2').val();
    var l = $('#txt_tot_doc').val();

    var txt = '';
    
    if(c.toString().trim() === '1'){
        
        var p = l+'|'+'Valor de la Factura'+':';
        p = p + b+'|'+'Centro de Costo'+':';
        p = p + d+'|'+'Cuenta'+':';
        p = p + f+'|'+'Monto';
        
        if(!isEmptySis(p)){
            return false;
        }
        
    } else if (c.toString().trim() === '2'){
        
        var p = l+'|'+'Valor de la Factura'+':';
        p = p + e+'|'+'Cuenta'+':';
        p = p + g+'|'+'Monto';
        
        if(!isEmptySis(p)){
            return false;
        }
        
    }
    
    var h = c.toString().trim() === '1'?d:e;
    var i = c.toString().trim() === '1'?f:g;
    var j = c.toString().trim() === '1'?d_d:e_d;
    
    if(k === 'M'){
        
        if(parseFloat(i) > parseFloat(l)){
            errorGeneral("Sistema Intranet",'El monto ingresado es mayor al valor de la factura.','danger'); 
            return false;
        }
        
        $('.mont_dis').html(i);
        
        dis_m2(h);
        
        return false;
        
    } else if(k === 'C'){
        
        if(parseFloat(i) > parseFloat(l)){
            errorGeneral("Sistema Intranet",'El monto ingresado es mayor al valor de la factura.','danger'); 
            return false;
        }
        
        $('.mont_dis').html(i);
        
        varCeCos(h);
        
        return false;
        
    }
        i_count_ge++;
        
        txt = txt +'<tr id="optionCuenta-'+i_count_ge+'">';
        txt = txt +'<td id="id_cuent">'+h+'</td>';
        txt = txt +'<td class="centro">'+j+'</td>';
        txt = txt +'<td id="tipo_cuent" class="centro">'+a+'</td>';
        txt = txt +'<td id="val_cuent" class="derecha">'+i+'</td>';
        txt = txt +'<td class="centro"><button type="button" onclick="removeOptionCuentas('+i_count_ge+','+i+')" class="btn btn-circle btn-danger"><i class="text-blanco fa fa-minus"></i></button></td>';
        txt = txt +'</tr>';
        
        total_ge = parseFloat(total_ge) + parseFloat(i);
        
    var tableTotal = '<tr>';
        tableTotal = tableTotal +'<td colspan="3">Total</td>';
        tableTotal = tableTotal +'<td class="derecha">'+total_ge+'</td>';
        tableTotal = tableTotal +'<td></td>';
        tableTotal = tableTotal +'</tr>';
        $('#cuenta_total').html(tableTotal);
        $('#table_cuentas').append(txt);
        
}

function calc_m2(p){
    
    var dis = $('#cmb_for_dis').val();
    if(dis !== 'NNN') {
        var a = [];
        var aa = [];
        var alm = null;
        var tt=0,oo=0,qq=0,rr=0,porc=0,mont=0;

        alm = $("input[name='opt_cnt_dis[]']:checked");

        $(alm).each( function () {
            var arr = $.parseJSON(decodeURIComponent($(this).attr('data-label')));
            var i_s = $(this).attr('id');
            i_s = i_s.replace('opt_','');
            var val_porc = $('#porc_'+i_s).val();
            val_porc = val_porc === ''?0:val_porc;
            arr.i = val_porc;
            a.push(arr);
            tt+=parseFloat(arr.e);
            rr+=parseFloat(val_porc);
            
        });

        if(!$.isEmptyObject(a)) {
            
            if(dis === 'P' && rr !== 100){
                errorGeneral("Sistema Intranet",'El total de porcentaje de los almacenes seleccionados debe de ser igual al 100%.Actualmente la suma del porcentaje es de <b>'+rr+'%</b>','danger'); 
                return false;
            }
            
            $('.acept_dis').show();
            $('.out_fa').hide();

            $('#tabledisxm2').empty();
            var row = '<table class="table table-bordered TFtable">';
                row = row +'<thead>';
                row = row +'<th>Grupo</th>';
                row = row +'<th>Centro</th>';
                row = row +'<th>Cuenta</th>';
                row = row +'<th>%</th>';
                row = row +'<th>Monto</th>';
                row = row +'</thead>';
                row = row +'<tbody>';

            $.each(a,function(e,dt){
                
                if(dis === 'M'){
                    porc = (dt.e/tt)*100;
                    mont = (parseFloat(p)*porc)/100;
                } else {
                    porc = dt.i;
                    mont = (parseFloat(p)*porc)/100;
                }
                oo+=parseFloat(mont);
                qq+=parseFloat(porc);
                
                if(mont !== 0 && porc !== 0){
                    dt.i = porc;
                    dt.j = mont;
                    row = row+'<tr>';
                    row = row+'<td>'+dt.f+'</td>';
                    row = row+'<td>'+dt.g+'</td>';
                    row = row+'<td>'+dt.b+' '+dt.c+' '+dt.d+' - '+dt.h+'</td>';
                    row = row+'<td class="derecha">'+formato_numero((porc),2,".",",")+'</td>';
                    row = row+'<td class="derecha">'+formato_numero((mont),2,".",",")+'</td>';
                    row = row+'</tr>';
                    aa.push(dt);
                }
            });
            row = row+'<tr>';
            row = row+'<td colspan="3"><strong>Totales</strong></td>';
            row = row+'<td class="derecha"><strong>'+formato_numero((qq),2,".",",")+'</strong></td>';
            row = row+'<td class="derecha"><strong>'+formato_numero((oo),2,".",",")+'</strong></td>';
            row = row+'</tr>';

            row = row +'</tbody>';
            row = row +'</table>';
            
            row = row +'<button type="button" class="btn btn-primary pull-right" onclick="acept_dis('+"'"+encodeURIComponent(JSON.stringify(aa)).replace(/['"]/g,"-")+"'"+','+"'M'"+')"><i class="text-red fa fa-chevron-left"></i> Aceptar</button>';

            $('#tabledisxm2').append(row);

        } else {
            errorGeneral("Sistema Intranet",'Seleccione por lo menos un centro de costo.','danger'); 
        }
    } else {
        errorGeneral("Sistema Intranet",'Seleccione la forma de distribuci&oacute;n.','danger'); 
    }
    
}

function calc_cc(p){
    
    var a = [];
    var aa = [];
    var tt=0,oo=0;
    
    for (var i = 1; i <= countCecos; i++){
        if($('#cecos_'+i).val() !== ''){
            var arr = $.parseJSON(decodeURIComponent($('#cecos_'+i).attr('data-label')));
            arr.g = $('#cecos_'+i).val();
            a.push(arr);
            tt+=parseFloat($('#cecos_'+i).val());
        }       
    }
    
    if(parseFloat(p) !== parseFloat(tt)){
        errorGeneral("Sistema Intranet",'El monto total de cuentas seleccionadas debe de ser igual al monto a distribuir.<br> Actualmente la suma total es de <b>'+tt+'</b>','danger'); 
        return false;
    }
    
    if(!$.isEmptyObject(a)) {
        $('.out_fa').hide();
        $('#tablecencos').empty();
        var row = '<table class="table table-bordered TFtable">';
            row = row +'<thead>';
            row = row +'<th>Centro</th>';
            row = row +'<th>Cuenta</th>';
            row = row +'<th>Monto</th>';
            row = row +'</thead>';
            row = row +'<tbody>';

        $.each(a,function(e,dt){
            oo+=parseFloat(dt.g);
            dt.h = dt.f;
            dt.j = dt.g;
            row = row+'<tr>';
            row = row+'<td>'+dt.e+'</td>';
            row = row+'<td>'+dt.b+' '+dt.c+' '+dt.d+' - '+dt.f+'</td>';
            row = row+'<td class="derecha">'+dt.g+'</td>';
            row = row+'</tr>';
            aa.push(dt);
        });
        
        row = row+'<tr>';
        row = row+'<td colspan="2"><strong>Totales</strong></td>';
        row = row+'<td class="derecha"><strong>'+formato_numero((oo),2,".",",")+'</strong></td>';
        row = row+'</tr>';

        row = row +'</tbody>';
        row = row +'</table>';

        row = row +'<button type="button" class="btn btn-primary pull-right" onclick="acept_dis('+"'"+encodeURIComponent(JSON.stringify(aa)).replace(/['"]/g,"-")+"'"+','+"'C'"+')"><i class="text-red fa fa-chevron-left"></i> Aceptar</button>';

        $('#tablecencos').append(row);

    } else {
        errorGeneral("Sistema Intranet",'Seleccione por lo menos un centro de costo.','danger'); 
    }
    
}

function acept_dis(d,t){
    var b = $.parseJSON(decodeURIComponent(d));
    
    var txt = '';
    
    $.each(b,function(e,dt){
        i_count_ge++;
        total_ge = parseFloat(total_ge) + parseFloat(dt.j);
        
        txt = txt +'<tr id="optionCuenta-'+i_count_ge+'">';
        txt = txt +'<td id="id_cuent">'+dt.b+'-'+dt.c+'-'+dt.d+'</td>';
        txt = txt +'<td class="centro">'+dt.h+'</td>';
        txt = txt +'<td id="tipo_cuent" class="centro">'+$('#cmb_tipo_cont').val()+'</td>';
        txt = txt +'<td id="val_cuent" class="derecha">'+formato_numero((dt.j),2,".",",")+'</td>';
        txt = txt +'<td class="centro"><button type="button" onclick="removeOptionCuentas('+i_count_ge+','+dt.j+')" class="btn btn-circle btn-danger"><i class="text-blanco fa fa-minus"></i></button></td>';
        txt = txt +'</tr>';
        
    });
        
    var tableTotal = '<tr>';
        tableTotal = tableTotal +'<td colspan="3">Total</td>';
        tableTotal = tableTotal +'<td class="derecha">'+formato_numero((total_ge),2,".",",")+'</td>';
        tableTotal = tableTotal +'<td></td>';
        tableTotal = tableTotal +'</tr>';
        $('#cuenta_total').html(tableTotal);
        $('#table_cuentas').append(txt);
    
    if(t==='M'){
        quitarmodalGeneral('DisM2','form_DisM2');
    } else{ 
        quitarmodalGeneral('CenCos','form_CenCos')
    }
}

function dis_m2(a){
    
    var count = 0;
    var url1 = $('#txt_urlGe').val();
    $('.out_fa').show();
    var datos = "c_m="+a;
    
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/solicitud_cheque/co_109/retDisxm2',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();

            if (returnData.trim().substr(0,2) === "OK"){
                
                $('.acept_dis').hide();
                newModal('DisM2','60','60','form_DisM2');
                
                var data = eval(returnData.trim().substr(3));
                if (data){ 
                    
                    $('#tabledisxm2').empty();
                    
                    var row = '<table class="table table-bordered TFtable">';
                    row = row +'<thead>';
                    row = row +'<th>Grupo</th>';
                    row = row +'<th>Centro</th>';
                    row = row +'<th>Cuenta</th>';
                    row = row +'<th>m2</th>';
                    row = row +'<th width="30px">%</th>';
                    row = row +'<th><center><input type="checkbox" name="ck_apro" id="ck_apro"/></center></th>';
                    row = row +'</thead>';
                    row = row +'<tbody>';
                    
                    $.each(data,function(e,dato){
                        count++;
                        row = row+'<tr>';
                        row = row+'<td>'+dato.f+'</td>';
                        row = row+'<td>'+dato.g+'</td>';
                        row = row+'<td>'+dato.b+' '+dato.c+' '+dato.d+' - '+dato.h+'</td>';
                        row = row+'<td class="derecha">'+formato_numero((dato.e),2,".",",")+'</td>';
                        row = row+'<td><input type="text" id="porc_'+count+'" onkeyPress="return isNumero(event,this)" style="width: 65px;text-align: center;font-weight: bold;"></td>';
                        row = row+'<td><center><input type="checkbox" id="opt_'+count+'" data-label="'+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+'" name="opt_cnt_dis[]"></center></td>';
                        row = row+'</tr>';
                    });

                    row = row +'</tbody>';
                    row = row +'</table>';
                    
                    $('#tabledisxm2').append(row);
                    cambia();
                }  

            } else {
                if(returnData.trim() === ''){
                    errorGeneral("Sistema Intranet",'No hay almac&eacute;nes para distribuir','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
                
        }
    });
}

function cambia(){
    
    var i = 1;
    $("#ck_apro").button().click(function(){
        if(i === 1){
            $("input[name='opt_cnt_dis[]']").prop('checked', true);
            i++;
        } else if(i === 2){
            $(this).prop('checked', false);
            $("input[name='opt_cnt_dis[]']").prop('checked', false);
            i = 1;
        }
    });
    
}

function varCeCos(a){
    var url1 = $('#txt_urlGe').val();
    var datos = "c_m="+a;
    countCecos = 0;
    dataLoading();
    
    $.ajax({
        type : 'POST',
        url : url1+'procesos/solicitud_cheque/co_109/retDisCeCos',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();

            if (returnData.trim().substr(0,2) === "OK"){
                
                //$('.acept_dis').hide();
                newModal('CenCos','60','60','form_CenCos');
                
                var data = eval(returnData.trim().substr(3));
                if (data){ 
                    
                    $('#tablecencos').empty();
                    
                    var row = '<table class="table table-bordered TFtable">';
                    row = row +'<thead>';
                    row = row +'<th>Centro</th>';
                    row = row +'<th>Cuenta</th>';
                    row = row +'<th>Monto</th>';
                    row = row +'</thead>';
                    row = row +'<tbody>';
                    
                    $.each(data,function(e,dato){
                        countCecos++;
                        row = row+'<tr>';
                        row = row+'<td>'+dato.e+'</td>';
                        row = row+'<td>'+dato.b+' '+dato.c+' '+dato.d+' - '+dato.f+'</td>';
                        row = row+'<td><input type="text" id="cecos_'+countCecos+'" data-label="'+encodeURIComponent(JSON.stringify(dato)).replace(/['"]/g,"-")+'" onkeyPress="return isNumero(event,this)" style="width: 65px;text-align: center;font-weight: bold;"></td>';
                        row = row+'</tr>';
                    });

                    row = row +'</tbody>';
                    row = row +'</table>';
                    
                    $('#tablecencos').append(row);
                    cambia();
                }  

            } else {
                if(returnData.trim() === ''){
                    errorGeneral("Sistema Intranet",'No hay almac&eacute;nes para distribuir','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
                
        }
    });
}

function removeOptionCuentas(id,cant) {
    
    if(confirm('Esta seguro de que desea eliminar este registro ?')){ 
        $('#optionCuenta-'+id).remove();
        total_ge = parseFloat(total_ge)-parseFloat(cant);
        var tableTotal = '<tr>';
        tableTotal = tableTotal +'<td colspan="3">Total</td>';
        tableTotal = tableTotal +'<td class="derecha">'+total_ge+'</td>';
        tableTotal = tableTotal +'<td></td>';
        tableTotal = tableTotal +'</tr>';
        $('#cuenta_total').html(tableTotal);
    }
    
}

function pasteModal() {
    newModal('Paste','40','60','form_Paste');
}

function convert(){
    
    var xl = $('#textarea_copy').val().trim();
    if (xl.trim() !== ''){
        
        var string = '[["';
            string = string + xl.replace(/\n+$/i,'').replace(/\n/g,'"],["').replace(/\t/g,'","');
            string = string + '"]]';

        var json_e = eval(string);
        var table = '';
        var cuentas = '';
        var cuentArray = [];
        var countCol = Object.keys(json_e[0]).length;
        
        if(countCol != '4'){
            console.log(countCol);
            console.log('count');
            errorGeneral("Sistema Intranet",'Solo puede ingresar 4 columnas.','danger'); 
            return false;
        }
        
        var url1 = $('#txt_urlGe').val();
        var datos = "c_e="+$("#cod_emp").val()+"&t_s=OK";
        
        dataLoading();
        
        $.ajax({
            type : 'POST',
            async: false,
            url : url1+"procesos/solicitud_cheque/co_109/retCuentas", 
            data: datos,
            success : function (returnData) { 
                cuentas = returnData;        
            }
        });
        
        var jcuent = eval(eval('('+cuentas+')'));
        
        if (jcuent.cuentas !== null){
            
            $.each(jcuent.cuentas,function(e,da){
                var obj = da.cta_myor.toString().trim()+""+da.codi_ceco.toString().trim()+""+da.cta_scta.toString().trim();
                cuentArray.push(obj);
            });

            $.each(json_e,function(e,dato){

                var valCuent = dato[0].toString().trim().replace(/[-]/g,"");
                if ($.inArray(valCuent, cuentArray) < 0) {

                    errorGeneral("Sistema Intranet",'La cuenta "'+dato[0]+'" no es valida o no pertenece a esta empresa, por favor verifique la cuenta','danger'); 
                    removedataLoading();
                    return false; 

                } else {

                    if(!isNaN(dato[3])){

                        i_count_ge++;
                        table = table +'<tr id="optionCuenta-'+i_count_ge+'">';
                        table = table +'<td>'+dato[0]+'</td>';
                        table = table +'<td class="centro">'+dato[1]+'</td>';
                        table = table +'<td class="derecha">'+dato[2]+'</td>';
                        table = table +'<td class="derecha">'+dato[3]+'</td>';
                        total_ge+=parseFloat(dato[3]);
                        table = table +'<td class="centro"><button type="button" onclick="removeOptionCuentas('+i_count_ge+','+dato[3]+')" class="btn btn-circle btn-danger"><i class="text-blanco fa fa-minus"></i></button></td>';
                        table = table +'</tr>';

                    } else {

                        errorGeneral("Sistema Intranet",'Solo puede ingresar valores en la columna "Valor", se ha detenido en la fila '+i_count_ge+'. ','danger'); 
                        removedataLoading();
                        return false; 

                    }

                }

            });

        var tableTotal = '<tr>';
            tableTotal = tableTotal +'<td colspan="3">Total</td>';
            tableTotal = tableTotal +'<td class="derecha">'+total_ge+'</td>';
            tableTotal = tableTotal +'<td></td>';
            tableTotal = tableTotal +'</tr>';
            $('#table_cuentas').append(table);
            $('#cuenta_total').html(tableTotal);
            removedataLoading();
            quitarmodalGeneral('Paste','form_Paste');
            
        } else {
            
            errorGeneral("Sistema Intranet",'Empresa no tiene cuentas disponibles','danger'); 
            removedataLoading();
            return false;
            
        }
        
    } else {
        errorGeneral("Sistema Intranet",'Campo de dato se encuentra vacio.','danger'); 
        return false;
    }
    
}

function validaporc100(){
    
    var suma = 0;
    var irSum = parseInt($('#txt_por_plg').val().toString().trim());
    
    for (var a = 1; a <= i_count_plan_pago; a++){
        if (document.getElementById('option[por]['+a+']')){
            suma = document.getElementById('option[por]['+a+']').innerHTML;
            irSum+=parseInt(suma.toString().trim());
        }
    }
    if (parseInt(irSum) > 100){
        return false;
    } else {
        return true;
    }
    
}

function addOptionPlanPago(){
    
    if ($('#txt_fec_plg').val().toString().trim() === ''){
        errorGeneral('Sistema Intranet','No ha ingresado al Fecha de Pago','danger');
        return false;
    }
    
    if ($('#txt_por_plg').val().toString().trim() === ''){
        errorGeneral('Sistema Intranet','No ha ingresado el Porcentaje de Pago','danger');
        return false;
    }
    
    if(!validaporc100()){
        errorGeneral('Sistema Intranet','La Forma de pago debe sumar 100%','danger');
        return false;
    }
    
    i_count_plan_pago++;
    
    if (i_count_plan_pago === 1){
        $('#table_plan_pago').empty();
    }
    
    var tab = '';
    tab = tab+'<tr id="option-'+i_count_plan_pago+'">';
    tab = tab+'<td><p class="fec_pla_pa" id="option[fec]['+i_count_plan_pago+']">'+$('#txt_fec_plg').val()+'</p></td>';
    tab = tab+'<td><p class="por_pla_pa" id="option[por]['+i_count_plan_pago+']">'+$('#txt_por_plg').val()+'</p></td>';
    tab = tab+'<td class="centro">';
    tab = tab+'<button type="button" onclick="removeOptionPlanPago('+i_count_plan_pago+')" class="btn btn-circle btn-danger add_option"><i class="text-blanco fa fa-minus"></i></button>';
    tab = tab+'</td>';
    tab = tab+'</tr>';
    
    $('#table_plan_pago').append(tab);
    $('#txt_fec_plg').val('');
    $('#txt_por_plg').val('');
    
}

function removeOptionPlanPago(id) {
    
    if(confirm('Esta seguro de que desea eliminar este registro ?')){ 
        $('#option-'+id).remove();
    }
    
}

function saveSolChe(){
    
    var ruc = $('#ruc_prov').text();
    var tip_prov = $('#tip_prov').val();
    var nom_prov = $('#name_prov').text();
    var dir_prov = $('#dir_prov').text();
    var nom_bene = $('#bene_prov').text();
    var ord_comp = $('#txt_num_oc').val();
    var tip_doc = $('#cmb_tip_doc').val();
    var num_doc = $('#txt_num_doc').val();
    var aut_sri = $('#txt_aut_sri').val();
    var fec_doc = $('#txt_fec_doc').val();
    var tip_egr = $('#cmb_tip_egre').val();
    var con_doc = $('#txt_concep_doc').val();
    var obs_doc = $('#txt_obs_doc').val();
    var pla_imp = $('#cmb_plan_imp').val();
    var sub_sol = $('#txt_subt_doc').val();
    var adic_sol = $('#txt_adic_doc').val();
    var porc_desc_sol = $('#txt_porc_desc_doc').val();
    var val_desc_sol = $('#txt_desc_doc').val();
    var base_imp_sol = $('#txt_base_imp_doc').val();
    var vari_sol = $('#txt_vari_doc').val();
    var imp_sol = $('#cmb_impu_doc').val();
    var val_imp_sol = $('#txt_res_impu_doc').val();
    var tot_sol = $('#txt_tot_doc').val();
    var aut = 9999;
    
    var json_cabecera = [];
    var json_cuentas = [];
    var json_plan_pago = [];
    var url1 = $('#txt_urlGe').val();
    
    var a = ruc+'|'+'Ruc'+':';
    a = a + tip_doc+'|'+'Tipo de Documento'+':';
    a = a + num_doc+'|'+'Numero de Documento'+':';
    a = a + fec_doc+'|'+'Fecha de Documento'+':';
    a = a + tip_egr+'|'+'Tipo de Egreso'+':';
    a = a + con_doc+'|'+'Concepto de Documento'+':';
    a = a + tot_sol+'|'+'Total'+':';
    a = a + i_count_ge+'|'+'Debe ingresar por lo menos una cuenta contable';
    
    var suma = 0;
    
    for (var p = 1; p <= i_count_plan_pago; p++){
        if (document.getElementById('option[por]['+p+']')){
            suma+=parseInt(document.getElementById('option[por]['+p+']').innerHTML);
        }
    }
    
    if (parseInt(suma) != 100){
        errorGeneral('Sistema Intranet','La Forma de pago debe sumar 100%','danger');
        return false;
    } 
    
    if(isEmptySis(a)){
        
        //txt_sin_aprob
        if (!$("#txt_sin_aprob").is(':checked')) {
            if (!$("input[name='cod_autoriza']").is(':checked')) {
                errorGeneral('Sistema Intranet','Debe Seleccionar 1 grupo de autorizadores','danger');
                return false;
            } else {
                aut = $('input:radio[name=cod_autoriza]:checked').attr('id');
            }
        }
        
        $('#plan_de_pago tr').each(function (){

            var item = {};
            var fec = $(this).find('.fec_pla_pa').html();
            var por = $(this).find('.por_pla_pa').html();

            if(fec != null && por != null){
                
                item["fec"] = fec;            
                item["por"] = por;

                json_plan_pago.push(item); 

            }

        });

        $('#cuenta_contable tr').each(function (){

            var item = {};
            var id = $(this).find('#id_cuent').html();
            var tip = $(this).find('#tipo_cuent').html();
            var val = $(this).find('#val_cuent').html();

            if(id != null && tip != null && val != null){
                
                item["id"] = id;            
                item["tip"] = tip;
                item["val"] = val;

                json_cuentas.push(item); 

            }

        });
        
        var item = {};
        item["ruc"]=ruc;
        item["num_ord_comp"]=ord_comp;
        item["tip_prov"]=tip_prov;
        item["nom_prov"]=nom_prov;
        item["dir_prov"]=dir_prov;
        item["nom_bene"]=nom_bene;
        item["tip_doc"]=tip_doc;
        item["num_doc"]=num_doc;
        item["fec_doc"]=fec_doc;
        item["aut_sri"]=aut_sri;
        item["tip_egr"]=tip_egr;
        item["con_doc"]=con_doc;
        item["obs_doc"]=obs_doc;
        item["pla_imp"]=pla_imp;
        item["sub_sol"]=sub_sol;
        item["adic_sol"]=adic_sol;
        item["porc_desc_sol"]=porc_desc_sol;
        item["val_desc_sol"]=val_desc_sol;
        item["base_imp_sol"]=base_imp_sol;
        item["vari_sol"]=vari_sol;
        item["imp_sol"]=imp_sol;
        item["val_imp_sol"]=val_imp_sol;
        item["tot_sol"]=tot_sol;
        item["cod_gru_aut"]=aut;
        if($("#txt_urgente").is(':checked')){item["cod_urg"]=1;} else {item["cod_urg"]=0;}
        if($("#txt_copia").is(':checked')){item["cod_cop"]=1;} else {item["cod_cop"]=0;}
        
        json_cabecera.push(item);
        
        dataLoading();
        
        var datos = 'c_a='+JSON.stringify(json_cabecera);
        datos = datos +'&p_g='+JSON.stringify(json_plan_pago);
        datos = datos +'&c_s='+JSON.stringify(json_cuentas);
        
        $.ajax({
            type : 'POST',
            url : url1+'procesos/solicitud_cheque/co_109/saveSolc',
            data: datos,
            success : function (returnData) {

                removedataLoading();
                
                var data = eval('('+returnData+')');

                if (data){

                    $.each(data,function(e,dat){
                        if (dat.a === 0){
                            errorGeneral("Sistema Intranet",dat.b,'success');
                            $('#form_solicitudes_ge')[0].reset();
                            $('#cuenta_total').html('');
                            $('#table_cuentas').html('');
                            $('#table_plan_pago').html('');
                            i_count_plan_pago = 0;
                            i_count_ge = 0;
                            return false;
                        } else {
                            errorGeneral("Sistema Intranet",dat.b,'danger');
                            return false;
                        }
                    });

                }

            }
        });
    }
    
}

function consulOrdCmp(a,b,c){
    
    $('#cmb_plan_imp').empty();
    dataLoading();
    var url1 = $('#txt_urlGe').val();
    var datos = "c_e="+a+"&c_n_p="+b+"&n_o="+c;
    $.ajax({
        type : 'POST',
        url : url1+'procesos/solicitud_cheque/co_109/retOc',
        data: datos,
        success : function (returnData) {
            
            removedataLoading();

            if (returnData.trim().substr(0,2) === "OK"){
                var data = eval('('+returnData.trim().substr(3)+')');
                
                if (data.ord_com != null){
                    
                    $.each(data.ord_com,function(e,dat){
                        $('#ruc_prov').text(dat.b); 
                        $('#name_prov').text(dat.c); 
                        $('#dir_prov').text(dat.d); 
                        $('#bene_prov').text(dat.g);
                        $('#txt_subt_doc').val(dat.f);
                        $("#cmb_impu_doc").val(dat.j);
                    });
                    
                    baseImp();
                    totIva();
                    
                } else {
                    limpiarDatos();
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger');
                    return false;
                }
                
                if (data.pla_imp){
                    $.each(data.pla_imp,function(e,da){
                        $('#cmb_plan_imp').append('<option value="'+da.b+'">'+da.a+'</option>');
                    });
                }
                
            } else {
                if(returnData.trim() === ''){
                    errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger'); 
                } else {  
                    errorGeneral("Sistema Intranet",returnData,'danger'); 
                }
            }
        }
    });
}

function limpiarDatos(){
    $('#ruc_prov').html('');
    $('#tip_prov').val('');
    $('#name_prov').html('');
    $('#dir_prov').html('');
    $('#bene_prov').html('');
    $('#form_solicitudes_ge')[0].reset();
    $('#cuenta_total').html('');
    $('#table_cuentas').html('');
    $('#table_plan_pago').html('');
    i_count_plan_pago = 0;
    i_count_ge = 0;
}

$(function() {
    var fecha = retFechaActual(2);
    
    $('.txt_aut_sri_fa').hide();
    
    $('#T_regProveed').dataTable();
    
    $(".convertText" ).button().click(function(){
        convert();
    });
    
    $(".searchProvee" ).button().click(function(){
        newModal('Provee','60','70','form_Provee');
    });
    
    $(".filtroProvee" ).button().click(function(){
        getProveedor();
    });

    $("#other" ).click(function() {
        $( "#target" ).submit();
    });
    
    $("._dis_almc" ).click(function() {
        calc_m2($('.mont_dis').html());
    });
    
    $("._dis_cecos" ).click(function() {
        calc_cc($('.mont_dis').html());
    });
    
    $(".pass_cuent" ).click(function() {
        pass_cuent('');
    });
    
    $(".doc_pen" ).click(function() {
        getDocPen();
    });
    
    $(".add_option_pago" ).click(function() {
        addOptionPlanPago();
    });
    
    $(".save_solche").click(function() {
        saveSolChe();
    });
    
    $('#txt_fec_doc').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('#txt_fec_plg').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        minDate:fecha,
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $('#cmb_cuentas').select2({ 
        width: '100%',
        placeholder: 'Seleccionar Cuenta'
    });
    
    $('#cmb_activoPasivo').select2({ 
        width: '100%',
        placeholder: 'Seleccionar Cuenta'
    });
    
    $('#txt_subt_doc').on("input", function(event) {
        baseImp();
    });
    
    $('#txt_adic_doc').on("input", function(event) {
        baseImp();
    });
    
    $('#txt_porc_desc_doc').on("input", function(event) {
        baseImp();
    });
    
    $('#txt_vari_doc').on("input", function(event) {
        baseImp();
    });
    
    $(".pass_dis_m2" ).click(function() {
        pass_cuent('M');
    });
    
    $(".pass_var_cc" ).click(function() {
        pass_cuent('C');
    });
    
    $("#cmb_impu_doc").change(function(event) {
        if (this.value.toString().trim() === '12'){
            
            var res = 0;
            
            $('#txt_res_impu_doc').val(res);
            totIva();
            baseImp();
            
        } else {
            baseImp();
        }
    });
    
    $("#cmb_tip_doc").change(function() {
        if (this.value.trim() === '58' || this.value.trim() === '59' || this.value.trim() === '60'){
            $('.txt_aut_sri_fa').show();
        } else {
            $('.txt_aut_sri_fa').hide();
        }
    });    
    
    $("#txt_sin_aprob").change(function() {
        if(this.checked) {
            $("input[name='cod_autoriza']").attr('disabled',true);
        } else {
            $("input[name='cod_autoriza']").attr('disabled',false);
        }
    });
    
}); 
