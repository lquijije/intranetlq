
function autDeleg(){
    
    var item = 'NNN';
    var url1 = $('#txt_urlGe').val();
    var aprobados = null;

    aprobados = $("input[name='op_dele[]']:checked");

    $(aprobados).each( function () {
        item = parseInt($(this).val());
    });
    
    if(va_sess()){
        var r = confirm("Desea delegar al siguiente colaborador.");
        if (r == true) {  
            var datos = "c_d="+item;
            dataLoading();
            $.ajax({
                type : 'POST',
                url : url1+"procesos/solicitud_cheque/co_1101/seDelg", 
                data: datos,
                success : function (retData){
                    removedataLoading();
                    var d = eval('('+retData+')');
                    if(d.err == ''){
                        if(parseInt(d.data.co_err) == 0){
                            errorGeneral("Sistema Intranet",'Registro actualizado correctamente','success');
                        } else {
                            errorGeneral("Sistema Intranet",d.data.tx_err,'danger');
                        }
                    } else {
                        errorGeneral("Sistema Intranet",d.err,'danger');
                    }
                }
            });
        }
    } else {
        redirectIntranet();
    }
}

$(document).ready(function() {
    $(".a_sol" ).click(function(){
        autDeleg();
    });
}); 