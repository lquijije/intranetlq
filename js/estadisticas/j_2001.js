
$(document).ready(function() {
    $('.form_datetime').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        timepicker:false,
        closeOnDateSelect:true,
        maxDate:$('#txt_fec').val()?$('#txt_fec').val():false
    });
}); 