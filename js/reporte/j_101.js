$(function() {
    retBarChart();
    
    $('#txt_fec_ini').datetimepicker({
        format:'Y/m/d',
        lang:'es',
        timepicker:false,
        closeOnDateSelect:true
    });
    
    $( ".search_chart" ).button().click(function(){
        retBarChart();
    });
    
    $("<div class='tooltip-inner' id='chart-tooltip'></div>").css({
        position: "absolute",
        display: "none",
        opacity: 0.8
    }).appendTo("body");
    
}); 

function retBarChart(){
    var url  = $('#txt_urlGe').val();
    var datos = "c_b="+$('#cmb_bodega').val();
        datos = datos + "&f_i="+$('#txt_fec_ini').val();
    dataLoading();
    $.ajax({
        type : 'POST',
        url : url+"reporte/co_101/retChart", 
        data: datos,
        success : function (datos) {

            removedataLoading();
            $('#tableChart').empty();
            var arreglo = datos.split('/*/');

            if(datos.trim() !== ''){
                $('#placeholder').empty();
                var data = eval(arreglo[0]);
                var options = {
                    xaxis: {
                        min: 0,
                        max: eval(arreglo[1]).length+1,
                        mode: null,
                        ticks: eval(arreglo[1])
                    }, yaxis: {
                        tickDecimals: 0,
                        axisLabelUseCanvas: true
                    }, grid: {
                        hoverable: true,
                        clickable: false,
                        borderWidth: 1
                    }, series: {
                        shadowSize: 1,
                        bars: {
                            show: true,
                            barWidth: 0.075,
                            align: "center",
                            order: 1
                        }
                    }
                };
                $.plot($("#placeholder"), data, options);
                 $("#placeholder").showTool();
                var string = '<ul class="list-unstyled" style="font-size:14px;">';
                var suma = 0;
                var p = '<table class="table"><tbody>';
                $.each(eval(arreglo[0]),function(e,a){
                    p = p+'<tr>';
                    p = p+'<td width ="1%" class="centro" style="border-top: 1px solid '+a.bars.fillColor+';"><i class="fa fa-square" style="color:'+a.bars.fillColor+';"></i></td>';
                    var i=0;
                    var color = '';
                    $.each(a.data,function(e,b){
                        if (i%2 === 0){
                            color ='#DDD';
                        } else {
                            color ='#FFF';
                        }
                        suma+=a.data[i][1];
                        p = p+'<td width ="2%" class="centro" style="border-top: 1px solid '+a.bars.fillColor+';background:'+color+'">'+a.data[i][1]+'</td>';
                        i++;

                    });
                    string = string+'<li><i class="fa fa-square" style="color:'+a.bars.fillColor+';"></i> '+a.label+' <b>Total:'+suma+'</b></li>';
                    suma=0;
                    p = p+'<td width="1%" style="border-top:none"></td></tr>';
                });

                p = p+'</tbody></table>';
                p = p+'<div>'+string+'</div>';
                $('#tableChart').append(p);
            } else {
                $('#placeholder').empty();
                var row = '<div style="margin-left: auto;margin-right: auto; width: 300px; text-align: center;padding-top: 130px;">';
                    row = row+'<h1 id="textChart" style="font-weight: bold;color:#C9C9C9;">No hay datos en la busqueda realizada</h1>';
                    row = row+'<i class="fa fa-bar-chart" style="color:#DCDCDC;font-size: 10em;"></i>';
                row = row+'</div>';
                $('#placeholder').append(row);
                errorGeneral("Sistema Intranet",'No hay datos en la busqueda realizada','danger');
            }
        }
    });
}


$.fn.showTool = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (item) {
            var y = item.datapoint[1].toFixed(2);
            $("#chart-tooltip").html(parseInt(y))
                .css({top: item.pageY + 5, left: item.pageX + 5})
                .fadeIn(200);
        } else {
            $("#chart-tooltip").hide();
        }
    });
}


