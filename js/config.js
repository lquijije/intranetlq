var conf_hd = {
    url : $('#txt_urlGe').val(),
    sol : [
        {
            type :'R',
            name :'Requerimiento',
            bg: 'modal-header menu-color-1',              
            color: 'text-blanco '
        },
        {
            type :'T',
            name :'Ticket',
            bg: 'modal-header menu-color-4',              
            color: 'text-blanco '
        }
    ],
    tx : {
        t_1 : 'Sistema Intranet',
        t_2 : 'Sin datos en la busqueda realizada',
        t_3 : 'Debe seleccionar las solicitudes.',
        t_4 : 'No hay empresas disponibles',
        t_5 : 'Registro grabado correctamente',
        t_6 : 'No tiene nada pendiente',
        t_7 : 'Parametros insuficientes',
        t_8 : 'Registro procesado correctamente',
        t_9 : 'Desea eliminar el regsitro ?',
        t_10 : ''
    }
    
};


