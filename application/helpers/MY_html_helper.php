<?php

if (!function_exists('log_P')) {

    function log_P($code, $message) {
        $trace = debug_backtrace();
        $file_err = str_replace(array(BASEPATH, APPPATH, '/media/Data/desarrollo/ipycca/', '/media/Data/produccion/ipycca/', 'controllers'), '', $trace[1]['file']);
        $arr_err = array('PHP' => array('IP' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0', 'co_error' => $code, 'smg_error' => $message, 'file' => $file_err, 'function' => $trace[1]['function'], 'line' => $trace[1]['line']));
        log_message('error', print_r($arr_err, true));
    }

}

if (!function_exists('agregarcss')) {

    function agregarcss($Direccion) {
        if (is_array($Direccion)) {
            foreach ($Direccion as $css) {
                echo '<link rel="stylesheet" type="text/css" href="' . site_url() . $css . '"/>';
            }
        } else {
            echo '<link rel="stylesheet" rel="stylesheet" type="text/css" href="' . site_url() . $Direccion . '">';
        }
    }

}

if (!function_exists('array_push_assoc')) {

    function array_push_assoc($array, $key, $value) {
        $array[$key] = $value;
        return $array;
    }

}

if (!function_exists('agregarjavascript')) {

    function agregarjavascript($Direccion) {
        if (is_array($Direccion)) {
            foreach ($Direccion as $javascript) {
                echo '<script type="text/javascript" src="' . site_url() . $javascript . '"></script>';
            }
        } else
            echo '<script type="text/javascript" src="' . site_url() . $Dirreccion . '"></script>';
    }

}

if (!function_exists('validaSession')) {

    function validaSession($controlador) {
        if (isset($_SESSION) && !empty($_SESSION)) {

            clear_cache_php();

            $cantidad = in_array_r($controlador, $_SESSION['menu']) ? 1 : 0;

            if ((int) $cantidad == 0) {
                redirect(site_url('general/principal'));
                echo "NO TIENE PERMISOS";
            }

            if (trim($_SESSION['chang_pass']) === 'N') {
                $CI = &get_instance();
                $CI->session->set_flashdata('error', 'Debe cambiar su contraseña para poder ingresar a las opciones del sistema.<br><br>Gracias.');
                redirect(site_url('login/c_pass'));
            }
        } else {
            redirect(site_url());
        }
    }

}

if (!function_exists('clear_cache_php')) {

    function clear_cache_php() {
        clearstatcache();
        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

}

if (!function_exists('in_array_r')) {

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                return $item;
            }
        }
        return false;
    }

}

if (!function_exists('BuscaIdMenu')) {

    function BuscaIdMenu($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['urlinvocar'] === $id) {
                return $key;
            }
        }
        return null;
    }

}

if (!function_exists('GetBoton')) {

    function GetBoton($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['var_1'] === $id) {
                return $key;
            }
        }
        return null;
    }

}

/**
 * Hidden Input Field
 *
 * Generates hidden fields.  You can pass a simple key/value string or an associative
 * array with multiple values.
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @return	string
 */
if (!function_exists('form_hidden')) {

    function form_hidden($name, $value = '', $recursing = FALSE) {
        static $form;
        if ($recursing === FALSE) {
            $form = "\n";
        }

        if (is_array($name)) {
            foreach ($name as $key => $val) {
                form_hidden($key, $val, TRUE);
            }
            return $form;
        }

        if (!is_array($value)) {
            $form .= '<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . form_prep($value, $name) . '" />' . "\n";
        } else {
            foreach ($value as $k => $v) {
                $k = (is_int($k)) ? '' : $k;
                form_hidden($name . '[' . $k . ']', $v, TRUE);
            }
        }

        return $form;
    }

}

if (!function_exists('decodeCaracteres')) {

    function decodeCaracteresEspeciales($string) {
        $entities = array("'", '%20', '%21', '%22', '%23', '%24', '%25', '%26', '%28', '%29', '%2A', '%2B', '%2C', '%2D', '%2E', '%2F', '%30', '%31', '%32', '%33', '%34', '%35', '%36', '%37', '%38', '%39',
            '%3A', '%3B', '%3C', '%3D', '%3E', '%3F', '%40', '%41', '%42', '%43', '%44', '%45', '%46', '%47', '%48', '%49', '%4A', '%4B', '%4C', '%4D', '%4E', '%4F', '%50', '%51', '%52', '%53', '%54', '%55', '%56', '%57', '%58', '%59', '%5A',
            '%61', '%62', '%63', '%64', '%65', '%66', '%67', '%68', '%69', '%6A', '%6B', '%6C', '%6D', '%6E', '%6F', '%70', '%71', '%72', '%73', '%74', '%75', '%76', '%77', '%78', '%79', '%7A',
            '%7B', '%7C', '%7D', '%7E', '%80', '%82', '%83', '%84', '%85', '%86', '%87', '%88', '%89', '%8A', '%8B', '%8C', '%8E', '%91', '%92', '%93', '%94', '%95', '%96', '%97', '%98', '%99', '%9A', '%9B', '%9C', '%9D', '%9E',
            '%9F', '%A1', '%A2', '%A3', '%A4', '%A5', '%A6', '%A7', '%A8', '%A9', '%AA', '%AB', '%AC', '%AE', '%AF', '%B0', '%B1', '%B2', '%B3', '%B4', '%B5', '%B6', '%B7', '%B8', '%B9', '%BA', '%BB', '%BC', '%BD', '%BE', '%BF',
            '%C0', '%C1', '%C2', '%C3', '%C4', '%C5', '%C6', '%C7', '%C8', '%C9', '%CA', '%CB', '%CC', '%CD', '%CE', '%CF', '%D0', '%D1', '%D2', '%D3', '%D4', '%D5', '%D6', '%D7', '%D8', '%D9', '%DA', '%DB', '%DC', '%DD', '%DE', '%DF',
            '%E0', '%E1', '%E2', '%E3', '%E4', '%E5', '%E6', '%E7', '%E8', '%E9', '%EA', '%EB', '%EC', '%ED', '%EE', '%EF', '%F0', '%F1', '%F2', '%F3', '%F4', '%F5', '%F6', '%F7', '%F8', '%F9', '%FA', '%FB', '%FC', '%FD', '%FE', '%FF');
        $replacements = array("%27", ' ', '!', '"', '#', '$', '%', '&', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '{', '|', '}', '~', '`', '‚', 'ƒ', '„', '…', '†', '‡', 'ˆ', '‰', 'Š', '‹', 'Œ', 'Ž', '‘', '’', '“', '”', '•', '–', '—', '˜', '™', 'š', '›', 'œ', '', 'ž',
            'Ÿ', '¡', '¢', '£', '¤', '¥', '¦', '§', '¨', '©', 'ª', '«', '¬', '®', '¯', '°', '±', '²', '³', '´', 'µ', '¶', '·', '¸', '¹', 'º', '»', '¼', '½', '¾', '¿',
            'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', '×', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß',
            'à', 'á', 'â', 'ã', 'ä', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', '÷', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ');
        return str_replace($entities, $replacements, $string);
    }

}

if (!function_exists('decodeCaracteresXML')) {

    function decodeCaracteresXML($string) {
        $entities = array('"', "'", '&', '<', '>');
        $replacements = array('&quot;', '&apos;', '&amp;', '&lt;', '&gt;');
        return str_replace($entities, $replacements, $string);
    }

}

if (!function_exists('reemplazar_caracteres')) {

    function reemplazar_caracteres($string) {

        $string = str_replace(
                array('&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Auml;', '&Aring;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;'), array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'à', 'á', 'â', 'ã', 'ä', 'å'), trim($string)
        );

        $string = str_replace(
                array('&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&egrave;', '&eacute;', '&ecirc;', '&euml;'), array('È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê', 'ë'), trim($string)
        );

        $string = str_replace(
                array('&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&Iuml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&iuml;'), array('Ì', 'Í', 'Î', 'Ï', 'ì', 'í', 'î', 'ï'), trim($string)
        );

        $string = str_replace(
                array('&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;'), array('ò', 'ó', 'ô', 'õ', 'ö'), trim($string)
        );

        $string = str_replace(
                array('&ugrave;', '&uacute;', '&ucirc;', '&uuml;'), array('ù', 'ú', 'û', 'ü'), trim($string)
        );

        $string = str_replace(
                array('&ntilde;', '&Ntilde;'), array('ñ', 'Ñ'), trim($string)
        );

        return $string;
    }

}

if (!function_exists('fnc_post')) {

    function fnc_post($opcion, $array, $url) {
        if (!empty($opcion) && empty($array)) {
            redirect(site_url($url));
        }
    }

}

if (!function_exists('quitar_saltos')) {

    function quitar_saltos($string) {
        return preg_replace('/\r\n+|\r+|\n+|\t+/i', ' ', $string);
    }

}

if (!function_exists('conveMes')) {

    function conveMes($b) {
        if (isset($b)) {
            $b = (int) $b;
            $Mes = Array();
            $Mes[1] = "ENERO";
            $Mes[2] = "FEBRERO";
            $Mes[3] = "MARZO";
            $Mes[4] = "ABRIL";
            $Mes[5] = "MAYO";
            $Mes[6] = "JUNIO";
            $Mes[7] = "JULIO";
            $Mes[8] = "AGOSTO";
            $Mes[9] = "SEPTIEMBRE";
            $Mes[10] = "OCTUBRE";
            $Mes[11] = "NOVIEMBRE";
            $Mes[12] = "DICIEMBRE";
            return $Mes[$b];
        }
    }

}

if (!function_exists('conveDia')) {

    function conveDia($b) {
        if (isset($b)) {
            $b = (int) $b;
            $Dias = Array();
            $Dias[1] = "Lunes";
            $Dias[2] = "Martes";
            $Dias[3] = "Miercoles";
            $Dias[4] = "Jueves";
            $Dias[5] = "Viernes";
            $Dias[6] = "Sabado";
            $Dias[7] = "Domingo";
            return $Dias[$b];
        }
    }

}

if (!function_exists('groupArray')) {

    function groupArray($array, $groupkey) {
        if (count($array) > 0) {
            $keys = array_keys($array[0]);
            $removekey = array_search($groupkey, $keys);

            if ($removekey === false) {
                return array("Clave \"$groupkey\" no existe");
            } else {
                unset($keys[$removekey]);
            }

            $groupcriteria = array();
            $return = array();

            foreach ($array as $value) {

                $item = null;
                foreach ($keys as $key) {
                    $item[$key] = $value[$key];
                }
                $busca = array_search($value[$groupkey], $groupcriteria);

                if ($busca === false) {
                    $groupcriteria[] = $value[$groupkey];
                    $return[] = array($groupkey => $value[$groupkey], 'groupeddata' => array());
                    $busca = count($return) - 1;
                }
                $return[$busca]['groupeddata'][] = $item;
            }
            return $return;
        } else {
            return array();
        }
    }

}

if (!function_exists('pingServer')) {

    function pingServer($domain, $puerto) {
        set_error_handler(function($errno, $errstr, $errfile, $errline ) {
            throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
        });
        try {
            $starttime = microtime(true);
            $file = @fsockopen($domain, $puerto, $errno, $errstr, 1);
            $stoptime = microtime(true);
            $status = 0;
            if (!$file) {
                $status = -1;
            } else {
                fclose($file);
                $status = ($stoptime - $starttime) * 1000;
                $status = floor($status);
            }
            if ($status <> -1) {
                return true;
            }
        } catch (Exception $exc) {
            //echo $exc->getTraceAsString();
            return false;
        }
    }

}

if (!function_exists('pingAddress')) {

    function pingAddress($ip) {
        $pingresult = exec("/bin/ping -c2 -w2 $ip", $outcome, $status);
        //echo $status;
        if ($status === 0) {
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('getUltimoDiaMes')) {

    function getUltimoDiaMes($elAnio, $elMes) {
        return date("d", (mktime(0, 0, 0, $elMes + 1, 1, $elAnio) - 1));
    }

}

if (!function_exists('date_convert')) {

    function date_convert($a, $n) {
        $CI = &get_instance();
        if ($CI->config->item('tipo_fecha') == 1) {
            $e = str_replace('/', '-', $a);
            $f = date($CI->config->item('formato_fecha'), strtotime(trim($e)));
            $b = explode('/', $f);
            $d = $b[1];
        } else if ($CI->config->item('tipo_fecha') == 2) {
            $e = str_replace('-', '/', $a);
            $f = date($CI->config->item('formato_fecha'), strtotime(trim($e)));
            $b = explode('/', $f);
            $d = $b[0];
        }

        if (isset($d)) {
            if ($d == '01') {
                $c = "Enero";
            } else if ($d == '02') {
                $c = "Febrero";
            } else if ($d == '03') {
                $c = "Marzo";
            } else if ($d == '04') {
                $c = "Abril";
            } else if ($d == '05') {
                $c = "Mayo";
            } else if ($d == '06') {
                $c = "Junio";
            } else if ($d == '07') {
                $c = "Julio";
            } else if ($d == '08') {
                $c = "Agosto";
            } else if ($d == '09') {
                $c = "Septiembre";
            } else if ($d == '10') {
                $c = "Octubre";
            } else if ($d == '11') {
                $c = "Noviembre";
            } else if ($d == '12') {
                $c = "Diciembre";
            }
        }

        if ($CI->config->item('tipo_fecha') == 1) {
            if ($n == 1) {
                unset($CI);
                return $b[0] . " de " . $c . " del " . $b[2];
            } else if ($n == 2) {
                $d = substr($c, 0, 3);
                unset($CI);
                return $b[0] . "/" . $d . "/" . $b[2];
            }
        } else if ($CI->config->item('tipo_fecha') == 2) {
            if ($n == 1) {
                unset($CI);
                return $b[1] . " de " . $c . " del " . $b[2];
            } else if ($n == 2) {
                $d = substr($c, 0, 3);
                unset($CI);
                return $b[1] . "/" . $d . "/" . $b[2];
            }
        }
    }

}
if (!function_exists('calcula_tiempo')) {

    function calcula_tiempo($start_time, $end_time) {
        $total_seconds = strtotime($end_time) - strtotime($start_time);
        $horas = floor($total_seconds / 3600);
        $minutes = ( ( $total_seconds / 60 ) % 60 );
        $seconds = ( $total_seconds % 60 );
        $time['horas'] = str_pad($horas, 2, "0", STR_PAD_LEFT);
        $time['minutes'] = str_pad($minutes, 2, "0", STR_PAD_LEFT);
        $time['seconds'] = str_pad($seconds, 2, "0", STR_PAD_LEFT);
        $time = implode(':', $time);
        return $time;
    }

}

if (!function_exists('templ_mail')) {

    function templ_mail($titulo, $descripcion, $asunto, $remitente, $arr, $adjuntos = null) {

        $response = array('co_err' => 0, 'tx_err' => '');

        if (isset($arr) && !empty($arr)) {

            $CI = &get_instance();
            $host = $CI->config->item('host_mail');
            $puerto = $CI->config->item('port_mail');
            $usuario = $CI->config->item('user_mail');
            $clave = $CI->config->item('pass_mail');
            $responder_a = "sqlmail@pycca.com";
            $cuerpo = '
            <center style="background-color:#E1E1E1;">
                    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
                    <tr>
                        <td align="center" valign="top" id="bodyCell">
                            <table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="900" id="emailBody">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:5px solid #F0514A;background-color:#00539F; position:relative;" bgcolor="#00539F">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="900" class="flexibleContainer">
                                                        <tr>
                                                            <td align="center" valign="top" width="900" class="flexibleContainerCell">
                                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td align="center" valign="top" class="textContent">
                                                                            <img src="http://www.pycca.com/images/logo-hover.png" width="105" height="32" style="position: absolute;left: 25px;"/>
                                                                            <h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">' . $titulo . '</h1>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="900" class="flexibleContainer">
                                                        <tr>
                                                            <td align="center" valign="top" width="900" class="flexibleContainerCell">
                                                                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td colspan="2" class="textContent">
                                                                                        ' . $descripcion . '
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="900" id="emailFooter">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="900" class="flexibleContainer">
                                                        <tr>
                                                            <td align="center" valign="top" width="900" class="flexibleContainerCell">
                                                                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td valign="top" bgcolor="#E1E1E1">
                                                                            <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                            <div>Copyright &#169; 2015 Intranet Pycca</div>
                                                                            <div>Este email ha sido generado autom&aacute;ticamente. Por favor no escribir a esta cuenta. </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>';

            $cabecera = "MIME-Version: 1.0\r\n";
            $cabecera .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $cabecera .= "From: Helpdesk\r\n";

            try {
                $mail = new PHPMailer();
                $mail->IsSMTP();
                $mail->CharSet = 'UTF-8';
                $mail->PluginDir = "";
                $mail->Host = $host;
                $mail->Port = $puerto;
                $mail->SMTPAuth = true;
                $mail->Username = $usuario;
                $mail->Password = $clave;
                $mail->From = $responder_a;
                $mail->FromName = $remitente;
                $mail->WordWrap = 50;
                $mail->Subject = $asunto;

                foreach ($arr as $row) {
                    $mail->AddAddress($row['mail'], $row['name']);
                }

                if (!empty($adjuntos)) {
                    foreach ($adjuntos as $raw) {
                        $mail->AddStringAttachment($raw['fileString'], $raw['name']);
                    }
                }

                $mail->AddBCC('fvera@pycca.com');
                $mail->Body = $cuerpo;
                $mail->IsHTML(true);
                $mail->Send();
            } catch (Exception $e) {
                $response['co_err'] = 1;
                $response['tx_err'] = $e->getMessage();
            }
        } else {
            $response['co_err'] = 1;
            $response['tx_err'] = 'Parametros incompletos';
        }

        return $response;
    }

}

if (!function_exists('setWeatherIcon')) {

    function setWeatherIcon($condid) {

        $icon = '';

        if ($condid <> '') {
            switch ($condid) {
                case '0': $icon = '<i class="wi wi-tornado"></i>';
                    break;
                case '1': $icon = '<i class="wi wi-storm-showers"></i>';
                    break;
                case '2': $icon = '<i class="wi wi-tornado"></i>';
                    break;
                case '3': $icon = '<i class="wi wi-thunderstorm"></i>';
                    break;
                case '4': $icon = '<i class="wi wi-thunderstorm"></i>';
                    break;
                case '5': $icon = '<i class="wi wi-snow"></i>';
                    break;
                case '6': $icon = '<i class="wi wi-rain-mix"></i>';
                    break;
                case '7': $icon = '<i class="wi wi-rain-mix"></i>';
                    break;
                case '8': $icon = '<i class="wi wi-sprinkle"></i>';
                    break;
                case '9': $icon = '<i class="wi wi-sprinkle"></i>';
                    break;
                case '10': $icon = '<i class="wi wi-hail"></i>';
                    break;
                case '11': $icon = '<i class="wi wi-showers"></i>';
                    break;
                case '12': $icon = '<i class="wi wi-showers"></i>';
                    break;
                case '13': $icon = '<i class="wi wi-snow"></i>';
                    break;
                case '14': $icon = '<i class="wi wi-storm-showers"></i>';
                    break;
                case '15': $icon = '<i class="wi wi-snow"></i>';
                    break;
                case '16': $icon = '<i class="wi wi-snow"></i>';
                    break;
                case '17': $icon = '<i class="wi wi-hail"></i>';
                    break;
                case '18': $icon = '<i class="wi wi-hail"></i>';
                    break;
                case '19': $icon = '<i class="wi wi-cloudy-gusts"></i>';
                    break;
                case '20': $icon = '<i class="wi wi-fog"></i>';
                    break;
                case '21': $icon = '<i class="wi wi-fog"></i>';
                    break;
                case '22': $icon = '<i class="wi wi-fog"></i>';
                    break;
                case '23': $icon = '<i class="wi wi-cloudy-gusts"></i>';
                    break;
                case '24': $icon = '<i class="wi wi-cloudy-windy"></i>';
                    break;
                case '25': $icon = '<i class="wi wi-thermometer"></i>';
                    break;
                case '26': $icon = '<i class="wi wi-cloudy"></i>';
                    break;
                case '27': $icon = '<i class="wi wi-night-cloudy"></i>';
                    break;
                case '28': $icon = '<i class="wi wi-day-cloudy"></i>';
                    break;
                case '29': $icon = '<i class="wi wi-night-cloudy"></i>';
                    break;
                case '30': $icon = '<i class="wi wi-day-cloudy"></i>';
                    break;
                case '31': $icon = '<i class="wi wi-night-clear"></i>';
                    break;
                case '32': $icon = '<i class="wi wi-day-sunny"></i>';
                    break;
                case '33': $icon = '<i class="wi wi-night-clear"></i>';
                    break;
                case '34': $icon = '<i class="wi wi-day-sunny-overcast"></i>';
                    break;
                case '35': $icon = '<i class="wi wi-hail"></i>';
                    break;
                case '36': $icon = '<i class="wi wi-day-sunny"></i>';
                    break;
                case '37': $icon = '<i class="wi wi-thunderstorm"></i>';
                    break;
                case '38': $icon = '<i class="wi wi-thunderstorm"></i>';
                    break;
                case '39': $icon = '<i class="wi wi-thunderstorm"></i>';
                    break;
                case '40': $icon = '<i class="wi wi-storm-showers"></i>';
                    break;
                case '41': $icon = '<i class="wi wi-snow"></i>';
                    break;
                case '42': $icon = '<i class="wi wi-snow"></i>';
                    break;
                case '43': $icon = '<i class="wi wi-snow"></i>';
                    break;
                case '44': $icon = '<i class="wi wi-cloudy"></i>';
                    break;
                case '45': $icon = '<i class="wi wi-lightning"></i>';
                    break;
                case '46': $icon = '<i class="wi wi-snow"></i>';
                    break;
                case '47': $icon = '<i class="wi wi-thunderstorm"></i>';
                    break;
                case '3200': $icon = '<i class="wi wi-cloud"></i>';
                    break;
                default: $icon = '<i class="wi wi-cloud"></i>';
                    break;
            }
        }

        return $icon;
    }

}

if (!function_exists('array_order_column')) {

    function array_order_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

}

if (!function_exists('add_ceros')) {

    function add_ceros($numero, $ceros) {
        $order_diez = explode(".", $numero);
        $dif_diez = $ceros - strlen($order_diez[0]);
        $insertar_ceros = '';
        for ($m = 0; $m < $dif_diez; $m++) {
            $insertar_ceros .= 0;
        }
        return $insertar_ceros .= $numero;
    }

}


if (!function_exists('acentos')) {

    function acentos($cadena) {
        $buscar = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ã¡,Ã©,Ã­,Ã&sup3;,Ãº,Ã±,ÃÃ¡,ÃÃ©,ÃÃ­,ÃÃ&sup3;,ÃÃº,ÃÃ±");
        $cambiar = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ");
        $textocorregido = str_replace($buscar, $cambiar, $cadena);
        unset($buscar, $cambiar);
        return $textocorregido;
    }

}

if (!function_exists('xmlEscape')) {

    function xmlEscape($string) {
        return str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
    }

}

if (!function_exists('eliminarEspacio')) {

    function eliminarEspacio($string) {
        $string = str_replace(array("'", '"', '[', ']', "[\n|\r|\n\r]", '/\t/', "\x0d|\x0a"), array('', '', '', '', '', ''), trim($string));
        $string = preg_replace('/\s(?=\s)/', '', $string);
        $string = preg_replace('/[\n\r\t]/', ' ', $string);
        $string = html_entity_decode($string, ENT_COMPAT, 'UTF-8');
        $string = strip_tags($string);
        $string = str_replace('\r', '', $string);
        $string = preg_replace('/(?<!>)n/', '<br />n', $string);
        $string = str_replace('\n', '', $string);
        $string = str_replace('/\t/', '', $string);
        $string = preg_replace('/[ ]+/', ' ', $string);
        $string = preg_replace('/<!--[^-]*-->/', '', $string);
        $string = strip_tags($string);
        $string = str_replace(array('"', "'"), '', $string);
        return $string;
    }

}


if (!function_exists('tiempo_ejecucion')) {

    function tiempo_ejecucion() {
        list($useg, $seg) = explode(' ', microtime());
        return ((float) $useg + (float) $seg);
    }

}


if (!function_exists('getUltimoDiaMes')) {

    function getUltimoDiaMes($elAnio, $elMes) {
        return date("d", (mktime(0, 0, 0, $elMes + 1, 1, $elAnio) - 1));
    }

}

if (!function_exists('meses')) {

    function meses() {
        return array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    }

}


if (!function_exists('cambiaFecha')) {

    function cambiaFecha($fecha, $formato_origen, $formato_destino) {
        #Formato de fecha
        # 1 = dd/mm/yyyy
        # 2 = mm/dd/yyyy
        # 3 = yyyy/mm/dd
        # 4 = dd-mm-yyyy
        # 5 = mm-dd-yyyy
        # 6 = yyyy-mm-dd
        if ($formato_origen === '1') {
            list($dia, $mes, $anio) = explode('/', $fecha);
        } else if ($formato_origen === '2') {
            list($mes, $dia, $anio) = explode('/', $fecha);
        } else if ($formato_origen === '3') {
            list($anio, $mes, $dia) = explode('/', $fecha);
        } else if ($formato_origen === '4') {
            list($dia, $mes, $anio) = explode('-', $fecha);
        } else if ($formato_origen === '5') {
            list($mes, $dia, $anio) = explode('-', $fecha);
        } else if ($formato_origen === '6') {
            list($anio, $mes, $dia) = explode('-', $fecha);
        }

        if ($formato_destino === '1') {
            return "$dia/$mes/$anio";
        } else if ($formato_destino === '2') {
            return "$mes/$dia/$anio";
        } else if ($formato_destino === '3') {
            return "$anio/$mes/$dia";
        } else if ($formato_destino === '4') {
            return "$dia-$mes-$anio";
        } else if ($formato_destino === '5') {
            return "$mes-$dia-$anio";
        } else if ($formato_destino === '6') {
            return "$anio-$mes-$dia";
        }
        return "";
    }

}

if (!function_exists('valorEnLetras')) {

    function valorEnLetras($x) {
        if ($x < 0) {
            $signo = "menos ";
        } else {
            $signo = "";
        }
        $x = abs($x);
        $C1 = $x;

        $G6 = floor($x / (1000000));  // 7 y mas

        $E7 = floor($x / (100000));
        $G7 = $E7 - $G6 * 10;   // 6

        $E8 = floor($x / 1000);
        $G8 = $E8 - $E7 * 100;   // 5 y 4

        $E9 = floor($x / 100);
        $G9 = $E9 - $E8 * 10;  //  3

        $E10 = floor($x);
        $G10 = $E10 - $E9 * 100;  // 2 y 1


        $G11 = round(($x - $E10) * 100, 0);  // Decimales
        //////////////////////

        $H6 = unidades($G6);

        if ($G7 == 1 AND $G8 == 0) {
            $H7 = "Cien ";
        } else {
            $H7 = decenas($G7);
        }

        $H8 = unidades($G8);

        if ($G9 == 1 AND $G10 == 0) {
            $H9 = "Cien ";
        } else {
            $H9 = decenas($G9);
        }

        $H10 = unidades($G10);

        if ($G11 < 10) {
            $H11 = "0" . $G11;
        } else {
            $H11 = $G11;
        }

        /////////////////////////////
        if ($G6 == 0) {
            $I6 = " ";
        } elseif ($G6 == 1) {
            $I6 = "MillÃ³n ";
        } else {
            $I6 = "Millones ";
        }

        if ($G8 == 0 AND $G7 == 0) {
            $I8 = " ";
        } else {
            $I8 = "Mil ";
        }

        $I10 = "Dolares ";
        $I11 = "/100 ";

        #$C3 = $signo.$H6.$I6.$H7.$I7.$H8.$I8.$H9.$I9.$H10.$I10.$H11.$I11;
        $C3 = $signo . $H6 . $I6 . $H7 . $H8 . $I8 . $H9 . $H10 . $I10 . $H11 . $I11;

        return $C3; //Retornar el resultado
    }

}

if (!function_exists('unidades')) {

    function unidades($u) {
        if ($u == 0) {
            $ru = " ";
        } elseif ($u == 1) {
            $ru = "Un ";
        } elseif ($u == 2) {
            $ru = "Dos ";
        } elseif ($u == 3) {
            $ru = "Tres ";
        } elseif ($u == 4) {
            $ru = "Cuatro ";
        } elseif ($u == 5) {
            $ru = "Cinco ";
        } elseif ($u == 6) {
            $ru = "Seis ";
        } elseif ($u == 7) {
            $ru = "Siete ";
        } elseif ($u == 8) {
            $ru = "Ocho ";
        } elseif ($u == 9) {
            $ru = "Nueve ";
        } elseif ($u == 10) {
            $ru = "Diez ";
        } elseif ($u == 11) {
            $ru = "Once ";
        } elseif ($u == 12) {
            $ru = "Doce ";
        } elseif ($u == 13) {
            $ru = "Trece ";
        } elseif ($u == 14) {
            $ru = "Catorce ";
        } elseif ($u == 15) {
            $ru = "Quince ";
        } elseif ($u == 16) {
            $ru = "Dieciseis ";
        } elseif ($u == 17) {
            $ru = "Decisiete ";
        } elseif ($u == 18) {
            $ru = "Dieciocho ";
        } elseif ($u == 19) {
            $ru = "Diecinueve ";
        } elseif ($u == 20) {
            $ru = "Veinte ";
        } elseif ($u == 21) {
            $ru = "Veintiun ";
        } elseif ($u == 22) {
            $ru = "Veintidos ";
        } elseif ($u == 23) {
            $ru = "Veintitres ";
        } elseif ($u == 24) {
            $ru = "Veinticuatro ";
        } elseif ($u == 25) {
            $ru = "Veinticinco ";
        } elseif ($u == 26) {
            $ru = "Veintiseis ";
        } elseif ($u == 27) {
            $ru = "Veintisiente ";
        } elseif ($u == 28) {
            $ru = "Veintiocho ";
        } elseif ($u == 29) {
            $ru = "Veintinueve ";
        } elseif ($u == 30) {
            $ru = "Treinta ";
        } elseif ($u == 31) {
            $ru = "Treinta y un ";
        } elseif ($u == 32) {
            $ru = "Treinta y dos ";
        } elseif ($u == 33) {
            $ru = "Treinta y tres ";
        } elseif ($u == 34) {
            $ru = "Treinta y cuatro ";
        } elseif ($u == 35) {
            $ru = "Treinta y cinco ";
        } elseif ($u == 36) {
            $ru = "Treinta y seis ";
        } elseif ($u == 37) {
            $ru = "Treinta y siete ";
        } elseif ($u == 38) {
            $ru = "Treinta y ocho ";
        } elseif ($u == 39) {
            $ru = "Treinta y nueve ";
        } elseif ($u == 40) {
            $ru = "Cuarenta ";
        } elseif ($u == 41) {
            $ru = "Cuarenta y un ";
        } elseif ($u == 42) {
            $ru = "Cuarenta y dos ";
        } elseif ($u == 43) {
            $ru = "Cuarenta y tres ";
        } elseif ($u == 44) {
            $ru = "Cuarenta y cuatro ";
        } elseif ($u == 45) {
            $ru = "Cuarenta y cinco ";
        } elseif ($u == 46) {
            $ru = "Cuarenta y seis ";
        } elseif ($u == 47) {
            $ru = "Cuarenta y siete ";
        } elseif ($u == 48) {
            $ru = "Cuarenta y ocho ";
        } elseif ($u == 49) {
            $ru = "Cuarenta y nueve ";
        } elseif ($u == 50) {
            $ru = "Cincuenta ";
        } elseif ($u == 51) {
            $ru = "Cincuenta y un ";
        } elseif ($u == 52) {
            $ru = "Cincuenta y dos ";
        } elseif ($u == 53) {
            $ru = "Cincuenta y tres ";
        } elseif ($u == 54) {
            $ru = "Cincuenta y cuatro ";
        } elseif ($u == 55) {
            $ru = "Cincuenta y cinco ";
        } elseif ($u == 56) {
            $ru = "Cincuenta y seis ";
        } elseif ($u == 57) {
            $ru = "Cincuenta y siete ";
        } elseif ($u == 58) {
            $ru = "Cincuenta y ocho ";
        } elseif ($u == 59) {
            $ru = "Cincuenta y nueve ";
        } elseif ($u == 60) {
            $ru = "Sesenta ";
        } elseif ($u == 61) {
            $ru = "Sesenta y un ";
        } elseif ($u == 62) {
            $ru = "Sesenta y dos ";
        } elseif ($u == 63) {
            $ru = "Sesenta y tres ";
        } elseif ($u == 64) {
            $ru = "Sesenta y cuatro ";
        } elseif ($u == 65) {
            $ru = "Sesenta y cinco ";
        } elseif ($u == 66) {
            $ru = "Sesenta y seis ";
        } elseif ($u == 67) {
            $ru = "Sesenta y siete ";
        } elseif ($u == 68) {
            $ru = "Sesenta y ocho ";
        } elseif ($u == 69) {
            $ru = "Sesenta y nueve ";
        } elseif ($u == 70) {
            $ru = "Setenta ";
        } elseif ($u == 71) {
            $ru = "Setenta y un ";
        } elseif ($u == 72) {
            $ru = "Setenta y dos ";
        } elseif ($u == 73) {
            $ru = "Setenta y tres ";
        } elseif ($u == 74) {
            $ru = "Setenta y cuatro ";
        } elseif ($u == 75) {
            $ru = "Setenta y cinco ";
        } elseif ($u == 76) {
            $ru = "Setenta y seis ";
        } elseif ($u == 77) {
            $ru = "Setenta y siete ";
        } elseif ($u == 78) {
            $ru = "Setenta y ocho ";
        } elseif ($u == 79) {
            $ru = "Setenta y nueve ";
        } elseif ($u == 80) {
            $ru = "Ochenta ";
        } elseif ($u == 81) {
            $ru = "Ochenta y un ";
        } elseif ($u == 82) {
            $ru = "Ochenta y dos ";
        } elseif ($u == 83) {
            $ru = "Ochenta y tres ";
        } elseif ($u == 84) {
            $ru = "Ochenta y cuatro ";
        } elseif ($u == 85) {
            $ru = "Ochenta y cinco ";
        } elseif ($u == 86) {
            $ru = "Ochenta y seis ";
        } elseif ($u == 87) {
            $ru = "Ochenta y siete ";
        } elseif ($u == 88) {
            $ru = "Ochenta y ocho ";
        } elseif ($u == 89) {
            $ru = "Ochenta y nueve ";
        } elseif ($u == 90) {
            $ru = "Noventa ";
        } elseif ($u == 91) {
            $ru = "Noventa y un ";
        } elseif ($u == 92) {
            $ru = "Noventa y dos ";
        } elseif ($u == 93) {
            $ru = "Noventa y tres ";
        } elseif ($u == 94) {
            $ru = "Noventa y cuatro ";
        } elseif ($u == 95) {
            $ru = "Noventa y cinco ";
        } elseif ($u == 96) {
            $ru = "Noventa y seis ";
        } elseif ($u == 97) {
            $ru = "Noventa y siete ";
        } elseif ($u == 98) {
            $ru = "Noventa y ocho ";
        } else {
            $ru = "Noventa y nueve ";
        }
        return $ru; //Retornar el resultado
    }

}

if (!function_exists('decenas')) {

    function decenas($d) {
        if ($d == 0) {
            $rd = "";
        } elseif ($d == 1) {
            $rd = "Ciento ";
        } elseif ($d == 2) {
            $rd = "Doscientos ";
        } elseif ($d == 3) {
            $rd = "Trescientos ";
        } elseif ($d == 4) {
            $rd = "Cuatrocientos ";
        } elseif ($d == 5) {
            $rd = "Quinientos ";
        } elseif ($d == 6) {
            $rd = "Seiscientos ";
        } elseif ($d == 7) {
            $rd = "Setecientos ";
        } elseif ($d == 8) {
            $rd = "Ochocientos ";
        } else {
            $rd = "Novecientos ";
        }
        return $rd; //Retornar el resultado
    }

}

if (!function_exists('validaCedula')) {

    function validaCedula($strCedula) {
        $array = null;
        if (is_null($strCedula) || empty($strCedula)) {//compruebo si que el numero enviado es vacio o null
            $array = array('co_err' => 1, 'tx_err' => 'Por Favor Ingrese la Cedula');
        } else {//caso contrario sigo el proceso
            if (is_numeric($strCedula)) {
                $total_caracteres = strlen($strCedula); // se suma el total de caracteres
                if ($total_caracteres == 10) {//compruebo que tenga 10 digitos la cedula
                    $nro_region = substr($strCedula, 0, 2); //extraigo los dos primeros caracteres de izq a der
                    if ($nro_region >= 1 && $nro_region <= 24) {// compruebo a que region pertenece esta cedula//
                        $ult_digito = substr($strCedula, -1, 1); //extraigo el ultimo digito de la cedula
                        //extraigo los valores pares//
                        $valor2 = substr($strCedula, 1, 1);
                        $valor4 = substr($strCedula, 3, 1);
                        $valor6 = substr($strCedula, 5, 1);
                        $valor8 = substr($strCedula, 7, 1);
                        $suma_pares = ($valor2 + $valor4 + $valor6 + $valor8);
                        //extraigo los valores impares//
                        $valor1 = substr($strCedula, 0, 1);
                        $valor1 = ($valor1 * 2);
                        if ($valor1 > 9) {
                            $valor1 = ($valor1 - 9);
                        } else {
                            
                        }
                        $valor3 = substr($strCedula, 2, 1);
                        $valor3 = ($valor3 * 2);
                        if ($valor3 > 9) {
                            $valor3 = ($valor3 - 9);
                        } else {
                            
                        }
                        $valor5 = substr($strCedula, 4, 1);
                        $valor5 = ($valor5 * 2);
                        if ($valor5 > 9) {
                            $valor5 = ($valor5 - 9);
                        } else {
                            
                        }
                        $valor7 = substr($strCedula, 6, 1);
                        $valor7 = ($valor7 * 2);
                        if ($valor7 > 9) {
                            $valor7 = ($valor7 - 9);
                        } else {
                            
                        }
                        $valor9 = substr($strCedula, 8, 1);
                        $valor9 = ($valor9 * 2);
                        if ($valor9 > 9) {
                            $valor9 = ($valor9 - 9);
                        } else {
                            
                        }

                        $suma_impares = ($valor1 + $valor3 + $valor5 + $valor7 + $valor9);
                        $suma = ($suma_pares + $suma_impares);
                        $dis = substr($suma, 0, 1); //extraigo el primer numero de la suma
                        $dis = (($dis + 1) * 10); //luego ese numero lo multiplico x 10, consiguiendo asi la decena inmediata superior
                        $digito = ($dis - $suma);
                        if ($digito == 10) {
                            $digito = '0';
                        } else {
                            
                        }//si la suma nos resulta 10, el decimo digito es cero
                        if ($digito == $ult_digito) {//comparo los digitos final y ultimo
                            $array = array('co_err' => 0, 'tx_err' => 'Cedula Correcta');
                        } else {
                            $array = array('co_err' => 2, 'tx_err' => 'Cedula Incorrecta');
                        }
                    } else {
                        $array = array('co_err' => 3, 'tx_err' => 'Este Nro de Cedula no corresponde a ninguna provincia del ecuador');
                    }
                } else {
                    $array = array('co_err' => 4, 'tx_err' => 'Es un Numero y tiene solo ' . $total_caracteres . ' caracteres.');
                }
            } else {
                $array = array('co_err' => 4, 'tx_err' => 'Esta Cedula no corresponde a un Nro de Cedula de Ecuador');
            }
        }
        return $array;
    }

}

if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}

/* * *********************************************************************************** */
/* RESTRINGE ACCESO EXTERNO MAL INTENCIONADO */
/* * *********************************************************************************** */
//if (isset($_SERVER['HTTP_REFERER'])) {
//    $fuente_valida = "//" . $_SERVER['HTTP_HOST'];
//    $posicion_fuente_valida = strpos($_SERVER['HTTP_REFERER'], $fuente_valida);
//    if ($posicion_fuente_valida != 5 && $posicion_fuente_valida != 6) {
//        die('Access denied');
//    }
//}
?>

