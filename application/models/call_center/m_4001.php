<?php

class m_4001 extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    public $estados = array(
        'C' => array(
            'name' => 'Contactado',
            'color' => '#00A65A'
        ),
        'I' => array(
            'name' => 'Incontactado',
            'color' => '#f0a30a'
        ),
        'P' => array(
            'name' => 'Pendiente',
            'color' => '#F56954'
        ),
        'E' => array(
            'name' => 'En proceso',
            'color' => '#2E8BEF'
        ),
        'A' => array(
            'name' => 'Agendado',
            'color' => '#633EBE'
        )
    );
    public $estadosDet = array(
        'N' => array(
            'name' => 'Pendiente',
            'color' => '#00A65A',
            'class' => 'label-danger'
        ),
        'S' => array(
            'name' => 'Grabado',
            'color' => '#F56954',
            'class' => 'label-success'
        )
    );
    public $tipoGestion = array(
        'E' => array(
            'name' => 'Equivocado',
            'color' => '#00A65A',
            'class' => 'label-danger'
        ),
        'I' => array(
            'name' => 'Incontactable',
            'color' => '#F56954',
            'class' => 'label-success'
        ),
        'F' => array(
            'name' => 'Sin Telefono',
            'color' => '#2E8BEF',
            'class' => 'label-success'
        )
    );

    function retDashboardAgente($cod_empl) {

        $pie = $promedios = $chart = $estados = null;
        $data = null;
        $error = '';

        if (!empty($cod_empl)) {

            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });

            try {

                $Sql = " NTS_INTERCAMBIO.dbo.sp_act_datos_ret_agente " . (int) $cod_empl . ";";
                $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
                //$conn = odbc_connect("DESARROLLO-REPORT", "LINVENT", "LINVENT");
                $resultset = odbc_exec($conn, $Sql);

                if (!$resultset) {
                    throw new PDOException("Error");
                }

                if (odbc_error()) {
                    $error = "ERROR: AL RETORNAR AGENTE " . odbc_errormsg($conn);
                } else {

                    do {

                        while ($row = odbc_fetch_array($resultset)) {

                            if (isset($row['resultset'])) {

                                switch ($row['resultset']) {

                                    case "PIE":

                                        $pie[] = array(
                                            'co_est' => trim($row['de_estado']),
                                            'ds_est' => trim($this->estados[trim($row['de_estado'])]['name']),
                                            'ca_est' => (int) $row['cantidad'],
                                        );

                                        break;

                                    case "PROMEDIOS":

                                        $promedios = array(
                                            'ba_pro' => gmdate("H\h i\m s\s", $row['promedio']),
                                            'mi_pro' => gmdate("H\h i\m s\s", $row['minimo']),
                                            'ma_pro' => gmdate("H\h i\m s\s", $row['maximo']),
                                        );

                                        break;

                                    case "CHART":

                                        $chart[] = array(
                                            'ho_cha' => trim($row['hora']),
                                            'ca_cha' => (int) $row['cantidad']
                                        );

                                        break;

                                    case "ESTADOS":

                                        $estados[] = array(
                                            'co_est' => trim($row['de_estado']),
                                            'ds_est' => trim($this->estados[trim($row['de_estado'])]['name']),
                                            'cl_est' => trim($this->estados[trim($row['de_estado'])]['color'])
                                        );

                                        break;
                                }
                            }
                        }
                    } while (odbc_next_result($resultset));

                    $pie = $this->agenteArrayPie($pie);
                    $chart = $this->agenteArrayBar($chart);

                    $data = array(
                        'pie' => $pie,
                        'pro' => $promedios,
                        'bar' => $chart,
                        'cmb' => $estados
                    );
                }

                odbc_close($conn);
            } catch (Exception $e) {
                $error = "ERROR AL RETORNAR USUARIO.";
            }
        }

        return array('data' => $data, 'error' => $error);
    }

    function agenteArrayPie($array) {

        $pie = null;

        if (!empty($array)) {
            foreach ($array as $row) {
                $pie [] = array(
                    'label' => $row['ds_est'] . ' <b>' . number_format($row['ca_est'], 0) . '</b>',
                    'data' => (int) $row['ca_est'],
                    'color' => $this->estados[trim($row['co_est'])]['color'],
                );
            }
        }

        return $pie;
    }

    function agenteArrayBar($array) {

        $data = $arrayHora = null;

        if (!empty($array)) {

            $bar = null;

            for ($i = 0; $i < count($array); $i++) {
                $arrayHora[] = array($i, str_replace(':00', 'h', $array[$i]['ho_cha']));
                $bar[] = array($i, $array[$i]['ca_cha']);
            }

            $data = array(
                'color' => '#5482FF',
                'data' => $bar
            );
        }

        return array('data' => array($data), 'ticks' => $arrayHora);
    }

    function retClientesEstado($tipo) {

        $array = null;

        if (!empty($tipo)) {

            $Sql = " SELECT a.co_campana,a.co_cliente,c.cl_identifica,c.cl_nombrelargo,a.fe_carga,a.ti_gestion, ";
            $Sql .= " a.va_Deuda,a.fe_asignado,a.nu_HoraInicio,a.nu_HoraFin,a.de_observacion,a.fe_agenda,a.de_estado,ms_diasvencidos ";
            $Sql .= " FROM NTS_INTERCAMBIO..ic_ClientesActualizaDatos_hist a ";
            $Sql .= " INNER JOIN NTS_CLIENTES..cl_clientes c ";
            $Sql .= " INNER JOIN NTS_TARJCRED..TC_MAESTRO ON MA_CLIENTE=CL_CLIENTE AND MA_EMPRESA=CL_EMPRESA AND TC_MAESTRO.ma_afinidad = 1 ";
            $Sql .= " INNER JOIN NTS_TARJCRED..TC_MAESTRO_SALDOS ON MA_CUENTA=MS_CUENTA AND MS_EMPRESA=MA_EMPRESA";
            $Sql .= " ON a.co_cliente = c.cl_cliente ";
            $Sql .= " AND c.cl_empresa = 27 ";
            $Sql .= " WHERE a.de_usuario = " . $_SESSION['c_e'] . " AND a.de_estado = '$tipo' ";
            $Sql .= " UNION ";
            $Sql .= " SELECT a.co_campana,a.co_cliente,c.cl_identifica,c.cl_nombrelargo,a.fe_carga,a.ti_gestion, ";
            $Sql .= " a.va_Deuda,a.fe_asignado,a.nu_HoraInicio,a.nu_HoraFin,a.de_observacion,a.fe_agenda,a.de_estado,ms_diasvencidos ";
            $Sql .= " FROM NTS_INTERCAMBIO..ic_ClientesActualizaDatos a ";
            $Sql .= " INNER JOIN NTS_CLIENTES..cl_clientes c ";
            $Sql .= " INNER JOIN NTS_TARJCRED..TC_MAESTRO ON MA_CLIENTE=CL_CLIENTE AND MA_EMPRESA=CL_EMPRESA AND TC_MAESTRO.ma_afinidad = 1 ";
            $Sql .= " INNER JOIN NTS_TARJCRED..TC_MAESTRO_SALDOS ON MA_CUENTA=MS_CUENTA AND MS_EMPRESA=MA_EMPRESA";
            $Sql .= " ON a.co_cliente = c.cl_cliente ";
            $Sql .= " AND c.cl_empresa = 27 ";
            $Sql .= " WHERE a.de_usuario = " . $_SESSION['c_e'] . " AND a.de_estado = '$tipo'; ";
            //$this->db = $this->load->database('desarrollo_report', true);

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR CLIENTES: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            $resArray = $result->result_array();

            if (!empty($resArray)) {

                foreach ($resArray as $row) {
                    $boton = '';
                    if ($row['de_estado'] <> 'C') {
                        $boton = '<button type="button" onclick="modalTelf(' . "'" . htmlentities(json_encode($this->arrayTelefonos($row), true)) . "'" . ')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-check-square-o"></i></button>';
                    }
                    $array[] = array(
                        'co_cam' => (int) $row['co_campana'],
                        'co_cli' => (int) $row['co_cliente'],
                        'id_cli' => trim($row['cl_identifica']),
                        'no_cli' => trim(utf8_encode($row['cl_nombrelargo'])),
                        'fe_car' => trim($row['fe_carga']),
                        'ti_ges' => trim($row['ti_gestion']),
                        'no_ges' => $this->tipoGestion[trim($row['ti_gestion'])]['name'],
                        'va_deu' => number_format($row['va_Deuda'], 2),
                        'fe_asi' => trim($row['fe_asignado']),
                        'ho_ini' => trim($row['nu_HoraInicio']),
                        'ho_fin' => trim($row['nu_HoraFin']),
                        'tx_obs' => trim(utf8_encode($row['de_observacion'])),
                        'fe_age' => !empty($row['fe_agenda']) ? date('H:i', strtotime($row['fe_agenda'])) : '',
                        'co_est' => trim($row['de_estado']),
                        'di_ven' => (int) $row['ms_diasvencidos'],
                        'ds_est' => $this->estados[trim($row['de_estado'])]['name'],
                        'html' => $boton
                    );
                }
            }
        }
//        print_r($array);
//        die;
        return $array;
    }

    function arrayTelefonos($data) {
        $cab = $tel = null;
        if (!empty($data)) {
            $cab['co_cam'] = (int) $data['co_campana'];
            $cab['id_cli'] = trim($data['cl_identifica']);
            $cab['co_cli'] = (int) $data['co_cliente'];
            $cab['di_ven'] = (int) $data['ms_diasvencidos'];
            $cab['no_cli'] = trim(utf8_encode($data['cl_nombrelargo']));
            $cab['fe_car'] = trim($data['fe_carga']);
            $cab['va_deu'] = number_format($data['va_Deuda'], 2);
            $cab['tx_obs'] = trim(utf8_encode(preg_replace("/\s+/"," ",$data['de_observacion']))); // ));
            $cab['co_est'] = trim($data['de_estado']);
            $cab['no_ges'] = $this->tipoGestion[trim($data['ti_gestion'])]['name'];
        }

        return array('cab' => $cab);
    }

    function retTipoTelf() {

        $array = null;

        $Sql = " SELECT dc_cod,dc_valor1 ";
        $Sql .= " FROM NTS_MONITRAN..vw_catalogo ";
        $Sql .= " WHERE ca_nombre = 'TipoLocali' ";
        $Sql .= " AND dc_valor2 = 'T' ";
        $Sql .= " ORDER BY dc_valor1;";
        //$this->db = $this->load->database('desarrollo_report', true);

        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            return "ERROR AL RETORNAR TIPO TELEFONO: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        $resArray = $result->result_array();

        if (!empty($resArray)) {

            foreach ($resArray as $row) {

                $array[] = array(
                    'co_tip' => trim($row['dc_cod']),
                    'ds_tip' => trim(utf8_encode($row['dc_valor1']))
                );
            }
        }

        return $array;
    }

    function validaUsusarioSyscards($co_usu) {

        $usuario = '';

        if (!empty($co_usu)) {

            $Sql = " SELECT us_login
            FROM WSDBWARE..WSOPERADORES
            INNER JOIN NTS_MONITRAN..mt_usuarios on user_sigen=us_login
            WHERE IDEmpleado=" . (int) $co_usu . ";";
            //$this->db = $this->load->database('desarrollo_report', true);

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = & load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL VALIDAR USUARIO <BR> COD_ERR:" . $this->db->_error_number(), $this->db->_error_message(), 'error_db');
                die;
            } else {

                $row = $result->row_array();

                if (!empty($row)) {
                    $usuario = $row['us_login'];
                }
            }
        }

        return $usuario;
    }

    function retTelefonoClientes($co_cli) {

        $array = null;

        if (!empty($co_cli)) {

            $Sql = " SELECT tf_codigo,CAT1.dc_valor1,tf_secuencial,tf_tiporeg,tf_tipotelf,tf_codigoregion,tf_telefono,tf_extencion, ";
            $Sql .= " rf_nombres,rf_apellidos,CAT2.DC_VALOR1+'' AS no_pariente,rf_parentesco,dr_ciudad,dr_provincia ,rf_secuencial";
            $Sql .= " FROM NTS_CLIENTES..cl_telefonos tf ";
            $Sql .= " INNER JOIN NTS_MONITRAN..vw_catalogo CAT1  ON CAT1.ca_nombre = 'TipoLocali'  AND CAT1.dc_cod = tf_tipotelf ";
            $Sql .= " LEFT JOIN NTS_CLIENTES..CL_REFERFAMILIA ON RF_CLIENTE=TF_CODIGO AND RF_EMPRESA=TF_EMPRESA  AND tf_codigosecu=rf_secuencial ";
            $Sql .= " LEFT JOIN NTS_MONITRAN..vw_catalogo CAT2 ON CAT2.ca_nombre = 'PARIENTEs'  AND CAT2.dc_cod = RF_PARENTESCO ";
            $Sql .= " LEFT JOIN NTS_CLIENTES..CL_DIRECCIONES ON dr_codigo=TF_codigo AND DR_EMPRESA=TF_EMPRESA and dr_tiporeg=tf_tiporeg and dr_codigosecu=rf_secuencial ";
            $Sql .= " WHERE tf_codigo  = " . (int) $co_cli;
            $Sql .= " AND tf_empresa = 27  AND tf_tiporeg IN ('CL','RF') order by tf_tiporeg; ";
            //$this->db = $this->load->database('desarrollo_report', true);

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR TELEFONO CLIENTES: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            $resArray = $result->result_array();

            if (!empty($resArray)) {

                foreach ($resArray as $row) {

                    $array[] = array(
                        'co_sec' => (int) $row['tf_secuencial'],
                        'se_ref' => (int) $row['rf_secuencial'],
                        'co_reg' => trim($row['tf_tiporeg']),
                        'co_tip' => trim($row['tf_tipotelf']),
                        'no_tip' => trim($row['dc_valor1']),
                        'co_are' => trim($row['tf_codigoregion']),
                        'co_cel' => trim($row['tf_telefono']),
                        'ex_tel' => trim($row['tf_extencion']),
                        'no_ref' => trim(utf8_decode($row['rf_nombres'])),
                        'ap_ref' => trim(utf8_decode($row['rf_apellidos'])),
                        'no_par' => trim(utf8_decode($row['no_pariente'])),
                        'co_par' => trim($row['rf_parentesco']),
                        'co_ciu' => trim($row['dr_ciudad']),
                        'co_pro' => trim($row['dr_provincia'])
                    );
                }
            }
        }

        $array = groupArray($array, 'co_reg');

        return $array;
    }

    function actualizaGestion($cod_cam, $cod_cli, $fec_car, $cod_est, $hor_ges, $cod_usu, $cod_ci, $observacion) {

        $array = array('co_err' => 0, 'tx_err' => '');
        $error = '';

        if (!empty($cod_cam) && !empty($cod_cli) && !empty($fec_car) && !empty($cod_est) && !empty($cod_usu) && !empty($cod_ci)) {

            $Sql = " NTS_INTERCAMBIO.dbo.sp_act_datos_update_gestion " . (int) $cod_cam . "," . (int) $cod_cli . ",'" . trim($fec_car) . "','" . trim($cod_est) . "','" . trim($hor_ges) . "'," . (int) $cod_usu . ",'" . trim($cod_ci) . "','" . trim(utf8_decode($observacion)) . "';";
            //$this->db = $this->load->database('desarrollo_report', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL PROCESAR CLIENTE: " . $this->db->_error_number() . " " . $this->db->_error_message();
                $array['co_err'] = 1;
                $array['tx_err'] = $error;
            } else {

                $row = $result->row_array();

                if (!empty($row)) {
                    $array['co_err'] = $row['co_error'];
                    $array['tx_err'] = $row['tx_error'];
                }
            }
        } else {
            $array['co_err'] = 1;
            $array['tx_err'] = 'Parametros Incompletos';
        }

        return $array;
    }

    function eliminaTelefeno($tip_reg, $cod_cli, $tip_tel, $cod_sec, $cod_usu, $cod_secu_ref) {

        $array = array('co_err' => 0, 'tx_err' => '');
        $error = '';

        if (!empty($tip_reg) && !empty($cod_cli) && !empty($tip_tel) && !empty($cod_sec) && !empty($cod_usu)) {

            $Sql = " NTS_INTERCAMBIO.dbo.sp_act_datos_delete_telefono '" . $tip_reg . "'," . (int) $cod_cli . ",'" . trim($tip_tel) . "'," . (int) $cod_sec . ",'" . trim($cod_usu) . "',$cod_secu_ref;";
            //$this->db = $this->load->database('desarrollo_report', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL ELIMINAR TELEFONO: " . $this->db->_error_number() . " " . $this->db->_error_message();
                $array['co_err'] = 1;
                $array['tx_err'] = $error;
            } else {

                $row = $result->row_array();

                if (!empty($row)) {
                    $array['co_err'] = $row['co_error'];
                    $array['tx_err'] = $row['tx_error'];
                }
            }
        } else {
            $array['co_err'] = 1;
            $array['tx_err'] = 'Parametros Incompletos';
        }

        return $array;
    }

    function actualiza_insertaTelefeno($tip_reg, $cod_cli, $tip_tel, $num_tel, $ext_tel, $are_sec, $cod_usu, $cod_secu = 0) {

        $array = array('co_err' => 0, 'tx_err' => '');
        $error = '';

        if (!empty($tip_reg) && !empty($cod_cli) && !empty($tip_tel) && !empty($num_tel) && !empty($cod_usu)) {

            $Sql = " NTS_INTERCAMBIO.dbo.sp_act_datos_insert_telefono '" . $tip_reg . "'," . (int) $cod_cli . ",'" . trim($tip_tel) . "','" . trim($num_tel) . "','" . trim($ext_tel) . "','" . trim($are_sec) . "','" . trim($cod_usu) . "'," . trim($cod_secu) . ";";
            //$this->db = $this->load->database('desarrollo_report', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL ELIMINAR TELEFONO: " . $this->db->_error_number() . " " . $this->db->_error_message();
                $array['co_err'] = 1;
                $array['tx_err'] = $error;
            } else {

                $row = $result->row_array();

                if (!empty($row)) {
                    $array['co_err'] = $row['co_error'];
                    $array['tx_err'] = $row['tx_error'];
                }
            }
        } else {
            $array['co_err'] = 1;
            $array['tx_err'] = 'Parametros Incompletos';
        }

        return $array;
    }

    function actualizaReferencia($tip_reg, $cod_cli, $tip_tel, $num_tel, $ext_tel, $are_sec, $cod_usu, $cod_secu, $nom_ref, $ape_ref, $pare_ref, $ciud_ref, $prov_ref, $secu_ref) {

        $array = array('co_err' => 0, 'tx_err' => '');
        $error = '';

        if (!empty($tip_reg) && !empty($cod_cli) && !empty($tip_tel) && !empty($num_tel) && !empty($cod_usu) && !empty($secu_ref)) {

            $Sql = " NTS_INTERCAMBIO.dbo.sp_act_datos_insert_telefono '" . $tip_reg . "'," . (int) $cod_cli . ",'" . trim($tip_tel) . "','" . trim($num_tel) . "','" . trim($ext_tel) . "','" . trim($are_sec) . "','" . trim($cod_usu) . "'," . trim($cod_secu) . ",";
            $Sql .= "'" . trim(utf8_decode($nom_ref)) . "','" . trim(utf8_decode($ape_ref)) . "','" . trim($pare_ref) . "','" . trim($ciud_ref) . "','" . trim($prov_ref) . "',$secu_ref;";
            //$this->db = $this->load->database('desarrollo_report', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL ELIMINAR TELEFONO: " . $this->db->_error_number() . " " . $this->db->_error_message();
                $array['co_err'] = 1;
                $array['tx_err'] = $error;
            } else {

                $row = $result->row_array();

                if (!empty($row)) {
                    $array['co_err'] = $row['co_error'];
                    $array['tx_err'] = $row['tx_error'];
                }
            }
        } else {
            $array['co_err'] = 1;
            $array['tx_err'] = 'Parametros Incompletos';
        }

        return $array;
    }

    function autoasignarCliente($usuario) {

        $cliente = null;
        $respuesta = array('co_err' => 0, 'tx_err' => '');

        if (!empty($usuario)) {

            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });

            try {

                $Sql = " NTS_INTERCAMBIO.dbo.sp_act_datos_autoasignar_cliente '" . $usuario . "';";
                $conn = odbc_connect("MASTER-P", "pica", "");
                //$conn = odbc_connect("DESARROLLO-REPORT", "LINVENT", "LINVENT");
                $resultset = odbc_exec($conn, $Sql);

                do {

                    while ($row = odbc_fetch_array($resultset)) {

                        if (!empty($row['resultset'])) {

                            if (isset($row['resultset'])) {

                                switch ($row['resultset']) {

                                    case "GESTION":

                                        $respuesta['co_err'] = (int) $row['co_error'];
                                        $respuesta['tx_err'] = trim($row['tx_error']);

                                        break;

                                    case "CLIENTE":

                                        $cliente = $this->arrayTelefonos($row);

                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
            } catch (Exception $e) {
                $respuesta['co_err'] = 1;
                $respuesta['tx_err'] = "ERROR AL AUTOASIGNAR :" . $e->getMessage();
            }
        } else {
            $respuesta['co_err'] = 1;
            $respuesta['tx_err'] = 'Parametros Incompletos';
        }

        return array('err' => $respuesta, 'cab' => $cliente);
    }

}
?>




