<?php

class m_4002 extends CI_Model {

    public $estados = array(
        'C' => array(
            'name' => 'Contactado',
            'color' => '#00A65A'
        ),
        'I' => array(
            'name' => 'Incontactado',
            'color' => '#f0a30a'
        ),
        'P' => array(
            'name' => 'Pendiente',
            'color' => '#F56954'
        ),
        'E' => array(
            'name' => 'En proceso',
            'color' => '#2E8BEF'
        ),
        'A' => array(
            'name' => 'Agendado',
            'color' => '#633EBE'
        )
    );
    public $estadosDet = array(
        'N' => array(
            'name' => 'Pendiente',
            'color' => '#00A65A',
            'class' => 'label-danger'
        ),
        'S' => array(
            'name' => 'Grabado',
            'color' => '#F56954',
            'class' => 'label-success'
        )
    );
    public $tipoGestion = array(
        'E' => array(
            'name' => 'Equivocado',
            'color' => '#00A65A',
            'class' => 'label-danger'
        ),
        'I' => array(
            'name' => 'Incontactable',
            'color' => '#F56954',
            'class' => 'label-success'
        ),
        'F' => array(
            'name' => 'Sin Telefono',
            'color' => '#2E8BEF',
            'class' => 'label-success'
        )
    );

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    function retDashboardAdministracion($fe_ini, $fe_fin) {

        $pie = $promedios = $usuarios = null;

        $error = '';
        if (!empty($fe_ini) && !empty($fe_fin)) {

            try {

                $Sql = " NTS_INTERCAMBIO.dbo.sp_act_datos_ret_administrador_agentes '" . trim($fe_ini) . "','" . trim($fe_fin) . "';";

                $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
                $resultset = odbc_exec($conn, $Sql);

                if (!$resultset) {
                    throw new PDOException("Error");
                }

                if (odbc_error()) {
                    $error = "ERROR: AL RETORNAR DASHBOARD " . odbc_errormsg($conn);
                } else {

                    do {

                        while ($row = odbc_fetch_array($resultset)) {

                            if (isset($row['resultset'])) {

                                switch ($row['resultset']) {

                                    case "PIE":

                                        $pie[] = array(
                                            'co_est' => trim($row['de_estado']),
                                            'ds_est' => trim($this->estados[trim($row['de_estado'])]['name']),
                                            'ca_est' => (int) $row['cantidad'],
                                        );

                                        break;

                                    case "PROMEDIOS":

                                        $promedios = array(
                                            'ba_pro' => gmdate("H:i:s", $row['promedio']),
                                            'mi_pro' => gmdate("H:i:s", $row['minimo']),
                                            'ma_pro' => gmdate("H:i:s", $row['maximo']),
                                        );

                                        break;

                                    case "USUARIOS":

                                        $usuarios[] = array(
                                            'co_usu' => (int) $row['de_usuario'],
                                            'no_usu' => trim(utf8_encode($row['nombre'])),
                                            'no_car' => trim(utf8_encode($row['cargo'])),
                                            'ca_tot' => (int) $row['total'],
                                            'ca_pen' => (int) $row['cantidad_pendiente'],
                                            'ca_fin' => (int) $row['cantidad_finalizada']
                                        );

                                        break;
                                }
                            }
                        }
                    } while (odbc_next_result($resultset));

                    if (!empty($usuarios)) {

                        for ($i = 0; $i < count($usuarios); $i++) {

                            $user = $this->retDashboardUsuarioFecha($usuarios[$i]['co_usu'], $fe_ini, $fe_fin);

                            if (!empty($user)) {

                                $usuarios[$i]['pie'] = $this->agenteArrayPie($user);
                                $usuarios[$i]['est'] = $user;
                            }
                        }
                    }

                    $data = array(
                        'pie' => $this->agenteArrayPie($pie),
                        'pro' => $promedios,
                        'usu' => $usuarios
                    );
                }

                odbc_close($conn);
            } catch (Exception $e) {
                $error = "ERROR AL RETORNAR USUARIO.";
            }
        }

        return array('data' => $data, 'error' => $error);
    }

    function retListaFechaDeCarga() {

        $array = null;

        $Sql = " NTS_INTERCAMBIO..sp_act_datos_resumen_cargas; ";

        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            return "ERROR AL OBTENER LISTA DE CARGA: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        $resArray = $result->result_array();

        if (!empty($resArray)) {

            foreach ($resArray as $row) {

                $array[] = array(
                    'fe_car' => trim($row['fe_carga']),
                    'to_car' => (int) $row['total'],
                    'as_car' => (int) $row['asignado'],
                    'pe_car' => (int) $row['pendiente']
                );
            }
        }

        return $array;
    }

    function retDashboardUsuarioFecha($cod_user, $fec_ini, $fec_fin) {

        $array = null;

        if (!empty($cod_user) && !empty($fec_ini) && !empty($fec_fin)) {

            $Sql = " NTS_INTERCAMBIO..sp_act_datos_resumen_usuarios ".(int) $cod_user.",'" . trim($fec_ini) . "','" . trim($fec_fin) . "'; ";

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL DETALLE USUARIOS: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            $resArray = $result->result_array();

            if (!empty($resArray)) {

                foreach ($resArray as $row) {

                    $array[] = array(
                        'co_est' => trim($row['de_estado']),
                        'ds_est' => trim($this->estados[trim($row['de_estado'])]['name']),
                        'ca_est' => (int) $row['cantidad']
                    );
                }
            }
        }


        return $array;
    }

    function agenteArrayPie($array) {

        $pie = null;

        if (!empty($array)) {
            foreach ($array as $row) {
                $pie [] = array(
                    'label' => $row['ds_est'] . ' <b>' . number_format($row['ca_est'], 0) . '</b>',
                    'data' => (int) $row['ca_est'],
                    'color' => $this->estados[trim($row['co_est'])]['color'],
                );
            }
        }

        return $pie;
    }

    function agenteArrayBar($array) {

        $data = $arrayHora = null;

        if (!empty($array)) {

            $bar = null;

            for ($i = 0; $i < count($array); $i++) {
                $arrayHora[] = array($i, str_replace(':00', 'h', $array[$i]['ho_cha']));
                $bar[] = array($i, $array[$i]['ca_cha']);
            }

            $data = array(
                'color' => '#5482FF',
                'data' => $bar
            );
        }

        return array('data' => array($data), 'ticks' => $arrayHora);
    }

    function retUsuaariosAsignar() {

        $array = null;

        $Sql = " SELECT a.COD_CCOSTO,a.COD_CARGO,b.NOMBRE+'' AS NO_CARGO,a.CODIGO,c.APELLIDO,c.NOMBRE ";
        $Sql .= " FROM [PYCCA-APP1].PERSONAL_PI.DBO.rh_employee a ";
        $Sql .= " INNER JOIN NTS_INTERCAMBIO..ic_GestoresActualizaDatos z ";
        $Sql .= " ON z.co_empleado = a.CODIGO ";
        $Sql .= " AND z.co_estado = 1 ";
        $Sql .= " INNER JOIN [PYCCA-APP1].PERSONAL_PI.DBO.RH_CARGO b ";
        $Sql .= " ON b.COD_CARGO = a.COD_CARGO ";
        $Sql .= " INNER JOIN [PYCCA-APP1].PERSONAL_PI.DBO.RH_PERSON c ";
        $Sql .= " ON a.CODIGO = c.CODIGO ";
        $Sql .= " WHERE a.COD_EMPRESA = 27 ";
        $Sql .= " AND a.COD_ESTADO_EMP = 1 ";
        $Sql .= " ORDER BY c.APELLIDO,c.NOMBRE;";

        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            return "ERROR AL RETORNAR USUARIOS: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        $resArray = $result->result_array();

        if (!empty($resArray)) {

            foreach ($resArray as $row) {

                $array[] = array(
                    'co_ccs' => (int) $row['COD_CCOSTO'],
                    'co_car' => (int) $row['COD_CARGO'],
                    'no_car' => trim(utf8_encode($row['NO_CARGO'])),
                    'co_usu' => (int) $row['CODIGO'],
                    'ap_usu' => trim(utf8_encode($row['APELLIDO'])),
                    'no_usu' => trim(utf8_encode($row['NOMBRE']))
                );
            }
        }

        return $array;
    }

    function asignarUsuarios($usuarios, $cantidad, $tipo) {

        $array = array('co_err' => '', 'tx_err' => '');
        $error = '';

        if (!empty($usuarios) && !empty($tipo) && $cantidad > 0) {

            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;

            ##<usuarios>

            $user = $dom->createElement('usuarios');
            $dom->appendChild($user);

            for ($i = 0; $i < count($usuarios); $i++) {

                $items = $dom->createElement("items");
                $user->appendChild($items);
                $items->appendChild($dom->createElement("codigo", (int) $usuarios[$i]));
            }

            ##</usuarios>

            $xml = $dom->saveXML();

            $Sql = " NTS_INTERCAMBIO..sp_act_datos_asigna_usuarios '$xml','" . trim($tipo) . "',$cantidad;";

            $result = $this->db->query($Sql);

            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL ASIGNAR COLABORADORES: " . $this->db->_error_number() . " " . $this->db->_error_message();
                $array['co_err'] = 1;
                $array['tx_err'] = $error;
            } else {

                $row = $result->row_array();

                if (!empty($row)) {
                    $array['co_err'] = $row['co_error'];
                    $array['tx_err'] = $row['tx_error'];
                }
            }
        } else {
            $array['co_err'] = 1;
            $array['tx_err'] = 'Parametros Incompletos';
        }

        return $array;
    }

    function cambioUsuarioAsignacion($fe_ini, $fe_fin, $usuario, $usuario_a_cambio, $estado) {

        $array = array('co_err' => '', 'tx_err' => '');
        $error = '';

        if (!empty($fe_ini) && !empty($fe_fin) && $usuario > 0 && $usuario_a_cambio > 0 && !empty($estado)) {

            $Sql = " NTS_INTERCAMBIO..sp_act_datos_asigna_usuarios_cambio '" . trim($fe_ini) . "','" . trim($fe_fin) . "',$usuario,$usuario_a_cambio,'" . trim($estado) . "';";
            $result = $this->db->query($Sql);

            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL GRABAR CAMBIO DE ASIGNACION: " . $this->db->_error_number() . " " . $this->db->_error_message();
                $array['co_err'] = 1;
                $array['tx_err'] = $error;
            } else {

                $row = $result->row_array();

                if (!empty($row)) {
                    $array['co_err'] = $row['co_error'];
                    $array['tx_err'] = $row['tx_error'];
                }
            }
        } else {
            $array['co_err'] = 1;
            $array['tx_err'] = 'Parametros Incompletos';
        }

        return $array;
    }

    function retReporteAsignado($fe_ini, $fe_fin) {

        $array = null;

        if (!empty($fe_ini) && !empty($fe_fin)) {

            $Sql = " NTS_INTERCAMBIO..sp_act_datos_reporte_excel '" . trim($fe_ini) . "','" . trim($fe_fin) . "';";
            
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR REPORTE: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            $resArray = $result->result_array();

            if (!empty($resArray)) {

                foreach ($resArray as $row) {

                    $array[] = array(
                        'fe_car' => trim($row['fe_gestion']),
                        'ti_ges' => trim($row['tipoGestion']),
                        'co_cam' => (int) $row['co_campana'],
                        'de_usu' => (int) $row['de_usuario'],
                        'no_usu' => trim(utf8_encode($row['nombre_usuario'])),
                        'co_cli' => (int) $row['co_cliente'],
                        'ce_cli' => trim($row['cl_identifica']),
                        'no_cli' => trim(utf8_encode($row['cl_nombrelargo'])),
                        'va_deu' => number_format($row['va_Deuda'], 2),
                        'fe_asg' => trim($row['fe_asignado']),
                        'ho_ini' => !empty($row['nu_HoraInicio']) ? date('H:is', strtotime($row['nu_HoraInicio'])) : '',
                        'ho_fin' => !empty($row['nu_HoraFin']) ? date('H:is', strtotime($row['nu_HoraFin'])) : '',
                        'de_obs' => trim(utf8_encode($row['de_observacion'])),
                        'de_est' => trim($row['de_estado'])
                    );
                }
            }
        }

        return $array;
    }

}
?>




