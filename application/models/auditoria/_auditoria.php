<?php

class _auditoria extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    public $estados = array(
        'G' => array(
            'name' => 'GENERADO',
            'color' => '#FFCE54'
        ),
        'C' => array(
            'name' => 'CONTEO',
            'color' => '#FC6E51'
        ),
        'S' => array(
            'name' => 'CONSOLIDADO',
            'color' => '#A0D468'
        ),
        'V1' => array(
            'name' => 'VERIFICADO AUDITORIA',
            'color' => '#48CFAD'
        ),
        'V2' => array(
            'name' => 'VERIFICADO ADMINISTRADOR',
            'color' => '#4FC1E9'
        ),
        'V3' => array(
            'name' => 'PROCESADO POR AUDITOR',
            'color' => '#5D9CEC'
        ),
        'V4' => array(
            'name' => 'AUTORIZADO GERENCIA',
            'color' => '#CCD1D9'
        ),
        'VC' => array(
            'name' => 'RECUENTO CONSOLIDADO',
            'color' => '#AC92EC'
        ),
        'X' => array(
            'name' => 'TOMA ELIMINADA',
            'color' => '#ED5565'
        ),
        'A' => array(
            'name' => 'AUTORIZADO GERENCIA',
            'color' => '#EC5F67'
        ),
        'R' => array(
            'name' => 'REVERSO',
            'color' => '#85C5C2'
        )
    );

    function _retAlmacenes($usuario) {
        $array = null;
        $error = '';
        if (!empty($usuario)) {
            $Sql = " IVDBINVENTAR.audit._get_almacenes '" . $usuario . "'; ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = & load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR ALMACENES <BR> COD_ERR:" . $this->db->_error_number(), $this->db->_error_message(), 'error_db');
                die;
            } else {
                $rs = $result->result_array();
                if (!empty($rs)) {
                    foreach ($rs as $row) {
                        $array[] = array(
                            'codigo' => $row['co_bodega'],
                            'nombre' => utf8_decode($row['descripcion']),
                        );
                    }
                }
            }
        }
        return $array;
    }

    function retDashboardAdministracion($fe_ini, $fe_fin) {
        $pie = $auditor = null;
        $error = '';
        if (!empty($fe_ini) && !empty($fe_fin)) {
            try {
                $pie = $this->retAdminPorEstado($fe_ini, $fe_fin);
                $auditor = $this->retAdminAsignacion($fe_ini, $fe_fin);
                if (!empty($auditor)) {
                    for ($i = 0; $i < count($auditor); $i++) {
                        $auditor[$i]['pie'] = $this->agenteArrayPie($auditor[$i]['groupeddata']);
                        $auditor[$i]['res'] = $this->retResumenAuditor($auditor[$i]['co_aud'], $fe_ini, $fe_fin);
                    }
                }
                $data = array(
                    'pie' => $this->agenteArrayPie($pie),
                    'aud' => $auditor
                );
            } catch (Exception $e) {
                $error = "ERROR AL RETORNAR USUARIO.";
                log_P($e->getCode(), $e->getMessage());
            }
        }
        return array('data' => $data, 'error' => $error);
    }

    function retResumenAuditor($co_auditor, $fe_ini, $fe_fin) {
        $array = array('dat' => '', 'err' => '');
        if ((int) $co_auditor > 0) {
            try {
                $Sql = " IVDBINVENTAR.audit.resumen_tomas_auditor " . (int) $co_auditor . ",'" . trim($fe_ini) . " 00:00','" . trim($fe_fin) . " 23:59'; ";
                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_message() . " " . $this->db->_error_number());
                } else {
                    $rs = $result->result_array();
                    if (!empty($rs)) {
                        foreach ($rs as $row) {
                            $array['dat'][] = array(
                                'co_est' => trim($row['co_estado']),
                                'ti_min' => (int) $row['tiempo_minutos'],
                                'to_art' => (int) $row['total_articulos'],
                                'su_art' => (int) $row['suma_articulos']
                            );
                        }
                    }
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL RESUMEN ARTICULO:" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function retAdminAsignacion($fec_ini, $fec_fin) {
        $array = null;
        $Sql = " IVDBINVENTAR.audit._visor_admin_asignacion '" . $fec_ini . "','" . $fec_fin . "'; ";
        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            return "ERROR AL OBTENER ASIGNACIONES: " . $this->db->_error_number() . " " . $this->db->_error_message();
        } else {
            $resArray = $result->result_array();
            if (!empty($resArray)) {
                foreach ($resArray as $row) {
                    $array[] = array(
                        'co_aud' => (int) $row['co_auditor'],
                        'no_aud' => trim(utf8_encode($row['nombre'])),
                        'no_car' => trim(utf8_encode($row['cargo'])),
                        'co_est' => trim($row['co_estado']),
                        'ds_est' => trim($row['tx_descripcion']),
                        'ca_est' => (int) $row['cantidad']
                    );
                }
            }
        }
        return groupArray($array, 'co_aud');
    }

    function retAdminPorEstado($fec_ini, $fec_fin) {
        $array = null;
        $Sql = " IVDBINVENTAR.audit._visor_admin_x_estado '" . $fec_ini . "','" . $fec_fin . "'; ";
        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            return "ERROR AL OBTENER ESTADOS: " . $this->db->_error_number() . " " . $this->db->_error_message();
        } else {
            $resArray = $result->result_array();
            if (!empty($resArray)) {
                foreach ($resArray as $row) {
                    $array[] = array(
                        'co_est' => trim($row['co_estado']),
                        'ds_est' => trim($row['tx_descripcion']),
                        'ca_est' => (int) $row['va_cantidad']
                    );
                }
            }
        }
        return $array;
    }

    function retListadosToma($fe_ini, $fe_fin, $co_est, $co_usu) {
        $array = array('err' => '', 'data' => '');
        if (!empty($fe_ini) && !empty($fe_fin) && !empty($co_est)) {
            try {
                if (!empty($co_usu)) {
                    $Sql = " IVDBINVENTAR.audit._visor_admin_listado '" . $fe_ini . "','" . $fe_fin . "','" . $co_est . "',$co_usu; ";
                } else {
                    $Sql = " IVDBINVENTAR.audit._visor_admin_listado '" . $fe_ini . "','" . $fe_fin . "','" . $co_est . "'; ";
                }
                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                } else {
                    $resArray = $result->result_array();
                    if (!empty($resArray)) {
                        foreach ($resArray as $row) {
                            $html = '<button type="button" style="margin-right: 5px;" onclick="viewDetToma(' . $row['co_bodega'] . ',' . $row['co_listado'] . ',0,' . "'" . trim($row['co_estado']) . "'" . ')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-check-square-o"></i></button>';
                            $html .= '<button type="button" style="margin-right: 5px;" onclick="fn_generarInforme(' . $row['co_bodega'] . ',' . $row['co_listado'] . ')" class="btn btn-circle btn-warning"><i class="text-blanco fa fa-send"></i></button>';
                            if ($co_est <> 'A' && $co_est <> 'X' && $co_est <> 'R') {
                                $html .= '<button type="button" onclick="delToma(' . $row['co_bodega'] . ',' . $row['co_listado'] . ')" class="btn btn-circle btn-danger"><i class="text-blanco fa fa-close"></i></button>';
                            }
                            $array['data'][] = array(
                                'co_bdg' => (int) $row['co_bodega'],
                                'no_bdg' => trim(utf8_encode($row['tx_bodega'])),
                                'co_lis' => (int) $row['co_listado'],
                                'fe_gen' => !empty($row['fe_generacion']) ? date('Y-m-d H:i:s', strtotime($row['fe_generacion'])) : '',
                                'co_tip' => trim($row['tipo']),
                                'tx_des' => trim(utf8_encode($row['tx_descripcion'])),
                                'co_est' => trim($row['co_estado']),
                                'no_est' => trim($row['no_estado']),
                                'ca_ite' => (int) $row['items'],
                                'html' => $html
                            );
                        }
                    }
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL OBTENER LISTADOS TOMA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = 'PARAMETROS INCOMPLETOS';
        }
        return $array;
    }

    function _deshabilitaToma($bodega, $listado) {
        $array = array('co_err' => 1, 'tx_err' => '');
        if ($bodega > 0 && $listado > 0) {
            try {
                $Sql = " IVDBINVENTAR.audit._actualiza_estado_toma $bodega,$listado,'X'; ";
                $resultado = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                } else {
                    $row = $resultado->row_array();
                    if (!empty($row)) {
                        $array['co_err'] = (int) $row['co_error'];
                        $array['tx_err'] = trim($row['tx_error']);
                    }
                }
            } catch (Exception $e) {
                $array['co_err'] = 1;
                $array['tx_err'] = "ERROR AL OBTENER LISTADOS TOMA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['tx_err'] = 'PARAMETROS INCOMPLETOS';
        }
        return $array;
    }

    function retToma($co_bodega, $co_listado) {
        $info = $articulos = $estructuras = $auditores = null;
        $error = '';
        if (!empty($co_bodega) && !empty($co_listado)) {
            try {
                $Sql = " IVDBINVENTAR.audit._visor_admin_listado_detalle " . trim($co_bodega) . "," . trim($co_listado) . ";";
                $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
                $resultset = odbc_exec($conn, $Sql);
                if (odbc_error()) {
                    throw new Exception(odbc_errormsg($conn));
                }
                do {
                    while ($row = odbc_fetch_array($resultset)) {
                        if (!empty($row['resultset'])) {
                            if (isset($row['resultset'])) {
                                switch ($row['resultset']) {
                                    case "INFO":
                                        $info = array(
                                            'no_alm' => trim(utf8_encode($row['almacen'])),
                                            'ds_tom' => trim(utf8_encode($row['descripcion'])),
                                            'ds_tip' => trim($row['tipo'])
                                        );
                                        break;
                                    case "ARTICULOS":
                                        $arrArt = null;
                                        $a = 0;
                                        foreach ($row as $clave => $valor) {
                                            $a++;
                                            if ($a > 8) {
                                                $arrArt['aud_' . $clave] = $valor;
                                            } else {
                                                $arrArt[$clave] = $valor;
                                            }
                                        }
                                        $articulos[] = $arrArt;
                                        break;
                                    case "ESTRUCTURA":
                                        $estructuras[] = array(
                                            'id' => (int) str_replace('-', '', $row['cod_orga']),
                                            'parent' => (int) str_replace('-', '', $row['padre']) == 0 ? '#' : (int) str_replace('-', '', $row['padre']),
                                            'text' => utf8_encode($row['des_orga']),
                                            'icon' => (int) $row['padre'] == 1 ? site_url('img/icon-user.png') : '',
                                            'state' => array("disabled " => "false")
                                        );
                                        break;
                                    case "AUDITORES":
                                        $auditores[] = array(
                                            'co_aud' => trim($row['co_auditor']),
                                            'ds_aud' => trim(utf8_decode($row['ds_auditor'])),
                                        );
                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
                odbc_close($conn);
            } catch (Exception $e) {
                $error = "ERROR AL RETORNAR TOMA.";
                log_P($e->getCode(), $e->getMessage());
            }
        }
        return array('inf' => $info, 'art' => $articulos, 'est' => $estructuras, 'aud' => $auditores, 'err' => $error);
    }

    function agenteArrayPie($array) {
        $pie = null;
        if (!empty($array)) {
            foreach ($array as $row) {
                $pie [] = array(
                    'label' => $row['co_est'] . '*|*' . $row['ds_est'] . ' <b>' . number_format($row['ca_est'], 0) . '</b>',
                    'data' => (int) $row['ca_est'],
                    'color' => $this->estados[trim($row['co_est'])]['color'],
                );
            }
        }
        return $pie;
    }

    function _retDetalleListadoLider($cod_almacen, $cod_listado, $co_ususario, $tipo) {

        $ubicaciones = $this->retUbicaciones();
        $ubicaciones = !empty($ubicaciones['ubi']) ? $ubicaciones['ubi'] : null;

        $info = $keys = $articulos = $sobrantes = null;
        $error = '';

        if (!empty($cod_almacen) && !empty($cod_listado) && !empty($co_ususario)) {

            try {

                $Sql = " IVDBINVENTAR.audit._visor_admin_listado_detalle " . $cod_almacen . "," . $cod_listado . "," . $co_ususario . "; ";
                $conn = odbc_connect("MASTER-P", "pica", "");
                $resultset = odbc_exec($conn, $Sql);
                if (odbc_error()) {
                    throw new Exception(odbc_errormsg($conn));
                }
                do {

                    while ($row = odbc_fetch_array($resultset)) {

                        if (!empty($row['resultset'])) {
                            if (isset($row['resultset'])) {

                                switch ($row['resultset']) {

                                    case "INFO":

                                        $info = array(
                                            'no_alm' => trim(utf8_decode($row['almacen'])),
                                            'ds_tom' => trim(utf8_decode($row['descripcion'])),
                                            'co_est' => trim($row['co_estado']),
                                            'co_tip' => (int) $row['co_tipo'],
                                            'ds_tip' => trim($row['tipo']),
                                            'da_ecv' => (int) $row['tiene_vc']
                                        );

                                        break;

                                    case "ARTICULOS":

                                        $arrArt = null;
                                        $ds_art = trim(utf8_encode(str_replace(array('"', "'"), array('', ''), $row['ds_articulo'])));
                                        $parametros = $cod_almacen . '-' . $cod_listado . '-' . $info['co_est'] . '-' . $info['co_tip'];

                                        $keys = array(
                                            'co_art' => trim($row['co_articulo']),
                                            'ds_art' => $ds_art,
                                            'co_fab' => trim(utf8_encode($row['co_fabrica'])),
                                            'fl_rev_user' => trim($row['usuario']),
                                            'fl_rev_user_ave' => trim($row['usuario_averiado']),
                                            'fl_rev_aud' => trim($row['fl_revision_auditor']),
                                            'fl_rev' => trim($row['fl_revision_auditor']),
                                            'fl_ger' => trim($row['fl_revision_gerente']),
                                            'fl_rev_ave' => trim($row['fl_revision_auditor_averiado']),
                                            'fl_ger_ave' => trim($row['fl_revision_gerente_averiado']),
                                            'va_sis' => $row['va_saldo_sistema'],
                                            'to_est' => $row['total_por_estado'],
                                            'va_con' => $row['va_consolidado'],
                                            'va_con_sum' => 0,
                                            'va_dif' => $row['diferencia'],
                                            'va_dif2' => $row['diferencia2'],
                                            'va_cos' => $row['costo'],
                                            'va_pvp' => $row['pvp'],
                                            'va_tot_cos' => $row['tot_costo'],
                                            'va_tot_pvp' => $row['tot_pvp'],
                                            'va_tot_dif_cos' => $row['tot_dif_costo'],
                                            'va_tot_dif_pvp' => $row['tot_dif_pvp'],
                                            'va_ant' => (int) $row['va_antiguedad'],
                                            'va_sal_vir' => (int) $row['va_saldo_virtual'],
                                            'abr' => trim($row['abrev']),
                                            'va_vir_hist' => $row['va_bodega_virtual_hist'],
                                            'f4' => (int) $row['f4'],
                                            'html_f4' => (int) $row['f4'] > 0 ? '<span class="label label-danger" style="padding: 2px;">F4</span>' : '',
                                            'html_transito' => (int) $row['transito'] > 0 ? '<span class="label label-success" style="padding: 2px;">TR</span>' : '',
                                            '_link' => '<a href="javascript:void(0)" onclick="consArt(' . "'" . htmlentities(json_encode($info)) . "'" . ',' . "'" . trim($row['co_articulo']) . "'" . ',' . "'" . trim(utf8_encode($ds_art)) . "'" . ',' . "'" . trim($parametros) . "'" . ');">' . trim($row['co_articulo']) . '</a>'
                                                //'_link' => '<a href="javascript:void(0)" onclick="consArt(' . "'" . htmlentities(json_encode($info)) . "'" . ',' . "'" . trim($row['co_articulo']) . "'" . ',' . "'" . trim(utf8_decode($ds_art)) . "'" . ',' . "'" . trim($parametros) . "'" . ');">' . trim($row['co_articulo']) . '</a>'
                                                //'link' => '<a href="javascript:void(0)" onclick="consArt(' + "'" + encodeURIComponent(JSON.stringify(c.inf)).replace(/['"]/g, "-") + "'" + ',' + "'" + art.co_art + "'" + ',' + "'" + art.ds_art + "'" + ',' + "'" + cmb + "'" + ');">' + art.co_art + '</a>'
                                        );

                                        if (isset($keys)) {
                                            foreach ($row as $clave => $valor) {
                                                $clave = (int) $clave;
                                                if ($clave > 0) {
                                                    $keys['va_con_sum'] += $valor;
                                                    $arrArt['ubica_' . $clave . '|' . ( isset($ubicaciones[$clave]) ? $ubicaciones[$clave]['ds_ubi'] : '')] = (int) $valor;
                                                }
                                            }

                                            $articulos[] = array_merge($keys, $arrArt);
                                        }

                                        break;

                                    case "SOBRANTES":

                                        $sobrantes[] = array(
                                            'co_art' => trim($row['co_articulo']),
                                            'ds_art' => trim(utf8_decode($row['ds_articulo'])),
                                            'co_fab' => trim(utf8_decode($row['co_fabrica'])),
                                            'va_can' => $row['va_cantidad'],
                                            'va_sis' => $row['va_saldo_sistema'],
                                            'va_cos' => $row['costo'],
                                            'va_pvp' => $row['pvp'],
                                            'va_tot_cos' => $row['tot_costo'],
                                            'va_tot_pvp' => $row['tot_pvp']
                                        );

                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
            } catch (Exception $e) {
                $error = "ERROR AL OBTENER DETALLE DE TOMA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $error = "PARAMETROS INCOMPLETOS";
        }

        return array('inf' => $info, 'art' => $articulos, 'sob' => $sobrantes, 'err' => $error);
    }

    function _retDetalleListadoLiderEXTRANET($cod_almacen, $cod_listado, $co_ususario) {

        $ubicaciones = $this->retUbicaciones();
        $ubicaciones = !empty($ubicaciones['ubi']) ? $ubicaciones['ubi'] : null;

        $info = $keys = $articulos = $sobrantes = $f4 = $transito = null;
        $error = '';

        if (!empty($cod_almacen) && !empty($cod_listado) && !empty($co_ususario)) {
            try {
                $Sql = " IVDBINVENTAR.audit._lider_listado_detalle_asignado " . $cod_almacen . "," . $cod_listado . "," . $co_ususario . "; ";
                $conn = odbc_connect("MASTER-P", "pica", "");
                $resultset = odbc_exec($conn, $Sql);
                if (odbc_error()) {
                    throw new Exception(odbc_errormsg($conn));
                }
                do {
                    while ($row = odbc_fetch_array($resultset)) {
                        if (!empty($row['resultset'])) {
                            if (isset($row['resultset'])) {
                                switch ($row['resultset']) {
                                    case "INFO":
                                        $info = array(
                                            'no_alm' => trim(utf8_decode($row['almacen'])),
                                            'ds_tom' => trim(utf8_decode($row['descripcion'])),
                                            'co_tip' => (int) $row['co_tipo'],
                                            'ds_tip' => trim($row['tipo']),
                                            'da_ecv' => (int) $row['tiene_vc']
                                        );
                                        break;
                                    case "ARTICULOS":
                                        $ds_art = trim(utf8_decode(str_replace(array('"', "'"), array('', ''), $row['ds_articulo'])));

                                        $keys = array(
                                            'co_art' => trim($row['co_articulo']),
                                            'ds_art' => $ds_art,
                                            'co_fab' => trim(utf8_decode($row['co_fabrica'])),
                                            'co_upc' => trim($row['upc']),
                                            'va_sis' => (int) $row['va_saldo_sistema'],
                                            'va_sis_hist' => (int) $row['va_saldo_sistema_hist'],
                                            'va_vir_hist' => $row['va_bodega_virtual_hist'],
                                            'to_est' => (int) $row['total_por_estado'],
                                            'va_con' => (int) $row['va_consolidado'],
                                            'va_dif' => (int) $row['diferencia'],
                                            'va_dif2' => (int) $row['diferencia2'],
                                            'va_cos' => $row['costo'],
                                            'va_pvp' => $row['pvp'],
                                            'va_tot_cos' => $row['tot_costo'],
                                            'va_tot_pvp' => $row['tot_pvp'],
                                            'va_tot_dif_cos' => $row['tot_dif_costo'],
                                            'va_tot_dif_pvp' => $row['tot_dif_pvp'],
                                            'va_ant' => (int) $row['va_antiguedad'],
                                            'va_sal_vir' => (int) $row['va_saldo_virtual'],
                                            'obs' => trim(utf8_encode($row['observacion']), ','),
                                            'abr' => trim($row['abrev']),
                                            'f4' => (int) $row['f4'],
                                            'html_f4' => (int) $row['f4'] > 0 ? '<span class="label label-danger" style="padding: 2px;">F4</span>' : '',
                                            'html_transito' => (int) $row['transito'] > 0 ? '<span class="label label-success" style="padding: 2px;">TR</span>' : '',
                                            '_link' => '<a href="javascript:void(0)" onclick="consArt(' . "'" . htmlentities(json_encode($info)) . "'" . ',' . "'" . trim($row['co_articulo']) . "'" . ',' . "'" . trim(utf8_decode($ds_art)) . "'" . ',' . $row['va_consolidado'] . ');">' . trim($row['co_articulo']) . '</a>'
                                                //'link' => '<a href="javascript:void(0)" onclick="consArt(' + "'" + encodeURIComponent(JSON.stringify(c.inf)).replace(/['"]/g, "-") + "'" + ',' + "'" + art.co_art + "'" + ',' + "'" + art.ds_art + "'" + ',' + "'" + cmb + "'" + ');">' + art.co_art + '</a>'
                                        );
                                        $arrArt = null;

                                        foreach ($row as $clave => $valor) {

                                            $clave = (int) $clave;

                                            if ($clave > 0) {
                                                $arrArt['ubica_' . $clave . '|' . ( isset($ubicaciones[$clave]) ? $ubicaciones[$clave]['ds_ubi'] : '')] = (int) $valor;
                                            }
                                        }

                                        $articulos[] = array_merge($keys, $arrArt);

                                        break;

                                    case "SOBRANTES":

                                        $sobrantes[] = array(
                                            'co_art' => trim($row['co_articulo']),
                                            'ds_art' => trim(utf8_decode($row['ds_articulo'])),
                                            'co_fab' => trim(utf8_decode($row['co_fabrica'])),
                                            'va_can' => (int) $row['va_cantidad'],
                                            'va_sis' => (int) $row['va_saldo_sistema'],
                                            'va_cos' => $row['costo'],
                                            'va_pvp' => $row['pvp'],
                                            'va_tot_cos' => $row['tot_costo'],
                                            'va_tot_pvp' => $row['tot_pvp']
                                        );

                                        break;

                                    case "F4":

                                        $f4[] = array(
                                            'co_art' => trim($row['co_articulo']),
                                            'ds_art' => trim(utf8_encode($row['ds_articulo'])),
                                            'co_ret' => (int) $row['co_retiro'],
                                            'co_ced' => trim($row['co_cedula']),
                                            'no_cli' => trim(utf8_encode($row['no_cliente'])),
                                            'co_mot' => (int) $row['co_motivo'],
                                            'ds_mot' => trim(utf8_encode($row['de_motivo'])),
                                            'de_sri' => trim($row['de_sri']),
                                            'co_caj' => (int) $row['co_caja'],
                                            'nu_doc' => (int) $row['nu_documento'],
                                            'va_can' => (int) $row['va_cantidad'],
                                            'fe_reg' => !empty($row['fe_registro']) ? date('Y-m-d h:i:s', strtotime($row['fe_registro'])) : '',
                                            'fe_com' => !empty($row['fe_compra']) ? date('Y-m-d', strtotime($row['fe_compra'])) : ''
                                        );

                                        break;

                                    case "TRANSITO":

                                        $transito[] = array(
                                            'co_art' => trim($row['co_articulo']),
                                            'ds_art' => trim(utf8_encode($row['ds_articulo'])),
                                            'co_ped' => (int) $row['co_pedido'],
                                            'tx_sal' => trim(utf8_encode($row['tx_despacha'])),
                                            'tx_des' => trim(utf8_encode($row['tx_destino'])),
                                            'fe_des' => !empty($row['fe_despacho']) ? date('Y-m-d', strtotime($row['fe_despacho'])) : '',
                                            'ca_ped' => (int) $row['cantpedido'],
                                            'ca_des' => (int) $row['cantdespachado'],
                                        );

                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
            } catch (Exception $e) {
                $error = "ERROR AL OBTENER DETALLE DE TOMA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        }

        return array('inf' => $info, 'art' => $articulos, 'sob' => $sobrantes, 'f4' => $f4, 'tra' => $transito, 'err' => $error);
    }

    function retDetalleArticuloListado($cod_almacen, $cod_listado, $tipo, $co_ususario, $co_articulo) {
        $historia = null;
        $array = array('lis' => '', 'his' => '', 'err' => '');
        if (!empty($cod_almacen) && !empty($cod_listado) && !empty($co_articulo) && !empty($tipo) && !empty($co_ususario)) {
            try {
                $Sql = " IVDBINVENTAR.audit._consulta_listado_detalle_articulo_asignado " . $cod_almacen . "," . $cod_listado . "," . $co_ususario . ",'" . $co_articulo . "'; ";
                $conn = odbc_connect("MASTER-P", "pica", "");
                $resultset = odbc_exec($conn, $Sql);
                if (odbc_error()) {
                    throw new Exception(odbc_errormsg($conn));
                }
                do {
                    while ($row = odbc_fetch_array($resultset)) {

                        if (!empty($row['resultset'])) {

                            if (isset($row['resultset'])) {

                                switch ($row['resultset']) {

                                    case "CONSOLIDADO":

                                        $array['lis'][] = array(
                                            'co_ubi' => (int) $row['co_ubicacion'],
                                            'ds_ubi' => trim(utf8_decode($row['tx_descripcion'])),
                                            'va_con' => (int) $row['va_conteo'],
                                            'va_for' => trim($row['va_formula']),
                                            'ds_obs' => trim($row['tx_observacion']),
                                            'va_conso' => (int) $row['va_consolidado'],
                                            'va_salsi' => (int) $row['va_saldo_sistema']
                                        );

                                        break;

                                    case "HISTORIA":

                                        $historia[] = array(
                                            'co_aud' => (int) $row['co_auditor'],
                                            'no_aud' => trim(utf8_decode($row['no_completo'])),
                                            'co_est' => trim($row['co_estado']),
                                            'no_est' => trim(utf8_decode($row['no_estado'])),
                                            'co_ubi' => (int) $row['co_ubicacion'],
                                            'no_ubi' => trim(utf8_decode($row['no_ubicacion'])),
                                            'va_can' => (int) $row['va_cantidad'],
                                            'va_for' => trim($row['va_formula']),
                                            'va_obs' => trim(utf8_encode($row['tx_observacion']))
                                        );

                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));

                if (!empty($historia)) {

                    $por_estado = groupArray($historia, 'co_est');

                    foreach ($por_estado as $key => $row) {

                        $usuarios = null;
                        $array['his'][$key] = array(
                            'co_est' => $row['co_est'],
                            'no_est' => $row['groupeddata'][0]['no_est']
                        );

                        $por_usuario = groupArray($row['groupeddata'], 'co_aud');

                        foreach ($por_usuario as $aud) {

                            $clean = array_map(
                                    function (array $elem) {
                                unset($elem['no_aud']);
                                return $elem;
                            }, $aud['groupeddata']
                            );
                            $usuarios[] = array(
                                'co_aud' => $aud['co_aud'],
                                'no_aud' => $aud['groupeddata'][0]['no_aud'],
                                'data' => $clean
                            );
                        }
                        $array['his'][$key]['data'] = $usuarios;
                    }
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL OBTENER ARTICULO:" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = 'PARAMETROS INCOMPLETOS';
        }
        return $array;
    }

    function retUbicaciones() {
        $array = array('ubi' => '', 'err' => '');
        try {
            $Sql = " SELECT co_ubicacion,tx_descripcion FROM ivdbinventar.audit.ubicaciones; ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                throw new Exception($this->db->_error_message() . " " . $this->db->_error_number());
            } else {
                $rs = $result->result_array();
                if (!empty($rs)) {
                    foreach ($rs as $row) {
                        $array['ubi'][(int) $row['co_ubicacion']] = array(
                            'co_ubi' => (int) $row['co_ubicacion'],
                            'ds_ubi' => trim(utf8_decode($row['tx_descripcion']))
                        );
                    }
                }
            }
        } catch (Exception $e) {
            $array['err'] = "ERROR AL OBTENER UBICACION:" . $e->getMessage();
            log_P($e->getCode(), $e->getMessage());
        }
        return $array;
    }

    function registrarProductos($datos, $cod_almacen, $cod_listado, $co_ususario, $averiado) {
        $array = array('co_err' => 1, 'tx_err' => '');
        $datos = json_decode($datos, true);
        if (!empty($cod_almacen) && !empty($cod_listado) && !empty($datos) && !empty($co_ususario)) {
            try {
                $dom = new DOMDocument("1.0");
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                ##<data>
                $data = $dom->createElement('data');
                $dom->appendChild($data);
                ##<articulos>
                $articulos = $dom->createElement('articulos');
                $data->appendChild($articulos);
                foreach ($datos as $row) {
                    $items = $dom->createElement("items");
                    $articulos->appendChild($items);
                    $items->appendChild($dom->createElement("id_art", $row['c_s']));
                    $items->appendChild($dom->createElement("co_est", $row['e_s']));
                }
                ##<articulos>
                ##</data>
                $xml = $dom->saveXML();
                $Sql = "IVDBINVENTAR.audit.autorizar_productos_auditor " . $cod_almacen . " ," . $cod_listado . "," . $co_ususario . ",'" . $xml . "'," . (int) $averiado . "; ";
                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_message() . " " . $this->db->_error_number());
                } else {
                    $raw = $result->row_array();
                    $array['co_err'] = (int) $raw['co_error'];
                    $array['tx_err'] = utf8_decode($raw['tx_error']);
                }
            } catch (Exception $e) {
                $array['co_err'] = $e->getCode();
                $array['tx_err'] = "ERROR AL REGISTRAR ARTICULOS:" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['tx_err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function registrarProductosGerente($datos, $cod_almacen, $cod_listado, $co_ususario, $averiado) {
        $array = array('co_err' => 1, 'tx_err' => '', 'data' => null);
        $datos = json_decode($datos, true);
        if (!empty($cod_almacen) && !empty($cod_listado) && !empty($datos) && !empty($co_ususario)) {
            try {
                $dom = new DOMDocument("1.0");
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                ##<data>
                $data = $dom->createElement('data');
                $dom->appendChild($data);
                ##<articulos>
                $articulos = $dom->createElement('articulos');
                $data->appendChild($articulos);
                foreach ($datos as $row) {
                    $items = $dom->createElement("items");
                    $articulos->appendChild($items);
                    $items->appendChild($dom->createElement("id_art", $row['c_s']));
                    $items->appendChild($dom->createElement("co_est", $row['e_s']));
                }
                ##<articulos>
                ##</data>
                $xml = $dom->saveXML();
                $Sql = "IVDBINVENTAR.audit.autorizar_productos_gerente " . $cod_almacen . "," . $cod_listado . "," . $co_ususario . ",'" . $xml . "'," . (int) $averiado . "; ";
                $conn = odbc_connect("MASTER-P", "pica", "");
                $resultset = odbc_exec($conn, $Sql);
                if (odbc_error()) {
                    throw new Exception(odbc_errormsg($conn));
                }
                do {
                    while ($row = odbc_fetch_array($resultset)) {
                        if (!empty($row['resultset'])) {
                            if (isset($row['resultset'])) {
                                switch ($row['resultset']) {
                                    case "ERROR":
                                        $array['co_err'] = (int) $row['co_error'];
                                        $array['tx_err'] = trim(utf8_decode($row['tx_error']));
                                        break;
                                    case "AJUSTE":
                                        $array['data'][] = array(
                                            'co_art' => trim($row['co_articulo']),
                                            'co_err' => trim($row['resultado']),
                                            'va_sal' => (int) $row['saldo'],
                                            'va_sal_ant' => (int) $row['saldo_anterior']
                                        );
                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
            } catch (Exception $e) {
                $array['tx_err'] = "ERROR AL REGISTRAR ARTICULOS: " . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['tx_err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function _actualizaToma($bodega, $listado, $co_estado) {
        $array = array('co_err' => 1, 'tx_err' => '', 'ca_pen' => 0);
        if ($bodega > 0 && $listado > 0 && !empty($co_estado)) {
            try {
                $Sql = " IVDBINVENTAR.audit._actualiza_estado_toma $bodega,$listado,'" . $co_estado . "'; ";
                $resultado = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_message() . " " . $this->db->_error_number());
                } else {
                    $row = $resultado->row_array();
                    if (!empty($row)) {
                        $array['co_err'] = (int) $row['co_error'];
                        $array['tx_err'] = trim($row['tx_error']);
                        $array['ca_pen'] = (int) $row['pendientes'];
                    }
                }
            } catch (Exception $e) {
                $array['co_err'] = $e->getCode();
                $array['tx_err'] = "ERROR AL ACTUALIZAR TOMA:" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['tx_err'] = 'PARAMETROS INCOMPLETOS';
        }
        return $array;
    }

    function retTomasPendientes($co_usuario) {
        $array = array('err' => '', 'data' => null);
        if (!empty($co_usuario)) {
            try {
                $Sql = " select a.co_bodega, tx_bodega=c.descripcion+'', a.co_listado, a.fe_generacion,tipo=f.tx_descripcion+'',a.tx_descripcion, ";
                $Sql .= " co_estado=a.co_estado,no_estado=e.tx_descripcion+'',items=count(b.co_articulo) ";
                $Sql .= " from IVDBINVENTAR.audit.listado a ";
                $Sql .= " inner join IVDBINVENTAR.audit.listado_articulos b on a.co_bodega=b.co_bodega and a.co_listado=b.co_listado ";
                $Sql .= " inner join IVDBINVENTAR.dbo.ivtbbodega c on a.co_bodega=c.co_bodega and responsable_division = $co_usuario ";
                $Sql .= " inner join IVDBINVENTAR.audit.estados e on a.co_estado=e.co_estado ";
                $Sql .= " inner join IVDBINVENTAR.audit.tipo_listado f on a.co_tipo = f.co_tipo ";
                $Sql .= " where	a.co_estado = 'V3' ";
                $Sql .= " group by a.co_bodega, c.descripcion, a.co_listado, a.co_Estado, a.fe_generacion,f.tx_descripcion, ";
                $Sql .= " a.tx_descripcion,a.co_estado, e.tx_descripcion ";
                $Sql .= " order by a.fe_generacion; ";

                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_message() . " " . $this->db->_error_number());
                } else {
                    $resArray = $result->result_array();
                    if (!empty($resArray)) {
                        foreach ($resArray as $row) {
                            $html = '<button type="button" style="margin-right: 5px;" onclick="viewDetToma(' . $row['co_bodega'] . ',' . $row['co_listado'] . ',0,' . "'" . trim($row['co_estado']) . "'" . ')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-check-square-o"></i></button>';
                            $html .= '<button type="button" style="margin-right: 5px;" onclick="fn_generarInforme(' . $row['co_bodega'] . ',' . $row['co_listado'] . ')" class="btn btn-circle btn-warning"><i class="text-blanco fa fa-send"></i></button>';
                            if (trim($row['co_estado']) <> 'A' && trim($row['co_estado']) <> 'X') {
                                $html .= '<button type="button" onclick="delToma(' . $row['co_bodega'] . ',' . $row['co_listado'] . ')" class="btn btn-circle btn-danger"><i class="text-blanco fa fa-close"></i></button>';
                            }
                            $array['data'][] = array(
                                'co_bdg' => (int) $row['co_bodega'],
                                'no_bdg' => trim(utf8_decode($row['tx_bodega'])),
                                'co_lis' => (int) $row['co_listado'],
                                'fe_gen' => !empty($row['fe_generacion']) ? date('Y-m-d H:i:s', strtotime($row['fe_generacion'])) : '',
                                'co_tip' => trim($row['tipo']),
                                'tx_des' => trim(utf8_decode($row['tx_descripcion'])),
                                'co_est' => trim($row['co_estado']),
                                'no_est' => trim($row['no_estado']),
                                'ca_ite' => (int) $row['items'],
                                'html' => $html
                            );
                        }
                    }
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL OBTENER TOMAS PENDIENTES:" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function retTomasAlmacen($co_bodega, $fe_inicio, $fe_fin) {

        $array = array('err' => '', 'data' => null);

        if (!empty($co_bodega) && !empty($fe_inicio) && !empty($fe_fin)) {
            try {
                $Sql = " SELECT b.descripcion,a.co_listado,a.co_estado,c.tx_descripcion +'' as no_estado,";
                $Sql .= " a.fe_generacion,a.tx_origen,a.tx_descripcion ";
                $Sql .= " FROM IVDBINVENTAR.audit.listado a ";
                $Sql .= " INNER JOIN IVDBINVENTAR.dbo.ivtbbodega b ON a.co_bodega = b.co_bodega ";
                $Sql .= " INNER JOIN IVDBINVENTAR.audit.estados c ON a.co_estado = c.co_estado ";
                $Sql .= " WHERE a.co_bodega = " . (int) $co_bodega;
                $Sql .= " AND CONVERT(DATE,a.fe_generacion) BETWEEN '" . trim($fe_inicio) . "' AND '" . trim($fe_fin) . "'; ";

                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                } else {
                    $resArray = $result->result_array();
                    if (!empty($resArray)) {
                        foreach ($resArray as $row) {
                            $array['data'][] = array(
                                'no_bdg' => trim(utf8_decode($row['descripcion'])),
                                'co_lis' => (int) $row['co_listado'],
                                'co_est' => trim($row['co_estado']),
                                'no_est' => trim($row['no_estado']),
                                'fe_gen' => !empty($row['fe_generacion']) ? date('Y-m-d H:i:s', strtotime($row['fe_generacion'])) : '',
                                'or_lis' => trim(utf8_decode($row['tx_origen'])),
                                'ds_lis' => trim(utf8_decode($row['tx_descripcion']))
                            );
                        }
                    }
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL OBTENER TOMAS ALMACEN:" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }

        return $array;
    }

    function _retLiderToma($co_bodega, $co_listado) {
        $array = array('err' => '', 'co_auditor' => 0);
        if (!empty($co_bodega) && !empty($co_listado)) {
            try {
                $Sql = " SELECT TOP 1 co_auditor FROM IVDBINVENTAR.audit.listado_auditores WHERE co_bodega = " . (int) $co_bodega . " AND co_listado = " . (int) $co_listado . " AND fl_lider = 1;";
                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                } else {
                    $row = $result->row_array();
                    if (!empty($row)) {
                        $array['co_auditor'] = (int) $row['co_auditor'];
                    }
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL OBTENER LIDER DE TOMA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function _retGerenteRegional($co_bodega) {
        $array = array('err' => '', 'co_ger' => 0, 'no_ger' => '', 'e_mail' => '');
        if (!empty($co_bodega)) {
            try {
                $Sql = " SELECT responsable_division,b.e_mail,b.nombres,b.apellidos FROM IVDBINVENTAR..ivtbbodega a INNER JOIN WEBPYCCA..tb_intranet_users b ON a.responsable_division = b.cod_empleado WHERE co_bodega = " . $co_bodega . ";";
                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                } else {
                    $row = $result->row_array();
                    if (!empty($row)) {
                        $array['co_ger'] = (int) $row['responsable_division'];
                        $array['no_ger'] = utf8_decode($row['nombres'] . " " . $row['apellidos']);
                        $array['e_mail'] = $row['e_mail'];
                    }
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL OBTENER GERENTE REGIONAL :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function _retDescripcionToma($co_bodega, $co_listado) {
        $array = array('err' => '', 'no_toma' => '');
        if (!empty($co_bodega) && !empty($co_listado)) {
            try {
                $Sql = " SELECT b.descripcion,tx_origen,tx_descripcion FROM IVDBINVENTAR.audit.listado a INNER JOIN IVDBINVENTAR.dbo.ivtbbodega b ON a.co_bodega = b.co_bodega WHERE a.co_bodega = " . $co_bodega . " AND a.co_listado = " . $co_listado . ";";
                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                } else {
                    $row = $result->row_array();
                    if (!empty($row)) {
                        $array['no_toma'] = utf8_decode($row['descripcion'] . " - " . $row['tx_descripcion']);
                    }
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL OBTENER DESCRIPCION TOMA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

}
?>




