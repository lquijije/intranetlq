<?php

class m_3003 extends CI_Model {

    var $res_fn;

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->load->library(array('connmysql'));
        $this->db->flush_cache();
        $this->res_fn = array('co_err' => 1, 'tx_err' => '', 'data' => null);
    }

    function _retArtitulosMysql() {

        $db = new connmysql();
        $db->host = '130.1.80.65';
        $db->user = 'root';
        $db->pass = 'clearlog';
        $db->db = 'dbpycca';

        $Sql = " SELECT a.product_id, a.product_code, b.product, c.price
                FROM dbpycca.cscart_products a
                INNER JOIN dbpycca.cscart_product_descriptions b ON a.product_id=b.product_id AND b.lang_code='es'
                INNER JOIN dbpycca.cscart_product_prices c ON a.product_id=c.product_id
                WHERE a.status = 'A'
                ORDER BY 1;";

        $db->connectMysqli();

        if (empty($db->err)) {

            $resultado = $db->cnn->query($Sql);

            if (!empty($db->cnn->error)) {
                $this->res_fn['co_err'] = $db->cnn->errno;
                $this->res_fn['tx_err'] = $db->cnn->error;
            } else {

                if ($resultado->num_rows > 0) {
                    $this->res_fn['co_err'] = 0;
                    $i = 0;
                    while ($row = $resultado->fetch_array()) {
                        $i++;

                        $this->res_fn['data'][] = array(
                            'id_art' => trim($row['product_id']),
                            'co_art' => trim($row['product_code']),
                            'ds_art' => utf8_encode(trim($row['product'])),
                            'va_art' => $row['price']
                        );
//                        if ($i == 12) {
//                            break;
//                        }
                    }
                } else {
                    $this->res_fn['tx_err'] = 'SIN DATOS EN LA BUSQUEDA REALIZADA';
                }
            }

            $db->closeMysqli();
        } else {
            $this->res_fn['tx_err'] = $db->err;
        }

        return $this->res_fn;
    }

    function _retArtitulosSql($array_productos) {

        $this->res_fn['co_err'] = 1;
        $this->res_fn['tx_err'] = '';
        $this->res_fn['data'] = null;

        if ($array_productos['co_err'] == 0 && !empty($array_productos['data'])) {

            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;

            ##<data>

            $data = $dom->createElement('data');

            $dom->appendChild($data);

            ##<articulos>

            $articulos = $dom->createElement('articulos');
            $data->appendChild($articulos);

            foreach ($array_productos['data'] as $row) {
                $items = $dom->createElement("items");
                $articulos->appendChild($items);
                $items->appendChild($dom->createElement("id_art", $row['id_art']));
                $items->appendChild($dom->createElement("co_art", $row['co_art']));
                $items->appendChild($dom->createElement('ds_art'))->appendChild($dom->createTextNode($row['ds_art']));
                $items->appendChild($dom->createElement("va_art", $row['va_art']));
            }

            ##<articulos>
            ##</data>

            $xml = $dom->saveXML();

            $Sql = " IVDBINVENTAR.._courier_articulos '$xml'; ";
            
            try {

                $resultado = $this->db->query($Sql);

                if ($this->db->_error_message() <> '') {
                    $this->res_fn['co_err'] = $this->db->_error_number();
                    $this->res_fn['tx_err'] = 'ERROR AL GENERAR TOMA COD_ERR:' . $this->db->_error_number() . ' ' . $this->db->_error_message();
                } else {

                    $rs = $resultado->result_array();

                    if (!empty($rs)) {
                        $this->res_fn['co_err'] = 0;
                        foreach ($rs as $row) {
                            $this->res_fn['data'][] = array(
                                'id_art' => (int) $row['product_id'],
                                'co_art' => $row['product_code'],
                                'ds_art' => utf8_encode($row['product_description']),
                                'va_art' => number_format($row['pvpiva'], 2),
                                'me_art' => $row['medida'],
                                'al_art' => $row['alto'],
                                'fo_art' => $row['fondo'],
                                'an_art' => $row['ancho'],
                                'pe_art' => $row['peso'],
                                'di_art' => $row['dimensiones'],
                                'la_art' => $row['largo'],
                                'ta_art' => utf8_encode($row['tamanio']),
                                'pr_art' => $row['profundidad']
                            );
                        }
                    }
                }
            } catch (Exception $exc) {
                $this->res_fn['co_err'] = $this->db->_error_number();
                $this->res_fn['tx_err'] = 'ERROR AL OBTENER ARTICULOS COD_ERR:' . $this->db->_error_number() . ' ' . $this->db->_error_message();
            }
        } else {
            $this->res_fn['tx_err'] = $array_productos['tx_err'];
        }

        return $this->res_fn;
    }

    function _actualizaArticulos($array) {

        $this->res_fn['co_err'] = 1;
        $this->res_fn['tx_err'] = '';
        $this->res_fn['data'] = null;

        if (!empty($array)) {

            try {

                $dom = new DOMDocument("1.0");
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;

                ##<data>

                $data = $dom->createElement('data');

                $dom->appendChild($data);

                ##<articulos>

                $articulos = $dom->createElement('articulos');
                $data->appendChild($articulos);

                foreach ($array as $row) {
                    $dimension = array(
                        'UNIDAD' => $row['un_m'],
                        'LARGO' => (int) $row['la_a'],
                        'ANCHO' => (int) $row['an_a'],
                        'ALTO' => (int) $row['al_a']
                    );

                    $dimension = empty($row['la_a']) && empty($row['an_a']) && empty($row['al_a']) ? '' : json_encode($dimension);

                    $items = $dom->createElement("items");
                    $articulos->appendChild($items);
                    $items->appendChild($dom->createElement("co_articulo", trim($row['co_a'])));
                    $items->appendChild($dom->createElement("largo", !empty($row['la_a']) ? $row['la_a'] . " " . $row['un_m'] : '' ));
                    $items->appendChild($dom->createElement("ancho", !empty($row['an_a']) ? $row['an_a'] . " " . $row['un_m'] : '' ));
                    $items->appendChild($dom->createElement("alto", !empty($row['al_a']) ? $row['al_a'] . " " . $row['un_m'] : '' ));
                    $items->appendChild($dom->createElement("peso", !empty($row['pe_a']) ? $row['pe_a'] . " " . $row['un_p'] : '' ));
                    $items->appendChild($dom->createElement("dimension", $dimension));

//                $do_dimen = $dom->createElement("dimension");
//                $items->appendChild($do_dimen);
//                $do_dimen->appendChild($dom->createCDATASection(json_encode($dimension)));
                }

                ##<articulos>
                ##</data>

                $xml = $dom->saveXML();
                $Sql = " IVDBINVENTAR.._actualiza_ficha_tecnica_articulos_ '$xml'; ";
                $resultado = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    $this->res_fn['co_err'] = $this->db->_error_number();
                    $this->res_fn['tx_err'] = 'ERROR AL GRABAR FICHA TECNICA COD_ERR:' . $this->db->_error_number() . ' ' . $this->db->_error_message();
                } else {
                    $rs = $resultado->row_array();
                    $this->res_fn['co_err'] = (int) $rs['co_error'];
                    $this->res_fn['tx_err'] = trim(utf8_encode($rs['tx_error']));
                }
            } catch (Exception $exc) {
                $this->res_fn['co_err'] = $this->db->_error_number();
                $this->res_fn['tx_err'] = 'ERROR AL GRABAR FICHA TECNICA COD_ERR:' . $this->db->_error_number() . ' ' . $this->db->_error_message();
            }
        } else {
            $this->res_fn['tx_err'] = 'PARAMETROS INCOMPLETOS';
        }

        return $this->res_fn;
    }

}

?>
