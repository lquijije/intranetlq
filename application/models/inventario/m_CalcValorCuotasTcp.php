<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class m_CalcValorCuotasTcp extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->db->trans_strict(FALSE);
		$this->db->trans_off();
		$this->db->save_queries=FALSE;
		$this->db->flush_cache();
	}

	function retTablaBusqueda($datos){
		
		$Sql  = "CALL IVDBINVENTAR.art9100_busqueda('$datos');";

		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;

			$result = $query->result_array();

			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$pvpIva = floatval($row['pvpiva']);
					$pvpOfertaIva = floatval($row['pvp_oferta_iva']);
					$descuento = 0;
					if($pvpIva>0){
						$descuento = round((1-($pvpOfertaIva/$pvpIva))*100);
					}
					$return_arr[] = array(
						'codigo'		=> trim($row['codigo']),
						'descripcion'	=> trim($row['descripcion']),
						'pvp'			=> trim($row['pvp']),
						'pvpiva'		=> trim($row['pvpiva']),
						'pvp_oferta_iva'=> trim($row['pvp_oferta_iva']),
						'fe_ohasta'		=> date($dformat,strtotime($row['fe_ohasta'])),
						'saldo'			=> trim($row['saldo']),
						'co_fabrica'	=> trim($row['co_fabrica']),
						'upc'			=> trim($row['upc']),
						'descuento'		=> $descuento 
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No existen productos similares a ".$datos;
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaAmortiza($monto){
		
		$Sql  = "CALL IVDBINVENTAR.ncuotas_diferido('$monto');";

		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				foreach ($result as $row) { 
					$return_arr[] = array(
						'cuota'		=> trim($row['cuota'])." meses",
						'valor'		=> trim($row['valor']),
						'capital'	=> trim($row['capital']),
						'interes'	=> trim($row['interes']),
						'tot_final' => trim($row['tot_final'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No pudimos generar la tabla de amortización, talvez el monto ($".$monto.") no es el correcto.";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}
}