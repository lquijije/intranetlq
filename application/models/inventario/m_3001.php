<?php

class m_3001 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function _retArtitulo($cod_art){
        
        $array = null;
        $error = '';
        
        if (!empty($cod_art)){
            
            $this->_logArticuloUpc($_SERVER['REMOTE_ADDR'],$_SESSION['c_e'],$cod_art,'','CONSULTA ART');
                    
            log_message('error',"CONSULTA ARTICULO: ".$cod_art." USUARIO: ".$_SESSION['c_e']);

            $Sql = " IVDBINVENTAR.dbo._consulta_articulo '".trim($cod_art)."'; ";
            $result = $this->db->query($Sql);

            if ($this->db->_error_message() <> ''){
                $error = "ERROR AL OBTENER ARTICULO: ".$this->db->_error_number()." ".$this->db->_error_message();
                log_message('error',"CONSULTA ARTICULO ERROR: ".$this->db->_error_number()." ".$this->db->_error_message()." USUARIO: ".$_SESSION['c_e']);
            } else {
                
                $row = $result->row_array();
                
                if(!empty($row)){
                    $array = array(
                        'cod_art'=>$row['co_articulo'],
                        'des_art'=>utf8_encode($row['ds_articulo']),
                        'empaque'=>(int)$row['empaque'],
                        'cod_fab'=>trim($row['co_fabrica']),
                        'cod_upc'=>trim($row['upc']),
                        'des_gru'=>utf8_encode($row['ds_grupo']),
                        'des_lin'=>utf8_encode($row['ds_linea']),
                        'des_sub'=>utf8_encode($row['ds_sublinea']),
                        'des_cat'=>utf8_encode($row['ds_categoria']),
                        'cod_fac'=>trim($row['co_factura'])
                    );
                }
            }
        }
        
        return array('data'=>$array,'err'=>$error);
 
    }
    
    function saveUpc($cod_art,$cod_upc){
        
        $array = array('co_err'=>'','tx_err'=>'');
        $error = '';
        
        if (!empty($cod_art) && !empty($cod_upc)){
            
            if (trim(strtoupper($cod_upc)) == 'SINCODIGOUPC'){
                $this->_logArticuloUpc($_SERVER['REMOTE_ADDR'],$_SESSION['c_e'],$cod_art,$cod_upc,'SIN CODIGO UPC');
            } else {
                $this->_logArticuloUpc($_SERVER['REMOTE_ADDR'],$_SESSION['c_e'],$cod_art,$cod_upc,'ACTUALIZA ART');
            }
            
            log_message('error',"ACTUALIZAR ARTICULO: ".$cod_art." UPC:".$cod_upc." USUARIO: ".$_SESSION['c_e']);
            $Sql = " IVDBINVENTAR.dbo._actualizaUpc '".trim($cod_art)."','".trim(strtoupper($cod_upc))."'; ";

            $result = $this->db->query($Sql);

            if ($this->db->_error_message() <> ''){
                $error = "ERROR AL ACTUALIZAR UPC: ".$this->db->_error_number()." ".$this->db->_error_message();
                $array['co_err'] = 1;
                $array['tx_err'] = $error;
                log_message('error',"ACTUALIZAR ARTICULO ERROR: ".$error." USUARIO: ".$_SESSION['c_e']);
            } else {
                
                $row = $result->row_array();
                
                if(!empty($row)){
                    $array['co_err'] = $row['co_error'];
                    $array['tx_err'] = $row['tx_error'];
                }
            }
        } else {
            $array['co_err'] = 1;
            $array['tx_err'] = 'Parametros Incompletos';
        }
        
        $this->_logArticuloUpc($_SERVER['REMOTE_ADDR'],$_SESSION['c_e'],$cod_art,$cod_upc,'ACTUALIZA OBS:'.$array['tx_err']);
        
        return $array;
        
    }
    
    function _logArticuloUpc($ip,$codigo_empleado,$codigo_articulo,$codigo_upc,$accion){

        $Sql = " INSERT INTO IVDBINVENTAR.dbo._log_actualiza_upc VALUES (GETDATE(),'".trim($ip)."','".trim($codigo_empleado)."','".trim($codigo_articulo)."','".trim($codigo_upc)."','".trim($accion)."'); ";
        $this->db->query($Sql);

//        if ($this->db->_error_message() <> ''){
//            ECHO "ERROR AL INSERTAR LOG: ".$this->db->_error_number()." ".$this->db->_error_message();
//        } else {
//            ECHO 'OK';
//        }
        
 
    }
}
?>
