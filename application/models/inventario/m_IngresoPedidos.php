<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class m_IngresoPedidos extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->db->trans_strict(FALSE);
		$this->db->trans_off();
		$this->db->save_queries=FALSE;
		$this->db->flush_cache();
	}

	function retBodegasAuth(){
		$codEmpl = $_SESSION['c_e'];
		$Sql = "CALL SGDBSEGURIDAD.art1000_bodegas_auth($codEmpl);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'co_bodega'	=> trim($row['co_bodega']),
					'ds_bodega'	=> trim($row['nombre_completo'])
					); 
			}
		}
		return $return_arr;
	}

	function retBodegasAuthPedido(){
		$codEmpl = $_SESSION['c_e'];
		$bodAut = $this->retBodegasAuth();


		$strBod = join(",",array_column($bodAut, 'co_bodega'));

		$Sql = "CALL IVDBINVENTAR.bodegas_autorizadas_ped('$strBod',$codEmpl,0,23);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'Código'			=> trim($row['co_bodega']),
					'Nombre'			=> trim($row['nombre_completo']),
					'Direccion'			=> trim($row['direccion']),
					'es_jefe'			=> trim($row['es_jefe']),
					'es_gerente'		=> trim($row['es_gerente'])
					); 
			}
		}
		return $return_arr;
	}

	function retTableBodPide(){
		$bodegas = $this->retBodegasAuthPedido();
		$html = '<table id="table_pedido" class="table table-bordered text-size-0-5">';
		$html .= '<thead>';
		foreach($bodegas[0] as $key=>$value){
			if($key=='Código' || $key=='Nombre')
				$html .= '<th style="color:white;background-color:#428bca;text-align:center;">' . $key . '</th>';
		}
		$html .= '</thead>';
		$html .= '<tbody>';
		foreach( $bodegas as $key=>$value){
			$html .= '<tr>';

			$html .= '<td style=text-align:right;> <a href="javascript:void(0)" onclick="quitarModal();setBodPide('."'".$value['Código']."','".$value['Nombre']."'".');" style="text-decoration:underline;";>'. $value['Código'] .' </a></td>';

			$html .= '<td style=text-align:left;>'. $value['Nombre'] . '</td>';		
			$html .= '</tr>';
		}
		$html .= '</tbody>';
		$html .= '</table>';
		return $html;
	}

	function retListMotivos(){
		
		$Sql = "CALL IVDBINVENTAR.ped1000_listmotivos();";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'Código'			=> trim($row['co_motivo']),
					'Motivo'			=> trim($row['no_motivo']),
					'st_motivo'			=> trim($row['st_motivo']),
					'info_adic'			=> trim($row['info_adic'])
					); 
			}
		}
		return $return_arr;
	}

	function retTableMotivo(){
		$motivos = $this->retListMotivos();
		$html = '<table id="table_motivo" class="table table-bordered text-size-0-5">';
		$html .= '<thead>';
		foreach($motivos[0] as $key=>$value){
			if($key=='Código' || $key=='Motivo')
				$html .= '<th style="color:white;background-color:#428bca;text-align:center;">' . $key . '</th>';
		}
		$html .= '</thead>';
		$html .= '<tbody>';
		foreach( $motivos as $key=>$value){
			$html .= '<tr>';

			$html .= '<td style=text-align:right;> <a href="javascript:void(0)" onclick="quitarModal();setMot('."'".$value['Código']."','".$value['Motivo']."'".');" style="text-decoration:underline;";>'. $value['Código'] .' </a></td>';

			$html .= '<td style=text-align:left;>'. $value['Motivo'] . '</td>';		
			$html .= '</tr>';
		}
		$html .= '</tbody>';
		$html .= '</table>';
		return $html;
	}

	function retListBodeA(){
		
		$Sql = "CALL IVDBINVENTAR.ped1000_listbodegas();";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'Código'			=> trim($row['co_bodega']),
					'Descripción'		=> trim($row['descripcion'])
					); 
			}
		}
		return $return_arr;
	}

	function retTableBodeA(){
		$motivos = $this->retListBodeA();
		$html = '<table id="table_bod_gen" class="table table-bordered text-size-0-5">';
		$html .= '<thead>';
		foreach($motivos[0] as $key=>$value){
			if($key=='Código' || $key=='Descripción')
				$html .= '<th style="color:white;background-color:#428bca;text-align:center;">' . $key . '</th>';
		}
		$html .= '</thead>';
		$html .= '<tbody>';
		foreach( $motivos as $key=>$value){
			$html .= '<tr>';

			$html .= '<td style=text-align:right;> <a href="javascript:void(0)" onclick="quitarModal();setBodA('."'".$value['Código']."','".$value['Descripción']."'".');" style="text-decoration:underline;";>'. $value['Código'] .' </a></td>';

			$html .= '<td style=text-align:left;>'. $value['Descripción'] . '</td>';		
			$html .= '</tr>';
		}
		$html .= '</tbody>';
		$html .= '</table>';
		return $html;
	}

	function retTablaBusqArt($datos){
		
		$Sql  = "CALL IVDBINVENTAR.art9100_busqueda('$datos');";

		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;

			$result = $query->result_array();

			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$pvpIva = floatval($row['pvpiva']);
					$pvpOfertaIva = floatval($row['pvp_oferta_iva']);
					$descuento = 0;
					if($pvpIva>0){
						$descuento = round((1-($pvpOfertaIva/$pvpIva))*100);
					}
					$return_arr[] = array(
						'codigo'		=> trim($row['codigo']),
						'descripcion'	=> trim($row['descripcion']),
						'pvp'			=> trim($row['pvp']),
						'pvpiva'		=> trim($row['pvpiva']),
						'pvp_oferta_iva'=> trim($row['pvp_oferta_iva']),
						'fe_ohasta'		=> date($dformat,strtotime($row['fe_ohasta'])),
						'saldo'			=> trim($row['saldo']),
						'co_fabrica'	=> trim($row['co_fabrica']),
						'upc'			=> trim($row['upc']),
						'descuento'		=> $descuento 
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No existen productos similares a ".$datos;
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaTraslados($bodent){

		$feped = date('Y-m-d', strtotime('-1 day'));
		
		$Sql  = "CALL IVDBINVENTAR.ped1000_constraslados($bodent,'$feped');";

		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;

			$result = $query->result_array();

			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_noped'		=> trim($row['co_noped']),
						'fe_pedido'		=> date($dformat,strtotime(trim($row['fe_pedido']))),
						'no_pedido_por'	=> trim($row['no_pedido_por']),
						'ds_comentario'	=> trim($row['ds_comentario']),
						'co_usuario'	=> trim($row['co_usuario']),
						'no_motivo'		=> trim($row['no_motivo']),
						'bodent'		=> trim($row['bodent']),
						'bodsal'		=> trim($row['bodsal'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No existen traslados ";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retStockBasico($codBod){

		$Sql = "CALL IVDBINVENTAR.ivtbbodega_select('$codBod');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			if(!empty($result)){
				return $result;
			}else {
				return "Empty Result ivtbbodega_select('$codBod');";
			}
		}else{
			return $query[0]." - ".$query[1];
		}
	}

	function retConsBodeGyeUio($codBod){

		$Sql = "CALL IVDBINVENTAR.rep4000_consbodegyeuio('2',$codBod);";
		
		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			if(!empty($result)){
				return $result;
			}else {
				return "Empty Result rep4000_consbodegyeuio('2',$codBod);";
			}
		}else{
			return $query[0]." - ".$query[1];
		}
	}

	function retExistenciaItem($datos){
		$codArt = $datos['c_codart'];
		$bodEnt = $datos['c_bodent'];
		$bodSal = $datos['c_bodsal'];
		$pedCon = 0;
		$pedida = 0;

		$pendientes   = $this->retPedPendientes($datos);  
		
		if(is_array($pendientes)){
			$pedCon = $pendientes['v_cont'];
			$pedida = $pendientes['v_pedidas'];
		}

		$Sql = "CALL IVDBINVENTAR.ped1000_existencia_item('$codArt',$bodSal,NULL);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			if(!empty($result)){

				$return_arr[] = array(
					'disponible'	=> trim($result['disponible']),
					'saldo'			=> trim($result['saldo']),
					'v_contped'		=> $pedCon,
					'v_pedidas'		=> $pedida
					); 

				return $return_arr;
			}else {
				return "Lamentamos el inconveniente - Empty Result ped1000_existencia_item('$codArt',$bodEnt,$bodSal);";
			}
		}else{
			return $query[0]." - ".$query[1];
		}
	}

	function retPedPendientes($datos){

		$codArt = $datos['c_codart'];
		$bodEnt = $datos['c_bodent'];

		$Sql = "CALL IVDBINVENTAR.ped100_pedpendientes('$codArt',$bodEnt);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			if(!empty($result)){
				return $result;
			}else {
				return "Lamentamos el inconveniente - Empty Result ped100_pedpendientes('$codArt',$bodEnt);";
			}
		}else{
			return $query[0]." - ".$query[1];
		}
	}

	function grabaPedido($datos){
		$detPed = $datos['detPedidos'];
		$cabPed = $datos['cabPedidos'];
		$codEmpl = $_SESSION['c_e'];

		$xml_data = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Pedido></Pedido>');
		$this->array_to_xml($cabPed,$xml_data,'cab');
        $xmlCab = $xml_data->asXML();
        
        $xml_data = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Pedido></Pedido>');
		$this->array_to_xml($detPed,$xml_data,'det');
		$xmlDet = $xml_data->asXML();

		$Sql = "CALL IVDBINVENTAR.ped1000_grabar('$xmlCab','$xmlDet',$codEmpl);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			if(!empty($result)){
				echo $result['co_message'].'-'.$result['ds_message'];
			}else {
				echo "Lamentamos el inconveniente - Empty Result ped1000_grabar('$xmlCab','$xmlDet',$codEmpl);";
			}
		}else{
			echo $query[0]." - ".$query[1];
		}
	}

	function array_to_xml( $data, &$xml_data ,$rowtag ) {
		foreach( $data as $key => $value ) {
			if( is_numeric($key) ){
				$key = $rowtag ; 
			}
			if( is_object($value) || is_array($value)) {
				$subnode = $xml_data->addChild($key);
				$this->array_to_xml($value, $subnode,$rowtag);
			} else {
				$xml_data->addChild("$key",htmlspecialchars("$value"));
			}
		}
	}

}