<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class m_ConsultaArticulo extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->db->trans_strict(FALSE);
		$this->db->trans_off();
		$this->db->save_queries=FALSE;
		$this->db->flush_cache();
	}

	function retEstructCom($datos){
		$Sql = "";
		$campo = "";

		if($datos['c_categ']!=''){
			$Sql  = " CALL IVDBINVENTAR.select_ivtbec_subcategoria(".$datos['c_grupo'].",".$datos['c_linea'].",".$datos['c_subli'].",".$datos['c_categ'].",0,1); ";
			$campo = "subcategoria";
		}elseif($datos['c_subli']!=''){
			$Sql  = " CALL IVDBINVENTAR.select_ivtbec_categoria(".$datos['c_grupo'].",".$datos['c_linea'].",".$datos['c_subli'].",0,1); ";
			$campo = "categoria";
		}elseif($datos['c_linea']!=''){
			$Sql  = " CALL IVDBINVENTAR.select_ivtbec_sublinea(".$datos['c_grupo'].",".$datos['c_linea'].",0,1); ";
			$campo = "sublinea";
		}elseif($datos['c_grupo']!=''){
			$Sql  = " CALL IVDBINVENTAR.select_ivtbec_linea(".$datos['c_grupo'].",0,1); ";
			$campo = "linea";
		}else{
			$Sql  = " CALL IVDBINVENTAR.select_ivtbec_grupo(0,1); ";
			$campo = "grupo";
		}

		$DB1 = $this->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		if(is_object($query)){	
			$result = $query->result_array();
			$arr = null;
			if (!empty($result)){
				foreach ($result as $row){
					$arr[] = array(
						'valor'		=> trim($row['co_'.$campo]),
						'mostrar'	=> trim($row['ds_'.$campo])
						);
				}
				echo "OK-".json_encode($arr);
			} else {
				echo "No existe Código de ".$campo;
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retFiltros2($datos){
		$valor = $datos['c_valr'];
		$antig = $datos['c_anti'];
		$filtr = $datos['c_filt'];
		
		$short = 'a.co_articulo ASC';

		if($antig=="1"){
			$short = "a.fe_ult_factura DESC";
		}

		switch ($datos['c_tipo']) {
			case '1':
			$cond = "' WHERE a.co_articulo LIKE ''".$valor."%'''";
			break;
			case '2':
			$cond = "' WHERE a.ds_articulo LIKE ''".$valor."%'''";
			break;
			case '3':
			$cond = "' WHERE a.co_fabrica LIKE ''".$valor."%'''";
			break;
			case '4':
			$cond = "' WHERE a.upc LIKE ''".$valor."%'''";
			break;
			case '5':
			$cond = "'".$valor."'";
			break;
		}

		$Sql  = " CALL IVDBINVENTAR.art1000_filtros_2(99,'$filtr',".$cond.",0,'$short'); ";

		$CI = &get_instance();
		
		$DB1 = $CI->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			$arr = null;

			if (!empty($result)){
				$arr[] = array(
					'codigo'		=> trim($result['co_articulo']),
					'descripcion'	=> trim($result['ds_articulo'])
					);
				echo "OK-".json_encode($arr);
			} else {
				echo "No existe Código de Artículo";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retConsArt($datos){
		$garantia = '';
		$oferta_activa = '0';
		$today = date('Y-m-d H:i:s');

		$Sql  = " CALL IVDBINVENTAR.art1000ivtbarticulosaldoscostos_consgene('$datos'); ";
		$CI = &get_instance();
		$DB1 = $CI->load->database('db_lx_master', true);

		$query= $DB1->query($Sql);
		if(is_object($query)){
			$result = $query->result_array();
			$arr = null;
			for ($i = 1; $i <= 12; $i++) {
				$cmb_meses[$i] = conveMes($i);
			}

			if (!empty($result)){
				foreach ($result as $row){
					$dformat = 'Y/m/d';
					$garantia = $this->retGarantia(trim($row['co_proveedor']));
					if ($today >= $row['fe_odesde'] && $today<=$row['fe_ohasta']) {
						$oferta_activa = '1';
					}

					$strfe_ult_factura = date($dformat,strtotime($row['fe_ult_factura']));
					$fe_ult_factura = DateTime::createFromFormat($dformat, $strfe_ult_factura);
					$diasDiff = date_diff(new DateTime(), $fe_ult_factura)->format('%a');
					$observacion = ($row['ds_observacion']=='')?$row['sb']:$row['ds_observacion'];
					$pvpIva = floatval($row['pvpiva']);
					$pvpOfertaIva = floatval($row['pvp_oferta_iva']);
					$descuento = 0;
					if($pvpIva>0){
						$descuento = round((1-($pvpOfertaIva/$pvpIva))*100);
					}

					$arr[] = array(
						'co_articulo'			=> trim($row['co_articulo']),
						'ds_articulo'			=> trim($row['ds_articulo']),
						'co_fabrica'			=> trim($row['co_fabrica']),
						'fe_odesde'				=> trim(date($dformat,strtotime($row['fe_odesde']))),
						'fe_ohasta'				=> trim(date($dformat,strtotime($row['fe_ohasta']))),
						'pvp_oferta_iva'		=> trim($row['pvp_oferta_iva']),
						'pvpiva'				=> trim($row['pvpiva']),
						'pvp_oferta'			=> trim($row['pvp_oferta']),
						'pvp'					=> trim($row['pvp']),
						'st_paga_iva'			=> trim($row['st_paga_iva']),         
						'fe_creacion'			=> trim(date($dformat,strtotime($row['fe_creacion']))),
						'co_fabricante'			=> trim($row['co_fabricante']),
						'fe_ult_factura'		=> trim($strfe_ult_factura)." - ".$diasDiff." Días",
						'empaque'				=> trim($row['empaque']),
						'empaque_in'			=> trim($row['empaque_in']),
						'und_factura'			=> trim($row['und_factura']),
						'costo_m_e'				=> trim($row['costo_m_e']),
						'costo_suc'				=> trim($row['costo_suc']),
						'costop'				=> trim($row['costop']),
						'base_comercial'		=> trim($row['base_comercial']),
						'costo_suc'				=> trim($row['costo_suc']),
						'co_proveedor'			=> trim($row['co_proveedor']),
						'co_factura'			=> trim($row['co_factura']),
						'co_grupo'				=> trim($row['co_grupo']),
						'ds_observacion'		=> trim($observacion),
						'se_descompone'			=> trim($row['se_descompone']),
						'pvp_ant'				=> trim($row['pvp_ant']),
						'fe_pvp_ant'			=> trim($row['fe_pvp_ant']),
						'semaforo'				=> trim($row['semaforo']),
						'disponible_compra'		=> trim($row['disponible_compra']),
						'co_grupo'				=> trim($row['co_grupo']),
						'co_linea'				=> trim($row['co_linea']),
						'co_sublinea'			=> trim($row['co_sublinea']),
						'co_categoria'			=> trim($row['co_categoria']),
						'co_subcategoria'		=> trim($row['co_subcategoria']),
						'ds_ranking'			=> trim($row['ds_ranking']),
						'disponible_compra_obs'	=> trim($row['disponible_compra_obs']),
						'proveedor'				=> trim($row['proveedor']),
						'canti'					=> trim($row['canti']),
						'sumatotal'				=> trim($row['sumatotal']),                                                          
						'publicidad'			=> trim($row['publicidad']),
						'stprove'				=> trim($row['stprove']),
						'dias_envio'			=> trim($row['dias_envio']),
						'ds_moneda'				=> trim($row['ds_moneda']),  
						'pais'					=> trim($row['pais']),  
						'sb'					=> trim($row['sb']),   
						'id_coleccion'			=> trim($row['id_coleccion']),
						'en_importacion'		=> trim($row['en_importacion']), 
						'upc'					=> trim($row['upc']), 
						'numero_partes'			=> trim($row['numero_partes']),
						'garantia'				=> $garantia,
						'oferta_activa'			=> $oferta_activa,
						'descuento'				=> $descuento
						);
				}
				echo "OK-".json_encode($arr);
			} else {
				echo "No existe Código de Artículo";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retGarantia($proveedor){
		$garantia = '';
		$Sql  = " CALL IVDBINVENTAR.ivtb_servicio_tecnico_select($proveedor); ";
		$CI = &get_instance();
		$DB1 = $CI->load->database('db_lx_master', true);
		$query1= $DB1->query($Sql);
		$result1 = $query1->result_array();
		if(!empty($result1)){
			foreach ($result1 as $rowp){
				$garantia = $rowp['ds_servicio']."/".$rowp['telefono']."/".$rowp['contacto'];
			}            		
		}
		return $garantia;
	}

	function retAdelante($datos){
		$tipBus = $datos['c_tipbus'];
		$codArt = $datos['c_cdart'];
		$codLik = $datos['c_cdlik'];
		$desArt = $datos['c_dsart'];
		$codFab = $datos['c_cdfab'];
		$codUpc = $datos['c_cdupc'];
		$estCom = $datos['c_escom'];
		$antigu = $datos['c_antig'];
		$filtro = $datos['c_filtr'];

		$spAnti = "";

		if($antigu=="1"){
			$spAnti = "_ant";
		}

		switch ($tipBus) {
			case '1':
			$Sql  = " CALL IVDBINVENTAR.art1000_next_code_2".$spAnti."(4,'$codArt',".(($antigu=="1")?"'$codLik',":"")."'$filtro'); ";
			break;
			
			case '2':
			$Sql  = " CALL IVDBINVENTAR.art1000_next_desc_2".$spAnti."(4,'$codArt','$desArt%','$filtro'); ";
			break;

			case '3':
			$Sql  = " CALL IVDBINVENTAR.art1000_next_fab_2".$spAnti."(4,'$codArt','$codFab%','$filtro'); ";
			break;

			case '4':
			$Sql  = " CALL IVDBINVENTAR.art1000_next_upc_2".$spAnti."(4,'$codArt','$codUpc%','$filtro'); ";
			break;

			case '5':
			$Sql  = " CALL IVDBINVENTAR.art1000_next_estcom_2".$spAnti."(4,'$codArt','$estCom','$filtro'); ";
			break;
		}

		$CI = &get_instance();
		$DB1 = $CI->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		
		if(is_object($query)){
			$result = $query->result_array();
			if(!empty($result)){

				foreach ($result as $row){
					$arr[] = array(
						'codigo'		=> trim($row['co_articulo']),
						'descripcion'	=> trim($row['ds_articulo'])
						);
				}     echo "OK-".json_encode($arr);
			} else {
				echo "Ya no hay mas artículos adelante, has llegado al último";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}		
	}

	function retAnterior($datos){
		$tipBus = $datos['c_tipbus'];
		$codArt = $datos['c_cdart'];
		$codLik = $datos['c_cdlik'];
		$desArt = $datos['c_dsart'];
		$codFab = $datos['c_cdfab'];
		$codUpc = $datos['c_cdupc'];
		$estCom = $datos['c_escom'];
		$antigu = $datos['c_antig'];
		$filtro = $datos['c_filtr'];

		$spAnti = "";

		if($antigu=="1"){
			$spAnti = "_ant";
		}

		switch ($tipBus) {
			case '1':
			$Sql  = " CALL IVDBINVENTAR.art1000_prev_code_2".$spAnti."(4,'$codArt',".(($antigu=="1")?"'$codLik',":"")."'$filtro'); ";
			break;
			
			case '2':
			$Sql  = " CALL IVDBINVENTAR.art1000_prev_desc_2".$spAnti."(4,'$codArt','$desArt%','$filtro'); ";
			break;

			case '3':
			$Sql  = " CALL IVDBINVENTAR.art1000_prev_fab_2".$spAnti."(4,'$codArt','$codFab%','$filtro'); ";
			break;

			case '4':
			$Sql  = " CALL IVDBINVENTAR.art1000_prev_upc_2".$spAnti."(4,'$codArt','$codUpc%','$filtro'); ";
			break;

			case '5':
			$Sql  = " CALL IVDBINVENTAR.art1000_prev_estcom_2".$spAnti."(4,'$codArt','$estCom','$filtro'); ";
			break;
		}

		$CI = &get_instance();
		$DB1 = $CI->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		if(is_object($query)){
			$result = $query->result_array();
			if(!empty($result)){

				foreach ($result as $row){
					$arr[] = array(
						'codigo'		=> trim($row['co_articulo']),
						'descripcion'	=> trim($row['ds_articulo'])
						);
				}     echo "OK-".json_encode($arr);
			} else {
				echo "Ya no hay mas artículos atrás, has llegado al primero";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}		
	}

	function retRotacion($datos){
		$codArt = $datos['c_cd'];
		$Sql  = "CALL IVDBINVENTAR.art1000ivtbsaldo_ivtbrotacioncons2('$codArt','N',4);";
		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'co_bodega'		=> trim($row['co_bodega']),
					'tipo'			=> trim($row['tipo']),
					'descripcion'	=> trim($row['descripcion']),
					'saldo'			=> trim(utf8_encode($row['saldo'])),
					'rot360'		=> trim(utf8_encode($row['r360mayor'])),
					'rot90'			=> trim(utf8_encode($row['rot90dias'])),
					'rotdiaria'		=> trim(utf8_encode($row['rotdiaria'])),
					'reservado'		=> trim(utf8_encode($row['reservado'])),
					'virtual'		=> trim(utf8_encode($row['tvirtual'])),
					'empresa_tipo'	=> trim(utf8_encode($row['empresa_tipo']))
					); 
			}
		}

		$Sql  = "CALL IVDBINVENTAR.averiados_saldos_pedidos('$codArt');";
		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'co_bodega'		=> "0",
					'tipo'			=> "",
					'descripcion'	=> trim($row['descripcion']),
					'saldo'			=> trim(utf8_encode($row['saldo'])),
					'rot360'		=> "0",
					'rot90'			=> trim(utf8_encode($row['envios'])),
					'rotdiaria'		=> "AV",
					'reservado'		=> "",
					'virtual'		=> trim(utf8_encode($row['tvirtual'])),
					'empresa_tipo'	=> "Y"
					); 
			}
		}
		$pedidos = $this->retPedidoPendienteFabrica($codArt);
		if(!empty($return_arr)){
			echo "OK-".json_encode(array("rotacion"=>$return_arr,"pedidos"=>$pedidos));
		}
		else{
			echo "No se encontro rotacion";
		}
	}

	function retPedidoPendienteFabrica($codArt){
		$Sql  = "CALL IVDBINVENTAR.pedido_pendiente_fabrica('$codArt',0);";
		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		$result = $query->row_array();
		if(!empty($result)){
			return $result;
		}
		else
			return false;
	}

	function getEstructComercial(){

		$Sql  = " CALL IVDBINVENTAR.select_ivtbec_grupo(0,1); ";
		$DB1 = $this->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		$result = $query->result_array();
		$arr = null;

		if (!empty($result)){
			foreach ($result as $row){
				$arr[] = array(
					'co_grupo'=> trim($row['co_grupo']),
					'ds_grupo'=> trim($row['ds_grupo'])
					);
			}
			$_SESSION['s_grupo'] = json_encode($arr);
		} 
		$Sql  = " CALL IVDBINVENTAR.select_ivtbec_linea(0,0,1); ";
		$DB1 = $this->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		$result = $query->result_array();
		$arr = null;

		if (!empty($result)){
			foreach ($result as $row){
				$arr[] = array(
					'co_grupo'=> trim($row['co_grupo']),
					'co_linea'=> trim($row['co_linea']),
					'ds_linea'=> trim($row['ds_linea'])
					);
			}
			$_SESSION['s_linea'] = json_encode($arr);
		} 
		$Sql  = " CALL IVDBINVENTAR.select_ivtbec_sublinea(0,0,0,1); ";
		$DB1 = $this->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		$result = $query->result_array();
		$arr = null;
		if (!empty($result)){
			foreach ($result as $row){
				$arr[] = array(
					'co_grupo'		=> trim($row['co_grupo']),
					'co_linea'		=> trim($row['co_linea']),
					'co_sublinea'	=> trim($row['co_sublinea']),
					'ds_sublinea'	=> trim($row['ds_sublinea'])
					);
			}
			$_SESSION['s_sublinea'] = json_encode($arr);
		} 
		$Sql  = " CALL IVDBINVENTAR.select_ivtbec_categoria(0,0,0,0,1); ";
		$DB1 = $this->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		$result = $query->result_array();
		$arr = null;
		if (!empty($result)){
			foreach ($result as $row){
				$arr[] = array(
					'co_grupo'		=> trim($row['co_grupo']),
					'co_linea'		=> trim($row['co_linea']),
					'co_sublinea'	=> trim($row['co_sublinea']),
					'co_categoria'	=> trim($row['co_categoria']),
					'ds_categoria'	=> trim($row['ds_categoria'])
					);
			}
			$_SESSION['s_categoria'] = json_encode($arr);
		} 
		$Sql  = " CALL IVDBINVENTAR.select_ivtbec_subcategoria(0,0,0,0,0,1); ";
		$DB1 = $this->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		$result = $query->result_array();
		$arr = null;
		if (!empty($result)){
			foreach ($result as $row){
				$arr[] = array(
					'co_grupo'			=> trim($row['co_grupo']),
					'co_linea'			=> trim($row['co_linea']),
					'co_sublinea'		=> trim($row['co_sublinea']),
					'co_categoria'		=> trim($row['co_categoria']),
					'co_subcategoria'	=> trim($row['co_subcategoria']),
					'ds_subcategoria'	=> trim($row['ds_subcategoria'])
					);
			}
			$_SESSION['s_subcategoria'] = json_encode($arr);
		} 
	}
	function retECArray($datos){
		$Sql = "";
		$campo = "";
		$grupo = $datos['c_grupo'];
		$linea = $datos['c_linea'];
		$sublinea = $datos['c_subli'];
		$categoria = $datos['c_categ'];
		$subcategoria = $datos['c_subca'];

		if($categoria!=''){
			$arrRes =  $this->retSubCategoria($grupo,$linea,$sublinea,$categoria);
			$campo  = "subcategoria";
		}elseif($sublinea!=''){
			$arrRes =  $this->retCategoria($grupo,$linea,$sublinea);
			$campo  = "categoria";
		}elseif($linea!=''){
			$arrRes =  $this->retSubLinea($grupo,$linea);
			$campo  = "sublinea";
		}elseif($grupo!=''){
			$arrRes =  $this->retLinea($grupo);
			$campo  = "linea";
		}else{
			$arrRes =  $this->retGrupo();
			$campo  = "grupo";
		}

		$arr = null;
		if (!empty($arrRes)){
			if($campo!='grupo'){
				$arr[] = array(
					'valor'		=>'-1',
					'mostrar'	=>''
					);
				$arr[] = array(
					'valor'		=>'0',
					'mostrar'	=>'TODAS'
					);
			}
			foreach ($arrRes as $row){
				$arr[] = array(
					'valor'		=>trim($row['co_'.$campo]),
					'mostrar'	=>trim($row['ds_'.$campo])
					);
			}
			echo "OK-".json_encode($arr);
		} else {
			echo "No existe ".$campo;
		}
	}

	function retGrupo(){
		if($_SESSION['s_grupo']){
			$json = $_SESSION['s_grupo'];
			return json_decode($json,true);
		}
	}

	function retLinea($grupo){
		
		//Buscar en Linea => Grupo = 4 
		if($_SESSION['s_linea']){
			$json 		= $_SESSION['s_linea'];
			$arrLinea 	= json_decode($json,true);
			$keysGrupo 	= array_keys(array_column($arrLinea, 'co_grupo'), $grupo);
			$arrRes 	= array_slice($arrLinea, $keysGrupo[0], count($keysGrupo));
			return $arrRes;
		}
	}


	function retSublinea($grupo,$linea){
		//Buscar en SubLinea => Grupo = 4 ; Linea = 2
		if($_SESSION['s_sublinea']){
			$json 			= $_SESSION['s_sublinea'];
			$arrSubLinea 	= json_decode($json,true);
			$keysGrupo 		= array_keys(array_column($arrSubLinea, 'co_grupo'), $grupo);
			$arrLinea 		= array_slice($arrSubLinea, $keysGrupo[0], count($keysGrupo));
			$keysLinea 		= array_keys(array_column($arrLinea, 'co_linea'), $linea);
			$arrRes 		= array_slice($arrLinea, $keysLinea[0], count($keysLinea));
			return $arrRes;
		}
	}

	function retCategoria($grupo,$linea,$sublinea){

		if($_SESSION['s_categoria']){
			$json 			= $_SESSION['s_categoria'];
			$arrCategoria 	= json_decode($json,true);
			$keysGrupo 		= array_keys(array_column($arrCategoria, 'co_grupo'), $grupo);
			$arrLinea 		= array_slice($arrCategoria, $keysGrupo[0], count($keysGrupo));
			$keysLinea 		= array_keys(array_column($arrLinea, 'co_linea'), $linea);
			$arrSubLinea 	= array_slice($arrLinea, $keysLinea[0], count($keysLinea));
			$keysSubLinea 	= array_keys(array_column($arrSubLinea, 'co_sublinea'), $sublinea);
			$arrRes 		= array_slice($arrSubLinea, $keysSubLinea[0], count($keysSubLinea));
			return $arrRes;
		}
	}

	function retSubCategoria($grupo,$linea,$sublinea,$categoria){

		if($_SESSION['s_subcategoria']){
			$json 			= $_SESSION['s_subcategoria'];
			$arrSubCategoria = json_decode($json,true);
			$keysGrupo 		= array_keys(array_column($arrSubCategoria, 'co_grupo'), $grupo);
			$arrLinea 		= array_slice($arrSubCategoria, $keysGrupo[0], count($keysGrupo));
			$keysLinea 		= array_keys(array_column($arrLinea, 'co_linea'), $linea);
			$arrSubLinea 	= array_slice($arrLinea, $keysLinea[0], count($keysLinea));
			$keysSubLinea 	= array_keys(array_column($arrSubLinea, 'co_sublinea'), $sublinea);
			$arrCategoria 	= array_slice($arrSubLinea, $keysSubLinea[0], count($keysSubLinea));
			$keysCategoria 	=  array_keys(array_column($arrCategoria, 'co_categoria'), $categoria);
			$arrRes 		= array_slice($arrCategoria, $keysCategoria[0], count($keysCategoria));
			return $arrRes;
		}
	}
	function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);
		$interval  = date_diff($datetime1, $datetime2);
		return $interval->format($differenceFormat);
	}

	function retTablaSimilares($datos){
		$tipBus = $datos['c_tipbus'];
		$codArt = $datos['c_cdart'];
		$desArt = $datos['c_dsart'];
		$codFab = $datos['c_cdfab'];
		$codUpc = $datos['c_cdupc'];
		$estCom = $datos['c_escom'];
		$filtro = $datos['c_filtr'];
		
		switch ($tipBus) {
			case '1':
			$Sql  = " CALL IVDBINVENTAR.art1000_similares(4,'$codArt',' 1=1 ','$filtro'); ";
			break;
			
			case '2':
			$Sql  = " CALL IVDBINVENTAR.art1000_similares(4,'$codArt',' a.ds_articulo LIKE ''$desArt%'' ','$filtro'); ";
			break;

			case '3':
			$Sql  = " CALL IVDBINVENTAR.art1000_similares(4,'$codArt',' a.co_fabrica LIKE ''$codFab%'' ','$filtro'); ";
			break;

			case '4':
			$Sql  = " CALL IVDBINVENTAR.art1000_similares(4,'$codArt',' a.upc LIKE ''$codUpc%'' ','$filtro'); ";
			break;

			case '5':
			$Sql  = " CALL IVDBINVENTAR.art1000_similares(4,'$codArt','$estCom','$filtro'); ";
			break;
		}

		$CI = &get_instance();
		$DB1 = $CI->load->database('db_lx_master', true);
		$query= $DB1->query($Sql);
		if(is_object($query)){
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row){
					$pvpIva = floatval($row['pvpiva']);
					$pvpOfertaIva = floatval($row['pvp_oferta_iva']);
					$descuento = 0;
					if($pvpIva>0){
						$descuento = round((1-($pvpOfertaIva/$pvpIva))*100);
					}
					$arr[] = array(
						'codigo'		=> trim($row['codigo']),
						'descripcion'	=> trim($row['descripcion']),
						'pvp'			=> trim($row['pvp']),
						'pvpiva'		=> trim($row['pvpiva']),
						'pvp_oferta_iva'=> trim($row['pvp_oferta_iva']),
						'fe_ohasta'		=> date($dformat,strtotime($row['fe_ohasta'])),
						'saldo'			=> trim($row['saldo']),
						'co_fabrica'	=> trim($row['co_fabrica']),
						'upc'			=> trim($row['upc']),
						'descuento'		=> $descuento 
						);
				}     echo "OK-".json_encode($arr);
			}else{
				echo "No existes artículos similares para producto ".$codArt;
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}	
	}

	function retBuscarSimilares($datos){
		
		$Sql  = "CALL IVDBINVENTAR.art1000_similares_busq('$datos');";

		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;

			$result = $query->result_array();

			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$pvpIva = floatval($row['pvpiva']);
					$pvpOfertaIva = floatval($row['pvp_oferta_iva']);
					$descuento = 0;
					if($pvpIva>0){
						$descuento = round((1-($pvpOfertaIva/$pvpIva))*100);
					}
					$return_arr[] = array(
						'codigo'		=> trim($row['codigo']),
						'descripcion'	=> trim($row['descripcion']),
						'pvp'			=> trim($row['pvp']),
						'pvpiva'		=> trim($row['pvpiva']),
						'pvp_oferta_iva'=> trim($row['pvp_oferta_iva']),
						'fe_ohasta'		=> date($dformat,strtotime($row['fe_ohasta'])),
						'saldo'			=> trim($row['saldo']),
						'co_fabrica'	=> trim($row['co_fabrica']),
						'upc'			=> trim($row['upc']),
						'descuento'		=> $descuento 
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No existen productos similares a ".$datos;
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaFicha($datos){

		$Sql = "CALL IVDBINVENTAR._get_ficha_tecnica_producto('$datos',0);";
		
		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				foreach ($result as $row) { 
					$return_arr[] = array(
						'codigo'		=> trim($row['codigo']),
						'descripcion'	=> trim($row['descripcion']),
						'atributo'		=> trim($row['atributo'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No existe ficha";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaPercha($datos){

		$Sql = "CALL IVDBINVENTAR.art1000_percha('$datos');";
		
		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				foreach ($result as $row) { 
					$return_arr[] = array(
						'bodega'	=> trim($row['bodega']),
						'zona'		=> trim($row['ds_zona']),
						'columna'	=> trim($row['columna']),
						'percha'	=> trim($row['npercha']),
						'nivel'		=> trim($row['nivel'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No existe ficha";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaForecast($datos){

		$Sql = "CALL IVDBINVENTAR.evaluacion_forecast_pedidos_x_item('$datos',0,NULL);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;
			$result = $query->result_array();

			if(!empty($result)){
				foreach ($result as $row) { 
					$return_arr[] = array(
						'anio'		=> trim($row['y']),
						'mes'		=> trim($row['monthname']),
						'importante'=> trim($row['importante']),
						'forecast'	=> trim($row['forecast']),
						'pedido'	=> trim($row['cantidad_pedido']),
						'recibido'	=> trim($row['cantidad_arecibir']),
						'vendido'	=> trim($row['venta']),
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No existe forecast";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaFactura($datos){
		$Sql = "";
		$codFac = $datos['c_codfac'];
		$codArt = $datos['c_codart'];
		$fDesde = $datos['c_desde'];
		$fHasta = $datos['c_hasta'];

		if($codFac==''){
			$Sql = "CALL IVDBINVENTAR.art100_facturas(NULL,'$codArt','$fDesde','$fHasta');";
		}else{
			$Sql = "CALL IVDBINVENTAR.art100_facturas('$codFac%','$codArt','$fDesde','$fHasta');";
		}

		$CI = &get_instance();

		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_factura'	=> trim($row['co_factura']),
						'no_proveedor'	=> trim($row['no_proveedor']),
						'fe_ingreso'	=> date($dformat,strtotime($row['fe_ingreso'])),
						'cantidad'		=> trim($row['cantidad']),
						'costo_m_e'		=> trim($row['costo_m_e']),
						'ds_moneda'		=> trim($row['ds_moneda']),
						'costo_suc'		=> trim($row['costo_suc']),
						'co_fabrica'	=> trim($row['co_fabrica']),
						'co_fabricante'	=> trim($row['co_fabricante']),
						'notas'			=> trim($row['notas']),
						'nume_pedi'		=> trim($row['nume_pedi']),
						'secu_pedi'		=> trim($row['secu_pedi']),
						'consolidado'	=> trim($row['consolidado']),
						'tipo_factura'	=> trim($row['tipo_factura']),
						'co_bodega_pica'=> trim($row['co_bodega_pica']),
						'codi_clie'		=> trim($row['codi_clie'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				if($codFac==''){
					echo "No encontramos ninguna factura para este artículo";				
				}
				else{
					echo "No encontramos ninguna factura que contenga ".$codFac." en ese rango de fechas";					
				}
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retMinFechaIngresoFactura(){
		$Sql = "SELECT DATE_FORMAT(MIN(fe_ingreso),'%Y-%m-%d') AS fecha FROM IVDBINVENTAR.ivtbfactura;";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		$result = $query->row_array();
		if(!empty($result)){
			return $result['fecha'];
		}
	}

	function retBodegasAuth(){
		$codEmpl = $_SESSION['c_e'];
		$Sql = "CALL SGDBSEGURIDAD.art1000_bodegas_auth($codEmpl);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'co_bodega'	=> trim($row['co_bodega']),
					'ds_bodega'	=> trim($row['nombre_completo'])
					); 
			}
		}
		return $return_arr;
	}

	function retTipoMovimiento($tipo){
		
		$Sql = "CALL IVDBINVENTAR.ivtbtipomovi_select('$tipo');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'co_tipo'			=> trim($row['co_tipo']),
					'ds_descripcion'	=> trim($row['ds_descripcion'])
					); 
			}
		}
		return $return_arr;
	}

	function retTablaPedido($datos){
		$codBod = $datos['c_codbod'];
		$codArt = $datos['c_codart'];
		$fDesde = $datos['c_fdesde'];
		$fHasta = $datos['c_fhasta'];
		$Condic = $datos['c_condic'];

		$Sql = "CALL IVDBINVENTAR.art1000_cons_pedido($codBod,'$codArt','$fDesde','$fHasta','$Condic');";
		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){

			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';

				foreach ($result as $row) { 
					$return_arr[] = array(
						'codigo'		=> trim($row['codigo']),
						'co_bode_s'		=> trim($row['co_bode_s']),
						'co_bode_e'		=> trim($row['co_bode_e']),
						'st_estado'		=> trim($row['st_estado']),
						'cantpedido'	=> trim($row['cantpedido']),
						'fe_pedido'		=> date($dformat,strtotime($row['fe_pedido'])),
						'despacho'		=> trim($row['despacho']),
						'camion_sale'	=> trim($row['camion_sale']),
						'camion_llega'	=> trim($row['camion_llega']),
						'recibido'		=> trim($row['recibido']),
						'estatus'		=> trim($row['estatus']),
						'bodeen'		=> trim($row['bodeen']),
						'bodesa'		=> trim($row['bodesa'	]),
						'fe_recepcion'	=> trim($row['fe_recepcion']),
						'tipo'			=> trim($row['tipo']),
						'co_motivo'		=> trim($row['co_motivo']),
						'id_picking'	=> trim($row['id_picking']),
						'tiempo_total'	=> trim($row['tiempo_total'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos ningún pedido para este artículo";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaDetallePedido($datos){
		$codPed = $datos['c_codped'];
		$bodEnt = $datos['c_bodent'];
		$bodSal = $datos['c_bodsal'];
		$pickin = $datos['c_pickin'];

		$Sql = "CALL IVDBINVENTAR.art1000_detalle_pedido($codPed,$bodSal,$bodEnt,$pickin);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';

				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_bode_e'		=> trim($row['co_bode_e']),
						'co_articulo'	=> trim($row['co_articulo']),
						'cantpedido'	=> trim($row['cantpedido']),
						'cantdespachado'=> trim($row['cantdespachado']),
						'cantrecepcion'	=> trim($row['cantrecepcion']),
						'st_listo'		=> trim($row['st_listo']),
						'st_despachado'	=> trim($row['st_despachado']),
						'bulto_despachado'=> trim($row['bulto_despachado']),
						'co_grupo'		=> trim($row['co_grupo']),
						'ds_articulo'	=> trim($row['ds_articulo']),
						'co_fabrica'	=> trim($row['co_fabrica']),
						'ds_grupo'		=> trim($row['ds_grupo'	]),
						'no_bulto'		=> trim($row['no_bulto']),
						'fe_recepcion'	=> date($dformat,strtotime(trim($row['fe_recepcion']))),
						'no_pedido_por'	=> trim($row['no_pedido_por']),
						'tx_nombre'		=> trim($row['tx_nombre']),
						'co_usuario'	=> trim($row['co_usuario']),
						'nota'			=> trim($row['nota'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos ningún pedido para este artículo";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}
	

	function retTablaMovimiento($datos){
		$codArt = $datos['c_codart'];
		$fDesde = $datos['c_fdesde'];
		$fHasta = $datos['c_fhasta'];
		$Condic = $datos['c_condic'];

		$Sql = "CALL IVDBINVENTAR.art1000_movimientos('$codArt','$fDesde','$fHasta','$Condic');";
		
		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){

			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_tipo_mov'	=> trim($row['co_tipo_mov']),
						'ds_tipo_mov'	=> trim($row['ds_tipo_mov']),
						'co_articulo'	=> trim($row['co_articulo']),
						'co_nota'		=> trim($row['co_nota']),
						'cantidad'		=> trim($row['cantidad']),
						'fe_movto'		=> date($dformat,strtotime($row['fe_movto'])),
						'co_usuario'	=> trim($row['co_usuario']),
						'co_bodega_sale'=> trim($row['co_bodega_sale']),
						'tx_nombre'		=> trim($row['tx_nombre']),
						'tx_cargo'		=> trim($row['tx_cargo']),
						'bode_entra'	=> trim($row['bode_entra']),
						'bode_sale'		=> trim($row['bode_sale'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos ningún movimiento para este artículo";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaHistMov($codigo){

		$Sql = "CALL HIS_INVENTARIO.art1000_movtos_historicos('$codigo');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){

			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_tipo_mov'	=> trim($row['codigo']),
						'ds_tipo_mov'	=> trim($row['ds_descripcion']),
						'co_articulo'	=> trim($row['co_articulo']),
						'co_nota'		=> trim($row['nota']),
						'cantidad'		=> trim($row['cantidad']),
						'fe_movto'		=> date($dformat,strtotime($row['fecha'])),
						'co_usuario'	=> trim($row['usuario']),
						'co_bodega_sale'=> trim($row['co_bodega_sale']),
						'tx_nombre'		=> trim($row['tx_nombre']),
						'tx_cargo'		=> trim($row['tx_cargo']),
						'bode_entra'	=> trim($row['bode_entr']),
						'bode_sale'		=> trim($row['bode_sale'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos ningún movimiento histórico para este artículo";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}



	function retTablaXlsCostoVentasPvp($datos){
		
		$fDesde = $datos['c_fdesde'];
		$fHasta = $datos['c_fhasta'];
		$codArt = $datos['c_codart'];
		$codBod = $datos['c_codbod'];
		$codCaj = $datos['c_codcaj'];
		$tipMov = $datos['c_tipmov'];

		$Sql = "CALL IVDBINVENTAR.xls_costo_ventas_pvp('$fDesde','$fHasta','$codArt',$codBod,$codCaj,'$tipMov',0);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){

			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';

				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_articulo'			=> trim($row['co_articulo']),
						'descripcion'			=> trim($row['descripcion']),
						'tipo'					=> trim($row['tipo']),
						'fe_venta'				=> date($dformat,strtotime($row['fe_venta'])),
						'co_bodega'				=> trim($row['co_bodega']),
						'co_caja'				=> trim($row['co_caja']),
						'no_documento'			=> trim($row['no_documento']),
						'cantidad'				=> trim($row['cantidad']),
						'pvp_unit'				=> trim($row['pvp_unit']),
						'valor_pvp'				=> trim($row['valor_pvp']),
						'valor_descuento'		=> trim($row['valor_descuento']),
						'venta_pika'			=> trim($row['venta_pika']),
						'porcentaje_impuesto'	=> trim($row['porcentaje_impuesto']),
						'pos3_database'			=> trim($row['pos3_database']),
						'ds_articulo'			=> trim($row['ds_articulo']),
						'pvp'					=> trim($row['pvp']),
						'pvpoferta'				=> trim($row['pvpoferta']),
						'oferta'				=> trim($row['oferta']),
						'fe_odesde'				=> trim($row['fe_odesde']),
						'fe_ohasta'				=> trim($row['fe_ohasta'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos información al respecto";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retArrayMovimiento($datos){
		$codArt = $datos['c_codart'];
		$fDesde = $datos['c_fdesde'];
		$fHasta = $datos['c_fhasta'];
		$Condic = $datos['c_condic'];

		$Sql = "CALL IVDBINVENTAR.art1000_movimientos('$codArt','$fDesde','$fHasta','$Condic');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		$return_arr=null;

		if(is_object($query)){
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_tipo_mov'	=> trim($row['co_tipo_mov']),
						'ds_tipo_mov'	=> trim($row['ds_tipo_mov']),
						'co_articulo'	=> trim($row['co_articulo']),
						'co_nota'		=> trim($row['co_nota']),
						'cantidad'		=> (int)trim($row['cantidad']),
						'fe_movto'		=> date($dformat,strtotime($row['fe_movto'])),
						'co_usuario'	=> trim($row['co_usuario']),
						'co_bodega_sale'=> trim($row['co_bodega_sale']),
						'tx_nombre'		=> trim($row['tx_nombre']),
						'tx_cargo'		=> trim($row['tx_cargo']),
						'bode_entra'	=> trim($row['bode_entra']),
						'bode_sale'		=> trim($row['bode_sale'])
						); 
				}
			}
		}
		return $return_arr;
	}

	function retArrayHistMov($codigo){

		$Sql = "CALL HIS_INVENTARIO.art1000_movtos_historicos('$codigo');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		$return_arr=null;
		if(is_object($query)){
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_tipo_mov'	=> trim($row['codigo']),
						'ds_tipo_mov'	=> trim($row['ds_descripcion']),
						'co_articulo'	=> trim($row['co_articulo']),
						'co_nota'		=> trim($row['nota']),
						'cantidad'		=> trim($row['cantidad']),
						'fe_movto'		=> date($dformat,strtotime($row['fecha'])),
						'co_usuario'	=> trim($row['usuario']),
						'co_bodega_sale'=> trim($row['co_bodega_sale']),
						'tx_nombre'		=> trim($row['tx_nombre']),
						'tx_cargo'		=> trim($row['tx_cargo']),
						'bode_entra'	=> trim($row['bode_entr']),
						'bode_sale'		=> trim($row['bode_sale'])
						); 
				}
			}
		}
		return $return_arr;
	}

	function retTablaIngFacturas($cf,$pv){

		$Sql = "CALL IVDBINVENTAR.rpt1000_factura($pv,'$cf');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		$return_arr=null;
		if(is_object($query)){
			$result = $query->result_array();
			$sum_costo_m_e = 0;
			$sum_costo_suc = 0;
			$sum_pvp	   = 0;
			$sum_factor	   = 0;
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$cantidad  = intval(trim($row['cantidad']));
					$costo_m_e = round(floatval(trim($row['costo_m_e'])),2);
					$costo_suc = round(floatval(trim($row['costo_suc'])),2);
					$pvp 	   = round(floatval(trim($row['pvp'])),2);
					
					$sb_costo_m_e = round($costo_m_e * $cantidad,2);
					$sb_costo_suc = round($costo_suc * $cantidad,2);
					$sum_costo_m_e = $sum_costo_m_e + $sb_costo_m_e;
					$sum_costo_suc = $sum_costo_suc + $sb_costo_suc;
					$sum_pvp	   = $sum_pvp + ($cantidad * $pvp);

					if($sum_costo_suc>0){
						$sum_factor =  ($sum_pvp/$sum_costo_suc);	
					}
					
					$return_arr[] = array(
						'proveedor'			=> trim($row['proveedor']),
						'fe_ingreso'		=> date($dformat,strtotime($row['fe_ingreso'])),
						'co_moneda'			=> trim($row['co_moneda']),
						'co_factura'		=> trim($row['co_factura']),
						'cantidad'			=> trim($row['cantidad']),
						'costo_m_e'			=> $costo_m_e,
						'costo_suc'			=> $costo_suc,
						'tipo_ingreso'		=> trim($row['tipo_ingreso']),
						'nomb_proveedor'	=> trim($row['nomb_proveedor']),
						'nomb_mone'			=> trim($row['nomb_mone']),
						'co_articulo'		=> trim($row['co_articulo']),
						'ds_articulo'		=> trim($row['ds_articulo']),
						'co_fabrica'		=> trim($row['co_fabrica']),
						'pvp'				=> trim($row['pvp']),
						'sb_costo_m_e'		=> number_format($sb_costo_m_e,2),
						'sb_costo_suc'		=> number_format($sb_costo_suc,2),
						'sm_costo_m_e'		=> number_format($sum_costo_m_e,2),
						'sm_costo_suc'		=> number_format($sum_costo_suc,2),
						'sm_factor'			=> number_format($sum_factor,2)
						); 
				}
			}
		}
		return $return_arr;
	}
}

