<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class m_DespacharMercaderia extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->db->trans_strict(FALSE);
		$this->db->trans_off();
		$this->db->save_queries=FALSE;
		$this->db->flush_cache();
	}

	function retBodegasAuth(){
		$codEmpl = $_SESSION['c_e'];
		$Sql = "CALL SGDBSEGURIDAD.art1000_bodegas_auth($codEmpl);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'co_bodega'	=> trim($row['co_bodega']),
					'ds_bodega'	=> trim($row['nombre_completo'])
					); 
			}
		}
		return $return_arr;
	}

	function retBodegasAuthPedido(){
		$codEmpl = $_SESSION['c_e'];
		$bodAut = $this->retBodegasAuth();


		$strBod = join(",",array_column($bodAut, 'co_bodega'));

		$Sql = "CALL IVDBINVENTAR.bodegas_autorizadas_ped('$strBod',$codEmpl,0,23);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'Código'			=> trim($row['co_bodega']),
					'Nombre'			=> trim($row['nombre_completo']),
					'Direccion'			=> trim($row['direccion']),
					'es_jefe'			=> trim($row['es_jefe']),
					'es_gerente'		=> trim($row['es_gerente'])
					); 
			}
		}
		return $return_arr;
	}

	function retTableBodDesp(){
		$bodegas = $this->retBodegasAuthPedido();
		$html = '<table id="table_pedido" class="table table-bordered text-size-0-5">';
		$html .= '<thead>';
		foreach($bodegas[0] as $key=>$value){
			if($key=='Código' || $key=='Nombre')
				$html .= '<th style="color:white;background-color:#428bca;text-align:center;">' . $key . '</th>';
		}
		$html .= '</thead>';
		$html .= '<tbody>';
		foreach( $bodegas as $key=>$value){
			$html .= '<tr>';

			$html .= '<td style=text-align:right;> <a href="javascript:void(0)" onclick="quitarModal();setBod('."'".$value['Código']."','".$value['Nombre']."'".');" style="text-decoration:underline;";>'. $value['Código'] .' </a></td>';

			$html .= '<td style=text-align:left;>'. $value['Nombre'] . '</td>';		
			$html .= '</tr>';
		}
		$html .= '</tbody>';
		$html .= '</table>';
		return $html;
	}

	function retTablaPedidosPorDespachar($datos){
		$bodSal = $datos['c_bodsal'];
		$estado = $datos['c_estado'];
		$zebra  = $datos['c_zebra'];
		$impres = $datos['c_impres'];
		$codPed = $datos['c_codped'];
		
		if($codPed==''){
			$codPed = 'NULL';
		}

		if($bodSal==11 || $bodSal==1 || $bodSal==61){
			$Sql = "CALL IVDBINVENTAR.ped3500_por_despachar_pb($bodSal,'$estado',$zebra,$impres,0,$codPed);";
		}else{
			$Sql = "CALL IVDBINVENTAR.ped3500_por_despachar($bodSal,'$estado',$zebra,$impres,0,$codPed);";
		}

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){

			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$return_arr[] = array(
						'zona'			=> trim($row['zona']),
						'picking'		=> trim($row['picking']),
						'co_noped'		=> trim($row['co_noped']),
						'bodent'		=> trim($row['bodent']),// date($dformat,strtotime($row['bodent'])),
						'items'			=> trim($row['items']),
						'fe_pedido'		=> date($dformat,strtotime($row['fe_pedido'])),
						'no_pedido_por'	=> trim($row['no_pedido_por']),
						'no_motivo'		=> trim($row['no_motivo']),
						'ds_comentario'	=> trim($row['ds_comentario']),
						'tx_nombre'		=> trim($row['tx_nombre']),
						'co_bode_e'		=> trim($row['co_bode_e']),
						'tipoen'		=> trim($row['tipoen']),
						'pedido_impreso'=> trim($row['pedido_impreso']),
						'id_picking'	=> trim($row['id_picking'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos información al respecto";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaDetallePedido($datos){
		$bodPed = $datos['c_codped'];
		$bodSal = $datos['c_bodsal'];
		$bodEnt = $datos['c_bodent'];
		$pickin = $datos['c_pickin'];
		
		if($pickin==''){
			$pickin = 'NULL';
		}

		$Sql = "CALL IVDBINVENTAR.ped3000_detalle_t($bodPed,$bodSal,$bodEnt,$pickin,0);";

		
		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){

			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_bode_e'			=> trim($row['co_bode_e']),
						'co_articulo'		=> trim($row['co_articulo']),
						'cantpedido'		=> trim($row['cantpedido']),
						'cantdespachado'	=> trim($row['cantdespachado']),
						'st_listo'			=> trim($row['st_listo']),
						'st_despachado'		=> trim($row['st_despachado']),
						'bulto_despachado'	=> trim($row['bulto_despachado']),
						'co_linea'			=> trim($row['co_linea']),
						'ds_articulo'		=> trim($row['ds_articulo']),
						'co_fabrica'		=> trim($row['co_fabrica']),
						'ds_grupo'			=> trim($row['ds_grupo']),
						'no_bulto'			=> trim($row['no_bulto']),
						'empaque'			=> trim($row['empaque']),
						'saldo'				=> trim($row['saldo']),
						'saldo_v'			=> trim($row['saldo_v']),
						'st_estado'			=> trim($row['st_estado']),
						'usuario'			=> trim($row['usuario']),
						'nombre'			=> trim($row['nombre']),
						'co_factura'		=> trim($row['co_factura']),
						'cantidad_pickeado'	=> trim($row['cantidad_pickeado']),
						'cantidad_pedida'	=> trim($row['cantidad_pedida']),
						'zona'				=> trim($row['zona']),
						'eje_longitudinal'	=> trim($row['eje_longitudinal']),
						'eje_transversal'	=> trim($row['eje_transversal']),
						'nivel'				=> trim($row['nivel']),
						'percha'			=> trim($row['percha']),
						'tiene_celulares'	=> trim($row['tiene_celulares'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos información al respecto";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

}