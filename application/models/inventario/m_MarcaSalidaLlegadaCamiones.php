<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class m_MarcaSalidaLlegadaCamiones extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->db->trans_strict(FALSE);
		$this->db->trans_off();
		$this->db->save_queries=FALSE;
		$this->db->flush_cache();
	}

	function retBodegasAuth(){
		$codEmpl = $_SESSION['c_e'];
		$Sql = "CALL SGDBSEGURIDAD.art1000_bodegas_auth($codEmpl);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'co_bodega'	=> trim($row['co_bodega']),
					'ds_bodega'	=> trim($row['nombre_completo'])
					); 
			}
		}
		return $return_arr;
	}

	function retBodegasAuthPedido(){
		$codEmpl = $_SESSION['c_e'];
		$bodAut = $this->retBodegasAuth();


		$strBod = join(",",array_column($bodAut, 'co_bodega'));

		$Sql = "CALL IVDBINVENTAR.bodegas_autorizadas_ped('$strBod',$codEmpl,0,23);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		$return_arr=null;
		$result = $query->result_array();
		if(!empty($result)){
			foreach ($result as $row) { 
				$return_arr[] = array(
					'Código'			=> trim($row['co_bodega']),
					'Nombre'			=> trim($row['nombre_completo']),
					'Direccion'			=> trim($row['direccion']),
					'es_jefe'			=> trim($row['es_jefe']),
					'es_gerente'		=> trim($row['es_gerente'])
					); 
			}
		}
		return $return_arr;
	}


	function retTablaSalidaEntradaPed($datos){
		$codBod = $datos['c_bod_se'];
		$numPed = $datos['c_no_ped'];
		$estado = $datos['c_s_esta'];
		$sTipo  = $datos['c_s_tipo'];
		$idPick = $datos['c_idpick'];

		$Sql = "CALL IVDBINVENTAR.ped5000consbussalent($codBod,$numPed,'$estado','$sTipo',$idPick);";
		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){

			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';
				$cdBod = (($sTipo=='1')?$result[0]['co_bode_e']:$result[0]['co_bode_s']);
				$arrStockBas = $this->retStockBasico($cdBod);
				foreach ($result as $row) { 
					
					$return_arr[] = array(
						'ds_grupo'			=> trim($row['ds_grupo']),
						'co_articulo'		=> trim($row['co_articulo']),
						'fe_ca_salida'		=> date($dformat,strtotime(trim($row['fe_ca_salida']))),
						'fe_ca_llegada'		=> date($dformat,strtotime(trim($row['fe_ca_llegada']))),
						'ds_articulo'		=> trim($row['ds_articulo']),
						'cantpedido'		=> trim($row['cantpedido']),
						'cantdespachado'	=> trim($row['cantdespachado']),
						'cantrecepcion'		=> trim($row['cantrecepcion']),
						'guia_remi'			=> trim($row['guia_remi']),
						'no_pedido_por'		=> trim($row['no_pedido_por']),
						'fe_pedido'			=> date($dformat,strtotime(trim($row['fe_pedido']))),
						'co_bode_e'			=> trim($row['co_bode_e']),
						'fe_despachado'		=> date($dformat,strtotime(trim($row['fe_despachado']))),
						'co_bode_s'			=> trim($row['co_bode_s']),
						'no_recibido_por'	=> trim($row['no_recibido_por']),
						'st_listo'			=> trim($row['st_listo']),
						'st_despachado'		=> trim($row['st_despachado']),
						'bulto_despachado'	=> trim($row['bulto_despachado']),
						'bulto_recibido'	=> trim($row['bulto_recibido']),
						'l1'				=> trim($row['l1']),
						'l2'				=> trim($row['l2']),
						'co_fabrica'		=> trim($row['co_fabrica']),
						'pica'				=> trim($row['pica']),
						'unidades'			=> trim($row['unidades']),
						'co_motivo'			=> trim($row['co_motivo']),
						'motivo_pedido'		=> trim($row['motivo_pedido']),
						'ds_comentario'		=> trim($row['ds_comentario']),
						'picking_activos'	=> trim($row['picking_activos']),
						'stock_basico'		=> $arrStockBas['stock_basico'],
						'tipo_bodega'		=> $arrStockBas['tipo']
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos ningún pedido";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTablaPedidosPendientes($datos){
		$codBod = $datos['c_bod_sa'];
		$esSali = $datos['c_es_sal'];
		$esChof = $datos['c_es_cho'];
		$sUsuar = ($esSali=='1')?'0':$_SESSION['c_e'];
		$idPedi = $datos['c_id_ped'];

		$Sql = "CALL IVDBINVENTAR.ped5000_pendientes($codBod,$esSali,$esChof,$sUsuar,0,".(($idPedi==0)?"NULL":$idPedi).");";
		
		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				$dformat = 'Y-m-d';

				foreach ($result as $row) { 
					$return_arr[] = array(
						'co_noped'			=> trim($row['co_noped']),
						'descripcion'		=> trim($row['descripcion']),
						'fe_pedido'			=> date($dformat,strtotime(trim($row['fe_pedido']))),
						'grupo_pedido'		=> trim($row['grupo_pedido']),
						'id_picking'		=> trim($row['id_picking']),
						'pre_guia_impresa'	=> trim($row['pre_guia_impresa'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo "No encontramos ningún pedido pendiente";
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function retTableBodDesp(){
		$bodegas = $this->retBodegasAuthPedido();
		$html = '<table id="table_pedido" class="table table-bordered text-size-0-5">';
		$html .= '<thead>';
		foreach($bodegas[0] as $key=>$value){
			if($key=='Código' || $key=='Nombre')
				$html .= '<th style="color:white;background-color:#428bca;text-align:center;">' . $key . '</th>';
		}
		$html .= '</thead>';
		$html .= '<tbody>';
		foreach( $bodegas as $key=>$value){
			$html .= '<tr>';

			$html .= '<td style=text-align:right;> <a href="javascript:void(0)" onclick="quitarModal();setBod('."'".$value['Código']."','".$value['Nombre']."'".');" style="text-decoration:underline;";>'. $value['Código'] .' </a></td>';

			$html .= '<td style=text-align:left;>'. $value['Nombre'] . '</td>';		
			$html .= '</tr>';
		}
		$html .= '</tbody>';
		$html .= '</table>';
		return $html;
	}

	function retTablaTransportistas($strBus){

		$Sql = "CALL IVDBINVENTAR.select_ivtbtransportista('$strBus');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);
		if(is_object($query)){
			$return_arr=null;
			$result = $query->result_array();
			if(!empty($result)){
				foreach ($result as $row) { 
					$return_arr[] = array(
						'id'		=> trim($row['id']),
						'nombres'	=> trim($row['nombres']),
						'direccion'	=> trim($row['direccion']),
						'placa'		=> trim($row['placa'])
						); 
				}
				echo "OK-".json_encode($return_arr);
			}else {
				echo 'No encontramos ningún transportista con datos "'.$strBus.'"';
			}
		}else{
			echo "Lamentamos el inconveniente"."</br>";
			echo $query[0]." - ".$query[1];
			log_P($query[0],$query[1]);
		}
	}

	function guardarTransp($datos){
		$id 	= $datos['c_ced'];
		$nombre = $datos['c_nom'];
		$direcc = $datos['c_dir'];
		$placa  = $datos['c_pla'];

		$Sql = "CALL IVDBINVENTAR.insert_ivtbtransportista('$id','$nombre','$direcc','$placa');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$dblxmaster->query($Sql);

		if ($dblxmaster->_error_message()){
			echo "ERROR AL GRABAR TRANSPORTISTA: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
			return;
		} 
		echo "OK";
	}

	function delete_col(&$array, $offset) {
		return array_walk($array, function (&$v) use ($offset) {
			array_splice($v, $offset, 1);
		});
	}

	function retStockBasico($codBod){

		$Sql = "CALL IVDBINVENTAR.ivtbbodega_select('$codBod');";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			if(!empty($result)){
				return $result;
			}else {
				return false;
			}
		}else{
			return false;
		}
	}

	function retBlomba($datos){
		$codPed		= $datos['c_pe'];
		$codBodSal	= $datos['c_bs'];
		$codBodLle	= $datos['c_be'];

		$Sql = "CALL IVDBINVENTAR.ped5000_selectblomba($codPed,$codBodSal,$codBodLle);";

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			if(!empty($result)){
				return $result['blomba_salida'];
			} else {
				return '';
			}
		}else{
			return '';
		}
	}

	function retPedValidaLlegadaPrevia($datos){
		$codPed		= $datos['c_pd'];
		$codBodSal	= $datos['c_bs'];
		$codBodLle	= $datos['c_bl'];
		$picking	= $datos['c_pk'];

		$Sql = "CALL IVDBINVENTAR.ped5000_valida_llegada_previa($codPed,$codBodSal,$codBodLle,$picking);";
	
		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$query = $dblxmaster->query($Sql);

		if(is_object($query)){
			$result = $query->row_array();
			if(!empty($result)){
				echo $result['co_message'].'-'.$result['ds_message'];
			} else {
				echo '5-Validacion no devolvio ningun dato';
			}
		}else{
			echo $query[0]."-".$query[1];
		}
	}

	function ped5000_llegada($datos){
		$chofer     = $datos['c_ch'];
		$codPed		= $datos['c_pd'];
		$codBodSal	= $datos['c_bs'];
		$codBodLle	= $datos['c_bl'];
		$picking	= $datos['c_pk'];
		$esSalida	= $datos['c_sl'];
		$codBloma	= $datos['c_bm'];
		$codEmpl 	= $_SESSION['c_e'];

		

		$CI = &get_instance();
		$dblxmaster = $CI->load->database('db_lx_master',true);
		$dblxmaster->trans_begin();
		$Sql = "CALL IVDBINVENTAR.ped5000_llegada($codBodSal,$codBodLle,$codPed,$esSalida,$codBloma,$picking,$codEmpl);";
		$dblxmaster->query($Sql);
		$Sql = "CALL IVDBINVENTAR.control_despachos($esSalida,'$chofer',$codPed,$codBodLle,$codBodSal,$codBloma,$picking);";
		$dblxmaster->query($Sql);
		
		if ($dblxmaster->trans_status() === FALSE)
		{
			$dblxmaster->trans_rollback();
			echo "ERROR AL GRABAR SALIDA: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
		}
		else
		{
			$dblxmaster->trans_commit();
			echo "OK";
		}
		// if ($dblxmaster->_error_message()){
		// 	echo "ERROR AL GRABAR LLEGADA: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
		// 	return;
		// }
		
	}

	// function controlDespachos($datos){
	// 	$chofer     = $datos['c_ch'];
	// 	$codPed		= $datos['c_pd'];
	// 	$codBodSal	= $datos['c_bs'];
	// 	$codBodLle	= $datos['c_bl'];
	// 	$picking	= $datos['c_pk'];
	// 	$esSalida	= $datos['c_sl'];
	// 	$codBloma	= $datos['c_bm'];
	// 	$codEmpl 	= $_SESSION['c_e'];

	// 	$Sql = "CALL IVDBINVENTAR.control_despachos($esSalida,'$chofer',$codPed,$codBodLle,codBodSal,$codBloma,$picking);";

	// 	$CI = &get_instance();
	// 	$dblxmaster = $CI->load->database('db_lx_master',true);
	// 	$dblxmaster->query($Sql);

	// 	if ($dblxmaster->_error_message()){
	// 		echo "ERROR AL GRABAR CONTROL DESPACHOS: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
	// 		return;
	// 	}
	// 	echo "OK";
	// }
}