<?php

class m_101 extends CI_Model{

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retCombo(){
        
        $Sql  = " SELECT DISTINCT b.co_bodega, b.descripcion ";
        $Sql .= " FROM webm_people_counter_url a ";
        $Sql .= " INNER JOIN ivdbinventar..ivtbbodega b on a.co_bodega=b.co_bodega ";
        $result = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            log_message('ERROR',"ERROR".$this->db->_error_number()." ".$this->db->_error_message()."</br>".$Sql);
        }
        
        return $result->result_array();
    }
    function retChart($datos){
        if(!empty($datos)){
            
            $Sql  = " people_counter_report ".$datos['c_b'].",'".$datos['f_i']."' ";
            //$Sql  = " people_counter_report 7,'2014-12-31' ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                log_message('ERROR',"ERROR".$this->db->_error_number()." ".$this->db->_error_message()."</br>".$Sql);
            }
            $array = groupArray($result->result_array(),'hora');
            $arr = null;
            $color = array(array('colorIN'=>'#4d5bb6','borderIN'=>'#3746a4','colorOUT'=>'#7692e8','borderOUT'=>'#6a7fc1'),
                array('colorIN'=>'#de0000','borderIN'=>'#b50202','colorOUT'=>'#ff5c5c','borderOUT'=>'#cb4a4a'),
                array('colorIN'=>'#ff0099','borderIN'=>'#bc0071','colorOUT'=>'#ff99cc','borderOUT'=>'#dd7fae'),
                array('colorIN'=>'#00ae4d','borderIN'=>'#009341','colorOUT'=>'#72bf44','borderOUT'=>'#6dab48'));
            
            $a = 1;
            for($i=0; $i<count($array); $i++){
                $arr[]= array((int)$a,$array[$i]['hora'].":00");
                $a++;
            }
            $label = json_encode($arr);
            $arrayGe = groupArray($result->result_array(),'counter');
            $RetArray= null;
            for($i = 0;$i < count($arrayGe); $i++){
                $q = 0;
                $arrayEntrada=null;
                $arraySalida=null;
                $salto = trim($arrayGe[$i]['counter']);
                foreach ($result->result_array() as $row){
                    if($salto === trim($row['counter'])){
                        $q++;
                        $arrayEntrada[]=array((int)$q,(int)$row['in']);
                        $arraySalida[]=array((int)$q,(int)$row['out']);
                    }
                }
                $RetArray[] = array('label'=>$arrayGe[$i]['counter']." IN",
                    'data'=>$arrayEntrada,'bars'=>array('fillColor'=>$color[$i]['colorIN'],'lineWidth'=>1),'color'=>$color[$i]['borderIN']);
                $RetArray[] = array('label'=>$arrayGe[$i]['counter']." OUT",
                    'data'=>$arraySalida,'bars'=>array('fillColor'=>$color[$i]['colorOUT'],'lineWidth'=>1),'color'=>$color[$i]['borderOUT']);
            }   
                
//            var data = [
//                {label: "Entrada 1", data: [[1, 4],[2, 1],[3, 1],[4, 4],[5, 3]], bars: {fillColor: "#336600"}, color: "#336600"},
//                {label: "Entrada 2", data: [[1, 4],[2, 1],[3, 1],[4, 4],[5, 3]], bars: {fillColor: "#E41B17"}, color: "#E41B17"}
//            ];
            
            if(count($RetArray)>0){
                echo json_encode($RetArray)."/*/".$label;
            }
           
        }
    
    }
    
}
?>
