<?php
class autocomplete_model extends CI_Model{

   function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
   
    function retMenuAuto(){
        
        $return_arr = null;
        
        $Sql  = ' SELECT DISTINCT b.menu,c.nombre,c.urlinvocar,c.icono,c.padre ';
        $Sql .= ' FROM tb_intranet_users_perfil a ';
        $Sql .= ' INNER JOIN tb_intranet_perfil_accesos b ';
        $Sql .= ' ON b.perfil = a.perfil ';
        $Sql .= ' AND b.tipo = 1 ';
        $Sql .= ' INNER JOIN tb_intranet_menu c ';
        $Sql .= ' ON c.codigo = b.menu ';
        $Sql .= " WHERE a.cod_empl = '".$_SESSION['c_e']."' ";
        $Sql .= ' ORDER BY b.menu ASC;';
        $CI = &get_instance();
        $dblxmaster = $CI->load->database('db_lx_master', true);
        $query = $dblxmaster->query($Sql);
        if ($dblxmaster->_error_message() <> ''){
            session_destroy();
            $_SESSION = null;    
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL OBTENER MENÚ: ".$dblxmaster->_error_number(),$dblxmaster->_error_message(), 'error_db');
            die;
        }
        
        $res = $query->result_array();
        
        if(!empty($res)){
            foreach ($query->result_array() as $row) { 
                $return_arr[] = array(
                    'label'     =>trim(utf8_encode($row['nombre'])),
                    'id'        =>trim($row['menu']),
                    'urlinvocar'=>trim(utf8_encode($row['urlinvocar'])),
                    'icono'     =>trim(utf8_encode($row['icono']))
                ); 
            }
        }
        
        return $return_arr;
 
    }     
 }
 
?>


