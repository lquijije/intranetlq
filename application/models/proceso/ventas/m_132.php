<?php

ini_set('max_execution_time', 300);

class m_132 extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    public $tipos = array(
        'C' => array(
            'name' => 'Cambio',
        ),
        'V' => array(
            'name' => 'Devolución',
        )
    );
    public $estados = array(
        'E' => array(
            'name' => 'Pendiente',
        ),
        'P' => array(
            'name' => 'Procesado',
        ),
        'C' => array(
            'name' => 'Caducado',
        ),
        'A' => array(
            'name' => 'Autorizado',
        ),
        'R' => array(
            'name' => 'Rechazado',
        ),
        'X' => array(
            'name' => 'Anulado',
        )
    );

    function aprobarSolicit($data) {

        $arrayResult = null;
        $data = json_decode($data['s_s'], true);

        if (!empty($data)) {

            for ($a = 0; $a < count($data); $a++) {

                $Sql = "PODBPOS..posgen_solicituddevolucion_updateaprobacion " . $data[$a]['c_s'] . "," . $_SESSION['c_e'] . ",'" . trim($data[$a]['t_s']) . "','" . trim($data[$a]['e_s']) . "',''; ";
                //$this->db = $this->load->database('desarrollo', true);
                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    return "ERROR AL ACTUALIZAR SOLICITUD: " . $this->db->_error_number() . " " . $this->db->_error_message();
                    break;
                }
                $raw = $result->row_array();

                $arrayResult[] = array(
                    'c_s' => $data[$a]['c_s'],
                    'd_e' => array(
                        'co_err' => (int) $raw['severidad'],
                        'tx_err' => utf8_decode($raw['mensaje'])
                    )
                );
            }
        }

        return $arrayResult;
    }

    function retSolicitudes($cod_empl) {

        $array = null;

        if (!empty($cod_empl)) {

            $Sql = " SELECT DISTINCT a.co_solicitud,a.ti_autorizacion,a.fe_solicitud,a.de_comentariocreacionsolicitud, ";
            $Sql .= " b.de_ip_address,a.co_bodega,b.de_bodega,b.de_sri,a.co_caja,a.nu_factura,a.fe_docoriginal,a.tipo_documento,a.rg_estado ";
            $Sql .= " FROM PODBPOS..posgensolicitudesdevolucion a ";
            $Sql .= " INNER JOIN PODBPOS..posgenbodega b ";
            $Sql .= " ON a.co_bodega = b.co_bodega ";
            $Sql .= " INNER JOIN PODBPOS..posgengerenteregionalalmacen c ";
            $Sql .= " ON a.co_gerentealmacen = c.co_gerentealmacen ";
            $Sql .= " WHERE c.co_gerenteregional = $cod_empl ";
            $Sql .= " AND a.rg_estado = 'E'; ";

            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR SOLICITUDES: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            $resArray = $result->result_array();

            if (count($resArray) > 0) {
                $a = 0;
                foreach ($resArray as $row) {
                    $a++;
                    $array[] = array(
                        '_link' => "<a href='javascript:void(0)' onclick='viewFact(" . '"' . $row['de_sri'] .'"'. "," . $row['co_bodega'] . "," . $row['co_caja'] . "," . $row['nu_factura'] . ");' class='underline'>" . trim($row['de_sri'] . "-" . $row['co_caja'] . "-" . $row['nu_factura']) . "</a>",
                        'co_sol' => (int) $row['co_solicitud'],
                        'ti_aut' => trim($row['ti_autorizacion']),
                        'no_ti_aut' => $this->tipos[trim($row['ti_autorizacion'])]['name'],
                        'de_sol' => !empty($row['fe_solicitud']) ? date('Y-m-d H:i:s', strtotime($row['fe_solicitud'])) : '',
                        'ms_sol' => trim(utf8_encode($row['de_comentariocreacionsolicitud'])),
                        'ip_bdg' => trim($row['de_ip_address']),
                        'co_bdg' => (int) $row['co_bodega'],
                        'ds_bdg' => trim(utf8_encode($row['de_bodega'])),
                        'co_caj' => (int) $row['co_caja'],
                        'nu_doc' => (int) $row['nu_factura'],
                        'fe_fac' => !empty($row['fe_docoriginal']) ? date('Y-m-d H:i:s', strtotime($row['fe_docoriginal'])) : '',
                        'nu_fac' => trim($row['de_sri'] . "-" . $row['co_caja'] . "-" . $row['nu_factura']),
                        'ti_doc' => trim($row['tipo_documento']),
                        'es_sol' => trim($row['rg_estado']),
                        'no_es_sol' => $this->estados[trim($row['rg_estado'])]['name'],
                        'ap_tex' => '<textarea name="tx_a' . $a . '" id="tx_a' . $a . '" class="form-control" style="width: 210px;"></textarea>',
                        'ap_sol' => '<td><center><input type="radio" name="ck_' . $a . '" id="option_apro[]" value="' . (int) $row['co_solicitud'] . '"></center></td>',
                        'ne_sol' => '<td><center><input type="radio" name="ck_' . $a . '" id="option_rech[]" value="' . (int) $row['co_solicitud'] . '"></center></td>'
                    );
                }
            }
        }

        return $array;
    }

    function retSolicitud($cod_soli) {

        $array = null;

        if (!empty($cod_soli)) {

            $Sql = " SELECT a.co_solicitud,a.ti_autorizacion,a.co_bodega,b.de_sri,a.co_caja,a.nu_factura,a.co_autorizacion, ";
            $Sql .= " a.de_observaciongerenteregional,a.co_gerentealmacen,c.e_mail,a.rg_estado ";
            $Sql .= " FROM PODBPOS..posgensolicitudesdevolucion a ";
            $Sql .= " INNER JOIN PODBPOS..posgenbodega b ";
            $Sql .= " ON a.co_bodega = b.co_bodega ";
            $Sql .= " INNER JOIN WEBPYCCA..tb_intranet_users c ";
            $Sql .= " ON a.co_gerentealmacen = c.cod_empleado ";
            $Sql .= " WHERE a.co_solicitud = $cod_soli ";
//            $Sql .= " AND a.co_gerenteregional = $cod_empl; ";
            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR SOLICITUD: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            $row = $result->row_array();

            if (count($row) > 0) {
                $array = array(
                    'co_sol' => (int) $row['co_solicitud'],
                    'ti_aut' => trim($row['ti_autorizacion']),
                    'no_ti_aut' => $this->tipos[trim($row['ti_autorizacion'])]['name'],
                    'nu_fac' => trim($row['de_sri'] . "-" . $row['co_caja'] . "-" . $row['nu_factura']),
                    'co_bdg' => (int) $row['co_bodega'],
                    'co_caj' => (int) $row['co_caja'],
                    'nu_doc' => (int) $row['nu_factura'],
                    'co_aut' => (int) $row['co_autorizacion'],
                    'de_obs' => trim($row['de_observaciongerenteregional']),
                    'co_gea' => (int) $row['co_gerentealmacen'],
                    'em_gea' => trim($row['e_mail']),
                    'es_sol' => trim($row['rg_estado']),
                    'no_es_sol' => $this->estados[trim($row['rg_estado'])]['name']
                );
            }
        }

        return $array;
    }

    function retDetalleFactura($co_bodega, $co_caja, $nu_documento) {

        $array = null;
        $error = '';

        if (!empty($co_bodega) && !empty($co_caja) && !empty($nu_documento)) {

            $Sql = " SELECT a.fe_transaccion,a.co_cliente,b.co_secuencia,b.co_articulo, c.ds_articulo, b.va_cantidad, b.va_pvpventa ";
            $Sql .= " FROM PODBPOS_CIERRES..postrxtransaccion a ";
            $Sql .= " INNER JOIN PODBPOS_CIERRES..posvtaventasdet b ";
            $Sql .= " ON a.co_bodega = b.co_bodega ";
            $Sql .= " AND a.co_caja = b.co_caja ";
            $Sql .= " AND a.ti_documento = b.ti_documento ";
            $Sql .= " AND a.co_transaccion = b.co_transaccion ";
            $Sql .= " AND a.nu_documento = b.nu_documento ";
            $Sql .= " LEFT OUTER JOIN IVDBINVENTAR..IVTBARTICULO c ";
            $Sql .= " ON b.co_articulo = c.co_articulo ";
            $Sql .= " WHERE a.co_bodega = $co_bodega ";
            $Sql .= " AND a.co_caja = $co_caja ";
            $Sql .= " AND a.nu_documento = $nu_documento ";
            $Sql .= " AND a.ti_documento = 'F' ";
            $Sql .= " ORDER BY b.co_secuencia; ";

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL RETORNAR FACTURA: " . $this->db->_error_number() . " " . $this->db->_error_message();
            } else {

                $resArray = $result->result_array();

                if (count($resArray) > 0) {

                    foreach ($resArray as $row) {

                        $array[] = array(
                            'fe_fac' => !empty($row['fe_transaccion']) ? date('Y-m-d H:i:s', strtotime($row['fe_transaccion'])) : '',
                            'co_cli' => trim($row['co_cliente']),
                            'co_sec' => (int) $row['co_secuencia'],
                            'co_art' => trim($row['co_articulo']),
                            'ds_art' => trim(utf8_encode($row['ds_articulo'])),
                            'va_can' => (float) $row['va_cantidad'],
                            'va_pvp' => (float) $row['va_pvpventa']
                        );
                    }
                }
            }
        }

        return array('fac' => $array, 'err' => $error);
    }

}

?>
