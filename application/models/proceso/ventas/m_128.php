<?php

class m_128 extends CI_Model{
    
    public $estados = array (
        'C' => array(
            'name'=>'Ya pago',
            'color'=>'#f0a30a'
        ),
        'P' => array(
            'name'=>'Pendiente',
            'color'=>'#F56954'
        ),
        'E' => array(
            'name'=>'Contactado',
            'color'=>'#00A65A'
        ),
        'X' => array(
            'name'=>'Error',
            'color'=>'#2E8BEF'
        ),
        'L' => array(
            'name'=>'IVR Encolado',
            'color'=>'#633EBE'
        )
    );
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retMonitor($fec_ini,$fec_fin){
        
        $array_1 = $array_2 = $array_2_1 = $array_3 = $array_3_1 = $array_4 = null;
        
        $error = '';
        
        $style = array (
            '1'=>
                array(
                    'color'=>'#2E8BEF',
                    'class'=>'fa-envelope',
                ),
            '2'=>
                array(
                    'color'=>'#00A65A',
                    'class'=>'fa-whatsapp',
                ),
            '3'=>
                array(
                    'color'=>'#00A65A',
                    'class'=>'fa-whatsapp',
                ),
            '4'=>
                array(
                    'color'=>'#F0514A',
                    'class'=>'fa-phone',
                )
        );
        
        if (!empty($fec_ini) && !empty($fec_fin)){
            
            $Sql = " NTS_INTERCAMBIO.dbo._sp_robot_monitor2 '".$fec_ini."','".$fec_fin."' ";
//            echo $Sql;
//            die;
            $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset = odbc_exec($conn,$Sql);
            
            if (odbc_error()){
                $error = "ERROR: AL RETORNAR REGLAS ".odbc_errormsg($conn);
            }
            
            do {
                
                while($row = odbc_fetch_array($resultset)){
                    
                    if(isset($row['resultset'])){
                        
                        switch ($row['resultset']) {
                            
                            case "TOTALGENERAL":
                                
                                $array_1 = array (
                                    'tot_env'=>(int)$row['tot_enviado'],
                                    'tot_gen'=>(int)$row['tot_general'],
                                    'por_act'=>(int)$row['por_actual'],
                                    'val_ges'=>(float)$row['val_gestionar'],
                                    'cos_ges'=>(float)$row['cos_gestionado']
                                );
                                
                                break;
                            case "CANAL":
                                
                                $array_2[] = array(
                                    'co_reg'=>(int)$row['co_campania'],
                                    'ds_reg'=>trim(utf8_encode($row['ds_regla'])),
                                    'co_can'=>(int)$row['co_canal'],
                                    'ds_can'=>trim(utf8_encode($row['ds_canal'])),
                                    'co_est'=>$row['co_estado'],
                                    'ca_est'=>(int)$row['cantidad'],
                                    'va_ges'=>(float)$row['va_gestion']
                                );
                                
                                break;
                            case "CAMPANIA":
                                
                                $array_3[] = array(
                                    'co_reg'=>(int)$row['co_campania'],
                                    'ds_reg'=>trim(utf8_encode($row['ds_regla'])),
                                    'es_reg'=>$row['est_reg'],
                                    'co_est'=>$row['co_estado'],
                                    'ca_est'=>(int)$row['cantidad'],
//                                    'do_est'=>(float)$row['costo']
                                );
                                
                                break;
                        }

                    }
                    
                }
            } while(odbc_next_result($resultset));
            
            if(!empty($array_2)){
                $grupoArray = groupArray($array_2,'co_can');
                $array_2_1 = $this->monitorArrayCanales($grupoArray,$style);
            }
            
            if(!empty($array_3)){
                $grupoArray3 = groupArray($array_3,'co_reg');
                $array_3_1 = $this->monitorArrayCampanias($grupoArray3);
            }
        }
        
        return array('res_1'=>$array_1,'res_2'=>$array_2_1,'res_3'=>$array_3_1,'err'=>$error);
        
    }
    
    function monitorArrayCanales($grupoArray,$style){
        
        $canal = null;
        
        for ($i=0;$i< count($grupoArray); $i++){
            
            $sumaCantidad = 0;
            $chart = null;
            $campanias = null;

            $canal[] = array (
                'co_can'=> $grupoArray[$i]['co_can'],
                'ds_can'=> "".$grupoArray[$i]['groupeddata'][0]['ds_can'],
                'cl_can'=> $style[(int)$grupoArray[$i]['co_can']]['color'],
                'st_can'=> $style[(int)$grupoArray[$i]['co_can']]['class']
            );

            $grupoArrayRegla = groupArray($grupoArray[$i]['groupeddata'],'co_reg');
            $grupoArrayEstado = groupArray($grupoArray[$i]['groupeddata'],'co_est');
            
            for ($e=0;$e< count($grupoArrayEstado); $e++){
                
                $SumEstado = 0;
                
                for ($e1=0;$e1< count($grupoArrayEstado[$e]['groupeddata']); $e1++){
                    $SumEstado+=$grupoArrayEstado[$e]['groupeddata'][$e1]['ca_est'];
                }
                
                $chart[] = array (
                    'label'=> $this->estados[trim($grupoArrayEstado[$e]['co_est'])]['name'].' <b>'.number_format($SumEstado,0).'</b>',
                    'data' => $SumEstado,
                    'color'=> $this->estados[trim($grupoArrayEstado[$e]['co_est'])]['color'],
                );
                
            }
            
            for ($a=0;$a< count($grupoArrayRegla); $a++){
                
                $SumCampania = 0;
                $SumGestion = 0;
                $chartCampania = null;
                
                for ($a1=0;$a1< count($grupoArrayRegla[$a]['groupeddata']); $a1++){
                    $SumCampania+=$grupoArrayRegla[$a]['groupeddata'][$a1]['ca_est'];
                    $SumGestion+=$grupoArrayRegla[$a]['groupeddata'][$a1]['va_ges'];
                    
                    $chartCampania[] = array (
                        'label'=> $this->estados[trim($grupoArrayRegla[$a]['groupeddata'][$a1]['co_est'])]['name'].' <b>'.number_format($grupoArrayRegla[$a]['groupeddata'][$a1]['ca_est'],0).'</b>',
                        'data' => $grupoArrayRegla[$a]['groupeddata'][$a1]['ca_est'],
                        'color'=> $this->estados[trim($grupoArrayRegla[$a]['groupeddata'][$a1]['co_est'])]['color'],
                    );
                    
                }
                
                $campanias[] = array(
                    'co_reg'=>$grupoArrayRegla[$a]['co_reg'],
                    'ds_reg'=>$grupoArrayRegla[$a]['groupeddata'][0]['ds_reg'],
                    'to_reg'=> $SumCampania,
                    'va_ges'=> $SumGestion,
                    'chart_dat'=> array_values($chartCampania)
                );
                $sumaCantidad+=$SumCampania;
            }
               
            $canal[$i]['ca_can'] = $sumaCantidad;
            $canal[$i]['chart'] = array('chart_dat'=>array_values($chart));
            $canal[$i]['campanias'] = $campanias;

        }
//        echo '<pre>';
//        print_r($canal);
//        die;
        return $canal;
        
    }
    
    function monitorArrayCampanias($grupoArray){
        
        $canal = null;
        
        for ($i=0;$i< count($grupoArray); $i++){

            $sumaCantidad = 0;
            $chart = null;

            $canal[] = array (
                'co_reg'=> $grupoArray[$i]['co_reg'],
                'ds_reg'=> $grupoArray[$i]['groupeddata'][0]['ds_reg'],
                'es_reg'=> $grupoArray[$i]['groupeddata'][0]['es_reg']
            );

            for ($a=0;$a< count($grupoArray[$i]['groupeddata']); $a++){

                $sumaCantidad+=(int)$grupoArray[$i]['groupeddata'][$a]['ca_est'];

                $chart [] = array (
                    'label'=> $this->estados[trim($grupoArray[$i]['groupeddata'][$a]['co_est'])]['name'].' <b>'.number_format($grupoArray[$i]['groupeddata'][$a]['ca_est'],0).'</b>',
                    'data' => (int)$grupoArray[$i]['groupeddata'][$a]['ca_est'],
                    'color'=> $this->estados[trim($grupoArray[$i]['groupeddata'][$a]['co_est'])]['color'],
                );

            }

            $canal[$i]['ca_reg'] = $sumaCantidad;
            $canal[$i]['chart'] = array('chart_dat'=>$chart);

        }
        
        return $canal;
        
    }
    
    function retReport($data){
        
        $array = null;
        
        if(!empty($data['c_c']) && !empty($data['f_i']) && !empty($data['f_f'])){
            
            $Sql = " NTS_INTERCAMBIO.._sp_robot_reporte ".(int)$data['c_c'].",'".trim($data['f_i'])."','".trim($data['f_f'])."' ";
            
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                echo "ERROR AL RETORNAR REPORTE <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row){
                   
                    $array[] = array(
                        'fe_gen'=> trim($row['fe_generacion']),
                        'co_cam'=> (int)$row['co_campania'],
                        'ds_cam'=> trim($row['ds_campania']),
                        'co_cli'=> (int)$row['co_cliente'],
                        'cc_cli'=> trim($row['co_cedula']),
                        'ap_cli'=> trim(utf8_encode($row['cl_apellidos'])),
                        'no_cli'=> trim(utf8_encode($row['cl_nombres'])),
                        'va_dia'=> (int)$row['va_dias_vencidos'],
                        'va_min'=> (float)$row['va_pago_minimo'],
                        'co_est_cab'=> trim($row['co_estado_cab']),
                        'no_est_cab'=> $this->estados[$row['co_estado_cab']]['name'],
                        'co_sec'=> (int)$row['co_secuencia'],
                        'co_can'=> (int)$row['co_canal'],
                        'co_pro'=> (int)$row['co_proveedor'],
                        'no_pro'=> trim($row['tx_descripcion']),
                        'fe_env'=> trim($row['fe_envio']),
                        'contac'=> trim($row['contacto']),
                        'tx_men'=> trim(utf8_encode($row['tx_mensaje'])),
                        'tx_res'=> trim(utf8_encode($row['tx_respuesta_envio'])),
                        'va_dur'=> trim($row['va_duracion']),
                        'co_est_det'=> trim($row['co_estado_det']),
                        'no_est_det'=> $this->estados[$row['co_estado_det']]['name']
                    );
                    
                }
                
            }
            
        }
        
        return $array;
        
    }
    
    function BuscaCedulaNumero($fe_ini,$fe_fin,$co_bus){
        
        $array = null;
        
        if(!empty($fe_ini) && !empty($fe_fin) && !empty($co_bus)){
            
            $Sql = " NTS_INTERCAMBIO.._sp_robot_consulta_contacto '".trim($fe_ini)."','".trim($fe_fin)."','".trim($co_bus)."';";
            
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                echo "ERROR AL RETORNAR BUSQUEDA <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row){
                           
                    $array[] = array(
                        'co_cam'=> (int)$row['co_campania'],
                        'ds_cam'=> trim($row['ds_campania']),
                        'co_cli'=> (int)$row['co_cliente'],
                        'ce_cli'=> trim($row['co_cedula']),
                        'ap_cli'=> trim(utf8_encode($row['cl_apellidos'])),
                        'no_cli'=> trim(utf8_encode($row['cl_nombres'])),
                        'va_dia'=> (int)$row['va_dias_vencidos'],
                        'va_min'=> (float)$row['va_pago_minimo'],
                        'va_ven'=> (float)$row['va_pago_vencido'],
                        'va_sal'=> (float)$row['va_saldo'],
                        'fe_pag'=> trim($row['fe_pago']),
                        'fe_gen'=> trim($row['fe_generacion']),
                        'co_est_cab'=> trim($row['estado_cab']),
                        'no_est_cab'=> $this->estados[$row['estado_cab']]['name'],
                        'co_sec'=> (int)$row['co_secuencia'],
                        'co_con'=> trim($row['contacto']),
                        'co_can'=> (int)$row['co_canal'],
                        'ds_can'=> trim($row['ds_canal']),
                        'no_pro'=> trim($row['ds_proveedor']),
                        'co_est_det'=> trim($row['estado_det']),
                        'no_est_det'=> $this->estados[$row['estado_det']]['name'],
                        'va_dur'=> trim($row['va_duracion']),
                        'va_pre'=> (float)$row['va_precio'],
                        'fe_env'=> trim($row['fe_envio']),
                        'tx_res'=> trim(utf8_encode($row['tx_respuesta_envio'])),
                        'tx_men'=> trim(utf8_encode($row['tx_mensaje']))
                    );
                    
                }
                
            }
            
        }
        
        return $array;
        
    }
    
    function retDetLlamada($fec_ini,$fec_fin){
        
        $arr_chart = $arr_resum = null;
        $error = '';
        
        if (!empty($fec_ini) && !empty($fec_fin)){
            
            $Sql = " NTS_INTERCAMBIO.dbo._robot_consulta_ivr_log '".trim($fec_ini)."','".trim($fec_fin)."'; ";
            //ECHO$Sql;
            $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset = odbc_exec($conn,$Sql);
            
            if (odbc_error()){
                $error = "ERROR: AL RETORNAR DETALLE LLAMADAS ".odbc_errormsg($conn);
            }
            
            do {
                
                while($row = odbc_fetch_array($resultset)){
                    
                    if(isset($row['resultset'])){
                        
                        switch ($row['resultset']) {
                            
                            case "CHART":
                                
                                $arr_chart[] = array(
                                    'di_dial'=>(int)$row['dia'],
                                    'tx_di_dial'=>trim($row['txt_dia']),
                                    'ho_dial'=>trim($row['hora']),
                                    'ca_dial'=>(int)$row['cantidad']
                                );
                                
                                break;
                            case "RESUMEN":
                                
                                $arr_resum[] = array(
                                    'ou_dial'=>trim($row['outDialResult']),
                                    'ca_dial'=>(int)$row['cantidad']
                                );
                                
                                break;
                        }

                    }
                    
                }
            } while(odbc_next_result($resultset));
            
        }
        
        return array('res_1'=>$arr_chart,'res_2'=>$arr_resum,'err'=>$error);
        
    }
    
}

?>




