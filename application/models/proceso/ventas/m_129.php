<?php

class m_129 extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->load->library(array('connmysql'));
        $this->db->flush_cache();
    }

    function retEstadoRobot() {

        $status = 0;

        $Sql = " SELECT bd_status FROM NTS_INTERCAMBIO..ic_robot_settings;";
        $result = $this->db->query($Sql);

        if ($this->db->_error_message() <> '') {
            return "ERROR AL RETORNAR STATUS ROBOT: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        $row = $result->row_array();
        if (count($row) > 0) {
            $status = (int) $row['bd_status'];
        }

        return $status;
    }

    function updEstadoRobot($a) {

        $arr_error = array(
            'data' =>
            array(
                'co_err' => 0,
                'tx_err' => ''
            )
        );

        if ($a <> '') {

            $Sql = " UPDATE NTS_INTERCAMBIO..ic_robot_settings SET bd_status = $a;";
            $this->db->query($Sql);

            if ($this->db->_error_message() <> '') {
                $arr_error['data']['co_err'] = 1;
                $arr_error['data']['tx_err'] = "ERROR AL ACTUALIZAR STATUS ROBOT: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }
        } else {
            $arr_error['data']['co_err'] = 1;
            $arr_error['data']['tx_err'] = "PARAMETROS INCOMPLETOS";
        }

        return $arr_error;
    }

    function retReglas() {

        $Sql = " SELECT co_campania,tx_descripcion,co_estado";
        $Sql .= " FROM NTS_INTERCAMBIO..ic_robot_campania ";
        $Sql .= " ORDER BY co_campania; ";

        $result = $this->db->query($Sql);

        $array = null;

        if ($this->db->_error_message() <> '') {
            return "ERROR AL RETORNAR REGLAS: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        if (count($result->result_array()) > 0) {

            foreach ($result->result_array() as $row) {
                $array[] = array(
                    'co_reg' => (int) $row['co_campania'],
                    'ds_reg' => trim(utf8_encode($row['tx_descripcion'])),
                    'co_est' => trim($row['co_estado']),
                    'ds_est' => trim($row['co_estado']) == 'A' ? 'Activo' : 'Inactivo'
                );
            }
        }

        return $array;
    }

    function retPrioridades() {

        $Sql = " SELECT co_prioridad ";
        $Sql .= " FROM NTS_INTERCAMBIO..ic_robot_prioridades ";
        $Sql .= " ORDER BY co_prioridad ASC; ";

        $result = $this->db->query($Sql);

        $array = null;

        if ($this->db->_error_message() <> '') {
            return "ERROR AL RETORNAR PRIORIDADES: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        if (count($result->result_array()) > 0) {

            foreach ($result->result_array() as $row) {
                $array[] = array(
                    'co_pri' => $row['co_prioridad']
                );
            }
        }

        return $array;
    }

    function retReglaRobot($cod_reg) {

        $arr_campania = $arr_param = $arr_detalle = $arr_canal = $arr_manual = null;
        $error = '';
        if (!empty($cod_reg)) {
            $Sql = " NTS_INTERCAMBIO.dbo._sp_robot_consulta_regla " . trim($cod_reg) . "; ";
            $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset = odbc_exec($conn, $Sql);
            if (odbc_error()) {
                $error = "ERROR: AL RETORNAR REGLAS " . odbc_errormsg($conn);
            }
            do {
                while ($row = odbc_fetch_array($resultset)) {
                    if (isset($row['resultset'])) {
                        switch ($row['resultset']) {
                            case "CAMPANIA":
                                $arr_campania = array(
                                    'co_cam' => (int) $row['co_campania'],
                                    'ds_cam' => trim(utf8_encode($row['tx_descripcion'])),
                                    'es_cam' => trim($row['co_estado'])
                                );
                                break;
                            case "PARAMETROS":
                                $arr_param[] = array(
                                    'co_par' => (int) $row['co_parametro'],
                                    'ds_par' => trim(utf8_encode($row['descripcion'])),
                                    'va_par' => trim($row['valor'])
                                );
                                break;
                            case "CANAL":
                                $arr_canal[] = array(
                                    'co_can' => (int) $row['co_canal'],
                                    'ds_can' => trim(utf8_encode($row['ds_canal'])),
                                    'me_can' => trim(utf8_encode($row['mensaje']))
                                );
                                break;
                            case "DETALLE":
                                $arr_detalle[] = array(
                                    'di_det' => trim($row['va_desplazamiento_dias']),
                                    'es_det' => trim($row['co_estado']),
                                    'ca_det' => trim($row['canales']),
                                    'pr_det' => trim($row['prioridades'])
                                );
                                break;
                            case "MANUAL":
                                $arr_manual[] = array(
                                    'fe_man' => trim($row['fe_generacion']),
                                    'to_man' => (int) $row['total']
                                );
                                break;
                        }
                    }
                }
            } while (odbc_next_result($resultset));
        }

        return array('res_1' => $arr_campania, 'res_2' => $arr_param, 'res_3' => $arr_canal, 'res_4' => $arr_detalle, 'res_5' => $arr_manual, 'err' => $error);
    }

    function retParametros() {

        $Sql = " SELECT co_parametro, descripcion ";
        $Sql .= " FROM NTS_INTERCAMBIO..ic_robot_reglas_parametros ";
        $Sql .= " ORDER BY descripcion; ";
        $result = $this->db->query($Sql);

        $array = null;

        if ($this->db->_error_message() <> '') {
            return "ERROR AL RETORNAR PARAMETROS: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        if (count($result->result_array()) > 0) {

            foreach ($result->result_array() as $row) {
                $array[] = array(
                    'co_par' => (int) $row['co_parametro'],
                    'ds_par' => trim(utf8_encode($row['descripcion']))
                );
            }
        }

        return $array;
    }

    function retCanales() {

        $Sql = " SELECT co_canal,tx_descripcion,va_max_longitud_mensaje ";
        $Sql .= " FROM NTS_INTERCAMBIO..ic_robot_canales ";
        $Sql .= " ORDER BY tx_descripcion ";
        $result = $this->db->query($Sql);

        $array = null;

        if ($this->db->_error_message() <> '') {
            return "ERROR AL RETORNAR PARAMETROS: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        if (count($result->result_array()) > 0) {

            foreach ($result->result_array() as $row) {
                $array[] = array(
                    'co_can' => (int) $row['co_canal'],
                    'ds_can' => trim(utf8_encode($row['tx_descripcion'])),
                    'va_can' => (int) $row['va_max_longitud_mensaje'],
                );
            }
        }

        return $array;
    }

    function grabarExcel($datos, $fecha, $campania) {
        $array = array('co_err' => 1, 'tx_err' => '', 'tot' => 0);
        if (!empty($datos) && !empty($fecha) && !empty($campania)) {
            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $cedula = $dom->createElement('cedula');
            $dom->appendChild($cedula);
            foreach ($datos as $row) {
                $items = $dom->createElement("items");
                $cedula->appendChild($items);
                $items->appendChild($dom->createElement("valor", trim($row)));
            }
            $xml = $dom->saveXML();
            $Sql = " NTS_INTERCAMBIO..insert_fixed_data " . (int) $campania . ",'" . $fecha . "','" . $xml . "'; ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $array['tx_err'] = "ERROR AL GRABAR EXCEL: " . $this->db->_error_number() . " " . $this->db->_error_message();
            } else {
                $row = $resultado->row_array();
                $array['co_err'] = (int) $row['co_error'];
                $array['tx_err'] = utf8_encode(trim($row['tx_error']));
                $array['tot'] = (int) $row['registros'];
            }
        }
        return $array;
    }

    function saveReglas($datos) {

        $caracteristicas = json_decode($datos['c_a'], true);
        $parametros = json_decode($datos['d_p'], true);
        $canales = json_decode($datos['d_c'], true);
        $detalle = json_decode($datos['d_d'], true);
        $arr_error = null;
        $error = '';

        if (trim($datos['e_t']) == 'I' || trim($datos['e_t']) == 'U') {

            //if(!empty($caracteristicas['c_r']) && !empty($caracteristicas['c_e'])){

            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;

            ##<caracteristicas>
            $caracteristicas['d_r'] = str_replace(array("&", "'", ','), array('', '', ''), $caracteristicas['d_r']);

            $cabecera = $dom->createElement('caracteristicas');
            $dom->appendChild($cabecera);

            $n_2 = $dom->createElement("des_reg", trim(utf8_encode($caracteristicas['d_r'])));
            $cabecera->appendChild($n_2);

            $n_5 = $dom->createElement("reg_est", trim($caracteristicas['c_e']));
            $cabecera->appendChild($n_5);

            ##</caracteristicas>
            ##<parametros>

            $pa_param = $dom->createElement('parametros');
            $dom->appendChild($pa_param);

            for ($i = 0; $i < count($parametros); $i++) {

                $items = $dom->createElement("items");
                $pa_param->appendChild($items);

                $items->appendChild($dom->createElement("codigo", (int) $parametros[$i]['co']));
                $items->appendChild($dom->createElement("valor", trim($parametros[$i]['va'])));
            }

            ##</parametros>
            ##<canales>

            $pa_canal = $dom->createElement('canales');
            $dom->appendChild($pa_canal);

            for ($i = 0; $i < count($canales); $i++) {

                $items = $dom->createElement("items");
                $pa_canal->appendChild($items);

                $items->appendChild($dom->createElement("canal", (int) $canales[$i]['co']));

                $mensaje = $dom->createElement("mensaje");
                $items->appendChild($mensaje);
                $mensaje->appendChild($dom->createCDATASection(trim(utf8_decode($canales[$i]['me']))));
            }

            ##</canales>
            ##<detalle>

            $pa_deta = $dom->createElement('detalle');
            $dom->appendChild($pa_deta);

            for ($i = 0; $i < count($detalle); $i++) {

                $items = $dom->createElement("items");
                $pa_deta->appendChild($items);

                $items->appendChild($dom->createElement("dia_des", trim($detalle[$i]['d_tx'])));
                $items->appendChild($dom->createElement("cod_est", trim($detalle[$i]['d_es'])));
            }

            ##</detalle>
            ##<detalle_canales>

            $pa_deta_canales = $dom->createElement('detalle_canales');
            $dom->appendChild($pa_deta_canales);

            for ($i = 0; $i < count($detalle); $i++) {

                $arrCanalDet = explode(',', $detalle[$i]['d_ca']);

                if (!empty($arrCanalDet)) {
                    for ($f = 0; $f < count($arrCanalDet); $f++) {
                        if (!empty($arrCanalDet[$f])) {
                            $items = $dom->createElement("items");
                            $pa_deta_canales->appendChild($items);

                            $items->appendChild($dom->createElement("dia_des", trim($detalle[$i]['d_tx'])));
                            $items->appendChild($dom->createElement("cod_can", trim($arrCanalDet[$f])));
                            $items->appendChild($dom->createElement("sec_can", (int) $f + 1));
                        }
                    }
                }
            }

            ##</detalle_canales> 
            ##<detalle_prioridades>

            $pa_deta_prioridades = $dom->createElement('detalle_prioridades');
            $dom->appendChild($pa_deta_prioridades);

            for ($i = 0; $i < count($detalle); $i++) {

                $arrPrioDet = explode(',', $detalle[$i]['d_pr']);

                if (!empty($arrPrioDet)) {

                    for ($f = 0; $f < count($arrPrioDet); $f++) {
                        if (!empty($arrPrioDet[$f])) {
                            $items = $dom->createElement("items");
                            $pa_deta_prioridades->appendChild($items);

                            $items->appendChild($dom->createElement("dia_des", trim($detalle[$i]['d_tx'])));
                            $items->appendChild($dom->createElement("cod_pri", trim($arrPrioDet[$f])));
                        }
                    }
                }
            }

            ##</detalle_prioridades>

            $xml = $dom->saveXML();

            if (trim($datos['e_t']) == 'I') {

                $Sql = " NTS_INTERCAMBIO.._sp_pr_robot_insert_update_campania '$xml'; ";
            } else if (trim($datos['e_t']) == 'U') {

                $Sql = " NTS_INTERCAMBIO.._sp_pr_robot_insert_update_campania '$xml'," . (int) $caracteristicas['c_r'] . "; ";
            }

            $resultado = $this->db->query($Sql);

            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL GRABAR CAMPANIA: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            $row = $resultado->row_array();

            $arr_error = array(
                'co_err' => (int) $row['co_error'],
                'tx_err' => utf8_encode(trim($row['tx_error']))
            );
        } else {

            $error = 'Error al grabar,parámetros incompletos.';
        }

        return array('data' => $arr_error, 'error' => $error);
    }

    function retCalendario($anio, $mes) {

        $array = null;

        if (!empty($anio) && !empty($mes)) {

            $Sql = " EXEC NTS_INTERCAMBIO.._robot_get_calendario $anio,$mes;";
            //echo $Sql;
            $result = $this->db->query($Sql);

            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR CALENDARIO: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            if (count($result->result_array()) > 0) {

                foreach ($result->result_array() as $row) {
                    $array[] = array(
                        'co_cam' => (int) $row['co_campania'],
                        'ds_cam' => trim(utf8_encode($row['tx_descripcion'])),
                        'es_cam' => trim($row['co_estado_campania']),
                        'es_fec' => trim($row['co_estado_fecha']),
                        'fe_pla' => !empty($row['fe_ejecucion']) ? date('Y-m-d H:i:s', strtotime($row['fe_ejecucion'])) : '',
                        'fe_dia' => !empty($row['fe_ejecucion']) ? date('d', strtotime($row['fe_ejecucion'])) : '',
                        'no_dia' => !empty($row['fe_ejecucion']) ? substr(conveDia(date('N', strtotime($row['fe_ejecucion']))), 0, 3) : '',
                        'fe_mes' => !empty($row['fe_ejecucion']) ? date('m', strtotime($row['fe_ejecucion'])) : '',
                        'id_fec' => !empty($row['fe_ejecucion']) ? date('d/m', strtotime($row['fe_ejecucion'])) : '',
                        'ds_pri' => trim($row['Prioridades']),
                        'ds_can' => trim($row['Canales'])
                    );
                }
            }
        }

        return $array;
    }

    function updatePrioridadCanalesRegla($datos) {

        $canales = json_decode($datos['de_can'], true);

        if (!empty($datos['co_reg']) && !empty($canales)) {

            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;

            ##<canales>

            $pa_canal = $dom->createElement('canales');
            $dom->appendChild($pa_canal);

            for ($i = 0; $i < count($canales); $i++) {

                $items = $dom->createElement("items");
                $pa_canal->appendChild($items);

                $items->appendChild($dom->createElement("codigo", (int) $canales[$i]['co_can']));
                $items->appendChild($dom->createElement("secuencia", (int) $canales[$i]['or_can']));
            }

            ##</canales>

            $xml = $dom->saveXML();

            $Sql = " NTS_INTERCAMBIO.._sp_robot_update_prioridad_canal_regla " . (int) $datos['co_reg'] . ",'$xml'; ";

            //$this->db = $this->load->database('desarrollo', true);
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL GRABAR REGLA: " . $this->db->_error_number() . " " . $this->db->_error_message();
                die();
            }

            $row = $resultado->row_array();

            $arr_error = array(
                'co_err' => (int) $row['co_error'],
                'tx_err' => utf8_encode(trim($row['tx_error']))
            );

            return array('data' => $arr_error);
        }
    }

}
?>




