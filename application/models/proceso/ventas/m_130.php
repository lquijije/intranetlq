<?php

class m_128 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->load->library(array('connmysql'));
        $this->db->flush_cache();
    }
    
    function retMonitor($fec_ini,$fec_fin){
        
        $array_1 = $array_2 = $array_2_1 = $array_3 = $array_3_1 = $array_4 = null;
        
        $error = '';
        
        $style = array (
            '1'=>
                array(
                    'color'=>'#2E8BEF',
                    'class'=>'fa-envelope',
                ),
            '2'=>
                array(
                    'color'=>'#00A65A',
                    'class'=>'fa-whatsapp',
                ),
            '3'=>
                array(
                    'color'=>'#00A65A',
                    'class'=>'fa-whatsapp',
                ),
            '4'=>
                array(
                    'color'=>'#F0514A',
                    'class'=>'fa-phone',
                )
        );
        
        $colorEstado = array (
            'C' => array(
                'color'=>'#f0a30a',
                'nombre'=>'Cancelado'
            ),
            'P' => array(
                'color'=>'#F56954',
                'nombre'=>'Pendiente'
            ),
            'E' => array(
                'color'=>'#00A65A',
                'nombre'=>'Enviado'
            ),
            'X' => array(
                'color'=>'#2E8BEF',
                'nombre'=>'Error'
            ),
            'L' => array(
                'color'=>'#633EBE',
                'nombre'=>'IVR Encolado'
            )
            
        );
        
        if (!empty($fec_ini) && !empty($fec_fin)){
            
            $Sql = " NTS_INTERCAMBIO.dbo._sp_robot_monitor '".$fec_ini."','".$fec_fin."' ";
//            echo $Sql;
//            die;
            $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset = odbc_exec($conn,$Sql);
            
            if (odbc_error()){
                $error = "ERROR: AL RETORNAR REGLAS ".odbc_errormsg($conn);
            }
            
            do {
                
                while($row = odbc_fetch_array($resultset)){
                    
                    if(isset($row['resultset'])){
                        
                        switch ($row['resultset']) {
                            
                            case "TOTALGENERAL":
                                
                                $array_1 = array (
                                    'tot_env'=>(int)$row['tot_enviado'],
                                    'tot_gen'=>(int)$row['tot_general'],
                                    'por_act'=>(int)$row['por_actual'],
                                    'val_ges'=>(float)$row['val_gestionar'],
                                    'cos_ges'=>(float)$row['cos_gestionado']
                                );
                                
                                break;
                            case "CANAL":
                                
                                $array_2[] = array(
                                    'co_reg'=>(int)$row['co_campania'],
                                    'ds_reg'=>trim(utf8_encode($row['ds_regla'])),
                                    'co_can'=>(int)$row['co_canal'],
                                    'ds_can'=>trim(utf8_encode($row['ds_canal'])),
                                    'co_est'=>$row['co_estado'],
                                    'ca_est'=>(int)$row['cantidad']
                                );
                                
                                break;
                            case "REGLA":
                                
                                $array_3[] = array(
                                    'co_reg'=>(int)$row['co_campania'],
                                    'ds_reg'=>trim(utf8_encode($row['ds_regla'])),
                                    'es_reg'=>$row['est_reg'],
                                    'co_est'=>$row['co_estado'],
                                    'ca_est'=>(int)$row['cantidad'],
//                                    'do_est'=>(float)$row['costo']
                                );
                                
                                break;
                            case "PROVEEDOR":
                                
                                $array_4[] = array(
                                    'co_pro'=>(int)$row['co_proveedor'],
                                    'ds_pro'=>trim(utf8_encode($row['tx_descripcion'])),
                                    'es_pro'=>(int)$row['bd_status']
                                );
                                
                                break;
                        }

                    }
                    
                }
            } while(odbc_next_result($resultset));
            
            if(!empty($array_2)){
                $grupoArray = groupArray($array_2,'co_can');
                $array_2_1 = $this->monitorArrayCanales($grupoArray,$style,$colorEstado);
            }
            
            if(!empty($array_3)){
                $grupoArray3 = groupArray($array_3,'co_reg');
                $array_3_1 = $this->monitorArrayCampanias($grupoArray3,$colorEstado);
            }
            
        }
        
        return array('res_1'=>$array_1,'res_2'=>$array_2_1,'res_3'=>$array_3_1,'res_4'=>$array_4,'err'=>$error);
        
    }
    
    function monitorArrayCampanias($grupoArray,$colorEstado){
        
        $canal = null;
        
        for ($i=0;$i< count($grupoArray); $i++){

            $sumaCantidad = 0;
            $chart = null;

            $canal[] = array (
                'co_reg'=> $grupoArray[$i]['co_reg'],
                'ds_reg'=> $grupoArray[$i]['groupeddata'][0]['ds_reg'],
                'es_reg'=> $grupoArray[$i]['groupeddata'][0]['es_reg']
            );

            for ($a=0;$a< count($grupoArray[$i]['groupeddata']); $a++){

                $sumaCantidad+=(int)$grupoArray[$i]['groupeddata'][$a]['ca_est'];

                $chart [] = array (
                    'label'=> $colorEstado[trim($grupoArray[$i]['groupeddata'][$a]['co_est'])]['nombre'].' <b>'.number_format($grupoArray[$i]['groupeddata'][$a]['ca_est'],0).'</b>',
                    'data' => (int)$grupoArray[$i]['groupeddata'][$a]['ca_est'],
                    'color'=> $colorEstado[trim($grupoArray[$i]['groupeddata'][$a]['co_est'])]['color'],
                );

            }

            $canal[$i]['ca_reg'] = $sumaCantidad;
            $canal[$i]['chart'] = array('chart_dat'=>$chart);

        }
        
        return $canal;
        
    }
    
    function monitorArrayCanales($grupoArray,$style,$colorEstado){
        
        $canal = null;
        
        for ($i=0;$i< count($grupoArray); $i++){

            $sumaCantidad = 0;
            $chart = null;
            $campanias = null;

            $canal[] = array (
                'co_can'=> $grupoArray[$i]['co_can'],
                'ds_can'=> "".$grupoArray[$i]['groupeddata'][0]['ds_can'],
                'cl_can'=> $style[(int)$grupoArray[$i]['co_can']]['color'],
                'st_can'=> $style[(int)$grupoArray[$i]['co_can']]['class']
            );

            $grupoArrayRegla = groupArray($grupoArray[$i]['groupeddata'],'co_reg');
            $grupoArrayEstado = groupArray($grupoArray[$i]['groupeddata'],'co_est');
            
            for ($e=0;$e< count($grupoArrayEstado); $e++){
                
                $SumEstado = 0;
                
                for ($e1=0;$e1< count($grupoArrayEstado[$e]['groupeddata']); $e1++){
                    $SumEstado+=$grupoArrayEstado[$e]['groupeddata'][$e1]['ca_est'];
                }
                
                $chart[] = array (
                    'label'=> $colorEstado[trim($grupoArrayEstado[$e]['co_est'])]['nombre'].' <b>'.number_format($SumEstado,0).'</b>',
                    'data' => $SumEstado,
                    'color'=> $colorEstado[trim($grupoArrayEstado[$e]['co_est'])]['color'],
                );
                
            }
            
            for ($a=0;$a< count($grupoArrayRegla); $a++){
                
                $SumCampania = 0;
                
                for ($a1=0;$a1< count($grupoArrayRegla[$a]['groupeddata']); $a1++){
                    $SumCampania+=$grupoArrayRegla[$a]['groupeddata'][$a1]['ca_est'];
                }
                
                $campanias[] = array(
                    'co_reg'=>$grupoArrayRegla[$a]['co_reg'],
                    'ds_reg'=>$grupoArrayRegla[$a]['groupeddata'][0]['ds_reg'],
                    'to_reg'=> $SumCampania
                );
                $sumaCantidad+=$SumCampania;
            }
               
            $canal[$i]['ca_can'] = $sumaCantidad;
            $canal[$i]['chart'] = array('chart_dat'=>array_values($chart));
            $canal[$i]['campanias'] = $campanias;

        }
        
        return $canal;
        
    }
    
    function retCanalesReporte($fecha,$cod_ca = ''){
        
        $bandera = true;
        $error   = '';
        $array_1 = $array_2 = $array_3 = null;
        
        if(trim($fecha) <> ''){

            $db = new connmysql();
            $db->host = "130.51.12.100";
            $db->user = "login";
            $db->pass = "siste2365";
            $db->db = "canales";
            $db->connectPDO();
            
            if(empty($db->err)){
                
                try {
                
                    $stmt = $db->cnn->prepare('CALL canales.reporte(:param1, :param2, :param3, :param4)');
                    
                    $execute = $stmt->execute(array(
                        ':param1' => date('Y',strtotime($fecha)), 
                        ':param2' => date('m',strtotime($fecha)), 
                        ':param3' => date('d',strtotime($fecha)), 
                        ':param4' => $cod_ca
                    ));
                    
                    if (!$execute) {
                        throw new PDOException ("Error");
                    }
                    
                } catch (PDOException $e) {
                    $bandera = false;
                    $error = "ERROR AL CONSULTAR DETALLE FACTURA EXECUTE: ";
                }
                
                if($bandera){
                    
                    do {

                        while($row = $stmt->fetch()){
                            
                            if(isset($row['resultset'])){

                                switch ($row['resultset']) {

                                    case "DETALLE":

                                        $array_1[] = array(
                                            'grupo2'=> trim($row['grupo2']),
                                            'id'=> (int)$row['id'],
                                            'name'=> trim($row['name']),
                                            'hora_sin'=>date('H:i',strtotime($row['hora_sin'])),
                                            //'hora_sin'=>date('H:i a',strtotime($row['hora_sin'])),
                                            'hora'=> trim($row['hora']),
                                            'call'=> (int)$row['calls']
                                        );

                                        break;
                                    case "TOTAL":

                                        $array_2[] = array(
                                            'grupo2'=> trim($row['grupo2']),
                                            'hora_sin'=>date('H:i',strtotime($row['hora_sin'])),
                                            'hora'=> trim($row['hora']),
                                            'call'=> (int)$row['calls'],
                                        );

                                        break;
                                    case "CONCURRENCIA":

                                        $array_3[] = array(
                                            'call'=> (int)$row['calls'],
                                            'veces'=> (int)$row['veces']
                                        );

                                        break;
                                }

                            }

                        }

                    } while($stmt->nextRowset());
                    
                }
                
            } else {
                $error = $db->err;
            }
            
            $db->closePDO();

        } else {
            $error = 'Parametros imcompletos';
        }
        
        return array('res_1'=>$array_1,'res_2'=>$array_2,'res_3'=>$array_3,'err'=>$error);
        
    }
    
    function retReport($data){
        
        $array = null;
        $estados = array (
            'C' => array(
                'name'=>'Cancelado'
            ),
            'P' => array(
                'name'=>'Pendiente'
            ),
            'E' => array(
                'name'=>'Enviado'
            ),
            'X' => array(
                'name'=>'Error'
            ),
            'L' => array(
                'name'=>'IVR Encolado'
            )
            
        );
        
        if(!empty($data['c_c']) && !empty($data['f_f'])){
            
            $Sql = " NTS_INTERCAMBIO.._sp_robot_reporte ".(int)$data['c_c'].",'".trim($data['f_f'])."' ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                echo "ERROR AL RETORNAR REPORTE <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row){
                   
                    $array[] = array(
                        'co_cli'=> (int)$row['co_cliente'],
                        'cc_cli'=> trim($row['co_cedula']),
                        'ap_cli'=> trim(utf8_encode($row['cl_apellidos'])),
                        'no_cli'=> trim(utf8_encode($row['cl_nombres'])),
                        'va_dia'=> (int)$row['va_dias_vencidos'],
                        'va_min'=> (float)$row['va_pago_minimo'],
                        'co_est_cab'=> trim($row['co_estado_cab']),
                        'no_est_cab'=> $estados[$row['co_estado_cab']]['name'],
                        'co_can'=> (int)$row['co_canal'],
                        'co_pro'=> (int)$row['co_proveedor'],
                        'no_pro'=> trim($row['tx_descripcion']),
                        'fe_env'=> trim($row['fe_envio']),
                        'contac'=> trim($row['contacto']),
                        'tx_men'=> trim(utf8_encode($row['tx_mensaje'])),
                        'tx_res'=> trim(utf8_encode($row['tx_respuesta_envio'])),
                        'va_dur'=> trim($row['va_duracion']),
                        'co_est_det'=> trim($row['co_estado_det']),
                        'no_est_det'=> $estados[$row['co_estado_det']]['name']
                    );
                    
                }
                
            }
            
        }
        
        return $array;
        
    }
    
    function updateEstadoProveedor($onoff,$id_pro){
        
        $array = null;
        
        if(!empty($id_pro)){
            
            $Sql = " nts_intercambio.._sp_pr_robot_update_estado_proveedor ".$id_pro.",".$onoff." ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL ACTUALIZAR PROVEEDOR: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            $row = $result->row_array();
            $array = array(
                'co_err'=>(int)$row['co_error'],
                'tx_err'=>utf8_decode($row['tx_error'])
            );
            
        }
        
        return $array;
        
    }
    
    function updateEstadoCampania($onoff,$id_cam){
        
        $array = null;
        
        if(!empty($id_cam)){
            
            $Sql = " nts_intercambio.._sp_pr_robot_update_estado_campania ".$id_cam.",".$onoff." ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL ACTUALIZAR CAMPANIA: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            $row = $result->row_array();
            $array = array(
                'co_err'=>(int)$row['co_error'],
                'tx_err'=>utf8_decode($row['tx_error'])
            );
            
        }
        
        return $array;
        
    }
    
    function retReglaRobot($cod_reg){
        
        $arr_regla = $arr_param = $arr_canal = $arr_hora = null;
        $error = '';
        
        if (!empty($cod_reg)){
            
            $Sql = " NTS_INTERCAMBIO.dbo._sp_robot_consulta_regla ".trim($cod_reg)." ";
            $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset = odbc_exec($conn,$Sql);
            
            if (odbc_error()){
                $error = "ERROR: AL RETORNAR REGLAS ".odbc_errormsg($conn);
            }
            
            do {
                
                while($row = odbc_fetch_array($resultset)){
                    
                    if(isset($row['resultset'])){
                        
                        switch ($row['resultset']) {
                            
                            case "PARAMETROS":
                                
                                $arr_regla = array (
                                    'co_reg'=>(int)$row['co_regla'],
                                    'ds_reg'=>trim(utf8_encode($row['tx_descripcion'])),
                                    'va_dia'=>(int)$row['va_dia_inicio'],
                                    'va_int'=>(int)$row['va_intervalo'],
                                    'va_top'=>(int)$row['va_tope_gestiones'],
                                    'co_est'=>trim($row['co_estado']),
                                );

                                $arr_param[] = array(
                                    'co_par'=>(int)$row['co_parametro'],
                                    'ds_par'=>trim(utf8_encode($row['descripcion'])),
                                    'va_par'=>$row['valor']
                                );
                                
                                break;
                            case "CANAL":
                                
                                $arr_canal[] = array(
                                    'co_can'=>(int)$row['co_canal'],
                                    'ds_can'=>trim(utf8_encode($row['tx_descripcion'])),
                                    'se_can'=>(int)$row['co_secuencia'],
                                    'me_can'=>trim(utf8_encode($row['mensaje']))
                                );
                                
                                break;
                            case "HORARIO":
                                
                                $arr_hora[] = array(
                                    'di_hor'=>(int)$row['dia_semana'],
                                    'fe_des'=> !empty($row['fe_desde'])?date('H:i',strtotime($row['fe_desde'])):'00:00',
                                    'fe_has'=> !empty($row['fe_hasta'])?date('H:i',strtotime($row['fe_hasta'])):'00:00'
                                );
                                
                                break;
                        }

                    }
                    
                }
            } while(odbc_next_result($resultset));
            
        }
        
        return array('res_1'=>$arr_regla,'res_2'=>$arr_param,'res_3'=>$arr_canal,'res_4'=>$arr_hora,'err'=>$error);
        
    }
    
    function BuscaCedulaNumero($fe_bus,$co_bus){
        
        $array = null;
        $estados = array (
            'C' => array(
                'name'=>'Cancelado'
            ),
            'P' => array(
                'name'=>'Pendiente'
            ),
            'E' => array(
                'name'=>'Enviado'
            ),
            'X' => array(
                'name'=>'Error'
            ),
            'L' => array(
                'name'=>'IVR Encolado'
            )
            
        );
        
        if(!empty($fe_bus) && !empty($co_bus)){
            
            $Sql = " NTS_INTERCAMBIO.._sp_robot_consulta_contacto '".trim($fe_bus)."','".trim($co_bus)."';";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                echo "ERROR AL RETORNAR BUSQUEDA <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row){
                           
                    $array[] = array(
                        'co_cam'=> (int)$row['co_campania'],
                        'ds_cam'=> trim($row['ds_campania']),
                        'co_cli'=> (int)$row['co_cliente'],
                        'ce_cli'=> trim($row['co_cedula']),
                        'ap_cli'=> trim(utf8_encode($row['cl_apellidos'])),
                        'no_cli'=> trim(utf8_encode($row['cl_nombres'])),
                        'va_dia'=> (int)$row['va_dias_vencidos'],
                        'va_min'=> (float)$row['va_pago_minimo'],
                        'va_ven'=> (float)$row['va_pago_vencido'],
                        'va_sal'=> (float)$row['va_saldo'],
                        'fe_pag'=> trim($row['fe_pago']),
                        'co_est_cab'=> trim($row['estado_cab']),
                        'no_est_cab'=> $estados[$row['estado_cab']]['name'],
                        'co_sec'=> (int)$row['co_secuencia'],
                        'co_con'=> trim($row['contacto']),
                        'co_can'=> (int)$row['co_canal'],
                        'ds_can'=> trim($row['ds_canal']),
                        'no_pro'=> trim($row['ds_proveedor']),
                        'co_est_det'=> trim($row['estado_det']),
                        'no_est_det'=> $estados[$row['estado_det']]['name'],
                        'va_dur'=> trim($row['va_duracion']),
                        'va_pre'=> (float)$row['va_precio'],
                        'fe_env'=> trim($row['fe_envio']),
                        'tx_res'=> trim(utf8_encode($row['tx_respuesta_envio'])),
                        'tx_men'=> trim(utf8_encode($row['tx_mensaje']))
                    );
                    
                }
                
            }
            
        }
        
        return $array;
        
    }
    
    function retDetLlamada($fec_ini){
        
        $arr_chart = $arr_resum = null;
        $error = '';
        
        if (!empty($fec_ini)){
            
            $Sql = " NTS_INTERCAMBIO.dbo._robot_consulta_ivr_log '".trim($fec_ini)."'; ";
            $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset = odbc_exec($conn,$Sql);
            
            if (odbc_error()){
                $error = "ERROR: AL RETORNAR DETALLE LLAMADAS ".odbc_errormsg($conn);
            }
            
            do {
                
                while($row = odbc_fetch_array($resultset)){
                    
                    if(isset($row['resultset'])){
                        
                        switch ($row['resultset']) {
                            
                            case "CHART":
                                
                                $arr_chart[] = array(
                                    'ou_dial'=>trim($row['outDialResult']),
                                    'ho_dial'=>trim($row['hora']),
                                    'ca_dial'=>(int)$row['cantidad']
                                );
                                
                                break;
                            case "RESUMEN":
                                
                                $arr_resum[] = array(
                                    'ou_dial'=>trim($row['outDialResult']),
                                    'ca_dial'=>(int)$row['cantidad']
                                );
                                
                                break;
                        }

                    }
                    
                }
            } while(odbc_next_result($resultset));
            
        }
        
        return array('res_1'=>$arr_chart,'res_2'=>$arr_resum,'err'=>$error);
        
    }
    
}

?>




