<?php

class m_131 extends CI_Model {

    function __construct() {
        parent::__construct();
        //$this->db = $this->load->database('desarrollo', true);
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    public $tipos = array(
        'C' => array(
            'name' => 'Cambio',
        ),
        'V' => array(
            'name' => 'Devolución',
        )
    );
    public $estados = array(
        'E' => array(
            'name' => 'Pendiente',
        ),
        'P' => array(
            'name' => 'Procesado',
        ),
        'C' => array(
            'name' => 'Caducado',
        ),
        'A' => array(
            'name' => 'Autorizado',
        ),
        'R' => array(
            'name' => 'Rechazado',
        ),
        'X' => array(
            'name' => 'Anulado',
        )
    );

    function saveSoliciAut($data) {

        $array = null;

        if (!empty($data['t_t']) && !empty($data['t_o']) && !empty($data['f_f']) && !empty($data['c_a']) && !empty($data['c_c']) && !empty($data['c_d']) && !empty($_SESSION['c_e'])) {

            $data['t_o'] = str_replace(array('"', "'"), array('', ''), utf8_encode($data['t_o']));
            $Sql = " PODBPOS..posgen_solicituddevolucion_insertar " . trim($data['c_a']) . "," . trim($data['c_a']) . "," . trim($data['c_c']) . "," . trim($data['c_d']) . ",'" . trim($data['t_t']) . "'," . trim($_SESSION['c_e']) . ",'" . trim($data['t_o']) . "','" . trim($data['f_f']) . "';";
            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $array = array(
                    'co_err' => $this->db->_error_number(),
                    'tx_err' => "ERROR AL INGRESAR SOLICITUD: " . $this->db->_error_message()
                );
            } else {
                $raw = $result->row_array();
                $array = array(
                    'co_err' => (int) $raw['severidad'],
                    'tx_err' => utf8_decode($raw['mensaje'])
                );
            }
        } else {
            $array = array(
                'co_err' => 1,
                'tx_err' => 'PARAMETROS INCOMPLETOS'
            );
        }

        return $array;
    }

    function retFactNTPYCCA($data) {

        $error = '';
        $array_rec_1 = $array_rec_2 = null;
        $can_art = $can_dyc = $bandera = 0;

        if (!empty($data['c_b']) && !empty($data['c_c']) && !empty($data['c_d']) && !empty($data['f_f'])) {

            $Sql = " IVDBINVENTAR..jf_constran " . (int) $data['c_b'] . "," . (int) $data['c_c'] . "," . (int) $data['c_d'] . ",'F','" . trim($data['f_f']) . "',2; ";
            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = "ERROR AL CONSULTAR DETALLE DOCUMENTO <BR>" . $this->db->_error_number() . " " . $this->db->_error_message();
            } else {
                $arrResult = $result->result_array();
                if (count($arrResult) > 0) {
                    foreach ($arrResult as $row) {
                        $can_art = (int) trim($row['cantidad']);
                        $can_dyc = (int) trim($row['cant_dyc']);
                        $array_rec_2[] = array(
                            'cod_lin' => (int) trim($row['linea']),
                            'cod_art' => trim($row['codigo']),
                            'des_art' => utf8_encode(trim($row['descripcion'])),
                            'can_art' => (int) trim($row['cantidad']),
                            'pvp_art' => (float) trim($row['pvp']),
                            'fec_fac' => trim($row['fecha']),
                            'fla_dyc' => (int) trim($row['flag_dyc']),
                            'can_dyc' => (int) trim($row['cant_dyc']),
                            'cod_cli' => trim($row['cod_cliente'])
                        );
                    }
                    $array_rec_1 = $this->retCliente($array_rec_2[0]['cod_cli']);
                }
            }
        }

        if ($can_art <> $can_dyc) {
            $bandera = 1;
        }

        return array('dt_1' => $array_rec_1, 'dt_2' => $array_rec_2, 'err' => $error, 'dyc' => $bandera);
    }

    function retCliente($CI) {

        $array = null;

        if (!empty($CI)) {

            $Sql = " SGDBSEGURIDAD.._consulta_cliente '" . $CI . "' ";
            //$this->db = $this->load->database('desarrollo', true);
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR CLIENTE <BR> COD_ERR:" . $this->db->_error_number() . " <BR> " . $this->db->_error_message();
            } else {
                if (count($resultado->row_array()) > 0) {

                    $row = $resultado->row_array();

                    $array = array(
                        'ide_cli' => trim($CI),
                        'nom_cli' => trim(utf8_encode($row['perNombre1'])),
                        'dir_cli' => trim(utf8_encode($row['direccion'])),
                        'tel_cli' => trim($row['telefono']),
                        'mail_cli' => strtolower(trim($row['correo']))
                    );
                }
            }
        }

        return $array;
    }

    function retSolicitudes($cod_alm, $cod_empl) {

        $array = null;

        if (!empty($cod_alm) && !empty($cod_empl)) {

            $Sql = " SELECT a.co_solicitud,a.ti_autorizacion,a.fe_solicitud,a.de_comentariocreacionsolicitud,a.co_bodega,b.de_sri,a.co_caja,a.nu_factura,a.tipo_documento,a.rg_estado ";
            $Sql .= " FROM PODBPOS..posgensolicitudesdevolucion a ";
            $Sql .= " INNER JOIN PODBPOS..posgenbodega b ";
            $Sql .= " ON a.co_bodega = b.co_bodega ";
            $Sql .= " WHERE a.co_almacensolicita = $cod_alm; ";
            //$Sql .= " AND a.co_gerentealmacen = $cod_empl;";
            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR SOLICITUDES: " . $this->db->_error_number() . " " . $this->db->_error_message();
            } else {
                $resArray = $result->result_array();
                if (count($resArray) > 0) {
                    foreach ($resArray as $row) {
                        $array[] = array(
                            'co_sol' => (int) $row['co_solicitud'],
                            'ti_aut' => trim($row['ti_autorizacion']),
                            'no_ti_aut' => $this->tipos[trim($row['ti_autorizacion'])]['name'],
                            'de_sol' => trim($row['fe_solicitud']),
                            'ms_sol' => trim(utf8_encode($row['de_comentariocreacionsolicitud'])),
                            'co_bdg' => (int) $row['co_bodega'],
                            'co_caj' => (int) $row['co_caja'],
                            'nu_doc' => (int) $row['nu_factura'],
                            'nu_fac' => trim($row['de_sri'] . "-" . $row['co_caja'] . "-" . $row['nu_factura']),
                            'ti_doc' => trim($row['tipo_documento']),
                            'es_sol' => trim($row['rg_estado']),
                            'no_es_sol' => $this->estados[trim($row['rg_estado'])]['name']
                        );
                    }
                }
            }
        }
        return $array;
    }

    function retPermSolic($cod_empl) {

        $array = null;

        if (!empty($cod_empl)) {

            $Sql = " SELECT a.ti_tipoautorizacion, ";
            $Sql .= " a.co_gerentealmacen,b.e_mail+'' AS email_gerentealmacen, ";
            $Sql .= " a.co_gerenteregional,c.e_mail+'' AS email_gerenteregional ";
            $Sql .= " FROM PODBPOS..posgengerenteregionalalmacen a ";
            $Sql .= " INNER JOIN ntpycca.webpycca.dbo.tb_intranet_users b ";
            $Sql .= " ON a.co_gerentealmacen = b.cod_empleado ";
            $Sql .= " INNER JOIN ntpycca.webpycca.dbo.tb_intranet_users c ";
            $Sql .= " ON a.co_gerenteregional = c.cod_empleado ";
            $Sql .= " WHERE a.co_gerentealmacen = " . trim($cod_empl) . ";";

            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR SOLICITUDES: " . $this->db->_error_number() . " " . $this->db->_error_message();
            } else {
                $resArray = $result->result_array();

                if (count($resArray) > 0) {

                    foreach ($resArray as $row) {

                        $array[] = array(
                            'ti_aut' => trim($row['ti_tipoautorizacion']),
                            'no_ti_aut' => $this->tipos[trim($row['ti_tipoautorizacion'])]['name'],
                            'co_gea' => (int) $row['co_gerentealmacen'],
                            'em_gea' => trim($row['email_gerentealmacen']),
                            'co_ger' => (int) $row['co_gerenteregional'],
                            'em_ger' => trim($row['email_gerenteregional'])
                        );
                    }
                }
            }
        }

        return $array;
    }

    function retPermisosSolicitud($cod_empl) {

        $array = null;

        if (!empty($cod_empl)) {

            $Sql = " SELECT DISTINCT ti_tipoautorizacion FROM PODBPOS..posgengerenteregionalalmacen WHERE co_gerentealmacen = " . trim($cod_empl) . ";";
            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL RETORNAR SOLICITUDES: " . $this->db->_error_number() . " " . $this->db->_error_message();
            } else {
                $resArray = $result->result_array();
                if (count($resArray) > 0) {
                    foreach ($resArray as $row) {
                        $array[] = array(
                            'ti_aut' => trim($row['ti_tipoautorizacion']),
                            'no_ti_aut' => $this->tipos[trim($row['ti_tipoautorizacion'])]['name']
                        );
                    }
                }
            }
        }
        return $array;
    }

}

?>
