<?php

class m_118 extends CI_Model{

    function __construct(){
        parent::__construct();
        //$this->db = $this->load->database('desarrollo', true);
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function reDocsProv($data){
        
        if(!empty($data)){
            
            $Sql = " EXEC PODBPOS.._ipycca_ret_reem_induccion '".$data['f_i']."','".$data['f_f']."','".$data['t_p']."' ";
            $query = $this->db->query($Sql); 
            
            if ($this->db->_error_message() <> ''){
                RETURN "ERROR AL RETORNAR DOCUMENTOS <BR> COD_ERR:".$this->db->_error_number()." - ".$this->db->_error_message();
                die;
            }
            
            $result = $query->result_array();
            $array = null;
            $nomb_esta = '';
            
            foreach ($result as $row){
                if(trim($row['de_estado']) === 'A'){
                    $nomb_esta = 'PENDIENTE';
                } else if(trim($row['de_estado']) === 'E'){
                    $nomb_esta = 'ENVIADO';
                } else if(trim($row['de_estado']) === 'C'){
                    $nomb_esta = 'CANCELADO';
                }
                
                $array[] = array(
                    'cod_bdg'=>$row['co_bodega'],
                    'cod_sri'=>(int)$row['sri'],
                    'des_bdg'=>utf8_encode(trim($row['descripcion'])),
                    'tip_doc'=>$row['ti_documento'],
                    'cod_caj'=>$row['co_caja'],
                    'num_doc'=>$row['nu_documento'],
                    'num_fac'=>trim($row['sri'])."-".str_pad(trim($row['co_caja']),3,0, STR_PAD_LEFT)."-".str_pad(trim($row['nu_documento']),9,0, STR_PAD_LEFT),
                    'cod_tra'=>$row['co_transaccion'],
                    'fec_tra'=>trim($row['fe_transaccion']) <> ''?date('Y/m/d H:i',strtotime($row['fe_transaccion'])):'',
                    'ide_cli'=>$row['co_cliente'],
                    'nom_cli'=>utf8_encode(trim($row['nomb_cliente'])),
                    'cod_art'=>$row['co_articulo'],
                    'des_art'=>utf8_encode(trim($row['ds_articulo'])),
                    'num_ser'=>$row['nu_serie'],
                    'val_cuo'=>$row['va_cuotas'],
                    'val_tot'=>number_format($row['va_total'],2),
                    'val_otr'=>number_format($row['va_otrasfp'],2),
                    'cod_pro'=>$row['co_proveedor'],
                    'nom_pro'=>utf8_encode(trim($row['nomb_proveedor'])),
                    'fec_env'=>trim($row['fe_envio']) <> ''?date('Y/m/d H:i',strtotime($row['fe_envio'])):'',
                    'user_env'=>$row['co_usuario_envio'],
                    'nomb_user_env'=>utf8_encode(trim($row['nomb_user_envio'])),
                    'fec_canc'=>trim($row['fe_cancelacion']) <> ''?date('Y/m/d H:i',strtotime($row['fe_cancelacion'])):'',
                    'user_can'=>$row['co_usuario_cancelacion'],
                    'nomb_user_can'=>utf8_encode(trim($row['nomb_user_cancelacion'])),
                    'fec_esti'=>trim($row['fe_estimada_cancelacion']) <> ''?date('Y/m/d H:i',strtotime($row['fe_estimada_cancelacion'])):'',
                    'cla_esti'=>date('Y/m/d H:i',strtotime($row['fe_estimada_cancelacion'])) <= date('Y/m/d H:i')?'red':'',
                    'cod_esta'=>$row['de_estado'],
                    'nom_esta'=>$nomb_esta
                ); 
            }

            return array('data'=>$array);
            
        }
        
    }
    
    function getProvIndu(){
        
        $Sql  = " SELECT DISTINCT b.co_proveedor, c.nomb_proveedor ";
        $Sql .= " FROM podbpos..posgenArticulosFinanciamientoEstado a ";
        $Sql .= " INNER JOIN ivdbinventar..ivtbarticulo b ON a.co_articulo=b.co_articulo ";
        $Sql .= " INNER JOIN sgdbseguridad..matbproveedores c ON b.co_proveedor=c.codi_proveedor and codi_empresa=27 ";
        
        $query = $this->db->query($Sql); 
        if ($this->db->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR PROVEEDORES <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        }
        $result = $query->result_array();
        $array = null;
        
        foreach ($result as $row){
            
            $array[] = array(
                'cod_pro'=>$row['co_proveedor'],
                'nom_pro'=>utf8_encode($row['nomb_proveedor'])
            ); 
        }
        
        return $array;
        
    }
    
    function updateEstado($data){
        
        if(!empty($data)){
            $Sql = " PODBPOS.._ipycca_update_estados_induccion ".$data['c_b'].",".$data['c_c'].",".$data['c_t'].",".$data['n_d'].",'".$data['e_d']."','".$data['t_t']."',".$_SESSION['c_e']." ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL ACTUALIZAR ESTADO: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            
            $row = $result->row_array();
            
            if ((int)$row['co_error'] === 0){
                echo 'OK-'.utf8_encode(trim($row['tx_error']));
            } else {
                echo $row['tx_error'];
            }
        }
    }
    
}
?>
