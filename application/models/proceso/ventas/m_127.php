<?php

class m_127 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        ///$this->db = $this->load->database('desarrollo', true);
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function verFactura($datos){
        
        $array = null;
        $error = '';
        
        if(!empty($datos)){
            
            if(!empty($datos['c_a']) && !empty($datos['c_c']) && !empty($datos['c_d'])){
                
                if(is_numeric($datos['c_a']) && is_numeric($datos['c_c']) && is_numeric($datos['c_d'])){
                    
                    $Sql = " PODBPOS.._sp_valida_promocion_pavo '".trim($datos['c_a'])."',".trim($datos['c_c']).",".trim($datos['c_d'])." ";
//                    echo $Sql;
//                    die;
                    $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
                    $resultset = odbc_exec($conn,$Sql);

                    if (odbc_error()){
                        $error = "ERROR AL VALIDAR PROMOCION: ".odbc_errormsg($conn);
                    }

                    $row = odbc_fetch_array($resultset);

                    if ((int)$row['co_err'] == 0){

                        odbc_next_result($resultset);
                        $raw = odbc_fetch_array($resultset);
                        
                        if(!empty($raw)){
                            
                            $array = array(
                                'co_bo'=>(int)$raw['co_bodega'],
                                'co_ca'=>(int)$raw['co_caja'],
                                'nu_do'=>(int)$raw['nu_documento'],
                                'va_ve'=>(float)$raw['va_valorventa'],
                                'co_cl'=>trim(utf8_encode($raw['ds_idcliente'])),
                                'no_cl'=>trim(utf8_encode($raw['ds_nombre'])),
                                'te_cl'=>trim(utf8_encode($raw['ds_telefono'])),
                                'fe_ve'=>empty($raw['fe_compra'])?'':date('d/m/Y H:i',strtotime($raw['fe_compra'])),
                                'co_em'=>(int)$raw['co_empleado']
                            );

                        }
                    } else {
                        $error = utf8_encode($row['tx_err']);
                    }
                } else {
                    $error = 'Debe ingresar solo números';
                }
                
            }
            
        }
        
        return array('data'=>$array,'error'=>$error);
            
    }
    
    function ingCupon($datos){
        
//        print_r($datos);
//        DIE;
        $co_err = 0;
        $error = '';
        
        if(!empty($datos)){
            
            if(!empty($datos['c_a']) && !empty($datos['c_c']) && !empty($datos['c_d'])){
                
                if(is_numeric($datos['c_a']) && is_numeric($datos['c_c']) && is_numeric($datos['c_d'])){
                    
                    $verFact = $this->verFactura($datos);
                    //print_r($result);
                    $tx_obser = '';
                    if(!empty($verFact['data']) && empty($verFact['error'])){
                        $Sql = " PODBPOS.._sp_update_promocion_pavo '".trim($datos['c_a'])."',".trim($datos['c_c']).",".trim($datos['c_d'])." ";
//                        echo $Sql;
//                        die;
                        $result = $this->db->query($Sql);
                        if ($this->db->_error_message() <> ''){
                            $error = "ERROR AL ACTUALIZAR PROMOCION: ".$this->db->_error_number()." ".$this->db->_error_message();
                        } else {

                            $row = $result->row_array();
                            $co_err = (int)$row['co_err'];
                            $error  = utf8_decode($row['tx_err']);
                            
                        }
                    }
                    
                } else {
                    $co_err = 1;
                    $error = 'Debe ingresar solo números.';
                }

            } else {
                $co_err = 1;
                $error = 'Parámatros Incompletos.';
            }
        
        } else {
            $co_err = 1;
            $error = 'Parámatros Incompletos.';
        }
        
        return array('data'=>$co_err,'error'=>$error);
            
    }
}
?>
