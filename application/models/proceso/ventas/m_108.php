<?php

class m_108 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function reDatVentas($fecha,$hora){
        
        @ini_set('display_errors', 0);
        error_reporting(0);

        if (!empty($fecha)){
            $Sql = " [db_estadisticas].dbo._widget_ventas '".trim($fecha)."','".trim($hora)."',1 ";
            $conn=odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset=odbc_exec($conn,$Sql);
            
            if (odbc_error()){
                return "ERROR AL RETORNAR VENTAS DIARIAS <BR>".odbc_errormsg($conn);
                die;
            }
            
            $array = $data = null;
            while( $row = odbc_fetch_array($resultset) ) {
                $array[] = array('semana'=>$row['semana'],'anio'=>$row['anio'],'anioDia'=>$row['anioDia']);
            } 
            $label = $array;
            odbc_next_result($resultset);
            $a = 0;
            while( $row = odbc_fetch_array($resultset) ) {
                $data[] = array('co_bodega'=>$row['co_bodega'],
                                'descripcion'=>$row['descripcion'],
                                'venta'=>$row['venta'],
                                'venta_07'=>$row['venta_07'],
                                'por_07'=>number_format($row['por_07'],2,'.',','),
                                'venta_365'=>$row['venta_365'],
                                'por_365'=>number_format($row['por_365'],2,'.',','),
                                'venta_365xDia'=>$row['venta_365xDia'],
                                'por_365xDia'=>number_format($row['por_365xDia'],2,'.',','));
                $a+=$row['venta'];
            } 
            
            if ($a>0){
                echo "OK-".json_encode($label)."/*/".json_encode($data);
            } else {
                echo 'No hay datos en la busqueda realizada';
            }
        }
    }
}
?>
