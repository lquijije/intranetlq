<?php
ini_set('max_execution_time', 300);
ini_set('display_errors', 1);
error_reporting(E_ALL);

class m_110 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function aprobarSolicit($data){
        
        $emp = trim($data['i_e']);
        $obs = trim($data['o_s']);
        $det = json_decode($data['d_s'],true);
        $arrayResult = null;
        
        if(!empty($emp) && !empty($det)){
            
            for ($a = 0; $a < count($det); $a++){
                
                $Sql = "PRDBPROCESO.._solcheque_aprobar ".(int)$det[$a]['c_s'].",'".trim($emp)."',".(int)$_SESSION['c_e'].",'".trim($det[$a]['e_s'])."',".trim($det[$a]['t_s']).",'".trim($obs)."';";
                $DB1 = $this->load->database('sqlnsip', true);
                $result = $DB1->query($Sql);
                
                if ($DB1->_error_message() <> ''){
                    return "ERROR AL ACTUALIZAR SOLICITUD: ".$DB1->_error_number()." ".$DB1->_error_message();
                    break;
                }
                
                $raw = $result->row_array();
                
                if(count($raw)>0){
                    $arrayResult[] = array (
                        'c_s'=>$det[$a]['c_s'],
                        'd_e'=>array(
                            'co_err'=>(int)$raw['co_error'],
                            'tx_err'=>utf8_decode($raw['tx_error'])
                        )
                    );
                }
                
            }
            
        }
            
        return $arrayResult;
            
    }
    
    function viewSolicit($data){
        
        $error = '';
        $array_rec_1 = $array_rec_2 = $array_rec_3 = $array_rec_4 = null;
        
        if (!empty($data['i_e']) && !empty($data['i_s'])){
            
            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });
            
            try
            {
                $Sql = " PRDBPROCESO.._solcheque_retorna_solicitud '".$data['i_e']."','".$data['i_s']."' ";
                $conn = odbc_connect("SQLNSIP-P", "pica", "");
                $resultset = odbc_exec($conn,$Sql);
                
                do {

                    while($row = odbc_fetch_array($resultset)){

                        if(isset($row['resultset'])){

                            switch ($row['resultset']) {

                                case "SOLICITUD":

                                    $array_rec_1 = array(
                                        'a'=>trim($row['codi_docu']),
                                        'b'=>utf8_encode(trim($row['nomb_docu'])),
                                        'c'=>trim($row['nume_docu']),
                                        'd'=>utf8_encode(trim($row['concepto'])),
                                        'e'=>utf8_encode(trim($row['soli_fina'])),
                                        'f'=>date('Y/m/d',strtotime(trim($row['fech_docu']))),
                                        'g'=>trim($row['fech_soli']),
                                        'h'=>trim($row['iden_prove']),
                                        'i'=>utf8_encode(trim($row['nomb_bene'])),
                                        'j'=>utf8_encode(trim($row['nomb_prove'])),
                                        'k'=>trim($row['ccosto']),
                                        'l'=>utf8_encode(trim($row['nomb_ceco'])),
                                        'm'=>trim($row['responsable']),
                                        'n'=>utf8_encode(trim($row['nomb_empl'])),
                                        'o'=>utf8_encode(trim($row['obse_soli1'])),
                                        'p'=>utf8_encode(trim($row['obse_soli'])),
                                        'q'=>date('Y/m/d',strtotime(trim($row['fech_venc']))),
                                        'r'=>trim($row['valor']),
                                        's'=>trim($row['valo_adicionales']),
                                        't'=>(int)trim($row['porc_desc']),
                                        'u'=>trim($row['valo_desc']),
                                        'v'=>trim($row['valo_vari']),
                                        'w'=>(int)trim($row['iva']),
                                        'x'=>trim($row['valo_iva']),
                                        'y'=>trim($row['nomb_empr']),
                                        'z'=>trim($row['status'])
                                    );

                                    break;
                                case "AUTORIZADORES":

                                    $fec = trim($row['fech_accion']) <> ''?date('Y/m/d',strtotime(trim($row['fech_accion']))):trim($row['fech_accion']);
                                    $array_rec_2[] = array(
                                        'a'=>(int)trim($row['secu_usua']),
                                        'b'=>(int)trim($row['codi_empl']),
                                        'c'=>utf8_encode(trim($row['nomb_empl'])),
                                        'd'=>utf8_encode(trim($row['tipo_accion'])),
                                        'e'=>$fec,
                                        'f'=>(int)trim($row['codi_remp']),
                                        'g'=>utf8_encode(trim($row['nomb_remp']))
                                    );

                                    break;
                                case "PLAN_PAGO":

                                    $array_rec_3[] = array(
                                        'a'=>(int)trim($row['secu_abon']),
                                        'b'=>date('Y/m/d',strtotime(trim($row['fech_abon']))),
                                        'c'=>(int)trim($row['porcentaje'])
                                    );

                                    break;
                                case "ASIENTOS":

                                    $array_rec_4[] = array(
                                        'a'=>(int)trim($row['secu_soli']),
                                        'b'=>utf8_encode(trim($row['actdescr'])),
                                        'c'=>utf8_encode(trim($row['actnumbr_1'])),
                                        'd'=>utf8_encode(trim($row['actnumbr_2'])),
                                        'e'=>utf8_encode(trim($row['actnumbr_3'])),
                                        'f'=>utf8_encode($row['nom_ceco']),
                                        'g'=>trim($row['debe']),
                                        'h'=>trim($row['haber'])
                                    );

                                    break;
                            }

                        }

                    }
                } while(odbc_next_result($resultset));
            }
            catch (Exception $e)
            {
                $error = "ERROR AL RETORNAR SOLICITUD.";
                
            }

        }
        
        return array('cab'=>$array_rec_1,'auto'=>$array_rec_2,'pago'=>$array_rec_3,'cont'=>$array_rec_4,'err'=>$error);
        
    }
    
    function retEmpresasAsignadas($co_empl){
        
        $error = '';
        $array = $array_rec_1 = $array_rec_2 = $array_rec_3 = null;
        
        if (!empty($co_empl)){
            
            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });
            
            try
            {
                $Sql = " PRDBPROCESO.._solcheque_pendientes_aprobar $co_empl; ";
                $conn = odbc_connect("SQLNSIP-P", "pica", "");
                $resultset = odbc_exec($conn,$Sql);
                
                do {

                    while($row = odbc_fetch_array($resultset)){

                        if(isset($row['resultset'])){

                            switch ($row['resultset']) {

                                case "STATUS":

                                    $array_rec_1 = array(
                                        'co_err'=> (int)$row['co_error'],
                                        'tx_err'=> trim($row['tx_error']),
                                    );

                                    break;
                                case "EMPRESAS":

                                    $array_rec_2[] = array(
                                        'co_emp'=> trim($row['codi_empr']),
                                        'no_emp'=> trim($row['nomb_empr']),
                                        'cant'=> (int)$row['cantidad']
                                    );

                                    break;
                                case "REEMPLAZO":

                                    $array_rec_3[] = array(
                                        'no_remp'=>trim($row['nomb_empl'])
                                    );

                                    break;
                            }

                        }

                    }
                } while(odbc_next_result($resultset));
            }
            catch (Exception $e)
            {
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR SOLICITUDES <BR> COD_ERR:",$e, 'error_db');
                die;
            }
            
            if ((int)$array_rec_1['co_err'] == 0){
                
                $array = array('empresas'=>$array_rec_2,'delegado'=>$array_rec_3);

            } else {
                
                $array = array('error'=>utf8_encode(trim($array_rec_1['tx_err'])));
                
            }
            
        }
        
        return $array;
        
    }
    
    function retSolicitudes($cod_emp){
        
        $error = '';
        $array = $array_rec_1 = null;
            
        if (!empty($cod_emp)){
            
            $Sql = " PRDBPROCESO.._solcheque_pendientes_aprobar ".(int)$_SESSION['c_e'].",1,'".trim($cod_emp)."';";
            $DB1 = $this->load->database('sqlnsip', true);
            $result = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error = "ERROR AL ACTUALIZAR SOLICITUD: ".$DB1->_error_number()." ".$DB1->_error_message();
            }

            $res = $result->result_array();
            
            if(!empty($res)){
                foreach ($res as $row){
                    $array_rec_1[] = array(
                        'cod_res'=>trim($row['responsable']),
                        'cod_gru'=>trim($row['grup_empr']),
                        'nom_emp'=>trim($row['nomb_empr']),
                        'tip_doc'=>trim($row['tipo_docu']),
                        'nom_doc'=>utf8_encode(trim($row['nomb_docu'])),
                        'num_doc'=>trim($row['nume_docu']),
                        'num_sol'=>trim($row['nume_soli']),
                        'status'=>trim($row['status']),
                        'valor'=>trim($row['valor']),
                        'val_var'=>trim($row['valo_vari']),
                        'val_iva'=>trim($row['valo_iva']),
                        'fecha'=>trim($row['fecha']),
                        'cod_pro'=>utf8_encode(trim($row['proveedor'])),
                        'nom_emp'=>utf8_encode(trim($row['nomb_empl'])),
                        'concept'=>utf8_encode(trim($row['concepto'])),
                        'nom_cec'=>utf8_encode(trim($row['nomb_ceco'])),
                        'cod_cec'=>trim($row['codi_ceco'])
                    );
                }
            }
            
            $array = array('solic'=>$array_rec_1,'err'=>$error);
                
        }
        
        return $array;
        
    }
    
    
}
?>
