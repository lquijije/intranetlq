<?php

class m_1101 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function delegaUsuario($cod_usu,$cod_del){
        
        $error = '';
        $array = null;
              
        if(!empty($cod_usu)){
            
            $Sql = "PRDBPROCESO.._sc_delegacion_update ".(int)$cod_usu.",".(int)$cod_del.";";
            $DB1 = $this->load->database('sqlnsip', true);
            $result = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error = "ERROR AL ACTUALIZAR DELEGADO: ".$DB1->_error_number()." ".$DB1->_error_message();
            } else {
                $raw = $result->row_array();

                if(count($raw)>0){
                    $array = array(
                        'co_err'=>(int)$raw['co_error'],
                        'tx_err'=>trim(utf8_decode($raw['tx_error']))
                    );
                }
            }
        }
            
        return array('data'=>$array,'err'=>$error);
            
    }
    
    function retDelegados($cod_emp){
        
        $error = '';
        $array = null;
            
        if (!empty($cod_emp)){
            
            $Sql  = " SELECT DISTINCT R.codi_remp, E.nomb_empl, R.remp_stat ";
            $Sql .= " FROM PRDBPROCESO..CHTBGRPREMPL R, PRDBPROCESO..chtbempleado E ";
            $Sql .= " WHERE R.CODI_remp = E.codi_empl ";
            $Sql .= " AND E.bd_empl = 'A'";
            $Sql .= " AND R.codi_empl = '".trim($cod_emp)."' ";
            $Sql .= " ORDER BY R.codi_remp;";
            
            $DB1 = $this->load->database('sqlnsip', true);
            $result = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER DELEGADOS: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            }  

            $res = $result->result_array();
            
            if(!empty($res)){
                foreach ($res as $row){
                    $array[] = array(
                        'co_rem'=>trim($row['codi_remp']),
                        'no_rem'=>trim(utf8_encode($row['nomb_empl'])),
                        'es_rem'=>trim($row['remp_stat'])
                    );
                }
            }
                
        }
        
        return $array;
        
    }
    
    
}
?>
