<?php
ini_set('max_execution_time', 300);

class m_109 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function valiOrdCom(){
        
        $Sql  = " SELECT veces=count(1) ";
        $Sql .= " FROM PRDBPROCESO..CHTBSOLIORDCOM ";
        $Sql .= " WHERE co_usuario= ".$_SESSION['c_e']." ";
        $DB1 = $this->load->database('sqlnsip-clon', true);
        $resultado = $DB1->query($Sql);
        if ($DB1->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL VALIDAR ORDEN DE COMPRA: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
            die;
        }            
        $row = $resultado->row_array(); 
        return (int)$row['veces'];
            
    }
    
    function retOrdCom($data){
        if(!empty($data)){
            
            $Sql = " PRDBPROCESO..get_ordcomp_header '".$data['c_e']."',".$data['c_n_p'].",".$data['n_o']." ";
            $DB1 = $this->load->database('sqlnsip-clon', true);
            $resultado = $DB1->query($Sql);
            if ($DB1->_error_message() <> ''){
                return "ERROR AL RETORNAR ORDEN DE COMPRA: ".$DB1->_error_number()." ".$DB1->_error_message();
                die();
            } 
            $array = $array_1 =null;
            
            foreach ($resultado->result_array() as $row) {
                
                $Sql = " PRDBPROCESO..IvaPlanImpuesto '".$data['c_e']."','".utf8_encode(trim($row['planimpu']))."' ";
                $DB1 = $this->load->database('sqlnsip-clon', true);
                $resultado = $DB1->query($Sql);     
                if ($DB1->_error_message() <> ''){
                    return "ERROR AL RETORNAR PLAN DE IMPUESTO: ".$DB1->_error_number()." ".$DB1->_error_message();
                    die();
                } 
                $raw = $resultado->row_array(); 
                
                $Sql = " PRDBPROCESO.._solcheque_get_proveedor '".trim($row['nume_iruc'])."','','".$_SESSION['sv_emp']."','".$_SESSION['ba_emp']."' ";
                $DB1 = $this->load->database('sqlnsip-clon', true);
                $resultado = $DB1->query($Sql);     
                if ($DB1->_error_message() <> ''){
                    return "ERROR AL RETORNAR PROVEEDOR: ".$DB1->_error_number()." ".$DB1->_error_message()."-".$Sql;
                    die();
                } 
                $rew = $resultado->row_array();
                
                $array[] = array(
                    'a'=> utf8_encode(trim($row['nombre_ocompra'])),      
                    'b'=> trim($row['nume_iruc']),      
                    'c'=> utf8_encode(trim($row['nombre'])), 
                    'd'=> utf8_encode(trim($rew['direccion'])), 
                    'e'=> utf8_encode(trim($row['desc_come'])),      
                    'f'=> trim($row['monto']),           
                    'g'=> utf8_encode(trim($row['beneficiario'])),      
                    'h'=> utf8_encode(trim($row['terminos'])),      
                    'i'=> utf8_encode(trim($row['planimpu'])),      
                    'j'=> (int)trim($raw['impuesto']),      
                    'k'=> trim($row['dias']),      
                    'l'=> trim($row['clave'])
                );
                
                $Sql = " PRDBPROCESO..ImpuestosAplicablesProveedor '".$data['c_e']."','".utf8_encode(trim($row['planimpu']))."' ";
                //echo $Sql;
                $DB2 = $this->load->database('sqlnsip-clon', true);
                $resultado_1 = $DB2->query($Sql);     
                if ($DB2->_error_message() <> ''){
                    return "ERROR AL VALIDAR IMPUESTOS APLICABLES PROVEEDOR: ".$DB2->_error_number()." ".$DB2->_error_message();
                    die();
                } 

                foreach ($resultado_1->result_array() as $row_1) {
                    $array_1[] = array(
                        'a'=> utf8_encode(trim($row_1['tx_descripcion'])),      
                        'b'=> trim($row_1['tx_planimpuesto'])
                    ); 
                }
                
            }
                
            return "OK-".json_encode(array('ord_com'=>$array,'pla_imp'=>$array_1));
            
        }
    }
    
    function retProveedor($data){
        if(!empty($data)){
            
            $Sql = " PRDBPROCESO.._solcheque_get_proveedor '".$data['ruc']."','".$data['name']."','".$_SESSION['sv_emp']."','".$_SESSION['ba_emp']."' ";
            //echo $Sql;
            $DB1 = $this->load->database('sqlnsip-clon', true);
            $resultado = $DB1->query($Sql);
            
            if ($DB1->_error_message() <> ''){
                return "ERROR AL RETORNAR PROVEEDOR: ".$DB1->_error_number()." ".$DB1->_error_message();
                die();
            } 
            
            if (count($resultado->result_array())> 0){
                $array = null;
                
                foreach ($resultado->result_array() as $row) {

                    $array[] = array(
                        'ruc_prov'=> trim($row['id_prov']),
                        'nom_prov'=> trim(utf8_encode($row['NOMBRE'])),
                        'dir_prov'=> trim(utf8_encode($row['direccion'])),
                        'bene_prov'=> trim(utf8_encode($row['beneficiario'])),
                        'term_prov'=> trim(utf8_encode($row['terminos'])),
                        'plan_imp'=> trim(utf8_encode($row['planimpu'])),
                        'dias_prov'=> trim($row['DIAS']),
                        'cla_prov'=> trim($row['clave']),
                        'mail_prov'=> trim($row['correo']),
                        'tip_prov'=> trim($row['tipo_proveedor']));
                }
                
                return "OK-".json_encode($array);
            }
        }
    }
    
    function retDocPen($data){
        if(!empty($data)){
            
            $Sql = " PRDBPROCESO.._electronicos_pendientes '".$data['co_e']."','".$data['ruc']."',1 ";
            //echo $Sql;
//            die;
            $DB1 = $this->load->database('sqlnsip-clon', true);
            $resultado = $DB1->query($Sql);
            if ($DB1->_error_message() <> ''){
                return "ERROR AL RETORNAR DOCUMENTOS PENDIENTES: ".$DB1->_error_number()." ".$DB1->_error_message();
                die();
            } 
            
            if (count($resultado->result_array())> 0){
                $array = null;
                
                foreach ($resultado->result_array() as $row) {
                    
                    $array[] = array(
                        
                        'a'=> str_pad(trim($row['establecimiento']),3,0, STR_PAD_LEFT),
                        'b'=> str_pad(trim($row['puntoEmision']),3,0, STR_PAD_LEFT),
                        'c'=> str_pad(trim($row['secuencial']),9,0, STR_PAD_LEFT),
                        'd'=> trim(utf8_encode($row['estado'])),
                        'e'=> trim($row['numeroAutorizacion']),
                        'f'=> date('Y/m/d',strtotime(trim($row['fechaAutorizacion']))),
                        'g'=> trim($row['claveAcceso']),
                        'h'=> date('Y/m/d',strtotime(trim($row['fechaEmision']))),
                        'i'=> trim($row['mail_from']),
                        'j'=> date('Y/m/d',strtotime(trim($row['mail_date']))),
                        'k'=> (float)trim($row['base']),
                        'l'=> (float)trim($row['impuesto']),
                        'm'=> (float)trim($row['total']));
                }
                return "OK-".json_encode($array);
            }
        }
    }
    
    function retDoc($data){
        if(!empty($data)){
            
            $Sql = " PRDBPROCESO.._electronicos_pendientes '".$data['co_e']."','".$data['ruc']."',1,".$data['e_o'].",".$data['p_n'].",".$data['s_l']." ";
            //echo $Sql;
            $conn=odbc_connect("SQLNSIP-D", "pica", "");
            $resultset=odbc_exec($conn,$Sql);
            if (odbc_error()){
                RETURN "ERROR: ERROR AL RETORNAR".odbc_errormsg($conn);
                die;
            }
            $array = $array_rec_1 = $array_rec_2 = $array_rec_3 = null;
            
            while( $row = odbc_fetch_array($resultset) ) {
                $array_rec_1[] = array(
                    'a'=>$row['estado'],
                    'b'=>$row['numeroAutorizacion'],
                    'c'=>$row['fechaAutorizacion'],
                    'd'=>$row['claveAcceso'],
                    'e'=>$row['fechaEmision'],
                    'f'=>$row['mail_from'],
                    'g'=>$row['mail_date'],
                    'h'=>$row['propina'],
                    'i'=>$row['mail_attachment']
                );
            } 
            
            odbc_next_result($resultset);
            
            while( $row = odbc_fetch_array($resultset) ) {
                $array_rec_2[] = array(
                    'a'=>$row['indice'],
                    'b'=>$row['codigoPrincipal'],
                    'c'=>$row['codigoAuxiliar'],
                    'd'=>utf8_encode($row['descripcion']),
                    'e'=>$row['cantidad'],
                    'f'=>$row['precioUnitario'],
                    'g'=>$row['descuento'],
                    'h'=>$row['totalSinImpuesto']
                );
            }
            
            odbc_next_result($resultset);
            
            while( $row = odbc_fetch_array($resultset) ) {
                $array_rec_3[] = array(
                    'a'=>$row['codigo'],
                    'b'=>$row['codigoPorcentaje'],
                    'c'=>$row['tarifa'],
                    'd'=>utf8_encode($row['impto1']),
                    'e'=>utf8_encode($row['impto2']),
                    'f'=>$row['baseImponible'],
                    'g'=>$row['valor']
                );
            }
            
            return $array = array('cab'=>$array_rec_1,'det'=>$array_rec_2,'imp'=>$array_rec_3);
            
        }
    }
    
    function retTipDocu(){
        
        $Sql = " PRDBPROCESO.._solcheque_get_tipo_documentos ";
        $DB1 = $this->load->database('sqlnsip-clon', true);
        $resultado = $DB1->query($Sql);
        if ($DB1->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR TIPO DE DOCUMENTOS: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
            die;
        }     

        if (count($resultado->result_array())> 0){
            $array = null;

            foreach ($resultado->result_array() as $row) {

                $array[] = array(
                    'cod_doc'=> trim($row['codi_docu']),
                    'nom_doc'=> trim(utf8_encode(ucwords(strtolower($row['nomb_docu'])))));
            }

            return $array;
        }
        
    }
    
    function retOpc(){
        
        $Sql  = " SELECT tipo FROM tb_intranet_opciones_form ";
        $Sql .= " WHERE cod_empl = ".$_SESSION['c_e'];
        $Sql .= " AND controlador = 'co_109' ";
        
        $DB1 = $this->load->database('sqlnsip-clon', true);
        $resultado = $DB1->query($Sql);
        if ($DB1->_error_message() <> ''){
            return "ERROR AL RETORNAR OPCIONES: ".$DB1->_error_number()." ".$DB1->_error_message();
            die();
        } 

        if (count($resultado->result_array())> 0){
            $array = null;

            foreach ($resultado->result_array() as $row) {
                $array[] = array('tipo'=>trim($row['tipo']));
            }

            return $array;
        }
        
    }
    
    function retTipEgre(){
        
        $Sql = " PRDBPROCESO.._solcheque_retTipo_Egre '".$_SESSION['co_emp']."','".$_SESSION['c_r']."' ";
        $DB1 = $this->load->database('sqlnsip-clon', true);
        $resultado = $DB1->query($Sql);
        
        if ($DB1->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR TIPO DE EGRESOS: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
            die;
        }   
        
        $array = null;

        if (count($resultado->result_array())> 0){

            foreach ($resultado->result_array() as $row) {

                $array[] = array(
                    'cod_mvt'=> trim($row['codi_mvto']),
                    'nom_mvt'=> trim(utf8_encode(ucwords(strtolower($row['desc_mvto'])))));
            }

            return $array;
        }
        
    }
    
    function retAutorizadores(){
        
        if (!empty($_SESSION['co_emp'])){
           
            $Sql = " PRDBPROCESO.._solcheque_aprobadores '".$_SESSION['co_emp']."','100','".$_SESSION['c_r']."','".$_SESSION['sv_emp']."','','' ";
//            echo $Sql;
//            die;
            $DB1 = $this->load->database('sqlnsip-clon', true);
            $resultado = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR AUTORIZADORES: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            }  
            
            if (count($resultado->result_array())> 0){
                $array = null;

                foreach ($resultado->result_array() as $row) {
                    $array[] = array(
                        'cod_gru'=> trim($row['codi_grup']),
                        'sec_usu'=> trim($row['secu_usua']),
                        'cod_emp'=> trim($row['codi_empl']),
                        'nom_emp'=> trim(utf8_encode(ucwords(strtolower($row['nomb_empl'])))));
                }

                return $array;
            } 
            
        }
        
    }
    
    function saveSolicitud($data,$all_cuentas){
        if (isset($_SESSION) && !empty($_SESSION)){
            if(!empty($data)){
                
                $string = file_get_contents("temp/json/".$_SESSION['c_r']."-39.json");
                $json_array = json_decode($string, true);
                $empresas = isset($json_array['master']['39']['empresas'])?$json_array['master']['39']['empresas']:array();
                
                $cuenta_varios = 0;
                $cuenta_prove = 0;
                $cuenta_prove_ext = 0;
                $cuenta_antic = 0;
                
                if (isset($empresas)){

                    for ($i = 0; $i < count($empresas); $i++){
                        if($i === (int)$_SESSION['id_emp']) {
                            $cuenta_varios = $empresas[$i]['VARIOS'];
                            $cuenta_prove = $empresas[$i]['SOLIC'];
                            $cuenta_prove_ext = $empresas[$i]['SOLIC_EXT'];
                            $cuenta_antic = $empresas[$i]['ANTICIPO'];
                        }
                    }

                }
                
                $dom = new DOMDocument("1.0");
                $dom->preserveWhiteSpace = false; 
                $dom->formatOutput = true; 
                
                ##<cuentas>
                    
                    $array_cuentas = json_decode($data['c_s'],true);
                    
                    $cuentas = $dom->createElement('cuentas');
                    $dom->appendChild($cuentas);
                    $count = 0;
                    $pri_cuent=$pri_ccost=null;
                    
                    for ($i = 0; $i < count($array_cuentas); $i++){
                        
                        $items = $dom->createElement("items"); 
                        $cuentas->appendChild($items);
                        
                        foreach ($all_cuentas as $row){
                            if (trim($row['cta_myor'])."-".trim($row['codi_ceco'])."-".trim($row['cta_scta']) === trim($array_cuentas[$i]['id'])){
                                if ($count === 0){
                                    $pri_cuent = trim($row['actindx']);
                                    $pri_ccost = trim($row['codi_ceco']);
                                }
                                $count++;
                                $n_16 = $dom->createElement("id",trim($row['actindx'])); 
                                $items->appendChild($n_16);
                            }
                        }
                        
                        $n_17 = $dom->createElement("tip",trim($array_cuentas[$i]['tip'])); 
                        $items->appendChild($n_17);
                        
                        $n_18 = $dom->createElement("val",trim($array_cuentas[$i]['val'])); 
                        $items->appendChild($n_18);
                        
                    }
                    
                ##</cuentas>
                
                ##<plandepago>
                    
                    $array_plan_pg = json_decode($data['p_g'],true);
                    
                    $plan_pago = $dom->createElement('plandepago');
                    $dom->appendChild($plan_pago);
                    $pri_fec = null;
                    $count = 0;
                    for ($i = 0; $i < count($array_plan_pg); $i++){
                        if($count === 0){
                           $pri_fec =trim($array_plan_pg[$i]['fec']);
                        }
                        $count++;
                        $items = $dom->createElement("items"); 
                        $plan_pago->appendChild($items);
                        
                        $n_19_1 = $dom->createElement("id",$count); 
                        $items->appendChild($n_19_1);
                        
                        $n_19 = $dom->createElement("fecha",trim($array_plan_pg[$i]['fec'])); 
                        $items->appendChild($n_19);
                        
                        $n_20 = $dom->createElement("porce",trim($array_plan_pg[$i]['por'])); 
                        $items->appendChild($n_20);
                        
                    }
                    
                ##</plandepago>
                    
                ##<cabecera>
                
                    $array_cab = json_decode($data['c_a'],true);
                    $cabecera = $dom->createElement('cabecera');
                    $dom->appendChild($cabecera);

                    $n_1 = $dom->createElement("ruc",trim($array_cab[0]['ruc'])); 
                    $cabecera->appendChild($n_1);
                    
                    $n_36 = $dom->createElement("tip_prov",trim($array_cab[0]['tip_prov'])); 
                    $cabecera->appendChild($n_36);

                    $n_28 = $dom->createElement("nom_prov",utf8_encode(trim($array_cab[0]['nom_prov']))); 
                    $cabecera->appendChild($n_28);

                    $n_26 = $dom->createElement("dir_prov",utf8_encode(trim($array_cab[0]['dir_prov']))); 
                    $cabecera->appendChild($n_26);

                    $n_27 = $dom->createElement("nom_bene",utf8_encode(trim($array_cab[0]['nom_bene']))); 
                    $cabecera->appendChild($n_27);

                    $n_2 = $dom->createElement("tip_doc",trim($array_cab[0]['tip_doc'])); 
                    $cabecera->appendChild($n_2);

                    $n_3 = $dom->createElement("num_doc",trim($array_cab[0]['num_doc'])); 
                    $cabecera->appendChild($n_3);

                    $n_4 = $dom->createElement("fec_doc",trim($array_cab[0]['fec_doc'])); 
                    $cabecera->appendChild($n_4);

                    $n_5 = $dom->createElement("con_doc",utf8_encode(trim($array_cab[0]['con_doc']))); 
                    $cabecera->appendChild($n_5);

                    $n_6 = $dom->createElement("obs_doc",utf8_encode(trim($array_cab[0]['obs_doc']))); 
                    $cabecera->appendChild($n_6);

                    $n_29 = $dom->createElement("pla_imp",utf8_encode(trim($array_cab[0]['pla_imp']))); 
                    $cabecera->appendChild($n_29);

                    $n_21 = $dom->createElement("cod_gru_aut",utf8_encode(trim($array_cab[0]['cod_gru_aut']))); 
                    $cabecera->appendChild($n_21);

                    $n_22 = $dom->createElement("cuent_vari",trim($cuenta_varios)); 
                    $cabecera->appendChild($n_22);

                    $n_23 = $dom->createElement("cuent_pro",trim($cuenta_prove)); 
                    $cabecera->appendChild($n_23);

                    $n_24 = $dom->createElement("cuent_pro_ext",trim($cuenta_prove_ext)); 
                    $cabecera->appendChild($n_24);

                    $n_25 = $dom->createElement("cuent_antic",trim($cuenta_antic)); 
                    $cabecera->appendChild($n_25);

                    $n_30 = $dom->createElement("tip_egr",trim($array_cab[0]['tip_egr'])); 
                    $cabecera->appendChild($n_30);

                    $n_31 = $dom->createElement("cod_urg",trim($array_cab[0]['cod_urg'])); 
                    $cabecera->appendChild($n_31);

                    $n_32 = $dom->createElement("cod_cop",trim($array_cab[0]['cod_cop'])); 
                    $cabecera->appendChild($n_32);
                    
                    $n_33 = $dom->createElement("pri_cuent",trim($pri_cuent)); 
                    $cabecera->appendChild($n_33);
                    
                    $n_34 = $dom->createElement("pri_ccost",trim($pri_ccost)); 
                    $cabecera->appendChild($n_34);
                    
                    $n_35 = $dom->createElement("pri_fec",trim($pri_fec)); 
                    $cabecera->appendChild($n_35);
                    
                    $n_37 = $dom->createElement("num_ord_comp",trim($array_cab[0]['num_ord_comp'])); 
                    $cabecera->appendChild($n_37);
                    
                    $n_38 = $dom->createElement("aut_sri",trim($array_cab[0]['aut_sri'])); 
                    $cabecera->appendChild($n_38);
                    
                ##</cabecera>
                    
                ##<valores>
                
                    $valores = $dom->createElement('valores');
                    $dom->appendChild($valores);
                
                    $n_7 = $dom->createElement("sub_sol",trim($array_cab[0]['sub_sol'])); 
                    $valores->appendChild($n_7);

                    $n_8 = $dom->createElement("adic_sol",trim($array_cab[0]['adic_sol'])); 
                    $valores->appendChild($n_8);

                    $n_9 = $dom->createElement("porc_desc_sol",trim($array_cab[0]['porc_desc_sol'])); 
                    $valores->appendChild($n_9);
                                    
                    $n_10 = $dom->createElement("val_desc_sol",trim($array_cab[0]['val_desc_sol'])); 
                    $valores->appendChild($n_10);
                    
                    $n_11 = $dom->createElement("base_imp_sol",trim($array_cab[0]['base_imp_sol'])); 
                    $valores->appendChild($n_11);
                    
                    $n_12 = $dom->createElement("vari_sol",trim($array_cab[0]['vari_sol'])); 
                    $valores->appendChild($n_12);
                    
                    $n_13 = $dom->createElement("imp_sol",trim($array_cab[0]['imp_sol'])); 
                    $valores->appendChild($n_13);
                    
                    $n_14 = $dom->createElement("val_imp_sol",trim($array_cab[0]['val_imp_sol'])); 
                    $valores->appendChild($n_14);
                    
                    $n_15 = $dom->createElement("tot_sol",trim($array_cab[0]['tot_sol'])); 
                    $valores->appendChild($n_15);
                
                ##</valores>
                    
                $xml = $dom->saveXML();
                
                $Sql = " PRDBPROCESO.._solcheque_ingreso_solicitud '".$_SESSION['c_e']."','".$_SESSION['c_r']."','".$_SESSION['co_emp']."','".$_SESSION['ba_emp']."','".$_SESSION['sv_emp']."','".$_SESSION['ind_emp']."','".$xml."'; ";
                
                ECHO $Sql;
                DIE;
                $DB2 = $this->load->database('sqlnsip-clon', true);
                $resultado_1 = $DB2->query($Sql);  
                if ($DB2->_error_message() <> ''){
                    return "ERROR AL GRABAR SOLICITUD: ".$DB2->_error_number()." ".$DB2->_error_message();
                    die();
                } 
                $arr_error = null;
                foreach ($resultado_1->result_array() as $row_1) {
                    $arr_error[] = array(   
                        'a'=> (int)$row_1['co_error'],
                        'b'=> utf8_encode(trim($row_1['tx_error'])),
                        'c'=> ISSET($row_1['pri_autori'])?utf8_encode(trim($row_1['pri_autori'])):''
                    ); 
                }

                return $arr_error;
            }
        }
    }
    
    function retPlanImp($plan){
        
        if (!empty($plan)){
            
            $array_1 = null;
            
            $Sql = " PRDBPROCESO..ImpuestosAplicablesProveedor '".$_SESSION['co_emp']."','".utf8_encode(trim($plan))."' ";
            $DB2 = $this->load->database('sqlnsip-clon', true);
            $resultado_1 = $DB2->query($Sql);     
            if ($DB2->_error_message() <> ''){
                return "ERROR AL RETORNAR IMPUESTOS APLICABLES: ".$DB2->_error_number()." ".$DB2->_error_message();
                die();
            } 
            foreach ($resultado_1->result_array() as $row_1) {
                $array_1[] = array(
                    'a'=> utf8_encode(trim($row_1['tx_descripcion'])),      
                    'b'=> trim($row_1['tx_planimpuesto'])
                ); 
            }
            
            return "OK-".json_encode($array_1);
            
        }
        
    }
    
    function retDistribucionxm2($c_my,$s_cm){
        
        if (!empty($c_my) && !empty($s_cm)){
            
            $array_1 = null;
            
            $Sql = " PRDBPROCESO.._solcheque_distri_x_m2 ".$c_my.",".$s_cm.", ".$_SESSION['c_r'];
            $DB2 = $this->load->database('sqlnsip-clon', true);
            $resultado_1 = $DB2->query($Sql);     
            if ($DB2->_error_message() <> ''){
                return "ERROR AL RETORNAR DISTRIBUCION X M2: ".$DB2->_error_number()." ".$DB2->_error_message();
                die();
            } 
            
            foreach ($resultado_1->result_array() as $row_1) {
                $array_1[] = array(
                    'a'=> trim($row_1['actindx']),
                    'b'=> trim($row_1['actnumbr_1']),
                    'c'=> trim($row_1['actnumbr_2']),
                    'd'=> trim($row_1['actnumbr_3']),
                    'e'=> trim($row_1['mts_ccos']),
                    'f'=> utf8_encode(trim($row_1['grup_ccos'])),
                    'g'=> utf8_encode(trim($row_1['NOMBRE'])),
                    'h'=> utf8_encode(trim($row_1['ACTDESCR']))
                ); 
            }
            
            if (!empty($array_1)){
                return "OK-".json_encode($array_1);
            }
            
        }
        
    }
    
    function retDistribucioncc($c_my,$s_cm){
        
        if (!empty($c_my) && !empty($s_cm)){
            
            $array_1 = null;
            
            $Sql = " PRDBPROCESO.._solcheque_distri_x_ce_cos ".$c_my.",".$s_cm.", ".$_SESSION['c_r'];
            $DB2 = $this->load->database('sqlnsip-clon', true);
            $resultado_1 = $DB2->query($Sql);     
            if ($DB2->_error_message() <> ''){
                return "ERROR AL RETORNAR DISTRIBUCION X CC: ".$DB2->_error_number()." ".$DB2->_error_message();
                die();
            } 
            
            foreach ($resultado_1->result_array() as $row_1) {
                $array_1[] = array(
                    'a'=> trim($row_1['actindx']),
                    'b'=> trim($row_1['actnumbr_1']),
                    'c'=> trim($row_1['actnumbr_2']),
                    'd'=> trim($row_1['actnumbr_3']),
                    'e'=> utf8_encode(trim($row_1['NOMBRE'])),
                    'f'=> utf8_encode(trim($row_1['ACTDESCR']))
                ); 
            }
            
            if (!empty($array_1)){
                return "OK-".json_encode($array_1);
            }
            
        }
        
    }
    
    function retAccSolCodEmp(){
        
        if (isset($_SESSION['c_e']) && !empty($_SESSION['c_e'])){
            
            $Sql = " SELECT sin_apro ";
            $Sql .= " FROM _solcheque_accesos ";
            $Sql .= " WHERE cod_empl = ".$_SESSION['c_e']." ";
            $Sql .= " AND sin_apro = 1 ";
            $DB2 = $this->load->database('sqlnsip-clon', true);
            $resultado_1 = $DB2->query($Sql);    
            
            if ($DB2->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR ACCESOS SOLICITUD: ".$DB2->_error_number(),$DB2->_error_message(), 'error_db');
                die;
            }            
            
            $row = $resultado_1->row_array();
            
            if(!empty($row['sin_apro']) && (int)$row['sin_apro'] == 1){
                return true;
            } else {
                return false;
            }
            
        }
        
    }
    
}

?>

