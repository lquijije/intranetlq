<?php

class m_117 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retMarcaciones($fec_1,$fec_2){
        
        $array = null;
        
        if (!empty($_SESSION['c_e'])){
            
            $Sql = " OBTIENE_MARCACIONES_G 0,'".trim($_SESSION['c_e'])."','".trim($_SESSION['co_empresa'])."','".$fec_1." 00:00','".$fec_2." 23:59' ";
            //echo $Sql;
            $DB1 = $this->load->database('nomina', true);
            $resultado = $DB1->query($Sql);
            
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR MARCACIONES <BR> COD_ERR:".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            }
            
            if (count($resultado->result_array())> 0){
                foreach ($resultado->result_array() as $row) {
                    $array[] = array(    
                        'fec_mar'=> date('Y/m/d',strtotime(trim($row['FECHA']))),       
                        'dia_mar'=> trim($row['DIA_SEMANA']), 
                        'ing_mar'=> trim($row['MARCACION_INGRESO']),
                        'hor_ent'=> trim($row['HORA_ENTRADA']),
                        'sal_mar'=> trim($row['MARCACION_SALIDA']),
                        'tie_lab'=> trim($row['TIEMPL_LAB']),    
                        'tip_reg'=> trim($row['TIPO_REGISTRO']),      
                        'equipo'=> trim($row['EQUIPO'])
                    );
                }
            }
            
        }
        
        return $array;
    }   
    
    function retHorario(){
        
        $array = null;
        
        if (!empty($_SESSION['c_e'])){
            
            $Sql = " _marcacion_horario_empleado ".trim($_SESSION['c_e']);
            $DB1 = $this->load->database('nomina', true);
            $resultado = $DB1->query($Sql);
            
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR HORARIO <BR> COD_ERR:".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            }
            
            if (count($resultado->result_array())> 0){
                foreach ($resultado->result_array() as $row) {
                    $array[] = array(    
                        'dia'=> trim($row['DIA']), 
                        'ent'=> trim($row['HORA_ENTRADA']),
                        'sal'=> trim($row['HORA_SALIDA'])
                    );
                }
            }
            
        }
        
        return $array;
    }   
    
}

?>

