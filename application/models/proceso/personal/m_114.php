<?php

class m_114 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function valUser($Usuario){ 
        $Sql  = " SELECT clave ";
        $Sql .= " FROM tb_intranet_users ";
        $Sql .= " WHERE cod_empleado = '$Usuario' ";
        $Sql .= " AND estado = 1 ";
//        echo $Sql;
//        die;
        $query = $this->db->query($Sql); 
        $array = null;
        
        if ($this->db->_error_message() <> ''){
            $array = array('co_error'=>"ERROR AL VALIDAR USUARIO: ".$this->db->_error_number()." - ".$this->db->_error_message(),'data'=>'');
        } else {
            $array = array('co_error'=>'','data'=>$query->row_array());
        }
        
        return $array;
        
    }
    
    function retBono(){
        
        @ini_set('display_errors', 0);
        error_reporting(0);
        
        $array_1 = $array_2 = $array_3 = null;
        
        if (!empty($_SESSION['c_e'])){
            
            $bandera = false;
            $error = '';
            $mes = date('m');
            $mes = $mes - 1;
            $Sql = " scdbscorecard..sccConsultaBono ".trim($_SESSION['c_e']).",".date('Y').",".$mes;
            
            $conn = odbc_connect("NOMINA-P", "pica","");
            $resultset = odbc_exec($conn,$Sql);
            
            if (odbc_error()){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL VALIDAR BONO <BR> COD_ERR:",odbc_errormsg($conn), 'error_db');
                die;
            }
            
            while($row = odbc_fetch_array($resultset)) {
                $error = (int)$row['co_error'];
                if (isset($row['co_error']) && (int)$row['co_error'] === 0){
                    $bandera = true;
                }
            }
            
            if ($bandera){
               
                while(odbc_next_result($resultset)){
                    
                    $ban = odbc_num_fields($resultset);
                    
                    if($ban === 11){
                        
                        while($raw = odbc_fetch_array($resultset)) {
                            
                            $array_1[] = array(    
                                'des_car'=> utf8_encode(trim($raw['ds_cargo'])), 
                                'val_por'=> trim($raw['va_porcentajemes']),
                                'tip_bon'=> trim($raw['co_tipobono']),
                                'des_des'=> utf8_encode(trim($raw['ds_descripcion'])),  
                                'fec_emp'=> date('Y/m/d',strtotime(trim($raw['fe_empleo']))),       
                                'dis_n_lab'=> trim($raw['va_diasnolaborados']),  
                                'txt_com'=> utf8_encode(trim($raw['tx_comentario'])),      
                                'bon_anu'=> trim($raw['va_bonoanual']),
                                'bon_obj'=> trim($raw['va_bonoobjetivo'])
                            );

                        }

                    } else if($ban === 12){
                        
                        while($row = odbc_fetch_array($resultset)) {
                            
                            $array_2[] = array(    
                                'cod_ind'=> trim($row['co_indicador']),
                                'txt_ind'=> utf8_encode(trim($row['tx_indicador'])),  
                                'val_min'=> trim($row['va_cumplimientominimo']),
                                'val_max'=> trim($row['va_cumplimientomaximo']),
                                'val_red'=> trim($row['va_rendimiento']),
                                'val_dis'=> trim($row['va_bonodistribuido']),
                                'val_cal'=> trim($row['va_bonocalculado']),
                                'val_gan'=> trim($row['va_bonoganado']),
                                'bon_est'=> trim($row['co_estadopagobono']),
                                'txt_est'=> utf8_encode(trim($row['tx_estadopagobono'])),      
                                'cod_cos'=> trim($row['co_ccosto']),
                                'txt_cos'=> utf8_encode(trim($row['tx_ccosto']))
                            );

                        }
                    } else if($ban === 7){
                        
                        while($row = odbc_fetch_array($resultset)) {
                            
                            $array_3[] = array(    
                                'des_cco'=> utf8_encode(trim($row['ds_ccosto'])), 
                                'val_red'=> trim($row['va_rendimiento']), 
                                'dis_n_lab'=> trim($raw['va_diasnolaborados']),  
                                'val_dis'=> trim($row['va_bonodistribuido']),
                                'val_gan'=> trim($row['va_bonoganado']),
                                'val_cal'=> trim($row['va_bonocalculado']),     
                                'val_bon_pag'=> trim($row['va_bonopagar'])
                            );

                        }
                    }
                    
                }
                
                
            }
            
            return array('error'=>$error,'res_1'=>$array_1,'res_2'=>$array_2,'res_3'=>$array_3);
        }
    }
    
    function retlinePool(){
        
        $array = null;
        
        if (!empty($_SESSION['c_e'])){
            
            $Sql = " _get_pool_celular_info ".trim($_SESSION['c_e']) ;
            $resultado = $this->db->query($Sql);
            
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR POOL USUARIO <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            
            if (count($resultado->result_array())> 0){
                foreach ($resultado->result_array() as $row) {
                    $array[] = array(    
                        'tip_ope'=> utf8_encode(trim($row['operadora'])), 
                        'num_cel'=> trim($row['telefono']),
                        'nom_equ'=> utf8_encode(trim($row['equipo'])),  
                        'imei'=> trim($row['imei']),    
                        'ult_ren'=> date('Y/m/d',strtotime(trim($row['ultima_renovacion']))),       
                        'ede_tot'=> trim($row['adendum_total']),      
                        'ede_x_pag'=> trim($row['adendum_por_pagar']),      
                        'val_cob'=> trim($row['cobertura']) 
                    );
                }
            }
            
        }
        
        return $array;
    }
    
    function retFechRol($cod_emp,$cod_pro,$tip_rol){
        
        $array = null;
        
        if (!empty($_SESSION['c_e'])){
            
            $Sql = " _rol_fechas 14,".trim($cod_emp).",'".trim($cod_pro)."',".trim($tip_rol);
            //echo $Sql;
            $DB1 = $this->load->database('nomina', true);
            $query= $DB1->query($Sql);
            
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR FECHAS <BR> COD_ERR:".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            }
            
            $resultado = $query->result_array();
            
            if (count($resultado) > 0){
                foreach ($resultado as $row) {
                    $array[] = array(    
                        'dias'=> utf8_encode(trim($row['dias'])), 
                        'hora'=> date('h:i',strtotime(trim($row['HORA']))),       
                        'fec_rol'=> date('Y/m/d h:i',strtotime(trim($row['FECHA']))), 
                        'fec_rol_1'=> trim($row['FECHA_ROL']),       
                        'est_sul'=> utf8_encode(trim($row['estado_inst'])) 
                    );
                }
            }
            
        }
        
        return $array;
    }
    
    function retRolPago($cod_emp,$fecha){
        
        $array = null;
        
        if (!empty($_SESSION['c_e'])){
            
            $Sql = " REPORTE_PAGO_WEBPYCCA ".trim($cod_emp).",'".trim($fecha)."','B',".trim($_SESSION['c_e']);
//            echo $Sql;
//            die;
            $DB1 = $this->load->database('nomina', true);
            $query= $DB1->query($Sql);
            
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR ROL <BR> COD_ERR:".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            }
            
            $resultado = $query->result_array();
            
            if (count($resultado) > 0){
                foreach ($resultado as $row) {
                    $array[] = array(    
                        'cod_i'=> trim($row['COD_TRANS_I']), 
                        'ent_i'=> trim($row['ENTRADA_I']), 
                        'res_i'=> trim($row['RESULTADO_I']),
                        'cod_e'=> trim($row['COD_TRANS_E']), 
                        'ent_e'=> trim($row['ENTRADA_E']), 
                        'res_e'=> trim($row['RESULTADO_E']) 
                    );
                }
            }
            
        }
        
        return $array;
    }
    
    function retEmpre(){
        
        $array = null;
        
        if (!empty($_SESSION['c_e'])){
            
            $Sql = " _rol_empresas_usuario ".trim($_SESSION['c_e']);
            
            $DB1 = $this->load->database('nomina', true);
            $query= $DB1->query($Sql);
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR EMPRESA USUARIO <BR> COD_ERR:".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            }
            $resultado = $query->result_array();
            
            if (count($resultado) > 0){
                foreach ($resultado as $row) {
                    
                    $array[] = array(    
                        'fec_emp'=> date('Y/m/d h:i',strtotime(trim($row['fecha_empleo']))), 
                        'tip_rol'=> trim($row['tipo_rol']), 
                        'cod_emp'=> trim($row['COD_EMPRESA']),
                        'des_emp'=> trim($row['nomb_emp']),
                        'des_cco'=> trim($row['nomb_ccos'])
                    );
                }
            }
            
        }
        
        return $array;
    }
    
    function valiNomina(){
        
        $array = null;
        
        if (!empty($_SESSION['c_e'])){
            
            $Sql = " SELECT bd_rol, bd_rol_plasticos ";
            $Sql .= " FROM rol_webpycca ";
//            echo $Sql;
//            die;
            $DB1 = $this->load->database('nomina', true);
            $query= $DB1->query($Sql);
            
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL VALIDAR NOMINA: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            }
            
            $resultado = $query->result_array();
            
            if (count($resultado) > 0){
                foreach ($resultado as $row) {
                    
                    $array[] = array(    
                        'bd_pycca'=> (int)trim($row['bd_rol']),
                        'bd_plast'=> (int)trim($row['bd_rol_plasticos'])
                    );
                }
            }
            
        }
        
        return $array;
    }
    
}

?>

