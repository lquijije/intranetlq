<?php

class m_111 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retEmpresaAsignadas(){
        if(!empty($_SESSION['c_e'])){
            
            $Sql = " webpycca.._get_almacenes_usuario ".$_SESSION['c_e']." ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR EMPRESAS <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            
            $array = null;
            
            if (count($resultado->result_array())> 0){
                foreach ($resultado->result_array() as $row) {

                    $array[] = array(
                        'cod_bdg'=> utf8_encode(trim($row['co_bodega'])),      
                        'des_bdg'=> utf8_encode(trim($row['descripcion'])), 
                        'ruc_bdg'=> trim($row['ruc']),       
                        'de_sri'=> trim($row['de_sri']),       
                        'ip'=> trim($row['de_ip_address']),         
                        'dir_bdg'=> utf8_encode(trim($row['direccion'])),      
                        'tip_bdg'=> utf8_encode(trim($row['tipo']))
                    );

                }

                return $array;
            }
        }
    }
    
    function retCliente($data){
        if(!empty($data)){
            
            $Sql = " SGDBSEGURIDAD.._consulta_cliente '".$data."' ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR CLIENTE <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            $array = null;

            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row) {

                    $array[] = array(
                        'nom_cli'=> trim($row['perNombre1']),
                        'dir_cli'=> trim(utf8_encode($row['direccion'])),
                        'tel_cli'=> trim($row['telefono']),
                        'mail_cli'=> trim($row['correo'])
                    );
                }
                
            }
            
            return array('data'=>$array);
            
        }
    }
        
    function saveReclamo($json){
        
        if (!empty($_SESSION['c_e'])){
            
            if(!empty($json)){
                    
                $array_cab = json_decode($json['c_a'],true);
                
                $dom = new DOMDocument("1.0");
                $dom->preserveWhiteSpace = false; 
                $dom->formatOutput = true; 
                    
                ##<cabecera>

                    $cabecera = $dom->createElement('cabecera');
                    $dom->appendChild($cabecera);

                    $n_1 = $dom->createElement("id",trim($array_cab[0]['id'])); 
                    $cabecera->appendChild($n_1);
                    
                    $n_2 = $dom->createElement("nom_cli",utf8_decode(trim($array_cab[0]['nom_cli']))); 
                    $cabecera->appendChild($n_2);

                    $n_3 = $dom->createElement("dir_cli",utf8_decode(trim($array_cab[0]['dir_cli']))); 
                    $cabecera->appendChild($n_3);

                    $n_4 = $dom->createElement("conv_cli",trim($array_cab[0]['conv_cli'])); 
                    $cabecera->appendChild($n_4);

                    $n_5 = $dom->createElement("celu_cli",trim($array_cab[0]['celu_cli'])); 
                    $cabecera->appendChild($n_5);

                    $n_6 = $dom->createElement("mail_cli",utf8_decode(trim($array_cab[0]['mail_cli']))); 
                    $cabecera->appendChild($n_6);

                    $n_7 = $dom->createElement("tipo",trim($array_cab[0]['tipo'])); 
                    $cabecera->appendChild($n_7);

                    $n_8 = $dom->createElement("num_fac",trim($array_cab[0]['num_fac'])); 
                    $cabecera->appendChild($n_8);
                    
                    $n_9 = $dom->createElement("des_serv",utf8_decode(trim($array_cab[0]['des_serv']))); 
                    $cabecera->appendChild($n_9);
                    
                    $n_10 = $dom->createElement("fec_recl",trim($array_cab[0]['fec_recl']." ".$array_cab[0]['hora'])); 
                    $cabecera->appendChild($n_10);

                    $n_11 = $dom->createElement("desc_clara",utf8_decode(trim($array_cab[0]['desc_clara']))); 
                    $cabecera->appendChild($n_11);

                    $n_12 = $dom->createElement("desc_prete",utf8_decode(trim($array_cab[0]['desc_prete']))); 
                    $cabecera->appendChild($n_12);
                    
                    $n_13 = $dom->createElement("bit_mail",trim($array_cab[0]['bit_mail'])); 
                    $cabecera->appendChild($n_13);
                    
                    $n_14 = $dom->createElement("bit_conv",trim($array_cab[0]['bit_conv'])); 
                    $cabecera->appendChild($n_14);
                    
                    $n_15 = $dom->createElement("bit_celu",trim($array_cab[0]['bit_celu'])); 
                    $cabecera->appendChild($n_15);
                    
                ##</cabecera>
                
                ##<plandepago>
                    
                    $array_articulos = json_decode($json['d_a'],true);
                    
                    $plan_pago = $dom->createElement('articulos');
                    $dom->appendChild($plan_pago);
                    
                    for ($i = 0; $i < count($array_articulos); $i++){
                        
                        $items = $dom->createElement("item"); 
                        $plan_pago->appendChild($items);
                        
                        $n_16 = $dom->createElement("id",trim($array_articulos[$i]['cod_art'])); 
                        $items->appendChild($n_16);
                        
                        $n_17 = $dom->createElement("descripcion",trim($array_articulos[$i]['rec_art'])); 
                        $items->appendChild($n_17);
                        
                    }
                    
                ##</plandepago>
                    
                $xml = $dom->saveXML();
                
                $Sql = " WEBPYCCA.._reclamos_ingreso ".trim($_SESSION['c_e']).",".trim($_SESSION['cod_almacen']).",'".trim($xml)."'; ";
//                echo $Sql;
//                die;
                $resultado = $this->db->query($Sql);    
                if ($this->db->_error_message() <> ''){
                    return "ERROR AL INGRESAR RECLAMO <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                    die;
                }
                $row = $resultado->row_array();
                
                $arr_error = array(   
                            'a'=> (int)$row['co_error'],
                            'b'=> utf8_encode(trim($row['tx_error'])),
                            'c'=> trim($row['cod_soli']),
                            'd'=> trim($row['des_bdg']));

                return array('data'=>$arr_error);
                
            }
        }
    }
    
    function retGrupoUsuariosAlmacen($cod_bdg){
        if(!empty($cod_bdg)){
            
            $Sql = " WEBPYCCA.._reclamos_grupos ".trim($cod_bdg);
            $resultado = $this->db->query($Sql);  
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR GRUPO RECLAMOS <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            $array = null;
            
            foreach ($resultado->result_array() as $row) {
                $array[] = array(   
                    'mail'=> trim($row['e_mail'])
                ); 
            }

            return $array;
            
        }
    }
    
    function retFacturas($data){
        if(!empty($data)){
            
            $Sql = " PODBPOS_CIERRES.._consulta_documento '".trim($data['c_i'])."',".trim($data['a_o']).",".trim($data['m_s']);
            $resultado = $this->db->query($Sql);
            
            if ($this->db->_error_message() <> ''){
                return "ERROR AL CONSULTAR FACTURAS <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            $array = null;

            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row) {
                    
                    $array[] = array(
                        
                        'fec_doc'=> date('Y/m/d',strtotime(trim($row['fe_transaccion']))),
                        'cod_bod'=> trim($row['co_bodega']),
                        'des_bod'=> utf8_encode(trim($row['descripcion'])),
                        'tip_doc'=> trim($row['ti_documento']),
                        'tip_nom'=> trim($row['ti_documento']) === 'F'?'Factura':'Nota de Credito',
                        'num_sri'=> str_pad(trim($row['sri']),3,0, STR_PAD_LEFT),
                        'num_caj'=> str_pad(trim($row['co_caja']),3,0, STR_PAD_LEFT),
                        'num_doc'=> str_pad(trim($row['nu_documento']),9,0, STR_PAD_LEFT),
                        'val_doc'=> (float)trim($row['va_totaltransaccion']));
                }
            }
            
            return array('data'=>$array);
            
        }
    }
    
    function retDetFacturas($data){
        if(!empty($data)){
            
            $Sql = " PODBPOS_CIERRES.._consulta_documento '','','','".trim($data['t_d'])."',".(int)$data['c_i'].",".(int)$data['c_c'].",".(int)$data['c_d'];
            //echo $Sql;
            $resultado = $this->db->query($Sql);
            
            if ($this->db->_error_message() <> ''){
                return "ERROR AL CONSULTAR DETALLES DOCUMENTO <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            $array = null;

            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row) {
                    
                    $array[] = array(
                        
                        'cod_art'=> trim($row['co_articulo']),
                        'des_art'=> utf8_encode(trim($row['ds_articulo'])),
                        'cat_art'=> (int)trim($row['va_cantidad']),
                        'val_art'=> (float)trim($row['va_pvpventa']),
                        'dev_art'=> (int)trim($row['va_cantidaddyc']));
                }
                
            }
            
            return array('data'=>$array);
        }
    }
    
}

?>

