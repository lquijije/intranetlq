<?php

class m_112 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retReclamos($ci='',$cod_recla='',$cod_alma=''){
        
        $array = $array_rec_1 = $array_rec_2 = null;
        
        if ($cod_recla === ''){
            
            if ($ci === ''){
                
                $Sql = " WEBPYCCA.._reclamos_consulta '','','',1 ";
                $resultado = $this->db->query($Sql);
                if ($this->db->_error_message() <> ''){
                    $error =& load_class('Exceptions', 'core');
                    echo $error->show_error("ERROR AL RETORNAR RECLAMOS <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                    die;
                }
                
            } else {
                
                $Sql = " WEBPYCCA.._reclamos_consulta '".$ci."'";
                $resultado = $this->db->query($Sql);
                if ($this->db->_error_message() <> ''){
                    return "ERROR AL RETORNAR RECLAMO <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                    die;
                }
                
            }

            if (count($resultado->result_array())> 0){
                foreach ($resultado->result_array() as $row) {

                    $array[] = array(      
                        'id_cli'=> trim($row['id_cliente']),       
                        'nom_cli'=> utf8_encode(trim($row['nom_cli'])), 
                        'cod_alm'=> trim($row['cod_almacen']), 
                        'nom_alm'=> utf8_encode(trim($row['nom_almacen'])),  
                        'cod_rcl'=> trim($row['cod_reclamo']),           
                        'tip_rcl'=> trim($row['tipo']),      
                        'des_abr'=> utf8_encode(trim($row['desc_abr'])),      
                        'fec_rcl'=> date('Y/m/d H:s',strtotime(trim($row['fech_reclamo']))),
                        'est_rcl'=> utf8_encode(trim($row['estado'])) 
                    );

                }
            }
            
            return array('data'=>$array);
                
        } else {
            
            @ini_set('display_errors', 0);
            error_reporting(0);
            
            $Sql = " WEBPYCCA.._reclamos_consulta '',".$cod_recla.",".$cod_alma;
            //echo $Sql;
            $conn = odbc_connect("MASTER-P", "LINVENT","LINVENT");
            $resultset=odbc_exec($conn,$Sql);
            
            if (odbc_error()){
                return "ERROR AL RETORNAR DETALLE <BR>".odbc_errormsg($conn);
                die;
            }
            
            $i = 0;

            while($row = odbc_fetch_array($resultset)) {
                $i++;
                $array_rec_1[] = array(
                    'cod_alm'=>trim($row['cod_almacen']),
                    'nom_alm'=>trim(utf8_encode($row['nom_almacen'])),
                    'dir_alm'=>trim(utf8_encode($row['dir_almacen'])),
                    'ruc_alm'=>trim($row['ruc']),
                    'cod_rec'=>trim($row['cod_reclamo']),
                    'id_cli'=>trim($row['id_cliente']),
                    'nom_cli'=>trim(utf8_encode($row['nom_cli'])),
                    'dir_cli'=>trim(utf8_encode($row['direccion'])),
                    'tel_con'=>trim($row['telf_conve']),
                    'tel_cel'=>trim($row['telf_celu']),
                    'ema_cli'=>trim(utf8_encode($row['email'])),
                    'tip_rec'=>trim($row['tipo']),
                    'num_fac'=>trim($row['num_fact']),
                    'des_abr'=>trim(utf8_encode($row['desc_abr'])),
                    'des_cla'=>trim(utf8_encode($row['descripcion_clara'])),
                    'dec_obj'=>trim(utf8_encode($row['objetivo'])),
                    'fec_inc'=>trim($row['fech_incidente']),
                    'r_mai'=>trim($row['resp_mail']),
                    'r_con'=>trim($row['resp_convencional']),
                    'r_cel'=>trim($row['resp_celular']),
                    'cod_art'=>trim($row['cod_articulo']),
                    'nom_art'=>trim(utf8_encode($row['ds_articulo'])),
                    'des_art'=>trim(utf8_encode($row['descripcion'])),
                    'fec_rec'=>trim($row['fech_reclamo'])
                );
            } 

            if ($i >=1){

                odbc_next_result($resultset);

                if (!empty($resultset)){

                    while($row = odbc_fetch_array($resultset)) {
                        $array_rec_2[] = array(
                            'fec_acc'=>date('Y/m/d',strtotime($row['fech_accion'])),
                            'des_det'=>trim(utf8_encode($row['descripcion']))
                        );
                    }

                }
            }

            return array('data'=>array('cab'=>$array_rec_1,'det'=>$array_rec_2));

        }
          
    }
    
    function saveAcciones($json){
        
        if (!empty($_SESSION['usuario'])){
            
            if(!empty($json)){
                 
                $dom = new DOMDocument("1.0");
                $dom->preserveWhiteSpace = false; 
                $dom->formatOutput = true; 
                
                ##<acciones>

                $plan_pago = $dom->createElement('acciones');
                $dom->appendChild($plan_pago);

                $items = $dom->createElement("item"); 
                $plan_pago->appendChild($items);

                $n_1 = $dom->createElement("descripcion",trim($json['d_e'])); 
                $items->appendChild($n_1);
                    
                ##</acciones>
                    
                $xml = $dom->saveXML();
                
                $Sql = " WEBPYCCA.._reclamos_ingresa_acciones ".$json['c_a'].",".$json['c_r'].",'".$xml."'; ";
                $resultado = $this->db->query($Sql);    
                
                if ($this->db->_error_message() <> ''){
                    return "ERROR AL INGRESAR ACCIÓN <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                    die;
                }
                $row = $resultado->row_array();
                
                $arr_error = array(
                    'a'=> (int)$row['co_error'],
                    'b'=> utf8_encode(trim($row['tx_error']))
                ); 
                
            }

            return array('data'=>$arr_error);
        }
    }
    
    function finReclamo($data){
        
        if (!empty($_SESSION['usuario'])){
            
            if(!empty($data)){
                
                $Sql = " WEBPYCCA.._reclamos_finaliza ".$data['c_b'].",".$data['c_r'];
                $resultado = $this->db->query($Sql);    
                
                if ($this->db->_error_message() <> ''){
                    return "ERROR AL FINALIZAR RECLAMO <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                    die;
                }
                
                $row = $resultado->row_array();
                $arr_error = array(   
                    'a'=> (int)$row['co_error'],
                    'b'=> utf8_encode(trim($row['tx_error']))
                ); 
                return array('data'=>$arr_error);
            }
        }
    }
    
}


?>

