<?php
@ini_set('max_execution_time', 300);
@ini_set('display_errors', 0);
error_reporting(0);

class m_101 extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retDetDocElectronicos($data){
        if(!empty($data)){
           
            $Sql = " podbpos.dbo.posGetServersMonitor '".$data['a_l']."' ";
            $query = $this->db->query($Sql); 
            $result = $query->result_array();
            
            $errorString='';
            $arrayGe = null;
            
            foreach ($result as $row){
                
                $band = true;
                
                if (trim($row['tipo_base']) <> 'sql'){
                    if(pingServer(trim($row['de_ip_address']),trim($row['puerto']))){
                        $dbMYSQL = new mysqli($row['de_ip_address'],"pos","siste2365");
                        if ($dbMYSQL->connect_errno) {
                            $errorString.= "<strong>&bull;</strong> Error de conexión ".$dbMYSQL->connect_error."<br>";
                            $band = false;
                        }
                        
                        if($band){

                            $SqlMYSQL= " CALL podbpos.posGenMonitorFactElectronica('".trim($data['f_i'])." 00:00','".trim($data['f_f'])." 23:59','".trim($data['tp'])."',".trim($row['co_bodega']).",''); ";

                            $queryMYSQL = $dbMYSQL->query($SqlMYSQL);
                            if (!$queryMYSQL) {
                                $band = false;
                                $errorString.= "<strong>&bull;</strong> Error PROC MYSQL".$row['de_bodega']."<br>";
                            } 
                                if($band){

                                    while ($rowMysql = $queryMYSQL->fetch_assoc()) {

                                        $arrayGe[]= array('tipo_cnx'=>'M',
                                            'co_bodega'=>$rowMysql['co_bodega'],
                                            'de_bodega'=>utf8_encode($rowMysql['de_bodega']),
                                            'id_ip_address'=>$rowMysql['de_ip_address'],
                                            'estado'=>utf8_encode($rowMysql['estado']),
                                            'cantidad'=>$rowMysql['cantidad'],
                                            'base_0'=>$rowMysql['base_0'],
                                            'base_12'=>$rowMysql['base_12'],
                                            'val_iva'=>$rowMysql['val_iva']);
                                    }

                                }
                                
                            mysqli_close($dbMYSQL);
                            
                        }

                    } else {
                        $errorString.= "<strong>&bull;</strong> ".$row['de_bodega']."<br>";
                    }
                } else {
                    if ((int)trim($row['co_bodega']) === 33){
                        
                        if(pingAddress('130.1.25.25')){
                            $SqlS = " exec podbpos.dbo.posGenMonitorFactElectronica '".trim($data['f_i'])." 00:00','".trim($data['f_f'])." 23:59','".trim($data['tp'])."',33,'' ";
                            $query = $this->db->query($SqlS); 
                           
                            if ($this->db->_error_message() <> ''){
                                $errorString.= "<strong>&bull;</strong> Error PROC SQL".$row['de_bodega']."<br>";
                                $band = false;
                            }
                            
                            if($band){

                                foreach ($query->result_array() as $row5){

                                    $arrayGe[]= array('tipo_cnx'=>'S',
                                        'co_bodega'=>$row5['co_bodega'],
                                        'de_bodega'=>utf8_encode($row5['de_bodega']),
                                        'id_ip_address'=>$row5['de_ip_address'],
                                        'estado'=>$row5['estado'],
                                        'cantidad'=>$row5['cantidad'],
                                        'base_0'=>$row5['base_0'],
                                        'base_12'=>$row5['base_12'],
                                        'val_iva'=>$row5['val_iva']);
                                }
                            }

                        } else {
                            $errorString.= "<strong>&bull;</strong> ".$row['de_bodega']."<br>";
                        }
                        
                    } else {

                        if(pingServer(trim($row['de_ip_address']),trim($row['puerto']))){

                            $SqlS = " exec podbpos.dbo.posGenMonitorFactElectronica '".trim($data['f_i'])." 00:00','".trim($data['f_f'])." 23:59','".trim($data['tp'])."',".trim($row['co_bodega']).",'' ";
                            
//                            ECHO($SqlS);
//                            DIE;    
                            $query = $this->db->query($SqlS); 
                            if ($this->db->_error_message() <> ''){
                                $errorString.= "<strong>&bull;</strong> Error PROC SQL".$row['de_bodega']."<br>";
                                $band = false;
                            }
                            
                            if ($band){
                                foreach ($query->result_array() as $row5){

                                    $bodega = (int)$row5['co_bodega'] === 4?'MAYORISTAS Y WEB':utf8_encode($row5['de_bodega']);
                                    $arrayGe[]= array('tipo_cnx'=>'S',
                                                'co_bodega'=>$row5['co_bodega'],
                                                'de_bodega'=>utf8_encode($bodega),
                                                'id_ip_address'=>$row5['de_ip_address'],
                                                'estado'=>$row5['estado'],
                                                'cantidad'=>$row5['cantidad'],
                                                'base_0'=>$row5['base_0'],
                                                'base_12'=>$row5['base_12'],
                                                'val_iva'=>$row5['val_iva']);
                                }
                            }

                        } else {
                            $errorString.= "<strong>&bull;</strong> ".$row['de_bodega']."<br>";
                        }
                        
                    }
                }
                
            }
            
            if (!empty($arrayGe)){
                $arreglo = array();
                $nomTipo = '';
                if (trim($data['tp']) === 'F'){
                    $nomTipo = 'FACTURA';
                } else if (trim($data['tp']) === 'V'){
                    $nomTipo = 'NOTA DE CRÉDITO';
                } else if (trim($data['tp']) === 'E'){
                    $nomTipo = 'RETENCIONES';
                } 

                $clase = '';

                foreach ($arrayGe as $row){  
                    if((trim($row['estado']) === 'RECHAZADO POR CIMA') || (trim($row['estado']) === 'NOVEDAD CON XML - COMUNIQUE A SISTEMAS') || (trim($row['estado']) === 'XML NO GENERADO - COMUNIQUE A SISTEMAS') || (trim($row['estado']) === 'INESPERADO - COMUNIQUE A SISTEMAS')){
                        $clase ='negada';
                    } else if((trim($row['estado']) === 'NO AUTORIZADO') || (trim($row['estado']) === 'CONTINGENCIA')){
                        $clase ='estado_3';
                    } else if((trim($row['estado']) === 'ENCOLADO EN CIMA')){
                        $clase ='estado_2';
                    } else if((trim($row['estado']) === 'ESPERANDO AUTORIZACION SRI')){
                        $clase ='aprobada';
                    } else if((trim($row['estado']) === 'AUTORIZADO')){
                        $clase ='estado_1';
                    }

                    $arreglo[] = array("tipo" => $nomTipo,
                        "co_bodega" => $row['co_bodega'],
                        "de_bodega" =>  utf8_encode($row['de_bodega']),
                        "tipo_cnx" => $row['tipo_cnx'],
                        "id_ip_address" => $row['id_ip_address'],
                        "estado" => utf8_encode($row['estado']),
                        "cantidad"=>(int)$row['cantidad'],
                        "clase" => $clase,
                        'base_0'=>$row['base_0'],
                        'base_12'=>$row['base_12'],
                        'val_iva'=>$row['val_iva']); 
                }
                
                echo trim("OK-".json_encode($arreglo));
                unset($row);
            } 
            
            echo trim("/*/".json_encode(array(array('error'=>$errorString))));
            
        }
    }
    
    function GetDetalleDocumentosELectronicos($fec_ini,$fec_fin,$tipo,$almacen,$ip,$estado,$conexion){
        
        $errorString='';
        $bandera = true;
        $arreglo = null;
                    
        if(trim($conexion) === 'M'){
            if(pingServer(trim($ip),'3306')){
                
                $dbMYSQL = new mysqli(trim($ip),"pos","siste2365");
                
                if ($dbMYSQL->connect_errno) {
                    $errorString.= "<strong>&bull;</strong> ERROR CONEXIÓN MYSQL: ".$dbMYSQL->connect_error."<br>";
                    $bandera = false;
                }
                
                if($bandera){

                    $SqlMYSQL= " CALL podbpos.posGenMonitorFactElectronica('".trim($fec_ini)." 00:00','".trim($fec_fin)." 23:59','".trim($tipo)."','".trim($almacen)."','".trim($estado)."'); ";
//                    echo $SqlMYSQL;
//                    die;
                    $queryMYSQL = $dbMYSQL->query($SqlMYSQL);
                    if (!$queryMYSQL) {
                        $errorString.= "<strong>&bull;</strong> ERROR PROC MYSQL: BDG ".$almacen."<br>";
                        $bandera = false;
                    } 

                    if($bandera){
                        
                        while ($row = $queryMYSQL->fetch_assoc()) {
                            $arreglo[] = array(
                                'fe_transaccion'=>!empty($row['fe_transaccion'])?date('Y/m/d h:i:s',strtotime($row['fe_transaccion'])):'',
                                'co_bodega'=>$row['co_bodega'],
                                'fe_envio'=>!empty($row['fe_envio'])?date('Y/m/d h:i:s',strtotime($row['fe_envio'])):'',
                                'de_envio'=>utf8_encode($row['de_envio']),
                                'fe_consulta'=>!empty($row['fe_consulta'])?date('Y/m/d h:i:s',strtotime($row['fe_consulta'])):'',
                                'co_caja' =>$row['co_caja'],
                                'ti_documento' =>$row['ti_documento'],
                                'nu_documento' =>$row['nu_documento'],
                                'de_xml' =>$row['de_xml'],
                                'estado' =>$row['estado'],
                                'de_consulta' =>$row['de_consulta'],
                                'nu_autorizacion' =>$row['nu_autorizacion'],
                                'documento' =>$row['documento'],
                                'va_totaltransaccion' =>$row['va_totaltransaccion'],
                                'co_cliente' =>$row['co_cliente'],
                                'conexion' =>$conexion
                                );
                        }

                        mysqli_close($dbMYSQL);
                    
                    }
                    
                }

            } else {
                $errorString.= "<strong>&bull;</strong> SIN CONEXION ALMACÉN MYSQL: ".$almacen."<br>";
            }

        } else if(trim($conexion) === 'S'){
            
            if(pingServer(trim($ip),'1433')){
                
                $SqlS = " exec podbpos.dbo.posGenMonitorFactElectronica '".trim($fec_ini)." 00:00','".trim($fec_fin)." 23:59','".trim($tipo)."','".trim($almacen)."','".trim($estado)."' ";
                $query = $this->db->query($SqlS); 
                if ($this->db->_error_message() <> ''){
                    $errorString.= "<strong>&bull;</strong> ERROR PROC SQL: BDG ".$almacen." <br>";
                    $bandera = false;
                }
                
                if($bandera){
                    foreach($query->result_array() as  $row) {

                        $arreglo[] = array(
                            'fe_transaccion'=>!empty($row['fe_transaccion'])?date('Y/m/d h:i:s',strtotime($row['fe_transaccion'])):'',
                            'co_bodega'=>$row['co_bodega'],
                            'fe_envio'=>!empty($row['fe_envio'])?date('Y/m/d h:i:s',strtotime($row['fe_envio'])):'',
                            'de_envio'=>utf8_encode($row['de_envio']),
                            'fe_consulta'=>!empty($row['fe_consulta'])?date('Y/m/d h:i:s',strtotime($row['fe_consulta'])):'',
                            'co_caja'=>$row['co_caja'],
                            'ti_documento'=>$row['ti_documento'],
                            'nu_documento'=>$row['nu_documento'],
                            'de_xml'=> $row['de_xml'],
                            'estado'=> utf8_encode($row['estado']),
                            'de_consulta'=> utf8_encode($row['de_consulta']),
                            'nu_autorizacion'=>$row['nu_autorizacion'],
                            'documento'=>$row['documento'],
                            'va_totaltransaccion'=>$row['va_totaltransaccion'],
                            'co_cliente'=>$row['co_cliente'],
                            'conexion'=>$conexion
                        );
                    }
                }
                
            } else {
                $errorString.= "<strong>&bull;</strong> SIN CONEXION ALMACÉN SQL BDG ".$almacen."<br>";
            }

        }
        
        return array('data'=>$arreglo,'error'=>$errorString) ;

    }
}
?>
