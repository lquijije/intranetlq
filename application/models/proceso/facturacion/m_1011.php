<?php

class m_1011 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retHistPass($c_i){
        
        $array = null;

        if(!empty($c_i)){
            
            $Sql = " SGDBSEGURIDAD..sp_hist_cambio_clave '".trim($c_i)."' ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR HISTORIAL DE CAMBIO DE CLAVE <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            $res = $resultado->result_array();
            if (count($res)> 0){
                foreach ($res as $row){
                    $array[] = array(
                        'ruc'=> trim($row['ruc']),
                        'fec'=> trim($row['fecha']),
                        'usu'=> trim($row['usuario']),
                    );
                }
            }
            
        }
        
        return $array;
            
    }
    
    function saveHistPass($c_i){
        
        $array = null;

        if(!empty($c_i)){
            
            $Sql = " SGDBSEGURIDAD..sp_insert_segmAccesoWebSRI '".trim($c_i)."','".$_SESSION['c_e']."' ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL GRABAR HISTORIAL DE CAMBIO DE CLAVE <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            $row = $resultado->row_array();
            if (count($row)> 0){
                $array = array(
                    'co_err'=> trim($row['error']),
                    'tx_err'=> trim($row['desc_error']),
                );
            }
            
        }
        
        return $array;
            
    }
}
?>
