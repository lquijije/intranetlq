<?php

class m_comprobantes extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    function retEstadosDocumentos() {
        $array = null;
        $error = '';
        $Sql = " SELECT DISTINCT b.codigo,b.descripcion ";
        $Sql .= " FROM SRDBSRI.cima.fac_cab_documentos a ";
        $Sql .= " INNER JOIN SRDBSRI.cima.estados b ";
        $Sql .= " ON a.ESTADO_TRANSACCION = b.codigo; ";
        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            $error = & load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR ESTADOS <BR> COD_ERR:" . $this->db->_error_number(), $this->db->_error_message(), 'error_db');
            die;
        } else {
            $rs = $result->result_array();
            if (!empty($rs)) {
                foreach ($rs as $row) {
                    $array[] = array(
                        'co_est' => trim($row['codigo']),
                        'no_est' => trim(utf8_decode($row['descripcion']))
                    );
                }
            }
        }
        return $array;
    }

    function retDocumentosEmitidos($anio, $mes, $dia, $tipo_autorizacion, $tipo_documento, $co_identificacion, $co_establecimiento, $co_puntoemision, $co_secuencial) {
        $array = array('dat' => '', 'tot' => '', 'doc' => '', 'aut' => '', 'err' => '');
        if ($anio > 0) {
            try {
                $mes = (int) $mes > 0 ? $mes : 'NULL';
                $dia = (int) $dia > 0 ? $dia : 'NULL';
                $tipo_autorizacion = trim($tipo_autorizacion) == 'NULL' ? 'NULL' : "'" . trim($tipo_autorizacion) . "'";
                $tipo_documento = trim($tipo_documento) == 'NULL' ? 'NULL' : (int) $tipo_documento;
                $co_identificacion = empty($co_identificacion) ? 'NULL' : "'" . trim($co_identificacion) . "'";
                $co_establecimiento = empty($co_establecimiento) ? 'NULL' : "'" . trim($co_establecimiento) . "'";
                $co_puntoemision = empty($co_puntoemision) ? 'NULL' : "'" . trim($co_puntoemision) . "'";
                $co_secuencial = empty($co_secuencial) ? 'NULL' : "'" . trim($co_secuencial) . "'";

                $Sql = " SRDBSRI.cima.documentos_emitidos_consulta " . $anio . "," . $mes . "," . $dia . "," . $tipo_autorizacion . "," . $tipo_documento . "," . $co_identificacion . "," . $co_establecimiento . "," . $co_puntoemision . "," . $co_secuencial . "; ";
                $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
                $resultset = odbc_exec($conn, $Sql);
                if (odbc_error()) {
                    throw new Exception(odbc_errormsg($conn));
                }
                do {
                    while ($row = odbc_fetch_array($resultset)) {
                        if (!empty($row['resultset'])) {
                            if (isset($row['resultset'])) {
                                switch ($row['resultset']) {
                                    case "DATA":
                                        $array['dat'][] = array(
                                            'fe_emi' => empty($row['fechaEmision']) ? '' : date('Y-m-d', strtotime($row['fechaEmision'])),
                                            'tp_doc' => trim(utf8_encode($row['tipoDocumento'])),
                                            'ra_soc' => trim(utf8_encode($row['razonSocialComprador'])),
                                            'id_com' => trim($row['identificacionComprador']),
                                            'nu_fac' => trim($row['CodEstablecimiento']) . trim($row['CodPuntEmision']) . trim($row['secuencial']),
                                            'nu_aut' => trim($row['autorizacion']),
                                            'es_aut' => trim($row['estadoAutorizacion']),
                                            'no_es_aut' => trim(utf8_encode($row['no_estadoAutorizacion'])),
                                            'fe_aut' => empty($row['fechaAutorizado']) ? '' : date('Y-m-d H:i:s', strtotime($row['fechaAutorizado'])),
                                            'e_mail' => '',
                                            //'e_mail' => trim($row['email']),
                                            'cl_acc' => trim($row['claveAcceso']),
                                            'sub_12' => $row['subtotal12'],
                                            'sub_0' => $row['subtotal0'],
                                            'iva_12' => $row['iva12'],
                                            'im_tot' => $row['importeTotal']
                                        );
                                        break;
                                    case "TOTALES":
                                        $array['tot'] = array(
                                            'sub_12' => $row['subtotal12'],
                                            'sub_0' => $row['subtotal0'],
                                            'iva_12' => $row['iva12'],
                                            'im_tot' => $row['importeTotal'],
                                        );
                                        break;
                                    case "DOCUMENTOS":
                                        $array['doc'][] = array(
                                            'co_doc' => trim($row['CodigoDocumento']),
                                            'no_doc' => trim(utf8_encode($row['tipoDocumento'])),
                                            'ca_doc' => (int) $row['cantidad']
                                        );
                                        break;
                                    case "ESTADOSAUTORIZACION":
                                        $array['aut'][] = array(
                                            'co_aut' => trim($row['estadoAutorizacion']),
                                            'no_aut' => trim($row['no_estadoAutorizacion']),
                                            'ca_aut' => (int) $row['cantidad']
                                        );
                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
                odbc_close($conn);
            } catch (Exception $e) {
                $array['err'] = "ERROR AL RETORNAR DOCUMENTOS EMITIDOS:" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function retDocumentosRecibidos($anio, $mes, $dia, $tipo_documento, $co_identificacion, $co_establecimiento, $co_puntoemision, $co_secuencial, $contabilizado) {
        $array = array('dat' => '', 'tot' => '', 'doc' => '', 'con' => '', 'err' => '');
        if ($anio > 0) {
            try {
                $mes = (int) $mes > 0 ? $mes : 'NULL';
                $dia = (int) $dia > 0 ? $dia : 'NULL';
                $tipo_documento = trim($tipo_documento) == 'NULL' ? 'NULL' : "'" . trim($tipo_documento) . "'";
                $co_identificacion = empty($co_identificacion) ? 'NULL' : "'" . trim($co_identificacion) . "'";
                $co_establecimiento = empty($co_establecimiento) ? 'NULL' : "'" . trim($co_establecimiento) . "'";
                $co_puntoemision = empty($co_puntoemision) ? 'NULL' : "'" . trim($co_puntoemision) . "'";
                $co_secuencial = empty($co_secuencial) ? 'NULL' : "'" . trim($co_secuencial) . "'";
                $contabilizado = $contabilizado == 0 ? 'NULL' : 1;

                $Sql = " SRDBSRI.cima.documentos_recibidos_consulta " . $anio . "," . $mes . "," . $dia . "," . $tipo_documento . "," . $co_identificacion . "," . $co_establecimiento . "," . $co_puntoemision . "," . $co_secuencial . "," . $contabilizado . "; ";
                $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
                $resultset = odbc_exec($conn, $Sql);
                if (odbc_error()) {
                    throw new Exception(odbc_errormsg($conn));
                }
                do {
                    while ($row = odbc_fetch_array($resultset)) {
                        if (!empty($row['resultset'])) {
                            if (isset($row['resultset'])) {
                                switch ($row['resultset']) {
                                    case "DATA":
                                        $array['dat'][] = array(
                                            'fe_emi' => empty($row['fechaEmision']) ? '' : date('Y-m-d', strtotime($row['fechaEmision'])),
                                            'tp_doc' => trim(utf8_encode($row['tipoDocumento'])),
                                            'ra_soc' => trim(utf8_encode($row['razonSocialComprador'])),
                                            'id_com' => trim($row['identificacionComprador']),
                                            'nu_fac' => trim($row['CodEstablecimiento']) . trim($row['CodPuntEmision']) . trim($row['secuencial']),
                                            'nu_aut' => trim($row['autorizacion']),
                                            'es_aut' => trim(utf8_encode($row['estadoAutorizacion'])),
                                            'no_es_aut' => trim($row['no_estadoAutorizacion']),
                                            'fe_aut' => empty($row['fechaAutorizado']) ? '' : date('Y-m-d H:i:s', strtotime($row['fechaAutorizado'])),
                                            'cl_acc' => trim($row['claveAcceso']),
                                            'sub_12' => $row['subtotal12'],
                                            'sub_0' => $row['subtotal0'],
                                            'iva_12' => $row['iva12'],
                                            'im_tot' => $row['importeTotal'],
                                            'contab' => (int) $row['contabilizado']
                                        );
                                        break;
                                    case "TOTALES":
                                        $array['tot'] = array(
                                            'sub_12' => $row['subtotal12'],
                                            'sub_0' => $row['subtotal0'],
                                            'iva_12' => $row['iva12'],
                                            'im_tot' => $row['importeTotal'],
                                        );
                                        break;
                                    case "DOCUMENTOS":
                                        $array['doc'][] = array(
                                            'co_doc' => trim($row['CodigoDocumento']),
                                            'no_doc' => trim(utf8_encode($row['tipoDocumento'])),
                                            'ca_doc' => (int) $row['cantidad']
                                        );
                                        break;
                                    case "CONTABILIZADO":
                                        $array['con'][] = array(
                                            'co_con' => trim($row['contabilizado']),
                                            'no_con' => trim($row['tx_contabilizado']),
                                            'ca_con' => (int) $row['cantidad']
                                        );
                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
                odbc_close($conn);
            } catch (Exception $e) {
                $array['err'] = "ERROR AL RETORNAR DOCUMENTOS RECIBIDOS:" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

}

?>
