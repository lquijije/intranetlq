<?php

class m_103 extends CI_Model{

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retComents($soli){
        
        if(!empty($soli)){
            $Sql = " helpdesk_consulta_comentarios ".$_SESSION['c_e'].",'".$soli."','','','TOP 20'";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL RETORNAR COMENTARIOS: ".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            } 
            return $result->result_array();
        }
    }
    
    function retEstadoAgrupados($user){
        if(!empty($user)){
            $Sql  = " SELECT f.co_estado,g.nombre+'' as nom_estado,g.variable_2+'' as icon , ";
            $Sql .= " count(1)+'' as cantidad ";
            $Sql .= " FROM tb_helpdesk_recurso a ";
            $Sql .= " INNER JOIN tb_helpdesk_actividad e ON a.co_empleado = e.co_asignado AND e.co_estado = 'A' ";
            $Sql .= " INNER JOIN tb_helpdesk_solicitud f ON e.co_solicitud = f.co_solicitud ";
            $Sql .= " LEFT JOIN tb_intranet_parametros g ON g.tipo = 4 and f.co_estado = g.variable_1 ";
            $Sql .= " WHERE a.co_empleado = $user ";
            $Sql .= " group by f.co_estado,g.nombre,g.variable_2 ";

            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL RETORNAR ESTADOS: ".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            }
            return $resultado->result_array();
        }
    }
    
    function retActiUser($dpto,$co_emplea,$fec_ini,$fec_fin){
        
        if (trim($fec_ini) <> '' && trim($fec_fin) <> ''){
            $Sql = " helpdesk_actividad_usuario ".$dpto.",".$co_emplea.",'".$fec_ini."','".$fec_fin."' ";
        } else {
            $Sql = " helpdesk_actividad_usuario ".$dpto.",".$co_emplea;
        }
        
        $result = $this->db->query($Sql);
            
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            return "ERROR AL RETORNAR USUARIOS ACTIVIDAD SOLICITUD".$this->db->_error_number()." ".$this->db->_error_message();
            die;
        } 
        
        if (count($result->result_array()) > 0){
            $array = null;
            foreach ($result->result_array() as $row){
                $array[] = array('co_empleado'=>  utf8_encode($row['co_empleado']),
                    'nombre'=>  utf8_encode(ucwords(strtolower($row['nombre']))),
                    'usuario'=> utf8_encode($row['usuario']),
                    'tx_descripcion'=> utf8_encode($row['tx_descripcion']),
                    'tipo'=> utf8_encode($row['tipo']),
                    'co_solicitud'=> $row['co_solicitud'],
                    'tx_titulo'=> utf8_encode($row['tx_titulo']),
                    'co_actividad'=> $row['co_actividad'],
                    'tx_detalle'=> utf8_encode($row['tx_detalle']),
                    'secuencia_prioridad'=> $row['secuencia_prioridad'],
                    'tiempo'=> $row['nu_tiempo_estimado']." ".$row['co_unidad_tiempo'],
                    'fe_asignacion'=> date('Y/m/d h:i',strtotime($row['fe_asignacion'])),
                    'fe_ingreso'=> date('Y/m/d h:i',strtotime($row['fe_ingreso'])),
                    'icon'=> $row['icon'],
                    'prioridad_usuario'=> $row['prioridad_usuario'],
                    'co_tipo'=> $row['co_tipo'],
                    'fl_siendo_atendido'=> $row['fl_siendo_atendido'],
                    'co_unidad_tiempo'=> $row['co_unidad_tiempo'],
                    'nu_tiempo_estimado'=> (int)$row['nu_tiempo_estimado'],
                    'va_progreso'=> $row['va_progreso'],
                    'cod_emp_ing'=> $row['cod_emp_ing'],
                    'nomb_empl_ing'=> $row['nomb_empl_ing'],
                    'nomb_ccosto_ing'=> $row['nomb_ccosto_ing']
                );
            }

            return ($array);
        }
        
    }
    
    function retUserArea($getDpto){
        
        $Sql  = " SELECT a.co_empleado,d.apellidos + ' ' +d.nombres+'' AS nombre,b.co_departamento,a.co_area,b.tx_descripcion ";
        $Sql .= " FROM tb_helpdesk_recurso a ";
        $Sql .= " INNER JOIN tb_helpdesk_area b ON a.co_area=b.co_area ";
        $Sql .= " INNER JOIN tb_helpdesk_departamento c ON b.co_departamento=c.co_departamento ";
        $Sql .= " INNER JOIN tb_intranet_users d ON a.co_empleado = d.cod_empleado  ";
        $Sql .= " WHERE c.co_departamento= ".$getDpto;
        $Sql .= " AND a.co_empleado= ".$_SESSION['c_e'];
        $Sql .= " ORDER BY c.co_departamento, a.co_area ";
        //echo $Sql;
        $result = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            return "ERROR AL RETORNAR SOLICIT: ".$this->db->_error_number()." ".$this->db->_error_message();
            die;
        } 
        
        if(count($result->result_array()) > 0){
            $dpto = '';
            $select= '<select id="areaUserSelect" class="js-states form-control">';
            foreach($result->result_array() as $row) {
                    foreach($result->result_array() as $row1) {
                        if (utf8_encode($row['tx_descripcion']) === utf8_encode($row1['tx_descripcion'])){
                            $select.= "<option value='".$row1['co_empleado']."'>".utf8_encode($row1['nombre'])."</option>";
                        }
                    }
                $dpto = utf8_encode($row['tx_descripcion']);
            }
            $select.= '</select>';
            return $select;
            
        } else {
            $select= '<select id="areaUserSelect" class="js-states form-control">';
            $select.= "<option value='NN' disabled selected>No hay usuarios disponibles</option>";
            $select.= '</select>';
            return $select;
        }
        
    }
    
    function enCurso($id_solici,$id,$tipo){
        
        $Sql  = " UPDATE tb_helpdesk_actividad ";
        $Sql .= " SET fl_siendo_atendido = null "; 
        $Sql .= " WHERE co_asignado = ".$_SESSION['c_e'];

        $this->db->query($Sql);
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            return "ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
            die();
        } 
        
        if ((int)$tipo === 1){
            
            $Sql  = " UPDATE tb_helpdesk_actividad ";
            $Sql .= " SET fl_siendo_atendido = 1 "; 
            $Sql .= " WHERE co_asignado = ".$_SESSION['c_e'];
            $Sql .= " and co_solicitud = $id_solici ";
            $Sql .= " and co_actividad = $id ";

            $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
                die();
            } 
        
        }
        echo "OK";
        
    }
    
    function savePorcTiempActi($data){
        if(!empty($data)){
            $cod_emp = isset($data['c_e']) && !empty($data['c_e'])?$data['c_e']:$_SESSION['c_e'];
            $Sql  = " helpdesk_actualiza_porc_actividad ".$data['c_s'].",".$data['c_a'].",".$cod_emp.",".$data['p_e'].",'".$data['u_d']."',".$data['t_o']." ";
            $query = $this->db->query($Sql);
            $row = $query->row_array(); 
            return $row;
            
        }
    }
    
}
?>
