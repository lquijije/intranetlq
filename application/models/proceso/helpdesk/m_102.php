<?php

class m_102 extends CI_Model{

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retComents($dpto){
        
        if(!empty($dpto)){
            $Sql = " helpdesk_consulta_comentarios ".$_SESSION['c_e'].",'','','','TOP 20',".$dpto;
            $result = $this->db->query($Sql);
            
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR COMENTARIOS <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            return $result->result_array();
        }
    }
    
    function retVenceAsignar($tipo,$dpto){
        if(!empty($dpto)){
            $Sql  = " tb_helpdesk_agrupa_vence_asigna '".$tipo."',$dpto ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR SIN ASIGNAR <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            $array = null;
            foreach ($result->result_array() as $row){
                $color = $row['co_tipo'] === 'R'?'#1DC096':'#2E8BEF';
                $highlight = $row['co_tipo'] === 'R'?'#169473':'#1171d9';
                $array[]= array(
                    'value'=>(int)$row['cantidad'],
                    'color'=>$color,
                    'highlight'=>$highlight,
                    'label'=>utf8_encode(ucwords(strtolower($row['nom_tipo']))));
            }

            return json_encode($array);
        }
    }
    
    function retTipoSolicit($data){
        if (!empty($data)){
            $Sql  = " tb_helpdesk_vence_asigna '".$data['t_p']."','".$data['t_s']."',".$data['d_o'];
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR SOLICIT <br> COD_ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            } 
            $array = null;
            $Retarray = $result->result_array();
            if (!empty($Retarray)){
                foreach ($Retarray as $row){
                    $array[] = array (
                        'co_solicitud'=>$row['co_solicitud'],
                        'co_tipo'=>$row['co_tipo'],
                        'nom_tipo'=>utf8_decode($row['nom_tipo']),
                        'tx_titulo'=>utf8_decode($row['tx_titulo']),
                        'co_estado'=>utf8_decode($row['co_estado']),
                        'nom_estado'=>utf8_decode($row['nom_estado']),
                        'fe_ingreso'=>$row['fe_ingreso'],
                    );
                }
            }
            
            return json_encode(array('data'=>$array));
        }
    }
    
    function retUserArea($getDpto){
        
        $Sql  = " SELECT a.co_empleado,d.apellidos + ' ' +d.nombres+'' AS nombre,b.co_departamento,a.co_area,b.tx_descripcion ";
        $Sql .= " FROM tb_helpdesk_recurso a ";
        $Sql .= " INNER JOIN tb_helpdesk_area b ON a.co_area=b.co_area ";
        $Sql .= " INNER JOIN tb_helpdesk_departamento c ON b.co_departamento=c.co_departamento ";
        $Sql .= " INNER JOIN tb_intranet_users d ON a.co_empleado = d.cod_empleado AND d.estado = 1 ";
        $Sql .= " WHERE c.co_departamento= ".$getDpto;
        $Sql .= " ORDER BY c.co_departamento, a.co_area ";
        //echo $Sql;
        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR USUARIO AREA <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        }
        
        if(count($result->result_array()) > 0){
            $dpto = '';
            $select= '<select id="areaUserSelect" class="js-states form-control">';
            $select.= "<option value='NN' disabled selected>Seleccionar Usuario</option>";
            foreach($result->result_array() as $row) {
                if($dpto != utf8_encode($row['tx_descripcion'])){
                    $select.= "<optgroup label='".utf8_encode($row['tx_descripcion'])."'>";
                    foreach($result->result_array() as $row1) {
                        if (utf8_encode($row['tx_descripcion']) === utf8_encode($row1['tx_descripcion'])){
                            $select.= "<option value='".$row1['co_empleado']."'>".utf8_encode($row1['nombre'])."</option>";
                        }
                    }
                    $select.= "</optgroup>";
                }
                $dpto = utf8_encode($row['tx_descripcion']);
            }
            $select.= '</select>';
            
        } else {
            $select= '<select id="areaUserSelect" class="js-states form-control">';
            $select.= "<option value='NN' disabled selected>No hay usuarios disponibles</option>";
            $select.= '</select>';
        }

        return $select;

    }
    
    function saveAsignaciones($data){
        if (!empty($data['d_e'])){
            $array = explode('|',$data['d_e']);
            $count = count($array);

            $dom = new DOMDocument("1.0");
            $node = $dom->createElement("items"); 
            $parnode = $dom->appendChild($node); 

            for($i = 0; $i < $count; $i++){
                $subArray = explode('/*/',$array[$i]);
                $node = $dom->createElement("item"); 
                $hijo = $parnode->appendChild($node);

                $node = $dom->createElement("codigo"); 
                $node->nodeValue= $subArray[0]; 
                $hijo->appendChild($node);

                $node = $dom->createElement("descripcion"); 
                $node->nodeValue= utf8_encode($subArray[1]); 
                $hijo->appendChild($node);
            }

            $xml = $dom->saveXML();
            
            $Sql  = " helpdesk_actividad_agrega ".trim($data['c_s']).",".trim($_SESSION['c_e']).",'".trim($xml)."' ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL GRABAR ACTIVIDADES <BR> COD_ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            }  
            
            $row = $result->row_array();
            return $row;
            
        } else {
            //echo 'No ha ingresado ninguna usuario';
        }
    }
    
    function UpdateFecTop($data){
        if(!empty($data)){
            if (!empty($data['f_e'])){
                
                $Sql  = " UPDATE tb_helpdesk_solicitud SET ";
                $Sql .= " fe_tope = '".trim($data['f_e'])."', ";
                $Sql .= " prioridad_usuario = ".trim( str_replace('star-','',$data['p_d']))." ";
                $Sql .= " WHERE co_solicitud = ".trim($data['c_s'])." ";
                $Sql .= " AND co_estado NOT IN ('F','C') ";
                $this->db->query($Sql);

            }            
            
            return 'Grabado Correctamente';

        }
    }
    
    function UpdateAsignacion($data){
        if (!empty($data)){
            
            $Sql = " helpdesk_actualiza_estado_actividad ".$data['c_s'].",".$data['c_a'].",".$data['c_e'].",'I'  ";
            $query = $this->db->query($Sql);
            $row_2 = $query->row_array(); 

            if((int)$row_2['co_error'] <> 0){
                echo $row_2['tx_error'];
                die;
            }
            
            $Sql  = " SELECT COUNT(*)+'' as cantidad ";
            $Sql .= " FROM tb_helpdesk_actividad ";
            $Sql .= " WHERE co_solicitud = ".$data['c_s'];
            $Sql .= " AND co_estado = 'A' ";
            
            $query = $this->db->query($Sql);
            if ($this->db->_error_message()){
                return "ERROR".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            }
            
            $row = $query->row_array();
            
            if((int)$row['cantidad'] === 0){
                
                $Sql = " helpdesk_actualiza_solicitud ".trim($data['c_s']).",".trim($_SESSION['c_e']).",'A' ";
                $query = $this->db->query($Sql);
                $row_1 = $query->row_array(); 

                if((int)$row_1['co_error'] <> 0){
                    echo $row_1['tx_error'];
                    die;
                }
                
            }
            
            echo 'Registro Eliminado Correctamente';
            
        }
    }
    
    function RetSolictAsig($co_solci) {
        if (!empty($co_solci)){
            
            $Sql  = " SELECT co_solicitud,co_actividad,co_asignador,co_asignado,b.apellidos + ' ' +b.nombres+'' as nombres,tx_detalle ";
            $Sql .= " FROM tb_helpdesk_actividad a ";
            $Sql .= " INNER JOIN tb_intranet_users b ";
            $Sql .= " ON a.co_asignado = b.cod_empleado ";
            $Sql .= " WHERE co_solicitud =".$co_solci;
            $Sql .= " AND co_estado = 'A' ";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL RETORNAR ACTIVIDADES".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            } 
            $array = null;
            if(count($result->result_array() > 0)){
                foreach ($result->result_array() as $row){
                    $array[] = array('co_solicitud'=>$row['co_solicitud'],
                        'co_actividad'=>$row['co_actividad'],
                        'co_asignador'=>$row['co_asignador'],
                        'co_asignado'=>$row['co_asignado'],
                        'nombres'=>utf8_encode($row['nombres']),
                        'tx_detalle'=>utf8_encode($row['tx_detalle'])
                    );
                }
            }
            
            return json_encode($array);
        }
    }
    
    function retTableMonitor($getDpto,$fec_ini,$fec_fin){
        if(!empty($getDpto) && !empty($fec_ini) && !empty($fec_fin)){
          
            $Sql = " helpdesk_monitor_table ".$getDpto.",'".$fec_ini." 00:00','".$fec_fin." 23:59' ";
            $result = $this->db->query($Sql);
            
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL RETORNAR TABLA MONITOR".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            } 
            return $result->result_array();
            
        } else {
        
        }
        
    }
    
    function retActiUser($co_emplea,$id_dpto){
        
        $Sql = " helpdesk_actividad_usuario $id_dpto,$co_emplea";
        //echo $Sql;
        $result = $this->db->query($Sql);
            
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            return "ERROR AL RETORNAR USUARIOS ACTIVIDAD SOLICITUD".$this->db->_error_number()." ".$this->db->_error_message();
            die;
        } 
        
        if (count($result->result_array()) > 0){
            $array = null;
            foreach ($result->result_array() as $row){
                $array[] = array('co_empleado'=>  utf8_encode($row['co_empleado']),
                        'nombre'=>  utf8_encode(ucwords(strtolower($row['nombre']))),
                        'usuario'=> utf8_encode($row['usuario']),
                        'tx_descripcion'=> utf8_encode($row['tx_descripcion']),
                        'tipo'=> utf8_encode($row['tipo']),
                        'co_solicitud'=> $row['co_solicitud'],
                        'tx_titulo'=> utf8_encode($row['tx_titulo']),
                        'co_actividad'=> $row['co_actividad'],
                        'tx_detalle'=> utf8_encode($row['tx_detalle']),
                        'secuencia_prioridad'=> $row['secuencia_prioridad'],
                        'tiempo'=> $row['nu_tiempo_estimado']." ".$row['co_unidad_tiempo'],
                        'fe_asignacion'=> date('Y/m/d h:i',strtotime($row['fe_asignacion'])),
                        'fe_ingreso'=> date('Y/m/d h:i',strtotime($row['fe_ingreso'])),
                        'icon'=> $row['icon'],
                        'prioridad_usuario'=> $row['prioridad_usuario'],
                        'fl_siendo_atendido'=> (int)$row['fl_siendo_atendido'],
                        'va_progreso'=> (int)$row['va_progreso']
                    );
            }

            echo json_encode($array);
        }
        
    }
    
    function ActualizaPrioridadActividad($data){
        if (!empty($data)){
            $array = explode('*//*',$data);
            $count = count($array);

            $dom = new DOMDocument("1.0");
            $node = $dom->createElement("items"); 
            $parnode = $dom->appendChild($node); 

            for($i = 0; $i < $count; $i++){
                $subArray = explode('=',$array[$i]);
                $childarray = explode('-',$subArray[0]);
                //print_r($childarray);
                $node = $dom->createElement("item"); 
                $hijo = $parnode->appendChild($node);
 
                $node = $dom->createElement("co_solicitud"); 
                $node->nodeValue= $childarray[0]; 
                $hijo->appendChild($node);
 
                $node = $dom->createElement("co_actividad"); 
                $node->nodeValue= $childarray[1]; 
                $hijo->appendChild($node);
                
                $node = $dom->createElement("secuencia"); 
                $node->nodeValue= $subArray[1]; 
                $hijo->appendChild($node);
            }
               
            $xml = $dom->saveXML();
            
            $Sql  = " helpdesk_actualiza_actividades '".trim($xml)."' ";
            $result =  $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL ACTUALIZAR ACTIVIDADES: ".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            }  
            $row = $result->row_array();
            
            if ((int)$row['co_error'] === 0){
                echo 'Grabado Correctamente';
            } else {
                echo $row['tx_error'];
            }
            
        } else {
            echo 'Error';
        }
    }
    
    function retReportActiUser($data){

        if(!empty($data)){
            $Sql = " helpdesk_report_actividades_usuario ".$data['c_d'].",'".$data['f_i']."','".$data['f_f']."'";
            
            $result = $this->db->query($Sql);

            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR REPORTE USUARIOS ACTIVIDAD".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            } 

            if (count($result->result_array()) > 0){
                $array = null;
                foreach ($result->result_array() as $row){
                    
                    $array[] = array(
                        'fe_tope'=> !empty($row['fe_tope'])?date('Y/m/d h:i',strtotime($row['fe_tope'])):'',
                        'fe_ingreso'=> !empty($row['fe_ingreso'])?date('Y/m/d h:i',strtotime($row['fe_ingreso'])):'',
                        'co_empleado'=> utf8_encode($row['co_asignado']),
                        'nombre'=> utf8_encode(ucwords(strtolower($row['nombre']))),
                        'tipo'=> utf8_encode($row['tipo']),
                        'co_solicitud'=> $row['co_solicitud'],
                        'tx_titulo'=> utf8_encode($row['tx_titulo']),
                        'co_estado'=> utf8_encode($row['nom_esta_soli']),
                        'co_actividad'=> $row['co_actividad'],
                        'tx_detalle'=> utf8_encode($row['tx_detalle']),
                        'fe_asignacion'=> !empty($row['fe_asignacion'])?date('Y/m/d h:i',strtotime($row['fe_asignacion'])):'',
                        'fl_siendo_atendido'=> (int)$row['fl_siendo_atendido'] === 1?'Atendido':'',
                        'va_progreso'=> (int)$row['va_progreso']
                    );
                }

                return $array;
            }
        }
        
    }
    
}

?>

