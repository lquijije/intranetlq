<?php

class m_101 extends CI_Model{

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retComents($cod_sol){
        
        $Sql = " helpdesk_consulta_comentarios 0,'".$cod_sol."','','','TOP 20' ";
        $result = $this->db->query($Sql);
        if ($this->db->_error_message()){
            return "ERROR AL RETORNAR COMENTARIOS: ".$this->db->_error_number()." ".$this->db->_error_message();
            die;
        } 
        
        $array = null;

        foreach ($result->result_array() as $row) {
            
            $array[] = array(
                'co_solicitud'=>$row['co_solicitud'],
                'co_tipo'=>$row['co_tipo'],
                'tx_titulo'=>utf8_encode($row['tx_titulo']),
                'fe_ingreso'=>$row['fe_ingreso'],
                'fe_comentario'=>utf8_encode($row['fe_comentario']),
                'tx_descripcion'=>utf8_encode($row['tx_descripcion']),
                'co_usuario'=>$row['co_usuario'],
                'fl_responder'=>$row['fl_responder'],
                'apellidos'=>utf8_encode($row['apellidos']),
                'nombres'=>utf8_encode($row['nombres']),
                'co_estado'=>$row['co_estado'],
            );
            
        }    
        
        return $array;

    }
    
    function retTopSolicit($user,$dpto){
        
        $Sql = " helpdesk_consulta_solicitud $user,$dpto ";
        $result = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            return "ERROR AL RETORNAR TOP SOLICIT: ".$this->db->_error_number()." ".$this->db->_error_message();
            die;
        } 
        
        $array = null;
        
        $row = $result->row_array(); 
           
        if((int)isset($row['co_error']) <> 0){
            echo $row['tx_error'];
            die();
        }
        
        foreach ($result->result_array() as $row) {
            
            $array[] = array(
                'co_solicitud'=>$row['co_solicitud'],
                'co_estado'=>$row['co_estado'],
                'nom_estado'=>$row['nom_estado'],
                'icon'=>$row['icon'],
                'tx_titulo'=>utf8_encode($row['tx_titulo']),
                'tx_descripcion_detallada'=>utf8_encode($row['tx_descripcion_detallada']),
                'fe_ingreso'=>$row['fe_ingreso'],
                'co_tipo'=>$row['co_tipo'],
                'tx_descripcion'=>utf8_encode($row['tx_descripcion']),
                'co_empleado'=>$row['co_empleado'],
                'nomb_empl'=>utf8_encode($row['nomb_empl'])
            );
            
        }    
        
        return $array;
    }
    
    function guardar($data){
        
        if (!empty($data)){
            
            $prio = $data['t_s'] === 'R'?'1':'3';
            $Sql = " helpdesk_ingreso_solicitud '".$data['t_s']."','".trim(utf8_decode(str_replace("'",'"',$data['t_o'])))."','".trim($data['d_n'])."',".$_SESSION['c_e'].",".$data['i_d'].",null,$prio,'".$data['e_o']."','".$data['a_n']."','".$data['c_s']."' ";
            $query = $this->db->query($Sql);
            $row = $query->row_array(); 
           
            if((int)$row['co_error'] === 0){
                return 'OK|'.$data['i_d']."|".$data['t_s']."|".$row['cod_sol']."|".$row['fec_sol'];
            } else {
                return $row['tx_error'];
            }
            
        }
        
    }
    
    function retCodSolict($data){
        if (!empty($data)){
            $Sql = " helpdesk_consulta_cod_solicitud ".$data['c_s'];
            $conn = odbc_connect("MASTER-P", "pica", "");
            $resultado = odbc_exec($conn,$Sql);
            $array_rec_1 = $array_rec_2 = null;
            $i = 0;
            while( $row = odbc_fetch_array($resultado) ) {
                $i++;
                $array_rec_1[] = array(
                    'co_solicitud'=>$row['co_solicitud'],                    
                    'co_tipo'=>$row['co_tipo'],                    
                    'tx_titulo'=>utf8_encode($row['tx_titulo']),
                    'tx_descripcion_detallada'=>utf8_encode($row['tx_descripcion_detallada']),
                    'co_empleado'=>$row['co_empleado'],
                    'fe_ingreso'=>date('d/m/Y h:i',strtotime($row['fe_ingreso'])),
                    'fe_tope'=>trim($row['fe_tope']) <> ''?date('d/m/Y',strtotime($row['fe_tope'])):'',
                    'co_departamento'=>$row['co_departamento'],
                    'co_estado'=>$row['co_estado'],
                    'prioridad_usuario'=>trim($row['prioridad_usuario']),
                    'icon'=>$row['icon'],
                    'nomb_empl_ing'=>utf8_encode($row['nomb_empl_ing']),
                    'nomb_ccosto_ing'=>utf8_encode($row['nomb_ccosto_ing'])
                );
            } 
            
            if ($i >=1){

                odbc_next_result($resultado);

                if (!empty($resultado)){

                    while($row = odbc_fetch_array($resultado)) {
                        $array_rec_2[] = array(
                            'des_area'=>trim(utf8_encode($row['tx_descripcion'])),
                            'nom_empl'=>trim(utf8_encode($row['nomb_empl'])),
                            'val_pro'=>(int)trim($row['va_progreso'])
                        );
                    }

                }
            }
               
            return json_encode(array('sol'=>$array_rec_1,'asi'=>$array_rec_2));
        }
    }
    
    function retCodSolictComent($data){
        
        if (!empty($data)){
            
            $Sql = " helpdesk_consulta_comentarios ".$data['p_p'].",'".$data['c_s']."' ";
            $resultado = $this->db->query($Sql);
            if (count($resultado->result_array())> 0){
                $array = null;
                
                foreach ($resultado->result_array() as $row) {
                    $site = 'img/fotos/'.trim($row['co_usuario']).'.JPG';
                    if (file_exists($site)) {
                        $site = site_url('img/fotos/'.trim($row['co_usuario']).'.JPG');
                    } else {
                        $site = site_url('img/fotos/user.png');
                    }
                                            
                    $array[] = array(
                        'co_solicitud'=>$row['co_solicitud'],
                        'tx_titulo'=>utf8_encode($row['tx_titulo']),
                        'co_tipo'=>$row['co_tipo'],
                        'fe_comentario'=>date('Y/m/d h:i',strtotime($row['fe_comentario'])),
                        'tx_descripcion'=>utf8_encode($row['tx_descripcion']),
                        'co_usuario'=>$row['co_usuario'],
                        'apellidos'=> utf8_encode(ucwords(strtolower($row['apellidos']))),
                        'nombres'=> utf8_encode(ucwords(strtolower($row['nombres']))),
                        'fl_responder'=>$row['fl_responder'],
                        'co_estado'=>$row['co_estado'],
                        'img' =>$site
                    );
                }
                
                return json_encode($array);
            }
            
        }
    }
    
    function guardarComent($data){
        if (!empty($data)){
            $Sql = " helpdesk_ingreso_comentario ".trim($data['c_s']).",'".trim($data['m_g'])."',".$_SESSION['c_e'].",1,".trim($data['f_p'])." ";
            $query = $this->db->query($Sql);
            $row = $query->row_array(); 
           
            if((int)$row['co_error'] === 0){
                echo 'OK';
            } else {
                echo $row['tx_error'];
            }
        }
    }
    
    function getMailDpto($dpto,$tip_sol){
        if (!empty($dpto) && !empty($tip_sol)){
            
            $Sql = " helpdesk_consulta_mail ".$dpto.",'".$tip_sol."' ";
            $result = $this->db->query($Sql);
           
            $array = null;

            foreach ($result->result_array() as $row) {

                $array[] = array(
                    'cod_empl'=>$row['co_empleado'],
                    'nom_empl'=>utf8_encode($row['nomb_compl']),
                    'nom_area'=>utf8_encode($row['nomb_area']),
                    'mail'=>utf8_encode($row['e_mail'])
                );

            }    

            return $array;
        }
    }
    
    function getMailAsignado($cod_sol,$tip_sol,$dpto){
        if (!empty($dpto) && !empty($tip_sol)){
            
            $Sql = " helpdesk_consulta_mail_asignado ".$cod_sol.",'".$tip_sol."',".$dpto;
            $result = $this->db->query($Sql);
           
            $array = null;

            foreach ($result->result_array() as $row) {

                $array[] = array(
                    'cod_empl'=>$row['co_empleado'],
                    'name'=>utf8_encode($row['nomb_compl']),
                    'mail'=>utf8_encode($row['e_mail'])
                );

            }    

            return $array;
        }
    }
    
    function getMailIngreSolic($cod_sol){
        if (!empty($cod_sol)){
            
            $Sql = " 
            SELECT b.nombres+' '+b.apellidos as nomb_compl,b.e_mail 
            FROM tb_helpdesk_solicitud as a
            INNER JOIN tb_intranet_users as b
            ON a.co_empleado = b.cod_empleado AND b.estado = 1
            WHERE co_solicitud = $cod_sol ";
            
            $result = $this->db->query($Sql);
            $array = null;
            foreach ($result->result_array() as $row) {

                $array[] = array(
                    'name'=>utf8_encode($row['nomb_compl']),
                    'mail'=>utf8_encode($row['e_mail'])
                );

            }    

            return $array;
        }
    }
    
    function getAllSolicit($data){
        if (!empty($data)){
            $cod_soli = !empty($data['c_b'])?",".$data['c_b']:"";
            $Sql = " helpdesk_consulta_solicitud ".$_SESSION['c_e'].",".trim($data['c_d']).",'".trim($data['f_i'])."','".trim($data['f_f'])."'".$cod_soli;
            $result = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL RETORNAR SOLICIT: ".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            } 

            $array = null;

            $row = $result->row_array(); 

            if((int)isset($row['co_error']) <> 0){
                echo $row['tx_error'];
                die();
            }

            foreach ($result->result_array() as $row) {

                $array[] = array(
                    'co_solicitud'=>$row['co_solicitud'],
                    'co_estado'=>$row['co_estado'],
                    'nom_estado'=>$row['nom_estado'],
                    'tx_titulo'=>utf8_encode($row['tx_titulo']),
                    'tx_descripcion_detallada'=>utf8_encode($row['tx_descripcion_detallada']),
                    'fe_ingreso'=>$row['fe_ingreso'],
                    'co_tipo'=>$row['co_tipo'],
                    'tx_descripcion'=>utf8_encode($row['tx_descripcion'])
                );

            }    

            return json_encode($array);


        }
    }
    
    function retEstadoAgrupados($depart){
        if(!empty($depart) && !empty($_SESSION['co_ccosto']) && !empty($_SESSION['co_empresa'])){
            $Sql = " helpdesk_agrupa_estados ".$_SESSION['co_ccosto'].",".$_SESSION['co_empresa'].",$depart";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL RETORNAR ESTADOS: ".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            }
            return $resultado->result_array();
        }
    }
    
    function solicitudSelect($depart){
        if(!empty($depart)){
            $Sql = " helpdesk_solicitud_dpto $depart ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL RETORNAR TIPO SOLICITUD: ".$this->db->_error_number()." ".$this->db->_error_message();
                die;
            }
            return $resultado->result_array();
        }
    }

    function cancelSolicit($data){
        if(!empty($data)){
            
            $Sql = " helpdesk_actualiza_solicitud ".trim($data['c_s']).",".trim($_SESSION['c_e']).",'C' ";
            
            $query = $this->db->query($Sql);
            $row = $query->row_array(); 
           
            if((int)$row['co_error'] === 0){
                return 'OK';
            } else {
                return $row['tx_error'];
            }
            
        }
    }
    
    function finSolicit($data){
        if(!empty($data)){
            $Sql = " helpdesk_actualiza_solicitud ".trim($data['c_s']).",".trim($_SESSION['c_e']).",'F' ";
            $query = $this->db->query($Sql);
            $row = $query->row_array(); 
           
            if((int)$row['co_error'] === 0){
                return 'OK';
            } else {
                return $row['tx_error'];
            }
        }
    }
    
}
?>
