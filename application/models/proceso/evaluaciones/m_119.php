<?php

class m_119 extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retNodes($node){
        
        $array = null;

            if(!empty($node) || $node == 0){

                $Sql  = " SELECT cod_orga,des_orga,posicion,padre ";
                $Sql .= " FROM tb_evalu_organigrama ";
                $Sql .= " WHERE padre = ".$node;
                $Sql .= " AND estado = 'A' ";
                $Sql .= " ORDER BY posicion; ";
                
                $res = $this->db->query($Sql);
                if ($this->db->_error_message()){
                    $error =& load_class('Exceptions', 'core');
                    echo $error->show_error("ERROR AL ETORNAR NODO: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                    die;
                } 

                foreach ($res->result_array() as $row){

                $array[] = array(
                    'cod_orga'=>$row['cod_orga'],
                    'des_orga'=>utf8_encode($row['des_orga']),
                    'posicion'=>$row['posicion'],
                    'padre'=>$row['padre']
                );

            }    
        
        }
        
        return $array;

    }
    
    function viewEmple($node){
        
        $array = '<div class="title_emple">';

        if(!empty($node) || $node == 0){

            $Sql  = " select b.co_empl,c.apellidos,c.nombres ";
            $Sql .= " from tb_evalu_organigrama a ";
            $Sql .= " left outer join tb_evalu_organigrama_detalle b ";
            $Sql .= " on a.cod_orga = b.co_orga ";
            $Sql .= " inner join tb_intranet_users c on b.co_empl is not null and b.co_empl = c.cod_empleado ";
            $Sql .= " where a.cod_orga = $node ";
            $Sql .= " ORDER BY c.apellidos ";
            echo $Sql;
            die;
            $res = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL ETORNAR EMPLEADOS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } 
            if (count($res->result_array()) > 0){
                foreach ($res->result_array() as $row){
                    $array .= '- '.utf8_encode($row['nombres']." ".$row['apellidos'])."<br>";
                }  
            }  

        }
        
        $array .='</div>';
        
        return $array;

    }
    
    function retParent($node){
        
        $array = null;

            if(!empty($node)){

                $Sql  = " SELECT cod_orga,des_orga,posicion,padre ";
                $Sql .= " FROM tb_evalu_organigrama ";
                $Sql .= " WHERE cod_orga = ".$node;
                $Sql .= " AND estado = 'A'; ";
                
                $res = $this->db->query($Sql);
                if ($this->db->_error_message()){
                    $error =& load_class('Exceptions', 'core');
                    echo $error->show_error("ERROR AL RETORNAR PADRES: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                    die;
                } 

                foreach ($res->result_array() as $row){

                $array[] = array(
                    'cod_orga'=>$row['cod_orga'],
                    'des_orga'=>utf8_encode($row['des_orga']),
                    'posicion'=>$row['posicion'],
                    'padre'=>$row['padre']
                );

            }    
        
        }
        
        return $array;

    }
    
}
?>
