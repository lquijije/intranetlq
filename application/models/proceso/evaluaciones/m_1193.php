<?php

class m_1193 extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retMetodos($id_met = null){
        
        $array = null;
        
        $Sql  = " SELECT co_metodo,ds_metodo,es_metodo ";
        $Sql .= " FROM tb_evalu_metodo ";
        if(!empty($id_met)){
            $Sql .= " WHERE co_metodo =  ".$id_met." ";
        }
        $Sql .= " ORDER BY 1; ";

        $res = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL OBTENER METODOS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        } 
        
        if(count($res->result_array()) > 0){

            foreach ($res->result_array() as $row){

                $array[] = array(
                    'co_met'=>$row['co_metodo'],
                    'ds_met'=>utf8_encode($row['ds_metodo']),
                    'es_met'=>$row['es_metodo'],
                    'ds_est'=>trim($row['es_metodo']) === 'A'?'Activo':'Inactivo'
                );

            } 
            
        }
        
        return $array;

    }
    
    function save_pregunta($data){
        
        $resp = '';
        $bandera = false;
        
        if(!empty($data)){
            
            if(trim($data['_acc']) === 'I'){
                $Sql = " _sp_evalu_metodo '".trim($data['_acc'])."','".trim($data['e_m'])."','".trim(utf8_decode($data['d_m']))."' ";
                $bandera = true;
            } else if(trim($data['_acc']) === 'U'){
                $Sql = " _sp_evalu_metodo '".trim($data['_acc'])."','".trim($data['e_m'])."','".trim(utf8_decode($data['d_m']))."',".$data['c_m']." ";
                $bandera = true;
            }

            if($bandera){
                $query = $this->db->query($Sql);
                if ($this->db->_error_message()){
                    $resp = "ERROR AL GUARDAR METODO: ".$this->db->_error_number()."<BR> ".$this->db->_error_message();
                }   

                $row = $query->row_array();

                if((int)$row['co_error'] == 0){
                    $resp = 'Registro grabado con exito';
                } else {
                    $resp = $row['tx_error'];
                }

            } else {
                $resp = 'Metodo Sin acción';
            }
        } else {
            $resp = 'No se han enviado todos los parametros necesarios';
        }

        return $resp;
    }
    
    
}
?>
