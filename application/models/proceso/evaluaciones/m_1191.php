<?php

class m_1191 extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
            
    function retPuntaje(){
        
        $array = null;
                
        if (!empty($_SESSION['c_e'])){
            
            $Sql  = " SELECT co_punt,ds_punt,valor,es_punt ";
            $Sql .= " FROM tb_evalu_puntuacion ";
            $Sql .= " WHERE es_punt = 'A' ";
            
            $res = $this->db->query($Sql);
            
            if ($this->db->_error_message()){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER PUNTAJE: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } 

            if(count($res->result_array()) > 0){

                foreach ($res->result_array() as $row){

                    $array[] = array(
                        'pu_cod'=>$row['co_punt'],
                        'pu_des'=>utf8_encode($row['ds_punt']),   
                        'pu_val'=>$row['valor'],
                        'pu_est'=>$row['es_punt']
                    );

                } 

            }
            
        }

        return $array;

    }
       
    function retEvaluAsignadas(){
        
        $array = null;
                
        if (!empty($_SESSION['c_e'])){
            
            $Sql  = " SELECT a.co_eval,b.tit_eval,b.fe_ini_eval,b.fe_fin_eval,a.co_estado,c.nombre,c.variable_2 ";
            $Sql .= " FROM tb_evaluacion_asignacion a ";
            $Sql .= " INNER JOIN tb_evaluacion_cab b ";
            $Sql .= " ON a.co_eval = b.co_eval AND b.es_eval = 'A' ";
            $Sql .= " INNER JOIN tb_intranet_parametros c ";
            $Sql .= " ON c.tipo = 7 AND a.co_estado = c.variable_1 ";
            $Sql .= " WHERE a.co_empleado = ".$_SESSION['c_e']." ";
            $Sql .= " AND a.co_estado NOT IN('F') ";
            $Sql .= " AND GETDATE() BETWEEN b.fe_ini_eval AND DATEADD(day,1,b.fe_fin_eval) ";
            
            $res = $this->db->query($Sql);
            
            if ($this->db->_error_message()){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER EVALUACIONES: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } 

            if(count($res->result_array()) > 0){

                foreach ($res->result_array() as $row){

                    $array[] = array(
                        'ev_cod'=>$row['co_eval'],
                        'ev_tit'=>utf8_encode($row['tit_eval']),
                        'ev_fi'=>!empty($row['fe_ini_eval'])?date('Y/m/d',strtotime($row['fe_ini_eval'])):'',
                        'ev_ff'=>!empty($row['fe_fin_eval'])?date('Y/m/d',strtotime($row['fe_fin_eval'])):'',    
                        'ev_es'=>$row['co_estado'],
                        'ev_ed'=>utf8_encode($row['nombre']),
                        'et_cl'=>utf8_encode($row['variable_2']),
                    );

                } 

            }
            
        }

        return $array;

    }
    
    function retEvaluaId($cod_eva){
        
        $arr_eval = null;
        $arr_preg = null;
        $error = '';
        
        if(!empty($cod_eva)){
            
            $Sql  = " SELECT a.co_eval,a.tit_eval,a.sub_tit_eval,a.des_eval,a.co_periodo,a.fe_ini_eval,a.fe_fin_eval,a.es_eval, ";
            $Sql .= " c.co_metodo,d.ds_metodo,b.co_pregunta,c.ds_pregunta ";
            $Sql .= " FROM tb_evaluacion_cab a ";
            $Sql .= " INNER JOIN tb_evaluacion_det b ";
            $Sql .= " ON b.co_eval = a.co_eval ";
            $Sql .= " INNER JOIN tb_evalu_preguntas c ";
            $Sql .= " ON b.co_pregunta = c.co_pregunta AND c.es_pregunta = 'A' ";
            $Sql .= " INNER JOIN tb_evalu_metodo d ";
            $Sql .= " ON c.co_metodo = d.co_metodo AND d.es_metodo = 'A' ";
            $Sql .= " WHERE a.co_eval = $cod_eva ";
            $Sql .= " AND a.es_eval = 'A' ";
            $Sql .= " ORDER BY c.co_metodo ";

            $res = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER EVALUACION: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } 
            
            $i = 0;

            if(count($res->result_array()) > 0){

                foreach ($res->result_array() as $row){
                    if ($i === 0){
                        $arr_eval[] = array(
                            'e_co'=>$row['co_eval'],
                            'e_t1'=>utf8_encode($row['tit_eval']),
                            'e_t2'=>utf8_encode($row['sub_tit_eval']),
                            'e_de'=>utf8_encode($row['des_eval']),
                            'e_pe'=>$row['co_periodo'],
                            'e_fi'=>!empty($row['fe_ini_eval'])?date('Y/m/d',strtotime($row['fe_ini_eval'])):'',
                            'e_ff'=>!empty($row['fe_fin_eval'])?date('Y/m/d',strtotime($row['fe_fin_eval'])):'',
                            'e_es'=>$row['es_eval'],
                            'e_ed'=>trim($row['es_eval']) === 'A'?'Activo':'Inactivo'
                        );
                    }
                    $arr_preg[] = array(
                        'p_cm'=>$row['co_metodo'],
                        'p_dm'=>utf8_encode($row['ds_metodo']),
                        'p_cp'=>$row['co_pregunta'],
                        'p_dp'=>utf8_encode($row['ds_pregunta'])
                    );
                    $i++;
                } 

            }

        }
        
        return array('d_ev'=>$arr_eval,'d_pr'=>$arr_preg,'err'=>$error); 
        
    }
    
    function saveInicial($cod_eva){
        
        $resp = '';
        
        if(!empty($cod_eva) && (isset($_SESSION['c_e']) && !empty($_SESSION['c_e']))){
            
            $Sql = " _sp_evalu_respuestas_ingreso 'A','',".trim($_SESSION['c_e']).",$cod_eva ";
            $query = $this->db->query($Sql);
        
            if ($this->db->_error_message()){
                $resp = "ERROR AL INGRESAR RESPUESTAS: ".$this->db->_error_number()."<BR> ".$this->db->_error_message();
            }   

            $row = $query->row_array();

            if((int)$row['co_error'] == 0){
                $resp = 'Registro grabado con exito';
            } else {
                $resp = $row['tx_error'];
            }

        } else {
            $resp = 'No se han enviado todos los parametros necesarios';
        }

        return $resp;
    }
    
    function saveRespuestas($cod_eva,$det_pre){
        
        $resp = '';
        
        if(!empty($cod_eva) && !empty($det_pre)){
            
            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false; 
            $dom->formatOutput = true; 
            
            ##<preguntas>              
                $pregt = $dom->createElement('preguntas');
                $dom->appendChild($pregt);
                
                foreach ($det_pre as $row){
                    $items = $dom->createElement("items"); 
                    $pregt->appendChild($items);
                    
                    $n_1 = $dom->createElement("pre",trim($row['p'])); 
                    $items->appendChild($n_1);
                    
                    $n_2 = $dom->createElement("res",trim($row['r'])); 
                    $items->appendChild($n_2);

                }

            ##</preguntas>
            
            $xml = $dom->saveXML();
            
            $Sql = " _sp_evalu_respuestas_ingreso 'U','$xml',".trim($_SESSION['c_e']).",$cod_eva ";
            $query = $this->db->query($Sql);
        
            if ($this->db->_error_message()){
                $resp = "ERROR AL INGRESAR RESPUESTAS: ".$this->db->_error_number()."<BR> ".$this->db->_error_message();
            }   

            $row = $query->row_array();

            if((int)$row['co_error'] == 0){
                $resp = 'Registro grabado con exito';
            } else {
                $resp = $row['tx_error'];
            }

        } else {
            $resp = 'No se han enviado todos los parametros necesarios';
        }

        return $resp;
    }
    
    function getRespuestas($cod_eva,$cod_usr){
        
        $array = null;
        
        if (!empty($cod_eva) && !empty($cod_usr)){
                
            $Sql  = " SELECT b.co_pregunta,b.co_respuesta ";
            $Sql .= " FROM tb_evaluacion_asignacion a ";
            $Sql .= " INNER JOIN tb_evaluacion_respuestas b ";
            $Sql .= " ON a.co_eval = b.co_eval AND co_respuesta <> 0 "; 
            $Sql .= " WHERE a.co_empleado = $cod_usr ";
            $Sql .= " AND a.co_eval = $cod_eva ";
            $Sql .= " ORDER BY b.co_pregunta ";
            
            $res = $this->db->query($Sql);
            
            if ($this->db->_error_message()){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER RESPUESTAS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } 

            if(count($res->result_array()) > 0){

                foreach ($res->result_array() as $row){

                    $array[] = array(
                        'c_p'=>$row['co_pregunta'],
                        'c_r'=>$row['co_respuesta']
                    );

                } 

            }

        }
        
        return array('data'=>$array);
        
    }
}
?>
