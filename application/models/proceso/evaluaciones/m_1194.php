<?php

class m_1194 extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retEvaluaId($cod_eva){
        
        $arr_eval = null;
        $arr_preg = null;
        $error = '';
        
        if(!empty($cod_eva)){
           
            $Sql  = " SELECT a.co_eval,a.tit_eval,a.sub_tit_eval,a.des_eval,a.co_periodo,a.fe_ini_eval,a.fe_fin_eval,a.es_eval, ";
            $Sql .= " c.co_metodo,d.ds_metodo,b.co_pregunta,c.ds_pregunta ";
            $Sql .= " FROM tb_evaluacion_cab a ";
            $Sql .= " INNER JOIN tb_evaluacion_det b ";
            $Sql .= " ON b.co_eval = a.co_eval ";
            $Sql .= " INNER JOIN tb_evalu_preguntas c ";
            $Sql .= " ON b.co_pregunta = c.co_pregunta AND c.es_pregunta = 'A' ";
            $Sql .= " INNER JOIN tb_evalu_metodo d ";
            $Sql .= " ON c.co_metodo = d.co_metodo AND d.es_metodo = 'A' ";
            $Sql .= " WHERE a.co_eval = $cod_eva ";
            $Sql .= " ORDER BY c.co_metodo ";

            $res = $this->db->query($Sql);
            if ($this->db->_error_message()){
                $error = "ERROR AL OBTENER EVALUACIONES: ".$this->db->_error_number()." <br> ".$this->db->_error_message();
            } 
            
            $i = 0;

            if(count($res->result_array()) > 0){

                foreach ($res->result_array() as $row){
                    if ($i === 0){
                        $arr_eval[] = array(
                            'e_co'=>$row['co_eval'],
                            'e_t1'=>utf8_encode($row['tit_eval']),
                            'e_t2'=>utf8_encode($row['sub_tit_eval']),
                            'e_de'=>utf8_encode($row['des_eval']),
                            'e_pe'=>$row['co_periodo'],
                            'e_fi'=>!empty($row['fe_ini_eval'])?date('Y/m/d',strtotime($row['fe_ini_eval'])):'',
                            'e_ff'=>!empty($row['fe_fin_eval'])?date('Y/m/d',strtotime($row['fe_fin_eval'])):'',
                            'e_es'=>$row['es_eval'],
                            'e_ed'=>trim($row['es_eval']) === 'A'?'Activo':'Inactivo'
                        );
                    }
                    $arr_preg[] = array(
                        'p_cm'=>$row['co_metodo'],
                        'p_dm'=>utf8_encode($row['ds_metodo']),
                        'p_cp'=>$row['co_pregunta'],
                        'p_dp'=>utf8_encode($row['ds_pregunta'])
                    );
                    $i++;
                } 

            }

        }
        
        return array('d_ev'=>$arr_eval,'d_pr'=>$arr_preg,'err'=>$error); 
        
    }
    
    function retEvalua(){
        
        $array = null;
        
        $Sql  = " SELECT co_eval,tit_eval,fe_ini_eval,fe_fin_eval,es_eval ";
        $Sql .= " FROM tb_evaluacion_cab ";
        $Sql .= " ORDER BY tit_eval ";
                
        $res = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL OBTENER EVALUACIONES: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        } 
        
        if(count($res->result_array()) > 0){

            foreach ($res->result_array() as $row){

                $array[] = array(
                    'ev_cod'=>$row['co_eval'],
                    'ev_tit'=>utf8_encode($row['tit_eval']),
                    'ev_f_i'=>!empty($row['fe_ini_eval'])?date('Y/m/d',strtotime($row['fe_ini_eval'])):'',
                    'ev_f_f'=>!empty($row['fe_fin_eval'])?date('Y/m/d',strtotime($row['fe_fin_eval'])):'',
                    'ev_e_d'=>$row['es_eval'],
                    'ev_e_c'=>trim($row['es_eval']) === 'A'?'Activo':'Inactivo'
                );

            } 
            
        }
        
        return $array;

    }
    
    function retMetodos(){
        
        $array = null;
        
        $Sql  = " SELECT co_metodo,ds_metodo ";
        $Sql .= " FROM tb_evalu_metodo ";
        $Sql .= " WHERE es_metodo = 'A' ";
        $Sql .= " ORDER BY 2; ";

        $res = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL OBTENER METODOS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        } 
        
        if(count($res->result_array()) > 0){

            foreach ($res->result_array() as $row){

                $array[] = array(
                    'cod_met'=>$row['co_metodo'],
                    'des_met'=>utf8_encode($row['ds_metodo'])
                );

            } 
            
        }
        
        return $array;

    }
    
    function retPreguntas($id_met){
       
        $array = null;
        $error = '';
        
        $Sql  = " SELECT co_pregunta,co_metodo, ";
        $Sql .= " ds_pregunta,es_pregunta ";
        $Sql .= " FROM tb_evalu_preguntas ";
        $Sql .= " WHERE co_metodo = ".$id_met.";";

        $res = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $error = "ERROR AL OBTENER PREGUNTAS POR METODO: ".$this->db->_error_number()."<br>".$this->db->_error_message();
        } 
        
        if(count($res->result_array()) > 0){

            foreach ($res->result_array() as $row){

                $array[] = array(
                    'co_pre'=>$row['co_pregunta'],
                    'ds_pre'=>utf8_encode($row['ds_pregunta']),
                    'co_met'=>$row['co_metodo'],
                    'es_pre'=>trim($row['es_pregunta']),
                    'ds_est'=>trim($row['es_pregunta']) === 'A'?'ACTIVO':'INACTIVO'
                );

            } 
            
        } else {
            $error = 'No hay datos en la busqueda realizada';
        }
        
        return array('data'=>$array,'err'=>$error);

    }
    
    function save_evaluacion($data){
        
        $resp = '';
        $bandera = false;
        
        if(!empty($data)){
            
            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false; 
            $dom->formatOutput = true; 

            ##<cabecera> 
            
                $array_preg = explode(',',$data['a_p']);
                
                $cabecera = $dom->createElement('cabecera');
                $dom->appendChild($cabecera);

                $n_1 = $dom->createElement("tit_1",trim($data['et_1'])); 
                $cabecera->appendChild($n_1);
                
                $n_2 = $dom->createElement("tit_2",trim($data['et_2'])); 
                $cabecera->appendChild($n_2);
                
                $n_3 = $dom->createElement("descr",trim($data['e_d'])); 
                $cabecera->appendChild($n_3);
                
                $n_4 = $dom->createElement("estad",trim($data['e_e'])); 
                $cabecera->appendChild($n_4);
                
                $n_5 = $dom->createElement("perio",trim($data['e_p'])); 
                $cabecera->appendChild($n_5);
                
                $n_6 = $dom->createElement("fe_ini",trim($data['f_i'])); 
                $cabecera->appendChild($n_6);
                
                $n_7 = $dom->createElement("fe_fin",trim($data['f_f'])); 
                $cabecera->appendChild($n_7);
                  
            ##</cabecera> 
            
            ##<preguntas>              
                $pregt = $dom->createElement('preguntas');
                $dom->appendChild($pregt);
                
                for ($i = 0; $i < count($array_preg); $i++){

                    $items = $dom->createElement("items"); 
                    $pregt->appendChild($items);
                    
                    $n_8 = $dom->createElement("id_pre",trim($array_preg[$i])); 
                    $items->appendChild($n_8);

                }

            ##</preguntas>
            
            $xml = $dom->saveXML();
            
            if(trim($data['_acc']) === 'I'){
                $Sql = " _sp_evalu_evaluacion '".trim($data['_acc'])."','".trim($xml)."',".trim($_SESSION['c_e'])." ";
                $bandera = true;
            } else if(trim($data['_acc']) === 'U'){
                $Sql = " _sp_evalu_evaluacion '".trim($data['_acc'])."','".trim($xml)."',".trim($_SESSION['c_e']).",".trim($data['e_c'])." ";
                $bandera = true;
            }

            if($bandera){
                $query = $this->db->query($Sql);
                if ($this->db->_error_message()){
                    $resp = "ERROR AL GUARDAR EVALUACION: ".$this->db->_error_number()."<BR> ".$this->db->_error_message();
                }   

                $row = $query->row_array();

                if((int)$row['co_error'] == 0){
                    $resp = 'Registro grabado con exito';
                } else {
                    $resp = $row['tx_error'];
                }

            } else {
                $resp = 'Metodo Sin acción';
            }
        } else {
            $resp = 'No se han enviado todos los parametros necesarios';
        }

        return $resp;
    }
    
    function retTreview(){
        
        $array = null;
        
        $Sql  = " SELECT cod_orga,des_orga,padre ";
        $Sql .= " FROM tb_evalu_organigrama ";
        $Sql .= " ORDER BY 1; ";
            
        $res = $this->db->query($Sql);
        if ($this->db->_error_message()){
//            $error =& load_class('Exceptions', 'core');
//            echo $error->show_error("ERROR AL OBTENER METODOS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
//            die;
        } 
        
        if(count($res->result_array()) > 0){

            foreach ($res->result_array() as $row){

                $array[] = array(
                    'id'=>(int)$row['cod_orga'],
                    'parent'=>(int)$row['padre'] === 0?'#':$row['padre'],
                    'text'=>utf8_encode($row['des_orga']),
                    'icon' => (int)$row['padre'] === 1?site_url('img/icon-user.png'):''
                );

            } 
            
        }
        
        if (!empty($array)){
            echo json_encode($array);  
        }
    }
    
}
?>
