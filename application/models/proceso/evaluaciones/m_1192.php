<?php

class m_1192 extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retMetodos(){
        
        $array = null;
        
        $Sql  = " SELECT co_metodo,ds_metodo ";
        $Sql .= " FROM tb_evalu_metodo ";
        $Sql .= " WHERE es_metodo = 'A' ";
        $Sql .= " ORDER BY 2; ";

        $res = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL OBTENER METODOS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        } 
        
        if(count($res->result_array()) > 0){

            foreach ($res->result_array() as $row){

                $array[] = array(
                    'cod_met'=>$row['co_metodo'],
                    'des_met'=>utf8_encode($row['ds_metodo'])
                );

            } 
            
        }
        
        return $array;

    }
    
    function retPreguntas($id_pre,$id_met = null){
        
        $array = null;
        
        $Sql  = " SELECT a.co_pregunta,a.co_metodo,b.ds_metodo, ";
        $Sql .= " a.ds_pregunta,a.es_pregunta ";
        $Sql .= " FROM tb_evalu_preguntas a ";
        $Sql .= " INNER JOIN tb_evalu_metodo b ";
        $Sql .= " ON a.co_metodo = b.co_metodo ";
        $Sql .= " AND b.es_metodo = 'A' ";
        
        if(!empty($id_pre)){
            $Sql .= " WHERE a.co_pregunta =  ".$id_pre.";";
        }
        if(!empty($id_met)){
            $Sql .= " WHERE a.co_metodo =  ".$id_met.";";
        }

        $res = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL OBTENER PREGUNTAS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        } 
        
        if(count($res->result_array()) > 0){

            foreach ($res->result_array() as $row){

                $array[] = array(
                    'co_pre'=>$row['co_pregunta'],
                    'ds_pre'=>utf8_encode($row['ds_pregunta']),
                    'co_met'=>$row['co_metodo'],
                    'ds_met'=>utf8_encode($row['ds_metodo']),
                    'es_pre'=>trim($row['es_pregunta']),
                    'ds_est'=>trim($row['es_pregunta']) === 'A'?'ACTIVO':'INACTIVO'
                );

            } 
            
        }
        
        return $array;

    }
    
    function save_pregunta($data){
        
        $resp = '';
        $bandera = false;
        
        if(!empty($data)){
            
            if(trim($data['_acc']) === 'I'){
                $Sql = " _sp_evalu_pregunta '".trim($data['_acc'])."',".trim($data['c_m']).",'".trim($data['e_p'])."','".trim(utf8_decode($data['d_p']))."' ";
                $bandera = true;
            } else if(trim($data['_acc']) === 'U'){
                $Sql = " _sp_evalu_pregunta '".trim($data['_acc'])."',".trim($data['c_m']).",'".trim($data['e_p'])."','".trim(utf8_decode($data['d_p']))."',".$data['c_p']." ";
                $bandera = true;
            }

            if($bandera){
                $query = $this->db->query($Sql);
                if ($this->db->_error_message()){
                    $resp = "ERROR AL GUARDAR PREGUNTA: ".$this->db->_error_number()."<BR> ".$this->db->_error_message();
                }   

                $row = $query->row_array();

                if((int)$row['co_error'] == 0){
                    $resp = 'Registro grabado con exito';
                } else {
                    $resp = $row['tx_error'];
                }

            } else {
                $resp = 'Metodo Sin acción';
            }
        } else {
            $resp = 'No se han enviado todos los parametros necesarios';
        }

        return $resp;
    }
    
    function retTreview(){
        
        $array = null;
        
        $Sql  = " SELECT cod_orga,des_orga,padre ";
        $Sql .= " FROM tb_evalu_organigrama ";
        $Sql .= " ORDER BY 1; ";
            
        $res = $this->db->query($Sql);
        if ($this->db->_error_message()){
//            $error =& load_class('Exceptions', 'core');
//            echo $error->show_error("ERROR AL OBTENER METODOS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
//            die;
        } 
        
        if(count($res->result_array()) > 0){

            foreach ($res->result_array() as $row){

                $array[] = array(
                    'id'=>(int)$row['cod_orga'],
                    'parent'=>(int)$row['padre'] === 0?'#':$row['padre'],
                    'text'=>utf8_encode($row['des_orga']),
                    'icon' => (int)$row['padre'] === 1?site_url('img/icon-user.png'):''
                );

            } 
            
        }
        
        if (!empty($array)){
            echo json_encode($array);  
        }
    }
    
}
?>
