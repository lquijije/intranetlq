<?php

class m_122 extends CI_Model{
    
    var $arrayConnect = array(
        'hostname' => '',
        'username' => "pos",
        'password' => "siste2365",
        'database' => "podbpos"
    );
    
    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->load->library(array('connmysql'));
        $this->db->flush_cache();
    }
    
    function retAtributosArticulo($cod_art){
        
        $array = null;
        
        if(!empty($cod_art)){
            
            $Sql  = " SELECT b.codigo,b.descripcion, atributo = REPLACE(a.valor_atributo, '\', '') ";
            $Sql .= " FROM IVDBINVENTAR..IVTB_FICHA_TECNICA a ";
            $Sql .= " INNER JOIN IVDBINVENTAR..IVTB_ATRIBUTOS b on a.cod_atributo = b.codigo ";
            $Sql .= " WHERE cod_articulo = '".trim($cod_art)."' ";
            $Sql .= " AND a.valor_atributo <>'' ";
            //$Sql .= " AND a.COD_ATRIBUTO IN(4,6) ";
            $Sql .= " ORDER BY a.COD_ATRIBUTO ";
            //echo $Sql;
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR ATRIBUTO <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
            }
            
            if(count($result->result_array())){
                
                foreach($result->result_array() as $row){

                    $array[] = array(
                        'co_atr'=> (int)$row['codigo'],
                        'ds_atr'=> trim(utf8_encode($row['descripcion'])),
                        'va_atr'=> trim(utf8_encode($row['atributo']))
                    );
                
                }
                
            }
            
        }
        
        return $array;
        
    }
    
    function retFacturas($data,$cnn){
        
        $this->arrayConnect['hostname'] = $cnn;
        $config = $this->arrayConnect;
        
        $db = new connmysql();
        $db->host = $config['hostname'];
        $db->user = $config['username'];
        $db->pass = $config['password'];
        $db->db = $config['database'];
        
        $db->connectMysqli();
        
        $array = null;
        
        if(!empty($cnn) && !empty($data)){
            
            $Sql = " CALL podbpos._get_Facturas (NULL,NULL,NULL,'".trim($data['c_i'])."',".trim($data['a_o']).",".trim($data['m_s']).")";
            
            $resultado = $db->cnn->query($Sql);
            
            if ($db->cnn->error <> '') {
                return "ERROR AL CONSULTAR FACTURAS <BR>ERROR: ".$db->cnn->errno." - ".$db->cnn->error;
                die;
            }
            
            if ($resultado->num_rows > 0){

                while($row = $resultado->fetch_array()){
                    $array[] = array(
                        'fec_doc'=> date('Y/m/d',strtotime(trim($row['fe_transaccion']))),
                        'cod_bod'=> trim($row['co_bodega']),
                        'des_bod'=> utf8_encode(trim($row['de_bodega'])),
                        'tip_doc'=> trim($row['ti_documento']),
                        'tip_nom'=> trim($row['ti_documento']) === 'F'?'Factura':'Nota de Credito',
                        'num_sri'=> str_pad(trim($row['de_sri']),3,0, STR_PAD_LEFT),
                        'num_caj'=> str_pad(trim($row['co_caja']),3,0, STR_PAD_LEFT),
                        'num_doc'=> str_pad(trim($row['nu_documento']),9,0, STR_PAD_LEFT),
                        'val_doc'=> number_format($row['va_totaltransaccion'],2)
                    );
                }
                
            }
            
        }
        
        $db->closeMysqli();
           
        return array('data'=>$array);
        
    }
    
    function retDetFacturas($data,$cnn){
        //$cnn = '130.1.45.49';
        error_reporting(0);
        $array_rec_1 = $array_rec_2 = $array_rec_3 = $array_rec_4 = $array_rec_5 =  null;
        $bandera = false;
        $error = '';
        
        $this->arrayConnect['hostname'] = $cnn;
        $config = $this->arrayConnect;
        
        $db = new connmysql();
        $db->host = $config['hostname'];
        $db->user = $config['username'];
        $db->pass = $config['password'];
        $db->db = $config['database'];
        $db->connectPDO();
        
        if(!empty($data) && !empty($cnn)){
            
            if(empty($db->err)){
                
                try {
                
                    $stmt = $db->cnn->prepare('CALL podbpos._get_Facturas(:param1, :param2, :param3, :param4, :param5, :param6)');
                    $execute = $stmt->execute(array(
                        ':param1' => (int)$data['c_i'], 
                        ':param2' => (int)$data['c_c'], 
                        ':param3' => (int)$data['c_d'], 
                        ':param4' => NULL, 
                        ':param5' => NULL,
                        ':param6' => NULL
                    ));

                    if (!$execute) {
                        throw new PDOException ("Error");
                    }
                    
                } catch (PDOException $e) {
                    $error = "ERROR AL CONSULTAR DETALLE FACTURA EXECUTE: ";
                }

                while ($row = $stmt->fetch()) {
                    $array_rec_2[] = array(
                        'fec_fac'=> trim($row['fe_transaccion']),
                        'ide_cli'=> trim($row['co_cliente']),
                        'cod_sec'=> trim($row['co_secuencia']),
                        'cod_art'=> trim($row['co_articulo']),
                        'des_art'=> utf8_encode(trim($row['de_articulo'])),
                        'cod_art_refe'=> trim(utf8_encode($row['co_averiadoarticulo'])),
                        'cod_user'=> trim(utf8_encode($row['de_sinreferenciadescripcion'])),
                        'can_art'=> (int)trim($row['va_cantidad']),
                        'val_art'=> (float)trim($row['va_pvpventa'])
                    );
                }

                $stmt->nextRowset();

                while ($row = $stmt->fetch()) {
                    $array_rec_2[] = array(
                        'fec_fac'=> "",
                        'ide_cli'=> "",
                        'cod_sec'=> 999,
                        'cod_art'=> trim($row['co_articulo']),
                        'des_art'=> utf8_encode(trim($row['de_articulo'])),
                        'cod_art_refe'=> trim(utf8_encode($row['co_averiadoarticulo'])),
                        'cod_user'=> trim(utf8_encode($row['de_sinreferenciadescripcion'])),
                        'can_art'=> (int)trim($row['va_cantidad']),
                        'val_art'=> (float)trim($row['va_pvpventa'])
                    );
                }
            } else {
                $error = $db->err;
            }
            
            $db->closePDO();
            
            if (!empty($array_rec_2)){
                
                $array_rec_1 = $this->retCliente($array_rec_2[0]['ide_cli']);
                
                $dom = new DOMDocument("1.0");
                $dom->preserveWhiteSpace = false; 
                $dom->formatOutput = true; 
                
                ##<factura>

                    $cabecera = $dom->createElement('factura');
                    $dom->appendChild($cabecera);

                    $n_1 = $dom->createElement("co_bod",(int)$data['c_i']); 
                    $cabecera->appendChild($n_1);
                    
                    $n_2 = $dom->createElement("co_caj",(int)$data['c_c']); 
                    $cabecera->appendChild($n_2);

                    $n_3 = $dom->createElement("nu_doc",(int)$data['c_d']); 
                    $cabecera->appendChild($n_3);
                    
                ##</factura>
                
                ##<articulos>
                    
                    $plan_pago = $dom->createElement('articulos');
                    $dom->appendChild($plan_pago);
                    
                    foreach($array_rec_2 as $row){

                        $items = $dom->createElement("items"); 
                        $plan_pago->appendChild($items);
                        
                        $n_4 = $dom->createElement("cod_art",$row['cod_art']); 
                        $items->appendChild($n_4);
                        
                        $n_5 = $dom->createElement("cod_sec",$row['cod_sec']); 
                        $items->appendChild($n_5);
                        
                        $n_6 = $dom->createElement("cod_art_refe",$row['cod_art_refe']); 
                        $items->appendChild($n_6);
                        
                        $n_7 = $dom->createElement("cod_user",$row['cod_user']); 
                        $items->appendChild($n_7);
                        
                        $n_8 = $dom->createElement("can_art",$row['can_art']); 
                        $items->appendChild($n_8);
                        
                        $n_9 = $dom->createElement("val_art",$row['val_art']); 
                        $items->appendChild($n_9);
                        
                    }
                    
                ##</articulos>
                    
                $xml = $dom->saveXML();
                //echo $xml;
                //die;
                $Sql = " PODBPOS_CIERRES.._garantiaExtRetFac2 '".$xml."'; ";
//                echo $Sql;
//                die;
                //$this->db = $this->load->database('desarrollo', true);
                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> ''){
                    $error = "ERROR AL CONSULTAR DETALLES DOCUMENTO <BR>".$this->db->_error_number()." ".$this->db->_error_message();
                } else {
                    if (count($result->result_array())> 0){
                        
                        $arrResult = $result->result_array();
                        
                        for($i=0; $i < count($arrResult); $i++){
                            
                            $array_rec_4[$i] = array(
                                'cod_gru'=> (int)trim($arrResult[$i]['co_grupo']),
                                'cod_lin'=> (int)trim($arrResult[$i]['co_linea']),
                                'tim_pro'=> (int)trim($arrResult[$i]['ge_prov_hasta']),
                                'cod_sec'=> trim($arrResult[$i]['co_secuencia']),
                                'cod_art'=> trim($arrResult[$i]['co_articulo']),
                                'des_art'=> utf8_encode(trim($arrResult[$i]['ds_articulo'])),
                                'cod_art_ref'=> trim($arrResult[$i]['co_averiadoarticulo']),
                                'cod_user_ref'=> $this->retColaborador(trim($arrResult[$i]['de_sinreferenciadescripcion'])),
                                'can_art'=> (int)trim($arrResult[$i]['va_cantidad']),
                                'val_art'=> (float)trim($arrResult[$i]['va_pvpventa']),
                                'co_cont'=> (int)trim($arrResult[$i]['co_contrato']),
                                'mod_art'=> '',
                                'mar_art'=> ''
                            );
                            
                            $atributos = $this->retAtributosArticulo(trim($arrResult[$i]['co_articulo']));
                            
                            if(!empty($atributos)){
                                foreach ($atributos as $ro){
                                    if((int)$ro['co_atr'] == 4){
                                        $array_rec_4[$i]['mar_art'] = trim(strtoupper($ro['va_atr']));
                                    } else if((int)$ro['co_atr'] == 6){
                                        $array_rec_4[$i]['mod_art']=  trim($ro['va_atr']);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //print_r($array_rec_4);
        return array('dt_1'=>$array_rec_1,'dt_2'=>$array_rec_4,'fe_f'=>$array_rec_2[0]['fec_fac'],'err'=>$error);
    }
    
    function retColaborador($array){
        $array = explode('|', $array);
        $colaborador = 0;
        if (!empty($array)){
            $search = 'Vendedor';
            foreach($array as $k=>$v) {
                if(preg_match("/\b$search\b/i", $v)) {
                    $usrArr = explode(':', $v);
                    $colaborador = (int)$usrArr[1];
                }
            }
        }
        return $colaborador;
    }
    
    function saveGarantia($data){
//        print_r($data);
//        die;
        $array = $array1 = null;
        
        if(!empty($data['id']) && !empty($data['no_']) && !empty($data['di_']) && !empty($data['te_'])){
            
            $Sql1 = " SGDBSEGURIDAD..Registrar_Datos_Cliente '".trim($data['id'])."',1,'".strtoupper(trim(utf8_decode($data['no_'])))."',29629,'".strtoupper(trim(utf8_decode($data['di_'])))."','".trim($data['te_'])."','".strtoupper(trim($data['ma_il']))."'; ";

            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql1);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL INGRESAR CLIENTE: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            $raw = $result->row_array();
            
            if((int)$raw['cod_error'] == 0){

                if(!empty($data['cb_']) && !empty($data['cj_']) && !empty($data['nd_'])){

                    $dom = new DOMDocument("1.0");
                    $dom->preserveWhiteSpace = false; 
                    $dom->formatOutput = true; 

                    ##<garantia>
                        $data['ma_el'] = str_replace(array("&","'",','),array('','',''),$data['ma_el']);
                        $data['mo_el'] = str_replace(array("&","'",','),array('','',''),$data['mo_el']);
                        $data['nu_se'] = str_replace(array("&","'",','),array('','',''),$data['nu_se']);
                        
                        $cabecera = $dom->createElement('garantia');
                        $dom->appendChild($cabecera);

                        $n_1 = $dom->createElement("ide_cli",trim($data['id'])); 
                        $cabecera->appendChild($n_1);

                        $n_2 = $dom->createElement("mail_cli",trim(utf8_decode($data['ma_il']))); 
                        $cabecera->appendChild($n_2);

                        $n_3 = $dom->createElement("co_bod",(int)$data['cb_']); 
                        $cabecera->appendChild($n_3);

                        $n_4 = $dom->createElement("co_caj",(int)$data['cj_']); 
                        $cabecera->appendChild($n_4);

                        $n_5 = $dom->createElement("nu_doc",(int)$data['nd_']); 
                        $cabecera->appendChild($n_5);

                        $n_6 = $dom->createElement("cod_art",trim($data['co_ap'])); 
                        $cabecera->appendChild($n_6);

                        $n_7 = $dom->createElement("time_art",(int)$data['ti_pr']); 
                        $cabecera->appendChild($n_7);

                        $n_8 = $dom->createElement("cod_art_gar",trim($data['co_ag'])); 
                        $cabecera->appendChild($n_8);

                        $n_9 = $dom->createElement("time_art_gar",trim($data['ti_ga']));
                        $cabecera->appendChild($n_9);

                        $n_10 = $dom->createElement("art_mar",trim(utf8_decode(strtoupper($data['ma_el'])))); 
                        $cabecera->appendChild($n_10);

                        $n_11 = $dom->createElement("art_mod",trim(utf8_decode(strtoupper($data['mo_el'])))); 
                        $cabecera->appendChild($n_11);

                        $n_12 = $dom->createElement("num_ser",trim(utf8_decode(strtoupper($data['nu_se'])))); 
                        $cabecera->appendChild($n_12);

                        $n_13 = $dom->createElement("pre_art",trim(utf8_decode($data['pr_ap']))); 
                        $cabecera->appendChild($n_13);

                        $n_14 = $dom->createElement("pre_art_gar",trim(utf8_decode($data['pr_ag']))); 
                        $cabecera->appendChild($n_14);

                        $n_15 = $dom->createElement("user_pos",trim(utf8_decode($data['us_pos']))); 
                        $cabecera->appendChild($n_15);

                        $n_16 = $dom->createElement("user_int",$_SESSION['c_e']); 
                        $cabecera->appendChild($n_16);

                        $n_17 = $dom->createElement("fec_fac",$data['fe_fac']); 
                        $cabecera->appendChild($n_17);
                        
                        $n_18 = $dom->createElement("sec_fac",$data['sec_fac']); 
                        $cabecera->appendChild($n_18);

                    ##</garantia>

                    $xml = $dom->saveXML();        

                    $Sql = " webpycca.._garantiaExtIngreso '".$xml."' ";
                    
                    //$this->db = $this->load->database('desarrollo', true);
                    $result = $this->db->query($Sql);
                    if ($this->db->_error_message() <> ''){
                        return "ERROR AL INGRESAR GARANT&Iacute;A: ".$this->db->_error_number()." ".$this->db->_error_message();
                    }
                    $row = $result->row_array();
                    $array = array(
                        'co_err'=>(int)$row['co_error'],
                        'tx_err'=>utf8_decode($row['tx_error']),
                        'id_gar'=>(int)$row['id_gara']
                    );
                }
            } else {
                return utf8_decode($raw['msg_error']);
            }
            
        }
        
        return $array;
        
    }
    
    function retCliente($CI){
        
        $array = null;

        if(!empty($CI)){
            
            $Sql = " SGDBSEGURIDAD.._consulta_cliente '".$CI."' ";
            //$this->db = $this->load->database('desarrollo', true);
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR CLIENTE <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            if (count($resultado->row_array())> 0){
                
                $row = $resultado->row_array();

                $array = array(
                    'ide_cli'=> trim($CI),
                    'nom_cli'=> trim(utf8_encode($row['perNombre1'])),
                    'dir_cli'=> trim(utf8_encode($row['direccion'])),
                    'tel_cli'=> trim($row['telefono']),
                    'mail_cli'=> strtolower(trim($row['correo']))
                );
                
            }
            
        }
        
        return $array;
            
    }
    
    function retGarantia($cod_gar,$tipo,$fec_ini = null,$fec_fin = null){
        
        $Sql = '';
        $array = null;
        
        if((int)$tipo == 1){
            $Sql = " webpycca.._garantiaExtConsulta ".(int)$cod_gar."  ";
        } else if((int)$tipo == 2){
            $Sql = " webpycca.._garantiaExtConsulta 0,'".$fec_ini."','".$fec_fin."'  ";
        }
        
        if(!empty($cod_gar) || !empty($fec_ini) || !empty($fec_fin)){
            
            //$this->db = $this->load->database('desarrollo', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR GARANT&Iacute;A: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            if(count($result->result_array())){
                foreach($result->result_array() as $row){
                    $array[] = array(
                        'co_con'=>(int)$row['co_contrato'],
                        'de_sri'=>trim($row['de_sri']),
                        'co_bdg'=>(int)$row['co_bodega'],
                        'ds_bdg'=>trim(utf8_encode($row['ds_bodega'])),
                        'co_caj'=>(int)$row['co_caja'],
                        'nu_doc'=>(int)$row['nu_documento'],
                        
                        'co_art'=>$row['cod_art'],
                        'ds_art'=>trim(utf8_encode($row['des_art'])),
                        'va_art'=>(float)$row['val_pvp_art'],
                        'pr_art'=>(int)$row['tiempo_garantia_almacen'],
                        'ds_mar'=>trim(utf8_encode($row['ds_marca'])),
                        'ds_mod'=>trim(utf8_encode($row['ds_modelo'])),
                        'nu_ser'=>trim(utf8_encode($row['nu_serie'])),
                        
                        'co_art_ga'=>$row['cod_art_gar'],
                        'ds_art_ga'=>trim(utf8_encode($row['des_art_gar'])),
                        'va_art_gar'=>(float)$row['val_pvp_art_gar'],
                        'pr_art_gar'=>(int)$row['tiempo_garantia_proveedor'],
                        
                        'em_env'=>trim(utf8_encode($row['email_enviado'])),
                        'id_cli'=>trim($row['co_cliente']),
                        'no_cli'=>trim(utf8_encode($row['perNombre1'])),
                        'di_cli'=>trim(utf8_encode($row['direccion'])),
                        'tl_cli'=>trim($row['telefono']),
                        'co_cli'=>trim(utf8_encode($row['correo'])),
                        
                        'co_usu_pos'=>(int)$row['co_usuario_pos'],
                        'no_usu_pos'=>trim(utf8_encode($row['NOMBRE'])),
                        'ap_usu_pos'=>trim(utf8_encode($row['APELLIDO'])),
                        
                        'fe_gar'=>!empty($row['fe_garantia'])?date('Y/m/d  H:i:s',strtotime($row['fe_garantia'])):'',
                        'fe_fac'=>!empty($row['fe_factura'])?date('Y/m/d  H:i:s',strtotime($row['fe_factura'])):''
                    );
                }
            }
        }
        
        return $array;
        
    }
    
}
?>
