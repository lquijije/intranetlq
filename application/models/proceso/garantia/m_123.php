<?php

class m_123 extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    function retGarantia($cod_gar,$tipo,$cedula = null){
        
        $Sql = '';
        $array = null;
        $error = '';
        if((int)$tipo == 1){
            $Sql = " webpycca.._garantiaExtConsulta ".(int)$cod_gar."  ";
        } else if((int)$tipo == 2){
            $Sql = " webpycca.._garantiaExtConsulta 0,'','','".trim($cedula)."' ";
        }
		
        if(!empty($cod_gar) || !empty($cedula)){

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR GARANT&Iacute;A: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            if(count($result->result_array())){
                foreach($result->result_array() as $row){
                    $array[] = array(
                        'co_con'=>(int)$row['co_contrato'],
                        'de_sri'=>trim($row['de_sri']),
                        'co_bdg'=>(int)$row['co_bodega'],
                        'ds_bdg'=>trim(utf8_encode($row['ds_bodega'])),
                        'co_caj'=>(int)$row['co_caja'],
                        'nu_doc'=>(int)$row['nu_documento'],
                        
                        'co_art'=>$row['cod_art'],
                        'ds_art'=>trim(utf8_encode($row['des_art'])),
                        'va_art'=>(float)$row['val_pvp_art'],
                        'pr_art'=>(int)$row['tiempo_garantia_almacen'],
                        'ds_mar'=>trim(utf8_encode($row['ds_marca'])),
                        'ds_mod'=>trim(utf8_encode($row['ds_modelo'])),
                        'nu_ser'=>trim(utf8_encode($row['nu_serie'])),
                        
                        'co_art_ga'=>$row['cod_art_gar'],
                        'ds_art_ga'=>trim(utf8_encode($row['des_art_gar'])),
                        'va_art_gar'=>(float)$row['val_pvp_art_gar'],
                        'pr_art_gar'=>(int)$row['tiempo_garantia_proveedor'],
                        
                        'em_env'=>trim(utf8_encode($row['email_enviado'])),
                        'id_cli'=>trim($row['co_cliente']),
                        'no_cli'=>trim(utf8_encode($row['perNombre1'])),
                        'di_cli'=>trim(utf8_encode($row['direccion'])),
                        'tl_cli'=>trim($row['telefono']),
                        'co_cli'=>trim(utf8_encode($row['correo'])),
                        
                        'co_usu_pos'=>(int)$row['co_usuario_pos'],
                        'no_usu_pos'=>trim(utf8_encode($row['NOMBRE'])),
                        'ap_usu_pos'=>trim(utf8_encode($row['APELLIDO'])),
                        
                        'fe_fac'=>!empty($row['fe_factura'])?date('Y/m/d  H:i:s',strtotime($row['fe_factura'])):'',
                        'fe_gar'=>!empty($row['fe_garantia'])?date('Y/m/d  H:i:s',strtotime($row['fe_garantia'])):''
                    );
                }
            }
            
        }
        
        return array('data'=>$array);
        
    }
    
    function retGarantiaFact($de_sri,$co_caja,$nu_doc){
        
        $Sql  = " SELECT a.co_contrato,a.fe_garantia,a.fe_factura, a.co_bodega,RTRIM(LTRIM(REPLACE(REPLACE(c.de_bodega,'123',''),'PYCCA','')))+'' as ds_bodega, ";
        $Sql .= " c.de_ciudad,c.de_sri,a.co_caja,a.nu_documento, a.co_cliente,b.perNombre1,a.email_enviado, ";
        $Sql .= " ISNULL((SELECT TOP 1 RTRIM(CONVERT(CHAR(40),telnumero)) FROM sgdbseguridad..segclitelefonos tel WHERE tel.perid = b.perid),'') AS telefono, ";
        $Sql .= " ISNULL((SELECT TOP 1 RTRIM(CONVERT(CHAR(400),DirDireccion)) FROM sgdbseguridad..segcliDirecciones dir WHERE dir.perid = b.perid),'') AS direccion, ";
        $Sql .= " ISNULL((SELECT TOP 1 RTRIM(CONVERT(CHAR(200),EMaDireccion)) FROM sgdbseguridad..segcliEMails mail WHERE mail.perid = b.perid),'') AS correo, ";
        $Sql .= " a.co_articulo+'' as cod_art,d.ds_articulo+'' as des_art,a.valor_articulo+'' as val_pvp_art,a.nu_serie,a.ds_marca,a.ds_modelo,a.tiempo_garantia_almacen, ";
        $Sql .= " a.co_articulo_garantia+'' as cod_art_gar,e.ds_articulo+'' as des_art_gar,a.valor_articulo_garantia+'' as val_pvp_art_gar, ";
        $Sql .= " a.tiempo_garantia_proveedor, a.co_usuario_pos,f.APELLIDO,f.NOMBRE ";
        $Sql .= " FROM WEBPYCCA..tb_contrato_garant_ext a ";
        $Sql .= " LEFT OUTER JOIN SGDBSEGURIDAD..segclipersona b ON a.co_cliente = b.PerID collate Latin1_General_CI_AS ";
        $Sql .= " INNER JOIN PODBPOS..posgenbodega c ON a.co_bodega = c.co_bodega ";
        $Sql .= " INNER JOIN IVDBINVENTAR..ivtbarticulo d ON a.co_articulo = d.co_articulo collate Latin1_General_CI_AS ";
        $Sql .= " INNER JOIN IVDBINVENTAR..ivtbarticulo e ON a.co_articulo_garantia = e.co_articulo collate Latin1_General_CI_AS ";
        $Sql .= " LEFT OUTER JOIN [PYCCA-APP1].personal_pi.dbo.rh_person f ON a.co_usuario_pos = f.CODIGO ";
        $Sql .= " WHERE a.ds_estado = 'A' ";
        $Sql .= " AND a.co_bodega = (SELECT co_bodega FROM PODBPOS..posgenbodega WHERE de_sri = '".trim($de_sri)."') ";
        $Sql .= " AND a.co_caja = $co_caja ";
        $Sql .= " AND a.nu_documento = $nu_doc ";
        $Sql .= " ORDER BY a.co_contrato DESC; ";
        
        $array = null;
        
        if(!empty($de_sri) || !empty($co_caja) || !empty($nu_doc)){
            
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR GARANT&Iacute;A: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            if(count($result->result_array())){
                foreach($result->result_array() as $row){
                    $array[] = array(
                        'co_con'=>(int)$row['co_contrato'],
                        'de_sri'=>trim($row['de_sri']),
                        'co_bdg'=>(int)$row['co_bodega'],
                        'ds_bdg'=>trim(utf8_encode($row['ds_bodega'])),
                        'co_caj'=>(int)$row['co_caja'],
                        'nu_doc'=>(int)$row['nu_documento'],
                        'co_art'=>$row['cod_art'],
                        'ds_art'=>trim(utf8_encode($row['des_art'])),
                        'va_art'=>(float)$row['val_pvp_art'],
                        'pr_art'=>(int)$row['tiempo_garantia_almacen'],
                        'ds_mar'=>trim(utf8_encode($row['ds_marca'])),
                        'ds_mod'=>trim(utf8_encode($row['ds_modelo'])),
                        'nu_ser'=>trim(utf8_encode($row['nu_serie'])),
                        'co_art_ga'=>$row['cod_art_gar'],
                        'ds_art_ga'=>trim(utf8_encode($row['des_art_gar'])),
                        'va_art_gar'=>(float)$row['val_pvp_art_gar'],
                        'pr_art_gar'=>(int)$row['tiempo_garantia_proveedor'],
                        'em_env'=>trim(utf8_encode($row['email_enviado'])),
                        'id_cli'=>trim($row['co_cliente']),
                        'no_cli'=>trim(utf8_encode($row['perNombre1'])),
                        'di_cli'=>trim(utf8_encode($row['direccion'])),
                        'tl_cli'=>trim($row['telefono']),
                        'co_cli'=>trim(utf8_encode($row['correo'])),
                        'co_usu_pos'=>(int)$row['co_usuario_pos'],
                        'no_usu_pos'=>trim(utf8_encode($row['NOMBRE'])),
                        'ap_usu_pos'=>trim(utf8_encode($row['APELLIDO'])),
                        'fe_gar'=>!empty($row['fe_garantia'])?date('Y/m/d  H:i:s',strtotime($row['fe_garantia'])):'',
                        'fe_fac'=>!empty($row['fe_factura'])?date('Y/m/d  H:i:s',strtotime($row['fe_factura'])):''
                    );
                }
            }
        }
        
        return array('data'=>$array);
        
    }
    
}
?>
