<?php

class m_124 extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retColaborador($data){  
        
        error_reporting(0);
        
        $user = $array = null;

        if (!empty($data['c_e']) && !empty($data['cmb_mes']) && !empty($data['cmb_anio'])){
            
            $Sql = " podbpos_cierres.dbo._ventasPorColaborador '".trim($data['c_e'])."',4,".(int)$data['cmb_mes'].",".(int)$data['cmb_anio']." ";
            $conn=odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset=odbc_exec($conn,$Sql);
            if (odbc_error()){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR: AL RETORNAR VENTAS POR COLABORADOR ",odbc_errormsg($conn), 'error_db');
                die;
            }
            while($row = odbc_fetch_array($resultset)){
                $user = array(
                    'co_nom'=>utf8_encode($row['NOMBRE']),
                    'co_ape'=>utf8_encode($row['APELLIDO']),
                    'co_car'=>utf8_encode($row['ds_cargo']),
                );
            } 
            
            odbc_next_result($resultset);
            while($row = odbc_fetch_array($resultset) ) {
                $array[] = array(
                    'co_art'=>$row['co_articulo'],
                    'ds_art'=>trim(utf8_encode($row['ds_articulo'])),
                    'ds_gru'=>trim(utf8_encode($row['ds_grupo'])),
                    'ds_lin'=>trim(utf8_encode($row['ds_linea'])),
                    'ds_s_lin'=>trim(utf8_encode($row['ds_sublinea'])),
                    'ds_cat'=>trim(utf8_encode($row['ds_categoria'])),
                    'ds_s_cat'=>trim(utf8_encode($row['ds_subcategoria'])),
                    'es_com'=>trim(utf8_encode($row['ds_grupo']))."/".trim(utf8_encode($row['ds_linea']))."/".trim(utf8_encode($row['ds_sublinea']))."/".trim(utf8_encode($row['ds_categoria']))."/".trim(utf8_encode($row['ds_subcategoria'])),
                    'ca_ven'=>(int)$row['cant_vend'],
                    'mo_ven'=>(float)$row['vent_total'],
                    'va_gan'=>(float)$row['valor_ganado']
                );
            }
            
        }
        
        return array('user'=>$user,'data'=>$array);
 
    }
    
}
?>
