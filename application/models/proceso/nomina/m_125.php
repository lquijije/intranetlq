<?php

class m_125 extends CI_Model{
    
    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retHoraSql(){
        
        $bandera = true;
        $Sql = " SELECT GETDATE() AS fe_actual;  ";
        $this->db = $this->load->database('nomina', true);

        $resultado = $this->db->query($Sql);
        if ($this->db->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR HORA ACTUAL NOMINA <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        }
        
        $row = $resultado->row_array();
        if (isset($row['fe_actual']) && !empty($row['fe_actual'])){
            if((int)date('d',strtotime($row['fe_actual'])) >= 22 && (int)date('d',strtotime($row['fe_actual'])) <= 31){
                $bandera = false;
            }
        }
        
        return $bandera;
            
    }
    
    function retCcostoIP($cod_empl,$ip){
        
        $array = null;

        if(!empty($cod_empl) && !empty($ip)){
            
            $Sql = " PERSONAL_PI.._sp_colaborador_ccosto_ip $cod_empl,'".$ip."' ";
	    $this->db = $this->load->database('nomina', true);
            
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR CENTROS DE COSTO <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            
            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row){
                    
                    $array[] = array(
                        'ip_bdg'=> trim($row['pos3_database']),
                        'co_bdg'=> trim($row['co_bodega']),
                        'ds_bdg'=> trim(utf8_encode($row['descripcion'])),
                        'co_cco'=> trim($row['co_ccosto'])
                    );
                }
                
            }
            
        }
        
        return $array;
            
    }
    
    function retUsuariosCcosto($fi,$ff,$cod_ccos){
        
        $array = null;

        if(!empty($fi) && !empty($ff) && !empty($cod_ccos)){
            
            $Sql = " PERSONAL_PI.._sp_colaborador_ccosto '".trim($fi)."','".trim($ff)."',$cod_ccos,".trim($_SESSION['c_e'])." ";
//            echo $Sql;
//            die;
            $this->db = $this->load->database('nomina', true);
            
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR EMPLEADOS CENTROS DE COSTO <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            
            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row){
                    $array[] = array(
                        'co_empl'=> (int)trim($row['co_empleado']),
                        'no_empl'=> trim(utf8_encode($row['NOMBRE'])),
                        'ap_empl'=> trim(utf8_encode($row['APELLIDO'])),
                        'co_carg'=> trim($row['COD_CARGO']),
                        'no_carg'=> trim(utf8_encode($row['NOMB_CARGO'])),
                        'co_turn'=> (int)trim($row['co_turno']),
                        'co_sali'=> (int)trim($row['salio']),
                        'em_proc'=> (int)trim($row['pass_table']),
                        'ul_turn'=> (int)trim($row['ultimo_turno'])
                    );
                }
                
            }
            
        }
        
        return $array;
            
    }
    
    function retTurnosConsulta($fi,$ff,$cod_ccos){
        
        $array = null;

        if(!empty($fi) && !empty($ff) && !empty($cod_ccos)){
            
            $Sql = " PERSONAL_PI.._sp_colaborador_turnos_consulta $cod_ccos,'".trim($fi)."','".trim($ff)."' ";
            $this->db = $this->load->database('nomina', true);
            
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR EMPLEADOS CENTROS DE COSTO <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            
            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row){
                    $array[] = array(
                        'co_ceco'=> (int)trim($row['co_ceco']),
                        'no_ceco'=> trim(utf8_encode($row['nom_ceco'])),
                        'co_empl'=> (int)trim($row['co_empleado']),
                        'no_empl'=> trim(utf8_encode($row['NOMBRE'])),
                        'ap_empl'=> trim(utf8_encode($row['APELLIDO'])),
                        'co_carg'=> trim($row['COD_CARGO']),
                        'no_carg'=> trim(utf8_encode($row['NOMB_CARGO'])),
                        'co_turn'=> (int)trim($row['co_turno']),
                        'no_turn'=> trim(utf8_encode($row['DESCRIPCION'])),
                        'em_proc'=> (int)trim($row['pass_table'])
                    );
                }
                
            }
            
        }
        
        return $array;
            
    }
    
    function retTurnosConsultaUltimo($cod_ccos){
        
        $array = null;

        if(!empty($cod_ccos)){
            
            $Sql = " PERSONAL_PI.._sp_colaborador_turnos_consulta $cod_ccos ";
            $this->db = $this->load->database('nomina', true);
            
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR EMPLEADOS CENTROS DE COSTO <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            
            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row){
                    $array[] = array(
                        'fe_des'=> trim($row['fe_desde']),
                        'fe_has'=> trim($row['fe_hasta']),
                        'co_ceco'=> (int)trim($row['co_ceco']),
                        'no_ceco'=> trim(utf8_encode($row['nom_ceco'])),
                        'co_empl'=> (int)trim($row['co_empleado']),
                        'no_empl'=> trim(utf8_encode($row['NOMBRE'])),
                        'ap_empl'=> trim(utf8_encode($row['APELLIDO'])),
                        'co_carg'=> trim($row['COD_CARGO']),
                        'no_carg'=> trim(utf8_encode($row['NOMB_CARGO'])),
                        'co_turn'=> (int)trim($row['co_turno']),
                        'no_turn'=> trim(utf8_encode($row['DESCRIPCION'])),
                        'em_proc'=> (int)trim($row['pass_table'])
                    );
                }
                
            }
            
        }
        
        return $array;
            
    }
    
    function retFecAutorizada(){
        
        $array = null;
            
        $Sql = " PERSONAL_PI.._get_turnos_periodo 0 ";
        $this->db = $this->load->database('nomina', true);

        $resultado = $this->db->query($Sql);
        if ($this->db->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR EMPLEADOS CENTROS DE COSTO <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        }

        if (count($resultado->row_array())> 0){
            $row = $resultado->row_array();
            $array = array(
                'fe_act'=> !empty($row['hoy'])?date('Y-m-d',strtotime($row['hoy'])):'',
                'fe_des'=> !empty($row['desde'])?date('Y-m-d',strtotime($row['desde'])):'',
                'fe_has'=> !empty($row['hasta'])?date('Y-m-d',strtotime($row['hasta'])):''
            );
        }
        
        return $array;
            
    }
    
    function retTurnos(){
        
        $array = null;
            
        $Sql = " PERSONAL_PI.._sp_colaborador_turnos ";
        $this->db = $this->load->database('nomina', true);

        $resultado = $this->db->query($Sql);
        if ($this->db->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR TURNOS <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
            die;
        }

        if (count($resultado->result_array())> 0){
            
            $array[]=array('co_turn'=> 0,'ds_turn'=> 'Sin Turno','in_time'=> '','fi_time'=> '');
                
            foreach ($resultado->result_array() as $row){
                $array[] = array(
                    'co_turn'=> (int)$row['cod_turno'],
                    'ds_turn'=> trim(utf8_decode($row['descripcion'])),
                    'in_time'=> !empty($row['start_time'])?date('H:i',strtotime($row['start_time'])):'',
                    'fi_time'=> !empty($row['en_time'])?date('H:i',strtotime($row['en_time'])):''
                );
            }
            
        }
        
        return $array;
            
    }
    
    function deleTurnos($data){
        $array = null;
        if(!empty($data['f_i']) && !empty($data['f_f']) && !empty($data['c_c']) && !empty($data['c_e'])){
            
            $Sql = " PERSONAL_PI.._sp_colaborador_turnos_elimina '".trim($data['f_i'])."','".trim($data['f_f'])."',".trim($data['c_c']).",".trim($data['c_e'])." ";
            $this->db = $this->load->database('nomina', true);
            
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL INGRESAR TURNOS: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            $row = $result->row_array();
            $array = array(
                'co_err'=>(int)$row['co_error'],
                'tx_err'=>utf8_decode($row['tx_error'])
            );
            
        }
        
        return array('data'=>$array);
    }
    
    function saveTurnos($cab,$det){
        
        $array = null;
        
        if(!empty($cab['f_i']) && !empty($cab['f_f']) && !empty($cab['c_c'])){
            
            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false; 
            $dom->formatOutput = true; 

            ##<turnos>
            
                $cabecera = $dom->createElement('turnos');
                $dom->appendChild($cabecera);
                
                foreach ($det as $row){
                    $item = $dom->createElement('item');
                    $cabecera->appendChild($item);
                    
                    $n_1 = $dom->createElement("co_use",trim($row['c_u'])); 
                    $item->appendChild($n_1);

                    $n_2 = $dom->createElement("co_tur",trim(utf8_decode($row['c_t']))); 
                    $item->appendChild($n_2);
                }
                
            ##</turnos>
                    
            $xml = $dom->saveXML();        
            
            $Sql = " PERSONAL_PI.._sp_colaborador_turnos_ingreso '".trim($cab['f_i'])."','".trim($cab['f_f'])."',".trim($cab['c_c']).",".trim($_SESSION['c_e']).",'".$xml."' ";
            $this->db = $this->load->database('nomina', true);
            
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL INGRESAR TURNOS: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            $row = $result->row_array();
            $array = array(
                'co_err'=>(int)$row['co_error'],
                'tx_err'=>utf8_decode($row['tx_error'])
            );
        }
        
        return $array;
        
    }
    
    function updaTurnos($data){
        
        $array = null;
        
        if(!empty($data['f_i']) && !empty($data['f_f']) && !empty($data['c_c']) && !empty($data['c_tu']) && !empty($data['a_tu'])){
            $Sql = " PERSONAL_PI.._sp_colaborador_turnos_update '".trim($data['f_i'])."','".trim($data['f_f'])."',".trim($data['c_c']).",".trim($data['c_ti']).",".trim($data['c_tu']).",".trim($data['a_tu'])." ";
            $this->db = $this->load->database('nomina', true);
            
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL INGRESAR TURNOS: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
            $row = $result->row_array();
            $array = array(
                'co_err'=>(int)$row['co_error'],
                'tx_err'=>utf8_decode($row['tx_error'])
            );
            
        }
        
        return array('data'=>$array);
        
    }
    
}
?>
