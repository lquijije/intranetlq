<?php

class m_1251 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function verEmpresa($cod_emp){
        
        $error = '';
        $array = null;
        
        if(!empty($cod_emp)){

            $Sql  = " SELECT a.COD_EMPR,b.nomb_empr ";
            $Sql .= " FROM [PYCCA-APP1].PRDBPRESTAMO.DBO.PRTBUSUARIOEMPRESA a ";
            $Sql .= " INNER JOIN PRDBPROCESO..MATBEMPRPICA b ";
            $Sql .= " ON a.COD_EMPR = b.codi_empr ";
            $Sql .= " WHERE a.COD_EMPL=".(int)$cod_emp;

            $DB1 = $this->load->database('sqlnsip', true);
            $result = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER EMPRESAS: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            } else {

                $row = $result->row_array();
                if(!empty($row)){
                    $array[] = array(
                        'co_empr'=>trim($row['COD_EMPR']),
                        'no_empr'=>trim($row['nomb_empr'])
                    );
                }
            }
        }
        
        return $array;
        
    }
    
    function verReemplazo($cod_emp){
        
        $array = null;
        
        if(!empty($cod_emp)){

            $Sql  = " SELECT B.COD_EMPR,c.nomb_empr ";
            $Sql .= " FROM   CHTBGRPREMPL A ";
            $Sql .= " INNER JOIN [PYCCA-APP1].PRDBPRESTAMO.DBO.PRTBUSUARIOEMPRESA B ";
            $Sql .= " ON A.CODI_EMPR=B.COD_EMPR ";
            $Sql .= " AND A.CODI_EMPL=B.COD_EMPL ";
            $Sql .= " INNER JOIN PRDBPROCESO..MATBEMPRPICA c ";
            $Sql .= " ON B.COD_EMPR = c.codi_empr ";
            $Sql .= " WHERE  A.CODI_EMPR IN ('PYCCA') ";
            $Sql .= " AND REMP_STAT='R' ";
            $Sql .= " AND CODI_REMP=".(int)$cod_emp;
            $Sql .= " UNION ";
            $Sql .= " SELECT B.COD_EMPR,c.nomb_empr ";
            $Sql .= " FROM   CHTBGRPREMPL A ";
            $Sql .= " INNER JOIN [PYCCA-APP1].PRDBPRESTAMO.DBO.PRTBUSUARIOEMPRESA B ";
            $Sql .= " ON A.CODI_EMPR=B.COD_EMPR ";
            $Sql .= " AND A.CODI_EMPL=B.COD_EMPL ";
            $Sql .= " INNER JOIN PRDBPROCESO..MATBEMPRPICA c ";
            $Sql .= " ON B.COD_EMPR = c.codi_empr ";
            $Sql .= " WHERE  A.CODI_EMPR IN ('XYZ') ";
            $Sql .= " AND REMP_STAT='R' ";
            $Sql .= " AND CODI_REMP=".(int)$cod_emp;

            $DB1 = $this->load->database('sqlnsip', true);
            $result = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER REEMPLAZO: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            } else {

                $res = $result->result_array();
                if(!empty($res)){
                    foreach($res as $row){
                        $array[] = array(
                            'co_empr'=>trim($row['COD_EMPR']),
                            'no_empr'=>trim($row['nomb_empr'])
                        );
                    }
                    
                }
                
            }
        }
        
        return $array;
        
    }
    
    function retSolicitudesPrestamos($cod_empr){
        
        $error = '';
        $array = null;
        $p = 0;
        if(!empty($cod_empr) && ($cod_empr == 'XYZ' || $cod_empr == 'PYCCA')){
            
            $Sql  = " SELECT TOP 5 S.cons_soli, E.nombre +'' AS EMPRESA , ";
            $Sql .= " REPLACE(REPLACE(REPLACE(C.nombre,'CC. ',''),'C.C.',''),'C.C. ','') +'' CENTRO, ";
            $Sql .= " S.cedu_deud, D.nomb_deud+'' AS DEUDOR, S.desc_ptmo, S.codi_conc, ";
            $Sql .= " CASE WHEN S.codi_conc = 'O' THEN 'ORDINARIO' ELSE 'EXTRAORDINARIO' END+'' AS DES_CONC, ";
            $Sql .= " CONVERT(VARCHAR(10),S.fech_ingr,101)+'' FECHA_INGRESO, S.mont_prest, S.valo_cuot, ";
            $Sql .= " DEUDA_ANTERIOR = (SELECT saldo = ISNULL(SUM(d.pago_capi + d.pago_inte - d.capi_real - d.inte_real),0) ";
            $Sql .= " FROM PRDBPRESTAMO..prtbprestamo p, PRDBPRESTAMO..prtbdocument d ";
            $Sql .= " WHERE p.cedu_deud = S.cedu_deud ";
            $Sql .= " AND p.cons_pres = d.cons_pres ";
            $Sql .= " AND p.stat_regi IN('','A') ";
            $Sql .= " AND d.stat_regi IN('','A')), ";
            $Sql .= " FECHA_INICIO = CONVERT(VARCHAR(10),S.fech_inic,101), S.mese_pact, M.desc_moti, nume_dsct, ";
            $Sql .= " CASE WHEN nume_dsct = 1 THEN 'M' ELSE 'Q' END AS DES_DSCT, ";
            $Sql .= " codi_usua,G.APELLIDO+' '+G.NOMBRE AS nomb_usua_ ";
            $Sql .= " FROM ";
            $Sql .= " PRDBPRESTAMO..prtbsolicitu S, ";
            $Sql .= " PRDBPRESTAMO..prtbdeudores D, ";
            $Sql .= " PRDBPRESTAMO..prtbmotipres M, ";
            $Sql .= " PERSONAL_PI..rh_empresa E, ";
            $Sql .= " PERSONAL_PI..rh_ccosto C, ";
            $Sql .= " PERSONAL_PI..RH_PERSON G ";
            
            if($cod_empr == 'XYZ'){
                $p = 1;
                $Sql .= " WHERE cia_Deud IN(1) ";
            } else {
                $p = 2;
                $Sql .= " WHERE cia_Deud IN(7,9,27,33,34,38,47) ";
            }
            
            //$Sql .= " AND stat_soli = 'V' ";
            $Sql .= " AND S.cedu_deud = D.cedu_deud ";
            $Sql .= " AND S.moti_pres = M.moti_pres ";
            $Sql .= " AND S.cia_deud = E.cod_empresa ";
            $Sql .= " AND S.cia_deud = C.cod_empresa ";
            $Sql .= " AND S.cent_cost = C.cod_ccosto ";
            $Sql .= " AND S.codi_usua = G.CODIGO ";
            $Sql .= " ORDER BY fech_inic DESC; ";

            $DB1 = $this->load->database('nomina', true);
            $result = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER SOLICITUDES: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            } else {

                $res = $result->result_array();

                if(!empty($res)){
                    $a = 0;
                    foreach ($res as $row){
                        $a++;
                        $array[] = array(
                            'co_empr'=>$cod_empr,
                            'co_sol'=>(int)$row['cons_soli'],
                            'co_sol_link'=>"<a href='javascript:void(0)' onclick='viewSolic(".$p.",".(int)$row['cons_soli'].");' class='underline'>".(int)$row['cons_soli']."</a>",
                            'em_sol'=>trim(utf8_encode($row['EMPRESA'])),
                            'al_sol'=>trim(utf8_encode($row['CENTRO'])),
                            'ci_sol'=>trim($row['cedu_deud']),
                            'de_sol'=>trim(utf8_encode($row['DEUDOR'])),
                            'mo_sol'=>trim(utf8_encode($row['desc_ptmo'])),
                            'cn_sol'=>trim($row['codi_conc']),
                            'de_con'=>trim($row['DES_CONC']),
                            'fe_sol'=>trim($row['FECHA_INGRESO']),
                            'fe_ini'=>trim($row['FECHA_INICIO']),
                            'mo_pre'=>number_format($row['mont_prest'],2),
                            'va_cuo'=>(float)$row['valo_cuot'],
                            'de_cuo'=>number_format($row['valo_cuot'],2)."X".(int)$row['nume_dsct'].$row['DES_DSCT'],
                            'de_ant'=>(int)$row['DEUDA_ANTERIOR'] > 0 ? number_format($row['DEUDA_ANTERIOR'],2) : '',
                            'me_pac'=>(int)$row['mese_pact'],
                            'de_mot'=>trim(utf8_encode($row['desc_moti'])),
                            'nu_dsc'=>(int)$row['nume_dsct'],
                            'de_dsc'=>trim($row['DES_DSCT']),
                            'co_usu'=>(int)$row['codi_usua'],
                            'no_usu'=>trim(utf8_encode($row['nomb_usua_'])),
                            'ap_sol'=>'<td><center><input type="radio" name="ck_'.$a.'" id="option_apro[]" value="'.(int)$row['cons_soli'].'"></center></td>',
                            'ne_sol'=>'<td><center><input type="radio" name="ck_'.$a.'" id="option_rech[]" value="'.(int)$row['cons_soli'].'"></center></td>'
                        );
                    }
                }
            }
            
        }
        
        return $array;
        
    }
    
    function retSolicitudNovedades($cod_sol){
        
        $error = '';
        $array = null;
        
        if(!empty($cod_sol)){

            $Sql  = " SELECT * FROM PRDBPRESTAMO..prtbnovesoli ";
            $Sql .= " WHERE cons_soli = ".(int)$cod_sol;

            $DB1 = $this->load->database('nomina', true);
            $result = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error = "ERROR AL OBTENER SOLICITUD: ".$DB1->_error_number()." ".$DB1->_error_message();
            } else {

                $row = $result->row_array();

                if(!empty($row)){
                    
                    if((int)$row['codi_nov1'] == 1){
                        $array [] = array(
                            'codi_nov'=>(int)$row['codi_nov1'],
                            'desc_nov'=>trim($row['desc_nov1'])
                        );
                    }
                    if((int)$row['codi_nov2'] == 1){
                        $array [] = array(
                            'codi_nov'=>(int)$row['codi_nov2'],
                            'desc_nov'=>trim($row['desc_nov2'])
                        );
                    }
                    if((int)$row['codi_nov3'] == 1){
                        $array [] = array(
                            'codi_nov'=>(int)$row['codi_nov3'],
                            'desc_nov'=>trim($row['desc_nov3'])
                        );
                    }
                    
                    if((int)$row['codi_nov4'] == 1){
                        $array [] = array(
                            'codi_nov'=>(int)$row['codi_nov4'],
                            'desc_nov'=>trim($row['desc_nov4'])
                        );
                    }
                    
                    if((int)$row['codi_nov5'] == 1){
                        $array [] = array(
                            'codi_nov'=>(int)$row['codi_nov5'],
                            'desc_nov'=>trim($row['desc_nov5'])
                        );
                    }
                }
            }
        }
        
        return array('data'=>$array,'err'=>$error);
        
    }
    
    function retSolicitud($cod_empr,$cod_sol){
        
        $error = '';
        $array = null;
        
        if(!empty($cod_sol) && !empty($cod_empr) && ($cod_empr == 'XYZ' || $cod_empr == 'PYCCA')){

            $Sql  = " SELECT S.cons_soli, ";
            $Sql .= " stat_soli = ";
            $Sql .= " CASE S.stat_soli ";
            $Sql .= " WHEN 'F' THEN 'Procesada' ";
            $Sql .= " WHEN 'A' THEN 'Aprobada' ";
            $Sql .= " WHEN 'R' THEN 'Rechazada' ";
            $Sql .= " WHEN 'V' THEN 'Verificada' ";
            $Sql .= " ELSE 'ERROR' END, ";
            $Sql .= " E.nombre+'' AS EMPRESA,C.nombre+'' AS CENTRO,D.nomb_deud+'' AS DEUDOR, ";
            $Sql .= " S.cedu_deud, S.codi_empl, S.desc_ptmo, S.codi_conc, ";
            $Sql .= " CASE WHEN S.codi_conc = 'O' THEN 'ORDINARIO' ELSE 'EXTRAORDINARIO' END AS DES_CONC, ";
            $Sql .= " CONVERT(VARCHAR(10),S.fech_ingr,101)+'' AS FECHA_INGRESO, ";
            $Sql .= " S.mont_prest, S.sald_deud, S.valo_cuot, DEUDA_ANTERIOR = ( ";
            $Sql .= " SELECT saldo = ISNULL(SUM(d.pago_capi + d.pago_inte - d.capi_real - d.inte_real),0) ";
            $Sql .= " FROM PRDBPRESTAMO..prtbprestamo p, PRDBPRESTAMO..prtbdocument d ";
            $Sql .= " WHERE p.cedu_deud = S.cedu_deud ";
            $Sql .= " AND p.cons_pres = d.cons_pres ";
            $Sql .= " AND p.stat_regi IN('','A') ";
            $Sql .= " AND d.stat_regi IN('','A')),S.suel_actu, S.prom_ingr, S.prom_disp, CONVERT(VARCHAR(10),S.fech_inic,101)+'' AS FECHA_INICIO, ";
            $Sql .= " S.mese_pact, S.porc_inte, M.desc_moti, COM.desc_niv1, COM.esta_niv1, COM.desc_niv2, COM.esta_niv2, COM.desc_niv3, COM.esta_niv3, nume_dsct, ";
            $Sql .= " CASE WHEN nume_dsct = 1 THEN 'Mensuales' ELSE 'Quincenales' END AS DES_DSCT,codi_usua, ";
            $Sql .= " G.APELLIDO+' '+G.NOMBRE AS nomb_usua_ ";
            $Sql .= " FROM ";
            $Sql .= " PRDBPRESTAMO..prtbsolicitu S, ";
            $Sql .= " PRDBPRESTAMO..prtbdeudores D, ";
            $Sql .= " PRDBPRESTAMO..prtbmotipres M, ";
            $Sql .= " PERSONAL_PI..rh_empresa E, ";
            $Sql .= " PERSONAL_PI..rh_ccosto C, ";
            $Sql .= " PRDBPRESTAMO..prtbestasoli COM, ";
            $Sql .= " PERSONAL_PI..RH_PERSON G ";
            $Sql .= " WHERE S.cons_soli = ".(int)$cod_sol;
            
            if($cod_empr == 'XYZ'){
                $Sql .= " AND cia_Deud IN(1) ";
            } else {
                $Sql .= " AND cia_Deud IN(7,9,27,33,34,38,47) ";
            }
            
            $Sql .= " AND S.cedu_deud = D.cedu_deud ";
            $Sql .= " AND S.moti_pres = M.moti_pres ";
            $Sql .= " AND S.cia_deud = E.cod_empresa ";
            $Sql .= " AND S.cia_deud = C.cod_empresa ";
            $Sql .= " AND S.cent_cost = C.cod_ccosto ";
            $Sql .= " AND S.cons_soli = COM.cons_soli ";
            $Sql .= " AND S.codi_usua = G.CODIGO; ";

            $DB1 = $this->load->database('nomina', true);
            $result = $DB1->query($Sql);

            if ($DB1->_error_message() <> ''){
                $error = "ERROR AL OBTENER SOLICITUD: ".$DB1->_error_number()." ".$DB1->_error_message();
            } else {

                $row = $result->row_array();

                if(!empty($row)){

                    $array = array(
                        'a'=>(int)$row['cons_soli'],
                        'b'=>trim($row['stat_soli']),
                        'c'=>trim(utf8_encode($row['EMPRESA'])),
                        'd'=>trim(utf8_encode($row['CENTRO'])),
                        'e'=>trim($row['cedu_deud']),
                        'f'=>trim(utf8_encode($row['DEUDOR'])),
                        'g'=>(int)$row['codi_empl'],
                        'h'=>trim(utf8_encode($row['desc_ptmo'])),
                        'i'=>trim($row['codi_conc']),
                        'j'=>trim($row['DES_CONC']),
                        'k'=>trim($row['FECHA_INGRESO']),
                        'l'=>number_format($row['mont_prest'],2),
                        'm'=>number_format($row['sald_deud'],2),
                        'n'=>number_format($row['valo_cuot'],2),
                        'o'=>(int)$row['DEUDA_ANTERIOR'] > 0 ? number_format($row['DEUDA_ANTERIOR'],2) : '',
                        'p'=>number_format($row['suel_actu'],2),
                        'q'=>number_format($row['prom_ingr'],2),
                        'r'=>number_format($row['prom_disp'],2),
                        's'=>trim($row['FECHA_INICIO']),
                        't'=>(int)$row['mese_pact'],
                        'u'=>number_format($row['porc_inte'],2),
                        'v'=>trim(utf8_encode($row['desc_moti'])),
                        'w'=>trim($row['esta_niv1']),
                        'x'=>trim($row['desc_niv1']),
                        'y'=>trim($row['esta_niv2']),
                        'z'=>trim($row['desc_niv2']),
                        'a1'=>trim($row['esta_niv3']),
                        'a2'=>trim($row['desc_niv3']),
                        'a3'=>(int)$row['nume_dsct'],
                        'a4'=>(int)$row['codi_usua'],
                        'a5'=>trim(utf8_encode($row['nomb_usua_'])),
                        'a6'=>trim($row['DES_DSCT'])
                    );
                }
            }
        }
        
        return array('data'=>$array,'err'=>$error);
        
    }
    
    function saveAprobacion($cod_empr,$cod_sol,$cod_est,$obs){
        
        $error = '';
        $array = null;
        
        if(!empty($cod_sol) && !empty($cod_est)){
            
            if(!empty($obs)){
                $Sql = "PRDBPRESTAMO..sp_actualiza_solicitud_prestamo1 '".trim($cod_empr)."',".$cod_sol.",'".$cod_est."','".trim($obs)."'; ";
            } else {
                $Sql = "PRDBPRESTAMO..sp_actualiza_solicitud_prestamo1 '".trim($cod_empr)."',".$cod_sol.",'".$cod_est."'; ";
            }
            
            $this->db = $this->load->database('nomina', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error = "ERROR AL ACTUALIZAR SOLICITUD: ".$this->db->_error_number()." ".$this->db->_error_message();
            } else {
                
                $row = $result->row_array();

                if(count($row) > 0){
                    $array = array (
                        'co_err'=>(int)$row['co_error'],
                        'tx_err'=>utf8_decode($row['tx_error'])
                    );
                }

            }
        } else {
            $error = 'PARAMETROS INCOMPLETOS';
        }
        
        return array('data'=>$array,'err'=>$error);
        
    }
    
}
?>
