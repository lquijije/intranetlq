<?php

class m_133 extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    function retCiudades($cod_ciu) {

        $array = null;

        $Sql = " SELECT co_ciudad,REPLACE(nombre,'123 ','')+'' AS nom_ciudad,telefono,direccion ";
        $Sql .= " FROM personal_pi..rh_credencial_ciudad ";
        $Sql .= " WHERE co_ciudad NOT LIKE('KC%') ";
        if (!empty($cod_ciu)) {
            $Sql .= " AND co_ciudad = '" . $cod_ciu . "' ";
        }
        $Sql .= " ORDER BY nombre; ";

        $this->db = $this->load->database('nomina', true);
        $resultado = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            $error = & load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR CIUDADES <BR> COD_ERR:" . $this->db->_error_number(), $this->db->_error_message(), 'error_db');
            die;
        }

        $res = $resultado->result_array();
        if (count($res) > 0) {
            foreach ($res as $row) {
                $array[] = array(
                    'co_ciu' => trim($row['co_ciudad']),
                    'no_ciu' => trim($row['nom_ciudad']),
                    'te_ciu' => trim(utf8_encode($row['telefono'])),
                    'di_ciu' => trim(utf8_encode($row['direccion']))
                );
            }
        }

        return $array;
    }

    function retColaboradorCr($cod_emp) {

        $error = '';
        $arr_1 = $arr_2 = $arr_3 = null;

        if (!empty($cod_emp)) {

            $Sql = " PERSONAL_PI.._credencial_consulta_empleado " . trim($cod_emp) . ";";
            $conn = odbc_connect("NOMINA-P", "pica", "");
            $resultset = odbc_exec($conn, $Sql);

            if (odbc_error()) {
                $error = "ERROR: AL RETORNAR REGLAS " . odbc_errormsg($conn);
            }

            do {

                while ($row = odbc_fetch_array($resultset)) {
                    if (isset($row['resultset'])) {

                        switch ($row['resultset']) {

                            case "COLABORADOR":

                                $arr_1 = array(
                                    'co_emp' => (int) $row['cod_empresa'],
                                    'no_emp' => trim($row['EMPRESA']),
                                    'co_col' => (int) $row['codigo'],
                                    'ce_col' => trim($row['cedula']),
                                    'no_col' => trim(utf8_encode($row['nombre'])),
                                    'ap_col' => trim(utf8_encode($row['apellido'])),
                                    'ce_col' => trim($row['cedula']),
                                    'no_car' => trim(utf8_encode($row['cargo'])),
                                    'co_cco' => trim($row['cod_ccosto']),
                                    'no_cco' => trim(utf8_encode($row['nom_ccosto']))
                                );

                                break;
                            case "TARJETA":

                                $arr_2 = array(
                                    'ta_nom' => trim(utf8_encode($row['NOMBRE'])),
                                    'ta_car' => trim(utf8_encode($row['CARGO'])),
                                    'ta_ciu' => trim($row['co_ciudad']),
                                    'no_ciu' => trim($row['ciudad_nombre']),
                                    'te_ciu' => trim($row['telefono']),
                                    'di_ciu' => trim(utf8_encode($row['direccion']))
                                );

                                break;
                            case "LOG":

                                $arr_3[] = array(
                                    'fe_emi' => !empty($row['fecha_emision']) ? date('Y/m/d H:i:s', strtotime($row['fecha_emision'])) : '',
                                    'mo_des' => (int) $row['monto_descontar']
                                );

                                break;
                        }
                    }
                }
            } while (odbc_next_result($resultset));
        }

        return array('res_1' => $arr_1, 'res_2' => $arr_2, 'res_3' => $arr_3, 'err' => $error);
    }

    function grabarCredencial($datos) {

        $array = null;
        $datos = json_decode($datos, true);

        if (!empty($datos['c_co'])) {

            $Sql = " PERSONAL_PI.._credencial_grabar " . (int) $datos['c_co'] . "," . (int) $_SESSION['c_e'] . ",'" . trim($datos['c_em']) . "','" . trim(utf8_decode($datos['n_em'])) . "','" . trim($datos['c_ci']) . "'," . (int) $datos['t_le'] . ",'" . trim($datos['c_ca']) . "',";
            $Sql .= (int) $datos['dol_cre'] == 0 ? 0 : 2;
            $Sql .= ";";
//            echo $Sql;
//            die;
            $this->db = $this->load->database('nomina', true);
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL GRABAR CREDENCIAL: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }
            $row = $result->row_array();
            if (!empty($row)) {
                $array = array(
                    'co_err' => (int) $row['co_error'],
                    'tx_err' => utf8_decode($row['tx_error'])
                );
            }
        }

        return $array;
    }

    function retReporte($fe_ini, $fe_fin, $co_des) {
        $array = array('err' => '', 'dat' => null);
        set_error_handler(function($errno, $errstr, $errfile, $errline ) {
            throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
        });
        try {
            if (!empty($fe_ini) && !empty($fe_ini)) {

                $Sql = "PERSONAL_PI.DBO.reporte_credenciales '" . date('Y-m-d', strtotime($fe_ini)) . "','" . date('Y-m-d', strtotime($fe_fin)) . "'," . (int) $co_des . ";";
                $this->db = $this->load->database('nomina', true);
                $resultado = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new PDOException("ERROR AL RETORNAR REPORTE <BR> COD_ERR:" . $this->db->_error_number() . " " . $this->db->_error_message());
                }
                $res = $resultado->result_array();
                if (!empty($res)) {
                    foreach ($res as $row) {
                        $array['dat'][] = array(
                            'fe_cre' => !empty($row['FECHA_EMISION']) ? date('Y-m-d H:i', strtotime($row['FECHA_EMISION'])) : '',
                            'st_cre' => $row['status'],
                            'co_emp' => (int) $row['CO_EMPRESA'],
                            'no_emp' => trim(utf8_encode($row['NOMBRE_EMPRESA'])),
                            'co_usu' => (int) $row['codigo'],
                            'no_usu' => trim(utf8_encode($row['NOMBRE_COLABORADOR'])),
                            'co_usu_ing' => (int) $row['usuario'],
                            'fe_eli' => !empty($row['FECHA_ELIMINA']) ? date('Y-m-d H:i', strtotime($row['FECHA_ELIMINA'])) : '',
                            'va_mon' => $row['va_monto_descontar'],
                        );
                    }
                }
            } else {
                throw new PDOException("PARAMETROS INCOMPLETOS");
            }
        } catch (Exception $exc) {
            $array['err'] = $exc->getMessage();
        }

        return $array;
    }

}

?>
