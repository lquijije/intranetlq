<?php

class m_126 extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    function retTurnos() {

        $array = null;

        $Sql = " PERSONAL_PI.._sp_colaborador_turnos ";
        $this->db = $this->load->database('nomina', true);

        $resultado = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            $error = & load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR TURNOS <BR> COD_ERR:" . $this->db->_error_number(), $this->db->_error_message(), 'error_db');
            die;
        }

        if (count($resultado->result_array()) > 0) {

            $array[] = array('co_turn' => 0, 'ds_turn' => 'Sin Turno', 'in_time' => '', 'fi_time' => '');

            foreach ($resultado->result_array() as $row) {
                $array[] = array(
                    'co_turn' => (int) $row['cod_turno'],
                    'ds_turn' => trim(utf8_decode($row['descripcion'])),
                    'in_time' => !empty($row['start_time']) ? date('H:i', strtotime($row['start_time'])) : '',
                    'fi_time' => !empty($row['en_time']) ? date('H:i', strtotime($row['en_time'])) : ''
                );
            }
        }

        return $array;
    }

    function retCcosto($cod_empl, $fi, $ff) {

        $array = null;

        if (!empty($cod_empl) && !empty($fi) && !empty($ff)) {

            $Sql1 = " PERSONAL_PI.._sp_colaborador_ingre_falta '" . trim($fi) . "','" . trim($ff) . "'; ";
            $this->db = $this->load->database('nomina', true);
            $query = $this->db->query($Sql1);
            if ($this->db->_error_message() <> '') {
                $error = & load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR FALTANTES E INGRESADOS X CECO <BR> COD_ERR:" . $this->db->_error_number(), $this->db->_error_message(), 'error_db');
                die;
            }
            $result = $query->result_array();

            if (!empty($result)) {
                foreach ($result as $row) {
                    $msm = $css = '';
                    if ((int) $row['total'] > 0 && (int) $row['total'] <> (int) $row['asignados']) {
                        $msm = 'PENDIENTE DE ASIGNAR - ' . ((int) $row['total'] - (int) $row['asignados']);
                        $css = 'warning';
                    } else if ((int) $row['total'] > 0 && (int) $row['total'] <> (int) $row['procesados']) {
                        $msm = 'PENDIENTE DE PROCESAR - ' . $row['faltantes'];
                    } else if ((int) $row['total'] > 0 && (int) $row['total'] == (int) $row['procesados']) {
                        $msm = 'PROCESADO';
                        $css = 'success';
                    } else if ((int) $row['total'] == 0) {
                        $msm = 'SIN INGRESAR';
                        $css = 'danger';
                    }
                    $array[] = array(
                        'ip_bdg' => "",
                        'co_bdg' => (int) $row['co_bodega'],
                        'ds_bdg' => trim(utf8_encode($row['descripcion'])),
                        'co_cco' => (int) $row['co_ccosto'],
                        'tx_cur' => trim($msm),
                        'st_yle' => trim($css)
                    );
                }
            }
        }
        return $array;
    }

    function retUsuariosCcosto($fi, $ff, $cod_ccos) {

        $array = null;

        if (!empty($fi) && !empty($ff) && !empty($cod_ccos)) {

            $Sql = " PERSONAL_PI.._sp_colaborador_ccosto '" . trim($fi) . "','" . trim($ff) . "',$cod_ccos," . trim($_SESSION['c_e']) . " ";
//            echo $Sql;
//            die;
            $this->db = $this->load->database('nomina', true);

            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                $error = & load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR EMPLEADOS CENTROS DE COSTO <BR> COD_ERR:" . $this->db->_error_number(), $this->db->_error_message(), 'error_db');
                die;
            }

            if (count($resultado->result_array()) > 0) {

                foreach ($resultado->result_array() as $row) {
                    if (isset($row['co_empleado'])) {
                        $array[] = array(
                            'co_empl' => (int) trim($row['co_empleado']),
                            'no_empl' => trim(utf8_encode($row['NOMBRE'])),
                            'ap_empl' => trim(utf8_encode($row['APELLIDO'])),
                            'co_carg' => trim($row['COD_CARGO']),
                            'no_carg' => trim(utf8_encode($row['NOMB_CARGO'])),
                            'co_turn' => (int) trim($row['co_turno']),
                            'co_sali' => (int) trim($row['salio']),
                            'em_proc' => (int) trim($row['pass_table']),
                            'ul_turn' => (int) trim($row['ultimo_turno']),
                            'no_turn' => trim($row['nomb_turno'])
                        );
                    }
                }
            }
        }

        return $array;
    }

    function retFecAutorizada() {

        $array = null;

        $Sql = " select top 3 fe_desde,fe_hasta 
        from ca_turnos_empl_intranet 
        group by fe_desde,fe_hasta
        order by fe_desde desc;";
        $this->db = $this->load->database('nomina', true);
        $resultado = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            $error = & load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR FECHAS <BR> COD_ERR:" . $this->db->_error_number(), $this->db->_error_message(), 'error_db');
            die;
        }
        $res = $resultado->result_array();
        if (count($res) > 0) {
            foreach ($res as $row) {
                $array[] = array(
                    'fe_des' => !empty($row['fe_desde']) ? date('Y-m-d', strtotime($row['fe_desde'])) : '',
                    'fe_has' => !empty($row['fe_hasta']) ? date('Y-m-d', strtotime($row['fe_hasta'])) : ''
                );
            }
        }

        return $array;
    }

    function deleTurnos($data) {
        $array = null;
        if (!empty($data['f_i']) && !empty($data['f_f']) && !empty($data['c_c']) && !empty($data['c_e'])) {

            $Sql = " PERSONAL_PI.._sp_colaborador_turnos_elimina '" . trim($data['f_i']) . "','" . trim($data['f_f']) . "'," . trim($data['c_c']) . "," . trim($data['c_e']) . " ";
            $this->db = $this->load->database('nomina', true);

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL INGRESAR TURNOS: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }
            $row = $result->row_array();
            $array = array(
                'co_err' => (int) $row['co_error'],
                'tx_err' => utf8_decode($row['tx_error'])
            );
        }

        return array('data' => $array);
    }

    function procTurnos($data) {

        $array = null;

        if (!empty($data['f_i']) && !empty($data['f_f']) && !empty($data['c_c'])) {

            $arr = json_decode($data['c_c']);

            for ($i = 0; $i < count($arr); $i++) {

                $Sql = " PERSONAL_PI.._sp_colaborador_turnos_procesar '" . trim($data['f_i']) . "','" . trim($data['f_f']) . "'," . trim($arr[$i]) . ";";
                $this->db = $this->load->database('nomina', true);

                $result = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    return "ERROR AL PROCESAR TURNOS: " . $this->db->_error_number() . " " . $this->db->_error_message();
                }
                $row = $result->row_array();
                $array = array(
                    'co_err' => (int) $row['co_error'],
                    'tx_err' => utf8_decode($row['tx_error'])
                );
            }
        }

        return $array;
    }

    function retCecoProce($f_i, $f_f) {

        $array = null;

        if (!empty($f_i) && !empty($f_f)) {

            $Sql = " SELECT a.co_ceco,f.NOMBRE+'' AS nom_ceco,a.co_empleado,c.NOMBRE,c.APELLIDO,b.COD_CARGO, ";
            $Sql .= " d.NOMBRE+'' as NOMB_CARGO,ISNULL(a.co_turno,0)+'' as co_turno,e.DESCRIPCION,a.pass_table ";
            $Sql .= " FROM ca_turnos_empl_intranet a ";
            $Sql .= " INNER JOIN RH_EMPLOYEE b ";
            $Sql .= " ON a.co_empleado = b.CODIGO ";
            $Sql .= " AND b.COD_EMPRESA = 27 ";
            $Sql .= " INNER JOIN RH_PERSON c ";
            $Sql .= " ON b.CODIGO = c.CODIGO ";
            $Sql .= " INNER JOIN RH_CARGO d ";
            $Sql .= " ON b.COD_CARGO = d.COD_CARGO ";
            $Sql .= " LEFT JOIN CA_TURNOS e ";
            $Sql .= " ON a.co_turno = e.COD_TURNO ";
            $Sql .= " AND e.COD_EMPRESA = 27 ";
            $Sql .= " INNER JOIN rh_ccosto f ";
            $Sql .= " ON a.co_ceco = f.COD_CCOSTO ";
            $Sql .= " AND f.COD_EMPRESA = 27 ";
            $Sql .= " WHERE CONVERT(VARCHAR(10),a.fe_desde,120) = '$f_i' ";
            $Sql .= " AND CONVERT(VARCHAR(10),a.fe_hasta,120) = '$f_f' ";
//            $Sql .= " AND co_turno > 0 ";
            $Sql .= " AND pass_table = 1 ";
            $Sql .= " ORDER BY a.co_ceco,a.co_turno,c.APELLIDO; ";

            $this->db = $this->load->database('nomina', true);
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL OBTENER CENTROS DE COSTO PROCESADOS: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }

            if (count($resultado->result_array()) > 0) {

                foreach ($resultado->result_array() as $row) {
                    $array[] = array(
                        'co_ceco' => (int) trim($row['co_ceco']),
                        'no_ceco' => trim(utf8_encode($row['nom_ceco'])),
                        'co_empl' => (int) trim($row['co_empleado']),
                        'no_empl' => trim(utf8_encode($row['NOMBRE'])),
                        'ap_empl' => trim(utf8_encode($row['APELLIDO'])),
                        'co_carg' => trim($row['COD_CARGO']),
                        'no_carg' => trim(utf8_encode($row['NOMB_CARGO'])),
                        'co_turn' => (int) trim($row['co_turno']),
                        'no_turn' => trim(utf8_encode($row['DESCRIPCION'])),
                        'em_proc' => (int) trim($row['pass_table'])
                    );
                }
            }
        }

        return array('data' => $array);
    }

    function saveTurnos($cab, $det) {

        $array = null;

        if (!empty($cab['f_i']) && !empty($cab['f_f']) && !empty($cab['c_c'])) {

            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;

            ##<turnos>

            $cabecera = $dom->createElement('turnos');
            $dom->appendChild($cabecera);

            foreach ($det as $row) {
                $item = $dom->createElement('item');
                $cabecera->appendChild($item);

                $n_1 = $dom->createElement("co_use", trim($row['c_u']));
                $item->appendChild($n_1);

                $n_2 = $dom->createElement("co_tur", trim(utf8_decode($row['c_t'])));
                $item->appendChild($n_2);
            }

            ##</turnos>

            $xml = $dom->saveXML();
            $Sql = " PERSONAL_PI.._sp_colaborador_turnos_ingreso '" . trim($cab['f_i']) . "','" . trim($cab['f_f']) . "'," . trim($cab['c_c']) . "," . trim($_SESSION['c_e']) . ",'" . $xml . "'; ";
//            echo $Sql;
//            die;
            $this->db = $this->load->database('nomina', true);

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL INGRESAR TURNOS: " . $this->db->_error_number() . " " . $this->db->_error_message();
            }
            $row = $result->row_array();
            $array = array(
                'co_err' => (int) $row['co_error'],
                'tx_err' => utf8_decode($row['tx_error'])
            );
        }

        return $array;
    }

}

?>
