<?php

class m_8800 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }

    function retEmpresaAsignadas(){

        if(!empty($_SESSION['c_e'])){
            $Sql = " webpycca.._get_almacenes_usuario ".$_SESSION['c_e'].",'A' ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR EMPRESAS <BR> COD_ERR:".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            
            $array = null;
            
            if (count($resultado->result_array())> 0){
                foreach ($resultado->result_array() as $row) {

                    $array[] = array(
                        'cod_bdg'=> utf8_encode(trim($row['co_bodega'])),      
                        'des_bdg'=> utf8_encode(trim($row['descripcion'])), 
                        'ruc_bdg'=> trim($row['ruc']),       
                        'de_sri'=> trim($row['de_sri']),       
                        'ip'=> trim($row['de_ip_address']),         
                        'dir_bdg'=> utf8_encode(trim($row['direccion'])),      
                        'tip_bdg'=> utf8_encode(trim($row['tipo']))
                    );

                }

                return $array;
            }
        }
    }
    
    function retCliente($data){
        if(!empty($data)){
            
            $Sql = " SGDBSEGURIDAD.._consulta_cliente '".$data."' ";
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR CLIENTE <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            $array = null;

            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row) {

                    $array[] = array(
                        'nom_cli'=> trim($row['perNombre1']),
                        'dir_cli'=> trim(utf8_encode($row['direccion'])),
                        'tel_cli'=> trim($row['telefono']),
                        'mail_cli'=> trim($row['correo'])
                    );
                }
                
            }
            
            return array('data'=>$array);
            
        }
    }
        
    function saveReclamo($json){
        
        if (!empty($_SESSION['c_e'])){
            
            if(!empty($json)){
                    
                $array_cab = json_decode($json['c_a'],true);
                
                $dom = new DOMDocument("1.0");
                $dom->preserveWhiteSpace = false; 
                $dom->formatOutput = true; 
                    
                ##<cabecera>

                    $cabecera = $dom->createElement('cabecera');
                    $dom->appendChild($cabecera);

                    $n_1 = $dom->createElement("id_cliente",trim($array_cab[0]['id_cliente'])); 
                    $cabecera->appendChild($n_1);
                    
                    $n_2 = $dom->createElement("nom_cli",utf8_decode(trim($array_cab[0]['nom_cli']))); 
                    $cabecera->appendChild($n_2);

                    $n_3 = $dom->createElement("direccion",utf8_decode(trim($array_cab[0]['direccion']))); 
                    $cabecera->appendChild($n_3);

                    $n_4 = $dom->createElement("telf_conve",trim($array_cab[0]['telf_conve'])); 
                    $cabecera->appendChild($n_4);

                    $n_5 = $dom->createElement("telf_celu",trim($array_cab[0]['telf_celu'])); 
                    $cabecera->appendChild($n_5);

                    $n_6 = $dom->createElement("email",utf8_decode(trim($array_cab[0]['email']))); 
                    $cabecera->appendChild($n_6);

                    $n_7 = $dom->createElement("tipo",trim($array_cab[0]['tipo'])); 
                    $cabecera->appendChild($n_7);

                    $n_8 = $dom->createElement("num_fac",trim($array_cab[0]['num_fac'])); 
                    $cabecera->appendChild($n_8);
                    
                    $n_9 = $dom->createElement("arti_deja",utf8_decode(trim($array_cab[0]['arti_deja']))); 
                    $cabecera->appendChild($n_9);
                    
                    $n_10 = $dom->createElement("descripcion",utf8_decode(trim($array_cab[0]['descripcion']))); 
                    $cabecera->appendChild($n_10);

                    $n_11 = $dom->createElement("objetivo",utf8_decode(trim($array_cab[0]['objetivo']))); 
                    $cabecera->appendChild($n_11);
					
					$n_12 = $dom->createElement("fech_reclamo",trim($array_cab[0]['fech_reclamo']." ".$array_cab[0]['hora'])); 
                    $cabecera->appendChild($n_12);
                   
				    $n_13 = $dom->createElement("fech_termino",trim($array_cab[0]['fech_termino']." ".$array_cab[0]['hora'])); 
                    $cabecera->appendChild($n_13);
                    
                    $n_14 = $dom->createElement("bit_mail",trim($array_cab[0]['bit_mail'])); 
                    $cabecera->appendChild($n_14);
                    
                    $n_15 = $dom->createElement("bit_conv",trim($array_cab[0]['bit_conv'])); 
                    $cabecera->appendChild($n_15);
                    
                    $n_16 = $dom->createElement("bit_celular",trim($array_cab[0]['bit_celular'])); 
                    $cabecera->appendChild($n_16);
                    
					$n_17 = $dom->createElement("no_enci",trim($array_cab[0]['no_enci'])); 
                    $cabecera->appendChild($n_17);
					
					$n_18 = $dom->createElement("pin_carga",trim($array_cab[0]['pin_carga'])); 
                    $cabecera->appendChild($n_18);
					
					$n_19 = $dom->createElement("se_descarga",trim($array_cab[0]['se_descarga'])); 
                    $cabecera->appendChild($n_19);
					
					$n_20 = $dom->createElement("se_caliente",trim($array_cab[0]['se_caliente'])); 
                    $cabecera->appendChild($n_20);
					
					$n_21 = $dom->createElement("se_apaga",trim($array_cab[0]['se_apaga'])); 
                    $cabecera->appendChild($n_21);
					
					$n_22 = $dom->createElement("tactil_nofunciona",trim($array_cab[0]['tactil_nofunciona'])); 
                    $cabecera->appendChild($n_22);
					
					$n_23 = $dom->createElement("audio_nfunc",trim($array_cab[0]['audio_nfunc'])); 
                    $cabecera->appendChild($n_23);
					
					$n_24 = $dom->createElement("tec_nfunc",trim($array_cab[0]['tec_nfunc'])); 
                    $cabecera->appendChild($n_24);
					
					$n_25 = $dom->createElement("micro_nfunc",trim($array_cab[0]['micro_nfunc'])); 
                    $cabecera->appendChild($n_25);
					
					$n_26 = $dom->createElement("se_baja",trim($array_cab[0]['se_baja'])); 
                    $cabecera->appendChild($n_26);
					
					$n_27 = $dom->createElement("no_wifi",trim($array_cab[0]['no_wifi'])); 
                    $cabecera->appendChild($n_27);
					
					$n_28 = $dom->createElement("msj_error",trim($array_cab[0]['msj_error'])); 
                    $cabecera->appendChild($n_28);
					
					$n_29 = $dom->createElement("disp_bloq",trim($array_cab[0]['disp_bloq'])); 
                    $cabecera->appendChild($n_29);
					
					$n_30 = $dom->createElement("pant_bloq",trim($array_cab[0]['pant_bloq'])); 
                    $cabecera->appendChild($n_30);
					
					$n_31 = $dom->createElement("desprog",trim($array_cab[0]['desprog'])); 
                    $cabecera->appendChild($n_31);
										
					$n_32 = $dom->createElement("no_sistoper",trim($array_cab[0]['no_sistoper'])); 
                    $cabecera->appendChild($n_32);
					
					$n_33 = $dom->createElement("otros_problema",trim($array_cab[0]['otros_problema'])); 
                    $cabecera->appendChild($n_33);
		
					$n_34 = $dom->createElement("pant_rota",trim($array_cab[0]['pant_rota'])); 
                    $cabecera->appendChild($n_34);
					
					$n_35 = $dom->createElement("carc_golp",trim($array_cab[0]['carc_golp'])); 
                    $cabecera->appendChild($n_35);
					
					$n_36 = $dom->createElement("carc_raya",trim($array_cab[0]['carc_raya'])); 
                    $cabecera->appendChild($n_36);
					
					$n_37 = $dom->createElement("disp_moja",trim($array_cab[0]['disp_moja'])); 
                    $cabecera->appendChild($n_37);
					
					$n_38 = $dom->createElement("disp_mani",trim($array_cab[0]['disp_mani'])); 
                    $cabecera->appendChild($n_38);
					
					$n_39 = $dom->createElement("endi",trim($array_cab[0]['endi'])); 
                    $cabecera->appendChild($n_39);
					
					$n_40 = $dom->createElement("dispo",trim($array_cab[0]['dispo'])); 
                    $cabecera->appendChild($n_40);
		
					$n_41 = $dom->createElement("bateria",trim($array_cab[0]['bateria'])); 
                    $cabecera->appendChild($n_41);
					
					$n_42 = $dom->createElement("cargador",trim($array_cab[0]['cargador'])); 
                    $cabecera->appendChild($n_42);
					
					$n_43 = $dom->createElement("cables",trim($array_cab[0]['cables'])); 
                    $cabecera->appendChild($n_43);
					
					$n_44 = $dom->createElement("tapa_post",trim($array_cab[0]['tapa_post'])); 
                    $cabecera->appendChild($n_44);
					
					$n_45 = $dom->createElement("cable_poder",trim($array_cab[0]['cable_poder'])); 
                    $cabecera->appendChild($n_45);
					
					$n_46 = $dom->createElement("cont_remo",trim($array_cab[0]['cont_remo'])); 
                    $cabecera->appendChild($n_46);
					
					$n_47 = $dom->createElement("audifono",trim($array_cab[0]['audifono'])); 
                    $cabecera->appendChild($n_47);
					
					$n_48 = $dom->createElement("caja_orig",trim($array_cab[0]['caja_orig'])); 
                    $cabecera->appendChild($n_48);
					
					$n_49 = $dom->createElement("manual",trim($array_cab[0]['manual'])); 
                    $cabecera->appendChild($n_49);
                    
					$n_50 = $dom->createElement("otros_entrega",trim($array_cab[0]['otros_entrega'])); 
                    $cabecera->appendChild($n_50);
                    
				##</cabecera>
                ##<plandepago>
                    
                    $array_articulos = json_decode($json['d_a'],true);
                    
                    $plan_pago = $dom->createElement('articulos');
                    $dom->appendChild($plan_pago);
                    
                    for ($i = 0; $i < count($array_articulos); $i++){
                        
                        $items = $dom->createElement("item"); 
                        $plan_pago->appendChild($items);
                        
                        $n_50 = $dom->createElement("cod_articulo",trim($array_articulos[$i]['cod_art'])); 
                        $items->appendChild($n_50);
                        
                        //$n_51 = $dom->createElement("descripcion",trim($array_articulos[$i]['descripcion'])); 
                        //$items->appendChild($n_51);
						
						$n_51 = $dom->createElement("modelo",trim($array_articulos[$i]['modelo_art'])); 
                        $items->appendChild($n_51);
						
						$n_52 = $dom->createElement("serie",trim($array_articulos[$i]['serie'])); 
                        $items->appendChild($n_52);
						
						//$n_20 = $dom->createElement("cant",trim($array_articulos[$i]['cant'])); 
                        //$items->appendChild($n_20);              
						                          
											
                    }
                    
                ##</plandepago>
                    
                $xml = $dom->saveXML();
                
                $Sql = " WEBPYCCA.._reclamos_ingreso_averiados ".trim($_SESSION['c_e']).",".trim($_SESSION['cod_almacen']).",'".trim($xml)."'; ";
                //echo $Sql;
                //die;
                $resultado = $this->db->query($Sql);    
                if ($this->db->_error_message() <> ''){
                    return "ERROR AL INGRESAR RECLAMO <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                    die;
                }
                $row = $resultado->row_array();
                
                $arr_error = array(   
                            'a'=> (int)$row['co_error'],
                            'b'=> utf8_encode(trim($row['tx_error'])),
                            'c'=> trim($row['cod_soli']),
                            'd'=> trim($row['des_bdg']));

                return array('data'=>$arr_error);
                
            }
        }
    }
    
    function retGrupoUsuariosAlmacen($cod_bdg){
        if(!empty($cod_bdg)){
            
            $Sql = " WEBPYCCA.._reclamos_grupos_averiados ".trim($cod_bdg);
            $resultado = $this->db->query($Sql);  
            if ($this->db->_error_message() <> ''){
                return "ERROR AL RETORNAR GRUPO RECLAMOS <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            $array = null;
            
            foreach ($resultado->result_array() as $row) {
                $array[] = array(   
                    'mail'=> trim($row['e_mail'])
                ); 
            }

            return $array;
            
        }
    }
    
    function retFacturas($data){
        if(!empty($data)){
            
            $Sql = " PODBPOS_CIERRES.._consulta_documento '".trim($data['c_i'])."',".trim($data['a_o']).",".trim($data['m_s']);
            $resultado = $this->db->query($Sql);
            
            if ($this->db->_error_message() <> ''){
                return "ERROR AL CONSULTAR FACTURAS <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            $array = null;

            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row) {
                    
                    $array[] = array(
                        
                        'fec_doc'=> date('Y/m/d',strtotime(trim($row['fe_transaccion']))),
                        'cod_bod'=> trim($row['co_bodega']),
                        'des_bod'=> utf8_encode(trim($row['descripcion'])),
                        'tip_doc'=> trim($row['ti_documento']),
                        'tip_nom'=> trim($row['ti_documento']) === 'F'?'Factura':'Nota de Credito',
                        'num_sri'=> str_pad(trim($row['sri']),3,0, STR_PAD_LEFT),
                        'num_caj'=> str_pad(trim($row['co_caja']),3,0, STR_PAD_LEFT),
                        'num_doc'=> str_pad(trim($row['nu_documento']),9,0, STR_PAD_LEFT),
                        'val_doc'=> (float)trim($row['va_totaltransaccion']));
                }
            }
            
            return array('data'=>$array);
            
        }
    }
    
    function retDetFacturas($data){
        if(!empty($data)){
            
            $Sql = " PODBPOS_CIERRES.._consulta_documento '','','','".trim($data['t_d'])."',".(int)$data['c_i'].",".(int)$data['c_c'].",".(int)$data['c_d'];
            //echo $Sql;
            $resultado = $this->db->query($Sql);
            
            if ($this->db->_error_message() <> ''){
                return "ERROR AL CONSULTAR DETALLES DOCUMENTO <BR> COD_ERR:".$this->db->_error_number()." <BR> ".$this->db->_error_message();
                die;
            }
            
            $array = null;

            if (count($resultado->result_array())> 0){
                
                foreach ($resultado->result_array() as $row) {
                    
                    $array[] = array(
                        
                        'cod_art'=> trim($row['co_articulo']),
                        'des_art'=> utf8_encode(trim($row['ds_articulo'])),
                        'cat_art'=> (int)trim($row['va_cantidad']),
                        'val_art'=> (float)trim($row['va_pvpventa']),
                        'dev_art'=> (int)trim($row['va_cantidaddyc']));
                }
                
            }
            
            return array('data'=>$array);
        }
    }
    
}

?>

