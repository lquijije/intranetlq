<?php

class m_116 extends CI_Model{

   function __construct()
    {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function ValidaComedor(){
        
        $array = null;
        $Sql  = " SELECT status=isnull(status,'X'), extra_time FROM ROL_CLUBDENTPAR ";
        $DB1 = $this->load->database('nomina', true);
        $query = $DB1->query($Sql);
        if ($DB1->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL VALIDAR COMEDOR: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
            die;
        } else {
            $row = $query->row_array();
            $array = array(
                'status'=>$row['status'],
                'extra' =>$row['extra_time']
            );
        }
        
        return $array;
        
    }
    
    function getComedor($extra){
       
        $array = null;
        
        if (isset($_SESSION['c_e'])){
            //$Sql  = " personal_pi.dbo.NOMICOM008 ".$_SESSION['c_e'].",'',1";
            $Sql  = " personal_pi.dbo.NOMICOM008 ".$_SESSION['c_e'].",'',".$extra;

            $DB1 = $this->load->database('nomina', true);
            $query = $DB1->query($Sql);
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL RETORNAR COMEDOR: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                log_message('ERROR',"ERROR AL RETORNAR COMEDOR:".$DB1->_error_number()." ".$DB1->_error_message()."*********".$Sql);
                die;
            } else {
                $res = $query->result_array();
                if(count($res) > 0){
                    foreach ($res as  $row){
                        $array[] = array(
                            'dia'=>$row['dia'],
                            'fecha'=>$row['fecha'],
                            'ingre'=>$row['fecha_ingreso'],
                            'jugo'=>$row['texto_jugo'],
                            'postr'=>$row['texto_postre'],
                            'dieta'=>$row['texto_dieta'],
                            'nor_1'=>$row['texto_normal'],
                            'nor_2'=>$row['texto_normal2'],
                            'aco_1'=>$row['texto_acompanante'],
                            'aco_2'=>$row['texto_acompanante2']
                        );
                    }
                }
            }
        }
        return $array;
    }
    
    function saveComedor($datos){
        
        $error = '';
        $arraSave = null;
        
        if (!empty($datos)){
            
            $array = json_decode($datos['j_n'],true);
            $array = json_decode($array,true);
            
            $dom = new DOMDocument("1.0");
            $dom->preserveWhiteSpace = false; 
            $dom->formatOutput = true; 
            $comedor = $dom->createElement('comedor');
            $dom->appendChild($comedor);
            
            for ($i = 0; $i<count($array); $i++){
                
                $items = $dom->createElement("items"); 
                $comedor->appendChild($items);
                $n_1 = $dom->createElement("fecha",trim($array[$i]['fecha'])); 
                $items->appendChild($n_1);
                $n_2 = $dom->createElement("tipo",trim($array[$i]['id'])); 
                $items->appendChild($n_2);
                $n_3 = $dom->createElement("acom",trim($array[$i]['text_2'])); 
                $items->appendChild($n_3);

            }
            
            $xml = $dom->saveXML();
            
            $Sql = " PERSONAL_PI.._comedor_comprar ".$_SESSION['c_e'].",'".$xml."' ";
            $DB1 = $this->load->database('nomina', true);
            $result = $DB1->query($Sql);
             
            if ($DB1->_error_message() <> ''){
                $error = "ERROR Al COMPRAR: ".$DB1->_error_number()." ".$DB1->_error_message();
                log_message('ERROR',"ERROR AL COMPRAR:".$DB1->_error_number()." ".$DB1->_error_message()."*********".$Sql);
            } else {
                $row = $result->row_array();
                if(count($row)>0){
                    $arraSave = array (
                        'co_err'=>(int)$row['co_error'],
                        'tx_err'=>trim($row['tx_error'])
                    );
                }
            }
            
        }
        
        return array('data'=>$arraSave,'err'=>$error);
        
    }
    
    function valid_feriado($fec){
        if(!empty($fec)){
            
            $Sql  = " SELECT anio,mes,dia,tipo ";
            $Sql .= " FROM personal_pi..rol_clubdent_feriados ";
            $Sql .= " WHERE anio = ".date('Y',strtotime($fec));
            $Sql .= " AND mes = ".(int)date('m',strtotime($fec));
            $Sql .= " AND dia = ".(int)date('d',strtotime($fec));
            
            $DB1 = $this->load->database('nomina', true);
            $query = $DB1->query($Sql);
            
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL VALIDAR FERIADO: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                log_message('ERROR',"ERROR AL VALIDAR FERIADO:".$DB1->_error_number()." ".$DB1->_error_message()."*********".$Sql);
                die;
            }
            
            $result = $query->row_array();
            
            if(!empty($result)){
                return $result['tipo'];
            }
            
        }
    }
    
    function ticketxDia($fec_ini,$fec_fin=null){
        
        $array = null;
        $subArray = null;
        $salto = '';
        $salto_1 = '';
        
        if(!empty($fec_ini)){
            
            if (!empty($fec_ini) && !empty($fec_fin)){
                $Sql = " _comedor_tixket_x_dia ".trim($_SESSION['c_e']).",'".date('Y-m-d',strtotime($fec_ini))." 00:00','".date('Y-m-d',strtotime($fec_fin))." 23:59' ";
            } else {
                $Sql = " _comedor_tixket_x_dia ".trim($_SESSION['c_e']).",'".date('Y-m-d',strtotime($fec_ini))." 00:00','".date('Y-m-d',strtotime($fec_ini))." 23:59' ";
            }
            
            $DB1 = $this->load->database('nomina', true);
            $query = $DB1->query($Sql);
            
            if ($DB1->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                log_message('ERROR',"ERROR AL RETORNAR TICKET POR DIA:".$DB1->_error_number()." ".$DB1->_error_message()."*********".$Sql);
                echo $error->show_error("ERROR AL RETORNAR TICKET POR DIA: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
                die;
            } else {
                
                $res = $query->result_array();
                
                if(count($res)> 0){
                    
                    foreach ($res as  $row){

                        if($salto <> trim(date('Y-m-d',strtotime($row['fecha_consumo'])))){
                            $subArray = null;
                            foreach ($query->result_array() as  $raw){
                                if($salto === $salto_1){
                                    $subArray [] = array(
                                        'tip'=>$row['tipo'],
                                        'aco'=>utf8_encode($row['acompanante']),
                                        'des'=>utf8_encode($row['plato'])
                                    );
                                }
                                $salto_1 = trim(date('Y-m-d',strtotime($raw['fecha_consumo'])));
                            }

                            $array [] = array(
                                'fecha'=>conveDia(date('w', strtotime($row['fecha_consumo'])))." ".date('d',strtotime($row['fecha_consumo'])),
                                'date'=>date('Y/m/d', strtotime($row['fecha_consumo'])),
                                'item'=>$subArray
                            );

                        }

                        $salto = trim(date('Y-m-d',strtotime($row['fecha_consumo'])));

                    }
                }
            }
            
        }
            
        return $array;
        
    }

}
?>
