<?php
ini_set('max_execution_time', 300);

class m_121 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function aprobarSolicit($data){
        
        if(!empty($data)){
            
            $array = null;
            $mensaje = null;
            
            $arr = explode(',',$data['s_a']);
            $arr1 = explode(',',$data['s_n']);
            
            if(!empty($data['s_a'])){
                
                for ($i=0;$i < count($arr); $i++){
                    $array[]=array('empr'=>$data['i_e'],'soli'=>$arr[$i],'tip'=>'A');
                }
                
            }
            
            if(!empty($data['s_n'])){
                
                for ($i=0;$i < count($arr1); $i++){
                    $array[]=array('empr'=>$data['i_e'],'soli'=>$arr1[$i],'tip'=>'N');
                }
                
            }
            
            for ($a = 0; $a < count($array); $a++){
                
                $Sql = "WEBPYCCA.._aprobar_oc ".$array[$a]['empr'].", ".$_SESSION['c_e'].",".$array[$a]['soli'].",'".$array[$a]['tip']."' ";
                $query = $this->db->query($Sql);
                if ($this->db->_error_message() <> ''){
                    $mensaje[]= array('cod_sol'=>$array[$a]['soli'],'cod_err'=>1,'txt_err'=>'Error al ejecutar Proc');
                } else {
                    $row = $query->row_array();
                    $mensaje[]= array('cod_sol'=>$array[$a]['soli'],'cod_err'=>$row['co_error'],'txt_err'=>utf8_encode($row['tx_error']));
                }
                
            }
            
            return $mensaje;
            
        }
    }
    
    function retSolicitudes($cod_emp){
            
        $array = null;
        
        if (!empty($cod_emp)){
            
            $Sql = " WEBPYCCA.._aprobacion_oc ".$cod_emp;
            $query = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER ORDENES DE COMPRA: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            }
            foreach ($query->result_array() as $row) {
                $array[] = array(
                    'cod_ord'=>trim($row['codi_orden_compra']),
                    'cod_pro'=>trim($row['codi_proveedor']),
                    'nom_pro'=>utf8_encode(trim($row['nomb_proveedor'])),
                    'des_obs'=>utf8_encode(trim($row['desc_observaciones'])),
                    'vol_fob'=>trim($row['valo_fob']),
                    'num_ite'=>trim($row['nume_items']),
                    'fec_ing'=>!empty($row['fech_ingreso'])?date('Y-m-d',strtotime($row['fech_ingreso'])):'',
                    'cod_mon'=>trim($row['codi_moneda']),
                    'nom_mon'=>trim($row['nomb_mone']),
                    'cod_fab'=>trim($row['codi_fabricante'])
                );
            }
               
        }
        
        return $array;
        
    }
    
    function retDetSolicitudes($cod_emp,$cod_sol){
            
        $array = null;
        $error = '';
        if (!empty($cod_emp) && !empty($cod_sol)){
            
            $Sql = " WEBPYCCA.._aprobacion_oc_det $cod_emp, $cod_sol ";
            
            $query = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error = "ERROR AL OBTENER DETALLE OC";
            }
            foreach ($query->result_array() as $row) {
                $array[] = array(
                    'cod_sec'=>trim($row['codi_secuencia']),
                    'cod_ite'=>trim($row['codi_item']),
                    'cod_fab'=>trim($row['codi_fabrica']),
                    'des_ite'=>utf8_encode(trim($row['desc_item'])),
                    'val_fob'=>trim($row['valo_fob']),
                    'val_ped'=>(int)trim($row['valo_pedido_unidades']),
                    'val_tot'=>trim($row['total'])
                );
            }
               
        }
        
        return array('dat'=>$array,'error'=>$error);
        
    }
    
    
}
?>
