<?php

class m_120 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retPromo($cod_pro){
        
        $array = null;
        $error = '';
        
        if (!empty($cod_pro)){
            
            $Sql  = " SELECT co_promocion,co_articulo,ds_articulo,img_face,img, ";
            $Sql .= " pre_inicial,pre_base,pre_record,shared,shared_meta, ";
            $Sql .= " fe_ini_promocion,fe_fin_promocion,estado ";
            $Sql .= " FROM tb_articulo_promocion ";      
            $Sql .= " WHERE co_promocion = $cod_pro; ";
            
            $DB2 = $this->load->database('db_preciorecord', true);
            $query = $DB2->query($Sql);
            if ($DB2->_error_message() <> ''){
                $error = "ERROR AL RETORNAR PROMOCION <BR> COD_ERR: ".$DB2->_error_number()."<br>".$DB2->_error_message();
            } else {
                if(count($query->result_array()) > 0){
                    foreach ($query->result_array() as $row){
                        $array[]= array(
                            'co_p'=>$row['co_promocion'],
                            'co_a'=>$row['co_articulo'],
                            'ds_a'=>$row['ds_articulo'],
                            'im_f'=>$row['img_face'],
                            'im_p'=>$row['img'],
                            'pr_i'=>$row['pre_inicial'],
                            'pr_b'=>$row['pre_base'],
                            'pr_r'=>$row['pre_record'],
                            'cn_s'=>$row['shared'],
                            'me_s'=>$row['shared_meta'],
                            'fe_i'=>!empty($row['fe_ini_promocion'])?date('Y/m/d',strtotime($row['fe_ini_promocion'])):'',
                            'fe_f'=>!empty($row['fe_fin_promocion'])?date('Y/m/d',strtotime($row['fe_fin_promocion'])):'',
                            'es_p'=>$row['estado']
                        );
                    }
                }
            }
        }
        
        return array('data'=>$array,'err'=>$error);
    }
    
    function retArt($cod_art){
        
        $array = null;
        $error = '';

        if(!empty($cod_art)){
            
            $Sql = " EXEC get_articulo '$cod_art' ";
            $query = $this->db->query($Sql); 
            
            if ($this->db->_error_message() <> ''){
                $error = "ERROR AL RETORNAR ARTICULO <BR> COD_ERR:".$this->db->_error_number()." - ".$this->db->_error_message();
            } else {

                $row = $query->row_array();

                if(isset($row['ds_articulo']) && !empty($row['ds_articulo'])){
                    $array = array (
                        'ds_art'=> utf8_decode($row['ds_articulo']),
                        'pr_art'=> $row['pvpiva']            
                    );
                } else {
                    $error = 'No hay datos en la busqueda realizada';
                }

            }
            
        } else {
            $error = 'No ha ingresado el codigo del art&iacute;culo';
        }
            
        return array('data'=>$array,'err'=>$error);
        
    }
    
    function savePromo($data,$img,$imgFace){
        
        $error = '';
        $result = null;
        
        if (!empty($data['_acc']) && trim($img) <> '' && trim($imgFace) <> ''){
            
            if (!empty($data['c']) && !empty($data['c_s']) && !empty($data['d']) && !empty($data['f_f']) && !empty($data['f_i']) && !empty($data['c']) && !empty($data['p_i']) && !empty($data['p_t']) ){
                
                if(trim($data['_acc']) === 'I'){
                    
                    $Sql = " PODBPOS.dbo._insertar_detalle_promocion 270,'".trim($data['c'])."',".trim($data['p_t'])." ";
                    
                    $DB1 = $this->load->database('desarrollo', true);
                    $query = $DB1->query($Sql); 

                    if ($DB1->_error_message() <> ''){
                        $error = "ERROR AL GUARDAR PROMO SQL <BR> COD_ERR:".$DB1->_error_number()." - ".$DB1->_error_message();
                    } else {
                        $row = $query->row_array();
                        if(isset($row['co_error']) && (int)$row['co_error'] == 0){
                            $nom_pro = str_replace(array("'",'"'), array('',''),trim($data['d']));
                            
                            $Sql = " CALL pr_save_promocion ('".trim($data['c'])."','".trim($nom_pro)."','".trim($img)."','".trim($imgFace)."','".trim($data['p_i'])."','".trim($data['p_t'])."','".trim($data['c_s'])."','".trim($data['f_i'])."','".trim($data['f_f'])."','".$_SESSION['c_e']."',@cod_err,@txt_err); ";
                            
                            $DB2 = $this->load->database('db_preciorecord', true);
                            $DB2->query($Sql);
                            if ($this->db->_error_message() <> ''){
                                $error = "ERROR AL GUARDAR PROMO MYSQL <BR> COD_ERR:".$DB2->_error_number()." - ".$DB2->_error_message();
                            } else {

                                $Sql = " SELECT @cod_err,@txt_err";
                                $query2 = $DB2->query($Sql);
                                if ($DB2->_error_message() <> ''){
                                    $error = "ERROR AL RET MYSQL:".$DB2->_error_number()." - ".$DB2->_error_message();
                                } 

                                $row1 = $query2->row_array();
                                $result = array('cod_err'=>$row1['@cod_err'],'txt_err'=>$row1['@txt_err']);
                            }
                        } else {
                            $error = $row['tx_error'];
                        }
                    }
                    
                } else if(trim($data['_acc']) === 'U'){
                    
                    $Sql  = " UPDATE tb_articulo_promocion SET ";
                    $Sql .= " ds_articulo = '".$data['d']."', ";
                    $Sql .= " pre_inicial = '".$data['p_i']."', ";
                    $Sql .= " pre_base = '".$data['p_t']."', ";
                    $Sql .= " shared_meta = '".$data['c_s']."', ";
                    $Sql .= " fe_ini_promocion = '".$data['f_i']."', ";
                    $Sql .= " fe_fin_promocion = '".$data['f_f']."', ";
                    $Sql .= " estado = '".$data['e_p']."' ";
                    $Sql .= " WHERE co_promocion = ".$data['c_p']."; ";
                    $DB2 = $this->load->database('db_preciorecord', true);
                    $DB2->query($Sql);
                    if ($this->db->_error_message() <> ''){
                        $error = "ERROR AL ACTUALIZAR PROMO MYSQL <BR> COD_ERR:".$DB2->_error_number()." - ".$DB2->_error_message();
                    } else {
                        $result = array('cod_err'=>0,'txt_err'=>'Actualizado Correctamente');
                    }
                    
                }
            }
        }
        
        return array('data'=>$result,'error'=>$error);
        
    }
    
    function codeImgs(){
        
        $array = null;
        $error = '';

        $Sql  = 'SELECT SUBSTRING_INDEX(img,".",1) AS codes FROM tb_articulo_promocion;';

        $DB2 = $this->load->database('db_preciorecord', true);
        $query = $DB2->query($Sql);
        if ($DB2->_error_message() <> ''){
            $error = "ERROR AL RETORNAR CODIGOS <BR> COD_ERR: ".$DB2->_error_number()."<br>".$DB2->_error_message();
        } else {
            if(count($query->result_array()) > 0){
                foreach ($query->result_array() as $row){
                    $array[] = array(
                        'code'=>$row['codes']
                    );
                }
            }
        }
        return array('data'=>$array,'err'=>$error);
    }
    
    function retLisPro(){
        
        $array = null;
            
        $Sql  = " SELECT co_promocion,co_articulo,ds_articulo,img_face,img, ";
        $Sql .= " pre_inicial,pre_base,pre_record,shared,shared_meta, ";
        $Sql .= " fe_ini_promocion,fe_fin_promocion,estado ";
        $Sql .= " FROM tb_articulo_promocion ";                            

        $DB2 = $this->load->database('db_preciorecord', true);
        $query = $DB2->query($Sql); 

        if ($DB2->_error_message() <> ''){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR LISTA <BR> COD_ERR: ".$DB2->_error_number(),$DB2->_error_message(), 'error_db');
            die;
        } else {
            if(count($query->result_array()) > 0){
                foreach ($query->result_array() as $row){
                    $array[]= array(
                        'co_pro'=>$row['co_promocion'],
                        'co_art'=>$row['co_articulo'],
                        'ds_art'=>utf8_decode($row['ds_articulo']),
                        'im_fac'=>$row['img_face'],
                        'im_pro'=>$row['img'],
                        'pr_ini'=>$row['pre_inicial'],
                        'pr_bas'=>$row['pre_base'],
                        'pr_rec'=>$row['pre_record'],
                        'cn_sha'=>$row['shared'],
                        'me_sha'=>$row['shared_meta'],
                        'fe_ini'=>!empty($row['fe_ini_promocion'])?date('Y/m/d',strtotime($row['fe_ini_promocion'])):'',
                        'fe_fin'=>!empty($row['fe_fin_promocion'])?date('Y/m/d',strtotime($row['fe_fin_promocion'])):'',
                        'es_pro'=> trim($row['estado']) === 'A'?'Activo':'Inactivo'
                    );
                }
            }
        }
         
        return $array;
        
    }
    
}


?>

