<?php

class m_uploadvtex extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    function _getInventar($tipo, $busqueda) {

        $array = array('dat' => '', 'err' => '');

        if (!empty($tipo) && !empty($busqueda)) {

            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });

            try {
                switch ($tipo) {
                    case 'C':
                        $Sql = " IVDBINVENTAR.dbo._consulta_articulo_bodeguero '" . trim($busqueda) . "';";
                        break;
                    case 'N':
                        $Sql = " IVDBINVENTAR.dbo._consulta_articulo_bodeguero '','" . trim($busqueda) . "';";
                        break;
                    case 'F':
                        $Sql = " IVDBINVENTAR.dbo._consulta_articulo_bodeguero '','','" . trim($busqueda) . "' ";
                        break;
                }

                $resultado = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception("ERROR AL RETORNAR ARTICULOS: " . $this->db->_error_number() . " " . $this->db->_error_message());
                }
                $res = $resultado->result_array();
                foreach ($res as $row) {
                    $array['dat'][] = array(
                        '_link' => '<a href="javascript:void(0)" onclick="modalArtInvent(' . "'" . htmlentities(json_encode($row, true)) . "'" . ');">' . trim($row['co_articulo']) . '</a>',
                        'cod_art' => $row['co_articulo'],
                        'ds_art' => utf8_encode($row['ds_articulo']),
                        'co_fab' => utf8_encode($row['co_fabrica'])
                    );
                }
            } catch (Exception $exc) {
                $array['err'] = $exc->getMessage();
            }
        } else {
            $array['err'] = 'PARAMETROS INCOMPLETOS';
        }

        return $array;
    }

    function consultaArticulo($cod_art) {

        $array = array('dat' => '', 'err' => '');

        if (!empty($cod_art)) {

            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });

            try {
                $Sql = " EXEC get_articulo '$cod_art' ";
                $query = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception("ERROR AL RETORNAR ARTICULOS: " . $this->db->_error_number() . " " . $this->db->_error_message());
                }
                $row = $query->row_array();
                $array = array(
                    'ds_art' => utf8_decode($row['ds_articulo']),
                    'pr_art' => $row['pvpiva']
                );
            } catch (Exception $exc) {
                $array['err'] = $exc->getMessage();
            }
        } else {
            $array['err'] = 'PARAMETROS INCOMPLETOS';
        }

        return $array;
    }

}
?>

