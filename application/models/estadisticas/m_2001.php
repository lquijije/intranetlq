<?php

class m_2001 extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function retVentasPorDia($fe_ini,$co_tip){
        
        $error = '';
        $array = $arrayCompl = null;

        if(!empty($fe_ini) && !empty($co_tip)){
            
            $Sql  = " db_estadisticas..sp_ventas_horaOK '".trim($fe_ini)."','', '', '".trim($co_tip)."';";
            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER ESTADISTICAS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } else {
                $res = $result->result_array();
                
                if(!empty($res)){
                    
                    foreach ($res as $row){
                        
                        $array[] = array(
                            'co_bdg'=>$row['Bodega'],
                            'ds_bdg'=>trim(utf8_encode($row['DESCRIPCION'])),
                            'ds_ciu'=>trim(utf8_encode($row['ciudad'])),
                            
                            'ds_10'=>$row['10:00'],
                            'ds_11'=>$row['11:00'],
                            'ds_12'=>$row['12:00'],
                            'ds_13'=>$row['13:00'],
                            'ds_14'=>$row['14:00'],
                            'ds_15'=>$row['15:00'],
                            'ds_16'=>$row['16:00'],
                            'ds_17'=>$row['17:00'],
                            'ds_18'=>$row['18:00'],
                            'ds_19'=>$row['19:00'],
                            'ds_20'=>$row['20:00'],
                            'ds_21'=>$row['21:00'],
                            'ds_22'=>$row['22:00'],
                            'ds_23'=>$row['23:00'],
                            
//                            'ds_10'=>number_format($row['10:00'],0),
//                            'ds_11'=>number_format($row['11:00'],0),
//                            'ds_12'=>number_format($row['12:00'],0),
//                            'ds_13'=>number_format($row['13:00'],0),
//                            'ds_14'=>number_format($row['14:00'],0),
//                            'ds_15'=>number_format($row['15:00'],0),
//                            'ds_16'=>number_format($row['16:00'],0),
//                            'ds_17'=>number_format($row['17:00'],0),
//                            'ds_18'=>number_format($row['18:00'],0),
//                            'ds_19'=>number_format($row['19:00'],0),
//                            'ds_20'=>number_format($row['20:00'],0),
//                            'ds_21'=>number_format($row['21:00'],0),
//                            'ds_22'=>number_format($row['22:00'],0),
//                            'ds_23'=>number_format($row['23:00'],0),
                            
                            'cl_10'=>utf8_encode($row['10_clima']),
                            'cl_11'=>utf8_encode($row['11_clima']),
                            'cl_12'=>utf8_encode($row['12_clima']),
                            'cl_13'=>utf8_encode($row['13_clima']),
                            'cl_14'=>utf8_encode($row['14_clima']),
                            'cl_15'=>utf8_encode($row['15_clima']),
                            'cl_16'=>utf8_encode($row['16_clima']),
                            'cl_17'=>utf8_encode($row['17_clima']),
                            'cl_18'=>utf8_encode($row['18_clima']),
                            'cl_19'=>utf8_encode($row['19_clima']),
                            'cl_20'=>utf8_encode($row['20_clima']),
                            'cl_21'=>utf8_encode($row['21_clima']),
                            'cl_22'=>utf8_encode($row['22_clima']),
                            'cl_23'=>utf8_encode($row['23_clima'])
                        );
                        
                    }   
                }
            }

        }
        
        return array('data'=>$array,'err'=>$error);
        
    }
    
    function retVentasPorDiaPr1($fe_ini,$co_tip){
        
        $error = '';
        $array = $arrayCompl = null;

        if(!empty($fe_ini) && !empty($co_tip)){
            
            //$Sql  = " db_estadisticas..sp_ventas_horaOK '2016-03-31','', '', 'VENTA';";
            $Sql  = " db_estadisticas..sp_ventas_horaOK '2016-04-01','', '', 'VENTA';";

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER ESTADISTICAS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } else {
                $res = $result->result_array();
                
                if(!empty($res)){
                    
                    foreach ($res as $row){
                        
                        $array[] = array(
                            'co_bdg'=>$row['Bodega'],
                            'ds_bdg'=>trim(utf8_encode($row['DESCRIPCION'])),
                            'ds_ciu'=>trim(utf8_encode($row['ciudad'])), 
                            
                            'ds_10'=>$row['10:00'] <> '' ? number_format($row['10:00'],2) :'NNN',
                            'ds_11'=>$row['11:00'] <> '' ? number_format($row['11:00'],2) :'NNN',
                            'ds_12'=>$row['12:00'] <> '' ? number_format($row['12:00'],2) :'NNN',
                            'ds_13'=>$row['13:00'] <> '' ? number_format($row['13:00'],2) :'NNN',
                            'ds_14'=>$row['14:00'] <> '' ? number_format($row['14:00'],2) :'NNN',
                            'ds_15'=>$row['15:00'] <> '' ? number_format($row['15:00'],2) :'NNN',
                            'ds_16'=>$row['16:00'] <> '' ? number_format($row['16:00'],2) :'NNN',
                            'ds_17'=>$row['17:00'] <> '' ? number_format($row['17:00'],2) :'NNN',
                            'ds_18'=>$row['18:00'] <> '' ? number_format($row['18:00'],2) :'NNN',
                            'ds_19'=>$row['19:00'] <> '' ? number_format($row['19:00'],2) :'NNN',
                            'ds_20'=>$row['20:00'] <> '' ? number_format($row['20:00'],2) :'NNN',
                            'ds_21'=>$row['21:00'] <> '' ? number_format($row['21:00'],2) :'NNN',
                            'ds_22'=>$row['22:00'] <> '' ? number_format($row['22:00'],2) :'NNN',
                            'ds_23'=>$row['23:00'] <> '' ? number_format($row['23:00'],2) :'NNN',
                            
                            'cl_10'=>$row['10_clima'] <> '' ? utf8_encode($row['10_clima']) : 'NNN',
                            'cl_11'=>$row['11_clima'] <> '' ? utf8_encode($row['11_clima']) : 'NNN',
                            'cl_12'=>$row['12_clima'] <> '' ? utf8_encode($row['12_clima']) : 'NNN',
                            'cl_13'=>$row['13_clima'] <> '' ? utf8_encode($row['13_clima']) : 'NNN',
                            'cl_14'=>$row['14_clima'] <> '' ? utf8_encode($row['14_clima']) : 'NNN',
                            'cl_15'=>$row['15_clima'] <> '' ? utf8_encode($row['15_clima']) : 'NNN',
                            'cl_16'=>$row['16_clima'] <> '' ? utf8_encode($row['16_clima']) : 'NNN',
                            'cl_17'=>$row['17_clima'] <> '' ? utf8_encode($row['17_clima']) : 'NNN',
                            'cl_18'=>$row['18_clima'] <> '' ? utf8_encode($row['18_clima']) : 'NNN',
                            'cl_19'=>$row['19_clima'] <> '' ? utf8_encode($row['19_clima']) : 'NNN',
                            'cl_20'=>$row['20_clima'] <> '' ? utf8_encode($row['20_clima']) : 'NNN',
                            'cl_21'=>$row['21_clima'] <> '' ? utf8_encode($row['21_clima']) : 'NNN',
                            'cl_22'=>$row['22_clima'] <> '' ? utf8_encode($row['22_clima']) : 'NNN',
                            'cl_23'=>$row['23_clima'] <> '' ? utf8_encode($row['23_clima']) : 'NNN'
                        );
                        
                    }      
                    
                    if(!empty($array)){
                        
                        for($a=10; $a <=23;$a++){
                            
                            if($array[0]['ds_'.$a] <> 'NNN'){
                                
                                $arrayCompl[$a] = array(
                                    'hora'=>$a.':00',
                                    'data'=>null
                                );
                                
                                $data = null;
                                foreach ($array as $raw){
                                    $data[] = array (
                                        'co_bdg'=>$raw['co_bdg'],
                                        'ds_bdg'=>$raw['ds_bdg'],
                                        'ds_ciu'=>$raw['ds_ciu'],
                                        'va_hor'=>$raw['ds_'.$a],
                                        'va_cli'=>$raw['cl_'.$a]
                                    );
                                }
                                $arrayCompl[$a]['data'] = $data;
                            }
                            
                        }
                        
                    }
                    
                }
            }

        }
        
        return array('data'=>$arrayCompl,'err'=>$error);
        
    }
    
    function retVentasPorDiaPr2($fe_ini,$co_tip){
        
        $error = '';
        $array = $arrayCompl = null;

        if(!empty($fe_ini) && !empty($co_tip)){
            
            //$Sql  = " db_estadisticas..sp_ventas_horaOK '2016-03-31','', '', 'VENTA';";
            $Sql  = " db_estadisticas..sp_ventas_horaOK '2016-04-01','', '', 'VENTA';";

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error =& load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER ESTADISTICAS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } else {
                $res = $result->result_array();
                
                if(!empty($res)){
                    
                    foreach ($res as $row){
                        
                        $array[] = array(
                            'co_bdg'=>$row['Bodega'],
                            'ds_bdg'=>trim(utf8_encode($row['DESCRIPCION'])),
                            'ds_ciu'=>trim(utf8_encode($row['ciudad'])),
                            'data'=> array(
                                array(
                                    'ds' => '10:00',
                                    'va' => number_format($row['10:00'],2),
                                    'cl' => utf8_encode($row['10_clima'])
                                ),
                                array(
                                    'ds' => '11:00',
                                    'va' => number_format($row['11:00'],2),
                                    'cl' => utf8_encode($row['11_clima'])
                                ),
                                array(
                                    'ds' => '12:00',
                                    'va' => number_format($row['12:00'],2),
                                    'cl' => utf8_encode($row['12_clima'])
                                ),
                                array(
                                    'ds' => '13:00',
                                    'va' => number_format($row['13:00'],2),
                                    'cl' => utf8_encode($row['13_clima'])
                                ),
                                array(
                                    'ds' => '14:00',
                                    'va' => number_format($row['14:00'],2),
                                    'cl' => utf8_encode($row['14_clima'])
                                ),
                                array(
                                    'ds' => '15:00',
                                    'va' => number_format($row['15:00'],2),
                                    'cl' => utf8_encode($row['15_clima'])
                                ),
                                array(
                                    'ds' => '16:00',
                                    'va' => number_format($row['16:00'],2),
                                    'cl' => utf8_encode($row['16_clima'])
                                ),
                                array(
                                    'ds' => '17:00',
                                    'va' => number_format($row['17:00'],2),
                                    'cl' => utf8_encode($row['17_clima'])
                                ),
                                array(
                                    'ds' => '18:00',
                                    'va' => number_format($row['18:00'],2),
                                    'cl' => utf8_encode($row['18_clima'])
                                ),
                                array(
                                    'ds' => '19:00',
                                    'va' => number_format($row['19:00'],2),
                                    'cl' => utf8_encode($row['19_clima'])
                                ),
                                array(
                                    'ds' => '20:00',
                                    'va' => number_format($row['20:00'],2),
                                    'cl' => utf8_encode($row['20_clima'])
                                ),
                                array(
                                    'ds' => '21:00',
                                    'va' => number_format($row['21:00'],2),
                                    'cl' => utf8_encode($row['21_clima'])
                                ),
                                array(
                                    'ds' => '22:00',
                                    'va' => number_format($row['22:00'],2),
                                    'cl' => utf8_encode($row['22_clima'])
                                ),
                                array(
                                    'ds' => '23:00',
                                    'va' => number_format($row['23:00'],2),
                                    'cl' => utf8_encode($row['23_clima'])
                                )
                            )
                        );
                        
                    }   
                    
                    array_order_column($array,'ds_bdg');
                    $arrayCompl = $array;
//                    echo '<pre>';
//                    print_r($array);
//                    die;
                    
//                    if(!empty($array)){
//                        
//                        for($a=10; $a <=23;$a++){
//                            
//                            if($array[0]['ds_'.$a] <> 'NNN'){
//                                
//                                $arrayCompl[$a] = array(
//                                    'hora'=>$a.':00',
//                                    'data'=>null
//                                );
//                                
//                                $data = null;
//                                foreach ($array as $raw){
//                                    $data[] = array (
//                                        'co_bdg'=>$raw['co_bdg'],
//                                        'ds_bdg'=>$raw['ds_bdg'],
//                                        'ds_ciu'=>$raw['ds_ciu'],
//                                        'va_hor'=>$raw['ds_'.$a],
//                                        'va_cli'=>$raw['cl_'.$a]
//                                    );
//                                }
//                                $arrayCompl[$a]['data'] = $data;
//                            }
//                            
//                        }
//                        
//                    }
                    
                }
            }

        }
        
//        echo '<pre>';
//        print_r($arrayCompl);
//        die;
        
        return array('data'=>$arrayCompl,'err'=>$error);
        
    }
    
    function retVentasPorGrupo($fe_ini){
        
        $array = null;

        if(!empty($fe_ini)){
            
            $Sql  = " db_estadisticas..sp_ventas_x_grupo '".trim($fe_ini)."';";

            $result = $this->db->query($Sql);
            if ($this->db->_error_message() <> ''){
                $error = & load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER ESTADISTICAS: ".$this->db->_error_number(),$this->db->_error_message(), 'error_db');
                die;
            } else {
                $res = $result->result_array();
                
                if(!empty($res)){
                    
                    foreach ($res as $row){
                        
                        $array[] = array(
                            'co_gru'=>(int)$row['co_grupo'],
                            'ds_gru'=>trim(utf8_encode($row['ds_grupo'])),
                            'ds_let'=>trim(utf8_encode($row['ds_letra'])),
                            'va_can'=>(int)$row['va_cantidad'],
                            'ca_tot'=>(int)$row['va_total'],
                            'va_por'=>$row['va_porcentaje'],
                            'va_men'=>$row['va_mensual_actual'],
                            'va_ani'=>$row['va_mensual_1year']
                        );
                        
                    }   
                }
            }

        }
        
        return array('data'=>$array);
        
    }
    
}
?>
