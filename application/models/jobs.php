<?php
class jobs extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function Actividad($sp){
        
        $array = null;

        if(!empty($sp)){
            
            $Sql = "PODBPOS.._pos_jobs_activity '".trim($sp)."'; ";
            $result = $this->db->query($Sql);  
            if ($this->db->_error_message() <> ''){
                return "ERROR AL EJECUTAR SP: ".$this->db->_error_number()." ".$this->db->_error_message();
                die();
            } 
            
            if(count($result->result_array()) > 0){

                foreach($result->result_array() as $row){
                    $array = array(
                        'job_id'=> trim($row['job_id']),
                        'name'=> trim(utf8_encode($row['name'])),
                        'enabled'=> (int)$row['enabled'],
                        'run_Requested_date'=> trim($row['run_Requested_date']),
                        'stop_execution_date'=> trim($row['stop_execution_date']),
                        'ElapsedSeconds'=> trim($row['ElapsedSeconds']),
                        'last_outcome_message'=> trim($row['last_outcome_message']),
                        'last_run_outcome'=> trim($row['last_run_outcome']),
                        'RowID'=> trim($row['RowID'])
                    );
                }

            }

        }
        
        return $array;
        
    }
    
    function Actualizar($enabled,$id_job){
        
        $arr_error = null;
        
        if(!empty($id_job)){
            
            $Sql = "WEBPYCCA..sp_update_job ".(int)$enabled.",'".trim($id_job)."'; ";
            $result = $this->db->query($Sql);  
            if ($this->db->_error_message() <> ''){
                return "ERROR AL EJECUTAR SP: ".$this->db->_error_number()." ".$this->db->_error_message();
                die();
            } 
                     
            $row = $result->row_array();
            
            $arr_error = array( 
                'data'=>
                array(   
                    'co_err'=> (int)$row['co_error'],
                    'tx_err'=> utf8_encode(trim($row['tx_error']))
                )
            ); 

        }
        
        return $arr_error;
        
    }
    
    function Iniciar($sp){
        
        if(!empty($sp)){
            
            try {
                
                $Sql = "msdb.dbo.sp_start_job '".trim($sp)."'; ";
                $execute = $this->db->query($Sql);  
                
                if (!$execute) {
                    throw new PDOException ("Error");
                }
                
            } catch(Exception $e){
                
            }
            
//            if ($this->db->_error_message() <> ''){
//                return "ERROR AL EJECUTAR SP: ".$this->db->_error_number()." ".$this->db->_error_message();
//                die();
//            } 

            return 'OK';
            
        }
        
    }
    
}
 
?>


