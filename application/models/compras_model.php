<?php
class compras_model extends CI_Model{

   function __construct()
    {
        parent::__construct();
      /*  $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();*/
    }
    
    
    function retDocCompras($id,$tipo,$fec_ini,$fec_fin) { 
        
        $id = !empty($id)?$id:0;
        $Sql = " sp_get_compra_solicitud ".$id.",'".$tipo."','".$fec_ini."','".$fec_fin."' ";
        //echo $Sql;  
        $query = $this->db->query($Sql);
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            return "ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
        }
        $arreglo = array();
        $estado = '';
       // print_r($query->result_array());
        foreach($query->result_array() as $row) {
            //print_r($row);
            if (trim($row['bd_status']) === 'I'){
                $estado = 'Ingresada';
            } else if (trim($row['bd_status']) === 'C'){
                $estado = 'Enviada a Cotizar';
            } else if (trim($row['bd_status']) === 'G'){
                $estado = 'Enviada a Aprobación';
            } else if (trim($row['bd_status']) === 'A'){
                $estado = 'Aprobada';
            } else if (trim($row['bd_status']) === 'N'){
                $estado = 'Negada';
            }

            $arreglo[] = array('co_solicitud' =>$row['co_solicitud'],
                               'fe_ingreso' => date('Y/m/d H:i',strtotime($row['fe_ingreso'])),
                               'co_usuario' =>$row['co_usuario'],
                               'co_responsable' =>$row['co_responsable'],
                               'fe_modificacion' =>$row['fe_modificacion'],
                               'tx_dispositivo' =>$row['tx_dispositivo'],
                               'tx_destino' =>$row['tx_destino'],
                               'tx_proveedor' =>$row['tx_proveedor'],
                               'va_cantidad' =>$row['va_cantidad'],
                               'va_precio' =>$row['va_precio'],
                               'bd_status' =>$row['bd_status'],
                               'nom_estado' =>$estado);
        }

        return $arreglo;

    }
    
    function updateDocumentosCompras($data) {
 
        $arr = explode(',',$data['codigo']);
        $cantidad = count($arr);
        for ($i=0; $i<$cantidad; $i++){
            
            $Sql  = " UPDATE compras.solicitud SET ";
            $Sql .= " bd_status = '".$data['tipo_change']."' ";
            $Sql .= " ,fe_modificacion = getdate() ";
            $Sql .= " WHERE co_solicitud = ".$arr[$i];     
            $Sql .= " AND bd_status = '".$data['tipo']."' ";

            $this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
            }
        }

        echo 'OK';
        
    }
    
    function saveRequisicion($person) {
    
        $Sql = " compras._solicitud_ingresa ".$_SESSION['c_e'].",".$_SESSION['c_r'].",'".$person['dispositivo']."','".$person['destino']."',".$person['cantidad']." ";
        $this->db->query($Sql,false,false);
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            return "ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
        }

        echo 'OK';
    }
    
    function saveRequisicionCotizar($data) {
    
        $Sql = " sp_actualiza_compra_cotiza '".$data['status']."',".$data['codigo'].",'".$data['proveedor']."',".$data['precio']." ";
        $this->db->query($Sql,false,false);
        if ($this->db->_error_message()){
            $this->db->trans_rollback();
            return "ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
        }
        
        echo 'OK';
        
    }


    }
 
?>


