<?php

class m_103 extends CI_Model{

   function __construct()
    {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
        
    function getComedor(){
        
        $Sql  = " personal_pi.dbo.COMEDOR_MENU ";
        $DB1 = $this->load->database('nomina', true);
        $query=$DB1->query($Sql);
        if ($DB1->_error_message()){
            $error =& load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL OBTENER COMEDOR <BR> COD_ERR: ".$DB1->_error_number(),$DB1->_error_message(), 'error_db');
            die;
        }
        
        return $query->result_array();
        
    }

}
?>
