<?php

class m_101 extends CI_Model{

   function __construct()
    {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
        
    function getPersonas($datos){
        
        $Sql  = " SELECT cedula, nombres, fech_nacim, nomb_conyu, domi_calle, nombre_padre, nombre_madre ";
        $Sql .= " FROM Tse..TBREGI_CIVIL ";
        if(trim($datos['t_p']) === 'C'){
            $Sql .= " WHERE CEDULA = '".trim($datos['c_i'])."' ";
        } else if(trim($datos['t_p']) === 'N'){
            $Sql .= " WHERE nombres LIKE '".trim($datos['n_e'])."%' ";
        }
        $Sql .= " ORDER BY cedula ";
        
        $query = $this->db->query($Sql);
        if ($this->db->_error_message() <> ''){
            return "ERROR AL OBTENER PERSONA<BR> COD_ERR: ".$this->db->_error_number()." ".$this->db->_error_message();
            die;
        }  

        $return_arr=null;
        $i = 0;
        foreach ($query->result_array() as $row) { 
            $i++;
            $return_arr[] = array('cedula'=>trim($row['cedula']),
                'nombres'=> utf8_encode(trim($row['nombres'])),
                'fech_nacim'=> trim(date('d/m/Y',strtotime($row['fech_nacim']))),
                'nomb_conyu'=> utf8_encode(trim($row['nomb_conyu'])),
                'domi_calle'=> utf8_encode(trim($row['domi_calle'])),
                'nombre_padre'=> utf8_encode(trim($row['nombre_padre'])),
                'nombre_madre'=> utf8_encode(trim($row['nombre_madre']))); 
        }

        unset($query);
        echo "OK-".json_encode($return_arr);
    }

}
?>
