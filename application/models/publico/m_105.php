<?php

class m_105 extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    function _getInventar($data) {
        if (!empty($data)) {

            $Sql = " IVDBINVENTAR.dbo._consulta_articulo_bodeguero '','" . trim($data['i_des']) . "','' ";
            //echo $Sql;
            $resultado = $this->db->query($Sql);
            if ($this->db->_error_message()) {
                $this->db->trans_rollback();
                return "ERROR AL RETORNAR INVENTARIO: " . $this->db->_error_number() . " " . $this->db->_error_message();
                die;
            }

            $array = null;
            if (count($resultado->result_array()) > 0) {
                foreach ($resultado->result_array() as $row) {
                    $array[] = array(
                        'cod_art' => $row['co_articulo'],
                        'ds_art' => utf8_encode($row['ds_articulo']),
                        'co_fab' => utf8_encode($row['co_fabrica']));
                }
                return "OK" . json_encode($array);
            } else {
                echo 'No hay datos en la busqueda realizada.';
            }
        }
    }

    function _retDataArt($data) {
        if (!empty($data)) {
            $Sql = " IVDBINVENTAR.dbo._consulta_articulo_bodeguero_detalle '" . trim($data['i_art']) . "', 'A,B' ";
            $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
            $resultset = odbc_exec($conn, $Sql);
            $array = $data = null;
            while ($row = odbc_fetch_array($resultset)) {
                $array[] = array(
                    'cod_art' => $row['co_articulo'],
                    'des_art' => utf8_encode($row['ds_articulo']),
                    'empaque' => $row['empaque'],
                    'cod_fab' => utf8_encode($row['co_fabrica']),
                    'des_gru' => utf8_encode($row['ds_grupo']),
                    'des_lin' => utf8_encode($row['ds_linea']),
                    'des_sub' => utf8_encode($row['ds_sublinea']),
                    'des_cat' => utf8_encode($row['ds_categoria']),
                    'cod_fac' => utf8_encode($row['co_factura']),
                    'pvp' => (float) $row['pvpiva'],
                    'pvp_ofe' => (float) $row['pvp_oferta_iva'],
                    'oferta' => utf8_encode($row['oferta']),
                    'desde' => date('Y/m/d', strtotime($row['desde'])),
                    'hasta' => date('Y/m/d', strtotime($row['hasta']))
                );
            }
            $label = $array;
            odbc_next_result($resultset);
            while ($row = odbc_fetch_array($resultset)) {
                $data[] = array('cod_bdg' => $row['co_bodega'],
                    'descripcion' => utf8_encode($row['descripcion']),
                    'saldo' => $row['saldo'],
                    'reservado' => $row['reservado'],
                    'percha' => $row['PERCHA']);
            }
            echo json_encode($label) . "/*/" . json_encode($data);
        }
    }

    function consultaArticulo($co_articulo, $co_usuario = 0) {

        $error = '';
        $info = $saldo = $percha = null;

        if (!empty($co_articulo)) {

            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });

            try {

                if ($co_usuario > 0) {
                    $Sql = " IVDBINVENTAR.dbo._consulta_articulo_bodeguero_detalle '" . $co_articulo . "','A,B'," . (int) $co_usuario . "; ";
                } else {
                    $Sql = " IVDBINVENTAR.dbo._consulta_articulo_bodeguero_detalle '" . $co_articulo . "','A,B'; ";
                }
                
                $conn = odbc_connect("MASTER-P", "pica", "");
                $resultset = odbc_exec($conn, $Sql);

                do {

                    while ($row = odbc_fetch_array($resultset)) {

                        if (!empty($row['resultset'])) {
                            if (isset($row['resultset'])) {

                                switch ($row['resultset']) {

                                    case "INFO":

                                        $info = array(
                                            'cod_art' => $row['co_articulo'],
                                            'des_art' => trim(utf8_encode($row['ds_articulo'])),
                                            'empaque' => trim($row['empaque']),
                                            'cod_fab' => trim(utf8_encode($row['co_fabrica'])),
                                            'des_gru' => trim(utf8_encode($row['ds_grupo'])),
                                            'des_lin' => trim(utf8_encode($row['ds_linea'])),
                                            'des_sub' => trim(utf8_encode($row['ds_sublinea'])),
                                            'des_cat' => trim(utf8_encode($row['ds_categoria'])),
                                            'cod_fac' => trim(utf8_encode($row['co_factura'])),
                                            'pvp' => (float) $row['pvpiva'],
                                            'pvp_ofe' => (float) $row['pvp_oferta_iva'],
                                            'oferta' => trim(utf8_encode($row['oferta'])),
                                            'desde' => date('Y/m/d', strtotime($row['desde'])),
                                            'hasta' => date('Y/m/d', strtotime($row['hasta']))
                                        );

                                        break;

                                    case "SALDO":

                                        $saldo[] = array(
                                            'cod_bdg' => (int) $row['co_bodega'],
                                            'descripcion' => trim(utf8_encode($row['descripcion'])),
                                            'saldo' => $row['saldo'],
                                            'reservado' => $row['reservado'],
                                            'percha' => $row['PERCHA']
                                        );

                                        break;

                                    case "PERCHA":

                                        $percha[] = array(
                                            'co_bdg' => (int) $row['co_bodega'],
                                            'bd_per' => (int) $row['bodega_permitida'],
                                            'pe_des' => trim(utf8_encode($row['DESCRIPCION'])),
                                            'ni_per' => $row['nivel'],
                                            'co_per' => $row['COLUMNA'],
                                            'np_per' => $row['NPERCHA'],
                                            'zo_per' => $row['zona'],
                                            'pe_des2' => trim(utf8_encode($row['descripcion2'])),
                                        );

                                        break;
                                }
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
            } catch (Exception $e) {
                $error = "ERROR AL OBTENER DETALLE DE TOMA :" . $e->getMessage();
            }
        }

        return array('inf' => $info, 'sal' => $saldo, 'per' => $percha, 'err' => $error);
    }

}

?>
