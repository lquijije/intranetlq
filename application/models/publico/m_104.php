<?php

class m_104 extends CI_Model{

   function __construct()
    {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
        
    function getAutos($tipo,$buscar){
              
        $Sql  = " SELECT placa, ";
        $Sql .= " PROPIETARIO=NOMBRE_COMPLETO, IDENTIFICACION, ";
        $Sql .= " CLASE=CLASE_VEH, MARCA=MARCA_VEH, MODELO=MODELO_VEH, COLOR, ";
        $Sql .= " CHASIS , MOTOR , TONELAJE, ";
        $Sql .= " ANIO_PROD, PAIS,SERVICIO, CANTON=CANTON_CIR ";
        $Sql .= " FROM SODBSOAT..VEHICULOS ";
        
        if(trim($tipo) === 'P'){
            $Sql .= " WHERE placa = '".trim($buscar)."' ";
        } else if(trim($tipo) === 'I'){
            $Sql .= " WHERE IDENTIFICACION = '".trim($buscar)."' ";
        }
        $Sql .= " ORDER BY NOMBRE_COMPLETO ";
        $query=$this->db->query($Sql);
        if ($this->db->_error_message() <> ''){
            return "ERROR AL OBTENER VEHÍCULOS<BR> COD_ERR: ".$this->db->_error_number()." ".$this->db->_error_message();
            die;
        }  

        $return_arr=null;
        $i = 0;
        
        foreach ($query->result_array() as $row) { 
            $i++;
            $return_arr[] = array(
                'DT_RowId'=>'row_'.$i,
                'placa'=>trim($row['placa']),
                'propietario'=>trim($row['NOMBRE_COMPLETO']),
                'identificacion'=>trim($row['IDENTIFICACION']),
                'clase'=>trim($row['CLASE_VEH']),
                'marca'=>trim($row['MARCA_VEH']),
                'modelo'=>trim($row['MODELO_VEH']),
                'color'=>trim($row['COLOR']),
                'chasis'=>trim($row['CHASIS']),
                'motor'=>trim($row['MOTOR']),
                'tonelaje'=>trim($row['TONELAJE']),
                'annio'=>trim($row['ANIO_PROD']),
                'pais'=>trim($row['PAIS']),
                'servicio'=>trim($row['SERVICIO']),
                'canton'=>trim($row['CANTON_CIR'])); 
        }
        
        $array = array(
            "recordsTotal"=> $i,
            "recordsFiltered"=> $i,
            "data"=>$return_arr);
//        print_r($array);
//        die;
        unset($query);
        echo json_encode($array);
    }

}
?>
