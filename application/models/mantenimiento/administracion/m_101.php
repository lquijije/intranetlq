<?php

class m_101 extends CI_Model{

   function __construct()
    {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
   

    function menu(){
        
        $Sql  = " SELECT codigo,nombre,padre,urlinvocar,posicion ";
        $Sql .= " FROM tb_intranet_menu ";
        $Sql .= " WHERE estado = 1 ";
        $Sql .= " AND codigo IN ";
        $Sql .= " (SELECT padre ";
        $Sql .= " FROM tb_intranet_menu ";
        $Sql .= " WHERE estado=1) ";
        $Sql .= " ORDER BY padre,nombre ";
        
        $query = $this->db->query($Sql);
        $Menu=null;
        $padres=null;
        foreach($query->result_array() as $row){
            if(trim($row['padre'])=='0')
                $row['padre']='root';
            $Menu[]=array($row['padre'],$row['codigo'],utf8_encode($row['nombre']),trim(utf8_encode($row['urlinvocar'])),false);
        }
        
        $Sql  = " SELECT codigo,nombre,padre,urlinvocar,posicion ";
        $Sql .= " FROM tb_intranet_menu ";
        $Sql .= " WHERE estado = 1 ";
        $Sql .= " AND codigo NOT IN ";
        $Sql .= " (SELECT padre ";
        $Sql .= " FROM tb_intranet_menu ";
        $Sql .= " WHERE estado=1) ";
        $Sql .= " ORDER BY padre,nombre ";
        //echo $Sql;
        $query = $this->db->query($Sql);
        
        foreach($query->result_array() as $row){
            if(trim($row['padre'])=='0')
                $row['padre']='root';
            $Menu[]=array($row['padre'],$row['codigo'],utf8_encode($row['nombre']),trim(utf8_encode($row['urlinvocar'])),false);
        }        
		
        $query->free_result();
        unset($query,$padres);
        return $Menu;
    }
    
    function delete($datos){
        if($datos!=null ){  
            $Sql  = " DELETE FROM tb_intranet_perfil ";
            $Sql .= " WHERE codigo = ".$datos['id']." "; 
            //echo $Sql;
            $this->db->query($Sql); 
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL ELIMINAR PERFIL: ".$this->db->_error_number()." ".$this->db->_error_message().$Sql;
            }    
            $Sql  = " DELETE FROM tb_intranet_perfil_accesos ";
            $Sql .= " WHERE perfil = ".$datos['id']." "; 
            //echo $Sql;
            $this->db->query($Sql); 
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR AL ELIMINAR ACCESOS PERFIL: ".$this->db->_error_number()." ".$this->db->_error_message().$Sql;
            }
            
            echo "OK";
            
        }
    }
    
    function grabar($datos=null){
        
        if($datos!=null ){ 
            
            $CI = &get_instance();            
            
            $fecha_act = $this->config->item('fecha_actual');
            
            if (trim($datos['accion']) === "I"){
                
                $Sql  = " INSERT INTO tb_intranet_perfil (descripcion,estado,user_creacion,fecha_creacion) VALUES (";
                $Sql .= " '".$datos['nombre']."',1,'".$_SESSION['usuario']."',$fecha_act) ";        
                
                $this->db->query($Sql);
                if ($this->db->_error_message()){
                    $this->db->trans_rollback();
                    return "ERROR AL GRABAR PERFIL: ".$this->db->_error_number()." ".$this->db->_error_message().$Sql;
                } 
                
                $SqlID = " SELECT @@IDENTITY AS maxcod; ";
                $idQuery = $this->db->query($SqlID);
                if ($this->db->_error_message()){
                    $this->db->trans_rollback();
                    return "ERROR GET ID: ".$this->db->_error_number()." ".$this->db->_error_message().$SqlID;
                }    
                
                $codigo = $idQuery->row_array();
                $codId = $codigo['maxcod'];

            } else if (trim($datos['accion']) === "U"){
                
                $Sql = " UPDATE tb_intranet_perfil SET ";
                $Sql.= " descripcion = '".$datos['nombre']."', ";
                $Sql.= " user_modificacion = '".$_SESSION['usuario']."', ";
                $Sql.= " fecha_modificacion = $fecha_act ";
                $Sql.= " WHERE codigo  = ".trim($datos['id'])." ";
                $Sql.= " AND estado = 1 ";
                
                $this->db->query($Sql);
                if ($this->db->_error_message()){
                    $this->db->trans_rollback();
                    return "ERROR AL ACTUALIZAR PERFIL: ".$this->db->_error_number()." ".$this->db->_error_message().$Sql;
                } 
                
                $codId = trim($datos['id']);
            }
            
            $this->m_101->grabar_accesos($codId,$datos);  
            
            unset($Sql,$datos);
            
        }
    }

    function grabar_accesos($codigo,$datos=null){
        
            if($datos!=null){  
                $CI = &get_instance();     
                $fecha_act = $this->config->item('fecha_actual');

                if (trim($datos['accion']) === "U"){
                    
                    $Sql =" DELETE FROM tb_intranet_perfil_accesos ";
                    $Sql.=" WHERE perfil = ".$codigo." ";
                    //echo $Sql;
                    $query=$this->db->query($Sql); 
                    if ($this->db->_error_message()){
                        $this->db->trans_rollback();
                        return "ERROR AL ACTUALIZAR ACCESOS PERFIL: ".$this->db->_error_number()." ".$this->db->_error_message();
                    }       
                    
                    $Sql =" DELETE FROM tb_intranet_perfil_opciones ";
                    $Sql.=" WHERE perfil = ".$codigo." ";
                    //echo $Sql;
                    $query=$this->db->query($Sql); 
                    if ($this->db->_error_message()){
                        $this->db->trans_rollback();
                        return "ERROR AL ACTUALIZAR ACCESOS PERFIL: ".$this->db->_error_number()." ".$this->db->_error_message();
                    }       
                    
                }
                
                $widgets = explode(',',$datos['widgets']);
                
                for ($a=0; $a < count($widgets); $a++) {
                    if(isset($widgets[$a])&& !empty($widgets[$a])){
                        
                        $Sql  =" INSERT INTO tb_intranet_perfil_accesos (perfil,tipo,menu,estado,user_creacion,fecha_creacion) VALUES (";
                        $Sql .=" '".$codigo."',2,'".$widgets[$a]."',";
                        $Sql .=" 1,'".$_SESSION['usuario']."',$fecha_act)";  
                        //echo $Sql;
                        $query=$this->db->query($Sql);
                        if ($this->db->_error_message()){
                            $this->db->trans_rollback();
                            return "ERROR AL ACTUALIZAR ACCESOS DEL PERFIL (WIDGETS): ".$this->db->_error_number()." ".$this->db->_error_message();
                        }  
                    }
                }
                
                $Accesos  = explode('/*/',$datos['postarbolP']);    
                        
                for ($i=0; $i < count($Accesos); $i++) {
                    if(isset($Accesos[$i])&& !empty($Accesos[$i])){
                        $abc = explode('|*|', $Accesos[$i]);
                        $Sql3 =" SELECT count(*)+'' AS cantidad ";
                        $Sql3.=" FROM tb_intranet_perfil_accesos ";
                        $Sql3.=" WHERE perfil='".$codigo."' and menu=".$abc[0]." and tipo = 1 ";                
                        //echo $Sql3;
                        $query3=$this->db->query($Sql3);
                        $row3 = $query3->row_array(); 

                        if ((int)$row3['cantidad'] == 0){ 
                            
                            $Sql  =" INSERT INTO tb_intranet_perfil_accesos (perfil,tipo,menu,estado,user_creacion,fecha_creacion,opciones) VALUES (";
                            $Sql .=" '".$codigo."',1,'".$abc[0]."',";
                            $Sql .=" 1,'".$_SESSION['usuario']."',$fecha_act,'".$abc[1]."')"; 
                            //echo $Sql;
                            $query=$this->db->query($Sql);
                            if ($this->db->_error_message()){
                                $this->db->trans_rollback();
                                return "ERROR AL ACTUALIZAR ACCESOS DEL PERFIL: ".$this->db->_error_number()." ".$this->db->_error_message();
                            }      
                            if(!empty($abc[1])){
                                $arrayAAA = explode('|',$abc[1]);
                                $countArry = count($arrayAAA);
                                for($a= 0; $a < $countArry; $a++){
                                    $subArray = explode(':',$arrayAAA[$a]);
                                    $abcde = explode(',',$subArray[1]);
                                    $cant = count($abcde);
                                    for ($v = 0; $v < $cant; $v++){
                                        
                                        $Sql  =" INSERT INTO tb_intranet_perfil_opciones VALUES (";
                                        $Sql .=" $codigo,'".$subArray[0]."',$abc[0],$abcde[$v],1)"; 
                                        //echo $Sql;
                                        $query=$this->db->query($Sql);
                                        if ($this->db->_error_message()){
                                            $this->db->trans_rollback();
                                            return "ERROR AL ACTUALIZAR OPCIONES DEL PERFIL: ".$this->db->_error_number()." ".$this->db->_error_message();
                                        } 
                                    }
                                }
                            }
                        }
                    }              
                }
               
                unset($CI,$Sql,$codigo,$datos);
                echo "OK";
            } else {
                return "pfff";            
            }
        }    
        
    function retPerfilWidget($id){

            $Sql  = " SELECT a.codigo,a.descripcion,a.cod_departamentos,b.menu ";
            $Sql .= " FROM tb_intranet_perfil AS a ";
            $Sql .= " LEFT JOIN tb_intranet_perfil_accesos AS b ";
            $Sql .= " ON b.perfil = a.codigo ";
            $Sql .= " AND b.tipo = 2 ";
            $Sql .= " WHERE a.codigo = $id ";
            $Sql .= " ORDER BY b.menu ASC ";
            //echo $Sql;
            $query=$this->db->query($Sql);
            if ($this->db->_error_message()){
                $this->db->trans_rollback();
                return "ERROR: ".$this->db->_error_number()." ".$this->db->_error_message();
            }  

            $return_arr=null;
            $i = 0;
            foreach ($query->result_array() as $row) { 
                $i++;
                $return_arr[] = array('codigo'=>trim($row['codigo']),'descripcion'=>trim(utf8_encode($row['descripcion'])),'dptos'=>trim($row['cod_departamentos']),'menu'=>trim($row['menu'])); 
            }

            unset($query);
            echo json_encode($return_arr);
       }

    function menu_perfil($id){
        
        $Sql  = " SELECT b.menu,b.opciones ";
        $Sql .= " FROM tb_intranet_perfil AS a ";
        $Sql .= " INNER JOIN tb_intranet_perfil_accesos AS b ";
        $Sql .= " ON b.perfil = a.codigo ";
        $Sql .= " AND b.tipo = 1 ";
        $Sql .= " WHERE a.codigo = $id ";
        $Sql .= " ORDER BY b.menu ASC ";
//        echo 
        $query=$this->db->query($Sql);
        $opciones="";
        $ini="cb_";            
        foreach($query->result_array() as $row){
            $opciones=$opciones."|*|".$ini.$row['menu']."-".$row['opciones'];
        }      
        $query->free_result();
        unset($query);
        return $opciones;
    }


}
?>
