<?php

class m_102 extends CI_Model{

   function __construct()
    {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function menu(){
        
        $Sql  = " SELECT codigo,nombre,padre,urlinvocar,posicion ";
        $Sql .= " FROM tb_intranet_menu ";
        $Sql .= " WHERE estado = 1 ";
        $Sql .= " AND codigo IN ";
        $Sql .= " (SELECT padre ";
        $Sql .= " FROM tb_intranet_menu ";
        $Sql .= " WHERE estado=1) ";
        $Sql .= " ORDER BY padre,posicion; ";
        
        //$query = $this->db->query($Sql);
        $CI = &get_instance();
        $dblxmaster = $CI->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);
        $Menu=null;
        $padres=null;
        foreach($query->result_array() as $row){
            if(trim($row['padre'])=='0')
                $row['padre']='root';
            $Menu[]=array($row['padre'],$row['codigo'],$row['nombre'],trim($row['urlinvocar']),false);
        }
        
        $Sql  = " SELECT codigo,nombre,padre,urlinvocar,posicion ";
        $Sql .= " FROM tb_intranet_menu ";
        $Sql .= " WHERE estado = 1 ";
        $Sql .= " AND codigo NOT IN ";
        $Sql .= " (SELECT padre ";
        $Sql .= " FROM tb_intranet_menu ";
        $Sql .= " WHERE estado=1) ";
        $Sql .= " ORDER BY padre,posicion; ";
        //echo $Sql;
        //$query = $this->db->query($Sql);
        $CI = &get_instance();
        $dblxmaster = $CI->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);
        foreach($query->result_array() as $row){
            if(trim($row['padre'])=='0')
                $row['padre']='root';
            $Menu[]=array($row['padre'],$row['codigo'],$row['nombre'],trim($row['urlinvocar']),false);
        }        
		
        $query->free_result();
        unset($query,$padres);
        return $Menu;
    }
    
    function getPerfil(){
        $Sql = " SELECT codigo,descripcion ";
        $Sql.= " FROM tb_intranet_perfil ";
        $Sql.= " WHERE estado = 1 ";
        $Sql.= " ORDER BY descripcion ASC; "; 
        //$query =  $this->db->query($Sql);
        $CI = &get_instance();
        $dblxmaster = $CI->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);
        if ($dblxmaster->_error_message()){
            $dblxmaster->trans_rollback();
            return "ERROR AL OBTENER PERFILES: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
        }
        return $query->result_array();
    }
    
    function getUsers(){
        
            $Sql  = "SELECT DISTINCT a.usuario,concat(a.apellidos,' ',a.nombres) AS nombre,cod_empleado,";
            $Sql .= "b.nombre AS nomestado  ,c.nombre AS nomb_ccosto ";
            $Sql .= "FROM tb_intranet_users  a ";
            $Sql .= "LEFT JOIN tb_intranet_parametros b ";
            $Sql .= "ON b.tipo = 3 AND b.codigo = a.estado ";
            $Sql .= "LEFT JOIN PERSONAL_PI.rh_ccosto c ON c.cod_empresa in('1','27') AND ";
            $Sql .= " a.cod_centro_costo = c.cod_ccosto ";
            $Sql .= "WHERE a.estado = 1 ";
            $Sql .= "ORDER BY a.usuario ASC;";
            $CI = &get_instance();

            $dblxmaster = $CI->load->database('db_lx_master',true);
            $query = $dblxmaster->query($Sql);
            if ($dblxmaster->_error_message()){
                $dblxmaster->trans_rollback();
                return "ERROR AL OBTENER USUARIOS: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
            }

            $return_arr=null;
            $i = 0;
            foreach ($query->result_array() as $row) { 
                $i++;
                $return_arr[] = array(
                    'c_p'=>trim($row['cod_empleado']),
                    'user'=>trim(utf8_encode($row['usuario'])),
                    'nombre'=>trim(utf8_encode($row['nombre'])),
                    'estado'=>trim(utf8_encode($row['nomestado'])),
                    'n_ccos'=>trim(utf8_encode($row['nomb_ccosto']))
                    ); 
            }

            unset($query);
            echo json_encode($return_arr);
    }
    
    function delete($datos){
        if($datos!=null ){  
            
            $Sql  = " DELETE FROM tb_intranet_perfil ";
            $Sql .= " WHERE codigo = ".$datos['id']."; ";                            
            //echo $Sql;
            //$this->db->query($Sql); 
            $CI = &get_instance();
            $dblxmaster = $CI->load->database('db_lx_master',true);
            $dblxmaster->query($Sql);
            if ($dblxmaster->_error_message()){
                $dblxmaster->trans_rollback();
                return "ERROR AL ELIMINAR PERFIL: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
            }    
            
            $Sql  = " DELETE FROM tb_intranet_perfil_accesos ";
            $Sql .= " WHERE perfil = ".$datos['id']."; ";                            
            //echo $Sql;
            //$this->db->query($Sql); 
            $dblxmaster = $CI->load->database('db_lx_master',true);
            $dblxmaster->query($Sql);
            if ($dblxmaster->_error_message()){
                $dblxmaster->trans_rollback();
                return "ERROR AL ELIMINAR ACCESOS PERFIL: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
            }
            
            echo "OK";
            
        }
    }
    
    function grabar($datos=null){
        
        if($datos!=null ){  
            $fecha_act = $this->config->item('fecha_actual_mysql');
            $CI = &get_instance();            
            $codigo=$CI->general_model->retCodigo('1.1.2','N','');  

            if (trim($datos['accion']) === "I"){  
                
                $Sql  = " SELECT count(1) AS cantidad ";
                $Sql .= " FROM tb_intranet_users ";
                $Sql .= " WHERE cod_empleado = '".trim($datos['c_e'])."'; ";
                //echo $Sql;
                //$query2 = $this->db->query($Sql);
                $dblxmaster = $CI->load->database('db_lx_master',true);
                $query2 = $dblxmaster->query($Sql);
                $row2 = $query2->row_array();
                
                if((int)$row2['cantidad'] == 0){
                    $Sql  = " SELECT count(1) AS cantidad ";
                    $Sql .= " FROM tb_intranet_users ";
                    $Sql .= " WHERE usuario = '".trim($datos['n_u'])."'; ";
                    //echo $Sql;
                    //$query1 = $this->db->query($Sql);
                    $dblxmaster = $CI->load->database('db_lx_master',true);
                    $query1 = $dblxmaster->query($Sql);
                    $row = $query1->row_array();

                    if((int)$row['cantidad'] == 0){
                        if($datos['co_c']==''){$datos['co_c']='NULL';}
                        $pass = $this->encrypt->encode($datos['c_a']);
                        $Sql = " INSERT INTO tb_intranet_users (cod_user,cod_empleado,usuario,clave,nombres,apellidos,estado,user_creacion,fecha_creacion,e_mail,cod_encargado,cod_centro_costo,cambio_pass) VALUES (";
                        $Sql.= " $codigo,".trim($datos['c_e']).",'".trim($datos['n_u'])."','".trim($pass)."','".utf8_decode(trim($datos['n_e']))."','".utf8_decode(trim($datos['a_e']))."',1,'".trim($_SESSION['usuario'])."',$fecha_act,'".trim($datos['e_l'])."',".$datos['c_r'].",".$datos['co_c'].","."'N');";              
                        
                        //$this->db->query($Sql);
                        $dblxmaster = $CI->load->database('db_lx_master',true);
                        $dblxmaster->query($Sql);
                        if ($dblxmaster->_error_message()){
                            $dblxmaster->trans_rollback();
                            return "ERROR AL GRABAR USUARIO: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
                        } 

                        if(!empty($datos['p_s'])){
                            
                            $ada =  explode(',',$datos['p_s']);
                            $con = count($ada);
                            
                            for($i=0; $i < $con; $i++){
                               
                                $Sql = " INSERT INTO tb_intranet_users_perfil (cod_empl,perfil,estado,user_creacion,fecha_creacion) VALUES (";
                                $Sql.= " ".$datos['c_e'].",'".$ada[$i]."',1,'".$_SESSION['usuario']."',$fecha_act);";                
                                //$this->db->query($Sql);
                                $dblxmaster = $CI->load->database('db_lx_master',true);
                                $dblxmaster->query($Sql);
                                if ($dblxmaster->_error_message()){
                                    $dblxmaster->trans_rollback();
                                    return "ERROR AL GRABAR PERMISO USUARIO: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
                                }  
                            }
                        }
                        
                        
                    } else {
                        echo "Nombre de Usuario ".trim($datos['n_u'])." ya existe por favor ingrese otro.";
                        die;
                    }
                } else {
                    echo "Código de Empleado ".trim($datos['c_e'])." ya existe.";
                    die;
                }
                
            } else if (trim($datos['accion']) === "U"){
                
                $pass = $this->encrypt->encode($datos['c_a']);
                $Sql = " UPDATE tb_intranet_users SET ";
                $Sql.= " usuario = '".$datos['n_u']."', ";
                $Sql.= " clave = '".$pass."', ";
                $Sql.= " nombres = '".utf8_decode($datos['n_e'])."', ";
                $Sql.= " apellidos = '".utf8_decode($datos['a_e'])."', ";
                $Sql.= " e_mail = '".$datos['e_l']."', ";
                $Sql.= " cod_encargado = '".$datos['c_r']."', ";
                $Sql.= " user_modificacion = '".$_SESSION['usuario']."', ";
                $Sql.= " fecha_modificacion = $fecha_act ";
                $Sql.= " WHERE cod_empleado  = ".trim($datos['c_e'])."; ";

                //$this->db->query($Sql);
                $dblxmaster = $CI->load->database('db_lx_master',true);
                $dblxmaster->query($Sql);
                if ($dblxmaster->_error_message()){
                    $dblxmaster->trans_rollback();
                    return "ERROR AL ACTUALIZAR USUARIO: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
                } 
                
                
                if(!empty($datos['p_s'])){
                    $Sql = " DELETE FROM tb_intranet_users_perfil ";
                    $Sql.= " WHERE cod_empl = '".$datos['c_e']."' ";
                    //echo $Sql;
                    //$this->db->query($Sql);
                    $dblxmaster = $CI->load->database('db_lx_master',true);
                    $dblxmaster->query($Sql);
                    if ($dblxmaster->_error_message()){
                        $dblxmaster->trans_rollback();
                        return "ERROR AL ELIMINAR USUARIO: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
                    }
                    
                    $ada =  explode(',',$datos['p_s']);
                    $con = count($ada);
                    //print_r($ada);
                    for($i=0; $i < $con; $i++){

                        $Sql = " INSERT INTO tb_intranet_users_perfil (cod_empl,perfil,estado,user_creacion,fecha_creacion) VALUES (";
                        $Sql.= " '".$datos['c_e']."','".$ada[$i]."',1,'".$_SESSION['usuario']."',$fecha_act);";                
//                        echo $Sql;
                        //$this->db->query($Sql);
                        $dblxmaster = $CI->load->database('db_lx_master',true);
                        $dblxmaster->query($Sql);
                        if ($dblxmaster->_error_message()){
                            $dblxmaster->trans_rollback();
                            return "ERROR AL GRABAR PERMISO USUARIO: ".$dblxmaster->_error_number()." ".$dblxmaster->_error_message().$Sql;
                        }  

                    }
                }
                        
                        
            }
            
            echo "OK";
            unset($Sql,$datos);
            
        }
    }

    function retUser($id){
            
            $Sql  = " SELECT a.cod_user,a.cod_empleado,a.cod_encargado,b.perfil,a.usuario,a.clave,a.nombres,a.apellidos,a.e_mail,a.estado ";
            $Sql .= " FROM tb_intranet_users AS a ";
            $Sql .= " LEFT JOIN tb_intranet_users_perfil AS b ";
            $Sql .= " ON a.cod_empleado = b.cod_empl "; 
            $Sql .= " WHERE a.cod_empleado = $id ";  
            $Sql .= " AND a.estado = 1;"; 
            //echo $Sql;        
            //$query=$this->db->query($Sql);
            $CI = &get_instance();
            $dblxmaster = $CI->load->database('db_lx_master',true);
            $query = $dblxmaster->query($Sql);
            $return_arr=null;
            $i = 0;
            
            foreach ($query->result_array() as $row) { 
                $i++;
                $return_arr[] = array(
                    'c_u'=>trim($row['cod_user']),
                    'c_e'=>trim($row['cod_empleado']),
                    'c_r'=>trim($row['cod_encargado']),
                    'p_s'=>trim($row['perfil']),
                    'u_o'=>trim($row['usuario']),
                    'c_l'=>utf8_decode(trim($this->encrypt->decode($row['clave']))),
                    'n_s'=>utf8_encode(trim($row['nombres'])),
                    'a_s'=>utf8_encode(trim($row['apellidos'])),
                    'e_l'=>utf8_encode(trim($row['e_mail'])),
                    'e_o'=>trim($row['estado'])); 
            }
            unset($query);
            return $return_arr;
    }
    
    function retEmpleado($cod_emp){
        
        $Sql  = " SELECT count(1) AS cantidad ";
        $Sql .= " FROM tb_intranet_users ";
        $Sql .= " WHERE cod_empleado = $cod_emp; ";
        //echo $Sql;
        //$query1 = $this->db->query($Sql);
        $CI = &get_instance();
        $dblxmaster = $CI->load->database('db_lx_master',true);
        $query1 = $dblxmaster->query($Sql);
        $row = $query1->row_array();
        //Acceso SQLSERVER NOMINA
        if((int)$row['cantidad'] == 0){
            $Sql  = " CALL PERSONAL_PI.sp_empleado_info('$cod_emp'); ";
            //echo$Sql;
	    $DB1 = $this->load->database('db_lx_master', true);
            $query= $DB1->query($Sql);
            $result = $query->result_array();
            $arr = null;
            
            if (!empty($result)){
                
                foreach ($result as $row){
                    $arr[] = array(
                        'apellido'=>trim(utf8_encode($row['apellido'])),
                        'nombre'=>trim(utf8_encode($row['nombre'])),
                        'cod_empresa'=>$row['COD_EMPRESA'],
                        'cod_ccosto'=>$row['COD_CCOSTO'],
                        'tx_ccosto'=>trim(utf8_encode($row['NOMBRE'])),
                        'orden'=>$row['orden']
                    );
                }
                
                echo "OK-".json_encode($arr);
                
            } else {
                echo "No existe Código de Empleado";
            } 
        } else {
            echo "El Código de Empleado ".$cod_emp." ya esta ingresado en el Sistema";
        }
        
    }
    
    function retResponsable($cod_emp){
        
        $Sql  = " CALL PERSONAL_PI.sp_empleado_info('$cod_emp'); ";
        //Acceso SQLSERVER NOMINA
	    $DB1 = $this->load->database('db_lx_master', true);
        $query= $DB1->query($Sql);
        $result = $query->result_array();
        $arr = null;
       
        if (!empty($result)){
            foreach ($result as $row){
                $arr[] = array(
                    'apellido'=>trim(utf8_encode($row['apellido'])),
                    'nombre'=>trim(utf8_encode($row['nombre'])),
                    'cod_empresa'=>$row['COD_EMPRESA'],
                    'cod_ccosto'=>$row['COD_CCOSTO'],
                    'tx_ccosto'=>trim(utf8_encode($row['NOMBRE'])),
                    'orden'=>$row['orden']
                );
            }
            
            echo "OK-".json_encode($arr);
            
        } else {
            echo "No existe Código de Empleado";
        } 
        
        
    }

}
?>
