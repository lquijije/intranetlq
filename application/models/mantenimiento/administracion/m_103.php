<?php

class m_103 extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries = FALSE;
        $this->db->flush_cache();
    }

    function delete($id) {
        $array = array('err' => '');
        if ((int) $id > 0) {
            try {
                $Sql = " UPDATE WEBPYCCA.dbo.tbl_noticia SET ";
                $Sql .= " fl_estado = 'E' ";
                $Sql .= " WHERE co_publicacion = " . (int) $id . "; ";
                $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL GRABAR NOTICIA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function publicar($id, $acc) {
        $array = array('err' => '');
        if ((int) $id > 0 && !empty($acc)) {
            try {
                $Sql = " UPDATE WEBPYCCA.dbo.tbl_noticia SET ";
                $Sql .= " fl_estado = '" . trim($acc) . "' ";
                $Sql .= " WHERE co_publicacion = " . (int) $id . "; ";
                $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                }
            } catch (Exception $e) {
                $array['err'] = "ERROR AL GRABAR NOTICIA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function grabar($accion, $co_not, $ti_noti, $ds_noti, $co_usuario, $co_estado) {
        $array = array('err' => '');
        if (!empty($accion) && !empty($ti_noti) && !empty($ds_noti) && (int) $co_usuario > 0 && !empty($co_estado)) {
            try {
                $Sql = " insertUpdateNoticias " . (int) $co_not . ",'" . utf8_decode($ti_noti) . "','" . trim(utf8_decode(str_replace("'", '"', $ds_noti))) . "'," . (int) $co_usuario . ",'" . $co_estado . "'," . (trim($accion) == "I" ? 0 : 1) . "; ";
                $resultado = $this->db->query($Sql);
                if ($this->db->_error_message() <> '') {
                    throw new Exception($this->db->_error_number() . " " . $this->db->_error_message());
                } else {
                    $row = $resultado->row_array();
                    if (!empty($row)) {
                        $array['co_err'] = (int) $row['co_error'];
                        $array['err'] = trim($row['tx_error']);
                    }
                }
            } catch (Exception $e) {
                $array['co_err'] = 1;
                $array['err'] = "ERROR AL GRABAR NOTICIA :" . $e->getMessage();
                log_P($e->getCode(), $e->getMessage());
            }
        } else {
            $array['err'] = "PARAMETROS INCOMPLETOS";
        }
        return $array;
    }

    function retNoticia($id) {
        $array = array('dat' => '', 'err' => '');
        $Sql = " SELECT co_publicacion,ds_titulo,ds_descripcion,fe_ingreso,co_usuario,fl_estado ";
        $Sql .= " FROM tbl_noticia ";
        $Sql .= " WHERE co_publicacion = $id ";
        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            $array['err'] = "ERROR AL RETORNAR NOTICIA <BR> COD_ERR:" . $this->db->_error_number() . " " . $this->db->_error_message();
        } else {
            $row = $result->row_array();
            if (!empty($row)) {
                $array['dat'] = array(
                    'co_not' => (int) $row['co_publicacion'],
                    'ds_not' => trim(utf8_encode($row['ds_titulo'])),
                    'ds_des' => trim(utf8_encode($row['ds_descripcion'])),
                    'fe_not' => trim($row['fe_ingreso']),
                    'co_usu' => trim($row['co_usuario']),
                    'fl_est' => trim($row['fl_estado']),
                    'ds_est' => trim($row['ds_estado'])
                );
            }
        }
        return $array;
    }

    function getNoticias() {
        $array = array('dat' => '', 'err' => '');
        $Sql = " SELECT co_publicacion,ds_titulo,ds_descripcion,fe_ingreso,co_usuario,fl_estado,CASE WHEN fl_estado = 'I' THEN 'INGRESADO' WHEN fl_estado = 'P' THEN 'PUBLICADO' WHEN fl_estado = 'E' THEN 'ELIMINADO' ELSE 'SIN ESTADO' END AS ds_estado  FROM WEBPYCCA..tbl_noticia; ";
        $result = $this->db->query($Sql);
        if ($this->db->_error_message() <> '') {
            $array['err'] = "ERROR AL RETORNAR NOTICIAS <BR> COD_ERR:" . $this->db->_error_number() . " " . $this->db->_error_message();
        } else {
            $rs = $result->result_array();
            if (!empty($rs)) {
                foreach ($rs as $row) {
                    $array['dat'][] = array(
                        'co_not' => (int) $row['co_publicacion'],
                        'ds_not' => trim(utf8_encode($row['ds_titulo'])),
                        'ds_des' => trim(utf8_encode($row['ds_descripcion'])),
                        'fe_not' => trim($row['fe_ingreso']),
                        'co_usu' => trim($row['co_usuario']),
                        'fl_est' => trim($row['fl_estado']),
                        'ds_est' => trim($row['ds_estado'])
                    );
                }
            }
        }
        return $array;
    }

}

?>
