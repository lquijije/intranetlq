<?php
class ccosto extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->db->trans_strict(FALSE);
        $this->db->trans_off();
        $this->db->save_queries=FALSE;
        $this->db->flush_cache();
    }
    
    function Almacenes(){
        
        $array = null;

        $Sql  = " SELECT DISTINCT a.co_bodega, a.descripcion, a.ruc,c.de_sri,a.direccion, a.tipo,c.de_ip_address,a.co_ccosto ";
        $Sql .= " FROM IVDBINVENTAR..ivtbbodega a ";
        $Sql .= " LEFT OUTER JOIN PODBPOS..posgenbodega c on a.co_bodega = c.co_bodega ";
        $Sql .= " WHERE tipo = 'A' ";
        $Sql .= " AND st_bodega='A' ";
        $Sql .= " ORDER BY descripcion";
        
        $result = $this->db->query($Sql);  
        if ($this->db->_error_message() <> ''){
            return "ERROR AL EJECUTAR CONSULTA: ".$this->db->_error_number()." ".$this->db->_error_message();
        } 

        if(count($result->result_array()) > 0){

            foreach($result->result_array() as $row){
                $array[] = array(  
                    'co_bdg'=> (int)$row['co_bodega'],
                    'ds_bdg'=> utf8_encode(trim($row['descripcion'])),
                    'ru_bdg'=> trim($row['ruc']),
                    'de_sri'=> trim($row['de_sri']),
                    'di_bdg'=> utf8_encode(trim($row['direccion'])),
                    'ti_bdg'=> trim($row['tipo']),
                    'ip_bdg'=> trim($row['de_ip_address']),
                    'co_cco'=> trim($row['co_ccosto'])
                );
            }

        }

        return $array;
        
    }

}
 
?>


