<?php

class general_model extends CI_Model {

    function __construct() {
        parent::__construct();
        /*  $this->db->trans_strict(FALSE);
          $this->db->trans_off();
          $this->db->save_queries=FALSE;
          $this->db->flush_cache(); */
    }

    function getColaborador($co_emp) {

        $array = null;

        if (!empty($co_emp)) {

            $Sql = " select a.CODIGO+'' as co_emp,b.NOMBRE+'' as no_emp,b.APELLIDO+'' as ap_emp,a.COD_CARGO+'' as co_carg,c.NOMBRE+'' as no_carg";
            $Sql .= " from rh_employee a ";
            $Sql .= " inner join RH_PERSON b ";
            $Sql .= " on a.CODIGO = b.CODIGO ";
            $Sql .= " inner join RH_CARGO c ";
            $Sql .= " on a.COD_CARGO = c.COD_CARGO ";
            $Sql .= " where a.CODIGO = $co_emp ";
            $Sql .= " and a.COD_ESTADO_EMP = 1; ";

            $this->db = $this->load->database('nomina', true);
            $query = $this->db->query($Sql);
            if ($this->db->_error_message() <> '') {
                return "ERROR AL OBTENER NOTICIAS: " . $this->db->_error_number() . " " . $this->db->_error_message() . $Sql;
            }

            $res = $query->row_array();

            if (count($res) > 1) {
                $array = array(
                    'co_emp' => (int) $res['co_emp'],
                    'no_emp' => trim(utf8_encode($res['no_emp'])),
                    'ap_emp' => trim(utf8_encode($res['ap_emp'])),
                    'co_carg' => trim($res['co_carg']),
                    'no_carg' => trim($res['no_carg'])
                );
            }
        }

        return $array;
    }

    function login($Usuario) {

        $array = null;

        if (!empty($Usuario)) {

            $Sql = " SELECT CONCAT(apellidos , ' ' , nombres) AS nombre, clave, cod_empleado,cod_encargado,cod_centro_costo,e_mail,cambio_pass ";
            $Sql .= " FROM tb_intranet_users ";
            $Sql .= " WHERE usuario = '$Usuario'";
            $Sql .= " AND estado = 1";
            $dblxmaster = $this->load->database('db_lx_master',true);
            $result = $dblxmaster->query($Sql);
            if ($dblxmaster->_error_message() <> '') {
                return "ERROR: " . $dblxmaster->_error_number() . " " . $dblxmaster->_error_message();
            }

            $array = $result->row_array();
        }

        return $array;
    }

    function actualiza_clave($usuario, $clave, $mail) {

        $Sql = " UPDATE tb_intranet_users SET ";
        if (!empty($clave)) {
            $Sql .= " clave = '" . $clave . "', ";
            $Sql .= " cambio_pass = 'S', ";
        }

        if (!empty($mail)) {
            $Sql .= " e_mail = '" . trim($mail) . "', ";
        }

        $Sql .= " user_modificacion = '" . trim($_SESSION['usuario']) . "', ";
        $Sql .= " fecha_modificacion = getdate() ";
        $Sql .= " WHERE usuario = '" . $usuario . "' ";
        $Sql .= " AND estado = 1; ";
        //$this->db->query($Sql);
        $dblxmaster = $this->load->database('db_lx_master',true);
        $dblxmaster-query($Sql);
        if ($this->db->_error_message()) {
            return "ERROR: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }
        return true;
    }

    function retCodigo($opcion, $aumenta, $tipo = '') {
        $Sql = "";

        if ($opcion === '1.1.1') {
            $Sql = "SELECT MAX(codigo)+1+'' AS codigo FROM tb_intranet_perfil;";
        } else if ($opcion === '1.1.2') {
            $Sql = "SELECT MAX(cod_user)+1+'' AS codigo FROM tb_intranet_users;";
        }

        //$query = $this->db->query($Sql);
        $dblxmaster = $this->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);
        $row = $query->row_array();
        $codigo = isset($row['codigo']) ? (int) $row['codigo'] : 1;
        if ($codigo === 0) {
            $codigo = 1;
        }
        if ($codigo < 10) {
            if ($aumenta === "S") {
                $codigo = '0' . $codigo;
            }
        }

        return $codigo;
    }

    function getWidget() {
        $Sql = " SELECT codigo,descripcion,vista ";
        $Sql.= " FROM tb_intranet_widget ";
        $Sql.= " WHERE estado = 1 ";
        $Sql.= " ORDER BY codigo ASC; ";
        //$query = $this->db->query($Sql);
        $dblxmaster = $this->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);
        if ($dblxmaster->_error_message()) {
            $dblxmaster->trans_rollback();
            return "ERROR AL OBTENER WIDGET: " . $dblxmaster->_error_number() . " " . $dblxmaster->_error_message();
        }
        return $query->result_array();
    }

    function getDepartamentos() {

        $Sql = " SELECT codigo,nombre,variable_1 ";
        $Sql.= " FROM dbo.tb_intranet_parametros ";
        $Sql.= " WHERE tipo = 6 ";
        $Sql.= " AND estado = 1; ";

        //$query = $this->db->query($Sql);
        $dblxmaster = $this->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);
        if ($dblxmaster->_error_message()) {
            $dblxmaster->trans_rollback();
            return "ERROR AL OBTENER DEPARTAMENTOS: " . $dblxmaster->_error_number() . " " . $dblxmaster->_error_message();
        }
        return $query;
    }

    function getAccesoMenuWidget($co_user) {

        $array = null;

        if (!empty($co_user)) {
            $Sql = " SELECT DISTINCT b.tipo,b.menu,c.vista ";
            $Sql.= " FROM tb_intranet_users_perfil AS a ";
            $Sql.= " INNER JOIN tb_intranet_perfil_accesos AS b ";
            $Sql.= " ON b.perfil = a.perfil ";
            $Sql.= " AND b.tipo = 2 ";
            $Sql.= " INNER JOIN tb_intranet_widget AS c ";
            $Sql.= " ON c.codigo = b.menu ";
            $Sql.= " WHERE a.cod_empl = '" . $co_user . "' ";
            $Sql.= " ORDER BY b.tipo ASC;";

            //$query = $this->db->query($Sql);
            $dblxmaster = $this->load->database('db_lx_master',true);
            $query = $dblxmaster->query($Sql);
            if ($dblxmaster->_error_message() <> '') {
                $error = & load_class('Exceptions', 'core');
                echo $error->show_error("ERROR AL OBTENER WIDGET: " . $dblxmaster->_error_number(), $dblxmaster->_error_message(), 'error_db');
                die;
            }

            $res = $query->result_array();

            if (!empty($res)) {
                foreach ($res as $row) {
                    $array[] = array(
                        'tipo' => $row['tipo'],
                        'menu' => $row['menu'],
                        'vista' => $row['vista']
                    );
                }
            }
        }

        return $array;
        
    }

    function getBotones() {

        $Sql = " SELECT a.codigo as cod_tipo,b.codigo as cod_para,a.nombre as nom_tipo, ";
        $Sql .= " b.nombre as nom_para,b.variable_1,b.variable_2,b.variable_3 ";
        $Sql .= " FROM tb_intranet_tipo_parametro a ";
        $Sql .= " INNER JOIN tb_intranet_parametros b ";
        $Sql .= " ON b.tipo = a.codigo ";
        $Sql .= " ORDER BY a.codigo,a.nombre; ";

        //$query = $this->db->query($Sql);
        $dblxmaster = $this->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);
        if ($dblxmaster->_error_message() <> '') {
            session_destroy();
            $_SESSION = null;
            $error = & load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL OBTENER BOTONES: " . $dblxmaster->_error_number(), $dblxmaster->_error_message(), 'error_db');
            die;
        }

        $salto = '';
        $array = null;

        if (count($query->result_array()) > 0) {
            $salto = $salto1 = '';

            foreach ($query->result_array() AS $row) {
                if ($salto !== $row['nom_tipo']) {
                    $array[$row['cod_tipo']] = array('tipo' => $row['nom_tipo']);
                    foreach ($query->result_array() AS $rq) {
                        if ($row['cod_tipo'] === $rq['cod_tipo']) {
                            $opcion[$rq['cod_para']] = $rq;
                        }
                    }
                    array_push($array[$row['cod_tipo']], $opcion);
                    $opcion = null;
                }
                $salto = $row['nom_tipo'];
            }
        } else {
            $array[] = null;
        }

        return $array;


//        $CI = get_instance();
//        $Sql = " SELECT a.codigo as cod_tipo,b.codigo as cod_para,a.nombre as nom_tipo, ";
//        $Sql .= " b.nombre as nom_para,b.variable_1,b.variable_2,b.variable_3 ";
//        $Sql .= " FROM tb_intranet_tipo_parametro a ";
//        $Sql .= " INNER JOIN tb_intranet_parametros b "; 
//        $Sql .= " ON b.tipo = a.codigo ";
//        $Sql .= " ORDER BY 1,3 ";
//        $query =$CI->db->query($Sql);
//        
//        $dom = new DOMDocument("1.0");
//        
//        $salto = '';
//        foreach ($query->result_array() as $row){
//            
//            if($salto !== $row['nom_tipo']){
//                $node = $dom->createElement("tipo"); 
//                $parnode = $dom->appendChild($node); 
//                $node->setAttribute("id",$row['cod_tipo']);
//                $node->setAttribute("name",$row['nom_tipo']);
//                
//                foreach ($query->result_array() as $raw){
//                    if($row['cod_tipo'] === $raw['cod_tipo']){
//                        $node = $dom->createElement("optiones"); 
//                        $node->nodeValue= $raw['nom_para']; 
//                        $parnode->appendChild($node);
//                        $node->setAttribute("id",$raw['cod_para']);
//                        $node->setAttribute("name",$raw['nom_para']);
//                        $node->setAttribute("var_1",$raw['variable_1']);
//                        $node->setAttribute("var_2",$raw['variable_2']);
//                        $node->setAttribute("var_3",$raw['variable_3']);
//                    }
//                }
//                
//            }
//            
//            $salto = $row['nom_tipo'];
//        }
        //$dom->save('temp/xmlMaster/master.xml');
    }

    function getPerfiles() {
        $Sql = 'SELECT codigo,descripcion ';
        $Sql .= 'FROM tb_intranet_perfil ';
        $Sql .= 'WHERE estado = 1 ';
        $Sql .= 'ORDER BY codigo ASC; ';
        //echo $Sql;
        //$query = $this->db->query($Sql);
        $dblxmaster = $this->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);
        if ($dblxmaster->_error_message()) {
            $dblxmaster->trans_rollback();
            return "ERROR AL OBTENER PERFILES: " . $dblxmaster->_error_number() . " " . $dblxmaster->_error_message() . $Sql;
        }

        $return_arr = null;
        foreach ($query->result_array() as $row) {
            $return_arr[] = array('codigo' => trim($row['codigo']), 'descripcion' => trim(utf8_encode($row['descripcion'])));
        }

        unset($query);
        echo json_encode($return_arr);
    }

    function getNoticias() {

        $array = null;

        $Sql = " SELECT co_publicacion,ds_titulo,ds_descripcion,fe_ingreso,co_usuario,fl_estado ";
        $Sql.= " FROM tbl_noticia ";
        $Sql.= " WHERE fl_estado = 'P' ";
        $Sql.= " AND year(fe_ingreso) = " . date('Y');
        $Sql.= " AND month(fe_ingreso) = " . date('m');
        $Sql.= " ORDER BY 4 DESC ";

        //$query = $this->db->query($Sql);
        $dblxmaster = $this->load->database('db_lx_master',true);
        $query = $dblxmaster->query($Sql);

        if ($dblxmaster->_error_message() <> '') {
            $error = & load_class('Exceptions', 'core');
            echo $error->show_error("ERROR AL RETORNAR NOTICIAS: " . $dblxmaster->_error_number(), $dblxmaster->_error_message(), 'error_db');
            die;
        }

        $res = $query->result_array();

        if (!empty($res)) {

            foreach ($res as $row) {
                $array[] = array(
                    'co_not' => $row['co_publicacion'],
                    'ti_not' => $row['ds_titulo'],
                    'de_not' => $row['ds_descripcion'],
                    'fe_not' => $row['fe_ingreso'],
                    'us_not' => $row['co_usuario'],
                    'es_not' => $row['fl_estado']
                );
            }
        }

        return $array;
    }

    function retPoctDoc($fec_ini, $fec_fin) {
        $bandera = true;
        $arrayError = null;
        $arreglo = null;
        $Sql = " exec podbpos.dbo.posGetServersMonitor";
        $query = $this->db->query($Sql);
        if ($this->db->_error_message()) {
            $this->db->trans_rollback();
            return "ERROR AL OBTENER ALMACENES: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        if ($bandera === true) {

            foreach ($query->result_array() as $row) {

                if (trim($row['tipo_base']) === 'sql') {
                    //print_r($row);
//                        die;
                    //echo $row['de_ip_address']."<br>";

                    if (pingServer($row['de_ip_address'], $row['puerto'])) {
                        $data = " podbpos..posgenResumenPorTipoFactElectronica '" . $fec_ini . " 00:00','" . $fec_fin . " 23:59'," . $row['co_bodega'] . " ";
                        $query = $this->db->query($data);
                        if ($this->db->_error_message()) {
                            $this->db->trans_rollback();
                            return "ERROR: " . $this->db->_error_number() . " " . $this->db->_error_message();
                        } else {
                            $bandera = true;
                        }

                        if ($bandera === true) {
                            foreach ($query->result_array() as $rowMysql1) {
                                $falta = $rowMysql1['ERRORES'] > 0 ? $rowMysql1['ERRORES'] : 0;
                                $arreglo[] = array('tipo' => $rowMysql1['ti_documento'],
                                    'totales' => $rowMysql1['TOTALES'],
                                    'aprobados' => $rowMysql1['APROBADOS'],
                                    'pendientes' => $rowMysql1['PENDIENTES'],
                                    'errores' => $falta);
                            }
                        }
                    } else {
                        $arrayError[] = "SIN CONEXION EN ALMACEN(SQL) " . $row['de_bodega'] . "<br>";
                    }
                } else if (trim($row['tipo_base']) === 'mysql') {

                    if (pingServer($row['de_ip_address'], $row['puerto'])) {

                        $dbMYSQL = new mysqli($row['de_ip_address'], "pos", "siste2365");
                        if ($dbMYSQL->connect_errno) {
                            $arrayError[] = "SIN CONEXION EN ALMACEN(MYSQL)" . $row['de_bodega'];
                            $bandera = false;
                        } else {
                            $bandera = true;
                        }

                        if ($bandera === true) {

                            $SqlMYSQL = " CALL podbpos.posgenResumenPorTipoFactElectronica ('" . trim($fec_ini) . " 00:00', '" . trim($fec_fin) . " 23:59'," . trim($row['co_bodega']) . "); ";
                            $queryMYSQL = $dbMYSQL->query($SqlMYSQL);
                            if (!$queryMYSQL) {
                                $arrayError[] = "ERROR AL EJECUTAR SP(MYSQL) " . $row['de_bodega'] . " - " . print_r($dbMYSQL->error, true);
                                $bandera = false;
                            } else {
                                $bandera = true;
                            }

                            if ($bandera === true) {

                                while ($rowMysql = $queryMYSQL->fetch_assoc()) {
                                    $falta = $rowMysql['ERRORES'] > 0 ? $rowMysql['ERRORES'] : 0;

                                    $arreglo[] = array('tipo' => $rowMysql['TIPO_DOC'],
                                        'totales' => $rowMysql['TOTALES'],
                                        'aprobados' => $rowMysql['APROBADOS'],
                                        'pendientes' => $rowMysql['PENDIENTES'],
                                        'errores' => $falta);
                                }
                            }

                            mysqli_close($dbMYSQL);
                        }
                    } else {
                        $arrayError[] = "SIN CONEXION EN ALMACEN(MYSQL) " . $row['de_bodega'] . "<br>";
                    }
                }
            }
        }

        //print_r($arreglo);
        //die;
        $resu = groupArray($arreglo, 'tipo');

        $group = null;
        $totales = $aprobados = $pendientes = $errores = 0;
        for ($i = 0; $i < count($resu); $i++) {
            for ($j = 0; $j < count($resu[$i]['groupeddata']); $j++) {
                $totales+=$resu[$i]['groupeddata'][$j]['totales'];
                $aprobados+=$resu[$i]['groupeddata'][$j]['aprobados'];
                $pendientes+=$resu[$i]['groupeddata'][$j]['pendientes'];
                $errores+=$resu[$i]['groupeddata'][$j]['errores'];
            }
            $group[] = array('tipo' => $resu[$i]['tipo'],
                'totales' => $totales,
                'aprobados' => $aprobados,
                'pendientes' => $pendientes,
                'errores' => $errores);
            $totales = $aprobados = $pendientes = $errores = 0;
        }

        return array('OK' => $group, 'ERROR' => $arrayError);
    }

    function retVentas($fecha, $hora) {

        if (!empty($fecha)) {
            $Sql = " [db_estadisticas].dbo._widget_ventas '" . trim($fecha) . "', '" . trim($hora) . "' ";
            $query = $this->db->query($Sql);
            if ($this->db->_error_message()) {
                $this->db->trans_rollback();
                return "ERROR: " . $this->db->_error_number() . " " . $this->db->_error_message() . $Sql;
            }
            $array = null;
            foreach ($query->result_array() as $row) {
                $array[] = array(
                    'tipo' => $row['tipo'],
                    'fecha' => date('Y-m-d', strtotime($row['fecha'])),
                    'dia' => $row['dia'],
                    'venta' => (int) $row['venta'],
                    //'venta'=>(int)number_format($row['venta'],2,'.',','),
                    'vtapy' => $row['VtaTPYCCA'],
                    'cobpy' => $row['CobTPYCCA']
                );
            }
            $arr = null;
            $a = 0;
            for ($i = 0; $i < count($array); $i++) {
                if ($array[$i]['tipo'] !== 'ANIO') {
                    $arr[] = array((int) $a, substr($array[$i]['dia'], 0, 2));
                    $a++;
                }
            }

            $label = json_encode($arr);

            if (count($array) > 0) {
                echo json_encode($array) . "/*/" . $label;
            }
        }
    }

    function valid_e_mail($user) {

        if (!empty($user)) {

            $Sql = ' SELECT COUNT(1) AS cant ';
            $Sql .= ' FROM tb_intranet_users ';
            $Sql .= ' WHERE cod_empleado = ' . $user;
            $Sql .= ' AND estado = 1  ';
            $Sql .= " AND (e_mail IS NULL OR e_mail = '') ";

            //$query = $this->db->query($Sql);
            $dblxmaster = $this->load->database('db_lx_master',true);
            $query = $dblxmaster->query($Sql);
            $row = $query->row_array();
            return $row['cant'];
        }
    }

    function retClima($ip) {

        $arr_actual = $arr_detalle = null;
        $error = '';

        if (!empty($ip)) {

            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });

            try {
                $Sql = " db_estadisticas.dbo._get_clima '" . trim($ip) . "';";
                $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
                $resultset = odbc_exec($conn, $Sql);

                do {

                    while ($row = odbc_fetch_array($resultset)) {

                        if (isset($row['resultset'])) {

                            switch ($row['resultset']) {

                                case "ACTUAL":

                                    $arr_actual = array(
                                        'mi_ciu' => trim(utf8_encode($row['mi_ciudad'])),
                                        'fe_act' => trim($row['fecha']),
                                        'co_cod' => (int) $row['condicion_codigo'],
                                        'ic_cod' => setWeatherIcon((int) $row['condicion_codigo']),
                                        'co_tex' => trim(utf8_encode($row['condicion_texto'])),
                                        'co_tem' => (int) $row['condicion_temperatura'],
                                        'at_hum' => (int) $row['atmosfera_humedad'],
                                        've_vie' => (float) $row['viento_velocidad']
                                    );

                                    break;
                                case "DETALLE":

                                    $arr_detalle[] = array(
                                        'ds_ciu' => trim(utf8_encode($row['ciudad'])),
                                        'co_cod' => (int) $row['condicion_codigo'],
                                        'ic_cod' => setWeatherIcon((int) $row['condicion_codigo']),
                                        'co_tex' => trim(utf8_encode($row['condicion_texto'])),
                                        'co_tem' => (int) $row['condicion_temperatura'],
                                        'at_hum' => (int) $row['atmosfera_humedad'],
                                        've_vie' => (float) $row['viento_velocidad']
                                    );

                                    break;
                            }
                        }
                    }
                } while (odbc_next_result($resultset));
            } catch (Exception $e) {
                $error = "ERROR AL RETORNAR CLIMA.";
            }
        }

        return array('res_1' => $arr_actual, 'res_2' => $arr_detalle, 'err' => $error);
    }

}
?>


