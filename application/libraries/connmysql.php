<?php

class connmysql {
    public $host ="";
    public $user = "";
    public $pass = "";
    public $db = "";
    public $cnn;
    public $err = "";
    
    /***************************************************************************/
    /************************* CONECCION MYSQLI ********************************/
    /***************************************************************************/
    
    function connectMysqli() {
        $con = mysqli_connect($this->host, $this->user, $this->pass, $this->db);
        if (!$con) {
            $this->err = "ERROR: Error en conexion mysqli.";
        } else {
            $this->cnn = $con;
        }
        return $this->cnn;
    }

    function closeMysqli() {
        mysqli_close($this->cnn);
    }
    
    /***************************************************************************/
    /*************************** CONECCION PDO *********************************/
    /***************************************************************************/
    
    function connectPDO(){
        
        try {
            $con = new PDO("mysql:host=".trim($this->host).";dbname=".trim($this->db)."",trim($this->user),$this->pass);
            $this->cnn = $con;
        }
        catch (PDOException $e)
        {
            $this->err = "ERROR: Error en conexion PDO --> ".$e->getMessage();
        }
        
        return $this->cnn;
    }
    
    function closePDO() {
        $this->cnn = null;
    }

}

?>
