<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/fpdf/fpdf.php'; 

class pdf_cr extends FPDF {
    
    function __construct(){
        parent::__construct();
    }
    
    var $tHeader;
    var $PaginaNueva;
    var $ImprimioCabecera;
    var $imprimeFooter;
    var $Orientacion;
    var $Titulo;
    var $SubTitulo;
    var $Campo_1;
    var $Campo_2;
    var $Campo_3;
    var $Campo_4;
    var $Campo_5;
    var $Campo_6;
    var $Grupo;
    var $javascript;
    var $n_js;
        
    function IncludeJS($script) {
            $this->javascript=$script;
    }

    function _putjavascript() {
            $this->_newobj();
            $this->n_js=$this->n;
            $this->_out('<<');
            $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R]');
            $this->_out('>>');
            $this->_out('endobj');
            $this->_newobj();
            $this->_out('<<');
            $this->_out('/S /JavaScript');
            $this->_out('/JS '.$this->_textstring($this->javascript));
            $this->_out('>>');
            $this->_out('endobj');
    }

    function _putresources() {
            parent::_putresources();
            if (!empty($this->javascript)) {
                    $this->_putjavascript();
            }
    }

    function _putcatalog() {
            parent::_putcatalog();
            if (!empty($this->javascript)) {
                    $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
            }
    }
    public function setImprimioCabecera($ImprimioCabecera){
        $this->ImprimioCabecera=$ImprimioCabecera;
    }

    public function setImprimeFooter($imprimeFooter){
        $this->imprimeFooter=$imprimeFooter;
    }
     
    public function setTitulo($titulo){
        $this->Titulo=$titulo;
    }
    
    public function setSubTitulo($SubTitulo){
        $this->SubTitulo=$SubTitulo;
    }
    public function Campo_1($Campo_1){
        $this->Campo_1=$Campo_1;
    }
    
    public function Campo_2($Campo_2){
        $this->Campo_2=$Campo_2;
    }
    
    public function Campo_3($Campo_3){
        $this->Campo_3=$Campo_3;
    }
    
    public function Campo_4($Campo_4){
        $this->Campo_4=$Campo_4;
    }
    
    public function Campo_5($Campo_5){
        $this->Campo_5=$Campo_5;
    }
    
    public function Campo_6($Campo_6){
        $this->Campo_6=$Campo_6;
    }
    
    public function setGrupo($Grupo){
        $this->Grupo=$Grupo;
    }
    
    public function getimprimeFooter(){
        return $this->imprimeFooter;
    }
   
    public function setPaginaNueva($PaginaNueva){
        $this->PaginaNueva=$PaginaNueva;
    }

    public function getPaginaNueva(){
        return $this->PaginaNueva;
    }
    
    public function setHeader($tHeader){
        $this->tHeader=$tHeader;
    }
    
    public function setOrientacion($Orientation){
        $this->Orientacion=$Orientation;
    }  
    
    public function Header() {
        

    }
    
    function Footer() {
        
    }
    
} 