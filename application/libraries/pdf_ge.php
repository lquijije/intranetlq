<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/fpdf/fpdf.php'; 

class pdf_ge extends FPDF {
    
    function __construct(){
        parent::__construct();
    }
    
    var $tHeader;
    var $PaginaNueva;
    var $ImprimioCabecera;
    var $imprimeFooter;
    var $Orientacion;
    var $Titulo;
    var $SubTitulo;
    var $Campo_1;
    var $Campo_2;
    var $Campo_3;
    var $Campo_4;
    var $Campo_5;
    var $Campo_6;
    var $Grupo;
    
    public function setImprimioCabecera($ImprimioCabecera){
        $this->ImprimioCabecera=$ImprimioCabecera;
    }

    public function setImprimeFooter($imprimeFooter){
        $this->imprimeFooter=$imprimeFooter;
    }
     
    public function setTitulo($titulo){
        $this->Titulo=$titulo;
    }
    
    public function setSubTitulo($SubTitulo){
        $this->SubTitulo=$SubTitulo;
    }
    public function Campo_1($Campo_1){
        $this->Campo_1=$Campo_1;
    }
    
    public function Campo_2($Campo_2){
        $this->Campo_2=$Campo_2;
    }
    
    public function Campo_3($Campo_3){
        $this->Campo_3=$Campo_3;
    }
    
    public function Campo_4($Campo_4){
        $this->Campo_4=$Campo_4;
    }
    
    public function Campo_5($Campo_5){
        $this->Campo_5=$Campo_5;
    }
    
    public function Campo_6($Campo_6){
        $this->Campo_6=$Campo_6;
    }
    
    public function setGrupo($Grupo){
        $this->Grupo=$Grupo;
    }
    
    public function getimprimeFooter(){
        return $this->imprimeFooter;
    }
   
    public function setPaginaNueva($PaginaNueva){
        $this->PaginaNueva=$PaginaNueva;
    }

    public function getPaginaNueva(){
        return $this->PaginaNueva;
    }
    
    public function setHeader($tHeader){
        $this->tHeader=$tHeader;
    }
    
    public function setOrientacion($Orientation){
        $this->Orientacion=$Orientation;
    }  
    
    public function Header() {
        
        if(isset($this->tHeader) && !empty($this->tHeader)){
            if ($this->tHeader == "1.1.1"){

                   $this->Image(site_url().'img/logo.png',-3,5,40);
                   $this->SetFont('helvetica', '',7);

                   $celda = $this->w - 250;
                   $this->Cell($celda);                                
                   $this->Cell(240,0 ,'Fecha de Impresion '.date("d/M/Y") , 0 , false, 'R', 0, '', 0, false, 'M', 'M'); 
                   $this->Ln(3);

                   $celda = $this->w - 120;
                   $this->Cell($celda);                                                
                   $this->Cell(110,0 ,'Hora de Impresion '.date("H:i:s") , 0 , false, 'R', 0, '', 0, false, 'M', 'M');

            } 
            
        } else {

            $this->Image(site_url().'img/logo.png',3,8,40);
            $this->SetFont('helvetica', '',7);

            $celda = $this->w - 250;
            $this->Cell($celda);                                
            $this->Cell(230,0 ,utf8_decode('Fecha de Impresión '.date("d/M/Y")) , 0 , false, 'R', 0, '', 0, false, 'M', 'M'); 
            $this->Ln(3);

            $celda = $this->w - 120;
            $this->Cell($celda);                                                
            $this->Cell(100,0 ,utf8_decode('Hora de Impresion '.date("H:i:s")) , 0 , false, 'R', 0, '', 0, false, 'M', 'M');

        }
        
        if(isset($this->Titulo) && !empty($this->Titulo)){
            $this->Ln(5);
            $this->SetFont('helvetica', 'B',15);
            $this->Cell(0, 10,utf8_decode($this->Titulo), 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $this->Ln(5);
        } 

        if(isset($this->SubTitulo) && !empty($this->SubTitulo)){
            $this->SetFont('helvetica', 'B',12);
            $this->Ln(2);
            $this->Cell(0, 10,utf8_decode($this->SubTitulo), 0, false, 'C', 0, '', 0, false, 'M', 'M');
            $this->Ln(5);
        }   

        if(isset($this->Campo_1) && !empty($this->Campo_1) || isset($this->Campo_2) && !empty($this->Campo_2) || isset($this->Campo_3) && !empty($this->Campo_3) || isset($this->Campo_4) && !empty($this->Campo_4) || isset($this->Campo_5) && !empty($this->Campo_5) ||isset($this->Campo_6) && !empty($this->Campo_6)){
            $this->SetFont('helvetica', 'B', 8);
            
            if ($this->Campo_1){
                $this->Cell(64, 25, $this->Campo_1, 0, false, 'L', 0, '', 0, false, 'M', 'M');
                if(!isset($this->Campo_2)){
                    $this->Ln(5); 
                }
            } 

            if ($this->Campo_2){
                $this->Cell(64, 25, $this->Campo_2, 0, false, 'L', 0, '', 0, false, 'M', 'M');
                if(!isset($this->Campo_3)){
                    $this->Ln(5); 
                }
            }
            if ($this->Campo_3){
                $this->Cell(64, 25, $this->Campo_3, 0, false, 'L', 0, '', 0, false, 'M', 'M');
                $this->Ln(5);
                if(!isset($this->Campo_3)){
                    $this->Ln(5); 
                }
            }
            if ($this->Campo_4){
                $this->Cell(64, 25, $this->Campo_4, 0, false, 'L', 0, '', 0, false, 'M', 'M');
                if(!isset($this->Campo_5)){
                    $this->Ln(5); 
                }
            }
            if ($this->Campo_5){
                $this->Cell(64, 25, $this->Campo_5, 0, false, 'L', 0, '', 0, false, 'M', 'M');
                if(!isset($this->Campo_6)){
                    $this->Ln(5); 
                }
            } 
            if ($this->Campo_6){
                $this->Cell(64, 25, $this->Campo_6, 0, false, 'L', 0, '', 0, false, 'M', 'M');
            } 
            $this->Ln(10);
        }   
        
        if(isset($this->Grupo) && !empty($this->Grupo)){

            $this->SetFont('helvetica', 'B', 7);
            $this->SetFillColor(0,83, 159);
            $this->SetDrawColor(0,83, 159);
            $this->SetTextColor(255,255,255);

            if ($this->Grupo == 'co_125'){
                $this->Ln(5);
                $this->Cell(10, 5, utf8_decode("#"), 0, 0,'C','true');
                $this->Cell(30, 5, utf8_decode("CÓDIGO"), 0, 0,'C','true');
                $this->Cell(75, 5, utf8_decode("NOMBRES"), 0, 0,'C','true');
                $this->Cell(75, 5, utf8_decode("TURNO"), 0, 0,'C','true');
                $this->Ln(5);
            } else if ($this->Grupo == 'co_126'){
                $this->Ln(5);
                $this->Cell(10, 5, utf8_decode("#"), 0, 0,'C','true');
                $this->Cell(40, 5, utf8_decode("ALMACÉN"), 0, 0,'C','true');
                $this->Cell(20, 5, utf8_decode("CODIGO"), 0, 0,'C','true');
                $this->Cell(65, 5, utf8_decode("NOMBRES"), 0, 0,'C','true');
                $this->Cell(55, 5, utf8_decode("TURNO"), 0, 0,'C','true');
                $this->Ln(5);
            } 
        }
        
        
        $this->SetFillColor(255,255,255);
        $this->SetDrawColor(204,204,204);
        $this->SetTextColor(0,0,0);


    }
    
    private function co_125() { 
        
        $this->Ln(5);
        $this->Cell(25, 5, utf8_decode("CI./RUC"), 0, 0,'C','true');
        $this->Cell(55, 5, utf8_decode("NOM. CLIENTE"), 0, 0,'C','true');
        $this->Cell(24, 5, utf8_decode("SUBTOTAL"), 0, 0,'C','true');
        $this->Cell(24, 5, utf8_decode("TARIFA 0%"), 0, 0,'C','true');
        $this->Cell(24, 5, utf8_decode("TARIFA 12%"), 0, 0,'C','true');
        $this->Cell(24, 5, utf8_decode("IVA"), 0, 0,'C','true');
        $this->Cell(24, 5, utf8_decode("TOTAL"), 0, 0,'C','true');
        $this->Ln(5);     
        
    }
    
    function Footer() {
        
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        //$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
        $this->Cell(0,10,$this->PageNo(),0,0,'R');
    }
    
} 