<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/fpdf/fpdf.php'; 

class pdf extends FPDF {
    
    function __construct(){
        parent::__construct();
    }
    var $col=0;
    var $y0;
    var $tHeader;
    var $PaginaNueva;
    var $Etiqueta;
    var $ImprimioCabecera;
    var $imprimeFooter;
    var $Orientacion;
    var $LineasImpresas;
    var $javascript;
    var $n_js;
        
    function IncludeJS($script) {
            $this->javascript=$script;
    }

    function _putjavascript() {
            $this->_newobj();
            $this->n_js=$this->n;
            $this->_out('<<');
            $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R]');
            $this->_out('>>');
            $this->_out('endobj');
            $this->_newobj();
            $this->_out('<<');
            $this->_out('/S /JavaScript');
            $this->_out('/JS '.$this->_textstring($this->javascript));
            $this->_out('>>');
            $this->_out('endobj');
    }

    function _putresources() {
            parent::_putresources();
            if (!empty($this->javascript)) {
                    $this->_putjavascript();
            }
    }

    function _putcatalog() {
            parent::_putcatalog();
            if (!empty($this->javascript)) {
                    $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
            }
    }
    
    public function setImprimioCabecera($ImprimioCabecera){
        $this->ImprimioCabecera=$ImprimioCabecera;
    }

    public function setImprimeFooter($imprimeFooter){
        $this->imprimeFooter=$imprimeFooter;
    }

    public function getimprimeFooter(){
        return $this->imprimeFooter;
    }
   
    public function setPaginaNueva($PaginaNueva){
        $this->PaginaNueva=$PaginaNueva;
    }

    public function getPaginaNueva(){
        return $this->PaginaNueva;
    }
    
    public function setHeader($tHeader){
        $this->tHeader=$tHeader;
    }
    
    public function setOrientacion($Orientation){
        $this->Orientacion=$Orientation;
    }  
    
    public function Header() {
        
        if(isset($this->tHeader) && !empty($this->tHeader)){
            if ($this->tHeader == "1.1.1"){

                   $this->Image(site_url().'img/logo.png',-3,5,40);
                   $this->SetFont('helvetica', '',7);

                   $celda = $this->w - 250;
                   $this->Cell($celda);                                
                   $this->Cell(240,0 ,'Fecha de Impresion '.date("d/M/Y") , 0 , false, 'R', 0, '', 0, false, 'M', 'M'); 
                   $this->Ln(3);

                   $celda = $this->w - 120;
                   $this->Cell($celda);                                                
                   $this->Cell(110,0 ,'Hora de Impresion '.date("H:i:s") , 0 , false, 'R', 0, '', 0, false, 'M', 'M');

            } else if ($this->tHeader == "1.1.2"){

                   $this->Image(site_url().'img/logo.png',3,8,40);
                   $this->SetFont('helvetica', '',7);

                   $celda = $this->w - 250;
                   $this->Cell($celda);                                
                   $this->Cell(230,0 ,'Fecha de Impresion '.date("d/M/Y") , 0 , false, 'R', 0, '', 0, false, 'M', 'M'); 
                   $this->Ln(3);

                   $celda = $this->w - 120;
                   $this->Cell($celda);                                                
                   $this->Cell(100,0 ,'Hora de Impresion '.date("H:i:s") , 0 , false, 'R', 0, '', 0, false, 'M', 'M');

            }
        }

    }
    
    function Footer() {
        
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        //$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
        $this->Cell(0,10,$this->PageNo().'/{nb}   ',0,0,'R');
    }
    
    /***************************************************************************/
    /*************************** SALTOS DE LINEA *******************************/
    /***************************************************************************/

    function SetCol($col)
    {
        //Set position at a given column
        $this->col=$col;
        $x=10+$col*95;
        $this->SetLeftMargin($x);
        $this->SetX($x);
    }

    function AcceptPageBreak()
    {
        //Method accepting or not automatic page break
        if($this->col<2)
        {
            //Go to next column
            $this->SetCol($this->col+1);
            //Set ordinate to top
            $this->SetY($this->y0);
            //Keep on page
            return false;
        }
        else
        {
            //Go back to first column
            $this->SetCol(0);
            //Page break
            return true;
        }
    }

    function ChapterTitle($num,$label)
    {
        //Title
        $this->SetFont('Arial','',12);
        $this->SetFillColor(200,220,255);
        $this->Cell(0,6,"Chapter  $num : $label",0,1,'L',1);
        $this->Ln(4);
        //Save ordinate
        $this->y0=$this->GetY();
    }

    function ChapterBody($txt)
    {
        $this->MultiCell(95,4,$txt);
        $this->Ln();
        $this->SetCol(0);
    }

    function PrintChapter($num,$title,$file)
    {
        //Add chapter
        $this->ChapterBody($file);
    }


} 