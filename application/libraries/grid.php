<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @category   Libreria del Grid
 * @package    Obras
 * @version    1
 * @author     John Quezada <john_quezada@hotmail.com>
 * @date       9 Agosto del 2013
 */

class grid
  {
        var $aColumns;
        var $aColumns_sp;
        var $aColumns_s;
        var $sIndexColumn;
        var $sIndexColumn_s;
	var $sTable;
        var $controlador='';
        var $where;
        var $join=array();
        var $pdf='';
        
        public function database ($sIndexColumn,$aColumns,$sTable) {
            
            $this->aColumns_s = $aColumns;
            $this->sIndexColumn_s = $sIndexColumn;
            $aColumnsi=array() ;
            $tablasN=array();
            foreach ($aColumns as &$valor){
              $valor = explode(" as ", trim($valor));  
              $tablasN[]=$valor[0];
              $valor = isset($valor[1])? $valor[1]:$valor[0];
              $palabra = explode(".",trim($valor));
              $palabra = isset($palabra[1])? $palabra[1]:$palabra[0];
              $aColumnsi[]=$palabra;
            }
            $this->aColumns=$aColumnsi;
            $this->aColumns_sp=$tablasN;
            /////////////////////////////////////////////
            $index = explode("as", trim($sIndexColumn));  
            $index = isset($index[1])? $index[1]:$index[0];
            $index=explode(".",$index);
            $index = isset($index[1])? $index[1]:$index[0];
            $this->sIndexColumn=trim($index);
            $this->sTable = $sTable;
        }
        
        
	private function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}
        

        
        public function where($where) {
            $this->where=$where;
        }
        
        private function ejewhere($CI,$like=''){
            $sql='';
            $where=(isset($this->where))? $this->where: '';
            if($like=='' && $where!=''){
                $sql=$where;
            }else if($like!='' && $where!=''){
                $sql="($where) and ($like)";
            } else if ($like!='' && $where==''){ 
                $sql=$like;
             }if($sql!='')
             $CI->db->where($sql, NULL, FALSE);   
        }
        
        public function inner_join($controlador,$variables,$estado='') {
            $this->join[]=array($controlador,$variables,$estado);
        }
		
		

        private function ejeinner_join($CI){
            foreach ($this->join as &$valor) {
                  $CI->db->join($valor[0],$valor[1],$valor[2]);
            }
        }
	
        public function jscript($tabla,$url)
                
	{ 
         $js=       "  $(document).ready(function() {
	  			$('#$tabla').dataTable( { ";
	 $js.='				"bProcessing": true,
					"bServerSide": true,
					"sAjaxSource": "'.$url.'",
					"fnServerData": function( sUrl, aoData, fnCallback, oSettings,Empresa,Sucursal ) {
						oSettings.jqXHR = $.ajax( {
							"url": sUrl,
							"data": aoData,
							"success": fnCallback,
							"dataType": "jsonp",
							"cache": false
						} );
					}
				} );
			} ); ';
         return $js;
	}
        
        public function modificar ($controlador){
            $this->controlador = $controlador;
        }
        
        public function pdf ($controlador){
            $this->pdf = $controlador;
        }
        
	public function ejecucion(){ 
        $CI =& get_instance();    
	$limit='10';
        $offset='0';
        $iDisplayStart=$CI->input->get('iDisplayStart');     
        $iDisplayLength=$CI->input->get('iDisplayLength');  
        $limit=$iDisplayLength.'';
        if ($iDisplayStart && $iDisplayLength!= '-1' )
	{ $limit=$iDisplayStart; $offset=$iDisplayLength; }
        $iSortCol_0=$CI->input->get('iSortCol_0');
        $sSearch=$CI->input->get('sSearch');
        $bandera=0;
        $or_like='';
	if ($sSearch!=FALSE && $sSearch!= '' )
	{
                $cantidad=count($this->aColumns);
		for ( $i=0 ; $i<$cantidad ; $i++ )
		{
			if ( $CI->input->get('bSearchable_'.$i) && $CI->input->get('bSearchable_'.$i) == "true" )
			{
				if($bandera==0)
                                {
                                    $or_like.=$this->aColumns_sp[$i]." LIKE '%$sSearch%' ";  $bandera=1;}
                                else {
                                     if($or_like!='') $or_like.=' or ';
                                     $or_like.=$this->aColumns_sp[$i]." LIKE '%$sSearch%' "; 
                                }
			}
		} 
	}
        $columnas=implode(",",$this->aColumns_s);
        $select=$this->sIndexColumn_s.','.$columnas;
        $CI->db->distinct();
        $CI->db->select($select,FALSE);       
        $CI->db->from($this->sTable);
        $this->ejeinner_join($CI);
        $this->ejewhere($CI,$or_like);
        $iFilteredTotal=$CI->db->count_all_results();
       // var_dump($CI->db->last_query());
        $CI->db->distinct();
        $CI->db->select($select,FALSE);       
        $CI->db->from($this->sTable);
        if($limit!=''  && $offset != '-1'){
            $CI->db->limit($limit,$offset);
        }
        $this->ejeinner_join($CI);
        $bandera=0;
        $this->ejewhere($CI,$or_like);
	if ($iSortCol_0)
	{
		for ( $i=0 ; $i<intval( $CI->input->get('iSortingCols')) ; $i++ )
		{
			if ( $CI->input->get('bSortable_'.intval($CI->input->get('iSortCol_'.$i)) ) == "true")
			{
                                if(isset($this->aColumns[intval($CI->input->get('iSortCol_'.$i))])){
				$columna=$this->aColumns[intval($CI->input->get('iSortCol_'.$i))];
				$orden =($CI->input->get('sSortDir_'.$i)==='asc' ? 'asc' : 'desc');
                                $CI->db->order_by($columna,$orden); 
                                }
			}
		}
	}
      $rResult = $CI->db->get();
      // var_dump($CI->db->last_query());
        $iTotal=$rResult->num_rows();
	$output = array(
		"sEcho" => intval($CI->input->get('sEcho')),
		"iTotalRecords" => (int)$iTotal,
		"iTotalDisplayRecords" => (int)$iFilteredTotal,
		"aaData" => array()
	);
	foreach ($rResult->result_array() as $aRow)
	{   
            $row = array();
		for ( $i=0 ; $i<count($this->aColumns) ; $i++ )
		{
                        if ( $this->aColumns[$i] == "version" )
			{      
                            $row[] =($aRow[$this->aColumns[$i]]=="0") ? '-' : trim($aRow[$this->aColumns[$i]]);
			}
			else if ( $this->aColumns[$i] != ' ' )
			{
				$row[] = trim($aRow[$this->aColumns[$i]]);
			}
		}
                if($this->controlador!=''){
                            $row[] = '<a href="'.site_url().$this->controlador.'/modificar/'.trim($aRow[$this->sIndexColumn]).'">editar</a>&nbsp;&nbsp;<a href="'.site_url().$this->controlador.'/eliminar/'.trim($aRow[$this->sIndexColumn]).'">eliminar</a>';
                        }
                if($this->pdf!=''){
                            $row[] = '<a href="'.site_url().$this->pdf.'/pdf/'.trim($aRow[$this->sIndexColumn]).'">pdf</a>';
                        }        
		$output['aaData'][] = $row;
	}
	$callback=$CI->input->get('callback');
	if($callback)
            echo $callback.'('.json_encode( $output ).');';
        else
            echo '('.json_encode( $output ).');';
        unset($CI);
  }
  
  
}
/* End of file Datatables.php */
/* Location: ./application/libraries/Datatables.php */
        
        
        
        