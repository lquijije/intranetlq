<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/barcodegen/class/BCGFontFile.php'; 
require_once dirname(__FILE__) . '/barcodegen/class/BCGColor.php'; 
require_once dirname(__FILE__) . '/barcodegen/class/BCGDrawing.php'; 
require_once dirname(__FILE__) . '/barcodegen/class/BCGcode39.barcode.php';

class barcodegen extends FPDF
{
    var $scode;
    var $sfont;
    var $scolor_fondo1;
    var $scolor_fondo2;
    var $scolor_fondo3;
    
    var $scolor_barra1;
    var $scolor_barra2;
    var $scolor_barra3;
    
    var $ssetScale;
    var $setThickness;
    var $surl;
    
    var $tamanio;
    function __construct()
    {
        parent::__construct();
        $this->scolor_barra1=0;
        $this->scolor_barra2=0;
        $this->scolor_barra3=0;
        $this->scolor_fondo1=255;
        $this->scolor_fondo2=255;
        $this->scolor_fondo3=255;
        $this->scode='BCGcode39.barcode';
        $this->sfont='Arial.ttf';
        $this->tamanio=18;
    }
    
     public function generar($text){
        require_once('class/'.$this->scode.'.php');
         $color_black = new BCGColor($this->scolor_barra1,$this->scolor_barra2,$this->scolor_barra3);
        $color_white = new BCGColor($this->scolor_fondo1,$this->scolor_fondo2,$this->scolor_fondo3);
        $font = new BCGFontFile('./font/'.$this->sfont, 18);
        $drawException = null;
        try {
                $code = new BCGcode39();
                $code->setScale(2); // Resolution
                $code->setThickness(30); // Thickness
                $code->setForegroundColor($color_black); // Color of bars
                $code->setBackgroundColor($color_white); // Color of spaces
                $code->setFont($font); // Font (or 0)
                $code->parse($text); // Text
        } catch(Exception $exception) {
                $drawException = $exception;
        }

        /* Here is the list of the arguments
        1 - Filename (empty : display on screen)
        2 - Background color */
        $drawing = new BCGDrawing('images/barra', $color_white);
        if($drawException) {
                $drawing->drawException($drawException);
        } else {
                $drawing->setBarcode($code);
                $drawing->draw();
        }
        // Header that says it is an image (remove it if you save the barcode to a file)
        header('Content-Type: image/png');
        header('Content-Disposition: inline; filename="barcode.png"');
        // Draw (or save) the image into PNG format.
        $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
        ///////////////////////////////
    }
    
     public function color_fondo($ImprimioCabecera){
        $this->ImprimioCabecera=$ImprimioCabecera;
    }
    
    public function color_barra($ImprimioCabecera){
        $this->ImprimioCabecera=$ImprimioCabecera;
    }
    
    public function code($ImprimioCabecera){
        $this->ImprimioCabecera=$ImprimioCabecera;
    }
    public function font($ImprimioCabecera){
        $this->font=$ImprimioCabecera;
    }
    
    
    
}

?>
