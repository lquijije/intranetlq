
<style>

.table {
    background: #FFF !important;
}

.wi {
    padding-top: 2px;
}
.table td {
    background: #FFF;
}
.table th {
    background: #39bee8 !important;
    color: #FFF !important;
}
.col_td > td {
    background: #AFE5F6 !important;
}

.table > tbody > tr > td {
    border: 1px solid #ccc;
}
.table > tbody > tr > td > span {
    display: block;
}
.border-l {
    border-left: 0px none !important;
}

.border-r {
    border-right: 0px none !important;
}

.bg_foot {
     background: rgb(251, 183, 183) none repeat scroll 0% 0%;
     font-weight: bold;
}
.tableWrapper {
    margin-top: 10px;
    overflow-x: auto;
/*  width: 100%;
  min-width: 300px;
  margin: 0 auto;*/
}

</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-line-chart"></i> Ventas Por Hora</div>
                    </div>
                </div>
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <?php 
                            echo form_open('estadisticas/co_2001',array('id' => 'form_venHo','name'=>'form_venHo')); 
                            ?>   
                                <div class="row">
                                    <div class="spa-3">
                                        <label>Fecha</label>
                                        <input id="txt_fec" name="txt_fec" class="form_datetime form-control" value="<?php echo $fec; ?>" readonly="" type="text">
                                    </div>
                                    <div class="spa-3">
                                        <label>Monto</label>
                                        <?php echo $cmb_mon ?>
                                    </div>
                                    <div class="spa-1">
                                        <label class="out-text">Consultar</label>
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-send"></i> Consultar</button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                
                <?php
                    if(empty($dataVtaDia['err'])){
                        
                        if(!empty($dataVtaDia['data'])){

                            $data = groupArray($dataVtaDia['data'],'ds_ciu');
                            
                            array_order_column($data,'ds_ciu');

                            $to_10 = $to_11 = $to_12 = $to_13 = $to_14 = $to_15 = $to_16 = $to_17 = $to_18 = $to_19 = $to_20 = $to_21 = $to_22 = $to_23 = 0;

                            ?>
                            <div class="row">
                                <div class="spa-12">
                                    <div class="box box-primary">                   
                                        <div class="tableWrapper">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">ALMAC&Eacute;N</th>
                                                        <th class="text-center">10:00 AM</th>
                                                        <th class="text-center">11:00 AM</th>
                                                        <th class="text-center">12:00 PM</th>
                                                        <th class="text-center">13:00 PM</th>
                                                        <th class="text-center">14:00 PM</th>
                                                        <th class="text-center">15:00 PM</th>
                                                        <th class="text-center">16:00 PM</th>
                                                        <th class="text-center">17:00 PM</th>
                                                        <th class="text-center">18:00 PM</th>
                                                        <th class="text-center">19:00 PM</th>
                                                        <th class="text-center">20:00 PM</th>
                                                        <th class="text-center">21:00 PM</th>
                                                        <th class="text-center">22:00 PM</th>
                                                        <th class="text-center">23:00 PM</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        foreach ($data as $row){
                                                            
                                                            $clima10 = $row['groupeddata'][0]['cl_10'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_10']) : array('');
                                                            $clima11 = $row['groupeddata'][0]['cl_11'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_11']) : array('');
                                                            $clima12 = $row['groupeddata'][0]['cl_12'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_12']) : array('');
                                                            $clima13 = $row['groupeddata'][0]['cl_13'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_13']) : array('');
                                                            $clima14 = $row['groupeddata'][0]['cl_14'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_14']) : array('');
                                                            $clima15 = $row['groupeddata'][0]['cl_15'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_15']) : array('');
                                                            $clima16 = $row['groupeddata'][0]['cl_16'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_16']) : array('');
                                                            $clima17 = $row['groupeddata'][0]['cl_17'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_17']) : array('');
                                                            $clima18 = $row['groupeddata'][0]['cl_18'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_18']) : array('');
                                                            $clima19 = $row['groupeddata'][0]['cl_19'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_19']) : array('');

                                                            $clima20 = $row['groupeddata'][0]['cl_20'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_20']) : array('');
                                                            $clima21 = $row['groupeddata'][0]['cl_21'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_21']) : array('');
                                                            $clima22 = $row['groupeddata'][0]['cl_22'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_22']) : array('');
                                                            $clima23 = $row['groupeddata'][0]['cl_23'] <>'No Data|No Data' ? explode('|',$row['groupeddata'][0]['cl_23']) : array('');

                                                            ?>
                                                                <tr class="col_td">
                                                                    <td colspan="2"><?php echo $row['ds_ciu']; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima10[0]); //echo  '<span>'; echo isset($clima10[1]) ?$clima10[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima11[0]); //echo '<span>'; echo isset($clima11[1]) ?$clima11[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima12[0]); //echo '<span>'; echo isset($clima12[1]) ?$clima12[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima13[0]); //echo '<span>'; echo isset($clima13[1]) ?$clima13[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima14[0]); //echo '<span>'; echo isset($clima14[1]) ?$clima14[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima15[0]); //echo '<span>'; echo isset($clima15[1]) ?$clima15[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima16[0]); //echo '<span>'; echo isset($clima16[1]) ?$clima16[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima17[0]); //echo '<span>'; echo isset($clima17[1]) ?$clima17[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima18[0]); //echo '<span>'; echo isset($clima18[1]) ?$clima18[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima19[0]); //echo '<span>'; echo isset($clima19[1]) ?$clima19[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima20[0]); //echo '<span>'; echo isset($clima20[1]) ?$clima20[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima21[0]); //echo '<span>'; echo isset($clima21[1]) ?$clima21[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima22[0]); //echo '<span>'; echo isset($clima22[1]) ?$clima22[1]:''; echo '</span>'; ?></td>
                                                                    <td class="text-center"><?php echo setWeatherIcon((int)$clima23[0]); //echo '<span>'; echo isset($clima23[1]) ?$clima23[1]:''; echo '</span>'; ?></td>
                                                                </tr>
                                                            <?php

                                                            foreach ($row['groupeddata'] as $raw){
                                                                $to_10 += $raw['ds_10'];
                                                                $to_11 += $raw['ds_11']; 
                                                                $to_12 += $raw['ds_12'];
                                                                $to_13 += $raw['ds_13'];
                                                                $to_14 += $raw['ds_14'];
                                                                $to_15 += $raw['ds_15'];
                                                                $to_16 += $raw['ds_16'];
                                                                $to_17 += $raw['ds_17'];
                                                                $to_18 += $raw['ds_18'];
                                                                $to_19 += $raw['ds_19'];
                                                                $to_20 += $raw['ds_20'];
                                                                $to_21 += $raw['ds_21'];
                                                                $to_22 += $raw['ds_22'];
                                                                $to_23 += $raw['ds_23'];
                                                                ?>
                                                                    <tr>
                                                                        <td class="border-r"></td>
                                                                        <td class="border-l"><?php echo $raw['ds_bdg']; ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_10'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_11'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_12'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_13'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_14'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_15'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_16'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_17'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_18'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_19'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_20'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_21'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_22'],0); ?></td>
                                                                        <td class="text-right">$<?php echo number_format($raw['ds_23'],0); ?></td>
                                                                    </tr>
                                                                <?php 
                                                            }
                                                        }
                                                    ?>
                                                    <tr class="bg_foot">
                                                        <td colspan="2">TOTALES</td>
                                                        <td class="text-right">$<?php echo number_format($to_10,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_11,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_12,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_13,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_14,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_15,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_16,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_17,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_18,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_19,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_20,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_21,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_22,0); ?></td>
                                                        <td class="text-right">$<?php echo number_format($to_23,0); ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <?php 

                        }

                        ?>

                        <?php 
                    }
                ?>
                
                <?php
                    if(!empty($dataVtaGru['data'])){
                        ?>
                        <div class="row">
                            <div class="spa-4">
                                <div class="box box-primary">
                                    <div style="margin-top:10px;">  
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Grupo</th>
                                                    <th class="text-center">Monto</th>
                                                    <th class="text-center">Porcentaje</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($dataVtaGru['data'] as $r){
                                                    //print_r($r);
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $r['ds_gru']." ".$r['ds_let']; ?></td>
                                                            <td class="text-right"><?php echo number_format($r['va_can'],0); ?></td>
                                                            <td class="text-right"><?php echo $r['va_por']."%"; ?></td>
                                                        </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                    }
                ?>
            </section>
        </div>
    </div>
</div>
