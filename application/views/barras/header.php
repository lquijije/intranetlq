<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> PYCCA | INTRANET</title>
        <link rel="icon" type="image/ico" href="<?php echo site_url() ?>img/favicon.ico" />
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo site_url() ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url() ?>css/style.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url() ?>css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url() ?>css/animation.css" rel="stylesheet" type="text/css" />

        <!--[if IE 9 ]><link type="text/css" href="<?php echo site_url() ?>css/ie.css"><![endif]-->

        <?php
            if(isset($css) && !empty($css))
            agregarcss($css) 
        ?>
               
        <?php 
            if(isset($js) && !empty($js))
            agregarjavascript($js); 
        ?>
        
    </head>
   
    <body id="bodyPage" style="background: #00539F;">
    <input type='hidden' id='txt_urlGe' name="txt_urlGe" value='<?php echo site_url() ?>' />
    <header class="header">
        <div class="spa-12">
            <div class="top-left">
                <a href="<?php echo site_url('general/principal')?>"><img src="<?php echo site_url()?>img/logo-hover.png" style="float: left; margin-right: 15px;" /></a>
            </div>
            <div class="top-right">
                
                <ul class="nav navbar-nav">
                    <li class="user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php if(isset($_SESSION['usuario'])){ ?>
                            <span class="titulo-1" style="font-size: 22px;text-shadow: 2px 2px 1px #000;"><?php  echo utf8_encode(ucwords(strtolower($_SESSION['nombre']))); ?> <i class="fa fa-user"></i> <i class="caret"></i></span>
                            <?php } ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <li><a href="<?php echo site_url().'login/c_pass'; ?>" >Cambiar Contraseña</a></li>
                                <li><a href="<?php echo site_url().'login/c_login/cerrar_session'; ?>" >Cerrar sesi&oacute;n</a></li>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <?php 
                if(isset($_SESSION['usuario']) && !empty($_SESSION['usuario'])){
                    ?>
                        <div style="position: absolute;right: 18px;top: 21px;">
                            <button type="button" class="btn-out openMenu" id="btn_buscar" name="btn_buscar" title="Buscar" style="border: 1px solid rgb(255, 255, 255);"><i class="fa fa-reorder"></i></button>
                        </div>
                    <?php 
                }
                ?>
            </div>
        </div>
    </header>
    