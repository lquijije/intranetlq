
<script type="text/javascript" src="<?php echo site_url('js/jquery.autocomplete.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/jquery-ui-1.10.3.js'); ?>"></script> 

<div id="menuEmergente">
    
    <div style="width: 100%;">
        <div class="row">
            <div style="position: absolute; right: 20px; top: 10px;">
                <a href="#" class="text-size-1-25 closeMenu"><i class="fa fa-close"></i></a>
            </div>
            <div class="col-md-12">
                <h3 style="margin-top: 20px; margin-bottom: 20px; color: #FFF;">Buscar Aplicaciones</h3>
                <input id="txt_searchGe" name="txt_searchGe" class="form-control" type="text" value="">
            </div>
            
            <div class="col-md-12">
                <section class="sidebar">
                    <?php if(isset($_SESSION['menuCompl'])){ echo $_SESSION['menuCompl'];}?>
                </section>
            </div>
            
        </div>
    </div>
            
</div>

<script>
<?php if(isset($_SESSION['menu'])) {?>
    $("#txt_searchGe").keypress(function() {
        var urlGe = $('#txt_urlGe').val();
        $('#sidebar-menu').css('display','none');

        var data = <?php echo json_encode($_SESSION['menu']);?>; 
        $('#txt_searchGe').autocomplete({
            source:data,
            select: function(event, ui) {
                window.location.href = urlGe+ui.item.urlinvocar;
                event.preventDefault();
                $('#txt_searchGe').val(ui.item.label);
            },
            focus: function(event, ui) {
                event.preventDefault();
                $('#txt_searchGe').val(ui.item.label);
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ) {
            ul.addClass('list-unstyled');
            ul.addClass('list-menu');
            return $("<li>")
            .append("<i class='fa fa-cogs me-list menu-color-"+item.id+"'></i> <a href='"+urlGe+"'>"+item.label+"</a>")
            .appendTo(ul);
        };
    });
<?php } ?>
</script>