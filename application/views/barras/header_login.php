<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> PYCCA | INTRANET</title>
        <link rel="icon" type="image/ico" href="<?php echo site_url('img/favicon.ico'); ?>" />
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo site_url() ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url() ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url() ?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url() ?>css/style.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url() ?>css/responsive.css" rel="stylesheet" type="text/css" />

        <!--[if IE 9 ]><link type="text/css" href="<?php echo site_url() ?>css/ie.css"><![endif]-->

        <?php
        if (isset($css) && !empty($css))
            agregarcss($css)
            ?>
        <?php
        if (isset($js) && !empty($js))
            agregarjavascript($js)
            ?>
    </head>

    <?php
    $message = '';
    $error = '';

    if ($this->session->flashdata('message')) {
        $message = $this->session->flashdata('message');
    }

    if ($this->session->flashdata('error')) {
        $error = $this->session->flashdata('error');
    }

    if (!empty($err_login)) {
        $error = $err_login;
    }
    ?>

    <?php if (!empty($message)) { ?>
        <script type="text/javascript">
            $(document).ready(function () {
                errorGeneral('INICIO SESI&Oacute;N', '<?php echo quitar_saltos($message); ?>', 'danger');
            });
        </script>
    <?php } ?>

    <?php if (!empty($error)) { ?>
        <script type="text/javascript">
            $(document).ready(function () {
                errorGeneral('INICIO SESI&Oacute;N', '<?php echo quitar_saltos(trim($error)); ?>', 'danger');
            });
        </script>
    <?php } ?>

    <body style="background: #00539F;">
        
        <header class="header">
            <div class="spa-12">
                <div class="top-left">
                    <a href="<?php echo site_url('login/c_login') ?>"><img src="<?php echo site_url() ?>img/logo-hover.png" /></a>
                </div>
                <div class="top-right">
                    <a id="activeDropdown" class="text-blanco text-size-1-5">
                        <i class="fa fa-user"></i>
                        <span class="titulo-1">Iniciar Sesión</span>
                    </a>
                </div>
            </div>
        </header>

        <div id="fadeLogin" class="fadebox"></div>       
        <div class=" col-md-2 dropdown-login">
            <div class="box-0 box-primary">
                <div class="box-body">
                    <div class="row">

                        <?php
                        $attributes = array('id' => 'f_login', 'name' => 'f_login');
                        echo form_open('login/c_login', $attributes);
                        ?>
                        <?php // echo validation_errors();  ?>


                        <div class="box-header">
                            <h3 class="box-title titulo-1">Iniciar sesi&oacute;n</h3>
                        </div>

                        <div class="box-body">

                            <div class="spa-12">
                                <label class="titulo-1">Usuario</label>
                                <?php
                                $data = array('name' => 'txt_usuario', 'id' => 'txt_usuario', 'value' => set_value('txt_usuario'), 'maxlength' => '15', 'class' => 'form-control', 'placeholder' => 'Usuario', 'autocomplete' => 'off');
                                echo form_input($data);
                                ?>
                            </div>
                            <div class="spa-12">
                                <label class="titulo-1">Contraseña</label>
                                <?php
                                $data = array('id' => 'txt_clave', 'name' => 'txt_clave', 'value' => '', 'class' => 'form-control', 'placeholder' => 'Contraseña', 'autocomplete' => 'off');
                                echo form_password($data);
                                unset($data);
                                ?>     
                            </div>  

                            <div class="pull-right" style="margin-top: 10px">           
                                <button type="submit" class="btn btn-primary titulo-1">Iniciar sesi&oacute;n</button>
                            </div>
                        </div>
                        </form>

                    </div> 
                </div> 
            </div>
        </div>