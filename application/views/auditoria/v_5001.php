<style>
    .content {
        overflow:unset;
    }
    .img-circle {
        border: 5px solid #85C5C2;
        margin-top: 10px;
    }
    .box {
        border-radius: 3px;
        background: #FFF;
    }
    .box .box-footer {
        background: #323A45;
    }
    .color_age {
        color: #85C5C2;
    }
    .btn_abs {
        position: absolute;
        right: 9px;
    }
    .box .box-body {
        min-height: 166px;
    }
    #pieAdmin  .legend > table {
        color: #FFF !important;
    }
    .legendLabel{
        text-align: left;
    }
    #reportrange {
        cursor: pointer;
        margin-right: 50px;
        font-size: 18px;
        color: #FFF;
    }
    h3 > span > b {
        font-size:25px;
    }
    table.dataTable {
        margin-top: 1px !important;
        margin-bottom: 1px !important;
    }
    .label-danger {
        position: relative;
        border-radius: 0.25em !important;
        right: 0px;
    }
    /*#detTabArtId_wrapper {
        width: 900px;
        margin: 0 auto;
    }*/
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <section class="content invoice">
            <div class="row">
                <div class="col-xs-6" style="margin-bottom: 10px;">
                    <h3 class="text-blanco"><i class="fa fa-list-ol"></i> Administrar Tomas</h3>
                </div>
                <div class="col-xs-6">
                    <div class="pull-right" id="reportrange">
                        <i class="fa fa-calendar"></i> 
                        <span>
                            <?php
                            $fec_ini = date('Y-m-d', strtotime('-14 days'));
                            $fec_fin = date('Y-m-d');
                            echo conveMes(date('n', strtotime($fec_ini))) . " " . date("j, Y", strtotime($fec_ini));
                            ?> - <?php echo conveMes(date('n')) . " " . date("j, Y");
                            ?>
                        </span> 
                        <b class="caret"></b>
                        <?php
                        $data = array(
                            'f_i' => $fec_ini,
                            'f_f' => $fec_fin
                        );

                        echo form_hidden($data);
                        ?>
                    </div>
                    <button type="button" class="btn-out ref_pag" title="Refrescar" style="margin:0; right: 20px;"><i class="fa fa-refresh"></i></button>
                </div>
            </div>

            <div class="row desPanel">
                <div class="spa-4">
                    <div class="spa-12">
                        <div class="box" style="background: #323A45;">
                            <div class="box-header">
                                <h3 class="text-blanco"><i class="fa fa-pie-chart"></i> Estados de Tomas 
                                    <div class="cant_cli" style="position: absolute; top: 10px; right: 10px;"><span id="tot_cli"></span> <i class="fa fa-list-ul"></i></div></h3>
                            </div>
                            <div class="box-body">
                                <div id="pieAdmin" class="des_box" style="height:220px;"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spa-8">
                    <div class="detAud">
                    </div>
                </div>
            </div>

            <div class="row desPanelDet">
                <button type="button" title="Regresar" class="btn btn-danger retPaini" style="position: absolute; right: 15px;border-radius: 100px;font-size: 18px;top: 55px;height: 35px;width: 35px;padding: 0px;z-index: 1;"><i class="fa fa-arrow-left"></i></button>
                <div class="box box-primary">
                    <div class="box-body">
                        <section class="col-xs-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_pa_1" data-toggle="tab">ART&Iacute;CULO</a></li>
                                <li><a href="#tab_pa_2" data-toggle="tab">AVERIADOS</a></li>
                                <li><a href="#tab_pa_3" data-toggle="tab">RESUMEN</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_pa_1"></div>
                                <div class="tab-pane" id="tab_pa_2"></div>
                                <div class="tab-pane" id="tab_pa_3">
                                    <div class="col-xs-6">
                                        <h4>Inventario</h4>
                                        <table class="table table-bordered TFtable" style="margin-bottom:0;">
                                            <tbody>
                                                <tr>
                                                    <td>TOTAL POBLACION PVP</td>
                                                    <td id="va_tot_pvp" class="text-right eli_tab"></td>
                                                </tr>
                                                <tr>
                                                    <td>TOTAL POBLACION COSTO</td>
                                                    <td id="va_tot_cos" class="text-right eli_tab"></td>
                                                </tr>
                                                <tr>
                                                    <td>TOTAL DIFERENCIA A PVP</td>
                                                    <td id="va_tot_dif_pvp" class="text-right eli_tab"></td>
                                                </tr>
                                                <tr>
                                                    <td>TOTAL DIFERENCIA A COSTO</td>
                                                    <td id="va_tot_dif_cos" class="text-right eli_tab"></td>
                                                </tr>
                                                <tr>
                                                    <td>TOTAL ITEMS</td>
                                                    <td id="countif" class="text-right eli_tab"></td>
                                                </tr>
                                                <tr>
                                                    <td>TOTAL UNIDADES NETO</td>
                                                    <td id="ubica_2" class="text-right eli_tab"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4>Averiados</h4>
                                        <table class="table table-bordered TFtable" style="margin-bottom:0;">
                                            <tbody>
                                                <tr>
                                                    <td>TOTAL PVP</td>
                                                    <td id="ave_va_tot_pvp" class="text-right eli_tab"></td>
                                                </tr>
                                                <tr>
                                                    <td>TOTAL COSTO</td>
                                                    <td id="ave_va_tot_cos" class="text-right eli_tab"></td>
                                                </tr>
                                                <tr>
                                                    <td>TOTAL ITEMS</td>
                                                    <td id="ave_countif" class="text-right eli_tab"></td>
                                                </tr>
                                                <tr>
                                                    <td>TOTAL UNIDADES NETO</td>
                                                    <td id="ave_ubica_2" class="text-right eli_tab"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab_pa_3_btn"></div>
                                </div>
                            </div>
                        </section>
                        <div class="clear"></div>
                    </div>
                </div>


            </div>
        </section>
    </div>
</div>


<div class="modal" id="modalDetArt">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('DetArt', '');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="nomCentr"></span> - Toma #<span id="nomToma"></span> - <span id="desTipo"></span></h3>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h4 class="text-center" style="margin: 0;"><span id="co_art"></span> - <span id="ds_art"></span></h4>
                    </div>
                    <div class="col-md-12">
                        <div id="cont_lis_det"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modalDetallToma">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" onclick="quitarmodalGeneral('DetallToma', '');" class="close">
                    <i class="fa fa-times-circle"></i>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title text-blanco"><span id="titleTelf"></span></h3>
            </div>
            <div class="modal-body">
                <div class="spa-12">
                    <div class="spa-12">
                        <table id="tabAud" class="table table-bordered"></table>
                    </div>
                    <div class="clearfix"></div>
                </div>  
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modalGeInforme">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('GeInforme', '');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Generar Informe</h3>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-xs-3">
                    <label>Seleccione informe:</label>
                    <select class="form-control" id="cmb_tip">
                        <option value="1">Administrador</option>
                        <option value="2">Auditor</option>
                    </select>                    
                </div>
                <div class="col-xs-9">
                    <label>Email:</label>
                    <input id="txt_mail" type="text" class="form-control">
                </div>
                <div class="col-xs-12">
                    <label>Contenido:</label>
                    <textarea id="cont_mail" class="form-control"></textarea>
                    <input id="c_b" type="hidden">
                    <input id="c_l" type="hidden">
                </div>
                <div class="col-xs-12"> 
                    <button type="button" class="btn btn-success pull-right ge_inform" style="margin:10px 0px;"><i class="fa fa-send"></i> Enviar</button>
                </div>
            </div>
        </div>
    </div>
</div>