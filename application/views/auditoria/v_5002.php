<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <h3 class="text-blanco"><i class="fa fa-cogs"></i> Procesar Tomas</h3>
                    </div>
                </div>
                <div class="row desPanel">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table id="tabContr" class="table table-bordered TFtable">
                                <?php if (empty($retPendientes['err'])) { ?>
                                    <thead>
                                        <tr>
                                            <th class="text-center">Bodega</th>
                                            <th class="text-right">Listado</th>
                                            <th class="text-left">Descripci&oacute;n</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Tipo</th>
                                            <th class="text-right">Items</th>
                                            <th class="text-center">Acci&oacute;n</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($retPendientes['data'])) {
                                            foreach ($retPendientes['data'] as $row) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row['no_bdg']; ?></td>
                                                    <td><?php echo $row['co_lis']; ?></td>
                                                    <td><?php echo $row['tx_des']; ?></td>
                                                    <td><?php echo $row['fe_gen']; ?></td>
                                                    <td><?php echo $row['co_tip']; ?></td>
                                                    <td class="text-right"><?php echo $row['ca_ite']; ?></td>
                                                    <td class="text-center"><?php echo $row['html']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                    <?php
                                } else {
                                    ?>
                                    <thead>
                                        <tr><th class="text-center">ERROR </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7"><?php echo $retPendientes['err']; ?></td>
                                        </tr>
                                    </tbody>
                                    <?php
                                }
                                ?>
                            </table>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="row desPanelDet">
                    <button type="button" title="Regresar" class="btn btn-danger retPaini" style="position: absolute; right: 15px;border-radius: 100px;font-size: 18px;top: 55px;height: 35px;width: 35px;padding: 0px;z-index: 1;"><i class="fa fa-arrow-left"></i></button>
                    <div class="box box-primary">
                        <div class="box-body">

                            <section class="col-xs-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_pa_1" data-toggle="tab">ART&Iacute;CULO</a></li>
                                    <li><a href="#tab_pa_2" data-toggle="tab">AVERIADOS</a></li>
                                    <li><a href="#tab_pa_3" data-toggle="tab">RESUMEN</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_pa_1"></div>
                                    <div class="tab-pane" id="tab_pa_2"></div>
                                    <div class="tab-pane" id="tab_pa_3">
                                        <div class="col-xs-6">
                                            <h4>Inventario</h4>
                                            <table class="table table-bordered TFtable" style="margin-bottom:0;">
                                                <tbody>
                                                    <tr>
                                                        <td>TOTAL POBLACION PVP</td>
                                                        <td id="va_tot_pvp" class="text-right eli_tab"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL POBLACION COSTO</td>
                                                        <td id="va_tot_cos" class="text-right eli_tab"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL DIFERENCIA A PVP</td>
                                                        <td id="va_tot_dif_pvp" class="text-right eli_tab"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL DIFERENCIA A COSTO</td>
                                                        <td id="va_tot_dif_cos" class="text-right eli_tab"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL ITEMS</td>
                                                        <td id="countif" class="text-right eli_tab"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL UNIDADES NETO</td>
                                                        <td id="ubica_2" class="text-right eli_tab"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-xs-6">
                                            <h4>Averiados</h4>
                                            <table class="table table-bordered TFtable" style="margin-bottom:0;">
                                                <tbody>
                                                    <tr>
                                                        <td>TOTAL PVP</td>
                                                        <td id="ave_va_tot_pvp" class="text-right eli_tab"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL COSTO</td>
                                                        <td id="ave_va_tot_cos" class="text-right eli_tab"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL ITEMS</td>
                                                        <td id="ave_countif" class="text-right eli_tab"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TOTAL UNIDADES NETO</td>
                                                        <td id="ave_ubica_2" class="text-right eli_tab"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalGeInforme">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('GeInforme', '');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Generar Informe</h3>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-xs-3">
                    <label>Seleccione informe:</label>
                    <select class="form-control" id="cmb_tip">
                        <option value="1">Administrador</option>
                        <option value="2">Auditor</option>
                    </select>                    
                </div>
                <div class="col-xs-9">
                    <label>Email:</label>
                    <input id="txt_mail" type="text" class="form-control">
                </div>
                <div class="col-xs-12">
                    <label>Contenido:</label>
                    <textarea id="cont_mail" class="form-control"></textarea>
                    <input id="c_b" type="hidden">
                    <input id="c_l" type="hidden">
                </div>
                <div class="col-xs-12"> 
                    <button type="button" class="btn btn-success pull-right ge_inform" style="margin:10px 0px;"><i class="fa fa-send"></i> Enviar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#tabContr').DataTable();
</script>