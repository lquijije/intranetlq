
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                <div class="row" style="margin-bottom: 10px;">
                    <h3 class="text-blanco"><i class="fa fa-search"></i> Consulta de Tomas</h3>
                </div>
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="col-xs-3">
                                <label>Almac&eacute;n</label>
                                <?php echo $cmb_almacen; ?>
                            </div>
                            <div class="col-xs-3">
                                <label>Fecha Inicio</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="txt_fec_ini" value="<?php echo $fecha_inicial; ?>" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control" type="text">
                                </div>
                                <button type="button" class="btn btn_oculto btn-danger bg-red seTom" style="height: 33px;"><i class="fa fa-search"></i></button>
                            </div>
                            <div class="col-xs-3">
                                <label>Fecha Final</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="txt_fec_fin" value="<?php echo $fecha_final; ?>" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control" type="text">
                                    <div class="input-group-addon" style="padding:0;">
                                        <button type="button" class="btn btn_oculto btn-danger bg-red seTom" style="height: 33px;"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">                      
                            <div id="detalleTomas" class="row" style="padding-left: 17px; padding-right: 17px;">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<div class="modal" id="modalGeInforme">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('GeInforme', '');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Generar Informe</h3>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-xs-3">
                    <label>Seleccione informe:</label>
                    <select class="form-control" id="cmb_tip">
                        <option value="1">Administrador</option>
                    </select>                    
                </div>
                <div class="col-xs-9">
                    <label>Email:</label>
                    <input id="txt_mail" type="text" class="form-control">
                </div>
                <div class="col-xs-12">
                    <label>Contenido:</label>
                    <textarea id="cont_mail" class="form-control"></textarea>
                    <input id="c_b" type="hidden">
                    <input id="c_l" type="hidden">
                </div>
                <div class="col-xs-12"> 
                    <button type="button" class="btn btn-success pull-right ge_inform" style="margin:10px 0px;"><i class="fa fa-send"></i> Enviar</button>
                </div>
            </div>
        </div>
    </div>
</div>