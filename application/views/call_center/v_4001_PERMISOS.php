
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <section class="content invoice">
            <div class="row">
                <div class="spa-12" style="margin-bottom: 10px;">
                    <h3 class="text-blanco"><i class="fa fa-user"></i> Agente</h3>
                </div>
            </div>
            <div class="row">
                <div class="box box-primary bg-red-gradient">
                    <div class="box-body">
                        <div class="row">
                            <div class="error-content">
                                <h3 class="text-center"><i class="fa fa-warning"></i> USTED NO TIENE PERMISOS PARA ESTA OPCI&Oacute;N COMUNIQUESE CON SISTEMAS.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
