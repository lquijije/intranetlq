<style>
    .img-circle {
        border: 5px solid #85C5C2;
        margin-top: 10px;
    }

    .box {
        border-radius: 3px;
        background: #DDD;
    }

    .box .box-footer {
        background: #323A45;
    }

    .color_age {
        color: #85C5C2;
    }

    h5, h6 {
        margin-top: 2px;
        margin-bottom: 0px;
    }

    .btn_abs {
        position: absolute;
        right: 9px;
    }

    .box .box-body {
        min-height: 166px;
    }

    #pieAdmin  .legend > table {
        background: #DDD !important;
    }

    .legendLabel{
        text-align: left;
    }

    #reportrange {
        cursor: pointer;
        margin-right: 50px;
        font-size: 21px;
        color: #FFF;
    }

    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }

    .tabla_fija table {
        border-collapse: collapse;
        width: auto;
        -webkit-overflow-scrolling: touch;
        -webkit-border-horizontal-spacing: 0;
    }

    .tabla_fija th,
    .tabla_fija td {
        word-wrap: break-word;
        word-break: break-all;
        -webkit-hyphens: auto;
        hyphens: auto;
    }

    .tabla_fija thead tr {
        display: block;
        position: relative;
    }

    .tabla_fija tbody {
        display: block;
        overflow: auto;
        height: 157px;
    }

    .excel_btn {
        right: 15px !important;
        top: 5px !important;
    }

    .cant_cli {
        position: absolute;
        right: 14px;
        top: 10px;
        font-size: 22px;
        color: rgb(255, 255, 255);
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <section class="content invoice">

            <div class="row">
                <div class="col-xs-6" style="margin-bottom: 10px;">
                    <h3 class="text-blanco"><i class="fa fa-users"></i> Administrar Agentes</h3>
                </div>
                <div class="col-xs-6">
                    <div class="pull-right" id="reportrange">
                        <i class="fa fa-calendar"></i> 
                        <span>
                            <?php
                            $fec_ini = date('Y-m-d');
                            $fec_fin = date('Y-m-d');
                            echo conveMes(date('n', strtotime($fec_ini))) . " " . date("j, Y", strtotime($fec_ini));
                            ?> - <?php echo conveMes(date('n')) . " " . date("j, Y");
                            ?>
                        </span> 
                        <b class="caret"></b>
                        <?php
                        $data = array(
                            'f_i' => $fec_ini,
                            'f_f' => $fec_fin
                        );

                        echo form_hidden($data);
                        ?>
                    </div>
                    <a href="#" class="excel_btn" target="_blank"></a>
                    <!--<button type="button" class="btn-out ref_pag" title="Refrescar" style="margin:0;"><i class="fa fa-refresh"></i></button>-->
                </div>
            </div>

            <div class="row">
                <div class="spa-4">
                    <div class="spa-12">
                        <div class="box" style="background: #323A45;">
                            <div class="box-header">
                                <h3 class="text-blanco"><i class="fa fa-pie-chart"></i> Estadistica</h3>
                                <div class="cant_cli"><i class="fa fa-users"></i> <span id="tot_cli"></span></div>
                            </div>
                            <div class="box-body">
                                <div id="pieAdmin" class="des_box" style="height:220px;"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="spa-12">
                        <div class="box">
                            <div class="box-header">
                                <h3><i class="fa fa-list-ol"></i> Fechas de Carga</h3>
                            </div>
                            <div class="box-body">
                                <?php
                                if (!empty($fe_cargas)) {
                                    ?>
                                    <div class="col-xs-12">
                                        <div class="col-xs-3">
                                            <i class="fa fa-calendar"></i> Fechas
                                        </div>
                                        <div class="col-xs-3">
                                            <i class="fa fa-users"></i> Total
                                        </div>
                                        <div class="col-xs-3">
                                            <i class="fa fa-user"></i> Asignado
                                        </div>
                                        <div class="col-xs-3">
                                            <i class="fa fa-exclamation"></i> Pendientes
                                        </div>
                                        <div style="height: 150px; width: 100%; overflow-y: auto; background: #FFF;">
                                            <table class="table table-bordered TFtable">
                                                <tbody>
                                                    <?php
                                                    foreach ($fe_cargas as $row) {
                                                        ?>
                                                        <tr>
                                                            <td width="25%"><?php echo $row['fe_car']; ?></td>
                                                            <td><?php echo $row['to_car']; ?></td>
                                                            <td><?php echo $row['as_car']; ?></td>
                                                            <td><?php echo $row['pe_car']; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <?php
                                }
                                ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spa-8">
                    <div class="detUsu">
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal" id="modalUsers">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-orange">
                <button type="button" onclick="quitarmodalGeneral('Users', '');" class="close">
                    <i class="fa fa-times-circle"></i>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title text-blanco"><span id="titleUser"></span></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="est_usu" class="col-xs-4">
                    </div>
                    <div class="col-xs-8">
                        <?php
                        if (!empty($usuarios)) {
                            ?>
                            <div class="col-xs-12">
                                <table class="tabla_fija table table-bordered TFtable">
                                    <tbody>
                                        <?php
                                        foreach ($usuarios as $row) {
                                            ?>
                                            <tr id="row_<?php echo trim($row['co_usu']); ?>">
                                                <td style="width:95%;"><?php echo $row['ap_usu'] . " " . $row['no_usu']; ?></td>
                                                <td style="width:5%;" class="text-center">
                                                    <input type="radio" id="cu_<?php echo trim($row['co_usu']); ?>" name="radio_usu[]" value="<?php echo trim($row['co_usu']); ?>">
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <button class="btn btn-danger pull-right sav_c" type="button" style="height: 33px;"><i class="fa fa-send"></i> Asignar</button>
                            </div>
                            <?php
                        } else {
                            ?>
                            <h3>Sin usuarios que mostrar.</h3>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>