<style>
    
    .legendLabel{
        color: #FFF !important;
    }
    .box {
        padding: 12px;
    }
    .btn.btn-circle {
        width: 25px;
        height: 25px;
        line-height: 25px;
    }
    .label-danger {
        position:relative;
        right: 0;
        border-radius: .25em;
    }
    .btn_era{
        position: absolute;
        right: 5px;
        top: 15px;
    }
    .switchery-small {
        position: absolute;
        right: 2px;
        top: 16px;
    }
    .btn_dos {
        position: absolute;
        right: 35px;
        top: 16px;
    }
    
    .botones {
/*        display: block;
        position: absolute;*/
        bottom: 8px;
        width: 99%;
    }
    
    .form-select {
        border-width: 0px 0px 3px;
        border-style: none none solid;
        border-color: #636363;
        /*text-align: center;*/
        font-size: 21px;
        height: 32px;
    }
    label {
        margin:0;
        font-weight: bold;
    }
    .modal-body {
        padding: 92px 20px;
    }
    .ti_tipo {
        margin-bottom: 10px;
        font-weight: bold;
        border-bottom: 1px solid rgb(204, 204, 204);
        padding-bottom: 5px;
        color: #9B3E3B;
    }
    .formWrap input {
        border-bottom: solid 1px #636363;   
    }
    
    .legend table {
        background: #323A45;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <section class="content invoice">
            <div class="row">
                <div class="spa-12" style="margin-bottom: 10px;">
                    <h3 class="text-blanco"><i class="fa fa-user"></i> Agente</h3>
                </div>
            </div>
            <div class="row">
                <div class="spa-4">
                    <div class="spa-12">
                        <div class="box" style="background: #323A45;">
                            <div class="box-header">
                                <h3 class="text-blanco"><i class="fa fa-pie-chart"></i> Estadistica</h3>
                            </div>
                            <div class="box-body">
                                <div id="pieAge" class="des_box" style="height:200px;"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="spa-12">
                        <div class="box">
                            <div class="box-header">
                                <h3><i class="fa fa-clock-o"></i> Tiempos Gesti&oacute;n</h3>
                            </div>
                            <div class="box-body">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td><b>Promedio:</b></td>
                                            <td class="text-right"><b class="des_box p1_"></b></td>
                                        </tr>
                                        <tr>
                                            <td><b>M&iacute;nima:</b></td>
                                            <td class="text-right"><b class="des_box p2_"></b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Maxima:</b></td>
                                            <td class="text-right"><b class="des_box p3_"></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="spa-12">
                        <div class="box">
                            <div class="box-header">
                                <h3><i class="fa fa-bar-chart"></i> Contacto por hora</h3>
                            </div>
                            <div class="box-body">
                                <div id="barAge" class="des_box" style="height:100px;"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spa-8">
                    <div class="spa-12">
                        <div class="box">
                            <div class="box-header">
                                <div class="spa-6">
                                    <h3><i class="fa fa-list"></i> Clientes</h3>
                                </div>
                                <div class="spa-6">
                                    <select id="cmb_est" name="cmb_est" class="form-control fontawesome-select"></select>
                                </div>
                            </div>
                            <div class="box-body">
                                <table id="tabCli" class="table table-bordered"></table>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal" id="modalDetallTelf">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" onclick="quitarmodalGeneral('DetallTelf','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title text-blanco"><span id="titleTelf"></span></h3>
        </div>
        <div class="modal-body">
            
            <div class="row pull-right oc_reg">
                <span class="sin_acc"><button type="button" class="btn btn-circle btn-success new_t"><i class="text-blanco fa fa-plus"></i></button> Agregar tel&eacute;fono</span>
            </div>
            
            <?php 
                echo form_open('call_center/co_4001',array('id'=>'form_new_phone','name'=>'form_new_phone'));

                $data = array(
                    'co_cli' =>'',
                    'fe_car' =>'',
                    'co_cam' =>'',
                    'co_ci' =>''
                );

                echo form_hidden($data);
            ?>
                <div class="row new_reg sin_acc" style="margin-bottom: 5px;">
                    
                    <div class="col-xs-2">
                        <label>Tipo Reg.</label>
                        <select id="cmb_tg" class="form-control form-select">
                            <option value="CL">CL</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label>Tipo</label>
                        <?php echo $cmb_tip; ?>
                    </div>
                    <div class="col-xs-2">
                        <label>Area</label>
                        <select id="cmb_area_te" class="form-control form-select">
                            <option value=""></option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="09">09</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <div class="formWrap">
                            <div class="group">
                                <input type="text" id="nu_tel" name="nu_tel" maxlength="10" onkeypress="return isNumero(event,this);">
                                <label>Numero</label>
                                <div class="bar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="formWrap">
                            <div class="group">
                                <input type="text" id="ex_tel" name="ex_tel" maxlength="5" onkeypress="return isNumero(event,this);">
                                <label>Ext.</label>
                                <div class="bar"></div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-circle btn-danger clo_t" style="position: absolute;top: 0px;right: 0px;"><i class="text-blanco fa fa-close"></i></button>
                    </div>
                    <input type="submit" style="visibility: hidden;">
                </div>
            <?php 
                echo form_close(); 
            ?>

            <div class="row">
                <div class="col-xs-12">
                    <div id="detET">
                    </div>
                </div>
                <div class="botones sin_acc">
                    
                    <div class="col-xs-12">
                        <textarea placeholder="Comentarios.." class="form-control" id="txt_comen" name="txt_comen"></textarea>
                        <div class="col-xs-7" style="margin-top: 10px;">
                            <button class="btn btn-success con_cli" type="button"><i class="fa fa-check"></i> Contactado</button>
                            <button class="btn btn-danger incon_cli" type="button"><i class="fa fa-times"></i> Incontactado</button>
                        </div>
                        <div class="col-xs-5" style="margin-top: 10px;">
                            <div class="input-group">
                                <input id="txt_hora" name="txt_hora" class="form_hour form-control " value="" type="text" readonly="" placeholder="17:00" style="height: 31px;">
                                <div class="input-group-addon" style="padding:0;">
                                    <button class="btn btn-warning age_cli" type="button" style="height: 29px;"><i class="fa fa-clock-o"></i> Agendar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
  </div>
</div>