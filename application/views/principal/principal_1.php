<script>
    $('.dropdown-login').hide();

    $(document).ready( function() {
        $( "#activeDropdown" ).click(function() {
            $('.dropdown-login').show();
            $('#fadeLogin').show();
            
        });
        
        $('#fadeLogin').click(function() {
            $('.dropdown-login').hide();
            $('#fadeLogin').hide();
            $('#f_login')[0].reset();
        });
    });

</script>   
<style>
    a:hover {
        color:#CCC !important;
    }
</style>
    
<div class="containear">
    <div class="separador">
    <div class="demo-wrapper">
        <div class="dashboard clearfix">
            <ul class="tiles list-unstyled">
                
                <?php
                    if(isset($widget) && count($widget) > 0){
                        $a = 0;
                        ?>
                        <div class="col2 clearfix">
                        <?php
                        foreach ($widget AS $row){
                            $a++;
                            $this->load->view($row['vista']);
                            if ($a%3==0){
                                ?></div><?php
                            }
                        }
                        if($a<3){
                           ?></div><?php 
                        }
                        ?>
                        <?php

                    }
                ?>
                
                <div class="col1 clearfix" style="margin-left: 0;">
                    <li class="tile-3xbig"style="background: #FFF;">
                        <div class="list-group-header_pri tile-9">
                            <span class=" text-size-2"><i class="fa fa-newspaper-o"></i></span>
                            <span class=" text-size-2 titulo-1"> Notipycca</span>
                        </div>
                        <div class="list-group" style="overflow-y:auto;height:38.3em;margin-top: 54px;">
                            <?php 
                            if (isset($noticias) && !empty($noticias)){ 
                                foreach ($noticias as $row){ ?>
                                 <div class="bs-example list-group-item">
                                    <div class="media">
                                        <div class="media-body" style="white-space: normal;">
                                            <h3 class="media-heading titulo-1"><?php echo trim($row['ds_titulo']) ?></h3>
                                            <h5 class="media-heading text-gris"><i class="fa fa-clock-o"></i> <?php echo date('Y/m/d H:i',strtotime($row['fe_ingreso'])) ?></h5>
                                            <p><?php echo ($row['ds_descripcion']) ?></p>
                                        </div>
                                    </div>
                                </div>  
                            <?php }
                            } else {
                               //echo 'No hay NOTICIAS';
                            }
                            
                            $site = "temp/xml/noticias.xml";

                            if (file_exists($site)) {

                                $bandera = true;
                                $xml = simplexml_load_file($site);
                                $paso = json_encode($xml,true);
                                $array = json_decode($paso,true);
                                $arrayGe = null;
                                if (!empty($array)){
                                    foreach ($array['channel']['item'] as $row){ 
//                                        echo '<pre>';
//                                        print_r($row);    
//                                        echo '</pre>';
                                    ?>
                                    <div class="bs-example list-group-item">
                                            <div class="media">
                                                <div class="media-body" style="white-space: normal;">
                                                    <h3 class="media-heading titulo-1"><?php echo trim($row['title']) ?></h3>
                                                    <h5 class="media-heading text-gris"><i class="fa fa-clock-o"></i> <?php echo date('Y/m/d H:i',strtotime($row['pubDate'])) ?></h5>
                                                    <p><?php echo isset($row['description'])?trim($row['description']):''; ?></p>
                                                </div>
                                            </div>
                                        </div>  
                                    <?php
                                    }
                                }
                            }  
                            
                            ?>

                        </div>
                    </li>
                </div>
        
                <div class="col2 clearfix">
                    <li class="tile tile-big tile-5 slideTextUp" data-page-type="r-page" data-page-name="random-r-page">
                        
                        <section>
                            <p>
                                <span class=" text-size-1-5 center-block"><i class="fa fa-cogs"></i></span>
                                <span class="titulo-1"> Servicios</span>
                            </p>
                        </section>
                        
                        <section style="padding-top: 20px;">
                            <ul class="list-unstyled">
                                <li><a href="<?php echo site_url('publico/co_101') ?>" class="titulo-1 text-black"><i class="fa fa-chevron-right"></i> Consulta por Cedula</a></li>
                                <li><a href="<?php echo site_url('publico/co_102') ?>" class="titulo-1 text-black"><i class="fa fa-chevron-right"></i> Cumpleaños</a></li>
                                <li><a href="<?php echo site_url('publico/co_103') ?>" class="titulo-1 text-black"><i class="fa fa-chevron-right"></i> Consulta el Menú del día</a></li>
                                <li><a href="<?php echo site_url('publico/co_104') ?>" class="titulo-1 text-black"><i class="fa fa-chevron-right"></i> Consulta De Vehiculos</a></li>
                            </ul>
                        </section>
                        
                    </li>
                    
                    <li class="tile tile-big tile-6 slideTextLeft" data-page-type="r-page" data-page-name="random-r-page">
                        <section>
                            <p>
                                <span class="titulo-1"> Nuestros Productos</span>
                            </p>
                        </section>
                        
                        <section style="padding-top: 50px;">
                            <ul class="list-unstyled">
                                <li><a href="http://webpycca/g-catalogos/default.asp" target="_blank" class="titulo-1 text-blanco"><i class="fa fa-chevron-right"></i> Cat&aacute;logo</a></li>
                                <li><a href="<?php echo site_url('publico/co_105') ?>" class="titulo-1 text-blanco"><i class="fa fa-chevron-right"></i> Consulta de Productos</a></li>
                            </ul>
                        </section>
                    </li>
                    
                    <a href="http://webpycca/Procedimientos/inicio.htm" target="_blank">
                        <li class="tile tile-small tile tile-2 slideTextRight" data-page-type="s-page" data-page-name ="random-restored-page">
                            <section><p class="fa fa-users text-size-3"></p></section>
                            <section><p class="titulo-1"> Organizaci&oacute;n y Metodos</p></section>
                        </li>
                    </a>
                    
                    <a href="<?php echo site_url('procesos/reclamos/co_113')?>">
                        <li class="tile tile-small last tile-8 " data-page-type="s-page" data-page-name ="random-restored-page">
                           <section>
                                <p>
                                    <span class=" text-size-1-5 center-block"><i class="fa fa-book"></i></span>
                                    <span class="titulo-1"> Libro de Reclamos</span>
                                </p>
                            </section>
                        </li>
                    </a>
                    
                    <li class="tile tile-small tile-12 slideTextUp" data-page-type="s-page" data-page-name="random-restored-page">
                        <section>
                            <p>
                                <span class="titulo-1"> Facturación Electronica</span>
                            </p>
                        </section>
                        <section>
                            <figure>
                                <a href="http://130.1.80.70:8080/InvoicePycca/" target="_blank"><img src="<?php echo site_url()?>img/e-factura.jpg"/></a>
                            </figure>
                        </section>
                    </li>
                    
<!--                    <li style="white-space: normal;" class="tile tile-small last tile-11 slideTextRight" data-page-type="s-page" data-page-name="random-restored-page">
                    <section><p class="titulo-1"> Formato de Liquidación de Viajes Nacionales y del Exterior  </p></section>
                    <section><p class="titulo-1"> Formularios y solicitudes para trámites internos </p></section>
                    </li>-->

                     <?php
                    
                    if (isset($bandEvalua) && $bandEvalua){
                        ?>
                            <a href="<?php echo site_url('procesos/evaluaciones/co_1191')?>" target="_blank">
                                <li class="tile tile-small last tile-12 " data-page-type="s-page" data-page-name ="random-restored-page">
                                   <section>
                                        <p>
                                            <span class=" text-size-1-5 center-block"><i class="fa fa-list-ol"></i></span>
                                            <span class="titulo-1"> Evaluaci&oacute;n <br>de Desempeño</span>
                                        </p>
                                    </section>
                                </li>
                            </a>
                        <?php 
                    } else {}
                        ?>
                </div>
                
                <div class="col3 clearfix">
                    <li class="tile tile-big tile-11">
                        <section>
                            <div class="col-xs-12" style="padding:  10px 0px 0px 10px; width: 100%; text-align: left; font-weight: bold;">
                            Guayaquil
                            </div>
                            <div class="col-xs-4" style="margin: 10px 0px;">
                                <img src="<?php echo site_url('img/weather/4.png') ?>" style="width: 25%">
                            </div>
                            <div class="col-xs-8" style="font-size: 40px;font-weight: bold;">
                                <span>28</span>
                                <span style="font-size: 20px; top: 0px; position: absolute;">ºC</span>
                            </div>
                            <div class="col-xs-6" style="font-size: 13px;">
                                <span>Viento<br>6km/h</span>
                            </div>
                            <div class="col-xs-6" style="font-size: 13px;">
                                <span>Humedad<br>7%</span>
                            </div>
                        </section>
                    </li>

                    <li class="tile-2xbig"style="background: #FFF;">
                        <div class="list-group-header_pri tile-9">
                            <span class=" text-size-2"><i class="fa fa-download"></i></span>
                            <span class=" text-size-2 titulo-1"> Descargas</span>
                        </div>
                        <div class="list-group" style="overflow-y:auto;height:17.4em;margin-top: 54px;padding-top: 5px;">
                            <ul class="list-unstyled donwload">
                                <li><a href="<?php echo site_url('uploads/viaticos.xls'); ?>" target="_blank"><i class="fa fa-arrow-circle-down"></i> Formato de Liquidación de Viajes Nacionales y del Exterior</a></li>
                                <li><a href="<?php echo site_url('uploads/CATALOGO_MABE_2015.pdf'); ?>" target="_blank"><i class="fa fa-arrow-circle-down"></i> Catálogo MABE 2015</a></li>
                                <li><a href="<?php echo site_url('uploads/Colchones_ Chaide_Chaide_ 2015.pdf'); ?>" target="_blank"><i class="fa fa-arrow-circle-down"></i> Catálogo CHAIDE & CHAIDE 2015</a></li>
                            </ul>
                        </div>
                    </li>

                    <li class="tile tile-big tile-6" style="background: transparent;">
                        <section style="padding-top: 70px;padding-left: 20px;">
                            <h3 class="titulo-1"><i class="text-size-1-25 fa fa-map-marker" style="margin-right: 7px;"></i> Pycca S.A.</h3>
                            <h5>AV. 9 DE OCTUBRE 609 Y BOYACA - ESCOBEDO</h5>
                            <h5>Servicio de Atención Telefónica llamando al 1800 179222</h5>
                        </section>
                    </li>
                </div>
                
            </ul>
        </div>
    </div>
</div>
</div>
<script>
    loadPagePrinc();
</script>