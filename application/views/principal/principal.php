<script>
    $('.dropdown-login').hide();

    $(document).ready( function() {
        $( "#activeDropdown" ).click(function() {
            $('.dropdown-login').show();
            $('#fadeLogin').show();
            
        });
        
        $('#fadeLogin').click(function() {
            $('.dropdown-login').hide();
            $('#fadeLogin').hide();
            $('#f_login')[0].reset();
        });
    });

</script>   
<style>
.spa-4 {
    display: inline-block;
    float: none;
    margin-top: 0.1%;
    margin-left: 0.3%;
    margin-right: 0.3%;
    margin-bottom: 0.2%;
}
.tile_py {
    float: left;
    background: #ccc;
    margin: 0.5%;
    color: #FFF !important;
    padding: 5px;
    position: relative;
    overflow: hidden;
    font-size: 18px;
}
.tile_py h1,h2 {
    margin: 0;
    padding: 0;
    text-align: center !important;
}
.tile_py h1{
    margin-top: 20px;
    font-size: 50px;
}
.tile_py_1 {
    height: 140px;
    width: 49%;
}
.tile_py_2 {
    height: 140px;
    width: 99%;
}
.tile_py_3 {
    height: 285px;
    width: 99%;
}
.tile_py_4 {
    height: 575px;
    width: 99%;
}

.tile_py_1 .bloq,.tile_py_2 .bloq {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    text-align: center;
    display: table;
    -webkit-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
}
.tile_co_0 { background: transparent !important;}
.tile_co_1 { background: #1abc9c !important;}
.tile_co_2 { background: #2ecc71 !important;}
.tile_co_3 { background: #3498db !important;}
.tile_co_4 { background: #9b59b6 !important;}
.tile_co_4:hover { background: #FFF !important;}
.tile_co_5 { background: #34495e !important;}
.tile_co_6 { background: #16a085 !important;}
.tile_co_7 { background: #27ae60 !important;}
.tile_co_8 { background: #2980b9 !important;}
.tile_co_9 { background: #8e44ad !important;}
.tile_co_10 { background: #2c3e50 !important;}
.tile_co_11 { background: #f1c40f !important;}
.tile_co_12 { background: #e67e22 !important;}
.tile_co_13 { background: #e74c3c !important;}
.tile_co_14 { background: #ecf0f1 !important;}
.tile_co_15 { background: #95a5a6 !important;}
.tile_co_16 { background: #f39c12 !important;}
.tile_co_17 { background: #d35400 !important;}
.tile_co_18 { background: #c0392b !important;}
.tile_co_19 { background: #bdc3c7 !important;}
.tile_co_20 { background: #7f8c8d !important;}

.title_py_Up div:nth-child(2) {
  top: 100%
}

.title_py_Up:hover div {
  -webkit-transform: translateY(-100%);
  -ms-transform: translateY(-100%);
  -o-transform: translateY(-100%);
  transform: translateY(-100%)
}

.title_py_Down div:nth-child(2) {
  top: -120%
}

.title_py_Down:hover div {
  -webkit-transform: translateY(100%);
  -ms-transform: translateY(100%);
  -o-transform: translateY(100%);
  transform: translateY(100%)
}

.title_py_Left div:nth-child(2) {
  left: 100%
}

.title_py_Left:hover div {
  -webkit-transform: translateX(-100%);
  -ms-transform: translateX(-100%);
  -o-transform: translateX(-100%);
  transform: translateX(-100%)
}

.title_py_Right div:nth-child(2) {
  left: -100%
}

.title_py_Right:hover div {
  -webkit-transform: translateX(100%);
  -ms-transform: translateX(100%);
  -o-transform: translateX(100%);
  transform: translateX(100%)
}
.clima_ico {
    font-size: 58px;
    font-weight: bold;
    margin-top: 5px;
}
.clima_vie {
    font-size: 15px;
    margin-top: 20px;
}
.gr_cen {
    font-size: 20px; 
    top: 0px; 
    position: absolute;
}
.gr_nom{
    font-size: 13px;
    margin-top: -12px;
    text-align: center
}
.wi {
    margin-left: 15px;
}
.table > tbody > tr > td{
    vertical-align:top;
}
</style>
<div class="containear">
    <div class="demo-wrapper">
        <div class="dashboard clearfix">
            
            <?php
                if(isset($widget) && count($widget) > 0){
                    $a = 0;
                    ?>
                    <div class="spa-4">
                    <?php
                    foreach ($widget AS $row){
                        $a++;
                        $this->load->view($row['vista']);
                        if ($a%3==0){
                            ?></div><?php
                        }
                    }
                    if($a<3){
                       ?></div><?php 
                    }
                    ?>
                    <?php

                }
            ?>
            <div class="spa-4">
                <div class="tile_py tile_py_4 tile_co_8">
                    <div class="list-group-header_pri bloq">
                        <h2><i class="fa fa-newspaper-o"></i> Notipycca</h2>
                    </div>
                    <div class="list-group" style="overflow-y:auto;height:512px;margin-top:10px;color: rgb(85, 85, 85);text-align: justify;">
                        <?php 
                        if (isset($noticias) && !empty($noticias)){ 
                            foreach ($noticias as $row){ ?>
                            <div class="bs-example list-group-item">
                                <div class="media">
                                    <div class="media-body" style="white-space: normal;">
                                        <h3 class="media-heading titulo-1"><?php echo trim($row['ti_not']) ?></h3>
                                        <h5 class="media-heading text-gris"><i class="fa fa-clock-o"></i> <?php echo date('Y/m/d H:i',strtotime($row['fe_not'])) ?></h5>
                                        <p style="font-size: 10px !important;"><?php echo ($row['de_not']) ?></p>
                                    </div>
                                </div>
                            </div>  
                        <?php }
                        } 

                        $site = "temp/xml/noticias.xml";

                        if (file_exists($site)) {

                            $bandera = true;
                            $xml = simplexml_load_file($site);
                            $paso = json_encode($xml,true);
                            $array = json_decode($paso,true);
                            $arrayGe = null;
                            if (!empty($array)){
                                foreach ($array['channel']['item'] as $row){ 
                                ?>
                                <div class="bs-example list-group-item">
                                        <div class="media">
                                            <div class="media-body" style="white-space: normal;">
                                                <h3 class="media-heading titulo-1"><?php echo trim($row['title']) ?></h3>
                                                <h5 class="media-heading text-gris"><i class="fa fa-clock-o"></i> <?php echo date('Y/m/d H:i',strtotime($row['pubDate'])) ?></h5>
                                                <p style="font-size: 14px !important;"><?php echo isset($row['description'])?trim($row['description']):''; ?></p>
                                            </div>
                                        </div>
                                    </div>  
                                <?php
                                }
                            }
                        }  

                        ?>

                    </div>
                </div>
            </div>
            <div class="spa-4">
                <div class="tile_py tile_py_2 tile_co_4 title_py_Up">
                    <div class="bloq">
                        <h1><i class="fa fa-cogs"></i></h1>
                        <h2>Servicios</h2>
                    </div>
                    <div class="bloq" style="padding-top: 20px;">
                        <ul class="list-unstyled">
                            <li><a href="<?php echo site_url('publico/co_101') ?>" class="text-black"><i class="fa fa-chevron-right"></i> Consulta por C&eacute;dula</a></li>
                            <li><a href="<?php echo site_url('publico/co_102') ?>" class="text-black"><i class="fa fa-chevron-right"></i> Cumplea&ntilde;os</a></li>
                            <li><a href="<?php echo site_url('publico/co_103') ?>" class="text-black"><i class="fa fa-chevron-right"></i> Consulta el Men&uacute; del d&iacute;a</a></li>
                            <li><a href="<?php echo site_url('publico/co_104') ?>" class="text-black"><i class="fa fa-chevron-right"></i> Consulta De Vehiculos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="tile_py tile_py_2 tile_co_3 title_py_Left">
                    <div class="bloq">
                        <h2 style="margin-top: 50px;">Nuestros Productos</h2>
                    </div>
                    <div class="bloq" style="padding-top: 50px;">
                        <ul class="list-unstyled">
                            <li><a href="http://webpycca/g-catalogos/default.asp" target="_blank" class="titulo-1 text-blanco"><i class="fa fa-chevron-right"></i> Cat&aacute;logo</a></li>
                            <li><a href="<?php echo site_url('publico/co_105') ?>" class="titulo-1 text-blanco"><i class="fa fa-chevron-right"></i> Consulta de Productos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="tile_py tile_py_1 tile_co_12 title_py_Right">
                    <a href="http://webpycca/Procedimientos/inicio.htm" target="_blank">
                        <div class="bloq">
                            <h4 style="margin-top: 60px;">Organizaci&oacute;n y M&eacute;todos</h4>
                        </div>
                        <div class="bloq" style="padding-top: 20px;">
                            <h1><i class="fa fa-users"></i></h1>
                        </div>
                    </a>
                </div>
                <div class="tile_py tile_py_1 tile_co_11 title_py_Down">
                    <div class="bloq">
                        <h4 style="margin-top:60px;">Facturaci&oacute;n Electronica</h4>
                    </div>
                    <div class="bloq">
                        <a href="http://130.1.80.70:8080/InvoicePycca/" target="_blank">
                            <img style="width: 100% !important" src="<?php echo site_url()?>img/e-factura.jpg"/>
                        </a>
                    </div>
                </div>
                <div class="tile_py tile_py_1 tile_co_7">
                    <a href="<?php echo site_url('procesos/reclamos/co_113')?>">
                        <div class="bloq" style="margin-top:25px;">
                            <span class=" text-size-1-5 center-block"><i class="fa fa-book"></i></span>
                            <h4>Libro de Reclamos</h4>
                        </div>
                    </a>
                </div>
            </div>
            <div class="spa-4">
                <div class="tile_py tile_py_2 tile_co_5">
                    <div>
                        <!--<h4><?php echo conveDia(date('N')); ?> <?php echo date('d'); ?> de <?php echo conveMes(date('m')); ?>/<?php echo date('Y'); ?> </h4>-->
                        <?php 
//                        error_reporting(E_ALL);
//                        ini_set('error_reporting', E_ALL);
                        $ci = &get_instance();
                        $clima = $ci->general_model->retClima($_SERVER['REMOTE_ADDR']);
                        $datosParametro = null;
                        
                        if(empty($clima['err'])){
                            if (isset($clima['res_1']) && !empty($clima['res_1'])){                         
                                $cmb_clima = '<select name="cmb_clima_ge" id="cmb_clima_ge" class="form-control cmb_clima">';

                                if (!empty($clima)){ 
                                    foreach ($clima['res_2'] as $row){
                                        $selected = '';
                                        isset($clima['res_1']['mi_ciu']) && !empty($clima['res_1']['mi_ciu']) && trim($clima['res_1']['mi_ciu']) == trim($row['ds_ciu']) ? $selected = 'selected="selected"':'';
                                        $cmb_clima .= '<option value="'.trim($row['ds_ciu']).'" '.$selected.' data-label="'.htmlentities(json_encode($row)).'">'.trim($row['ds_ciu']).'</option>';
                                    }
                                } else {
                                    $cmb_clima .= '<option value="NNN">Sin datos</option>';
                                }

                                $cmb_clima .= '</select>';
                                
                                echo $cmb_clima;
                                ?>
                                    <div class="col-xs-6 clima_ico">
                                        <div class="spa-6">
                                            <i class="wi wi-day-cloudy"></i>
                                        </div>
                                        <div class="spa-6">
                                            <span><?php echo $clima['res_1']['co_tem'];?><span class="gr_cen">&#186;C</span></span>
                                        </div>
                                        <div class="spa-12 gr_nom"><?php echo $clima['res_1']['co_tex'];?></div>
                                    </div>
                                    <div class="col-xs-6 clima_vie">
                                        <div style="font-weight: bold;">Viento = <?php echo $clima['res_1']['ve_vie'];?> km/h</div>
                                        <div style="font-weight: bold;">Humedad = <?php echo $clima['res_1']['at_hum'];?>%</div>
                                    </div>
                                <?php 
                            } else {
                                ?>
                                    <h1><i class="fa fa-bolt"></i></h1>
                                    <h4>Inconvenientes al obtener Clima</h4>
                                <?php
                            }
                        } else {
                            ?>
                                <h1><i class="fa fa-bolt"></i></h1>
                                <h4 class="text-center">Inconvenientes al obtener Clima</h4>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="tile_py tile_py_3 tile_co_17">
                    <div class="list-group-header_pri">
                        <h2><i class="fa fa-download"></i> Descargas</h2>
                    </div>
                    <div class="list-group" style="overflow-y:auto;height:222px;margin-top:10px;color: rgb(85, 85, 85);text-align: justify; background: #FFF;">
                        <ul class="list-unstyled donwload">
                            <li><h4></h4><a href="<?php echo site_url('uploads/viaticos.xls'); ?>" target="_blank"><i class="fa fa-arrow-circle-down"></i> Formato de Liquidaci&oacute;n de Viajes Nacionales y del Exterior</a></li>
                            <li><a href="<?php echo site_url('uploads/CATALOGO_MABE_2015.pdf'); ?>" target="_blank"><i class="fa fa-arrow-circle-down"></i> Cat&aacute;logo MABE 2015</a></li>
                            <li><a href="<?php echo site_url('uploads/Colchones_ Chaide_Chaide_ 2015.pdf'); ?>" target="_blank"><i class="fa fa-arrow-circle-down"></i> Cat&aacute;logo CHAIDE & CHAIDE 2015</a></li>
                        </ul>
                    </div>
                </div>
                <div class="tile_py tile_py_2 tile_co_0">
                    <div style="padding-top: 50px;padding-left: 20px;text-align: left;">
                        <h3 class="titulo-1"><i class="text-size-1-25 fa fa-map-marker" style="margin-right: 7px;"></i> Pycca S.A.</h3>
                        <h5 style="margin-left: 35px;line-height: 8px;">AV. 9 DE OCTUBRE 609 Y BOYACA - ESCOBEDO</h5>
                        <h5 style="margin-left: 35px;line-height: 8px;">Servicio de Atenci&oacute;n Telef&oacute;nica llamando al 1800 179222</h5>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<script>
    loadPagePrinc();
</script>