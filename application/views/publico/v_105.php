
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                
                <div class="row" style="margin-bottom: 4px;">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-2 text-blanco"><i class="fa fa-search"></i> Consulta de Productos </span>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-2">
                                    <label>Descripción</label>
                                    <input id="txt_descr" name="txt_descr" type="text" class="form-control input-sm">
                                </div>

                                <div class="spa-1">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchInventar"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div id="contTable">
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalProdInvet">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-red">
                <button type="button" onclick="quitarmodalGeneral('ProdInvet', '');" class="close">
                    <span aria-hidden="true"><i class="fa fa-times-circle"></i></span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title"><span id="prodGeAl"></span></h3>
            </div>
            <div class="modal-body">
                <section id="DatProdInvet"></section>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>