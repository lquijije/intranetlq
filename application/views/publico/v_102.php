<script>
    $('#bodyPage').css('background','url(../img/cumpleanios.jpg) top left no-repeat');
    $('#bodyPage').css('background-attachment','fixed');
    $('#bodyPage').css('background-size','100%');
    $('.header').css('background','none');
</script>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="contect invoice8" style="top:50px;padding: 70px; position: absolute; height: 100%; left: 19%; opacity: 0.6; background: none repeat scroll 0% 0% rgb(34, 34, 34);"></section>
            <section class="content invoice8">
                <div class="row">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-2 text-blanco">Cumpleaños del mes </span>
                        <small class="text-size-1 text-gris">Consulta </small>
                    </div>
                </div>
                
                
                <div class="row" style="margin-top: 2px;">
                    <div class="main">
                        <?php 
                        if (isset($cumpleanios) && !empty($cumpleanios)){ 
                            $monthNames = Array();
                            $monthNames[1]= "Enero";
                            $monthNames[2]= "Febrero";
                            $monthNames[3]= "Marzo";
                            $monthNames[4]= "Abril";
                            $monthNames[5]= "Mayo";
                            $monthNames[6]= "Junio";
                            $monthNames[7]= "Julio";
                            $monthNames[8]= "Agosto";
                            $monthNames[9]= "Septiembre";
                            $monthNames[10]= "Octubre";
                            $monthNames[11]= "Noviembre";
                            $monthNames[12]= "Diciembre";

                            $DIas = Array();
                            $DIas[1]= "Lunes";
                            $DIas[2]= "Martes";
                            $DIas[3]= "Miercoles";
                            $DIas[4]= "Jueves";
                            $DIas[5]= "Viernes";
                            $DIas[6]= "Sabado";
                            $DIas[0]= "Domingo";

                            $saltoDia = '';
                            $saltoMes = '';
                            $i = 0;
                            foreach ($cumpleanios as $row){
                                if($saltoMes !== trim((int)$row['MES'])){ 
                                   if($i === 0){
                                        ?>
                                            </ul>
                                        <?php
                                   }
                                   ?>
                                    <ul class="cbp_tmtimeline">
                                    <li>
                                        <div class="cbp_tmicon1"><?php echo $monthNames[$row['MES']] ?></div>
                                    </li>  
                                    <?php  
                                }

                                if($saltoDia !== trim((int)$row['DIA'])){ 
                                    $a = trim((int)$row['MES'])."-".trim((int)$row['DIA']);
                                    ?> 
                                    <li>
                                        <time class="cbp_tmtime"><span></span> <span><?php echo $DIas[date('w', strtotime(date('Y')."-".$row['MES']."-".(int)$row['DIA']))] ?></span></time>
                                        <div class="cbp_tmicon"><?php echo (int)$row['DIA'] ?></div>
                                        <div class="cbp_tmlabel">
                                            <table class="table table-p">
                                                <thead>
                                                    <tr>
                                                        <th width="50%">Nombre</th>
                                                        <th width="50%">Centro De Costo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                    foreach ($cumpleanios as $list){
                                                        $b= trim((int)$list['MES'])."-".trim((int)$list['DIA']);
                                                        if($a == $b){                                                                             
                                                        ?>  
                                                            <tr>    
                                                                <td><?php echo trim(utf8_encode($list['NOMBRE'])); ?></td>
                                                                <td><?php echo trim(utf8_encode($list['CENTRO'])); ?></td>
                                                            </tr>
                                                        <?php 
                                                        }
                                                    }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </li>
                               <?php  }
                               $saltoDia = trim((int)$row['DIA']);
                               $saltoMes = trim((int)$row['MES']);
                            } 

                        } else {
                        }
                        ?>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>
