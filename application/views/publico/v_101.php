<div class="demo-wrapper">
    <div class="dashboard clearfix">
            <section class="content invoice4">
                
                <div class="row" style="margin-bottom: 10px;">
                    
                    <div class="pull-left">
                        <span class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Búsqueda de personas por cédula o nombre</span>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="spa-6">
                        
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-9">
                                    <label>Cédula </label>
                                    <input type="text" id="txt_ci" onkeypress="return isNumero(event,this)" name="txt_ci" class="form-control input-sm" value="">
                                </div>

                                <div class="spa-1">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchCI"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="spa-6">
                        <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-9">
                                    <label>Nombre </label>
                                    <input type="text" id="txt_name" name="txt_name" class="form-control input-sm uppercase" value="">
                                </div>

                                <div class="spa-1">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchName"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                            </div></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="spa-12">
                        
                    <div class="box box-primary">
                    <div class="box-body">                      
                        <div id="detallePersonas" class="row" style="padding-left: 17px; padding-right: 17px;">

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                    </div>
                </div>
                
            </section>
    </div>
</div>
