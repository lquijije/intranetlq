
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                <div class="row" style="margin-bottom: 4px;">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-2 text-blanco">Consulta de Vehículos </span>
                        <small class="text-size-1 text-gris">Consulta </small>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-2">
                                    <label>Tipo de Busqueda</label>
                                    <select id="tipo" class="input-sm form-control" onchange="changeTip()">
                                        <option value="P">Placa</option>
                                        <option value="I">Identificación</option>
                                    </select>
                                </div>
                                
                                <div class="spa-2">
                                    <label class="out-text">Busqueda</label>
                                    <input type="text" id="txt_search" name="txt_search" class="form-control input-sm" value="">
                                </div>

                                <div class="spa-1">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchAuto"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">                      
                        <div id="detalleAuto" class="row" style="padding-left: 17px; padding-right: 17px;">

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>
