<script>
    $('#bodyPage').css('background','url(../img/carnes.jpg) top left no-repeat');
    $('#bodyPage').css('background-attachment','fixed');
    $('#bodyPage').css('background-size','100%');
    $('.header').css('background','none');
</script>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-2"></div>
        <div class="spa-8" style="background: rgba(34, 34, 34,0.7); padding: 20px;">

            <div class="spa-12">

                <div class="pull-left">
                    <p style="font-family:ZombieTreats; font-size:50px; color:white;">MENU DE LA SEMANA</p>
                </div>

            </div>

            <div class="spa-12" style="margin-top: 2px;">
                    <div class="main">
                        <?php 
                        if (isset($comedor) && !empty($comedor)){
                            $monthNames = Array();
                            $monthNames[1]= "Enero";
                            $monthNames[2]= "Febrero";
                            $monthNames[3]= "Marzo";
                            $monthNames[4]= "Abril";
                            $monthNames[5]= "Mayo";
                            $monthNames[6]= "Junio";
                            $monthNames[7]= "Julio";
                            $monthNames[8]= "Agosto";
                            $monthNames[9]= "Septiembre";
                            $monthNames[10]= "Octubre";
                            $monthNames[11]= "Noviembre";
                            $monthNames[12]= "Diciembre";
                            $i=0;
                            $saltaMes ='';

                            foreach ($comedor as $row){
                            $a= (int)date('m', strtotime($row['fecha']));
                            $i++;

                            if($saltaMes !== (int)date('m', strtotime($row['fecha']))){
                                ?>
                                    <ul class="cbp_tmtimeline">
                                        <li>
                                            <div class="cbp_tmicon1"><?php echo $monthNames[(int)date('m', strtotime($row['fecha']))] ?></div>
                                        </li>
                                        <?php 

                                            foreach ($comedor as $row1){

                                                $b= (int)date('m', strtotime($row1['fecha']));
                                                if($a === $b){
                                                   ?>
                                                    <li>
                                                        <time class="cbp_tmtime"><span></span> <span><?php echo $row1['dia'] ?></span></time>
                                                        <div class="cbp_tmicon"><?php echo date('d', strtotime($row1['fecha'])) ?></div>
                                                        <div class="cbp_tmlabel">
                                                            <ul class="list-unstyled font-1">
                                                                <li>- (Normal 1) <?php echo utf8_encode(ucwords(strtolower($row1['texto_normal']))) ?></li>
                                                                <li>- (Normal 2) <?php echo utf8_encode(ucwords(strtolower($row1['texto_normal2']))) ?> </li>
                                                                <li>- (Dieta) <?php echo utf8_encode(ucwords(strtolower($row1['texto_dieta']))) ?></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                   <?php
                                                }
                                            }

                                        ?>

                                        </ul>    
                                <?php
                            }

                            $saltaMes = (int)date('m', strtotime($row['fecha']));
                            } 

                        } 
                        ?>
                    </div>
            </div>

        </div>
        
    </div>
</div>
