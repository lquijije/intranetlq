<script>
    $('.dropdown-login').hide();

    $(document).ready( function() {
        $( "#activeDropdown" ).click(function() {
            $('.dropdown-login').show();
            $('#fadeLogin').show();
            
        });
        
        $('#fadeLogin').click(function() {
            $('.dropdown-login').hide();
            $('#fadeLogin').hide();
            $('#f_login')[0].reset();
        });
    });

</script>   
<style>
    a:hover {
        color:#CCC !important;
    }
</style>

<div separador>
    <div class="demo-wrapper">
        <div class="dashboard clearfix">
            <ul class="tiles list-unstyled">
                
                <div class="col2 clearfix">
                    <li class="tile tile-big tile-5 slideTextUp" data-page-type="r-page" data-page-name="random-r-page">
                        
                        <section>
                            <p>
                                <span class=" text-size-1-5 center-block"><i class="fa fa-cogs"></i></span>
                                <span class="titulo-1"> Servicios</span>
                            </p>
                        </section>
                        
                        <section style="padding-top: 20px;">
                            <ul class="list-unstyled">
                                <li><a href="<?php echo site_url('publico/co_101') ?>" class="titulo-1 text-black"><i class="fa fa-chevron-right"></i> Consulta por Cedula</a></li>
                                <li><a href="<?php echo site_url('publico/co_102') ?>" class="titulo-1 text-black"><i class="fa fa-chevron-right"></i> Cumpleaños</a></li>
                                <li><a href="<?php echo site_url('publico/co_103') ?>" class="titulo-1 text-black"><i class="fa fa-chevron-right"></i> Consulta el Menú del día</a></li>
                                <li><a href="<?php echo site_url('publico/co_104') ?>" class="titulo-1 text-black"><i class="fa fa-chevron-right"></i> Consulta De Vehiculos</a></li>
                            </ul>
                        </section>
                    </li>
                    
                    <li class="tile tile-big tile-6 slideTextLeft" data-page-type="r-page" data-page-name="random-r-page">
                        <section>
                            <p>
                                <span class="titulo-1"> Nuestros Productos</span>
                            </p>
                        </section>
                        <section>
                            <figure>
                                <a href="#" target="_blank"><img src="<?php echo site_url()?>img/compra-empresarial.jpg"/></a>
                            </figure>
                        </section>
                    </li>
                    
                    <a href="http://webpycca/Procedimientos/inicio.html" target="_blank">
                        <li class="tile tile-small tile tile-2 slideTextRight" data-page-type="s-page" data-page-name ="random-restored-page">
                            <section><p class="fa fa-users text-size-3"></p></section>
                            <section><p class="titulo-1"> Organizaci&oacute;n y Metodos</p></section>
                        </li>
                    </a>
                    
                    <li class="tile tile-small last tile-8 rotate3d rotate3dY" data-page-type="r-page" data-page-name="random-r-page">
                        <section class="faces">
                            <section class="front"><span class="fa fa-download"></span></section>
                            <section class="back"><p class="titulo-1">Descargas</p></section>
                        </section>
                    </li>
                    
                    <li class="tile tile-small tile-12 slideTextUp" data-page-type="s-page" data-page-name="random-restored-page">
                        <section>
                            <p>
                                <span class="titulo-1"> Facturación Electronica</span>
                            </p>
                        </section>
                        <section>
                            <figure>
                                <a href="http://130.1.80.70:8080/InvoicePycca/" target="_blank"><img src="<?php echo site_url()?>img/e-factura.jpg"/></a>
                            </figure>
                        </section>
                    </li>
                    
                    <a href="<?php echo site_url('uploads/viaticos.xls') ?>">
                        <li style="white-space: normal;" class="tile tile-small last tile-11 slideTextRight" data-page-type="s-page" data-page-name="random-restored-page">
                        <section><p class="titulo-1"> Formato de Liquidación de Viajes Nacionales y del Exterior  </p></section>
                        <section><p class="titulo-1"> Formularios y solicitudes para trámites internos </p></section>
                        </li>
                    </a>
                </div>
                
                <div class="col2 clearfix">
                    <li class="tile-3xbig" style="background: #FFF;">
                        <div>
                        <div class="list-group">
                            
                            <div class="list-group-item active">
                                <span class=" text-size-2"><i class="fa fa-newspaper-o"></i></span>
                                <span class=" text-size-2 titulo-1"> Notipycca</span>
                            </div>
                            
                            <?php 
                            if (isset($noticias) && !empty($noticias)){
                                foreach ($noticias as $row){ ?>
                                <div class="bs-example list-group-item">
                                <div class="media">
                                    <div class="media-body" style="white-space: normal;">
                                        <h3 class="media-heading titulo-1"><?php echo trim($row['ti_not']) ?></h3>
                                        <h5 class="media-heading text-gris"><i class="fa fa-clock-o"></i> <?php echo date('Y/m/d H:i',strtotime($row['fe_not'])) ?></h5>
                                        <p style="font-size: 10px !important;"><?php echo ($row['de_not']) ?></p>
                                    </div>
                                </div>
                            </div>  
                            <?php }
                            } ?>
                            
                        </div>
                            
                        </div>
                    </li>
                </div>
                
                <div class="col3 clearfix">      
                    <li class="tile tile-big">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="<?php echo site_url('img/slide1.png')?>" alt="">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="<?php echo site_url('img/slide1.png')?>" alt="">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left fa fa-arrow-left" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>

                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <i class="glyphicon glyphicon-chevron-left fa fa-arrow-right" aria-hidden="true"></i>
                              <span class="sr-only">Next</span>
                            </a>

                          </div>
                    </li>
                    <li class="tile tile-2xbig  fig-tile" data-page-type="custom-page" data-page-name="random-r-page" style="background: transparent;">
                       
                    </li>
                    <li class="tile tile-big tile-6" style="background: transparent;">
                        <div style="padding-top: 70px;padding-left: 20px;">
                            <h3 class="titulo-1"><i class="text-size-1-25 fa fa-map-marker" style="margin-right: 7px;"></i> Pycca S.A.</h3>
                            <h5>Boyaca y Avenida 9 de Octubre 609</h5>
                            <h5>Servicio de Atención Telefónica llamando al 1800 179222</h5>
                        </div>
                    </li>
                </div>

            </ul>
        </div>
    </div>
</div>