<?php

if($this->session->flashdata('message')){
    $message = $this->session->flashdata('message');
}

if($this->session->flashdata('error')){
    $error = $this->session->flashdata('error');
}

if(function_exists('validation_errors') && validation_errors() != ''){
    $error = validation_errors();
} 

?>

<?php if (!empty($message)): ?>
<script type="text/javascript">
    $(document).ready(function() {
        errorGeneral('Sistema Intranet',"<?php echo quitar_saltos($message);?>",'danger');
    });
</script>
<?php endif; ?>

<?php if (!empty($error)): ?>
<script type="text/javascript">
    $(document).ready(function() {
        var a = "<?php echo quitar_saltos(trim($error));?>";
        errorGeneral('Sistema Intranet',a,'danger');
    });
</script>
<?php endif; ?>
 
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice15">
                
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-cogs"></i> Actualizaci&oacute;n de Datos</div>
                    </div>
                </div>
                
                <div class="row" style="margin-top: 10px;">
                    <?php $attributes = array('class' => 'form-horizontal', 'id' => 'f_login','name' => 'f_login');   
                    echo form_open($form_pass,$attributes); ?>
                    
                    <div class="spa-12 box box-primary" style="padding:15px;">
                        
                        <div class="spa-12" style="margin-bottom: 10px;">
                            <label>Usuario</label>
                            <?php  $data = array('name' => 'txt_usuario','id'=> 'txt_usuario','class'=>'form-control','value'=> set_value('txt_usuario'),'maxlength'=>'25');
                            echo form_input($data); ?>
                        </div>

                        <div class="spa-12" style="margin-bottom: 10px;"> 
                            <label>Contraseña Actual</label>
                            <?php  $data = array('id'=>'txt_clave','name'=>'txt_clave','value'=>'','class'=>'form-control');
                              echo form_password($data);
                              unset($data); ?>     
                        </div>
                        
                        <?php if($band == false){ ?>
                            <div class="spa-12" style="margin-bottom: 10px;"> 
                                <label>Contraseña Nueva</label>
                                <?php  $data = array('id'=>'txt_nueva_clave','name'=>'txt_nueva_clave','value'=>'','class'=>'form-control');
                                  echo form_password($data);
                                  unset($data); ?>     
                            </div>
                        <?php } ?>
                                                
                        <?php if($band == false){ ?>
                            <div class="spa-12"> 
                                <label>Vuelve a escribir la Contraseña</label>
                                <?php  $data = array('id'=>'txt_rep_clave','name'=>'txt_rep_clave','value'=>'','class'=>'form-control');
                                  echo form_password($data);
                                  unset($data); ?>     
                            </div>
                        <?php } ?>
                        
                        <div class="spa-12" style="margin-bottom: 10px;">
                            <label>E-Mail</label>
                            <?php  $data = array('name' => 'txt_mail','id'=> 'txt_mail','class'=>'form-control','value'=>$_SESSION['e_mail']);
                            echo form_input($data); ?>
                        </div>

                        <div class="spa-12" style="margin-top: 10px;"> 
                            <div class="pull-right">
                                <button type="button" class="pull-right btn btn-primary btn-process save_pass_new"><i class="fa fa-save text-red"></i> Guardar</button>
                            </div>
                        </div>

                    </div>
                    
                    <?php echo form_close(); ?>
                </div>
                
                <div class="clearfix"></div>
                
            </section>
        </div>
    </div>
</div>
	
 