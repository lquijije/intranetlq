
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .dashboard {
        padding: 5em 3em;
    }
    .input-sm {
        height:25px;
    }
    .spa-3,.spa-4 {
        text-align: right;
        font-weight:bold;
    }
    .spa-1-5 {
        width: 10.8%;
    }
    .sta {
        color:#FFF;
        background: none repeat scroll 0% 0% #2E8BEF !important;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><!--<i class="fa fa-search"></i>--> Aprobaci&oacute;n de Orden de Compras Importaciones</div>
                    </div>
                </div>
                
                <div class="row" style="margin-top: 10px;">
                    <div class="spa-12">
                        <div class="box box-primary">
                        <div class="box-body">
                            <div id="table_solic">
                                <table id="table_solicitudes" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No. Orden Compra</th>
                                            <th>Cod. Proveedor</th>
                                            <th>Proveedor</th>
                                            <th>Observaci&oacute;n</th>
                                            <th>Valor FOB</th>
                                            <th># Items</th>
                                            <th>Fecha de Ingreso</th>
                                            <th>Cod. Fabricante</th>
                                            <th>Visualizar</th>
                                            <th><input type="radio" name="ck_apro" id="ck_apro" /> Aprobar</th>
                                            <th><input type="radio" name="ck_apro" id="ck_rech" /> Negar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php 
                                        $i = 0;
                                        if (isset($solicitudes) && !empty($solicitudes)){
                                            foreach($solicitudes as $row){
                                                $i++;
                                                ?>  
                                                <tr id="id_<?php echo trim($row['cod_ord']); ?>">
                                                    <td><?php echo $row['cod_ord']; ?></td>
                                                    <td><?php echo $row['cod_pro']; ?></td>
                                                    <td><?php echo $row['nom_pro']; ?></td>
                                                    <td><?php echo $row['des_obs']; ?></td>
                                                    <td class="derecha"><?php echo number_format($row['vol_fob'],2); ?></td>
                                                    <td class="centro"><?php echo $row['num_ite']; ?></td>
                                                    <td class="centro"><?php echo $row['fec_ing']; ?></td>
                                                    <td class="centro"><?php echo $row['cod_fab']; ?></td>
                                                    <td class="centro">
                                                        <button type="button" onclick="viewSolic(<?php echo trim($cod_empr); ?>,<?php echo trim($row['cod_ord']) ?>)" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button>
                                                    </td>
                                                    <td><center><input type="radio" name="ck_<?php echo $i; ?>" id="option_apro[]" value="<?php echo $row['cod_ord']; ?>"></center></td>
                                                    <td><center><input type="radio" name="ck_<?php echo $i; ?>" id="option_rech[]" value="<?php echo $row['cod_ord']; ?>"></center></td>
                                                </tr>
                                                <?php
                                            }
                                        } 
                                        
                                        ?>
                                                
                                    </tbody>
                                </table>
                                
                            </div>
                            <button type="button" class="btn btn-primary right-btn apr_soli"><i class="text-red fa fa-check"></i> Procesar</button>
                            <div class="clearfix"></div>
                        </div>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalDetallSolic">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header bg-orange">
            <button type="button" onclick="quitarmodalGeneral('DetallSolic','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="titleDoc"></span></h3>
        </div>
        <div class="modal-body">
            <div id="view_Soli_cab">
                
            </div>
            
            <div class="clearfix"></div>
        </div>
    </div>
  </div>
</div>

<input type="hidden" id="c_emp" value="<?php echo $cod_empr ?>">