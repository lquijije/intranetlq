<script>
//    $('#bodyPage').css('background','url(../../img/prueba.jpg) top left no-repeat');
//    $('#bodyPage').css('background-attachment','fixed');
//    $('#bodyPage').css('background-size','100%');
//    $('.header').css('background','none');
</script>
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #DDD;
        padding: 2px;
    }
    .red{
        color:#FF0000;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                
                <div class="spa-12">
                    <div class="spa-2"></div>
                    <div class="spa-8" style="text-align: center;margin-bottom:25px;"><span class="text-size-2 text-blanco">Evaluaciones de Desempeño</span></div>
                </div>
                
                <div class="spa-12">
                    <div class="spa-1"></div>
                    <div class="spa-10">
                    <div>
                        <div class="list-group-header bg-red">
                            <span class=" text-size-1-5 titulo-1"> <i class="fa fa-list-ol"></i> Mis Evaluaciones</span>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="box">
                        <div class="box-body">   
                            <table class="table table-bordered TFtable" id="tab_pregu">
                                <thead>
                                    <tr>
                                        <th>T&iacute;tulo</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Fin</th>
                                        <th>Estado</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(isset($evaluaciones) && !empty($evaluaciones)){
                                    foreach ($evaluaciones as $row){
                                        ?>
                                        <tr>
                                            <td><?php echo $row['ev_tit']; ?></td>
                                            <td class="centro"><?php echo $row['ev_fi']; ?></td>
                                            <td class="centro"><?php echo $row['ev_ff']; ?></td>
                                            <td class="centro">
                                                <span class="label <?php echo $row['et_cl']; ?>"><?php echo $row['ev_ed']; ?></span>
                                            </td>
                                            <td class="centro"><button type="button" class="btn btn-circle btn-warning" onclick="evaDes(<?php echo $row['ev_cod']; ?>,'<?php echo str_replace(array('"',"'"),array('',''),$row['ev_tit']); ?>','<?php echo $row['ev_es']; ?>');"><i class="text-blanco text-size-1 fa fa-external-link"></i></button></td>
                                        </tr>
                                        <?php 
                                    }
                                } else {
                                    ?>
                                        <tr>
                                            <td colspan="5" style="text-align:center;"> No tiene Evaluaciones Asignadas.</td>
                                        </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalEva">
  <div class="modal-dialog">
    <div style="margin: 5px; position: absolute; z-index: 9; right: 5px; top: 5px;">
        <button type="button" onclick="quitarmodalGeneral('Eva','');" class="close">
            <i class="fa fa-times-circle"></i>
        </button>
    </div>
    <div class="modal-content">
        <div class="modal-body">
            <div class="separador" style="padding-bottom: 15px;">
                <div id="pasEva" class="spa-12">
                    
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>
