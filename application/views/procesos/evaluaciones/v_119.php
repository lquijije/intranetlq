
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #DDD;
        padding: 2px;
    }
    .red{
        color:#FF0000;
    }
    
    .panel-row {
        background-image: -webkit-linear-gradient(top, #e8e8e8 0, #e8e8e8 100%);
        background-image: -o-linear-gradient(top, #e8e8e8 0, #e8e8e8 100%);
        background-image: linear-gradient(to bottom, #e8e8e8 0, #e8e8e8 100%);
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5', endColorstr='#ffe8e8e8', GradientType=0);
        border: 1px solid #DDD;
        padding: 10px;
        margin-bottom: 10px;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Evaluaciones</div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-2">
                                    <label>Fecha Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_ini" value="" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                        <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="">
                                    </div>
                                </div>
                                <div class="spa-2">
                                    <label>Fecha Final</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_fin" value="" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                        <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="">
                                    </div>
                                </div>
                                <div class="spa-1">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchDoc" onclick="javascript:window.open('http://intranet:99/ipycca/procesos/evaluaciones/co_119','_blank','width='+screen.width+',height='+screen.height+',top=0,left=0,toolbar=no,location=no,scrollbars=yes,resizable=no,status=no,menubar=no,directories=no' );"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                                <div class="spa-7">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-success pull-right new_eva"><i class="fa fa-plus"></i> Crear Evaluaci&oacute;n</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table id="table_tot_eva" class="table table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <th>Estado</th>
                                    <th>T&iacute;tulo</th>
                                    <th>Creación</th>
                                    <th>Vista previa</th>
                                    <th>Diseñar</th>
                                    <th>Configurar</th>
                                    <th>Recopilar</th>
                                    <th>Informes</th>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalEvaCre">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header bg-red">
            <button type="button" onclick="resetSolici();" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 id="newTitle" class="modal-title">Nueva Evaluaci&oacute;n</h3>
        </div>
        
        <?php 
            $attributes = array('id' => 'form_EvaCre','name'=>'form_EvaCre');
            $hidden=array('txt_accion'=>'',
                          'txt_co_estado'=>'',
                          'id_co_solic'=>'',
                          'id_dpto'=>'');   
            echo form_open('procesos/evaluaciones/co_119',$attributes,$hidden); 
        ?>   
        
        <div class="modal-body">
            
            <section>

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab" id="tab1_click">Evaluaci&oacute;n</a></li>
                    <li><a href="#tab2" data-toggle="tab">Preguntas</a></li>
                    <li><a href="#tab3" data-toggle="tab">Configurar</a></li>
                    <li><a href="#tab4" data-toggle="tab">Recopilar</a></li>
                    <li><a href="#tab5" data-toggle="tab">Informes</a></li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="tab1">
                        <div class="spa-6 panel-row">
                            <label>T&iacute;tulo</label>
                            <input name="txt_eva_tit_1" value="" id="txt_eva_tit_1" class="form-control" type="text">                        
                        </div>
                        <div class="spa-6 panel-row">
                            <label>Sub T&iacute;tulo</label>
                            <input name="txt_eva_tit_2" value="" id="txt_eva_tit_2" class="form-control" type="text">                        
                        </div>
                        <div class="spa-12 panel-row">
                            <label>Descripci&oacute;n</label>
                            <textarea name="txt_eva_des" value="" id="txt_eva_des" class="form-control"></textarea>
                        </div>
                        <div class="spa-3 panel-row">
                            <label>M&eacute;todo de Evaluaci&oacute;n</label>
                            <select class="form-control">
                                <option>-Seleccionar M&eacute;todo-</option>
                                <option>Desempe&ntilde;o</option>
                                <option>Sobre 10</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="tab-pane " id="tab2">
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="tab-pane " id="tab3">
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="tab-pane " id="tab4">
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="tab-pane " id="tab5">
                        <div class="clearfix"></div>
                    </div>

                <div class="clear"></div>
                </div>
            </section>
            
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>
