
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #DDD;
        padding: 2px;
    }
    .red{
        color:#FF0000;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-question-circle"></i> Preguntas</div>
                    </div>
                    <button type="button" class="btn btn-danger newPregu" style="position: absolute; right: 0;"><i class="fa fa-edit"></i> Nueva Pregunta</button>
                </div>
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <?php 
                                    $attributes = array('id' =>'form_seapre','name'=>'form_seapre');
                                    echo form_open('procesos/evaluaciones/co_1192',$attributes); 
                                ?>   

                                <div class="spa-3">
                                    <label>M&eacute;todo</label>
                                    <?php echo $cmb_met_sea; ?>
                                </div>

                                <?php echo form_close(); ?>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">   
                        <table class="table table-bordered" id="tab_pregu">
                            <thead>
                                <tr>
                                    <td>C&oacute;digo</td>
                                    <td>M&eacute;todo</td>
                                    <td>Pregunta</td>
                                    <td>Estado</td>
                                    <td>Modificar</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if(isset($preguntas) && !empty($preguntas)){
                                    foreach ($preguntas as $row){
                                        ?>
                                        <tr>
                                            <td><?php echo $row['co_pre']; ?></td>
                                            <td><?php echo $row['ds_met']; ?></td>
                                            <td><?php echo $row['ds_pre']; ?></td>
                                            <td class="centro"><?php echo $row['ds_est']; ?></td>
                                            <td class="centro"><button type="button" class="btn btn-circle btn-warning" onclick="viewPre(<?php echo $row['co_pre']; ?>,'U');"><i class="text-blanco text-size-1 fa fa-edit"></i></button></td>
                                        </tr>
                                        <?php 
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalNewPre">
    <div class="modal-dialog">
        
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('NewPre','form_NewPre');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="name_pro"></span>Nueva Pregunta</h3>
        </div>
        
        <div class="modal-content">
            
            <div class="modal-body">
                
                <?php 
                    $attributes = array('id' =>'form_NewPre','name'=>'form_NewPre');
                    echo form_open('procesos/evaluaciones/co_1192',$attributes); 

                    $data = array(
                        'co_acc' =>'',
                        'co_pre' =>''
                    );

                    echo form_hidden($data);
                ?>   

                <div class="spa-12">
                    <label>Pregunta</label>
                    <textarea id="txt_des_pre" class="form-control" style="height: 100px;"></textarea>
                </div>

                <div class="spa-9">
                    <label>M&eacute;todo</label>
                    <?php echo $cmb_met; ?>
                </div>

                <div class="spa-3">
                    <label>Estado</label>
                    <select id="cmb_est" class="form-control">
                        <option value="A">Activo</option>
                        <option value="I">Inactivo</option>
                    </select>
                </div>

                <div class="spa-12" style="margin-top: 15px;">
                    <button type="button" class="btn btn-primary savPre pull-right"><i class="text-red fa fa-save"></i> Guardar</button>
                </div>

                <div class="clearfix"></div>
                <?php echo form_close(); ?>

            </div>
            
        </div>
        
    </div>
</div>
