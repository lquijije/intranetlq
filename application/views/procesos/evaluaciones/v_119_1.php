
<style>
    .line {
        border:none;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Organigrama</div>
                    </div>
                </div>
            </section>
            <section class="content invoice">
                
                
                <div class="row">
                    <?php  
                        if(isset($orgaHTML) && !empty($orgaHTML)){
                            echo $orgaHTML; 
                        }  
                    ?> 
                    <div id="main"></div>
                </div>
                
            </section>
        </div>
    </div>
</div>
<!--<script>
        $(function() {
            $("#organisation").orgChart({container: $("#main"), interactive: true, fade: true, speed: 'slow'});
        });
        </script>-->