
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #DDD;
        padding: 2px;
    }
    .red{
        color:#FF0000;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-list-ol"></i> Evaluaciones</div>
                    </div>
                    <button type="button" class="btn btn-danger newPregu" style="position: absolute; right: 0;"><i class="fa fa-edit"></i> Nueva Evaluaci&oacute;n</button>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">   
                        <table class="table table-bordered TFtable" id="tab_pregu">
                            <thead>
                                <tr>
                                    <th>C&oacute;digo</th>
                                    <th>T&iacute;tulo</th>
                                    <th>Fecha Inicio</th>
                                    <th>Fecha Final</th>
                                    <th>Estado</th>
                                    <th>Modificar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(isset($evaluaciones) && !empty($evaluaciones)){
                                foreach ($evaluaciones as $row){
                                    ?>
                                    <tr>
                                        <td><?php echo $row['ev_cod']; ?></td>
                                        <td><?php echo $row['ev_tit']; ?></td>
                                        <td class="centro"><?php echo $row['ev_f_i']; ?></td>
                                        <td class="centro"><?php echo $row['ev_f_f']; ?></td>
                                        <td class="centro"><?php echo $row['ev_e_c']; ?></td>
                                        <td class="centro"><button type="button" class="btn btn-circle btn-warning" onclick="viewEva(<?php echo $row['ev_cod']; ?>,'U');"><i class="text-blanco text-size-1 fa fa-edit"></i></button></td>
                                    </tr>
                                    <?php 
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalNewEva">
    <div class="modal-dialog">
        
        <div class="modal-header bg-red">
            <button type="button" onclick="resetEval();" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="name_pro"></span>Nueva Evaluaci&oacute;n</h3>
        </div>
        
        <div class="modal-content">
            
            <div class="modal-body">
                
                <section>

                    <ul class="nav nav-tabs">
                        <li class="active" ><a href="#tab1" data-toggle="tab" id="tab1_click">Evaluaci&oacute;n</a></li>
                        <li><a href="#tab2" data-toggle="tab">Preguntas</a></li>
                        <li><a href="#tab3" data-toggle="tab">Mail</a></li>
                        <li><a href="#tab4" data-toggle="tab">Asignaci&oacute;n</a></li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab1">
                            <?php 
                                $attributes = array('id' =>'form_NewEva','name'=>'form_NewEva');
                                echo form_open('procesos/evaluaciones/co_1194',$attributes); 

                                $data = array(
                                    'co_acc' =>'',
                                    'co_eva' =>''
                                );

                                echo form_hidden($data);
                            ?>   

                                <div class="spa-6">
                                    <label>T&iacute;tulo</label>
                                    <input name="txt_eva_tit_1" value="" id="txt_eva_tit_1" class="form-control" type="text">                        
                                </div>
                            
                                <div class="spa-6">
                                    <label>Sub T&iacute;tulo</label>
                                    <input name="txt_eva_tit_2" value="" id="txt_eva_tit_2" class="form-control" type="text">                        
                                </div>
                            
                                <div class="spa-12">
                                    <label>Descripci&oacute;n</label>
                                    <textarea name="txt_eva_des" value="" id="txt_eva_des" class="form-control"></textarea>
                                </div>
                            
                                <div class="spa-2-5">
                                    <label>Fecha Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_ini" value="" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control" type="text">
                                        <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="">
                                    </div>
                                </div>
                            
                                <div class="spa-2-5">
                                    <label>Fecha Final</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_fin" value="" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control" type="text">
                                        <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="">
                                    </div>
                                </div>
                            
                                <div class="spa-2">
                                    <label>Periodo</label>
                                    <select id="cmb_per" class="form-control">
                                        <option value="1">Anual</option>
                                        <option value="2">Mensual</option>
                                    </select>
                                </div>
                            
                                <div class="spa-2">
                                    <label>Estado</label>
                                    <select id="cmb_est" class="form-control">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>
                                    </select>
                                </div>

                                <div class="spa-12" style="margin-top: 15px;">
                                    <button type="button" class="btn btn-primary savEva pull-right"><i class="text-red fa fa-save"></i> Guardar</button>
                                </div>

                            <div class="clearfix"></div>
                            <?php echo form_close(); ?>
                        </div>
                        
                        <div class="tab-pane" id="tab2">
                            <div class="spa-12">
                                
                            </div>  
                            
                            <div class="spa-6">
                                <label>M&eacute;todo</label>
                                <?php echo $cmb_met; ?>
                            </div>
                            <div class="spa-6">
                                <button type="button" class="btn btn-success seaPregu" style="margin-top: 27px;"><i class="fa fa-search"></i> Buscar</button>
                            </div>
                            
                            <div class="spa-12">
                                <div class="row" style="margin: 10px;">

                                    <table id="tb_pr_as" class="table table-bordered TFtable" style="margin-top: 10px;">
                                        <thead>
                                            <tr>
                                                <th>M&eacute;todo</th>
                                                <th>C&oacute;digo</th>
                                                <th>Pregunta</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tab_preg_asig">
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab3">
                            <div class="spa-12">
                                <label>Contenido E-Mail</label>
                                <textarea id="txt_cont_mail" rows="50"></textarea>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tab4">
                            <div class="spa-12">
                                <div id="jstree">
                                </div>
                            </div>  
                        </div>

                    </div>

                </section>

            </div>
            
        </div>
        
    </div>
</div>

<div class="modal" id="modalViewPre">
    <div class="modal-dialog">
        
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ViewPre','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="name_pro"></span>Preguntas por m&eacute;todo <span id="d_met"></span></h3>
        </div>
        
        <div class="modal-content">
            
            <div class="modal-body">
                <div class="spa-12">
                    <table class="table table-bordered TFtable" style="margin-top: 10px;">
                        <thead>
                            <tr>
                                <th>C&oacute;digo</th>
                                <th>Pregunta</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="tab_preguntas">
                        </tbody>
                    </table>
                </div>
                <div class="spa-12">
                    <button type="button" class="btn btn-danger addPregu pull-right"><i class="fa fa-plus"></i> Añadir</button>
                </div>
            </div>
            
        </div>
        
    </div>
</div>

<script>
    CKEDITOR.replace('txt_cont_mail');
</script>