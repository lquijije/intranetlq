
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #DDD;
        padding: 2px;
    }
    .red{
        color:#FF0000;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice8">
                
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-list-alt"></i> M&eacute;todos</div>
                    </div>
                    <button type="button" class="btn btn-danger newPregu" style="position: absolute; right: 0;"><i class="fa fa-edit"></i> Nuevo M&eacute;todo</button>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">   
                        <table class="table table-bordered" id="tab_met">
                            <thead>
                                <tr>
                                    <td>C&oacute;digo</td>
                                    <td>M&eacute;todo</td>
                                    <td>Estado</td>
                                    <td>Modificar</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(isset($metodos) && !empty($metodos)){
                                foreach ($metodos as $row){
                                    ?>
                                    <tr>
                                        <td><?php echo $row['co_met']; ?></td>
                                        <td><?php echo $row['ds_met']; ?></td>
                                        <td class="centro"><?php echo $row['ds_est']; ?></td>
                                        <td class="centro"><button type="button" class="btn btn-circle btn-warning" onclick="viewPre(<?php echo $row['co_met']; ?>,'U');"><i class="text-blanco text-size-1 fa fa-edit"></i></button></td>
                                    </tr>
                                    <?php 
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalNewMet">
    <div class="modal-dialog">
        
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('NewMet','form_NewMet');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="name_pro"></span>Nuevo M&eacute;todo</h3>
        </div>
        
        <div class="modal-content">
            
            <div class="modal-body">
                
                <?php 
                    $attributes = array('id' =>'form_NewMet','name'=>'form_NewMet');
                    echo form_open('procesos/evaluaciones/co_120',$attributes); 

                    $data = array(
                        'co_acc' =>'',
                        'co_met' =>''
                    );

                    echo form_hidden($data);
                ?>   

                <div class="spa-9">
                    <label>M&eacute;todo</label>
                    <input id="txt_des_met" type="text" class="form-control">
                </div>

                <div class="spa-3">
                    <label>Estado</label>
                    <select id="cmb_est" class="form-control">
                        <option value="A">Activo</option>
                        <option value="I">Inactivo</option>
                    </select>
                </div>

                <div class="spa-12" style="margin-top: 15px;">
                    <button type="button" class="btn btn-primary savMet pull-right"><i class="text-red fa fa-save"></i> Guardar</button>
                </div>

                <div class="clearfix"></div>
                <?php echo form_close(); ?>

            </div>
            
        </div>
        
    </div>
</div>
