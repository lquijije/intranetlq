<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                <div>
                    <a href="<?php echo site_url('procesos/evaluaciones/co_1191'); ?>" class="btn-out" title="Volver" style="left: 12px; font-size: 39px;width: 30px;"><i class="fa fa-arrow-circle-left"></i></a>
                </div>
                <form id="tab_Resp">
                    <table class="table table-bordered evalua">
                        <thead>

                            <?php

                            if (isset($_eval) && !empty($_eval)){
                                if (isset($_eval['d_ev']) && !empty($_eval['d_ev'])){
                                    foreach ($_eval['d_ev'] as $row){
                                        ?>
                                        <tr>
                                            <th colspan="6" class="tb_eva_txt"><h3 class="centro"><?php echo trim($row['e_t1']); ?></h3></th>
                                        </tr>

                                        <tr>
                                            <th colspan="6" class="tb_eva_txt"><h3 class="centro" style="padding-bottom: 20px;"><?php echo trim($row['e_t2']); ?></h3></th>
                                        </tr>
                                        <?php
                                    }
                                }
                            }

                            ?>

                            <tr>
                                <th colspan="6" class="tb_eva_txt_line">Evaluaci&oacute;n General</th>
                                <input type="hidden" id="co_eva_preg" value="<?php echo $cod_evalu_preg; ?>">
                            </tr>
                            <tr>
                                <th width="50%"></th>      

                                <?php 

                                if (isset($_punt) && !empty($_punt)){
                                    foreach ($_punt as $row){
                                        ?>
                                            <th width="5%" class="centro"><?php echo trim($row['pu_des']); ?></th>
                                        <?php
                                    }
                                }

                                ?>
                            </tr>
                        </thead>
                        <tbody id="tab_preg_asig">

                            <?php 

                            if (isset($_eval) && !empty($_eval)){
                                if (isset($_eval['d_pr']) && !empty($_eval['d_pr'])){
                                    $cod_met = 0;
                                    $i = 0;
                                    $ii = 0;
                                    foreach ($_eval['d_pr'] as $row){

                                        if ($cod_met <> (int)$row['p_cm']){
                                            $i = 0;
                                            ?>
                                                <tr>
                                                    <td colspan="6" style="text-align: center; background: rgb(204, 204, 204) none repeat scroll 0% 0%;"><b><?php echo trim($row['p_dm']); ?></b></td>
                                                </tr>
                                            <?php 
                                        }
                                        $ii++;
                                        $i++;
                                        $cod_met = (int)$row['p_cm'];
                                        ?>
                                            <tr id="option-<?php echo trim($row['p_cp']); ?>">
                                                <td><?php echo "<b>".$i .".- </b>".trim($row['p_dp']); ?></td>      

                                                <?php
                                                    foreach ($_punt as $raw){
                                                        ?>
                                                            <td><center><input type="radio" name="ck_<?php echo trim($row['p_cp']); ?>" id="opt_<?php echo trim($row['p_cp']); ?>_<?php echo trim($raw['pu_cod']); ?>" value="<?php echo trim($raw['pu_cod']); ?>"></center></td>
                                                        <?php
                                                    }
                                                ?>
                                            </tr>
                                        <?php
                                    }
                                }
                            }

                            ?>


                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </form>
                <button type="button" class="btn btn-success pull-right saveEvaPreg"><i class="fa fa-save"></i> Guardar</button></div>
                <div class="clearfix"></div>
            </section>
        </div>
    </div>

<?php

if($ejescript && !empty($cod_evalu_preg)){
    ?>

        <script>
            $(function(){
                _c_e(<?php echo $cod_evalu_preg; ?>)
            }); 
        </script>

    <?php
}

?>