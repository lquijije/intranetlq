
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #ccc;
        padding: 2px;
    }
    /*#FBFBDA*/
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Consulta Garant&iacute;as Extendidas</div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-2">
                                    <label>Busqueda</label>
                                    <select id="bu_sea" class="form-control">
                                        <option value="2">C&eacute;dula</option>
                                        <option value="1">Contrato</option>
                                        <option value="3">Factura</option>
                                    </select>
                                </div>
                                <div class="spa-2">
                                    <label>#</label>
                                    <input name="ci_txt" value="" id="ci_txt" class="form-control fallback" type="text" onkeypress="return isNumero(event,this)">
                                </div>
                                <div class="spa-1">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary seaCont"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">                      
                            <div id="detContra" class="row" style="padding-left: 17px; padding-right: 17px;">
                                <table id="tabContr" class="table table-bordered TFtable">
                                    <thead>
                                        <tr>
                                            <th>Co. Contrato</th>
                                            <th># De Factura</th>
                                            <th>Identificaci&oacute;n</th>
                                            <th>Cliente</th>
                                            <th>Fecha Contrato</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        
    </div>
</div>

<div class="modal" id="modalViewCon">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ViewCon','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Contrato Garant&iacute;a Extendida # <span id="id__contr"></span></h3>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="separador" style="padding-bottom: 15px;margin-bottom: 15px;">
                    <div class="spa-12">
                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="text-center">Cliente</h3>
                            </div>
                            <div class="panel-body">
                                <div class="spa-12 salt">
                                    <div class="spa-2">
                                        <b>Identificaci&oacute;n: </b>
                                    </div>
                                    <div class="spa-3">
                                        <span class="_i_cli"></span>
                                    </div>
                                    
                                    <div class="spa-2">
                                        <b>Nombres: </b>
                                    </div>
                                    <div class="spa-5">
                                        <span class="_n_cli"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-2">
                                        <b>Tel&eacute;fono: </b>
                                    </div>
                                    <div class="spa-3">
                                        <span class="_t_cli"></span>
                                    </div>
                                    
                                    <div class="spa-2">
                                        <b>E-Mail: </b>
                                    </div>
                                    <div class="spa-5">
                                        <span class="_e_cli"></span>
                                    </div>
                                </div>
                                
                                <div class="spa-12 salt">
                                    <div class="spa-2">
                                        <b>Direcci&oacute;n: </b>
                                    </div>
                                    <div class="spa-10">
                                        <span class="_d_cli"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-2">
                                        <b>Factura: </b>
                                    </div>
                                    <div class="spa-10">
                                        <span class="_f_cli"></span>
                                    </div>
                                </div>
  
                            </div>
                        </div>
                    
                    </div>
                    <div class="spa-6">
                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="text-center">Producto Adquirido</h3>
                            </div>
                            <div class="panel-body">
                                
                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>C&oacute;digo: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_c_art"></span>
                                    </div>
                                </div>
                                
                                <div class="spa-12 salt">
                                    <div class="spa-4">
                                        <b>Descripci&oacute;n: </b>
                                    </div>
                                    <div class="spa-8">
                                        <span class="_d_art"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>No. Serie: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_n_ser"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-6">
                                        <b>Precio de Compra: </b>
                                    </div>
                                    <div class="spa-6">
                                        $<span class="_p_con"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>Marca: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_ma_art"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>Modelo: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_mo_art"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spa-6">
                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="text-center">Garant&iacute;a Adquirida</h3>
                            </div>
                            <div class="panel-body">

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>C&oacute;digo: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_c_art_ga"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-4">
                                        <b>Descripci&oacute;n: </b>
                                    </div>
                                    <div class="spa-8">
                                        <span class="_d_art_ga"></span>
                                    </div>
                                </div>
                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>Duraci&oacute;n: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_du_art_ga"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>Precio: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_pe_art_ga"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spa-6">
                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="text-center">Documentos</h3>
                            </div>
                            <div class="panel-body">
                                
                                <div class="spa-6">
                                    <div class="box">
                                        <div class="box-body text-center">
                                            <div class="col-xs-12">
                                                <b>Certificado</b>
                                            </div>
                                            <div class="col-xs-12">
                                                <a id="a_h_11" target="_blank">
                                                    <img src="<?php echo site_url('img/icono_pdf.png') ?>" style="width: 55px;">
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="spa-6">
                                    <div class="box">
                                        <div class="box-body text-center">
                                            <div class="col-xs-12">
                                                <b>Contrato</b>
                                            </div>
                                            <div class="col-xs-12">
                                                <a id="a_h_22" target="_blank">
                                                    <img src="<?php echo site_url('img/icono_pdf_see.png') ?>" style="width: 55px;">
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>