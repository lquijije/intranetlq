
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #ccc;
        padding: 2px;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                <div class="row">
                    <div class="spa-5" style="width: auto;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-file-text"></i> Ingreso Garant&iacute;a Extendida </div>
                    </div>
                    
                    <div class="spa-7">
                        <?php echo form_open('procesos/garantia/co_122',array('id' => 'f_garan','name' => 'f_garan')); ?>
                        <ul class="nav navbar-nav">
                            <li class="user-menu">
                                
                                <?php 
                                
                                $data = array(
                                    'co_alm' => $_SESSION['cod_almacen'],
                                    'co_sr' => $_SESSION['cod_sri'],
                                    'co_ip' => $_SESSION['cod_ip']
                                );

                                echo form_hidden($data);
                                
                                    if (isset($empresas) && !empty($empresas)){
                                        $count = count($empresas);
                                        $arreglo= null;
                                        $select_title="";
                                        $select_items="";
                                        for ($i=0; $i<$count; $i++){
                                            if (trim($_SESSION['cod_almacen']) === trim($empresas[$i]['cod_bdg'])) {

                                                if ($count === 1){
                                                    $select_title ='<a href="javascript:void(0)" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($empresas[$i]['des_bdg']).' </span></a><ul class="dropdown-menu">';
                                                } else {
                                                    $select_title ='<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($empresas[$i]['des_bdg']).' <i class="caret"></i></span></a><ul class="dropdown-menu">';
                                                }

                                            } else {
                                                $CI = &get_instance();
                                                $ip = $CI->encrypt->encode(trim($empresas[$i]['ip']));
                                                $select_items.="<li><a href='javascript:void(0)' onclick='passEmpr(\"".trim($empresas[$i]['cod_bdg'])."\",\"".trim($empresas[$i]['de_sri'])."\",\"".trim($ip)."\")'>".trim($empresas[$i]['des_bdg'])."</a></li>";
                                            }
                                        }

                                        echo $select_title.$select_items.'</ul>';   

                                        ?> 
                                        <?php
                                    } else {
                                        ?>
                                            <a href="#" style="padding-top: 0px;">
                                                <span class="titulo-1 text-size-1-5" >No tiene Empresas Asignadas </span>
                                            </a>
                                        <?php
                                    }

                                ?>
                            </li>
                        </ul>
                        </form>
                    </div>
                </div>
                <div class="row">
                <?php 
                    echo form_open('procesos/garantia/co_122',array('id' => 'f_new_ga','name' => 'f_new_ga')); 
                    $data = array(
                        'co_up' => '',
                    );

                    echo form_hidden($data);
                
                ?>
                    
                    <div class="spa-4">          

                        <div class="separador box box-primary">
                            <div class="box-body">
                                <div class="separador salt" style="margin-bottom: 10px;">
                                    <h3>Factura</h3>
                                </div>
                                <div class="separador">
                                <div class="spa-12">
                                    <label># del Documento</label>
                                </div>
                                <div class="spa-3">
                                    <input id="txt_sr" name="txt_sr" class="form-control" type="text" readonly="" onkeypress="return isNumero(event,this)" value="<?php echo trim($_SESSION['cod_sri']); ?>">
                                </div>
                                <div class="spa-3">
                                    <input id="txt_cj" name="txt_cj" class="form-control" type="text" onkeypress="return isNumero(event,this)">
                                </div>
                                <div class="spa-6">
                                    <div class="input-group">
                                    <input id="txt_dc" name="txt_dc" class="form-control" type="text" onkeypress="return isNumero(event,this)">
                                    <input type="hidden" id="fe_fa" value="">
                                    <input type="hidden" id="sec_fa" value="">
                                        <div class="input-group-addon" style="padding:0;">
                                            <button type="button" class="btn btn_oculto btn-danger sea_f" style="height: 33px;"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
<!--                                <div class="spa-12">
                                    <a href="javascript:void(0)" onclick="busAvan();" class="link pull-right">(Busqueda Avanzada)</a>
                                </div>-->
                            </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="separador box box-primary">
                            <div class="box-body">
                                <div class="separador salt" style="margin-bottom: 10px;">
                                    <h3>Cliente</h3>
                                </div>

                                <div class="separador">
                                    <div class="spa-12">
                                        <label>Identificación</label>
                                    </div>
                                    <div class="spa-12">
                                        <div class="input-group">
                                            <input id="txt_ide" name="txt_ide" class="form-control" type="text" maxlength="13" onkeypress="return isNumero(event,this)">
                                            <div class="input-group-addon" style="padding:0;">
                                                <button type="button" class="btn btn_oculto btn-danger sea_c" style="height: 33px;"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="separador">
                                    <div class="spa-12">
                                        <label>Nombres y Apellidos</label>
                                    </div>
                                    <div class="spa-12">
                                        <input id="txt_nomb_ape" name="txt_nomb_ape" class="form-control uppercase" type="text">
                                    </div>
                                </div>

                                <div class="separador">
                                    <div class="spa-12">
                                        <label>Dirección</label>
                                    </div>
                                    <div class="spa-12">
                                        <textarea id="txt_direc" name="txt_direccion" type="text" class="form-control uppercase"></textarea>
                                    </div>
                                </div>

                                <div class="separador">
                                    <div class="spa-6">
                                        <label>Tel. Conven.</label>
                                        <input id="txt_conve" name="txt_convencional" class="form-control" onkeypress="return isNumero(event,this)" type="text">
                                    </div>
                                    <div class="spa-6">
                                        <label>Tel. Celular</label>
                                        <input id="txt_celular" name="txt_celular" class="form-control" onkeypress="return isNumero(event,this)" type="text">
                                    </div>
                                </div>

                                <div class="separador">
                                    <div class="spa-12">
                                        <label>E-mail</label>
                                    </div>
                                    <div class="spa-12">
                                        <input id="txt_mail" name="txt_mail" class="form-control" onblur="validaMail(this.value)" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <div class="spa-8">   
                        <div class="separador box box-primary">
                            <div class="box-body">
                                <div style="position:absolute;right:2%;">
                                    <div class="separador">
                                        <label>Enviar por E-Mail</label>
                                        <input id="bit_mail" name="bit_mail" style="margin-right: 10px;" type="checkbox">
                                    </div>
                                </div>
                                <div class="separador salt" style="margin-bottom: 10px;">
                                    <h3>Garant&iacute;a Extendida</h3>
                                </div>
                                <div class="separador">
                                    <label class="text-justify">Este contrato tiene validez exclusivamente para el producto descrito anteriormente.</label>
                                </div>

                                <div class="spa-3">
                                     <label>C&oacute;digo</label>
                                     <input type="text" id="txt_c_g" name="txt_c_g" class="form-control" readonly="">
                                </div>
                                
                                <div class="spa-4">
                                    <label>Descripci&oacute;n</label>
                                     <input type="text" id="txt_d_g" name="txt_d_g" class="form-control" readonly="">
                                </div>
                                
                                <div class="spa-2">
                                     <label>Precio</label>
                                     <input type="text" id="txt_p_g" name="txt_p_g" class="form-control" readonly="">
                                </div>

                                <div class="spa-3">
                                    <label>Duraci&oacute;n</label>
                                    <div class="separador">
                                        <b style="font-size: 18px;"><span id="cmb_ti_gar">0</span> AÑOS</b>
                                    </div>
<!--                                    <select id="cmb_ti_con" name="cmb_ti_con" class="form-control">
                                        <option value="1">12 Meses</option>
                                        <option value="2">24 Meses</option>
                                        <option value="3">36 Meses</option>
                                    </select>-->
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="separador box box-primary">
                            <div class="box-body">
                                <div class="separador salt" style="margin-bottom: 10px;">
                                    <h3>Datos del Producto Adquirido</h3>
                                </div>

                                <div class="spa-3">
                                    <label>C&oacute;digo</label>
                                    <input type="text" id="txt_c_a" name="txt_c_a" class="form-control" readonly="">
                                    <input type="hidden" id="txt_g_a" name="txt_g_a">
                                </div>

                                <div class="spa-6">
                                    <label>Descripci&oacute;n</label>
                                    <input type="text" id="txt_d_a" name="txt_d_a" class="form-control" readonly="">
                                </div>
                                
                                <div class="spa-3">
                                    <label>Serial No.</label>
                                    <input type="text" id="txt_s_n_a" name="txt_s_n_a" class="form-control uppercase">
                                </div>
                                
                                <div class="spa-3">
                                    <label>Garant&iacute;a Fabricante</label>
                                    <div class="separador">
                                        <b style="font-size: 18px;"><span id="cmb_ti_pro">0</span> MESES</b>
                                    </div>
<!--                                    <select id="cmb_ti_pro" name="cmb_ti_ga" class="form-control">
                                        <option value="1">1 Año</option>
                                        <option value="2">2 Años</option>
                                        <option value="3">3 Años</option>
                                    </select>-->
                                </div>
                                
                                <div class="spa-3">
                                    <label>Precio de Compra</label>
                                    <input type="text" id="txt_p_c_a" name="txt_p_c_a" class="form-control" readonly="">
                                </div>
                                
                                <div class="spa-3">
                                    <label>Marca</label>
                                    <input type="text" id="txt_m_a" name="txt_m_a" class="form-control uppercase">
                                </div>
                                
                                <div class="spa-3">
                                    <label>Modelo</label>
                                    <input type="text" id="txt_mo_a" name="txt_mo_a" class="form-control uppercase">
                                </div>
                                
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-danger res_g"><i class="fa fa-close"></i> Limpiar</button>
                            <button type="button" class="btn btn-success sav_g"><i class="fa fa-save"></i> Grabar</button>
                        </div>
                    </div>
                </form>
            </div>
            </section>
        </div>
        
    </div>
</div>

<div class="modal" id="modalDetFact">
  <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" class="close cl_fr_1">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Art&iacute;culos de factura # <span id="id__fact"></span></h3>
        </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' =>'form_DetFact','name'=>'form_DetFact');
            echo form_open('procesos/garantia/co_122',$attributes); 
        ?>   
        
        <div class="modal-body">
            
            <div class="separador">
                
                <table class="table table-bordered TFtable">
                    <thead>
                        <th>C&oacute;digo</th>
                        <th>Descripci&oacute;n</th>
                        <th>Cantidad</th>
                        <th>Valor</th>
                        <th></th>
                    </thead>
                    <tbody id="det_articulos">
                    </tbody>
                </table>

            </div>
            <div id="botonServicios" class="separador" style="position: relative;">
                
            </div>
                                
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalFact">
  <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('Fact','form_Fact');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Facturas por Cliente</h3>
        </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' =>'form_Fact','name'=>'form_Fact');
            echo form_open('procesos/garantia/co_122',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div class="separador line" style="padding-bottom: 15px;margin-bottom: 15px;">
                
                <div class="spa-2">
                    <label>Año</label>
                    <?php echo $cmb_anio ?>
                </div>
                
                <div class="spa-2-5">
                    <label>Mes</label>
                    <?php echo $cmb_mes ?>
                </div>
                
                <div class="spa-3">
                    <label>Identificaci&oacute;n</label>
                    <input id="txt_sear_ci" name="txt_sear_ci" class="form-control " value="" type="text">
                </div>

                <div class="spa-2">
                    <label class="out-text">Buscar</label>
                    <button type="button" class="btn btn-primary filtroFact"><i class="text-red fa fa-search"></i> Buscar</button>
                </div>

            </div>
            
            <div class="separador">
                <div id="tableFact" style="margin-left: 15px;margin-right: 15px;">
                    <table id="T_regFact" class="table table-bordered table-hover">
                    <thead>
                    <th>Almac&eacute;n</th>
                    <th>Tipo de Documento</th>
                    <th># Documento</th>
                    <th>Valor Transacci&oacute;n</th>
                    <th>Fecha</th>
                    <th></th>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
            
                                
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalViewCon">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ViewCon','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Contrato Garant&iacute;a Extendida # <span id="id__contr"></span></h3>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="separador" style="padding-bottom: 15px;margin-bottom: 15px;">
                    <div class="spa-12">
                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="text-center">Cliente</h3>
                            </div>
                            <div class="panel-body">
                                <div class="spa-12 salt">
                                    <div class="spa-2">
                                        <b>Identificaci&oacute;n: </b>
                                    </div>
                                    <div class="spa-3">
                                        <span class="_i_cli"></span>
                                    </div>
                                    
                                    <div class="spa-2">
                                        <b>Nombres: </b>
                                    </div>
                                    <div class="spa-5">
                                        <span class="_n_cli"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-2">
                                        <b>Tel&eacute;fono: </b>
                                    </div>
                                    <div class="spa-3">
                                        <span class="_t_cli"></span>
                                    </div>
                                    
                                    <div class="spa-2">
                                        <b>E-Mail: </b>
                                    </div>
                                    <div class="spa-5">
                                        <span class="_e_cli"></span>
                                    </div>
                                </div>
                                
                                <div class="spa-12 salt">
                                    <div class="spa-2">
                                        <b>Direcci&oacute;n: </b>
                                    </div>
                                    <div class="spa-10">
                                        <span class="_d_cli"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-2">
                                        <b>Factura: </b>
                                    </div>
                                    <div class="spa-10">
                                        <span class="_f_cli"></span>
                                    </div>
                                </div>
  
                            </div>
                        </div>
                    
                    </div>
                    <div class="spa-6">
                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="text-center">Producto Adquirido</h3>
                            </div>
                            <div class="panel-body">
                                
                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>C&oacute;digo: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_c_art"></span>
                                    </div>
                                </div>
                                
                                <div class="spa-12 salt">
                                    <div class="spa-4">
                                        <b>Descripci&oacute;n: </b>
                                    </div>
                                    <div class="spa-8">
                                        <span class="_d_art"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>No. Serie: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_n_ser"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-6">
                                        <b>Precio de Compra: </b>
                                    </div>
                                    <div class="spa-6">
                                        $<span class="_p_con"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>Marca: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_ma_art"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>Modelo: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_mo_art"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spa-6">
                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="text-center">Garant&iacute;a Adquirida</h3>
                            </div>
                            <div class="panel-body">

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>C&oacute;digo: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_c_art_ga"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-4">
                                        <b>Descripci&oacute;n: </b>
                                    </div>
                                    <div class="spa-8">
                                        <span class="_d_art_ga"></span>
                                    </div>
                                </div>
                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>Duraci&oacute;n: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_du_art_ga"></span>
                                    </div>
                                </div>

                                <div class="spa-12 salt">
                                    <div class="spa-3">
                                        <b>Precio: </b>
                                    </div>
                                    <div class="spa-9">
                                        <span class="_pe_art_ga"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spa-6">
                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="text-center">Documentos</h3>
                            </div>
                            <div class="panel-body">
                                
                                <div class="spa-6">
                                    <div class="box">
                                        <div class="box-body text-center">
                                            <div class="col-xs-12">
                                                <b>Certificado</b>
                                            </div>
                                            <div class="col-xs-12">
                                                <a id="a_h_11" target="_blank">
                                                    <img src="<?php echo site_url('img/icono_pdf.png') ?>" style="width: 55px;">
                                                </a>
                                            </div>
<!--                                            <div class="col-xs-6">
                                                <a id="a_h_12" target="_blank"><img src="<?php echo site_url('img/icono_pdf_see.png') ?>" class="img-responsive"></a>
                                            </div>-->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="spa-6">
                                    <div class="box">
                                        <div class="box-body text-center">
                                            <div class="col-xs-12">
                                                <b>Contrato</b>
                                            </div>
<!--                                            <div class="col-xs-6">
                                                <a id="a_h_21" target="_blank"><img src="<?php echo site_url('img/icono_pdf.png') ?>" class="img-responsive"></a>
                                            </div>-->
                                            <div class="col-xs-12">
                                                <a id="a_h_22" target="_blank">
                                                    <img src="<?php echo site_url('img/icono_pdf_see.png') ?>" style="width: 55px;">
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div id="_pdf"></div>