
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #ccc;
        padding: 2px;
    }
    /*#FBFBDA*/
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice8">
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Consulta De Ventas Por Colaborador</div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <?php $attributes = array('id' => 'f_rol','name' => 'f_rol');   
                                    echo form_open('procesos/garantia/co_124',$attributes); ?>
                                    <div class="spa-2">
                                        <label>C&oacute;digo</label>
                                        <input id="c_e" name="c_e" value="<?php echo $cod_user; ?>" class="form-control" onkeypress="return isNumero(event,this)" type="text">
                                    </div>
                                    <div class="spa-3">
                                        <label>Mes</label>
                                        <?php echo $cmb_mes; ?>
                                    </div>

                                    <div class="spa-3">
                                        <label>A&ntilde;o</label>
                                        <?php echo $cmb_anio; ?>
                                    </div>
                                    <div class="spa-1">
                                        <label class="out-text">Buscar</label>
                                        <button type="submit" class="btn btn_oculto btn-primary sea_f" style="height: 33px;ma"><i class="fa fa-search text-red"></i> Buscar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">   
                            <div class="row">                   
                                <table class="table table-bordered TFtable">
                                    <thead>
                                    <tr>
                                        
                                        <?php 
                                            if(isset($data_search) && !empty($data_search)){
                                                ?>
                                                    <th colspan="4" class="centro"><b class="text-size-1-5"><?php echo $data_search['user']['co_nom']." ".$data_search['user']['co_ape']; ?></b></th>    
                                                <?php 
                                            } else {
                                                ?>
                                                    <th colspan="4" class="centro"><b class="text-size-1-5">COLABORADOR</b></th>
                                                <?php 
                                            }
                                        ?>
                                    </tr>
                                    <tr>
                                        <th>Estructura Comercial</th>
                                        <th>Unid. Vendidas</th>
                                        <th>Valor $</th>
                                        <th>Comisi&oacute;n Ganada $</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            
                                            $sum_val = $sum_can = $sum_com =  0;
                                            
                                            if(isset($data_search) && !empty($data_search) && !empty($data_search['data'])){
                                                foreach($data_search['data'] as $row){
                                                    $sum_can += $row['ca_ven'];
                                                    $sum_val += $row['mo_ven'];
                                                    $sum_com += $row['va_gan'];
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $row['es_com']; ?></td>
                                                            <td class="centro"><?php echo $row['ca_ven']; ?></td>
                                                            <td class="derecha"><?php echo number_format($row['mo_ven'],2); ?></td>
                                                            <td class="derecha"><?php echo number_format($row['va_gan'],2); ?></td>
                                                        </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                    <tr style="height: 28px;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr style="height: 28px;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr style="height: 28px;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr style="height: 28px;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>                           
                                                <?php
                                            }
                                        ?>
                                        <tr>
                                            <td class="centro"><b style="font-size: 18px;">TOTAL VENDIDO</b></td>
                                            <td class="centro"><b style="font-size: 18px;"><?php echo (int)$sum_can; ?></b></td>
                                            <td class="derecha"><b style="font-size: 18px;">$<?php echo number_format($sum_val,2); ?></b></td>
                                            <td class="derecha"><b style="font-size: 18px;">$<?php echo number_format($sum_com,2); ?></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
