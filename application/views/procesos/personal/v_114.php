
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .dashboard {
        padding: 5em 3em;
    }
    .spa-3,.spa-4 {
        text-align: right;
        font-weight:bold;
    }
    .spa-1-5 {
        width: 10.8%;
    }
    .sta {
        color:#FFF;
        background: none repeat scroll 0% 0% #2E8BEF !important;
    }
</style>
<?php
if($this->session->flashdata('message')){
    $message = $this->session->flashdata('message');
}

if($this->session->flashdata('error')){
    $error = $this->session->flashdata('error');
}

if(function_exists('validation_errors') && validation_errors() != ''){
    $error = validation_errors();
} 
?>

<?php if (!empty($message)): ?>
<script type="text/javascript">
    $(document).ready(function() {
        errorGeneral('Sistema Intranet',"<?php echo quitar_saltos($message);?>",'danger');
    });
</script>
<?php endif; ?>

<?php if (!empty($error)): ?>
<script type="text/javascript">
    $(document).ready(function() {
        errorGeneral('Sistema Intranet',"<?php echo quitar_saltos(trim($error));?>",'danger');
    });
</script>
<?php endif; ?>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice15">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-dollar"></i> Rol de Pago</span>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <?php $attributes = array('id' => 'f_rol','name' => 'f_rol');   
                                    echo form_open('procesos/personal/co_114',$attributes); ?>
                                
                                <div class="spa-12">
                                    <label>Clave </label>
                                    <?php $data = array('id'=>'txt_clave','name'=>'txt_clave','value'=>'','class'=>'form-control','autocomplete'=>'off');
                                    echo form_password($data); ?>   
                                </div>

                                <div class="spa-12" style="margin-top: 10px;">
                                    <button type="submit" class="btn btn-primary pull-right"><i class="text-red fa fa-sign-in"></i> Aceptar</button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>
