
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    
    .red{
        color:#FF0000;
    }
</style>

<div class="demo-wrapper">
    <div class="clearfix " style="padding: 5em">
        <section class="content invoice3">
            <div class="row" style="margin-bottom: 10px;">
                <div class="spa-12">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Consulta de Marcaciones</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="spa-3-5">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <?php $attributes = array('id' => 'f_marcacion','name' => 'f_marcacion');   
                                    echo form_open('procesos/personal/co_117',$attributes); ?>

                                <div class="spa-12">
                                    <label>Periodo </label>
                                    <?php echo $cmb_fechas; ?>   
                                </div>

                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="spa-8">
                    <div class="box box-primary">
                    <h4 style="font-weight: bold;">Marcaciones registradas</h4>
                        <div class="box-body">
                            <div class="row">
                                <table class="table table-bordered TFtable">
                                    <thead>
                                        <th>Fecha</th>
                                        <th>D&iacute;a</th>
                                        <th>Hora Entrada</th>
                                        <th>Hora Salida</th>
                                        <th>Horas Lab</th>
                                        <th>Tipo</th>
                                        <th>Equipo Firma</th>
                                    </thead>
                                    <tbody>
                                        <?php 

                                        $suma = $sumaGe = 0; 

                                        if(isset($marcaciones) && !empty($marcaciones)){

                                            $a = groupArray($marcaciones,'fec_mar');
                                            $i = 0;
                                            $fecha = '';
                                            $color = '';
                                            $bandera_1 = '';

                                            foreach($marcaciones as $row){
                                                $sumaGe+= $row['tie_lab'];

                                                if ($fecha <> trim($row['fec_mar'])){
                                                        $color = $row['ing_mar'] > $row['hor_ent'].':30'?'danger red':'';

                                                        if ($bandera_1 <> trim($row['dia_mar']) && $i <> 0){
                                                            ?>
                                                                <tr>
                                                                    <td colspan="4" class="derecha" style="background:#DCDCDC;border-left: 1px solid #DCDCDC;"><strong>Total</strong></td>
                                                                    <td class="centro"><strong><?php echo number_format($suma,2); ?></strong></td>
                                                                </tr>
                                                            <?php
                                                        }
                                                        $suma=0;
                                                    ?>

                                                    <tr>
                                                        <td class="centro <?php echo $color;?>" rowspan="<?php echo count($a[$i]['groupeddata']); ?>"><?php echo date('d/m/Y',strtotime($row['fec_mar'])) ?></td>
                                                        <td class="centro <?php echo $color;?>" rowspan="<?php echo count($a[$i]['groupeddata']); ?>"><?php echo $row['dia_mar'] ?></td>
                                                    <?php

                                                    $i++;   
                                                } 
                                                ?>
                                                    <td class="centro <?php echo $color;?>"><?php echo $row['ing_mar'] ?></td>
                                                    <td class="centro"><?php echo $row['sal_mar'] ?></td>
                                                    <td class="centro"><?php echo number_format($row['tie_lab'],2) ?></td>
                                                    <td class="centro"><?php echo $row['tip_reg'] ?></td>
                                                    <td class="centro"><?php echo $row['equipo'] ?></td>
                                                </tr>

                                                <?php

                                                $suma+= $row['tie_lab'];
                                                $bandera_1 = trim($row['dia_mar']);
                                                $fecha = trim($row['fec_mar']);
                                                $color = '';
                                            }
                                            ?>

                                            <tr>
                                               <td colspan="4" class="derecha" style="background:#DCDCDC;border-left: 1px solid #DCDCDC;border-bottom:1px solid #DCDCDC;"><strong>Total</strong></td>
                                               <td class="centro"><strong><?php echo number_format($suma,2); ?></strong></td>
                                            </tr>

                                            <?php
                                        } 

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <h4 style="font-weight: bold;">Total General: <?php echo number_format($sumaGe,2); ?> horas</h4>
                    </div>
                </div>
                
                <div class="spa-4">
                    <div class="box box-primary">
                    <h4 style="font-weight: bold;">Horario de Trabajo</h4>
                        <div class="box-body">
                            <div class="row">
                                <table class="table table-bordered TFtable">
                                    <thead>
                                        <th>D&iacute;a</th>
                                        <th>Entrada</th>
                                        <th>Salida</th>
                                    </thead>
                                    <tbody>
                                        <?php 

                                        if (!empty($horario)){
                                            foreach ($horario as $row){
                                                ?>
                                                    <tr>
                                                        <td><?php echo $row['dia'] ?></td>
                                                        <td class="centro"><?php echo $row['ent'] ?></td>
                                                        <td class="centro"><?php echo $row['sal'] ?></td>
                                                    </tr>
                                                <?php 
                                            }
                                        } 

                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                     </div>
                </div>
                
            </div>
        </section>
    </div>
</div>

