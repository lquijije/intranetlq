
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
        /*font-size: 14px;*/
    }
    .dashboard {
        padding: 5em 3em;
    }
    
</style>

<div class="demo-wrapper">
    <div class="clearfix" style="padding: 5em 3em;">
        <div class="spa-12">
            <section class="content invoice8">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-dollar"></i> Rol de Pago</span>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            
                            <div class="row">
                                <?php $attributes = array('id' => 'f_rol','name' => 'f_rol');   
                                    echo form_open('procesos/personal/co_114/retROL',$attributes); ?>
                                    
                                    <div class="spa-6">
                                        <label>Seleccione el tipo de comprobante</label>
                                        <?php echo $cmb_tipo ?>
                                    </div>
                                    
                                    <div class="spa-6">
                                        <label>Empresa </label>
                                        <?php echo $cmb_emp; ?>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">  
                            <div class="row">
                                
                            <?php if($valida === true){ ?>

                                <div class="spa-12">
                                    <h4 style="font-weight: bold;"><?php echo $tipo_compro; ?></h4>
                                </div>

                                <?php $attributes = array('id' => 'f_rol','name' => 'f_rol');   
                                    echo form_open('procesos/personal/co_114/retROL',$attributes); ?>
                                    <input type="hidden" name="cmb_emp" value="<?php echo $idd_emp; ?>">
                                    <input type="hidden" name="cmb_tipo" value="<?php echo $idd_tip; ?>">
                                    <div class="spa-1">
                                        <label>Centro:</label>
                                    </div>
                                    <div class="spa-11">
                                        <label><strong> <?php echo $centro; ?></strong></label>
                                    </div>
                                    <div class="spa-1">
                                        <label>Fecha</label>
                                    </div>
                                    <div class="spa-3">
                                        <?php echo $cmb_fech_rol ?>
                                    </div>
                                    <div class="spa-3">
                                        <strong><?php echo $fecha ?></strong>
                                    </div>
                                </form>
                                
                                <div class="spa-12">
                                    <table class="table table-bordered TFtable">
                                        <thead>
                                        <tr>
                                            <th colspan="3" class="centro" width="49%">Concepto de ingresos</th>
                                            <th style="background: #DCDCDC;" width="2%"></th>
                                            <th colspan="3" class="centro" width="49%">Concepto de egresos</th>
                                        </tr>
                                        <tr>
                                            <th style="background: #FFF7DD; color: #555;">Descripci&oacute;n</th>
                                            <th style="background: #FFF7DD; color: #555;">Hora / D&iacute;a</th>
                                            <th style="background: #FFF7DD; color: #555;">Valor</th>
                                            <th style="background: #DCDCDC;" width="2%"></th>
                                            <th style="background: #FFF7DD; color: #555;">Descripci&oacute;n</th>
                                            <th style="background: #FFF7DD; color: #555;">Hora / D&iacute;a</th>
                                            <th style="background: #FFF7DD; color: #555;">Valor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(isset($rolPago) && !empty($rolPago)){

                                                $val_ing = 0;
                                                $val_egr = 0;

                                                foreach ($rolPago as $row){
                                                    $val_ing+= (float)$row['res_i'];
                                                    $val_egr+= (float)trim($row['res_e'],'-');
                                                    ?>
                                                    <tr>
                                                        <td style="height: 28px;"><?php echo $row['cod_i'] ?></td>
                                                        <td class="centro"><?php echo (int)$row['ent_i'] === 0?'':(int)$row['ent_i']; ?></td>
                                                        <td class="derecha"><?php echo $row['res_i'] ?></td>
                                                        <td style="background: #DCDCDC;"></td>
                                                        <td><?php echo $row['cod_e'] ?></td>
                                                        <td class="centro"><?php echo $row['ent_e'] ?></td>
                                                        <td class="derecha"><?php echo trim($row['res_e'],'-') ?></td>
                                                    </tr>
                                                    <?php 
                                                }

                                                $total = $val_ing - $val_egr;

                                                ?>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="2" >Total Ingresos</td>
                                                        <td class="derecha">$ <?php echo $val_ing;?></td>
                                                        <td style="background: #DCDCDC;"></td>
                                                        <td colspan="2">Total Descuentos</td>
                                                        <td class="derecha">$ <?php echo $val_egr;?></td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="3" style="height: 28px;"></td>
                                                    </tr>
                                                    <tr style="font-weight: bold; font-size: 18px;">
                                                        <td colspan="2">Neto a Pagarse</td>
                                                        <td class="derecha">$ <?php echo $total; ?></td>
                                                    </tr>
                                                <?php 
                                            }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>

                            <?php } else { ?>
                                
                                <div class="spa-12">
                                    <h4 style="font-weight: bold;">Nómina est&aacute; en procesos internos, verifique despues del cierre de periodo.</h4>
                                </div>
                                
                            <?php } ?>
                            
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if(isset($bono['error']) && (int)$bono['error'] === 0 && $valida === true){ ?>
                    
                    <?php if(!empty($bono['res_2'])){ ?>
                    
                    <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">
                    <div class="row" style="padding-bottom: 10px;"> 
                        <div class="spa-12">
                            <h4 style="font-weight: bold;">Calificaci&oacute;n del bono mensual</h4>
                            
                            <div class="spa-2">
                                <label><strong>Tipo Bono</strong></label>
                            </div>
                            <div class="spa-4">
                                <label><?php echo $bono['res_1'][0]['tip_bon']." - ".$bono['res_1'][0]['des_des'] ?></label>
                            </div>
                            
                            <div class="spa-2">
                                <label><strong>Fecha Empleo</strong></label>
                            </div>
                            <div class="spa-4">
                                <label><?php echo $bono['res_1'][0]['fec_emp'] ?></label>
                            </div>
                            
                            <div class="spa-2">
                                <label><strong>Bono anual</strong></label>
                            </div>
                            <div class="spa-4">
                                <label>$ <?php echo number_format($bono['res_1'][0]['bon_anu']); ?></label>
                            </div>
                            
                            <div class="spa-2">
                                <label><strong>Dias no laborados</strong></label>
                            </div>
                            <div class="spa-4">
                                <label><?php echo $bono['res_1'][0]['dis_n_lab'] ?></label>
                            </div>
                            
                            <div class="spa-2">
                                <label><strong>Bono objetivo</strong></label>
                            </div>
                            <div class="spa-4">
                                <label><?php echo (int)$bono['res_1'][0]['val_por']."% -> <strong>$".number_format($bono['res_1'][0]['bon_obj'],2)."</strong>" ?></label>
                            </div>
                            
                            <table class="table table-bordered TFtable">
                                <thead>
                                <tr>
                                    <th>C.Costo</th>
                                    <th>Indicador</th>
                                    <th>Rango aceptable</th>
                                    <th>Rendimiento</th>
                                    <th>Base</th>
                                    <th>Calculado</th>
                                    <th>A pagar</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 

                                        $base = $pagar = 0;

                                        foreach ($bono['res_2'] as $row){ 
                                            $base+=(float)$row['val_dis'];
                                            $pagar+=(float)$row['val_gan'];
                                    ?>
                                        <tr>
                                            <td><?php echo $row['cod_cos']." - ".$row['txt_cos'];?></td>
                                            <td><?php echo $row['cod_ind']." - ".$row['txt_ind'];?></td>
                                            <td class="centro"><?php echo (int)$row['val_min']."% - ".(int)$row['val_max']."%";?></td>
                                            <td class="centro"><?php echo (int)$row['val_red']."%";?></td>
                                            <td class="derecha">$ <?php echo $row['val_dis'];?></td>
                                            <td class="derecha">$ <?php echo $row['val_cal'];?></td>
                                            <td class="derecha">$ <?php echo $row['val_gan'];?></td>
                                        </tr>

                                    <?php } ?>

                                        <tr>
                                            <td style="font-weight: bold;" class="derecha" colspan="4">Totales</td>
                                            <td style="font-weight: bold;" class="derecha">$ <?php echo number_format($base,2); ?></td>
                                            <td></td>
                                            <td style="font-weight: bold;" class="derecha">$ <?php echo number_format($pagar,2); ?></td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    </div>
                    </div>
                    </div>
                
                    <?php } ?>
                        
                    <?php if(!empty($bono['res_3'])){ ?>
                
                        <div class="row">
                        <div class="box box-primary">
                        <div class="box-body">  
                        <div class="row" style="padding-bottom: 10px;"> 
                            <div class="spa-12">
                                <h4 style="font-weight: bold;">Detalle de alcance de meta trimestral</h4>
                                <table class="table table-bordered TFtable">
                                    <thead>
                                    <tr>
                                        <th>C.Costo</th>
                                        <th>Dias no laborados</th>
                                        <th>Rendimiento</th>
                                        <th>Base</th>
                                        <th>Calculado</th>
                                        <th>Trimestre</th>
                                        <th>A pagar</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach ($bono['res_2'] as $row){ ?>

                                            <tr>
                                                <td><?php echo $row['des_cco'];?></td>
                                                <td><?php echo $row['dis_n_lab'];?></td>
                                                <td class="derecha">$ <?php echo number_format($row['val_red'],2);?></td>
                                                <td class="derecha">$ <?php echo number_format($row['val_dis'],2);?></td>
                                                <td class="derecha">$ <?php echo number_format($row['val_cal'],2);?></td>
                                                <td class="derecha">$ <?php echo number_format($row['val_gan'],2);?></td>
                                                <td class="derecha">$ <?php echo number_format($row['val_bon_pag'],2);?></td>
                                            </tr>

                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        </div>
                        </div>
                        </div>
                
                    <?php } ?>

                <?php } ?>

                <?php if(isset($linePool) && !empty($linePool)){ ?>
                
                    <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">
                    <div class="row" style="padding-bottom: 10px;"> 
                        <div class="spa-12">
                            <h4 style="font-weight: bold;">Usted tiene las siquientes lineas del POOL de la empresa</h4>
                            
                            <table class="table table-bordered TFtable">
                                <thead>
                                <tr>
                                    <th>Operadora</th>
                                    <th>Numero</th>
                                    <th>Equipo</th>
                                    <th>Ultima Renovaci&oacute;n</th>
                                    <th>Adendum</th>
                                    <th>Adendum Pendiente</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($linePool as $row){ ?>
                                        <tr>
                                            <td><?php echo $row['tip_ope'];?></td>
                                            <td><?php echo $row['num_cel'];?></td>
                                            <td><?php echo $row['nom_equ'];?></br><b>Cobertura: $ <?php echo $row['val_cob'];?></b></td>
                                            <td><?php echo $row['ult_ren'];?></td>
                                            <td class="derecha">$ <?php echo $row['ede_tot'];?></td>
                                            <td class="derecha">$ <?php echo $row['ede_x_pag'];?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                    </div>
                    </div>

                <?php } ?>
                            
            </section>
        </div>
    </div>
</div>
