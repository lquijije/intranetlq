
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .dashboard {
        padding: 5em 3em;
    }
    .spa-3,.spa-4 {
        text-align: right;
        font-weight:bold;
    }
    .spa-1-5 {
        width: 10.8%;
    }
    .sta {
        color:#FFF;
        background: none repeat scroll 0% 0% #2E8BEF !important;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                <div class="row" style="margin-bottom: 4px;">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Consulta de Reclamos</span>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">

                                <div class="spa-2">
                                    <label>Cédula </label>
                                    <input type="text" id="txt_ci" onkeypress="return isNumero(event,this)" name="txt_ci" class="form-control input-sm" value="">
                                </div>

                                <div class="spa-1">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchCI"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">                      
                        <div id="detalleReclamos" class="row" style="padding-left: 17px; padding-right: 17px;">

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalViewDoc">
  <div class="modal-dialog">
        
      <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ViewDoc','form_ViewDoc');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
          <h3 class="modal-title"><span id="name_pro"></span> (Solicitud de Reclamo # <span id="num_doc_fac"></span>)</h3>
        </div>
        <div class="modal-content">
            <?php 
                $attributes = array('id' =>'form_ViewDoc','name'=>'form_ViewDoc');
                echo form_open('procesos/solicitud_cheque/co_109',$attributes); 
            ?>   

            <div id="cont_cab" class="modal-body">
                
            </div>
            
            <div class="clearfix"></div>
            <?php echo form_close(); ?>
        </div>
        
  </div>
</div>
