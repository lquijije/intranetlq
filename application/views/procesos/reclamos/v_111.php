<style>

.content {
    overflow: visible;
}

.dropdown-menu > li > a {
    padding: 0px 20px;
    font-size: 13px;
}

.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    vertical-align: middle;
}

</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
 
            <section class="content invoice">
                <button type="button" class="btn btn-success saveReclamo" style="position: absolute; right: 0;margin: 0.2% 0.6%;"><i class="fa fa-save"></i> Grabar Reclamo</button>
                <div class="row">
                    <div class="spa-4" style="max-width: 23.8%; width: auto;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-file-text"></i> Reclamos </div>
                    </div>
                    
                    <div class="spa-8">
                        <?php echo form_open('procesos/reclamos/co_111',array('id' => 'f_solch','name' => 'f_solch')); ?>
                        
                        <ul class="nav navbar-nav">
                            <li class="user-menu">
                                
                                <?php 
                                
                                $data = array(
                                    'cod_emp' => $_SESSION['cod_almacen']
                                );

                                echo form_hidden($data);
                                
                                    if (isset($empresas) && !empty($empresas)){
                                        $count = count($empresas);
                                        $arreglo= null;
                                        $select_title="";
                                        $select_items="";
                                        for ($i=0; $i<$count; $i++){
                                            if (trim($_SESSION['cod_almacen']) === trim($empresas[$i]['cod_bdg'])) {

                                                if ($count === 1){
                                                    $select_title ='<a href="#" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($empresas[$i]['des_bdg']).' </span></a><ul class="dropdown-menu">';
                                                } else {
                                                    $select_title ='<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($empresas[$i]['des_bdg']).' <i class="caret"></i></span></a><ul class="dropdown-menu">';
                                                }

                                            } else {
                                                $select_items.="<li><a href='javascript:void(0)' onclick='passEmpr(\"".trim($empresas[$i]['cod_bdg'])."\",\"".$i."\")'>".trim($empresas[$i]['des_bdg'])."</a></li>";
                                            }
                                        }

                                        echo $select_title.$select_items.'</ul>';   

                                        ?> 
                                        <?php
                                    } else {
                                        ?>
                                            <a href="#" style="padding-top: 0px;">
                                                <span class="titulo-1 text-size-1-5" >No tiene Empresas Asignadas </span>
                                            </a>
                                        <?php
                                    }

                                ?>
                            </li>
                        </ul>
                        </form>
                    </div>
                </div>
                <?php 
                    echo form_open('procesos/reclamos/co_111',array('id' =>'form_reclamos_ge','name'=>'form_reclamos_ge')); 
                ?>  
                <div class="row">
                    <div class="spa-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    
                                    <div class="separador">
                                        <h4 style="font-weight: bold;">¿Quien realiza el Reclamo? </h4> 
                                    </div>
                                    
                                    <div class="separador">
                                        <div class="spa-12">
                                            <label>Identificaci&oacute;n</label>
                                        </div>
                                        <div class="spa-11">
                                            <input id="txt_identificacion" name="txt_identificacion" type="text" class="form-control">
                                        </div>
                                        <div class="spa-1">
                                            <button type="button" class="btn btn_oculto btn-square bg-red searchClient"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                    
                                    <div class="separador">
                                        <div class="spa-12">
                                            <label>Nombres y Apellidos</label>
                                        </div>
                                        <div class="spa-12">
                                            <input id="txt_nomb_ape" name="txt_nomb_ape" type="text" class="form-control">
                                        </div>
                                    </div>
                                    
                                    <div class="separador">
                                        <div class="spa-12">
                                            <label>Direcci&oacute;n</label>
                                        </div>
                                        <div class="spa-12">
                                            <textarea id="txt_direccion" name="txt_direccion" type="text" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="separador">
                                        <div class="spa-6">
                                            <label>Tel&eacute;fono Convencional</label>
                                            <input id="txt_convencional" name="txt_convencional" type="text" class="form-control" onkeypress="return isNumero(event,this)">
                                        </div>
                                        <div class="spa-6">
                                            <label>Tel&eacute;fono Celular</label>
                                            <input id="txt_celular" name="txt_celular" type="text" class="form-control" onkeypress="return isNumero(event,this)">
                                        </div>
                                    </div>
                                    
                                    <div class="separador">
                                        <div class="spa-12">
                                            <label>E-mail</label>
                                        </div>
                                        <div class="spa-12">
                                            <input id="txt_mail" name="txt_mail" type="text" class="form-control" onblur="validaMail(this.value)">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="getID" class="spa-8">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div style="position:absolute;right:2%;">
                                        <div class="separador" style="border:2px solid #CCC; padding:10px;"><h4 style="font-weight: bold;">Forma en que desea recibir respuesta</h4>
                                            <label>E-Mail</label>
                                            <input id="bit_mail" name="bit_mail" type="checkbox" style="margin-right: 10px;">
                                            <label>Convencional</label>
                                            <input id="bit_convencional" name="bit_convencional" type="checkbox" style="margin-right: 10px;">
                                            <label>Celular</label>
                                            <input id="bit_celular" name="bit_celular" type="checkbox">
                                        </div>
                                    </div>
                                    <div class="separador">
                                        <h4 style="font-weight: bold;">Identificaci&oacute;n del Bien o Servicio</h4>
                                    </div>
                                    
                                    <div class="separador">
                                        
                                        <div class="spa-2">
                                            <label>Tipo</label>
                                            <select id="cmb_tipo" name="cmb_tipo" class="form-control">
                                                <option value="NNN">Seleccionar</option>
                                                <option value="B">Bien</option>
                                                <option value="S">Servicio</option>
                                            </select>
                                        </div>
                                        
                                        <div id="ifB" class="spa-3" style="display: none;">
                                            <label>No. De Factura</label>
                                            <div class="input-group">
                                                <input type="text" id="txt_num_doc" name="txt_num_doc" class="form-control" value="" readonly="">
                                                <div class="input-group-addon" style="padding:0;">
                                                    <button type="button" class="btn btn_oculto busFact"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="ifS" class="separador" style="display: none;">
                                            <div class="spa-12">
                                                <label>Descripci&oacute;n del servicio</label>
                                                <textarea id="txt_desc_serv" name="txt_desc_serv" type="text" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        
                                        <div id="ifA" class="separador" style="display: none;">
                                            <div class="spa-12" style="margin-top: 10px;">
                                                <table id="getArti" class="table table-bordered TFtable">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">Detalle de Art&iacute;culos</th>
                                                        </tr>
                                                        <tr>
                                                            <th>C&oacute;digo</th>
                                                            <th>Descripci&oacute;n</th>
                                                            <th>Reclamo</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="Pass_det_art">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    
                                    <div class="separador">
                                        <h4 style="font-weight: bold;">Descripci&oacute;n del Reclamo</h4>
                                    </div>

                                    <div class="separador">
                                        <div class="spa-3">
                                            <label>Fecha del inconveniente</label>
                                            <div class="input-group">
                                                <input id="txt_fec_doc" name="txt_fec_doc" class="form_datetime form-control " value="" type="text" readonly="">
                                                <div class="input-group-addon" style="padding:0;">
                                                    <button type="button" class="btn btn_oculto"><i class="fa fa-calendar"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="spa-2">
                                            <label>Hora</label>
                                            <div class="input-group">
                                                <input id="txt_hora" name="txt_hora" class="form_hour form-control " value="" type="text" readonly="">
                                                <div class="input-group-addon" style="padding:0;">
                                                    <button type="button" class="btn btn_oculto"><i class="fa fa-clock-o"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="separador">
                                        <div class="spa-12">
                                            <label>Descripci&oacute;n clara y consica del reclamo</label>
                                            <textarea id="txt_desc_clara" name="txt_desc_clara" type="text" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="separador">
                                        <div class="spa-12">
                                            <label>Resultado que pretende obtener con el reclamo</label>
                                            <textarea id="txt_pretende" name="txt_pretende" type="text" class="form-control"></textarea>
                                        </div>
                                    </div>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                </form>
            </section>
            
        </div>
    </div>
</div>

<div class="modal" id="modalFact">
  <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('Fact','form_Fact');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Facturas por Cliente</h3>
        </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' =>'form_Fact','name'=>'form_Fact');
            echo form_open('procesos/reclamos/co_111',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div class="separador line" style="padding-bottom: 15px;margin-bottom: 15px;">
                
                <div class="spa-2">
                    <label>Año</label>
                    <?php echo $cmb_anio ?>
                </div>
                
                <div class="spa-2-5">
                    <label>Mes</label>
                    <?php echo $cmb_mes ?>
                </div>
                
                <div class="spa-3">
                    <label>Identificaci&oacute;n</label>
                    <input id="txt_sear_ci" name="txt_sear_ci" class="form-control " value="" type="text">
                </div>

                <div class="spa-2">
                    <label class="out-text">Buscar</label>
                    <button type="button" class="btn btn-primary filtroFact"><i class="text-red fa fa-search"></i> Buscar</button>
                </div>

            </div>
            
            <div class="separador">
                <div id="tableFact" style="margin-left: 15px;margin-right: 15px;">
                    <table id="T_regFact" class="table table-bordered table-hover">
                    <thead>
                    <th>Almac&eacute;n</th>
                    <th>Tipo de Documento</th>
                    <th># Documento</th>
                    <th>Valor Transacci&oacute;n</th>
                    <th>Fecha</th>
                    <th></th>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
            
                                
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalDetFact">
  <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('DetFact','form_DetFact');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Art&iacute;culo de factura # <span id="id__fact"></span></h3>
        </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' =>'form_DetFact','name'=>'form_DetFact');
            echo form_open('procesos/reclamos/co_111',$attributes); 
        ?>   
        
        <div class="modal-body">
            
            <div class="separador">
                
                <table class="table table-bordered TFtable">
                    <thead>
                        <th>C&oacute;digo</th>
                        <th>Descripci&oacute;n</th>
                        <th>Cantidad</th>
                        <th>Valor</th>
                        <th></th>
                    </thead>
                    <tbody id="det_articulos">
                    </tbody>
                </table>

            </div>
            <div id="botonServicios" class="separador" style="position: relative;">
                
            </div>
                                
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

