
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #DDD;
        padding: 2px;
    }
    .red{
        color:#FF0000;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Reembolso Cocina de Inducci&oacute;n</div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-2-5">
                                    <label>Proveedores</label>
                                    <?php echo ($cmb_prov);?>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="spa-2">
                                    <label>Fecha Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_ini" value="<?php echo date('Y/m/d',strtotime('-1 month')) ?>" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                        <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="<?php echo date('Y/m/d',strtotime('-1 month')) ?>">
                                    </div>
                                </div>
                                <div class="spa-2">
                                    <label>Fecha Final</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_fin" value="<?php echo date('Y/m/d') ?>" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                        <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="<?php echo date('Y/m/d') ?>">
                                    </div>
                                </div>
                                <div class="spa-1">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchDoc"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div id="table_con_ind">
                            </div>
                            <div id="total" style="display: none;">
                                <h4 style="font-weight: bold;">Total Pendiente: $<span class="tot_pen"></span></h4>
                                <h4 style="font-weight: bold;">Total Enviado: $<span class="tot_env"></span></h4>
                                <h4 style="font-weight: bold;">Total Cancelado: $<span class="tot_can"></span></h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalViewDet">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ViewDet','');" class="close">
                <span aria-hidden="true"><i class="fa fa-times-circle"></i></span>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Fact:# <span id="reemGeAl" style="font-size: 20px;" ></span></h3>
        </div>
        
        <div class="modal-body">
                
            <div class="spa-7">
                <div class="centro text-size-1-5 line" style="margin-bottom: 10px;padding-bottom: 10px;">Datos de la Factura</div>
                
                <div class="spa-12 salt">
                    <div class="spa-4">
                        <b>CI.: </b>
                    </div>
                    <div class="spa-8">
                        <span class="_i_cli"></span>
                    </div>
                </div>
                
                <div class="spa-12 salt">
                    <div class="spa-4">
                        <b>Nombres: </b>
                    </div>
                    <div class="spa-8">
                        <span class="_n_cli"></span>
                    </div>
                </div>
                
                <div class="spa-12">
                    <div class="spa-4">
                        <b>Fecha: </b>
                    </div>
                    <div class="spa-8">
                        <span class="_f_fac"></span>
                    </div>
                </div>
                
                <div class="spa-12">
                    <div class="spa-12">
                        <table class="table table-bordered TFtable">
                            <thead>
                                <th>Art&iacute;culo</th>
                                <th></th>
                                <th>Numero Serie</th>
                                <th>Cuotas</th>
                                <th>Valor Total</th>
                            </thead>
                            <tbody id="det_art_fac">

                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
            
            <div class="spa-5">
                <div class="centro text-size-1-5 line" style="margin-bottom: 10px;padding-bottom: 10px;">Datos del Pago</div>
                
                <div class="spa-12 salt">
                    <div class="spa-4">
                        <b>Fecha Envio: </b>
                    </div>
                    <div class="spa-8">
                        <span class="_f_env"></span>
                    </div>
                </div>
                
                <div class="spa-12 salt">
                    <div class="spa-4">
                        <b>Usuario Envio: </b>
                    </div>
                    <div class="spa-8">
                        <span class="_u_env"></span>
                    </div>
                </div>
                
                <div class="spa-12 salt">
                    <div class="spa-4">
                        <b>Fecha Cancelaci&oacute;n: </b>
                    </div>
                    <div class="spa-8">
                        <span class="_f_can"></span>
                    </div>
                </div>
                
                <div class="spa-12 salt">
                    <div class="spa-4">
                        <b>Usuario Cancelaci&oacute;n: </b>
                    </div>
                    <div class="spa-8">
                        <span class="_u_can"></span>
                    </div>
                </div>
                
                <div class="spa-12">
                    <div class="spa-4">
                        <b>Fecha Estimada de Cancelaci&oacute;n: </b>
                    </div>
                    <div class="spa-8">
                        <span class="_f_est"></span>
                    </div>
                </div>
                
            </div>
            
            <div class="spa-12 buttons">
                
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
    </div>
  </div>
</div>