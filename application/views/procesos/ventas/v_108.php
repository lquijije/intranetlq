<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice3">
                <div class="row">
                    <div class="spa-12">
                        <span class="titulo-1 text-size-2 text-blanco">Ventas Diarias</span>
                        <small class="text-size-1 text-gris">Ventas</small>
                    </div>
                </div>
                
                <div class="row">
                    <div class="spa-3 box box-primary">
                        <div class="box-body">
                            <div class="row">

                                <div class="spa-12">
                                    <label>Fecha</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input id="txt_fec_Ge" name="txt_fec_Ge" type="text" class="form-control" readonly="readonly" value="<?php echo $fecha." | ".$hora ?>">
                                    </div>
                                </div>
                                
                                <div class="spa-12" style="margin-top:5px;">
                                    <button type="button" class="pull-right btn btn-primary searchDatVent"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="spa-9" id="ContVentPri" style="border:5px dashed #FFF;">
                        <div id="placeholder" style="height:500px;">
                            <div style="margin-left: auto;margin-right: auto; width: 300px; text-align: center;padding-top: 130px;">
                                <h1 id="textChart" style="font-weight: bold;color:#C9C9C9;">Area de Estadistica</h1>
                                <i class="fa fa-table" style="color:#DCDCDC;font-size: 10em;"></i>
                            </div>
                        </div>
                        <div id="tableVentasDiarias" class="spa-12">
                            
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>

<script>

$(document).ready(function() {

    if($('#txt_fec_Ge').val().trim() !==''){
        if($('#txt_fec_Ge').val().trim() ==='|'){
            $('#txt_fec_Ge').val('');
        } else {
            getDatVentas();
        }
    }
});


</script>