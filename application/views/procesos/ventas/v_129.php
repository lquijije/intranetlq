<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle {
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #ccc;
        padding: 2px;
    }
    .external-event {
        background: #CCC !important;
    }
    .btn-out {
        position: relative;
    }
    .btn-out_2 {
        height: 30px;
        border: 0px none;
        cursor: pointer;
        color: #FFF;
        background: transparent none repeat scroll 0% 0%;
        position: absolute;
        top: 17px;
    }
    .media-body, .media-left, .media-right {
        display: table-cell !important;
    }
    .media-heading {
        border-top: 1px solid #ccc !important;
    }

</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="col-xs-12">
            <section class="content">
                <div class="col-xs-3">
                    <div class="col-xs-12" style="margin-bottom:13px;">
                        <div class="pull-left">
                            <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-calendar"></i> Planificación</div>
                        </div>
                    </div>

                    <div class="col-xs-12" style="width:100%;">
                        <div class="box box-primary" style="padding: 0;">
                            <div class="list-group-header bg-red">
                                <!--<span class=" text-size-1-5 titulo-1 tit_cal"></span>-->
                                <table style="width: 100%;">
                                    <tr>
                                        <th class="izquierda">
                                            <button type="button" class="btn-out p_res" style="margin: 0;"><i class="fa fa-arrow-circle-left"></i> </button>
                                        </th>
                                        <th class="centro tit_cal" style="font-size:20px;">
                                        </th>
                                        <th class="derecha">
                                            <button type="button" class="btn-out p_sum" style="margin: 0;"><i class="fa fa-arrow-circle-right"></i> </button>
                                        </th>
                                    </tr>
                                </table>
                                <input type="hidden" id="fe_ini" name="fe_ini" value="<?php echo date('Y-m-d'); ?>">
                            </div>
                            <div id="plani_tra" class="list-group" style="overflow-y:auto;height:25em;padding-top:10px;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-6">

                    <div class="col-xs-12" style="margin-bottom:10px;">
                        <div class="pull-left">
                            <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-trophy"></i> Campa&ntilde;as</div>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-success new_camp"><i class="fa fa-plus"></i> Nuevo</button>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body">                      
                                <div id="detContra" class="row" style="padding-left: 17px; padding-right: 17px;">
                                    <table id="tabCamp" class="table table-bordered TFtable">
                                        <thead>
                                            <tr>
                                                <th>C&oacute;digo</th>
                                                <th>Descripci&oacute;n</th>
                                                <th>Estado</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($Reglas)) {
                                                foreach ($Reglas as $row) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $row['co_reg']; ?></td>
                                                        <td><?php echo $row['ds_reg']; ?></td>
                                                        <td class="centro"><?php echo $row['ds_est']; ?></td>
                                                        <td class="centro">
                                                            <button type="button" onclick="viewReg(<?php echo $row['co_reg']; ?>)" class="btn btn-circle btn-warning" onclick="viewPro(1, 'U');"><i class="text-blanco text-size-1 fa fa-edit"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-xs-3">
                    <div class="col-xs-12" style="margin-bottom:13px;">
                        <div class="pull-left">
                            <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-cogs"></i> Job</div>
                        </div>
                    </div>

                    <div class="col-xs-12" style="width:100%;">
                        <div class="box">
                            <div id="porc_ge" class="panel_slide_rob" style="background: #FF6B6B;padding:20px 20px 0px 20px;position:relative;">
                                <div class="titulo-1 text-size-1-5 text-blanco" style="margin-bottom:15px;"> 
                                    Generar Datos
                                    <button title="Iniciar" type="button" class="btn-out_2 ini_job"><i class="text-size-1-25 fa fa-play-circle"></i></button>
                                </div>
                                <table class="table text-blanco" style="margin-top: 15px;">
                                    <tbody>
                                        <tr>
                                            <td>JOB</td>
                                            <td class="derecha"><?php echo $activi_job['name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Habilitar Robot</td>
                                            <td class="derecha">
                                                <?php
                                                $checkSp = $estadoRobot == 1 ? 'checked' : '';
                                                ?>
                                                <input id="enable_job" type="checkbox" class="js-switch" <?php echo $checkSp; ?>/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Inici&oacute;</td>
                                            <td id="job_ini" class="derecha"><?php echo!empty($activi_job['run_Requested_date']) ? date('Y-m-d H:i:s', strtotime($activi_job['run_Requested_date'])) : ''; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Termin&oacute;</td>
                                            <td id="job_ter" class="derecha"><?php echo!empty($activi_job['stop_execution_date']) ? date('Y-m-d H:i:s', strtotime($activi_job['stop_execution_date'])) : ''; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Demor&oacute;</td>
                                            <td id="job_dem" class="derecha"><?php
                                                $abc = (int) $activi_job['ElapsedSeconds'];
                                                echo gmdate('H:i:s', $abc);
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <td>Estado</td>
                                            <td class="derecha"><?php echo $activi_job['last_run_outcome']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Mensaje</td>
                                            <td id="job_men" class="derecha"><?php echo $activi_job['last_outcome_message']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalModReg">
    <div class="modal-dialog">

        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ModReg', 'form_ModReg');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Campa&ntilde;a #<span id="name_reg"></span></h3>
        </div>

        <div class="modal-content">

            <div class="modal-body">

                <?php
                $attributes = array('id' => 'form_ModReg', 'name' => 'form_ModReg');
                echo form_open('procesos/ventas/co_128', $attributes);

                $data = array(
                    'co_acc' => '',
                    'co_reg' => ''
                );

                echo form_hidden($data);
                ?> 

                <section>

                    <ul class="nav nav-tabs">
                        <li id="m_n1" class="active"><a href="#tab1" data-toggle="tab">Inicio</a></li>
                        <li id="m_n2"><a href="#tab2" data-toggle="tab">Canales</a></li>
                        <li id="m_n3"><a href="#tab3" data-toggle="tab">Planificaci&oacute;n</a></li>
                        <li id="m_n4"><a href="#tab4" data-toggle="tab">Excel Manual</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="col-xs-12">

                                <h3 class="salt text-center">Caracteristicas</h3>

                                <div class="col-xs-9">
                                    <label>Descripci&oacute;n</label>
                                    <input class="form-control" id="txt_des" name="txt_des">
                                </div>

                                <div class="col-xs-3">
                                    <label>Estado</label>
                                    <select id="cmb_est" class="form-control">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-xs-12">
                                <h3 class="salt text-center">Par&aacute;metros</h3>
                                <div style="margin-bottom: 5px;">
                                    <div class="col-xs-6">
                                        <?php echo $cmb_param; ?>
                                    </div>

                                    <div class="col-xs-6">

                                        <div class="input-group">
                                            <input id="txt_param" name="txt_param" class="form-control" onkeypress="return isNumero(event, this)" type="text">
                                            <div class="input-group-addon" style="padding:0;">
                                                <button type="button" class="btn btn_oculto btn-danger add_p" style="height: 33px;"><i class="fa fa-plus"></i> Añadir</button>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="col-xs-12">
                                    <table class="table table-bordered TFtable">
                                        <thead>
                                        <th>C&oacute;digo</th>
                                        <th>Descripci&oacute;n</th>
                                        <th>Valor</th>
                                        <th></th>
                                        </thead>
                                        <tbody id="reg_para">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="col-xs-12">
                                <div class="col-xs-12" style="margin-bottom: 5px;">
                                    <div class="col-xs-10">
                                        <?php echo $cmb_canal; ?>
                                    </div>

                                    <div class="col-xs-2">
                                        <button type="button" class="btn btn_oculto btn-success add_c" style="height: 33px;"><i class="fa fa-plus"></i> Añadir</button>

                                        <!--<div class="input-group">
                                                <input id="txt_dc" name="txt_dc" class="form-control" onkeypress="return isNumero(event,this)" type="text">
                                                <div class="input-group-addon" style="padding:0;">
                                                    <button type="button" class="btn btn_oculto btn-danger sea_f" style="height: 33px;"><i class="fa fa-plus"></i> Añadir</button>
                                                </div>
                                            </div>-->
                                    </div>

                                    <div class="col-xs-12">
                                        <textarea name="txt_men_ad_can" value="" id="txt_men_ad_can" class="form-control" style="height: 100px;"></textarea>
                                    </div>

                                </div>

                                <div class="col-xs-12">
                                    <ul id="contCanalAsig" class="list-unstyled col-xs-12"></ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="col-xs-12">
                                <div style="margin-bottom: 5px;">
                                    <div class="col-xs-2">
                                        <input id="txt_plani" name="txt_plani" class="form-control" onkeypress="return isNumero(event, this)" type="text">
                                    </div>
                                    <div class="col-xs-3" id="div_can_det">
                                        <?php echo $cmb_can_pla; ?>    
                                    </div>
                                    <div class="col-xs-3">
                                        <?php echo $cmb_pri_pla; ?>
                                    </div>
                                    <div class="col-xs-2">
                                        <select id="cmb_est_pla" class="form-control">
                                            <option value="A">Activo</option>
                                            <option value="I">Inactivo</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <button type="button" class="btn btn_oculto btn-danger add_pl" style="height: 33px;"><i class="fa fa-plus"></i> Añadir</button>
                                    </div>

                                </div>

                                <div class="col-xs-12">
                                    <table class="table table-bordered TFtable">
                                        <thead>
                                        <th>Dias desplazamiento</th>
                                        <th>Canales</th>
                                        <th>Prioridades</th>
                                        <th>Estado</th>
                                        <th></th>
                                        </thead>
                                        <tbody id="reg_plani">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4">
                            <div class="col-xs-3">
                                <h4><i class="fa fa-paste"></i> Datos desde excel</h4>
                                <label>Fecha</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="txt_fec_ini" id="txt_fec_ini" data-date-format="yyyy-mm-dd" class="form_datetime form-control" type="text">
                                </div>
                                <textarea id="textarea_copy" class="form-control" style="height: 250px;" placeholder="..."></textarea>
                                <button type="button" class="btn btn-success convertText pull-right">Ingresar</button>
                            </div>
                            <div class="col-xs-9">
                                <table class="table table-bordered TFtable">
                                    <thead>
                                    <th>Fecha</th>
                                    <th>Total</th>
                                    <th></th>
                                    </thead>
                                    <tbody id="reg_fech"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </section>

                <div class="col-xs-12">
                    <button type="button" class="btn btn-primary pull-right sav_r"><i class="fa fa-save"></i> Guardar</button>
                </div>

                <?php echo form_close(); ?>

            </div>

        </div>

    </div>
</div>
