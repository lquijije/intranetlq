<?php 
    date_default_timezone_set("America/Guayaquil"); 
?>
<script src="<?php echo site_url(); ?>js/plugins/chart/Chart.min.js" type="text/javascript"></script>
<script src="<?php echo site_url(); ?>js/plugins/chart/util.js" type="text/javascript"></script>
<style>
    /*
        [class*="spa-"] { margin: 0.2% 0.1%; }
    */
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .box .box-body {
        padding: 20px;
    }
    .leg_cha {
        position: absolute; 
        top: 50px; 
        left: 45px; 
    }
    #reportrange{
        cursor: pointer;
        margin-right: 50px;
    }
    .pieLabelBackground{
        border-radius: 5px;top: 1px;    
    }
    .chart-legend {
        margin: 10px auto; 
    }
    .legendLabel {
        padding-right: 10px;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .btn-out_2 {
        height: 30px;
        border: 0px none;
        cursor: pointer;
        color: #FFF;
        background: transparent none repeat scroll 0% 0%;
        position: absolute;
        top: 17px;
    }
    .salt {
        border-bottom: 1px solid #CCC;
    }
    
    .red{
        color:#FF0000;
    }
    
    .progress{
        margin-bottom: 0px;
        height: 15px;
        font-size: 8px;
    }
    
    .font_pro {
        font-size: 30px !important;
    }
    
    .tab_2 {
        border-collapse:collapse!important;
    }
    

    .flot-x-axis div.flot-tick-label { 
        /* Rotate Axis Labels */
        transform: translateX(50%) rotate(60deg); /* CSS3 */
        transform-origin: 0 0;

        -ms-transform: translateX(50%) rotate(60deg); /* IE */
        -ms-transform-origin: 0 0;

        -moz-transform: translateX(50%) rotate(60deg); /* Firefox */
        -moz-transform-origin: 0 0;

        -webkit-transform: translateX(50%) rotate(60deg); /* Safari and Chrome */
        -webkit-transform-origin: 0 0;

        -o-transform: translateX(50%) rotate(60deg); /* Opera */
        -o-transform-origin: 0 0;
    }
    
    .dashboard {
        padding: 5em 2em 8px;
    }
    
    .external-event{
        background: #DDD;
        padding:10px;
    }
    
    #panels-wrap {
        position:relative;
        overflow:hidden;
    }
    #slide {
        position:relative;
        left:-100%;
        width:200%;
    }
    .panel_slide_rob {
        position:relative;
        display:inline-block;
        width:50%;
        height:100%;
        top:0;
        vertical-align:top;
    }
    
    .excel_das {
/*        border: 0px none;
        cursor: pointer;
        color: #FFF;
        background: transparent none repeat scroll 0% 0%;
        margin: 5px;
        font-size: 20px;
        background-image: url("../img/excel.gif");
        background-size: 25px 25px;
        background-repeat: no-repeat;
        width: 25px;
        height: 25px;*/
        border: 0px none;
        cursor: pointer;
        color: #FFF;
        font-size: 20px;
        width: 25px;
        background: transparent url("<?php echo site_url(); ?>img/excel.gif") no-repeat scroll 0% 0% / 25px 25px;
        height: 25px;
        position: absolute;
        left: 10px;
        top: 10px;
    }
    
    .tab_concurrencia {
        width: 100%;
        margin: 0;
    }
    
    .tab_concurrencia th {
        min-width: 1%;
        text-align: center;
        border: 2px solid #FFF;
        color:#FFF;
    }
    
    .tab_concurrencia td {
        text-align: center;
    }
    
    .pie_call {
        cursor: pointer;
    }
    
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                
                <div class="spa-12" style="padding:10px; color:#FFF;">
                    <div class="separador">
                        <div class="col-lg-4">
                            <span class=" text-size-1-5 titulo-1"> <i class="fa fa-dashboard"></i> Robot</span>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input id="txt_search" name="txt_search" class="form-control" onkeypress="return isNumero(event,this)" type="text" placeholder="Cédula o Número">
                                <div class="input-group-addon" style="padding:0;">
                                    <button type="button" class="btn btn-success sea_c" style="height: 33px;"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="pull-right" id="reportrange" style="font-size: 21px;">
                                <i class="fa fa-calendar"></i> 
                                <span><?php echo date('d',strtotime($fec_ini))." ".substr(conveMes(date('n',strtotime($fec_ini))),0,3)." ".date('Y',strtotime($fec_ini)); ?> - <?php echo date('d',strtotime($fec_fin))." ".substr(conveMes(date('n',strtotime($fec_fin))),0,3)." ".date('Y',strtotime($fec_fin)); ?></span> 
                                <b class="caret"></b>
                            </div>
                            <button type="button" class="btn-out ref_pag" title="Refrescar"><i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                </div>
                
                <?php 
                    $attributes = array('id' =>'form_mon_robot','name'=>'form_mon_robot');
                    echo form_open('procesos/ventas/co_128',$attributes); 

                    $data = array(
                        'f_i' => $fec_ini,
                        'f_f' => $fec_fin
                    );

                    echo form_hidden($data);
                ?>   
                
                <div class="spa-12">
                    
                <?php if(!empty($r2_Data)){ ?>
                    <div class="spa-12">
                        <?php
                            for ($i=0;$i< count($r2_Data); $i++){
                                $cursor = (int)$r2_Data[$i]['co_can'] == 4?'pie_call':'';
                        ?>
                            <div class="spa-4">
                                <div class="box">
                                    <div class="list-group-header" style="background:<?php echo $r2_Data[$i]['cl_can']; ?>;color:#FFF;">
                                        <div class="col-xs-7">
                                            <h3><i class="fa <?php echo $r2_Data[$i]['st_can']; ?>"></i> <?php echo trim($r2_Data[$i]['ds_can']); ?></h3>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="pull-right">
                                                <h3><i class="fa fa-users"></i> <?php echo number_format($r2_Data[$i]['ca_can']); ?></h3>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="box-body" style="position: relative;">
                                        <a href="<?php echo site_url('lib/ge_excel.php?c_c='.(int)$r2_Data[$i]['co_can'].'&f_i='.$fec_ini.'&f_f='.$fec_fin); ?>" style="z-index: 99;" target="_blank" class="excel_das" title="Exportar Excel"></a>
                                        <div class="spa-12" style="margin-bottom: 15px;">
                                            <div id="flot-pie-<?php echo (int)$r2_Data[$i]['co_can']; ?>" class="<?php echo $cursor; ?>" style="height: 150px;"></div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <?php 
//                                            echo '<pre>';
//                                            print_r($r2_Data[$i]['campanias']);
//                                            echo '</pre>';
//                                            die;
                                            for ($a=0;$a< count($r2_Data[$i]['campanias']); $a++){
                                        ?>
                                        
                                            <div class="box">
                                                <div class="list-group-header" style="padding: 5px;background: rgb(224, 224, 224) none repeat scroll 0% 0%;">
                                                    <table class="table">
                                                            <tr>
                                                                <td width="40%"><i class="fa <?php echo $r2_Data[$i]['st_can']; ?>"></i> <?php echo ucwords(strtolower($r2_Data[$i]['campanias'][$a]['ds_reg'])); ?></td>
                                                                <td width="30%" class="derecha" style="line-height: 10px;"><i class="fa fa-users"></i> <?php echo number_format($r2_Data[$i]['campanias'][$a]['to_reg']); ?><br><span style="font-size: 11px;">Clientes</span></td>
                                                                <?php if($update) { ?><td width="30%" class="derecha" style="line-height: 10px;"><i class="fa fa-dollar"></i> <?php echo number_format($r2_Data[$i]['campanias'][$a]['va_ges'],2); ?><br><span style="font-size: 11px;">Gestionado</span></td><?php } ?>
                                                            </tr>
                                                    </table>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="box-body">
                                                    <div class="spa-12" style="margin-bottom: 15px;">
                                                        <div id="flot-pie-<?php echo (int)$r2_Data[$i]['co_can']; ?>ca<?php echo $r2_Data[$i]['campanias'][$a]['co_reg']; ?>" class="<?php echo $cursor; ?>" style="height: 80px;"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <script>
                                                    $(document).ready(function(){
                                                        var chaDataCamp = eval(<?php echo json_encode($r2_Data[$i]['campanias'][$a]['chart_dat']); ?>);
                                                        $.plot("#flot-pie-<?php echo (int)$r2_Data[$i]['co_can']; ?>ca<?php echo $r2_Data[$i]['campanias'][$a]['co_reg']; ?>", chaDataCamp, {
                                                            series: {
                                                                pie: {
                                                                    //innerRadius: 0.5,
                                                                    show: true,       
                                                                    radius: 1,
                                                                    label: {
                                                                        show:true,
                                                                        radius:1,
                                                                        threshold: 0.1,
                                                                        formatter: labelFormatter,
                                                                        background: {
                                                                            opacity: 0.7,
                                                                            color: '#333'
                                                                        }
                                                                    }
                                                                }
                                                            },
                                                            grid: {
                                                                hoverable: true
                                                            },
                                                            legend: {
                                                                show: true
                                                            }

                                                        });
                                                    });
                                                </script>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                        <?php 
                                            }
                                        ?>
                                            
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                        
                            <script>
                                $(document).ready(function(){
                                    var chaData = eval(<?php echo json_encode($r2_Data[$i]['chart']['chart_dat']); ?>);
                                    $.plot("#flot-pie-<?php echo (int)$r2_Data[$i]['co_can']; ?>", chaData, {
                                        series: {
                                            pie: {
                                                //innerRadius: 0.5,
                                                show: true,       
                                                radius: 1,
                                                label: {
                                                    show:true,
                                                    radius:1,
                                                    threshold: 0.1,
                                                    formatter: labelFormatter,
                                                    background: {
                                                        opacity: 0.7,
                                                        color: '#333'
                                                    }
                                                }
                                            }
                                        },
                                        grid: {
                                            hoverable: true
                                        },
                                        legend: {
                                            show: true
                                        }

                                    });
                                });
                            </script>
                        <?php } ?>
                        <div class="clearfix"></div>

                    </div>
                <?php } else { ?>
                    
                    <div class="spa-12" id="ContVentPri" style="border:5px dashed #FFF;">
                        <div id="placeholder" style="height:500px;">
                            <div style="margin-left: auto;margin-right: auto; width: 500px; text-align: center;padding-top: 130px;">
                                <h1 id="textChart" style="font-weight: bold;color:#C9C9C9;">Sin datos en busqueda realizada</h1>
                                <i class="fa fa-pie-chart" style="color:#DCDCDC;font-size: 10em;"></i>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                </div>
                
                <?php echo form_close(); ?>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalModBus">
    <div class="modal-dialog">
        
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ModBus','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><i class="fa fa-search"></i> Busqueda <span id="name_bus"></span></h3>
        </div>
        
        <div class="modal-content">
            
            <div class="modal-body">
                <div class="spa-12">
                    
                    <div class="spa-12 salt">
                        <div class="spa-2">
                            <b>Cod Cliente: </b>
                        </div>
                        <div class="spa-4">
                            <span class="_co_cli"></span>
                        </div>
                        <div class="spa-2">
                            <b>Cédula: </b>
                        </div>
                        <div class="spa-4">
                            <span class="_ce_cli"></span>
                        </div>
                    </div>
                    
                    <div class="spa-12 salt">
                        <div class="spa-2">
                            <b>Nombres: </b>
                        </div>
                        <div class="spa-4">
                            <span class="_no_cli"></span>
                        </div>
                        <div class="spa-2">
                            <b>Dias vencido: </b>
                        </div>
                        <div class="spa-4">
                            <span class="_di_ven"></span>
                        </div>
                    </div>
                    
                    <div class="spa-12 salt">
                        <div class="spa-2">
                            <b>Pago minimo: </b>
                        </div>
                        <div class="spa-1">
                            <span class="_pa_min"></span>
                        </div>
                        <div class="spa-2">
                            <b>Pago vencido: </b>
                        </div>
                        <div class="spa-1">
                            <span class="_pa_ven"></span>
                        </div>
                        <div class="spa-2">
                            <b>Saldo: </b>
                        </div>
                        <div class="spa-1">
                            <span class="_va_sal"></span>
                        </div>
                        <div class="spa-2">
                            <b>Estado: </b>
                        </div>
                        <div class="spa-1">
                            <span class="_es_cab"></span>
                        </div>
                    </div>
                    <div class="spa-12 salt" style="margin-top:20px;">
                        <table class="table table-bordered TFtable">
                            <thead>
                                <th>Campaña</th>
                                <th>Secuencia</th>
                                <th>Contacto</th>
                                <th>Canal</th>
                                <th>Proveedor</th>
                                <th>Mensaje</th>
                                <th>Estado</th>
                                <th>Duración</th>
                                <th>Precio</th>
                                <th>Fecha Envio</th>
                                <th>Respuesta</th>
                                <th>Fecha Generaci&oacute;n</th>
                            </thead>
                            <tbody id="reg_busq">
                            </tbody>
                        </table>
                    </div>
                </div>  
                                
            </div>
            
        </div>
        
    </div>
</div>

<div class="modal" id="modalModCall">
    <div class="modal-dialog">
        
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ModCall','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Llamadas <span id="name_call"></span></h3>
        </div>
        
        <div class="modal-content">
            
            <div class="modal-body">
                <div  style="margin-top:30px;">
                    <div id="canvas_call_det" class="spa-8"></div>
                    <div id="canvas_call_res" class="spa-4"></div>    
                </div>
            </div>
            
        </div>
        
    </div>
</div>

<script>

function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;border-radius: 3px;'>" + Math.round(series.percent) + "%</div>";
}

</script>
