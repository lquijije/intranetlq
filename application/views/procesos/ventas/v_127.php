<style>
    .salt {
        border-bottom: 1px solid #FFF;
        padding: 2px;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice15">
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-ticket"></i> Generar orden pavo</div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <?php $attributes = array('id' => 'f_cla','name' => 'f_cla');   
                                    echo form_open('procesos/ventas/co_127',$attributes); ?>
                                    <div class="separador salt" style="margin-bottom: 10px;">
                                        <h3>Factura</h3>
                                    </div>
                                    <div class="separador">
                                    <div class="spa-12">
                                        <label># del Documento</label>
                                    </div>
                                    <div class="separador">
                                        <div class="spa-12">
                                            <input id="nu_doc" name="nu_doc" class="form-control fallback" type="text" onkeypress="return isNumero(event,this)" placeholder="___-___-_________">
                                        </div>
                                        <div class="spa-12" style="margin-top: 10px;">
                                            <button type="button" class="btn pull-right btn-success sea_f" style="margin-left: 10px;"><i class="fa fa-ticket"></i> Verificar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalGenCup">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('GenCup','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Validar cup&oacute;n FACT# <span id="id__fact"></span></h3>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="separador" style="padding-top:25px;">
                    <div class="spa-12">
                        <div class="spa-12 salt">
                            <div class="spa-4">
                                <b>Identificaci&oacute;n: </b>
                            </div>
                            <div class="spa-8">
                                <span class="_i_cli"></span>
                            </div>
                        </div>
                        <div class="spa-12 salt">
                            <div class="spa-4">
                                <b>Nombres: </b>
                            </div>
                            <div class="spa-8">
                                <span class="_n_cli"></span>
                            </div>
                        </div>
                        <div class="spa-12 salt">
                            <div class="spa-4">
                                <b>Tel&eacute;fono: </b>
                            </div>
                            <div class="spa-8">
                                <span class="_t_cli"></span>
                            </div>
                        </div>
                        <div class="spa-12 salt">
                            <div class="spa-4">
                                <b>Fecha de compra: </b>
                            </div>
                            <div class="spa-8">
                                <span class="_f_com"></span>
                            </div>
                        </div>
                    </div>     
                    
                    <div class="col-xs-2 col-xs-offset-5">
                        <button type="button" class="btn btn-success val_c" style="margin: 10px; font-size: 20px;"><i class="fa fa-ticket"></i> Entregar Orden</button>
                    </div>
                    
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
    
<script type="text/javascript" src="<?php echo site_url(); ?>js/plugins/mask/jquery.mask.js"></script>
<script type="text/javascript">
  $(function() {
    $('.fallback').mask("000r000r000000000", {
      translation: {
        'r': {
          pattern: /[\/]/, 
          fallback: '-'
        }, 
        placeholder: "__-__-____"
      }
    });

  });
</script>