
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .dashboard {
        padding: 5em 3em;
    }
    .input-sm {
        height:25px;
    }
    .spa-3,.spa-4 {
        text-align: right;
        font-weight:bold;
    }
    .spa-1-5 {
        width: 10.8%;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-check"></i> Aprobaci&oacute;n POS</div>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px;">
                    <div class="spa-12">
                        <div class="box box-primary">
                            <div class="box-body">                      
                                <div id="detContra" class="row" style="padding-left: 17px; padding-right: 17px;">
                                    <table id="tab_Aut" class="table table-bordered TFtable" style="width: 100%;"></table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success pull-right a_sol" style="margin-top: 10px;"><i class="fa fa-check"></i> Procesar</button>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalDetallSolic">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header bg-orange">
            <button type="button" onclick="quitarmodalGeneral('DetallSolic','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="titleDoc"></span></h3>
        </div>
        <div class="modal-body">
            <div id="view_Soli_cab">
                
            </div>
            
            <div class="clearfix"></div>
        </div>
    </div>
  </div>
</div>

<div class="modal" id="modalDetFact">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header bg-orange">
            <button type="button" onclick="quitarmodalGeneral('DetFact','');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Factura #<span id="nuFact"></span></h3>
        </div>
        <div class="modal-body">
            <div id="view_fact"></div>
            <div class="clearfix"></div>
        </div>
    </div>
  </div>
</div>