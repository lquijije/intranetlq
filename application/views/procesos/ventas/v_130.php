<!--<link href='<?php echo base_url('js/plugins/fullcalendar/fullcalendar.css'); ?>' rel='stylesheet' />
<link href='<?php echo base_url('js/plugins/fullcalendar/fullcalendar.print.css'); ?>' rel='stylesheet' media='print' />
<script src='<?php echo base_url('js/plugins/fullcalendar/fullcalendar.js'); ?>'></script>
<script src='<?php echo base_url('js/plugins/fullcalendar/lang/es.js'); ?>'></script>-->

<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle {
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #ccc;
        padding: 2px;
    }
    .external-event {
        background: #CCC !important;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice1">
                <div class="spa-4">
                    <div class="row" style="margin-bottom:13px;">
                        <div class="pull-left">
                            <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-calendar"></i> Planificación</div>
                        </div>
                    </div>
                        
                    <div class="row" style="width:100%;">
                        <div class="box box-primary" style="padding: 0;">
                            <div class="list-group-header bg-red">
                                <span class=" text-size-1-5 titulo-1">Enero</span>
                            </div>
                            <div id="plani_tra" class="list-group" style="overflow-y:auto;height:25em;padding-top:10px;"></div>
                        </div>
                    </div>
                        
                </div>
                <div class="spa-8">
                    
                    <div class="row" style="margin-bottom:10px;">
                        <div class="pull-left">
                            <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-trophy"></i> Campa&ntilde;as</div>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-success new_camp"><i class="fa fa-plus"></i> Nuevo</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="box box-primary">
                            <div class="box-body">                      
                                <div id="detContra" class="row" style="padding-left: 17px; padding-right: 17px;">
                                    <table id="tabCamp" class="table table-bordered TFtable">
                                        <thead>
                                            <tr>
                                                <th>C&oacute;digo</th>
                                                <th>Descripci&oacute;n</th>
                                                <th>Estado</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 

                                                if(!empty($Reglas)){

                                                    foreach ($Reglas as $row){
                                                        ?>

                                                        <tr>
                                                            <td class="centro"><?php echo $row['co_reg']; ?></td>
                                                            <td class="centro"><?php echo $row['ds_reg']; ?></td>
                                                            <td class="centro"><?php echo $row['ds_est']; ?></td>
                                                            <td class="centro">
                                                                <button type="button" onclick="viewReg(<?php echo $row['co_reg']; ?>)" class="btn btn-circle btn-warning" onclick="viewPro(1,'U');"><i class="text-blanco text-size-1 fa fa-edit"></i></button>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                    }

                                                }

                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalModReg">
    <div class="modal-dialog">
        
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ModReg','form_ModReg');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Campa&ntilde;a #<span id="name_reg"></span></h3>
        </div>
        
        <div class="modal-content">
            
            <div class="modal-body">
                
                <?php 
                    $attributes = array('id' =>'form_ModReg','name'=>'form_ModReg');
                    echo form_open('procesos/ventas/co_128',$attributes); 

                    $data = array(
                        'co_acc' =>'',
                        'co_reg' =>''
                    );

                    echo form_hidden($data);
                ?> 
                
                <section>

                    <ul class="nav nav-tabs">
                        <li id="m_n1" class="active"><a href="#tab1" data-toggle="tab">Inicio</a></li>
                        <li class="" id="m_n2"><a href="#tab2" data-toggle="tab">Canales</a></li>
                        <li class="" id="m_n3"><a href="#tab3" data-toggle="tab">Planificacion</a></li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab1">
                            
                            <div class="spa-12">
                                
                                <h3 class="salt text-center">Caracteristicas</h3>
                                
                                <div class="spa-9">
                                    <label>Descripci&oacute;n</label>
                                    <input class="form-control" id="txt_des" name="txt_des">
                                </div>
                                
                                <div class="spa-3">
                                    <label>Estado</label>
                                    <select id="cmb_est" class="form-control">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>
                                    </select>
                                </div>
                                
                            </div>
                            
                            <div class="spa-12">
                                <h3 class="salt text-center">Par&aacute;metros</h3>
                                <div style="margin-bottom: 5px;">
                                    <div class="spa-6">
                                        <?php echo $cmb_param; ?>
                                    </div>

                                    <div class="spa-6">
                                        
                                        <div class="input-group">
                                            <input id="txt_param" name="txt_param" class="form-control" onkeypress="return isNumero(event,this)" type="text">
                                            <div class="input-group-addon" style="padding:0;">
                                                <button type="button" class="btn btn_oculto btn-danger add_p" style="height: 33px;"><i class="fa fa-plus"></i> Añadir</button>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="spa-12">
                                    <table class="table table-bordered TFtable">
                                        <thead>
                                            <th>C&oacute;digo</th>
                                            <th>Descripci&oacute;n</th>
                                            <th>Valor</th>
                                            <th></th>
                                        </thead>
                                        <tbody id="reg_para">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="tab-pane" id="tab2">
                            <div class="spa-12">
                                <div class="spa-12" style="margin-bottom: 5px;">
                                    <div class="spa-10">
                                        <?php echo $cmb_canal; ?>
                                    </div>

                                    <div class="spa-2">
                                        <button type="button" class="btn btn_oculto btn-success add_c" style="height: 33px;"><i class="fa fa-plus"></i> Añadir</button>

                                    <!--<div class="input-group">
                                            <input id="txt_dc" name="txt_dc" class="form-control" onkeypress="return isNumero(event,this)" type="text">
                                            <div class="input-group-addon" style="padding:0;">
                                                <button type="button" class="btn btn_oculto btn-danger sea_f" style="height: 33px;"><i class="fa fa-plus"></i> Añadir</button>
                                            </div>
                                        </div>-->
                                    </div>
                                    
                                    <div class="spa-12">
                                        <textarea name="txt_men_ad_can" value="" id="txt_men_ad_can" class="form-control" style="height: 100px;"></textarea>
                                    </div>
                                    
                                </div>

                                <div class="spa-12">
                                    <ul id="contCanalAsig" class="list-unstyled spa-12"></ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tab3">
                            <div class="spa-12">
                                <div style="margin-bottom: 5px;">
                                    <div class="spa-2">
                                        <input id="txt_plani" name="txt_plani" class="form-control" onkeypress="return isNumero(event,this)" type="text">
                                    </div>
                                    <div class="spa-3" id="div_can_det">
                                        <?php echo $cmb_can_pla; ?>    
                                    </div>
                                    <div class="spa-3">
                                        <?php echo $cmb_pri_pla; ?>
                                    </div>
                                    <div class="spa-2">
                                        <select id="cmb_est_pla" class="form-control">
                                            <option value="A">Activo</option>
                                            <option value="I">Inactivo</option>
                                        </select>
                                    </div>
                                    <div class="spa-2">
                                        <button type="button" class="btn btn_oculto btn-danger add_pl" style="height: 33px;"><i class="fa fa-plus"></i> Añadir</button>
                                    </div>

                                </div>

                                <div class="spa-12">
                                    <table class="table table-bordered TFtable">
                                        <thead>
                                            <th>Dias desplazamiento</th>
                                            <th>Canales</th>
                                            <th>Prioridades</th>
                                            <th>Estado</th>
                                            <th></th>
                                        </thead>
                                        <tbody id="reg_plani">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </section>
               
                <div class="spa-12">
                    <button type="button" class="btn btn-primary pull-right sav_r"><i class="fa fa-save"></i> Guardar</button>
                </div>
                
                <?php echo form_close(); ?>
                                
            </div>
            
        </div>
        
    </div>
</div>
