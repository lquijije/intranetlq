
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    .salt {
        border-bottom: 1px solid #ccc;
        padding: 2px;
    }
    .content {
        overflow: visible;
    }
    .dropdown-menu > li > a {
        padding: 1px 20px;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice6">
                <div class="row">
                    <div class="spa-5" style="width: auto;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-file-text"></i> Solicitud de Autorizaci&oacute;n </div>
                    </div>
                    
                    <div class="spa-7">
                        <?php echo form_open('procesos/ventas/co_131',array('id' => 'f_solici','name' => 'f_solici')); ?>
                        <ul class="nav navbar-nav">
                            <li class="user-menu">
                                
                                <?php 
                                
                                $data = array('co_alm' => $_SESSION['cod_almacen'],'co_sr' => $_SESSION['cod_sri']);

                                echo form_hidden($data);

                                if (isset($empresas) && !empty($empresas)){
                                    $count          = count($empresas);
                                    $arreglo        = null;
                                    $select_title   = "";
                                    $select_items   = "";
                                    
                                    for ($i=0; $i<$count; $i++){
                                        if (trim($_SESSION['cod_almacen']) === trim($empresas[$i]['cod_bdg'])) {
                                            if ($count === 1){
                                                $select_title ='<a href="javascript:void(0)" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($empresas[$i]['des_bdg']).' </span></a><ul class="dropdown-menu">';
                                            } else {
                                                $select_title ='<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($empresas[$i]['des_bdg']).' <i class="caret"></i></span></a><ul class="dropdown-menu">';
                                            }
                                        } else {
                                            $select_items.="<li><a href='javascript:void(0)' onclick='passEmpr(\"".trim($empresas[$i]['cod_bdg'])."\",\"".trim($empresas[$i]['de_sri'])."\")'>".trim($empresas[$i]['des_bdg'])."</a></li>";
                                        }
                                    }

                                    echo $select_title.$select_items.'</ul>';   

                                    ?> 
                                    <?php
                                } else {
                                    ?>
                                        <a href="#" style="padding-top: 0px;">
                                            <span class="titulo-1 text-size-1-5" >No Tiene Almacenes Asignados </span>
                                        </a>
                                    <?php
                                }

                                ?>
                            </li>
                        </ul>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="spa-12">          
                        <div class="separador box box-primary" style="padding-bottom:15px;">
                            <div class="box-body">
                                <div class="separador salt" style="margin-bottom: 10px;">
                                    <h3>Factura</h3>
                                </div>
                                <div class="spa-3">
                                    <label>Motivo</label>
                                    <div class="spa-12">
                                        <?php echo $cmb_tip; ?>
                                    </div>
                                </div>
                                <div class="spa-3">
                                    <label>Fecha</label>
                                    <div class="input-group">
                                        <input id="txt_fec_doc" name="txt_fec_doc" class="form_datetime form-control" value="" readonly="" type="text">
                                        <div class="input-group-addon" style="padding:0;">
                                            <button type="button" class="btn btn_oculto" style="height: 33px;"><i class="fa fa-calendar"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="spa-2">
                                    <label># de Documento</label>
                                    <input id="txt_sr" name="txt_sr" class="form-control" type="text" readonly="" onkeypress="return isNumero(event,this)" value="<?php echo trim($_SESSION['cod_sri']); ?>">
                                </div>
                                <div class="spa-2">
                                    <label class="out-text"># Caja</label>
                                    <input id="txt_cj" name="txt_cj" class="form-control" type="text" onkeypress="return isNumero(event,this)">
                                </div>
                                <div class="spa-2">
                                    <label class="out-text"># Documento</label>
                                    <div class="input-group">
                                    <input id="txt_dc" name="txt_dc" class="form-control" type="text" onkeypress="return isNumero(event,this)">
                                        <div class="input-group-addon" style="padding:0;">
                                            <button type="button" class="btn btn_oculto btn-danger sea_f" style="height: 33px;"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="spa-12" style="margin-bottom: 15px;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-check"></i> Estado de Solicitudes</div>
                    </div>
                    <div class="spa-12">
                        
                        <div class="box box-primary">
                            <div class="box-body">                      
                                <div id="detContra" class="row" style="padding-left: 17px; padding-right: 17px;">
                                    <table id="tab_Aut" class="table table-bordered TFtable"></table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        
    </div>
</div>

<div class="modal" id="modalDetFact">
  <div class="modal-dialog">
    <div class="modal-header bg-red">
        <button type="button" class="close" onclick="quitarmodalGeneral('DetFact','');">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title">Art&iacute;culos de factura # <span id="id__fact"></span></h3>
    </div>
    <div class="modal-content">
        <div class="modal-body">
            <div id="cntDetFac"></div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>