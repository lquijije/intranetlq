<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice10">
                
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><!--<i class="fa fa-search"></i>--> Aprobaci&oacute;n de Solicitud de Cheque</div>
                    </div>
                </div>
                
                <div class="row" style="margin-top: 10px;">
                    <div class="spa-12">
                        
                    <div class="box box-primary bg-red-gradient">
                        <div class="box-body" style="padding: 35px;">
                            <div class="row">

                                <div class="spa-12" style="font-size: 16px;">
                                    <?php echo trim($txt_error); ?>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    </div>
                    
                </div>
                
            </section>
        </div>
    </div>
</div>

