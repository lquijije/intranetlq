<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice12">
                <div class="row">
                    <div class="spa-12" style="margin-bottom: 15px;">
                        <h3 class="text-center text-blanco"><i class="fa fa-check"></i> Delegaci&oacute;n de Autorizaci&oacute;n</h3>
                    </div>
                </div>
                <div class="row">
                    <?php
                        if(!empty($delegados)){
                    ?>
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <table class="table table-bordered TFtable"> 
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Delegar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $checkNadie = 0;
                                            foreach ($delegados as $row){
                                                $check = !empty($row['es_rem']) ? 'checked':'';
                                                $checkNadie+= !empty($row['es_rem']) ? 1:0;
                                                ?>
                                                <tr>
                                                    <td><?php echo $row['no_rem']; ?></td>
                                                    <td class="text-center"><input type="radio" name="op_dele[]" value="<?php echo $row['co_rem']; ?>" <?php echo $check; ?>></td>
                                                </tr>
                                                <?php
                                            }
                                        ?>
                                        <tr class="success">
                                            <td>>>> No delegar a nadie <<<</td>
                                            <td class="text-center"><input type="radio" name="op_dele[]" value="0" <?php echo $checkNadie == 0?'checked':''; ?>></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-danger a_sol"><i class="fa fa-check"></i> Aceptar</button>
                    </div>
                    <?php
                        } else {
                    ?>
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <h3 class="text-center">Sin usuarios de reemplazo</h3>
                                </div>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                </div>
            </section>
        </div>
    </div>
</div>
