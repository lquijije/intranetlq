<style>

    .btn-out {
        height: auto;
        right: 0;
        left: -40px;
        top: 28%;
    }

    .nav-tabs {
        margin-bottom: 0px;
    }

    .btn.btn-circle{
        width: 25px;
        height: 25px;
        line-height: 25px;
    }

    textarea.form-control {
        height: 97px;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align:middle;
    }

    p {
        margin: 0px;
    }
    
    .btn.btn-circle{
        width: 18px;
        height: 18px;
        line-height: 18px;
        font-size: 9px;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
 
            <section class="content invoice">
                <button type="reset" class="btn btn-success save_solche" style="position: absolute; right: 0;margin: 0.2% 0.6%;"><i class="fa fa-save"></i> Grabar</button>
                <div class="row">
                    <div class="spa-4" style="max-width: 23.8%; width: auto;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-ticket"></i> Pagos </div>
                    </div>
                    
                    <div class="spa-8">
                        <?php $attributes = array('id' => 'f_solch','name' => 'f_solch');   
                            echo form_open('procesos/solicitud_cheque/co_109',$attributes); ?>
                        <?php // echo validation_errors(); ?>
                        <ul class="nav navbar-nav">
                            <li class="user-menu">
                                
                                <?php 
                                
                                $data = array(
                                    'cod_emp' => $_SESSION['co_emp'],
                                    'id_emp' => $_SESSION['id_emp']
                                );

                                echo form_hidden($data);
                                
                                    if ($empresas <> 'NN'){

                                        if (isset($empresas) && !empty($empresas)){
                                            $count = count($empresas);
                                            $arreglo= null;
                                            $select_title="";
                                            $select_items="";
                                            for ($i=0; $i<$count; $i++){
                                                if (trim($_SESSION['co_emp']) === trim($empresas[$i]['codi_empr'])) {
                                                   
                                                    if ($count === 1){
                                                        $select_title ='<a href="#" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($empresas[$i]['nomb_empr']).' </span></a><ul class="dropdown-menu">';
                                                    } else {
                                                        $select_title ='<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($empresas[$i]['nomb_empr']).' <i class="caret"></i></span></a><ul class="dropdown-menu">';
                                                    }
                                                    
                                                } else {
                                                    $select_items.="<li><a href='javascript:void(0)' onclick='passEmpr(\"".trim($empresas[$i]['codi_empr'])."\",\"".$i."\")'>".trim($empresas[$i]['nomb_empr'])."</a></li>";
                                                }
                                            }

                                            echo $select_title.$select_items.'</ul>';   

                                            ?> 
                                            <?php
                                        } else {
                                            ?>
                                                <a href="#" style="padding-top: 0px;">
                                                    <span class="titulo-1 text-size-1-5" >No tiene Empresas Asignadas </span>
                                                </a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <script>
                                            var a = document.createElement('div');
                                            a.setAttribute("id","fadeMASTER");
                                            a.setAttribute("class","fadebox");
                                            a.setAttribute("style","z-index:9999");
                                            $("body").append(a);    
                                            $("#fadeMASTER").css('display','block');
                                            errorGeneral('Sistema Intranet','No se encuentra XML MASTER, intente volver a iniciar sesión si no se soluciona comuniquese con sistemas.','danger');
                                        </script>
                                        <?php 
                                    }
                                ?>
                            </li>
                        </ul>
                        </form>
                    </div>
                </div>
                <?php 
                    $attributes = array('id' =>'form_solicitudes_ge','name'=>'form_solicitudes_ge');
                    echo form_open('procesos/solicitud_cheque/co_109',$attributes); 
                ?>  
                <div class="row">
                    <div class="spa-4">
                        <div class="box box-primary"  style="height:331px;">
                            <div class="box-body">
                                <div class="row">
                                    
                                    <div class="separador">
                                        <h4 style="font-weight: bold;">¿A quien se le realiza el Pago? </h4> 
                                        <input type="hidden" id="tip_prov" name="tip_prov" value="">
                                    </div>
                                    <div class="separador line pad_pro">
                                        <label> <i style="width: 20px;" class="fa"><b class=" text-size-1-25">#</b></i><span id="ruc_prov"></span></label>
                                        <button type="button" class="btn btn_oculto btn-circle_1 bg-red pull-right searchProvee"><i class="fa fa-search"></i></button>
                                    </div>
                                    <div class="separador line pad_pro">
                                        <label> <i style="width: 20px;" class="fa fa-user"></i><span id="name_prov"></span></label>
                                    </div>
                                    <div class="separador line pad_pro">
                                        <label> <i style="width: 20px;" class="fa fa-map-marker"></i><span id="dir_prov"></span></label>
                                    </div>
                                    <div class="separador pad_pro">
                                        <label> <i style="width: 20px;" class="fa fa-dollar"></i><span id="bene_prov"></span></label>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="getID" class="spa-8">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div style="position:absolute;right:6%;">
                                        <?php 
                                        
                                        if ($val_sin_aprob) {?>
                                            <label>Sin Aprobaci&oacute;n</label>
                                            <input id="txt_sin_aprob" name="txt_sin_aprob" type="checkbox" style="margin-right: 10px;">
                                        <?php } ?>
                                        <label>Copia</label>
                                        <input id="txt_copia" name="txt_copia" type="checkbox" style="margin-right: 10px;">
                                        <label>Urgente</label>
                                        <input id="txt_urgente" name="txt_urgente" type="checkbox">
                                    </div>
                                    <div class="separador">
                                        <h4 style="font-weight: bold;">Documento
                                        <a href="javascript:void(0)" class="link doc_pen">(Ver Pendientes)</a>
                                        </h4>
                                    </div>
                                    
                                    <div class="spa-6">
                                        
                                        <?php if ($o_c === 1){?>
                                            <div class="separador">
                                        <?php } else {?>
                                            <div class="separador" style="display: none;">
                                        <?php } ?>
                                            <div class="spa-4">
                                               <label>O.Compra</label>
                                               
                                            </div>
                                            <div class="spa-8">
                                                <div class="input-group">
                                                    <input type="text" id="txt_num_oc" name="txt_num_oc" class="form-control input-sm" value="">
                                                    <div class="input-group-addon" style="padding:0;">
                                                        <button type="button" class="btn btn_oculto" onclick="consulOrdCmp('<?php echo $_SESSION['co_emp']; ?>',<?php echo $_SESSION['id_emp_sip']; ?>,$('#txt_num_oc').val().toString().trim())"><i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="separador">
                                            
                                            <div class="spa-4">
                                               <label>Tipo</label>
                                            </div>
                                            <div class="spa-8">
                                                <?php echo $cmb_tip_doc ?>
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-4">
                                               <label>Numero</label>
                                            </div>
                                            <div class="spa-8">
                                                <div class="input-group">
                                                    <input type="text" id="txt_num_doc" name="txt_num_doc" class="form-control input-sm" value="" onkeypress="return isNumero(event,this)">
                                                    <div class="input-group-addon" style="padding:0;">
                                                        <button type="button" class="btn btn_oculto"><i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                
                                        <div class="separador txt_aut_sri_fa">
                                            <div class="spa-4">
                                               <label>Autorizaci&oacute;n SRI</label>
                                            </div>
                                            <div class="spa-8">
                                                <input type="text" id="txt_aut_sri" name="txt_aut_sri" class="form-control input-sm" value="" readonly="readonly">
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-4">
                                               <label>Fecha</label>
                                            </div>
                                            <div class="spa-8">
                                                <div class="input-group">
                                                    <input type="text" id="txt_fec_doc" name="txt_fec_doc" class="form_datetime form-control input-sm" value="" readonly="readonly">
                                                    <div class="input-group-addon" style="padding:0;">
                                                        <button type="button" class="btn btn_oculto"><i class="fa fa-calendar"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            
                                            <div class="spa-4">
                                               <label>Tipo Egreso</label>
                                            </div>
                                            <div class="spa-8">
                                                <?php echo $cmb_tip_egre ?>
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-4">
                                               <label>Concepto</label>
                                            </div>
                                            <div class="spa-8">
                                                <input id="txt_concep_doc" name="txt_concep_doc" type="text" class="form-control input-sm" value="">
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-4">
                                               <label>Observación</label>
                                            </div>
                                            <div class="spa-8">
                                                <textarea id="txt_obs_doc" name="txt_obs_doc" class="form-control input-sm"></textarea>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="spa-6">
                                        <div class="separador">
                                            <div class="spa-5">
                                               <label>Subtotal:</label>
                                            </div>
                                            <div class="spa-6">
                                                <input id="txt_subt_doc" name="txt_subt_doc" type="text" class="form-control input-sm derecha" onkeypress="return isDecimal(event,this)" value="">
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-5">
                                               <label>Adicionales:</label>
                                            </div>
                                            <div class="spa-6">
                                                <input id="txt_adic_doc" name="txt_adic_doc" type="text" class="form-control input-sm derecha" onkeypress="return isDecimal(event,this)" value="">
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-3">
                                               <label>Descuento:</label>
                                            </div>
                                            <div class="spa-2">
                                                <input id="txt_porc_desc_doc" name="txt_porc_desc_doc" type="text" class="form-control input-sm derecha" onkeypress="return isNumero(event,this)" value="">
                                            </div>
                                            <div class="spa-6">
                                                <input id="txt_desc_doc" name="txt_desc_doc" type="text" class="form-control input-sm derecha" onkeypress="return isDecimal(event,this)" value="" readonly="">
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-5">
                                               <label>Base Imponible:</label>
                                            </div>
                                            <div class="spa-6">
                                                <input id="txt_base_imp_doc" name="txt_base_imp_doc" type="text" class="form-control input-sm derecha" onkeypress="return isDecimal(event,this)" value="" readonly="">
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-5">
                                               <label>Varios:</label>
                                            </div>
                                            <div class="spa-6">
                                                <input id="txt_vari_doc" name="txt_vari_doc" type="text" class="form-control input-sm derecha" onkeypress="return isDecimal(event,this)" value="">
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-3">
                                               <label>Impuesto:</label>
                                            </div>
                                            <div class="spa-2">
                                                <select id="cmb_impu_doc" name="cmb_impu_doc" class="form-control input-sm">
                                                    <option value="0">0</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="spa-6">
                                                <input id="txt_res_impu_doc" name="txt_res_impu_doc" type="text" class="form-control input-sm derecha" onkeypress="return isDecimal(event,this)" value="" readonly="">
                                            </div>
                                        </div>
                                        
                                        <div class="separador">
                                            <div class="spa-5">
                                               <label>Total:</label>
                                            </div>
                                            <div class="spa-6">
                                                <input id="txt_tot_doc" name="txt_tot_doc" type="text" class="form-control input-sm derecha" onkeypress="return isDecimal(event,this)" value="" readonly="">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="spa-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    
                                    <div class="spa-7">
                                        <h4 style="font-weight: bold;">Información Contable</h4>
                                        <div class="separador">
                                            <div class="spa-2">
                                               <label>Plan De Impuesto</label>
                                            </div>
                                            <div class="spa-3-5">
                                                <select id="cmb_plan_imp" name="cmb_plan_imp" class="form-control input-sm"></select>
                                            </div>
                                        </div>
                                        
                                        <div class="separador" style="padding:5px; margin-top: 5px; overflow-y: auto; height:335px; background: #FFF;">
                                            <table id="cuenta_contable" class="table table-bordered TFtable">
                                                <thead>
                                                    <tr>
                                                        <th width="20%">Cuenta</th>
                                                        <th width="40"></th>
                                                        <th width="10%">Tipo</th>
                                                        <th width="25%">Valor</th>
                                                        <th width="5%">
                                                            <button type="button" class="btn btn_oculto btn-circle_1 bg-orange" onclick="pasteModal()" value="convert"><i class="fa fa-paste"></i></button>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="table_cuentas">
                                                </tbody>
                                                <tfoot id="cuenta_total">
                                                    
                                                </tfoot>
                                            </table>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="spa-5">
                                        
                                        <div class="spa-12" style="margin: 15px 4px; padding: 10px;border-radius: 15px; border: 8px solid #CCC;">
                                            
                                            <div class="separador">
                                                <div class="spa-12">
                                                    <label>Tipo</label>
                                                    <select id="cmb_tipo_cont" name="cmb_tipo_cont" class="form-control input-sm">
                                                        <option value="S">Subtotal</option>
                                                        <option value="V">Varios</option>
                                                    </select>
                                                </div>
                                                      
                                            </div>
                                            
                                            <div class="separador" style="margin-top: 10px; background:#FFF;">
                                                <section>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active"><a href="#tab1" data-toggle="tab" class="tip_c_1" onclick="$('#tip_cuent').val(1)">Gasto</a></li>
                                                        <li><a href="#tab2" data-toggle="tab" onclick="$('#tip_cuent').val(2)">Activo / Pasivo</a></li>
                                                        <input id="tip_cuent" name="tip_cuent" type="hidden" value="1">
                                                    </ul>

                                                    <div class="tab-content" style="padding: 10px;">

                                                        <div class="tab-pane active" id="tab1">
                                                            <div class="spa-12">
                                                                <label>Centros de Costo</label>
                                                                <?php echo $cmb_cen_cost ?>
                                                            </div>
                                                            
                                                            <div class="spa-12">
                                                                <label>Cuentas</label>
                                                                <select id="cmb_cuentas" class="form-control"></select>
                                                            </div>
                                                            
                                                            <div class="spa-4">
                                                                <label>Monto</label>
                                                                <input id="txt_mont_1" name="txt_mont_1" type="text" onkeypress="return isDecimal(event,this)" class="form-control imput-sm">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="tab-pane " id="tab2">
                                                            
                                                            <div class="spa-12">
                                                                <label>Activo Pasivo</label>
                                                                <?php echo $cmb_activoPasivo; ?>
                                                            </div>
                                                            
                                                            <div class="spa-4">
                                                                <label>Monto</label>
                                                                <input id="txt_mont_2" name="txt_mont_2" type="text" onkeypress="return isDecimal(event,this)" class="form-control imput-sm">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="clearfix"></div>
                                                        
                                                    </div>
                                                </section>
                                            </div>
                                            
                                        </div>
                                        <div class="spa-12" style="position: relative; padding-left: 2px;">
                                            <button type="button" class="btn btn-success pass_cuent"><i class="fa fa-chevron-left"></i> Adicionar</button>
                                            
                                            <?php  if(isset($_SESSION['co_emp']) && trim($_SESSION['co_emp']) == 'PYCCA'){ ?>
                                            
                                                <button type="button" class="btn btn-danger pass_dis_m2"><i class="fa fa-chevron-left"></i> Distrib. x m2</button>
                                                <button type="button" class="btn btn-warning pass_var_cc"><i class="fa fa-chevron-left"></i> Varios C.C.</button>

                                            <?php } ?>
                                                
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="spa-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    
                                    <div class="separador">
                                        <h4 style="font-weight: bold;">Plan de Pago</h4>
                                    </div>

                                    <div class="separador">
                                        <div class="spa-6">
                                            <label>Fecha de Pago</label>
                                            <div class="input-group">
                                                <input type="text" id="txt_fec_plg" name="txt_fec_plg" class="form_datetime form-control" value="" readonly="">
                                                <div class="input-group-addon" style="padding:0;">
                                                    <button type="button" class="btn btn_oculto"><i class="fa fa-calendar"></i></button>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="spa-4">
                                            <label>Porcentaje de Pago</label>
                                            <input class="form-control" id="txt_por_plg" name="txt_por_plg" onkeypress="return isNumero(event,this)" value="">
                                        </div>
                                        
                                        <div class="spa-2">
                                            <label class="out-text">Ingresar</label>
                                            <button type="button" class="btn btn_oculto btn-square bg-red add_option_pago"  value="convert"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    
                                    <div class="separador" style="padding-top: 5px;">
                                        <table id="plan_de_pago" class="table table-bordered TFtable">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>%</th>
                                                    <th width="5%"></th>
                                                </tr>
                                            </thead>
                                            <tbody id="table_plan_pago">
                                                <tr>
                                                    <td colspan="3" class="centro">Ingrese el Plan de Pago</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="spa-8">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    
                                        <div class="separador">
                                            <h4 style="font-weight: bold;">Autorización</h4>
                                        </div>

                                        <div class="separador">
                                               <label>Grupo De Autorizadores</label>
                                        </div>
                                        
                                        <div class="separador" style="padding:5px;">
                                            <table class="table table-bordered TFtable">
                                                <thead>
                                                    <tr>
                                                        <th width="5%"></th>
                                                        <th>Grupo</th>
                                                        <th>Orden</th>
                                                        <th>Nombre</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (!empty($autorizadores)){
                                                        
                                                        $result = groupArray($autorizadores,'cod_gru');
                                                        $salto = '';
                                                        $i = 0;
                                                        
                                                        foreach ($autorizadores as $row){ 
                                                            if ($salto <> trim((int)$row['cod_gru'])){ ?>
                                                            <tr>
                                                                <td class="centro" rowspan="<?php echo count($result[$i]['groupeddata']); ?>" style="background: #FFF;"><input type="radio" name="cod_autoriza" id="<?php echo $row['cod_gru']; ?>"/></td>
                                                                <td class="centro" rowspan="<?php echo count($result[$i]['groupeddata']); ?>" style="background: #FFF;"><?php echo $row['cod_gru']; ?></td>
                                                                <?php $i++;
                                                            }
                                                            ?>
                                                                <td><?php echo $row['sec_usu']; ?></td>
                                                                <td><?php echo $row['nom_emp']; ?></td>
                                                            </tr>
                                                    
                                                            <?php 
                                                    
                                                            $salto = trim((int)$row['cod_gru']);
                                                            }
                                                    } else { ?>
                                                            <tr>
                                                                <td colspan="4">No tiene asignado Autorizadores</td>
                                                            </tr>
                                                    <?php } ?> 
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <form>
            </section>
            
        </div>
    </div>
</div>

<div class="modal" id="modalPaste">
    <div class="modal-dialog">

            <div class="modal-header bg-orange">
            <button type="button" onclick="quitarmodalGeneral('Paste','form_Paste');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Pegado Especial de Cuentas</h3>
            </div>
        </form>
            <?php 
                $attributes = array('id' =>'form_Paste','name'=>'form_Paste');
                echo form_open('procesos/solicitud_cheque/co_109',$attributes); 
            ?>   

            <div class="modal-body">

                <div class="spa-12">
                    <label>Ingrese sus cuentas</label>
                    <textarea id="textarea_copy" class="form-control" style="height: 200px;"></textarea>
                </div>
                
                <div class="clearfix"></div>
            </div>
            
            <div class="modal-footer">
                <button type="reset" class="btn btn-success convertText">Convertir</button>
            </div>

            <?php echo form_close(); ?>

    </div>
</div>

<div class="modal" id="modalProvee">
  <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="exitModal();" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Proveedores</h3>
        </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' =>'form_Provee','name'=>'form_Provee');
            echo form_open('procesos/solicitud_cheque/co_109',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div class="separador line" style="padding-bottom: 15px;margin-bottom: 15px;">
                
                <div class="spa-3">
                    <label>Ruc</label>
                    <input id="txt_search_ruc" name="txt_search_ruc" type="text" value="" class="form-control input-sm">
                </div>
                
                <div class="spa-3">
                    <label>Nombre</label>
                    <input id="txt_search_name" name="txt_search_name" type="text" value="" class="form-control input-sm">
                </div>

                <div class="spa-2">
                    <label class="out-text">Buscar</label>
                    <button type="button" class="btn btn-primary filtroProvee"><i class="text-red fa fa-search"></i> Buscar</button>
                </div>

            </div>
            
            <div class="separador">
                <div id="tableProveed" style="margin-left: 15px;margin-right: 15px;">
                    <table id="T_regProveed" class="table table-bordered table-hover">
                    <thead>
                    <th>Ruc</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Correo</th>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
            
                                
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalDocPen">
  <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('DocPen','form_DocPen');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Seleccione la Factura a pagar</h3>
        </div>
    <div class="modal-content">
        
        <?php 
            $attributes = array('id' =>'form_DocPen','name'=>'form_DocPen');
            echo form_open('procesos/solicitud_cheque/co_109',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div class="separador" style="padding-bottom: 15px;">
                
                <div id="tableDocPen" style="margin-left: 15px;margin-right: 15px;">
                    <table id="T_DocPen" class="table table-bordered table-hover">
                    <thead>
                        <th>Fecha Emisión</th>
                        <th>Numero Documento</th>
                        <th>Estado</th>
                        <th>Autorización</th>
                        <th>Total</th>
                        <th></th>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
            
                                
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalViewDoc">
  <div class="modal-dialog">
        
      <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('ViewDoc','form_ViewDoc');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="name_pro"></span> (FACTURA# <span id="num_doc_fac"></span>)</h3>
        </div>
        <div class="modal-content">
            <?php 
                $attributes = array('id' =>'form_ViewDoc','name'=>'form_ViewDoc');
                echo form_open('procesos/solicitud_cheque/co_109',$attributes); 
            ?>   

            <div class="modal-body">
                
                <div class="spa-12">
                    <table class="table table-bordered">
                        <tbody id="cont_cab">

                        </tbody>
                    </table>
                </div>

                <div class="spa-12">

                    <table class="table table-bordered TFtable">
                        <thead>
                            <tr>
                                <th class="centro" colspan="6">DETALLE DE PRODUCTOS</th>
                            </tr>
                            <tr>
                                <th>C&oacute;digo</th>
                                <th>Descripci&oacute;n</th>
                                <th>Cantidad</th>
                                <th>Precio Unitario</th>
                                <th>Descuento</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>

                        <tbody id="cont_det_pro">

                        </tbody>
                    </table>

                </div>
                
                <div class="spa-5" style="float: right;">

                    <table class="table table-bordered TFtable">
                        <thead>

                            <tr>
                                <th class="centro" colspan="3">IMPUESTOS</th>
                            </tr>

                            <tr>
                                <th>Descripci&oacute;n</th>
                                <th>Base Imponible</th>
                                <th>Monto</th>
                            </tr>

                        </thead>

                        <tbody id="cont_imp">

                        </tbody>
                    </table>

                    <table class="table table-bordered">
                        <tbody>
                            <td class="centro"><b>Total</b></td>
                            <td class="derecha" id="cont_total"></td>
                        </tbody>
                    </table>
                    <div id="btn_doc">
                        
                    </div>
                </div>
                
                <div class="clearfix"></div>
            </div>
            
            <div class="clearfix"></div>
            <?php echo form_close(); ?>
        </div>
        
  </div>
</div>
    
<div class="modal" id="modalDisM2">
  <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('DisM2','form_DisM2');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Distribuci&oacute;n entre Almacenes</h3>
        </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' =>'form_DisM2','name'=>'form_DisM2');
            echo form_open('procesos/solicitud_cheque/co_109',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div class="separador line" style="padding-bottom: 15px;margin-bottom: 15px;">
                
                <div class="spa-3">
                    <label>Monto a distribuir</label><br>
                    <label style="line-height: 25px; font-weight: bold;" class="text-size-2">$ <span class="mont_dis"></span></label>
                </div>
                
                <div class="spa-3 out_fa">
                    <label>Forma de ditribuci&oacute;n</label>
                    <select class="form-control sm-input" id="cmb_for_dis">
                        <option value="NNN">Seleccionar</option>
                        <option value="M">Metro Cuadrado</option>
                        <option value="P">Porcentaje</option>
                    </select>
                </div>
                
                <div class="spa-3 out_fa" style="display: none;">
                    <label>Tipo Almac&eacute;n</label>
                    <select class="form-control sm-input" id="cmb_tip_alm">
                        <option value="NNN">Seleccionar</option>
                        <option value="G">123 Grande</option>
                        <option value="P">123 Peque&ntilde;o</option>
                        <option value="A">Almac&eacute;n</option>
                    </select>
                </div>

                <div class="spa-2 out_fa">
                    <label class="out-text">Calcular</label>
                    <button type="button" class="btn btn-primary _dis_almc"><i class="text-red fa fa-calculator"></i> Calcular</button>
                </div>
                
            </div>
            
            <div class="separador">
                <div id="tabledisxm2">
                    
                </div>
                
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>
    
<div class="modal" id="modalCenCos">
  <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('CenCos','form_CenCos');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Distribuci&oacute;n entre Almacenes</h3>
        </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' =>'form_CenCos','name'=>'form_CenCos');
            echo form_open('procesos/solicitud_cheque/co_109',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div class="separador line" style="padding-bottom: 15px;margin-bottom: 15px;">
                
                <div class="spa-3">
                    <label>Monto a distribuir</label><br>
                    <label style="line-height: 25px; font-weight: bold;" class="text-size-2">$ <span class="mont_dis"></span></label>
                </div>
                
                <div class="spa-2 out_fa">
                    <label class="out-text">Buscar</label>
                    <button type="button" class="btn btn-primary _dis_cecos"><i class="text-red fa fa-calculator"></i> Calcular</button>
                </div>
                
            </div>
            
            <div class="separador">
                <div id="tablecencos">
                    
                </div>
                
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<script>

$( "#cmb_cen_cost" ).change(function() {
    if (this.value.trim() !== 'NNN'){
        var name = ($("#cmb_cen_cost option:selected").text());
        var url1 = $('#txt_urlGe').val();
        var datos = "c_c="+this.value+"&c_e="+$("#cod_emp").val();
        dataLoading();
        $.ajax({
            type : 'POST',
            url : url1+"procesos/solicitud_cheque/co_109/retCuentas", 
            data: datos,
            success : function (returnData) { 
                removedataLoading();     
                
                var data = eval('('+returnData+')'); 
                $('#cmb_cuentas').html('');
                
                if(data){
                    var cmb = '';
                    $.each(data.cuentas,function(a,dato){
                        cmb = cmb+'<option value="'+dato.cta_myor+'-'+dato.codi_ceco+'-'+dato.cta_scta+'">'+dato.actdescr+'</option>';
                    });
                    
                    $('#cmb_cuentas').append(cmb);
                    
                    $('#cmb_cuentas').select2({ 
                        width: '100%',
                        placeholder: 'Seleccionar Cuenta'
                    });

                } else {
                    errorGeneral("Sistema Intranet",'No existen cuentas asignadas a el centro de Costo '+name,'danger'); 
                }
            }
        });
        
    } else {
        
        $('#cmb_cuentas').html('');
        
        $('#cmb_cuentas').select2({ 
            width: '100%',
            placeholder: 'No hay cuentas'
        });

    }
});    

</script>

