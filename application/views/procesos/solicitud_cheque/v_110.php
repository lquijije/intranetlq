
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .dashboard {
        padding: 5em 3em;
    }
    .input-sm {
        height:25px;
    }
    .spa-3,.spa-4 {
        text-align: right;
        font-weight:bold;
    }
    .spa-1-5 {
        width: 10.8%;
    }
    .sta {
        color:#FFF;
        background: none repeat scroll 0% 0% #2E8BEF !important;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-check"></i> Aprobaci&oacute;n de Solicitud de Cheque</div>
                    </div>
                </div>
                
                <div class="row" style="margin-top: 10px;">
                    <div class="spa-4" style="text-align:left !important;">
                        
                    <div class="box box-primary">
                        <div class="box-body" style="padding: 12px;">
                            <div class="row">

                                <div class="spa-12">
                                    <label>Empresa</label>
                                    <?php echo $cmb_empr ?>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    </div>
                    
                    <?php  if(!empty($reemplazo)){ ?>
                        <div class="spa-4" style="text-align:left !important;">
                            <div class="box box-primary" style="height: 142px;">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="spa-12">
                                            <h4>Usted está reemplazando a:</h4>
                                            <ul>
                                                <?php foreach ($reemplazo as $row){ ?>
                                                    <li><?php echo $row['no_remp'] ?></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    
                </div>
                
                <div class="row">
                    <div class="spa-12">
                        <div class="box box-primary">
                        <div class="box-body">
                            <div id="table_solic">
                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="spa-12">
                        <div class="pull-right">
                            <button type="button" class="btn btn-success apr_soli"><i class="fa fa-check"></i> Procesar</button>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalDetallSolic">
  <div class="modal-dialog">
    <div class="modal-header bg-orange">
        <button type="button" onclick="quitarmodalGeneral('DetallSolic','form_DetallSolic');" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span id="titleDoc"></span></h3>
    </div>

    <div class="modal-content">
        
        <?php 
            $attributes = array('id' => 'form_DetallSolic','name'=>'form_DetallSolic');
            echo form_open('procesos/solicitud_cheque/co_110',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div id="view_Soli_cab">
                
            </div>
            
            <div class="clearfix"></div>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<script>
//    $('#bodyPage').css('background','url(../../img/real-madrid-wallpaper-7.jpg) top left no-repeat');
//    $('#bodyPage').css('background-attachment','fixed');
//    $('#bodyPage').css('background-size','100%');
</script>