
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }
    table {
      border-collapse: collapse;
      width: auto;
      -webkit-overflow-scrolling: touch;
      -webkit-border-horizontal-spacing: 0;
    }
    th,
    td {
      min-width: 3%;
      word-wrap: break-word;
      word-break: break-all;
      -webkit-hyphens: auto;
      hyphens: auto;
    }
    thead tr {
      display: block;
      position: relative;
    }
    tbody {
      display: block;
      overflow: auto;
      min-height: 50px;
      height: 400px;
    }

    .TFtable tr:nth-child(2n+1) {
        background: #FFF;
    }

</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice8">
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-calendar"></i> Procesar Turnos </div>
                    </div>
                </div>
                <div class="row">
                    <div class="spa-12">
                        <?php 
                            $attributes = array('id' =>'form_fech','name'=>'form_fech');
                            echo form_open('procesos/nomina/co_126',$attributes); 
                            echo $cmb_fec; 
                            echo form_close(); 
                        ?>
                        <button type="button" class="btn-out ref_pag" title="Refrescar"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
                <div class="row">
                    <div class="table_container spa-12" style="margin-top: 10px;">
                        <table class="table table-bordered TFtable" style="margin-bottom: 10px;">
                            <thead>
                            <tr>
                                <th colspan="2" style="border: medium none;">Almac&eacute;n</th>
                                <th style="border: medium none;">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $i = 0;
                                $a = 0;
                                $b = 0;
                                if(isset($ccosto) && !empty($ccosto)){
                                    foreach($ccosto as $row){
                                        //print_r($row);
                                        $i++;
                                        $activ = '';
                                        trim($row['st_yle']) == '' || trim($row['st_yle']) == 'warning' ?$a++:0;
                                        trim($row['tx_cur']) == 'PROCESADO' ?$b++:0;
                                        ?>
                                            <tr id="<?php echo trim($row['co_cco']); ?>" class="<?php echo trim($row['st_yle']); ?>">
                                                <td style="width: 3%;"><?php echo $i; ?></td>
                                                <td class="nom_alm" style="width: 20%;"><?php echo $row['ds_bdg']; ?></td>
                                                <td style="width: 20%;"><?php echo $row['tx_cur']; ?></td>
                                                <td style="width: 2%;"><center><button type="button" onclick="viewPersonTurn(<?php echo trim($row['co_cco']); ?>);" class="btn btn-circle btn-success"><i class="fa fa-plus"></i></button></center></td>
                                                <td style="width: 2%;">
                                                    <?php
                                                    
                                                        if(trim($row['st_yle']) == '' || trim($row['st_yle']) == 'warning'){
                                                            ?>
                                                            <center>
                                                                <input type="checkbox" id="ck_<?php echo trim($row['co_cco']); ?>" name="option_tur[]" value="<?php echo trim($row['co_cco']); ?>">
                                                            </center>
                                                            <?php 
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php 
                                    }
                                } else {
                                    ?>
                                        <tr>
                                            <td><div> Sin datos</div></td>
                                        </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                    </div>
                </div>
                <div class="row">
                    <div class="spa-6">
                        <div class="titulo-1 text-size-1-25 text-blanco" style="margin-bottom: 2px;"># Almacenes <span id="count_cola"><?php echo $i; ?></span> </div>
                        <div class="titulo-1 text-size-1-25 text-blanco"># Almacenes procesados <span id="count_asig"><?php echo $b; ?></span> </div>
                    </div>
                    <div class="spa-6">
                        <div class="pull-right">
                            <?php
                            if($a > 0){
                                ?>
                                    <input type="checkbox" id="ck_mail" name="ck_mail"><span style="color:#FFF;"> Desea enviar por Mail</span>
                                    <button type="button" class="btn btn-danger pro_t"><i class="fa fa-send"></i> Procesar Turnos</button>
                                <?php 
                            }
                            
                            if($b > 0){
                                ?>
                                    <button type="button" class="btn btn-success tu_print">Imprimir <i class="text-blanco fa fa-print"></i></button>
                                <?php 
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        
    </div>
</div>

<div class="modal" id="modalDetTurn">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" class="close" onclick="closeMoTurno('DetTurn','');">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Almac&eacute;n <span id="id__turn"></span></h3>
        </div>
        
        <div class="modal-content">
           
            <div class="modal-body">
                
                <div class="spa-12">
                    <table class="table table-bordered TFtable">
                        <thead>
                            <tr>
                                <th colspan="2" style="border: medium none;">C&oacute;digo</th>
                                <th style="border: medium none;">Nombres</th>
                                <th style="border: medium none;">Cargo</th>
                                <th style="border: medium none;">Turno</th>
                            </tr>
                        </thead>
                        <tbody id="tab_turns">
                        </tbody>
                    </table>
                </div>
                
                <div class="spa-12">
                    <div class="spa-6">
                        <div class="titulo-1 text-size-1-25" style="margin-bottom: 2px;"># De Colaboradores <span id="count_colaModal">0</span> </div>
                        <div class="titulo-1 text-size-1-25"># De Turnos Asigandos <span id="count_asigModal">0</span> </div>
                    </div>
                    <div class="spa-6">
                        <div class="pull-right fg">
                            
                        </div>
                    </div>
                </div> 

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>
    </div>
</div>

