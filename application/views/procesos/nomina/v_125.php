
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .btn-circle{
        width: 23px !important;
        height: 23px !important;
        line-height: 23px !important;
    }

/*table {
  border-collapse: collapse;
  width: auto;
  -webkit-overflow-scrolling: touch;
  -webkit-border-horizontal-spacing: 0;
}
th,
td {
  min-width: 80px;
  word-wrap: break-word;
  word-break: break-all;
  -webkit-hyphens: auto;
  hyphens: auto;
}
thead tr {
  display: block;
  position: relative;
}
tbody {
  display: block;
  overflow: auto;
  min-height: 50px;
  height: 400px;
}*/

.TFtable tr:nth-child(2n+1) {
    background: #FFF;
}

</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                <div class="row">
                    <div class="spa-5" style="width: auto;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-calendar"></i> Turnos </div>
                    </div>
                     
                        <div class="spa-7">
                            <?php echo form_open('procesos/nomina/co_125',array('id' => 'f_turnos','name' => 'f_turnos')); ?>
                            <ul class="nav navbar-nav">
                                <li class="user-menu">

                                    <?php 

                                    $data = array(
                                        'co_bdg' => $_SESSION['co_alm_125'],
                                        'co_cco' => $_SESSION['co_cco_125'],
                                        'fe_in' => $fechas['fe_des'],
                                        'fe_fi' => $fechas['fe_has']
                                    );

                                    echo form_hidden($data);

                                        if (isset($ccosto) && !empty($ccosto)){
                                            $count = count($ccosto);
                                            $arreglo= null;
                                            $select_title="";
                                            $select_items="";
                                            for ($i=0; $i<$count; $i++){
                                                if (trim($_SESSION['co_alm_125']) === trim($ccosto[$i]['co_bdg'])) {

                                                    if ($count === 1){
                                                        $select_title ='<a href="javascript:void(0)" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($ccosto[$i]['ds_bdg']).' </span></a><ul class="dropdown-menu">';
                                                    } else {
                                                        $select_title ='<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.trim($ccosto[$i]['ds_bdg']).' <i class="caret"></i></span></a><ul class="dropdown-menu">';
                                                    }

                                                } else {
                                                    $select_items.="<li><a href='javascript:void(0)' onclick='passEmpr(\"".trim($ccosto[$i]['co_bdg'])."\",\"".trim($ccosto[$i]['co_cco'])."\")'>".trim($ccosto[$i]['ds_bdg'])."</a></li>";
                                                }
                                            }

                                            echo $select_title.$select_items.'</ul>';   

                                            ?> 
                                            <?php
                                        } else {
                                            ?>
                                                <a href="#" style="padding-top: 0px;">
                                                    <span class="titulo-1 text-size-1-5" >No tiene Almacenes Asignados </span>
                                                </a>
                                            <?php
                                        }

                                    ?>
                                </li>
                            </ul>
                            </form>
                        </div>
                    
                </div>
                
                <?php if($bandera){ ?> 
                    <?php if($fecActual){ ?>
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="spa-12">
                                <div class="titulo-1 text-size-1-25 text-blanco">Desde el <?php echo date('d',strtotime($fechas['fe_des']))."/".conveMes(date('m',strtotime($fechas['fe_des'])))."/".date('Y',strtotime($fechas['fe_des'])) ?> Hasta el <?php echo date('d',strtotime($fechas['fe_has']))."/".conveMes(date('m',strtotime($fechas['fe_has'])))."/".date('Y',strtotime($fechas['fe_has'])) ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <!--<pre>-->
                            <?php 

                                $totGeCola = 0;
                                $banderaUl = false;
                                $banderaAc = false;

                                if(isset($turnosActuales) && !empty($turnosActuales)){

                                    ?>
                                    <div class="separador">
                                        <!--<pre>-->
                                    <?php 

                                    for($i = 0; $i < count($turnosActuales); $i++){

    //                                    echo '<pre>';
    //                                    echo count($turnosActuales[$i]['groupeddata']);
    //                                    print_r($turnosActuales[$i]['groupeddata']);
    //                                    echo '</pre>';
                                        $Es = 'S';
                                        $class = 'btn-warning';
                                        $fa = 'fa-check-circle';
                                        $colabo = 0;
                                        $process = 0;

                                        for($o = 0; $o < count($turnosActuales[$i]['groupeddata']); $o++){

                                            $colabo++;
                                            if((int)$turnosActuales[$i]['groupeddata'][$o]['em_proc'] == 1){
                                                $process++;
                                            }

                                        }
                                        //echo "Cola(".$colabo."=".$process.")<br>";
                                        if($colabo == $process){
                                            $class = 'btn-success';
                                            $fa = 'fa-check';
                                            $Es = 'P';
                                        }

                                        $totGeCola += count($turnosActuales[$i]['groupeddata']);
                                        ?>
                                            <div class="spa-3">
                                                <div class="box box-primary">
                                                    <div class="box-body">
                                                        <div class="separador" style="margin-bottom: 10px;">
                                                            <button type="button" onclick="viewPersonTurn('<?php echo $Es; ?>',<?php echo (int)$turnosActuales[$i]['co_turn']; ?>,'<?php echo htmlentities(json_encode($turnosActuales[$i]['groupeddata'])); ?>')" class="btn <?php echo $class; ?>" style="position: absolute;right: 0;top: 0"><i class="text-blanco fa <?php echo $fa; ?>"></i></button>
                                                            <h3 class="text-center" style="margin-bottom: 10px;">Turno <?php echo (int)$turnosActuales[$i]['co_turn']; ?></h3>
                                                            <select id="cmb_turn_a_<?php echo trim($turnosActuales[$i]['co_turn']); ?>" class="form-control <?php echo $Es <> 'P'?'cmb_upt':''; ?>" <?php echo $Es == 'P'?'disabled':''; ?>>
                                                                <?php 
                                                                    if(isset($turnos) && !empty($turnos)){
                                                                        foreach ($turnos as $tur){
                                                                            $selec = '';
                                                                            if($turnosActuales[$i]['co_turn'] == $tur['co_turn']){
                                                                                $selec = 'selected';
                                                                            }
                                                                            ?>
                                                                                <option value="<?php echo $tur['co_turn'] ?>" <?php echo $selec; ?>><?php echo $tur['ds_turn'] ?></option> 
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="separador">
                                                            <div class="titulo-1 text-size-1" style="margin-bottom: 2px;">Colaboradores: <span id="count_cola"><?php echo count($turnosActuales[$i]['groupeddata']); ?></span> </div>
                                                            <!--<div class="titulo-1 text-size-1">Turnos Asigandos: <span id="count_asig"><?php echo count($turnosActuales[$i]['groupeddata']); ?></span> </div>-->
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php 
                                    }
                                    
                                    ?>
                                        <!--</pre>-->
                                    </div>
                                    <?php 
                                } else {
                                    $banderaAc = true;
                                }

                                /*----------------------------------------------------------------------*/
                                /*----------------------------------------------------------------------*/

                                if(isset($turnosUltimos) && !empty($turnosUltimos)){
                                    ?>
                                    <div class="separador">
                                        <!--<pre>-->
                                    <?php 


                                    for($i=0;$i < count($turnosUltimos); $i++){

                                        $cFal = $tFal = 0;
                                        $arrayUltimos= null;

                                        for($a=0; $a < count($turnosUltimos[$i]['groupeddata']); $a++){
                                            if($turnosUltimos[$i]['groupeddata'][$a]['co_turn'] == 0){
                                                $cFal++;
                                                $arrayUltimos[] = $turnosUltimos[$i]['groupeddata'][$a];
                                            }
                                            $tFal++;
                                        }
                                        //echo $cFal." - ".$tFal."<br>";
                                        //echo $row['ul_turn'];
                                        //print_r($arrayUltimos);
                                        //print_r($row['groupeddata']);

                                        $totGeCola += $cFal;
                                        if ($cFal > 0){
                                        ?>
                                            <div class="spa-3">
                                                <div class="box box-primary">
                                                    <div class="box-body">
                                                        <div class="separador" style="margin-bottom: 10px;">
                                                            <button type="button" onclick="viewPersonTurn('U',<?php echo (int)$turnosUltimos[$i]['ul_turn']; ?>,'<?php echo htmlentities(json_encode($arrayUltimos)); ?>')" class="btn btn-danger" style="position: absolute;right: 0;top: 0"><i class="text-blanco fa fa-bars"></i></button>
                                                            <h3 class="text-center" style="margin-bottom: 10px;">Turno <?php echo (int)$turnosUltimos[$i]['ul_turn']; ?></h3>
                                                            <select id="cmb_turn_u_<?php echo trim($turnosUltimos[$i]['ul_turn']); ?>" class="form-control cmb_upt">
                                                                <?php 
                                                                    if(isset($turnos) && !empty($turnos)){
                                                                        foreach ($turnos as $tur){
                                                                            $selec = '';
                                                                            if($turnosUltimos[$i]['ul_turn'] == $tur['co_turn']){
                                                                                $selec = 'selected';
                                                                            }
                                                                            ?>
                                                                                <option  value="<?php echo $tur['co_turn'] ?>" <?php echo $selec; ?>><?php echo $tur['ds_turn'] ?></option> 
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="separador">
                                                            <div class="titulo-1 text-size-1" style="margin-bottom: 2px;">Colaboradores: <span id="count_cola"><?php echo $cFal; ?></span> </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php 
                                        }
                                    }
                                    ?>
                                        <!--</pre>-->
                                    </div>
                                    <?php 
                                } else {
                                    $banderaUl = true;
                                }

                                if($banderaAc == true && $banderaUl == true){
                                    ?>
                                        <div class="separador">
                                            Sin Datos
                                        </div>
                                    <?php
                                } else {
                                    ?>
                                        <div class="separador">
                                            <div class="spa-12">
                                                <div class="titulo-1 text-size-1-25 text-blanco" style="margin-bottom: 10px;">Total de colaboradores: <?php echo $totGeCola; ?></div>
                                                <div class="spa-2">
                                                    <button type="button" class="btn btn-success"><i class="text-blanco fa fa-check" style="width: 12px;"></i> </button> <span class="text-blanco"> Procesados</span>
                                                </div>
                                                <div class="spa-2">
                                                    <button type="button" class="btn btn-warning"><i class="text-blanco fa fa-check-circle"></i> </button> <span class="text-blanco"> Asignados</span>
                                                </div>
                                                <div class="spa-2">
                                                    <button type="button" class="btn btn-danger"><i class="text-blanco fa fa-bars"></i> </button> <span class="text-blanco"> Sin asignar</span>
                                                </div>
                                                <div class="spa-6">
                                                    <div class="pull-right">
                                                        <?php
                                                            if($imprimir){
                                                                ?>
                                                                    <button type="button" class="btn btn-danger tu_print">Imprimir <i class="text-blanco fa fa-print"></i></button>
                                                                <?php 
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                }

                            ?>
                            <!--</pre>-->
                        </div>
                    <?php } else { ?>
                        <div class="invoice10 text-center" style="margin-top:10em;">
                            <h2 class="text-blanco">Sin acceso, el tiempo de asignaci&oacute;n de turnos a expirado.</h2>
                            <h1 class="text-blanco" style="font-weight: bold;"><?php echo "1 / ".conveMes(date('m')+1)." / ".date('Y'); ?></h1>
                            <h4 class="text-blanco" style="font-weight: bold;">Fecha de la proxima asignaci&oacute;n.</h4>
                            <button type="button" class="btn btn-danger tu_ult">Imprimir ultimo periodo <i class="text-blanco fa fa-print"></i></button>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="row text-center">
                        <div class="box box-primary bg-red-gradient invoice8">
                            <div class="box-body">
                                <div class="row">
                                    <div class="error-content">
                                        <h3>Por favor comuníquese con sistemas EXT(2222) para que se le asigne los almacenes correspondientes.</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </section>
        </div>
    </div>
</div>

<?php if($fecActual){ ?> 

<div class="modal" id="modalDetTurn">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" class="close" onclick="closeMoTurno('DetTurn','form_DetTurn');">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Colaboradores por turno #<span id="id__turn"></span></h3>
        </div>
        
        <div class="modal-content">
            <?php 
                $attributes = array('id' =>'form_DetTurn','name'=>'form_DetTurn');
                echo form_open('procesos/nomina/co_125',$attributes); 
            ?>   

            <div class="modal-body">
                
                <div class="spa-12">
                    <table class="table table-bordered TFtable">
                        <thead>
                            <tr>
                                <th style="width: 5%;border: medium none;">#</th>
                                <th style="width: 5%;border: medium none;">C&oacute;digo</th>
                                <th style="border: medium none;">Nombres</th>
                                <th style="border: medium none;">Cargo</th>
                                <th style="border: medium none;">Turno</th>
                            </tr>
                        </thead>
                        <tbody id="tab_turns">
                        </tbody>
                    </table>
                </div>
                
                <div class="spa-12">
                    <div class="spa-6">
                        <div class="titulo-1 text-size-1-25" style="margin-bottom: 2px;"># De Colaboradores <span id="count_colaModal">0</span> </div>
                        <div class="titulo-1 text-size-1-25"># De Turnos Asigandos <span id="count_asigModal">0</span> </div>
                    </div>
                    <div class="spa-6">
                        <div class="pull-right">
                            <?php
                            //if($b > 0){
                                ?>
                                    <button type="button" class="btn btn-success sav_t"><i class="fa fa-save"></i> Grabar</button>
                                <?php 
                            //}
                            ?>
                        </div>
                    </div>
                </div> 

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<?php } ?>