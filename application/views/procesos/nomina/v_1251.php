<style>
    .underline {
        text-decoration: underline;
    }
    .topTr {
        font-weight: bold;
        background: rgb(167, 167, 167) none repeat scroll 0% 0%;
        color: rgb(255, 255, 255);
        text-align: center;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-check"></i> Aprobaci&oacute;n Prestamos</div>
                    </div>
                </div>
                
                <div class="row" style="margin-top: 10px;">
                    <div class="spa-4" style="text-align:left !important;">
                        
                    <div class="box box-primary">
                        <div class="box-body" style="padding: 12px;">
                            <div class="row">

                                <div class="spa-12">
                                    <label>Empresa</label>
                                    <?php echo $cmb_empr ?>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="spa-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <?php 
                                if($empr){
                                    ?>
                                        <div id="detContra" class="row" style="padding-left: 17px; padding-right: 17px;">
                                            <table id="tab_Aut" class="table table-bordered TFtable" style="width: 100%;"></table>
                                        </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="text-center">
                                        <h3 class="text-center">Usted no tiene empresas disponibles por favor comuniquese con sistemas.</h3>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <?php if($empr){ ?>
                            <button type="button" class="btn btn-success pull-right a_sol" style="margin-top: 10px;"><i class="fa fa-check"></i> Procesar</button>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalDetallSolic">
  <div class="modal-dialog">
    <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('DetallSolic','');" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span id="titleDoc"></span></h3>
    </div>

    <div class="modal-content">
        <div class="modal-body">
            <div id="view_Soli"></div>
            <div class="clearfix"></div>
        </div>
    </div>
  </div>
</div>
