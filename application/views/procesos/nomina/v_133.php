<style>

    .embed-container {
        height: 485px;
        width: 100%;
        padding-bottom: 56.25%;
        overflow: hidden;
        position: relative;
    }

    .embed-container object {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
    }
    ._taI,._taN,._taC,._taE {
        position: absolute;    
        width: 100%;
        font-family: helvetica;
    }
    ._taI {
        height: 192px;
        width: 192px;
        top: 117px;
        left: 48px;
    }

    ._taI img {
        position: absolute;
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        margin: auto;
    }

    ._taN {
        top: 332px;
        color: rgb(255, 255, 255);
        font-weight: bold;
        height: 40px;
        line-height: 40px;
    }

    ._taC {
        top: 380px;
        color: #000;
        font-size: 13pt;
        height: 20px;
        line-height: 20px;
    }

    ._taE {
        top: 400px;
        color: #000;
        font-size: 13pt;
        height: 20px;
        line-height: 20px;
    }

    #panels-wrap {
        position:relative;
        overflow:hidden;
    }
    #slide {
        position:relative;
        left:-100%;
        width:200%;
    }
    .panel_slide_rob {
        position:relative;
        display:inline-block;
        width:50%;
        height:100%;
        top:0;
        vertical-align:top;
    }   
    .pos_tar {
        padding: 45px 24px 22px 24px;
        font-size: 14pt;
        font-family: helvetica;
        font-weight: bold;
        text-align: center;
    }
    .pos_tar p {
        margin-bottom: 30px;
    }
    .font_emp1 {
        font-family: 'OratorStd' !important;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="tab_st">
            <div class="toolbar_tab">
                <h3><i class="fa fa-address-card"></i> Imprimir Credenciales</h3>
                <div class="tabs">
                    <ul>
                        <li class="tabitem active"><a href="#box1">Credencial<span></span></a></li>
                        <li class="tabitem"><a href="#box2">Reporte<span></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="content">
                <div id="box1" class="tab show">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="col-sm-12">
                                <h3 style="margin: 10px;">Datos Personales</h3>
                            </div>
                            <div class="col-sm-12">
                                <ul class="list-unstyled">
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>Empresa</b></div>
                                        <div class="col-xs-5 no_emp"></div>
                                        <div class="col-xs-4">
                                            <div class="input-group">
                                                <input id="txt_ce" name="txt_ce" class="form-control" type="text" onkeypress="return isNumero(event, this)" placeholder="C&oacute;digo">
                                                <input type="hidden" id="c_empl" name="c_empl">
                                                <input type="hidden" id="c_empr" name="c_empr">
                                                <div class="input-group-addon" style="padding:0;">
                                                    <button type="button" class="btn btn_oculto btn-danger bg-red sea_e" style="height: 33px;"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>Nombre</b></div>
                                        <div class="col-xs-9 no_col"></div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>C&eacute;dula</b></div>
                                        <div class="col-xs-9 ce_col"></div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>Cargo</b></div>
                                        <div class="col-xs-9 no_car"></div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>C.Costo</b></div>
                                        <div class="col-xs-9 no_cco"></div>
                                        <div class="clearfix"></div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12">
                                <h3 style="margin: 10px;">Tarjeta</h3>
                            </div>
                            <div class="col-sm-12">
                                <ul class="list-unstyled">
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>Nombre</b></div>
                                        <div class="col-xs-9">
                                            <input type="text" id="ed_nom" name="ed_nom" class="form-control" value="">
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>Cargo</b></div>
                                        <div class="col-xs-9">
                                            <input type="text" id="ed_car" name="ed_car" class="form-control" value="">
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>Tamaño Letra</b></div>
                                        <div class="col-xs-9">
                                            <select id="ed_let" name="ed_let" class="form-control">
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11" selected="">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>Ciudad</b></div>
                                        <div class="col-xs-9">
                                            <?php echo $ed_ciu; ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="col-xs-3"><b>Fecha Emisi&oacute;n</b></div>
                                        <div class="col-xs-9">
                                            <div id="fe_log"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div style="width:308px;float:left">
                            <div class="box box-0">
                                <div class="box-body text-center">
                                    <div id="panels-wrap">
                                        <div id="slide">
                                            <div id="porc_ge" class="panel_slide_rob">
                                                <div style="position: relative;" class="pos_tar">
                                                    <button type="button" class="btn-out leftPage" title="Posterior Tarjeta" style="color: #333;font-size: 35px;"><i class="fa fa-arrow-circle-left"></i></button>    
                                                    <p>Esta tarjeta es personal e Intransferible.</p>
                                                    <p><span class="_tEmpr">PYCCA S.A. </span>no se responsabiliza por el mal uso de esta tarjeta.</p>
                                                    <p>Si encuentra estar tarjeta, favor devolverla en nuestras oficinas.</p>
                                                    <p class="_taD"></p>
                                                </div>
                                            </div><!--
                                            --><div class="panel_slide_rob">
                                                <div style="position: relative;">
                                                    <button type="button" class="btn-out rightPage" title="Anterior Tarjeta" style="color: #333;font-size: 35px;"><i class="fa fa-arrow-circle-right"></i></button>
                                                    <div class="_taI"></div>
                                                    <div class="_taN" style="font-size:15pt;"></div>
                                                    <div class="_taC"></div>
                                                    <div class="_taE"></div>
                                                    <div class="_taIMG"><img src="<?php echo site_url('img/credencial.jpg'); ?>" width="308"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="col-xs-8" style="margin-top: 15px;">
                                            <input id="enable_dol" type="checkbox" class="js-switch" style="float: left;margin-right: 9px;"/>
                                            <div style="float: left;"> <b>Cobrar el Valor de $2</b></div>
                                        </div>
                                        <div class="col-xs-4">
                                            <button type="button" class="btn btn-danger pull-right p_cre" style="margin-top: 10px;"><i class="fa fa-print"></i> Imprimir</button>    
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div id="_pdf"></div>
                        </div>
                    </div>
                </div>
                <div id="box2" class="tab">
                    <div class="row-fluid">
                        <div class="col-xs-12">
                            <input id="ck_descuento" type="checkbox" checked="checked">
                            <label>Solo con descuento</label>
                        </div>
                        <div class="col-xs-3">
                            <label>Fecha Desde</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input name="txt_fec_ini" value="<?php echo date('Y/m/d',strtotime('-60 days')); ?>" id="txt_fec_ini" data-date-format="yyyy/mm/dd" class="form_datetime form-control" type="text">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <label>Fecha Hasta</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input name="txt_fec_fin" value="<?php echo date('Y/m/d'); ?>" id="txt_fec_fin" data-date-format="yyyy/mm/dd" class="form_datetime form-control" type="text">
                                <div class="input-group-addon" style="padding:0;">
                                    <button type="button" class="btn btn_oculto btn-danger bg-red sea_r" style="height: 33px;"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="tabReport" class="row-fluid"></div>
                    <div id="sumReport" class="row-fluid"></div>
                </div>
            </div>
        </div>
    </div>
</div>