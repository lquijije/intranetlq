<script>
    $('#bodyPage').css('background', 'url(../../img/carnes.jpg) top left no-repeat');
    $('#bodyPage').css('background-attachment', 'fixed');
    $('#bodyPage').css('background-size', '150%');
    $('.header').css('background', 'none');
</script>
<style>
    .cbp_tmtimeline::before {
        left: 16%;
    }
    .timeLine {
        background: #75b2f4 !important;
        color: #083565 !important;
    }
    .feriLine {
        background: #75b2f4 !important;
        color: #083565 !important;
    }
    td {
        vertical-align: top;
    }
    .come_tic {
        position: absolute; 
        right: -12px;
        top: -18px; 
        background: #1DC096; 
        width: 40px; 
        height: 40px;
        line-height: 40px;
        text-align: center;
        border-radius: 100%;
    }

    .diy {
        background:#FFF;margin-top: 2px; padding: 5px; border-top: 5px solid #F4543C;
    }

    .diy:hover {
        background:#FEE9D0;
    }

</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix" style="position: relative;">
        <div class="spa-9" style="background: rgba(34, 34, 34,0.7); padding: 20px;">
            <div class="spa-12">
                <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-cutlery"></i> Tickets de Comida</div>
            </div>

            <div class="main spa-12" style="max-width:none;">
                <?php
                $i = 0;
                if (isset($comedor) && !empty($comedor) && $valida == true) {

                    $saltaMes = '';
                    $finde = '';
                    foreach ($comedor as $row) {

                        $a = (int) date('m', strtotime($row['fecha']));

                        if ($saltaMes !== (int) date('m', strtotime($row['fecha']))) {
                            ?>
                            <ul class="cbp_tmtimeline">
                                <li>
                                    <div class="cbp_tmicon1" style="left: 16%;"><?php echo conveMes(date('n', strtotime($row['fecha']))) ?></div>
                                </li>
                                <?php
                                foreach ($comedor as $row1) {
                                    $i++;
                                    $CI = & get_instance();
                                    $finde = (trim(strtoupper($row1['dia'])) === 'SABADO' || trim(strtoupper($row1['dia'])) === 'DOMINGO') ? 'timeLine' : '';
                                    $b = (int) date('m', strtotime($row1['fecha']));
                                    $feriado = $CI->m_116->valid_feriado($row1['fecha']);
                                    $feri = (isset($feriado) && !empty($feriado)) ? 'feriLine' : '';
                                    if ($a === $b) {
                                        ?>
                                        <li>
                                            <time class="cbp_tmtime" style="width: 20%;"><span></span> <span><?php echo $row1['dia'] . "<br>" . $feriado ?></span></time>
                                            <div class="cbp_tmicon" style="left: 16%;"><?php echo date('d', strtotime($row1['fecha'])) ?></div>
                                            <div class="cbp_tmlabel <?php echo $finde . " " . $feri; ?>" style="margin: 0px 0px 25px 20%; position: relative;">
                                                <?php
                                                $ticket = $CI->m_116->ticketxDia($row1['fecha']);
                                                $cant = count($ticket[0]['item']);
                                                if ($cant <> 0) {
                                                    ?>
                                                    <a href="javascript:void(0)" onclick="viewtick('<?php echo htmlentities(json_encode($ticket)); ?>')"><div class="come_tic"><i class="fa fa-ticket"></i><span style="font-weight: bold;"><?php echo $cant; ?></span></div></a>
                                                <?php } ?>
                                                <table style="font-size: 14px;">
                                                    <thead>
                                                    <th width='500px'></th>
                                                    <th width='300px'></th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                if (!empty($row1['nor_1'])) {
                                                                    if ($valida) {
                                                                        ?><input type="radio" id="1" name="plato_<?php echo $i; ?>" data_label="<?php echo trim(utf8_encode($row1['nor_1'])) ?>"/> <?php } ?><?php echo trim(utf8_encode(ucwords(strtolower($row1['nor_1'])))) ?><br><?php } ?>
                                                                <?php
                                                                if (!empty($row1['nor_2'])) {
                                                                    if ($valida) {
                                                                        ?><input type="radio" id="3" name="plato_<?php echo $i; ?>" data_label="<?php echo trim(utf8_encode($row1['nor_2'])) ?>"/> <?php } ?><?php echo trim(utf8_encode(ucwords(strtolower($row1['nor_2'])))) ?><br><?php } ?>
                                                                <?php
                                                                if (!empty($row1['dieta'])) {
                                                                    if ($valida) {
                                                                        ?><input type="radio" id="2" name="plato_<?php echo $i; ?>" data_label="<?php echo trim(utf8_encode($row1['dieta'])) ?>"/> <?php } ?><?php echo trim(utf8_encode(ucwords(strtolower($row1['dieta'])))) ?><?php } ?>
                                                            </td> 
                                                            <td>
                                                                <?php
                                                                if (!empty($row1['aco_1'])) {
                                                                    if ($valida) {
                                                                        ?><input type="radio" id="<?php echo trim($row1['aco_1']); ?>" name="acomp_<?php echo $i; ?>"/> <?php } ?><?php echo utf8_encode(ucwords(strtolower($row1['aco_1']))) ?><br><?php } ?>
                                                                        <?php
                                                                        if (!empty($row1['aco_2'])) {
                                                                            if ($valida) {
                                                                                ?><input type="radio" id="<?php echo trim($row1['aco_2']); ?>" name="acomp_<?php echo $i; ?>"/> <?php } ?><?php echo utf8_encode(ucwords(strtolower($row1['aco_2']))) ?><br><?php } ?>
                                                                        <?php if ($valida) { ?>
                                                                            <input type="radio" id="" name="acomp_<?php echo $i; ?>"/> No deseo acompañante
                                                                            <input type="hidden" id="fech_<?php echo $i; ?>" value="<?php echo date('Y-m-d', strtotime($row1['fecha'])); ?>">
                                                                            <input type="hidden" id="json_<?php echo $i; ?>" value="<?php echo trim(trim(htmlentities(json_encode($ticket)), '['), ']'); ?>">  
                                                                        <?php } ?>
                                                            </td> 
                                                        </tr>
                                                        <?php if ($valida) { ?>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <button type="button" class="btn btn-danger pull-right" onclick="saveTicket(<?php echo $i; ?>, 'U')"><i class="fa fa-cutlery"></i> Comprar</button>
                                                                </td>
                                                            </tr>
                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    <?php
                                }
                            }
                            ?>
                            </ul>    
                            <?php
                        }

                        $saltaMes = (int) date('m', strtotime($row['fecha']));
                    }
                } else {
                    ?>
                    <div class="centro text-size-2 text-blanco">Lo sentimos, la compra de tickets est&aacute; suspendida por el momento. Cualquier inquietud favor comunicar a Jennifer Limones extension 1215.</div>
                    <?php
                }
                ?>
                <?php if (!empty($comedor) && $valida == true) { ?>
                    <button type="button" class="btn btn-danger pull-right" onclick="saveTicket(<?php echo $i; ?>, 'T')"><i class="fa fa-cutlery"></i> Compra Multiple</button>
                <?php } ?>
            </div>
        </div>
        <div class="spa-3">

            <div class="spa-12" style="background: rgba(34, 34, 34,0.7); padding: 25px; text-align: justify;">
                <h4 class="text-blanco">Para REALIZAR LA COMPRA favor seleccione el tipo de Comida y el acompañante, luego presione el botón de comprar de cada día.</h4>
                <h5 class="text-red">ATENCI&Oacute;N!!! Las compras de tickets de comida solo se podrán hacer máximo hasta los días miércoles a las 17h00
                    <strong>"Recuerde que una vez realizada la compra no se puede reversar, asegúrese bien antes de presionar el botón. Si tiene alguna duda lea bien la ayuda para evitar inconvenientes.
                        La lista simpre contiene los días de la siguiente semana, no se puede comprar de la semana en curso."</strong> 
                </h5>
                <button type="button" class="btn-out pull-right helpCome" style="position: relative;"><i class="fa fa-question-circle"></i></button>
            </div>

            <div class="spa-12" style="background: rgba(34, 34, 34,0.7); padding: 25px;margin-top: 15px;">
                <div class="titulo-1 text-size-1-5 text-blanco centro"><i class="fa fa-calendar"></i> Ticket Comprados</div>
                <h5 class="text-blanco centro">Ultimos 10 d&iacute;as</h5>
                <div style="font-size: 13px;">
<?php
if (isset($calendar) && !empty($calendar)) {
    foreach ($calendar as $row) {
        ?>
                            <a href="javascript:void(0)" onclick="viewtick('<?php echo htmlentities(json_encode(array($row))); ?>')" style="color:#333;">
                                <div class="spa-3 centro diy">
                                    <b><?php echo($row['fecha']) . " " . conveMes(date('n', strtotime($row['date']))); ?></b>
                                </div>
                            </a>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="text-blanco centro">No tiene ticket comprados en los ultimos 10 d&iacute;as</div>
    <?php
}
?>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal" id="modalHelp">
    <div class="modal-dialog">
        <div style="margin: 5px; position: absolute; z-index: 9; right: 5px; top: 5px;">
            <button type="button" onclick="quitarmodalGeneral('Help', '');" class="close">
                <i class="fa fa-times-circle"></i>
            </button>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="separador" style="padding-bottom: 15px;">

                    <div class="spa-12">
                        <h4 style="font-weight: bold; padding-bottom: 10px;" class="line">NUEVO sistema de servicio de Comedor para Pycca-Pica</h4>
                        <p>Estimados colaboradores, uno de los objetivos de la empresa es brindar un mayor beneficio al empleado y mantener un ambiente laboral y cálido, por tal motivo hemos revisado propuestas de algunos proveedores de servicio de almuerzos y se ha logrado un convenio con le empresa CASA MILA que nos ofrece una mejor atención y calidad en el menu diario que incluye:</p>

                        <p><i class="fa fa-angle-double-right" style="font-weight: bold;"></i> NORMAL: Sopa, dos opciones de plato fuerte, ensalada, postre y jugo o cola.<br>
                            <i class="fa fa-angle-double-right" style="font-weight: bold;"></i> DIETA: Sopa, una sola opción de plato fuerte, ensaladas, frutas, jugo o cola.</p>

                        <p>Las nuevas reglas son:</p>

                        <i class="fa fa-angle-double-right" style="font-weight: bold;"></i> El costo de cada ticket es de $2.80 <br>
                        <i class="fa fa-angle-double-right" style="font-weight: bold;"></i> La empresa beneficia a sus empleados asumiendo parte de ese valor. A continuación la tabla de subsidios: <br>
                    </div>

                    <div class="spa-5" style="margin-top: 20px;">
                        <table class="table table-bordered TFtable">
                            <thead>
                            <th>TIPO EMPLEADO</th>
                            <th>A DESCONTAR</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>PICA-PYCCA OFICINAS</td>
                                    <td>$1.13</td>

                                </tr>
                                <tr>
                                    <td>ALMACENES</td>
                                    <td>$1.63</td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>

                    <div class="spa-12">
                        <i class="fa fa-angle-double-right" style="font-weight: bold;"></i> Si el empleado compró un ticket y no lo consume se le descontará $2.80 <br>
                        <i class="fa fa-angle-double-right" style="font-weight: bold;"></i> Ahora se podrá comprar más de un almuerzo por día, el valor a <span style="font-weight: bold;">descontar por el almuerzo adicional será de $2.80</span>  <br>
                        <i class="fa fa-angle-double-right" style="font-weight: bold;"></i> No existe la opción de solo segundo o solo sopa, la compra es de un almuerzo completo.  <br>
                        <i class="fa fa-angle-double-right" style="font-weight: bold;"></i> La compra de tickets se la realizará hasta el día miercoles para poder consumir desde el siguiente lunes. Por ejemplo, hasta Miercoles 1 de Junio debimos haber comprado los tickets de comida para la semana del 8 al 12. Solo por esta vez se ha extendido el tiempo por motivos de ajustes en el sistema.  <br>
                        <i class="fa fa-angle-double-right" style="font-weight: bold;"></i> Ahora habrán 3 tipos de comida y para cada una de ellas se deberá seleccionar un acompañante.  <span style="font-weight: bold;">Inclusive la Dieta podrá seleccionar acompañante.</span> <br>
                    </div>

                    <div class="spa-12">

                        <h5 style="font-weight: bold;" class="text-red">Una vez realizada la compra del ticket no podrá ser anulado.</h5>
                        <h4 style="font-weight: bold; padding-bottom: 10px;" class="line">Preguntas frecuentes</h4>

                        <h5 style="font-weight: bold;">¿Cuando puedo realizar la compra? </h5>
                        <p>La compra de tickets se puede realizar hasta el MIERCOLES 16h59. Las compras dentro de este rango servirán para reservas de la siguiente semana. </p>

                        <h5 style="font-weight: bold;">¿Cuántos tickets puedo comprar? </h5>
                        <p>Se puede comprar por día la cantidad de tickets que desee, este ticket es de una comida completa. La primera compra es subsidiada dependiente del tipo de empleado, pero las siguientes se descontarán a $2.80, es decir no aplica subsidio. </p>

                        <h5 style="font-weight: bold;">¿Pierdo mi ticket si no voy a comer un día?</h5>
                        <p>Cuando Usted compra un ticket está reservando comida para ese día específico, el proveedor se encarga de preparar la cantidad exacta para que alcance a los empleados que reservaron y en base a eso se le pagará al proveedor. Por tal motivo una vez realizada la compra no se puede anular. Si no va a comer ese día igual se le descontará en nómina. </p>

                        <h5 style="font-weight: bold;">¿Puedo comprar tickets para otra persona?</h5>
                        <p>Se puede comprar más de un ticket que puede ser consumido por Usted o por quien sea, pero debe ir a poner su huella para que se registre el consumo. </p>

                        <h5 style="font-weight: bold;">¿Puedo anular la compra de un ticket?</h5>
                        <p>No es posible anular la compra de tickets.</p>

                        <h5 style="font-weight: bold;">¿Como puedo saber el consumo de mis ticketeras?</h5>
                        <p>En la misma opción de Compra se puede visualizar los TICKETS COMPRADOS.</p>

                        <h5 style="font-weight: bold;">¿Que hago si no me detecta la huella el sistema?</h5>
                        <p>En casos excepcionales como este se debe acercar a la persona encargada de despachar la comida y enseñarle la credencial en la que consta el codigo de empleado y nombre. Es importante que lleven consigo la credencial que la empresa les entrega no solo para el comedor sino para todas las laboras llevan a cabo durante la jornada laboral. En el departamento de sistemas se tratará de solucionar el inconveniente para que la próxima vez use su huella. </p>

                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="modal" id="modalTicket">
    <div class="modal-dialog">
        <div style="margin: 5px; position: absolute; z-index: 9; right: 5px; top: 5px;">
            <button type="button" onclick="quitarmodalGeneral('Ticket', '');" class="close">
                <i class="fa fa-times-circle"></i>
            </button>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div class="separador" style="padding-bottom: 15px;">

                    <div id="viewTick" class="spa-12">

                    </div>
                    <div id="viewSave" class="spa-12">

                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>