
<script src="<?php echo site_url(); ?>js/plugins/ace-builds-master/src-noconflict/ace.js" type="text/javascript"></script>

<style>
    #editor {
        margin: 80px 10px 80px 10px;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
    }
    .dashboard {
        padding: 5em 0em;
    }
    
    select[multiple], select[size] {
        height: 200px;
    }
</style>


<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice1">
                
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Consulta de Documentos Electronicos</div>
                    </div>
                </div>
                
                <div class="row" style="margin-top: 10px;">
                    <div class="spa-2 box box-primary" style="padding:15px;">
                        
                        <div class="spa-12">
                            <label>Fecha Inicio</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                                </div>
                                <input name="txt_fec_ini" value="<?php echo $fecha_inicial; ?>" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="">
                            </div>
                        </div>

                        <div class="spa-12">
                            <label>Fecha Final</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                                </div>
                                <input name="txt_fec_fin" value="<?php echo $fecha_final; ?>" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="">
                            </div>
                        </div>

                        <div class="spa-12">
                            <label>Tipo de Documento</label>
                            <select id="cmb_tipo" name="cmb_tipo" class="form-control input-sm">
                                <option value="F">Factura</option>
                                <option value="V">Nota de Crédito</option>
                                <option value="E">Retenciones</option>
                                <option value="G">Guia Remisón</option>
                            </select>
                        </div>

                        <div class="spa-12">
                            <label>Almacen</label>
                            <?php
                            if(isset($_SESSION['almacenes']) && !empty($_SESSION['almacenes'])){
                                ?>
                                    <select id="cmb_almacen" name="cmb_almacen" class="form-control" multiple>
                                        <option value="nnn" selected="">Todos</option>
                                <?php
                                    foreach ($_SESSION['almacenes'] AS $row){
                                        ?><option value="<?php echo trim($row['co_bodega']) ?>|<?php echo trim($row['tipo_base']) ?>"><?php echo trim($row['de_bodega']) ?></option><?php
                                    }
                                ?>
                                    </select>
                                <?php 
                            }
                            ?>
                            <label>Para seleccionar mas de un almacen mantenga presionada la tecla "CONTROL" y luego seleccione.</label>
                        </div>

                        <div class="spa-12">
                            <label class="out-text">Buscar</label>
                            <button type="button" class="btn btn-primary searchDocu"><i class="text-red fa fa-search"></i> Buscar</button>
                        </div>
                        
                    </div>
                    <div class="spa-10">
                        <div class="box box-primary">
                            <div class="box-body">                      
                                <div id="detalleDocumentos" class="row" style="padding-left: 17px; padding-right: 17px;">
                                        
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        
                        <div class="box box-primary">
                            <div class="box-body">                      
                                <div id="totales" class="row" style="padding-left: 9px; padding-right: 9px;">

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                        
                </div>
                
                <div class="clearfix"></div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalDetallDocts">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" onclick="quitarmodalGeneral('DetallDocts','form_DetallDocts');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Detalles de <span id="titleDoc"></span></h3>
        </div>
        
        <?php 
            $attributes = array('id' => 'form_DetallDocts','name'=>'form_DetallDocts');
            echo form_open('procesos/facturacion/co_101',$attributes); 
        ?>   
        <div class="modal-body">
            <div id="detalleDocuElect" class="row" style="padding-left: 17px; padding-right: 17px;">

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clear"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalDetallXML">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" onclick="quitarmodalGeneral('DetallXML','form_DetallXML');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">XML <span id="titleDocXML"></span></h3>
        </div>
        
        <?php 
            $attributes = array('id' => 'form_DetallXML','name'=>'form_DetallXML');
            echo form_open('procesos/facturacion/co_101',$attributes); 
        ?>   
        <div class="modal-body">
            <div class="spa-12">
                <div id="contentXML">
                    <pre id="editor">

                    </pre>
                </div>
            </div>
        <div class="clearfix"></div>
        </div>
        <div class="clear"></div>
<!--        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-process">Procesar</button>
        </div>-->
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    if($('#txt_fec_ini').val().trim() !==''){
        getDocuError('<?php echo trim($tipoDoc); ?>');
        $('#cmb_tipo').val('<?php echo trim($tipoDoc); ?>')
    }
});
</script>