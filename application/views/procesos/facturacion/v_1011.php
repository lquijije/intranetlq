<?php
if($this->session->flashdata('error')){
    $error = $this->session->flashdata('error');
}

if(function_exists('validation_errors') && validation_errors() != ''){
    $error = validation_errors();
} 

?>

<?php 
if (!empty($message)): ?>
<script type="text/javascript">
    $(document).ready(function() {
        errorGeneral('Sistema Intranet',"<?php echo quitar_saltos($message);?>",'success');
    });
</script>
<?php endif; ?>

<?php if (!empty($error)): ?>
<script type="text/javascript">
    $(document).ready(function() {
        var a = "<?php echo quitar_saltos(trim($error));?>";
        errorGeneral('Sistema Intranet',a,'danger');
    });
</script>
<?php endif; ?>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice15">
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-key"></i> Cambio de Clave Facturaci&oacute;n El&eacute;ctronica</div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <?php $attributes = array('id' => 'f_cla','name' => 'f_cla');   
                                    echo form_open('procesos/facturacion/co_1011',$attributes); ?>
                                    <div class="spa-12">
                                        <label>Identificaci&oacute;n</label>
                                        <input id="c_u" name="c_u" value="<?php echo $cod_ci; ?>" class="form-control" onkeypress="return isNumero(event,this)" type="text">
                                    </div>
                                    <div class="spa-12" style="margin-top: 10px;">
                                        <?php if (isset($_SESSION['verifica']) && $_SESSION['verifica'] == 1){ ?>
                                            <button type="submit" class="btn pull-right btn-success" style="margin-left: 10px;"><i class="fa fa-send"></i> Resetear</button>
                                        <?php } else { ?>
                                            <button type="submit" class="btn pull-right btn-success" style="margin-left: 10px;"><i class="fa fa-question"></i> Verificar</button>
                                        <?php } ?>
                                            <a href='<?php echo site_url('procesos/facturacion/co_1011')?>' class="btn pull-right btn-danger"><i class="fa fa-close"></i> Cancelar</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>


<?php if (!empty($dataFec) && $_SESSION['verifica'] == 1){ 
?>
<div style="display: block; z-index: 1041;" class="fadebox" id="fadeClave"></div>
<div style="top: 35%; bottom: 25%; width: 40%; height: 30%; display: block; z-index: 1042;" class="modal" id="modalClave">
  <div class="modal-dialog">
    <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('Clave','');" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title">HistorialCambio de Clave</h3>
    </div>
        
    <div class="modal-content">
        <div class="modal-body">
            <div class="separador" style="text-align: center !important;">
                
                <table class="table table-bordered TFtable">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Usuario</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            foreach ($dataFec as $row){ ?>
                                <tr>
                                    <td><?php echo date('d/m/Y h:i:s',strtotime($row['fec'])); ?></td>
                                    <td><?php echo $row['usu']; ?></td>
                                </tr>
                        <?php 
                            }
                        ?>
                    </tbody>
                </table>
                <h3 style="font-weight: bold; text-align: center !important;"></h3>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>

<?php }?>

