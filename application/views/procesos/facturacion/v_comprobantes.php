<style>
    .form-control-sm {
        height: 22px !important;
    }
    .b_header {
        padding: 5px;
        background: #283D62;
    }
    .b_header_1 {
        padding: 5px;
        background: #4D4A4A;
    }
    .b_header h5 {
        color:#FFF;
        text-align: center;
        font-weight: bold;
        font-size: 12px;
        margin: 0;
    }
    .b_body {
        font-size: 12px;
        padding: 5px;
        border: 1px solid #BFBFBF;
        background: #FFF;
    }
    .table {
        font-size: 12px;
        margin-bottom: 0px;
    }
    .table thead tr th {
        vertical-align: middle;
    }
    .TFtable th {
        background-color: #4D4A4A !important;
    }
    .TFtable tr:nth-child(2n+1) {background: #e4e4e4;}
    .TFtable1 th {
        background-color: #283D62 !important;
    }
    .scrollBar {
        position: relative;
        height: 600px;
    }
    .legendLabel > div > a {
        text-align: left;
        color:#333 !important;
    }
    .table.dataTable {
        margin-top: 0px !important;
    }
    .floatThead-container{
        padding-left: 5px !important;
    }
    .esx {
        margin-bottom: 6px !important;
    }

</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <section class="content invoice">
            <div class="tab_st tab_color_2">
                <div class="toolbar_tab">
                    <h3><i class="fa fa-search"></i> Documentos Electr&oacute;nicos</h3>
                    <div class="tabs">
                        <ul>
                            <!--<li class="tabitem active"><a href="#box1">Emitidos<span></span></a></li>-->
                            <li class="tabitem"><a href="#box2">Recibidos<span></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="content">
                    <!--                    <div id="box1" class="tab show">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="b_header">
                                                            <h5>Documentos</h5>
                                                        </div>
                                                        <div class="b_body">
                                                            <div class="col-xs-4">
                                                                <label>A&ntilde;o</label>
                    <?php echo $cmb_anio; ?>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <label>Mes</label>
                    <?php echo $cmb_meses; ?>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <label>D&iacute;a</label>
                    <?php echo $cmb_dia; ?>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <table class="table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width: 30%;">Tipo Aut.</td>
                                                                            <td><?php echo $cmb_estados; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Tipo Doc.</td>
                                                                            <td><?php echo $cmb_documentos; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>CI/Ruc</td>
                                                                            <td><input id="tx_identificacion" type="text" class="form-control form-control-sm" onkeypress="return isNumero(event, this);" maxlength="13"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                    
                                                            <div class="col-xs-4">
                                                                <label>Establecim</label>
                                                                <input id="tx_estable" type="text" class="form-control form-control-sm" onkeypress="return isNumero(event, this);" maxlength="3">
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <label>Punt. Emis.</label>
                                                                <input id="tx_pemision" type="text" class="form-control form-control-sm" onkeypress="return isNumero(event, this);" maxlength="3">
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <label>Secuencial</label>
                                                                <input id="tx_secuencial" type="text" class="form-control form-control-sm" onkeypress="return isNumero(event, this);" maxlength="9">
                                                            </div>
                                                            <button class="btn btn-sm btn-success pull-right se_enviados" type="button" style="margin: 8px 5px 5px;"><i class="fa fa-search"></i> Consultar</button>
                                                            <button class="btn btn-sm btn-danger pull-right cl_enviados" type="button" style="margin: 8px 5px 5px;"><i class="fa fa-close"></i> Limpiar</button>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="b_header">
                                                            <h5>Gr&aacute;fico Documentos</h5>
                                                        </div>
                                                        <div class="b_body">
                                                            <div id="pieDoc" class="des_box" style="height:150px;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="b_header">
                                                            <h5>Gr&aacute;fico Autorizaciones</h5>
                                                        </div>
                                                        <div class="b_body">
                                                            <div id="pieAut" class="des_box" style="height:150px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-9">
                                                    <div class="row" id="contEmitidos"></div>
                                                    <div class="col-xs-3 pull-right" id="contEmitidosTot"></div>
                                                </div>
                                            </div>
                                        </div>-->
                    <div id="box2" class="tab show">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="b_header b_header_1">
                                        <h5>Documentos</h5>
                                    </div>
                                    <div class="b_body">
                                        <div class="border_section">
                                            <div class="border_top-border border_left"></div>
                                            <div class="border_top-border border_right"></div>
                                            <h5><b>Emisi&oacute;n</b></h5>
                                            <div style="margin: 0 1px 5px 1px;">
                                                <div class="col-xs-4">
                                                    <label>A&ntilde;o</label>
                                                    <?php echo $cmb_anio_rec; ?>
                                                </div>
                                                <div class="col-xs-5">
                                                    <label>Mes</label>
                                                    <?php echo $cmb_meses_rec; ?>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label>D&iacute;a</label>
                                                    <?php echo $cmb_dia_rec; ?>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="border_section">
                                            <div class="border_top-border border_left"></div>
                                            <div class="border_top-border border_right"></div>
                                            <h5><b>Filtro</b></h5>
                                            <div style="margin: 0 1px 5px 1px;">

                                                <div class="col-xs-12">
                                                    <table class="">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 30%;">Contabilizado</td>
                                                                <td><input id="ck_contabilizado"  type="checkbox"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tipo Doc.</td>
                                                                <td><?php echo $cmb_documentos_rec; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>CI/Ruc</td>
                                                                <td><input id="tx_identificacion_rec" type="text" class="form-control form-control-sm" onkeypress="return isNumero(event, this);" maxlength="13"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label>Establecim</label>
                                                    <input id="tx_estable_rec" type="text" class="form-control form-control-sm" onkeypress="return isNumero(event, this);" maxlength="3">
                                                </div>
                                                <div class="col-xs-4">
                                                    <label>Punt. Emis.</label>
                                                    <input id="tx_pemision_rec" type="text" class="form-control form-control-sm" onkeypress="return isNumero(event, this);" maxlength="3">
                                                </div>
                                                <div class="col-xs-4">
                                                    <label>Secuencial</label>
                                                    <input id="tx_secuencial_rec" type="text" class="form-control form-control-sm" onkeypress="return isNumero(event, this);" maxlength="9">
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <button class="btn btn-sm btn-success pull-right se_recibidos" type="button" style="margin: 8px 5px 5px;"><i class="fa fa-search"></i> Consultar</button>
                                        <button class="btn btn-sm btn-danger pull-right cl_recibidos" type="button" style="margin: 8px 5px 5px;"><i class="fa fa-close"></i> Limpiar</button>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="b_header b_header_1">
                                        <h5>Gr&aacute;fico Documentos</h5>
                                    </div>
                                    <div class="b_body">
                                        <div id="pieDocRec" class="des_box" style="height:150px;"></div>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="b_header b_header_1">
                                        <h5>Gr&aacute;fico Contabilizado</h5>
                                    </div>
                                    <div class="b_body">
                                        <div id="pieConRec" class="des_box" style="height:150px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row" id="contRecibidos"></div>
                                <div class="col-xs-3 pull-right" id="contRecibidosTot"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>