<script src="<?php echo site_url(); ?>js/plugins/chart/Chart.js" type="text/javascript"></script>
<?php 
$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"); 

?>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    vertical-align: middle;
     border: 1px solid #DDD;
}

.table {
    margin-bottom: 0px;
}
.doughnut-legend{
    list-style: none;
    position: relative;
}
.btn.btn-circle{
    width: 20px;
    height: 20px;
    line-height: 20px;
}
.notify{
    font-size: 10px;
    top: 0;
    left: 5px;
    position: absolute;
    border-radius: 100%;
    width: 18px;
    height: 18px;
    line-height: 18px;
    text-align: center;
}
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix" style="padding: 5em 3em;">
        <div class="spa-12">
            <section class="content invoice">
                
                <div class="row">	
                    <div class="spa-4" style="max-width: 23.8%; width: auto;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-ticket"></i> Helpdesk Monitor</div>
                    </div>
                    
                    <div class="spa-2-5" style="max-width: 19.6333%; width: auto;">
                        <?php $attributes = array('id' => 'f_help','name' => 'f_help');   
                            echo form_open('procesos/helpdesk/co_102',$attributes); ?>
                            <ul class="nav navbar-nav">
                                <li class="user-menu">

                                    <?php 
                                    
                                    $data = array(
                                        'dpto' => $getDpto,
                                        'f_i' => $fec_ini,
                                        'f_f' => $fec_fin
                                    );

                                    echo form_hidden($data);
                                    
                                    if (isset($dptos) && !empty($dptos)){

                                        $count = count($dptos);
                                        if(isset($_SESSION['master'][6]) && !empty($_SESSION['master'][6])){
                                            $arreglo = $_SESSION['master'][6];
                                        } else {
                                            $arreglo= null;
                                        }

                                        $select_title="";
                                        $select_items="";
                                        for ($i=0; $i<$count; $i++){
                                            if (trim($getDpto) === trim($arreglo[0][$dptos[$i]]['cod_para'])) {
                                                if ($count === 1){
                                                    $select_title ='<a href="#" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.$arreglo[0][$dptos[$i]]['nom_para'].' </span></a><ul class="dropdown-menu">';
                                                } else {
                                                    $select_title ='<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.$arreglo[0][$dptos[$i]]['nom_para'].' <i class="caret"></i></span></a><ul class="dropdown-menu">';
                                                }
                                            } else {
                                                $select_items.="<li><a href='javascript:void(0)' onclick='passDpto(\"".trim($arreglo[0][$dptos[$i]]['cod_para'])."\")'>".$arreglo[0][$dptos[$i]]['nom_para']."</a></li>";
                                            }
                                        }

                                        echo $select_title.$select_items.'</ul>';   

                                        ?> 
                                        <?php
                                    } else {
                                        ?>
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;">
                                                <span class="titulo-1" style="font-size: 22px;">No tiene Asignado Departamentos</span>
                                            </a>
                                        <?php
                                    }
                                    ?>
                                </li>
                            </ul>
                        </form>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="spa-3-5">
                        <div class="helpdesk_solicitu_1">
                            <div class="header-fixed" style="width:27.9667%;">
                                <div class="list-group-header bg-red">
                                    <span class=" text-size-1-5 titulo-1"> <i class="fa fa-comments"></i> Comentarios</span>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div style="padding-top: 52px;">
                                
                                <div class="activity-list">
                                    <?php 
                                    
                                    if(isset($coment) && !empty($coment)){
                                        foreach($coment as $row){
                                            $tip = trim($row['co_tipo']) === 'T'?'Ticket':'Requerimiento';
                                            $site = 'img/fotos/'.trim($row['co_usuario']).'.JPG';
                                            $site = file_exists($site)?site_url('img/fotos/'.trim($row['co_usuario']).'.JPG'):site_url('img/fotos/user.png'); 
                                            if ((int)$row['fl_responder'] === 1){
                                                ?>
                                                <div class="activity">
                                                   
                                                    <img class="activity_img" src="<?php echo $site ?>" />
                                                    <div class="activity__message">
                                                        <div class="activity__message__header">
                                                            <h4><?php echo ucwords(strtolower($row['nombres']." ".$row['apellidos'])) ?> </h4>
                                                            <div style="color: #949494;font-size: 12px;"><?php echo $tip;?> #<?php echo $row['co_solicitud']; ?> - <?php echo trim($row['tx_titulo']); ?></div>
                                                            <span class="span_right"><?php echo date('d', strtotime($row['fe_comentario']))." ".conveMes(date('n', strtotime($row['fe_comentario'])))." ".date('Y (h:i)', strtotime($row['fe_comentario'])); ?>
                                                            </span>
                                                        </div>
                                                      <p> <?php echo $row['tx_descripcion']; ?></p>
                                                      <?php
                                                        if (trim($row['co_usuario']) <> trim($_SESSION['c_e'])){
                                                            if(trim($row['co_estado']) <> 'F' && trim($row['co_estado']) <> 'C'){
                                                            ?>
                                                                <a href="javascript:void(0)" onclick="viewSolic('<?php echo $row['co_tipo']; ?>',<?php echo $row['co_solicitud']; ?>,'',1)">Responder</a>
                                                            <?php                                                            
                                                            }
                                                        }
                                                      ?>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="activity" style="font-weight: bold;min-height:0px;">
                                                    <div class="activity_img"><i class="fa fa-exclamation-circle"></i></div>
                                                    <div class="activity__message">
                                                        <div class="activity__message__header">
                                                            <span><?php echo date('d', strtotime($row['fe_comentario']))." ".conveMes(date('n', strtotime($row['fe_comentario'])))." ".date('Y (h:i)', strtotime($row['fe_comentario'])); ?></span>
                                                        </div>
                                                      <p> <?php echo $row['tx_descripcion']; ?></p>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }else {
                                        ?>
                                        <div class="spa-12">
                                            <div class="small-box ">
                                                <div class="inner centro">
                                                    <h4>No hay Comentarios</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  
                                    }
                                    
                                    ?>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="spa-6">
                        <div class="helpdesk_solicitu_2">
                            <div class="list-group-header" style="background: #00A65A; color: #FFF;">
                                <span class=" text-size-1-5 titulo-1"> <i class="fa fa-users"></i> Asignado</span>
                                <div class="pull-right" id="reportrange" style="font-size: 21px;">
                                    <i class="fa fa-calendar"></i> 
                                    <span><?php echo $meses[date('n',strtotime($fec_ini))-1]." ".date("j, Y", strtotime($fec_ini)); ?> - <?php echo $meses[date('n' ,  strtotime($fec_fin))-1]." ".date("j, Y",  strtotime($fec_fin)); ?></span> 
                                    <b class="caret"></b>
                                </div>
                                <button type="button" class="btn-out" onclick="window.location.reload();" title="Refrescar"><i class="fa fa-refresh"></i></button>
                                <button type="button" class="btn-out excel_btn" title="Exportar a Excel"></button>
                                <a class="excel_btn" href="#"></a>
                            </div>
                            <div class="clear"></div>
                            <div>
                                <table class="table table-bordered">
                                    <thead>
                                        <th colspan="3"></th>
                                        <th colspan="3">Prioridad</th>
                                        <th></th>
                                    </thead>
                                    <thead>
                                        <th class="centro" width="10%">Area</th>
                                        <th class="centro" width="50%">Recurso</th>
                                        <th width="5%">Finalizado</th>
                                        
                                        <th width="10%" class="centro">
                                            <i class="fa fa-star" style="color: #F1C40F;"></i>
                                            <i class="fa fa-star" style="color: #F1C40F;"></i>
                                            <i class="fa fa-star" style="color: #F1C40F;"></i>
                                        </th>
                                        <th width="10%" class="centro">
                                            <i class="fa fa-star" style="color: #F1C40F;"></i>
                                            <i class="fa fa-star" style="color: #F1C40F;"></i>
                                            <i class="fa fa-star" style="color: #ccc;"></i>
                                        </th>
                                        
                                        <th width="10%" class="centro">
                                            <i class="fa fa-star" style="color: #F1C40F;"></i>
                                            <i class="fa fa-star" style="color: #ccc;"></i>
                                            <i class="fa fa-star" style="color: #ccc;"></i>
                                        </th>
                                        <th width="5%">Actual</th>
                                    </thead>
                                    <tbody>
                                        
                                        <?php 
                                            $siteGe= '';
                                            if(isset($tablaMonitor) && !empty($tablaMonitor)){
                                                if (count($tablaMonitor)> 0){
                                                    $aB = groupArray($tablaMonitor,'tx_descripcion');
                                                    $depart = '';
                                                    $i = 0;
                                                    foreach ($tablaMonitor as $row){
                                                        $siteGe = 'img/fotos/'.trim($row['co_empleado']).'.JPG';
                                                        
                                                        if (file_exists($siteGe)) {
                                                            $siteGe = site_url('img/fotos/'.trim($row['co_empleado']).'.JPG');
                                                        } else {
                                                            $siteGe = site_url('img/fotos/user.png');
                                                        }
                                                        
                                                        if ($depart <> trim((int)$row['co_area'])){
                                                        ?>
                                                        <tr>
                                                            <td class="centro" rowspan="<?php echo count($aB[$i]['groupeddata']); ?>"><?php echo $row['tx_descripcion'];?></td>
                                                            <?php $i++;} ?>
                                                            <td>
                                                                <div class="spa-2">
                                                                    <a href="javascript:void(0)" onclick="asigUsers(<?php echo $row['co_empleado']?>,<?php echo $getDpto; ?>)">
                                                                        <img class="activity_img_circle" src="<?php echo $siteGe ?>">
                                                                    </a>
                                                                </div> 
                                                                <div class="spa-10">
                                                                    <div class="spa-12"><a href="javascript:void(0)" class="link" onclick="asigUsers(<?php echo $row['co_empleado']?>,<?php echo $getDpto; ?>)"><?php echo utf8_encode($row['nombre']) ;?></a></div>
                                                                    <div class="spa-12 text-gris"><?php echo utf8_encode($row['usuario']);?></div>
                                                                </div>
                                                            </td>
                                                            <td class="centro"><?php echo $row['finalizados'];?></td>
                                                            <td class="centro"><?php echo $row['p_3'];?></td>
                                                            <td class="centro"><?php echo $row['p_2'];?></td>
                                                            <td class="centro"><?php echo $row['p_1'];?></td>
                                                            <td class="centro">
                                                                <?php
                                                                if ((int)$row['en_curso'] <> 0){
                                                                    //$funcion =  "viewSolic('".$row['co_tipo']."',".$row['co_solicitud'].")";
                                                                    ?>
                                                                <span class="label label-success">#<?php echo trim($row['en_curso'])?></span>        
                                                                    <?php 
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                        $depart = trim((int)$row['co_area']);
                                                    }
                                                } 
                                            } else {
                                                ?>
                                                    <tr>
                                                        <td colspan="7">
                                                            <div class="spa-12">
                                                                <div class="small-box ">
                                                                    <div class="inner centro">
                                                                        <h4>No hay Asignaciones</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php  
                                            }

                                        ?>
                                                    
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        
                            <div class="helpdesk_solicitu_2" style="background: none;">
                                <div class="spa-2-5">
                                            
                                </div>
                                
                                <div class="spa-2-5">
                                    <div id="canvas-holder">
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                    
                    <div class="spa-2-5">
                        <div class="helpdesk_solicitu_2">
                            <div style="padding: 20px;">
                                <span class="text-size-1-5 titulo-1" > <i class="fa fa-clock-o"></i> Por Vencer</span>
                            </div>
                            <div class="clear"></div>
                            <div class="list-group" style="padding-top: 10px;">
                                <?php 
                                if($PorVencer <> 'null'){
                                    ?><canvas id="chart_p_v"/><?php
                                } else {
                                    ?>
                                    <div class="spa-12" style="padding-bottom: 20px;">
                                        <div class="small-box ">
                                            <div class="inner centro">
                                                <h4>No hay Solicitudes</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        
                        <div class="helpdesk_solicitu_2">
                            <div style="padding: 20px;">
                                <span class="text-size-1-5 titulo-1"> <i class="fa fa-exclamation-circle"></i> Sin Asignar</span>
                            </div>
                            <div class="clear"></div>
                            
                            <div class="list-group" style="padding-top: 10px;">
                                <?php 
                                if($SinAsignar <> 'null'){
                                    ?><canvas id="chart_s_g"/><?php
                                } else {
                                    ?>
                                    <div class="spa-12" style="padding-bottom: 20px;">
                                        <div class="small-box ">
                                            <div class="inner centro">
                                                <h4>No hay Solicitudes</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="clearfix"></div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalViewSolicit">
  <div class="modal-dialog">
    <div class="modal-header bg-red">
          <button type="button" onclick="quitarmodalGeneral('ViewSolicit','form_ViewSolicit'),window.location.reload()" class="close">
              <i class="fa fa-times-circle"></i>
              <span class="sr-only">Close</span>
          </button>
          <h3 class="modal-title"><span class="titleSol"></span> - <i class="iconViewSolict fa "></i>  <span class="titleViewSolict"></span></h3>
    </div>
    <div class="modal-content">
        
        <span>
        <div class="modal-body">
            <div  style="position: relative" class="row">
                
            <div class="spa-8">
                <div class="spa-12 line" style="margin-bottom: 15px; padding-bottom: 15px; position:relative;">
                    <div class="spa-6">
                        <b>Ingreso la solicitud: </b><br>
                        <span class="_ing_sol"></span>
                    </div>
                    <div class="spa-6">
                        <b>Centro de Costo: </b><br>
                        <span class="_ing_cco_sol"></span>
                    </div>
                    <div class="spa-6">
                        <b>Fecha de Solicitud: </b><br>
                        <span class="_ing_fec_sol"></span>
                    </div>
                </div>
                <div class="spa-12">
                    <div class="contViewSolict">
                    </div>
                </div>
            </div>
            
            <div class="spa-4" style="position: relative;">
                <?php 
                    $attributes = array('id' =>'form_ViewSolicit','name'=>'form_ViewSolicit');
                    $hidden=array('id_Viewsolic'=>'',
                                  'id_ViewActi'=>'',
                                  'tipo_sol'=>'',
                                  'id_dpto'=>$getDpto);   
                    echo form_open('procesos/helpdesk/co_102',$attributes,$hidden); 
                ?> 
                <div class="phone-containter">
                    <div id="idComentsSolic" class="phone">
                    </div>

                    <div class="send-container" style="width: 75%;top: 0;">
                      <input type="text" id="msgInput" class="form-control" placeholder="Escribir un comentario..." />
                    </div>
                    <div style="width: 25%;top: 0;position: absolute;right: -12px;">
                        <label>Publico</label><br>
                        <input type="checkbox" id="public_" name="public_"/>
                    </div>
                </div>
                
                <?php echo form_close(); ?>
            </div>
                
            </div> 
            
            <div class="clearfix"></div>
        </div>
        </span>
    </div>
  </div>
</div>

<div class="modal" id="modalSolicit">
  <div class="modal-dialog">
    <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('Solicit','form_Solicit'),window.location.reload();" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span id="titleDoc"></span></h3>
    </div>
    <div class="modal-content">
        <?php 
            $attributes = array('id' => 'form_Solicit','name'=>'form_Solicit');
            echo form_open('procesos/helpdesk/co_102',$attributes); 
        ?>   
        <div class="modal-body">
            <div id="tableSolicit">
                
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clear"></div>
        
        <?php echo form_close(); ?>
    </div>
  </div>
</div>

<div class="modal" id="modalAsigSolicit">
  <div class="modal-dialog">
      
    <div class="modal-header bg-blue">
        <button type="button" onclick="quitarmodalGeneral('AsigSolicit','form_AsigSolicit');" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span id="titleAsigSol"></span> - <i id="iconAsigSolict" class="fa fa-times"></i>  <span id="titleAsigSolict"></span></h3>
    </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' => 'form_AsigSolicit','name'=>'form_AsigSolicit');
            echo form_open('procesos/facturacion/co_102',$attributes); 
        ?>   
        
        <input type="hidden" id="hid_cod_soli" name="hid_cod_soli" value="">
        <input type="hidden" id="hid_tip_soli" name="hid_tip_soli" value="">
        
        <div class="modal-body">
                
            <div class="spa-7" style="border-right: 1px solid #DDD;padding-right: 20px;">
                <div class="spa-12 line" style="margin-bottom: 15px; padding-bottom: 15px; position:relative;">
                    <div class="spa-6">
                        <b>Ingreso la solicitud: </b><br>
                        <span class="_ing_sol"></span>
                    </div>
                    <div class="spa-6">
                        <b>Centro de Costo: </b><br>
                        <span class="_ing_cco_sol"></span>
                    </div>
                    <div class="spa-6">
                        <b>Fecha de Solicitud: </b><br>
                        <span class="_ing_fec_sol"></span>
                    </div>
                </div>
                <div class="spa-12">
                    <div id="contAsigSolict">
                    </div>
                </div>
            </div>
            
            <div class="spa-5">
                
                <div class="line" style="margin-bottom: 20px; padding-bottom: 15px;">

                    <div class="spa-4" style="margin-bottom:7px;">
                        <label style="margin-bottom:10px;">Fecha Tope:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input id="txt_fec_top" name="txt_fec_top" value="" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                        </div>
                    </div>

                    <div class="spa-8">
                        <label>Prioridad:</label>
                        <div class="stars">
                            <input type="radio" name="star" class="star-1" id="star-1" />
                            <label class="star-1" for="star-1">1</label>
                            <input type="radio" name="star" class="star-2" id="star-2" />
                            <label class="star-2" for="star-2">2</label>
                            <input type="radio" name="star" class="star-3" id="star-3" />
                            <label class="star-3" for="star-3">3</label>
                            <span></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <section>
                    
                    <ul class="nav nav-tabs">
                        <li class="active" ><a href="#tab1" data-toggle="tab" id="tab1_click">Asignaci&oacute;n</a></li>
                        <li><a href="#tab2" data-toggle="tab">Comentarios</a></li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab1">

                            <h3 class="modal-title centro" style="margin-bottom: 15px;">Asignación de Usuarios</h3>

                            <div class="spa-11">
                                <input type="hidden" id="cod_Empl_Asig" name="cod_Empl_Asig" value="">
                                <?php echo $Select; ?>
                            </div>

                            <div class="spa-1">
                                <button type="button" class="btn btn-square btn-danger add_option"><i class="text-blanco fa fa-plus"></i></button>
                            </div>

                            <div class="spa-12">
                                <textarea id="ObsevaUsua" name="ObsevaUsua" class="form-control" style="margin-top: 5px;" placeholder="Observación"></textarea>
                            </div>

                            <div class="spa-12" style="margin-top: 10px;">
                                <table class="table table-bordered">
                                    <thead class="bg-red">
                                        <th width="95%">Recursos</th>
                                        <th width="5%"></th>
                                    </thead>
                                    <tbody id="detalleAsigUsers">
                                    </tbody>
                                </table>
                                <div id="cont_asig" class="pull-right" style="margin-top: 10px;">

                                </div>
                            </div>
                
                        </div>
                        
                        <div class="tab-pane" id="tab2">
                            
                            
                            <h3 class="modal-title centro" style="margin-bottom: 15px;">Comentarios</h3>
                            
                            <?php echo form_close(); ?>
                            <div class="spa-12" style="position: relative;">
                                <?php 
                                    $attributes = array('id' =>'form_ViewSolicit_1','name'=>'form_ViewSolicit_1');
                                    echo form_open('procesos/helpdesk/co_102',$attributes); 
                                ?> 
                                <div class="phone-containter">
                                    <div id="idComentsSolic_1" class="phone" style="margin-top: 56px; height: 350px;">
                                    </div>

                                    <div class="send-container" style="width: 85%;top: 0;">
                                      <input type="text" id="msgInput_1" class="form-control" placeholder="Escribir un comentario..." />
                                    </div>
                                    <div style="width: 10%;top: 0;position: absolute;right:0;">
                                        <label>Publico</label><br>
                                        <input type="checkbox" id="public_1" name="public_1"/>
                                    </div>
                                </div>

                                <?php echo form_close(); ?>
                            </div>
                            
                            
                            
                        </div>
                        
                    </div>
                </section>
                
            </div>
            
            <div class="clearfix"></div>
        </div>
        
        
    </div>
  </div>
</div>

<div class="modal" id="modalUsers">
  <div class="modal-dialog">
      
    <div class="modal-header bg-blue">
        <button type="button" onclick="quitarmodalGeneral('Users','form_Users');" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span id="imgUserAsig"></span>  <span id="titleUser"></span></h3>
    </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' => 'form_Users','name'=>'form_Users');
            echo form_open('procesos/facturacion/co_102',$attributes); 
        ?>   
        
        <div class="modal-body">
            
            <div class="spa-12">
                <h3 class="modal-title centro" style="margin-bottom: 15px;">Prioridad de Actividades</h3>
                <ul id="contUsersAsigSelect" class="list-unstyled">
                    
                </ul>
                <div id="cont_asig" class="pull-right">
                    <button type="button" class="btn btn-success actualizaActivi">Guardar Prioridad</button>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>  

<script>
    
    $(document).ready( 
        function () {
            
            var dSinAsignar = eval('<?php echo $SinAsignar; ?>');
            if (dSinAsignar){
                
                var chart1 = new Chart(document.getElementById("chart_s_g").getContext("2d")).Doughnut(dSinAsignar);
                $("#chart_s_g").click(function(evt){
                    var array = chart1.getSegmentsAtEvent(evt);
                    if(!$.isEmptyObject(array)){
                        getSolicit('S',array[0].label,<?php echo $getDpto; ?>);
                    }
                }); 

                var legendHolder = document.createElement('div');
                legendHolder.innerHTML = chart1.generateLegend();
                chart1.chart.canvas.parentNode.parentNode.appendChild(legendHolder.firstChild);
                
            }
            
            var dPorVencer = eval ('<?php echo $PorVencer; ?>');
            if(dPorVencer){
                var chart2 = new Chart(document.getElementById("chart_p_v").getContext("2d")).Doughnut(dPorVencer);
                $("#chart_p_v").click(function(evt){
                    var array = chart2.getSegmentsAtEvent(evt);
                    if(!$.isEmptyObject(array)){
                        getSolicit('P',array[0].label,<?php echo $getDpto; ?>);
                    }
                }); 

                var legendHolder1 = document.createElement('div');
                legendHolder1.innerHTML = chart2.generateLegend();
                chart2.chart.canvas.parentNode.parentNode.appendChild(legendHolder1.firstChild);
            }

        }
    );
    
    $('#form_ViewSolicit_1').bind('submit', function() { 
        var msgText = $('#msgInput_1').val();
        $('#msgInput_1').val('');
        if (msgText != '') addMsg_1(msgText);
        $('#idComentsSolic_1').animate({ scrollTop: $('#idComentsSolic_1').height() }, 600);
        return false;
    });
    
    $('#form_ViewSolicit').bind('submit', function() { 
        var msgText = $('#msgInput').val();
        $('#msgInput').val('');
        if (msgText != '') addMsg(msgText);
        $('#idComentsSolic').animate({ scrollTop: $('#idComentsSolic').height() }, 600);
        return false;
    });

    function addMsg_1(msg) {
        var idSoli = $('#hid_cod_soli').val();
        var public = $("#public_1").is(':checked')?1:0;
        saveComentSolic(msg,idSoli,public);
        var $_a = $('#idComentsSolic_1');
        var row = '<div class="activity">';
        row = row+'<img class="activity_img" src="<?php echo $siteGe; ?>">';
        row = row+'<div class="activity__message">';
        row = row+'<div class="activity__message__header">';
        row = row+'<h4><?php echo $_SESSION['nombre'] ?></h4>';
        row = row+'</span>';
        row = row+'</div>';
        row = row+'<p>'+msg+'</p>';
        row = row+'<span class="timeChat"><?php echo date("d")." ".conveMes(date("n"))." ".date("Y (h:i)"); ?></span>';
        row = row+'</div>';
        row = row+'<div>';
        $_a.append(row);
    }
    
    function addMsg(msg) {
        var idSoli = $('#id_Viewsolic').val();
        var public = $("#public_").is(':checked')?1:0;
        saveComentSolic(msg,idSoli,public);
        var $_a = $('#idComentsSolic');
        var row = '<div class="activity">';
        row = row+'<img class="activity_img" src="<?php echo $siteGe; ?>">';
        row = row+'<div class="activity__message">';
        row = row+'<div class="activity__message__header">';
        row = row+'<h4><?php echo $_SESSION['nombre'] ?></h4>';
        row = row+'</span>';
        row = row+'</div>';
        row = row+'<p>'+msg+'</p>';
        row = row+'<span class="timeChat"><?php echo date("d")." ".conveMes(date("n"))." ".date("Y (h:i)"); ?></span>';
        row = row+'</div>';
        row = row+'<div>';
        $_a.append(row);
    }

</script>