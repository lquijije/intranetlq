
<?php 
$siteGe = 'img/fotos/'.trim($_SESSION['c_e']).'.jpg';
$siteGe = file_exists($siteGe)?$siteGe = site_url('img/fotos/'.trim($_SESSION['c_e']).'.jpg'):$siteGe = site_url('img/fotos/user.png');
?>

<style>
    .col-sm-6,.col-sm-12,.col-sm-1 {
        padding: 0;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    vertical-align: middle;
     border: 1px solid #DDD;
}

</style>
<div class="demo-wrapper">
    <div class="clearfix" style="padding: 5em;">
        <div class="spa-12">
            <section class="content invoice2">
                <button type="button" class="btn-out" onclick="window.location.reload();" title="Refrescar"><i class="fa fa-refresh"></i></button>
                <div class="row">
                    <div class="spa-4" style="max-width: 23.8%; width: auto;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-ticket"></i> Helpdesk Ingreso</div>
                    </div>
                    <div class="spa-2-5" style="max-width: 19.6333%; width: auto;">
                        <?php $attributes = array('id' => 'f_help','name' => 'f_help');   
                            echo form_open('procesos/helpdesk/co_101',$attributes); ?>
                        
                        <ul class="nav navbar-nav">
                            <li class="user-menu">
                                
                                <?php 
                                $data = array(
                                    'dpto' => $getDpto,
                                );
                                
                                echo form_hidden($data);
                                
                                if (isset($dptos) && !empty($dptos)){
                                    
                                    $count = count($dptos);
                                    $arreglo = isset($_SESSION['master'][6]) && !empty($_SESSION['master'][6])?$_SESSION['master'][6]:$arreglo = null;
                                    $select_title="";
                                    $select_items="";
                                    for ($i=0; $i<$count; $i++){
                                        if (trim($getDpto) === trim($arreglo[0][$dptos[$i]]['cod_para'])) {
                                            if ($count === 1){
                                                $select_title ='<a href="#" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.$arreglo[0][$dptos[$i]]['nom_para'].' </span></a><ul class="dropdown-menu">';
                                            } else {
                                                $select_title ='<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.$arreglo[0][$dptos[$i]]['nom_para'].' <i class="caret"></i></span></a><ul class="dropdown-menu">';
                                            }
                                        } else {
                                            $select_items.="<li><a href='javascript:void(0)' onclick='passDpto(\"".trim($arreglo[0][$dptos[$i]]['cod_para'])."\")'>".$arreglo[0][$dptos[$i]]['nom_para']."</a></li>";
                                        }
                                    }
                                    echo $select_title.$select_items.'</ul>';   
                                    ?> 
                                    <?php
                                } else {
                                    ?>
                                        <a href="#" style="padding-top: 0px;">
                                            <span class="titulo-1 text-size-1-5" >No tiene Asignado Departamentos</span>
                                        </a>
                                    <?php
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="row">
                    <div class="spa-4-5">
                        <div class="box box-primary menu-color-6 text-blanco" style="padding: 5px 0 5px 0;">
                            <?php 
                                if(isset($estados) && !empty($estados)){
                                    foreach ($estados as $row){
                                        ?>
                                        <div class="spa-3">
                                            <div class="small-box ">
                                                <div class="inner centro" style="border-right:1px solid rgba(0,0,0,0.1)">
                                                    <i class="fa <?php echo $row['icon']; ?> text-size-1-5"></i>
                                                    <h3 class="centro"><?php echo $row['cantidad']; ?></h3>
                                                    <p><?php echo ucwords(strtolower(str_replace('DE APROBACION','',$row['nom_estado']))); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="spa-12">
                                        <div class="small-box ">
                                            <div class="inner centro">
                                                <h4>No hay Solicitudes</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php  
                                }
                            ?>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="helpdesk_solicitu">
                            <div class="header-fixed" style="width: 36.3%;">
                                <div class="list-group-header">
                                    <span class=" text-size-1-5 titulo-1"> Solicitudes Recientes</span>
                                    <div><a href="javascript:void(0)" class="link btnN" id="<?php echo $getDpto; ?>">Nueva Solicitud</a> | <a href="javascript:void(0)" onclick="modalAllSolicit(<?php echo trim($getDpto) ?>)" class="link">Ver Todas</a></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="list-group" style="padding-top: 69px;">
                                <?php 
                                if(isset($TopSolic) && !empty($TopSolic)){
                                    foreach ($TopSolic as $row){
                                        $color = trim($row['co_tipo']) === 'R'?'menu-color-1':'menu-color-4';
                                        $funcion =  "viewSolic('".$row['co_tipo']."',".$row['co_solicitud'].",'',0)";
                                        $funcionI =  "consulSolici(".$row['co_solicitud'].",'".$row['co_estado']."')";
                                        $linKK = trim($row['co_estado']) === 'I'?$funcionI:$funcion;
                                        ?>
                                        <div class="bs-example list-group-item">
                                            <div class="media">
                                              <div class="media-body">
                                                <span class="span_right"><?php echo date('d', strtotime($row['fe_ingreso']))." ".conveMes(date('n', strtotime($row['fe_ingreso'])))." ".date('Y', strtotime($row['fe_ingreso'])); ?></span>
                                                <div class="help-num text-blanco <?php echo $color; ?>"><i class="fa <?php echo $row['icon']; ?>"></i> <?php echo $row['co_solicitud'] ?></div>
                                                <h4 class="media-heading titulo-1"><a href="javascript:void(0)" onclick="<?php echo trim($linKK); ?>" class="link_1"><?php echo $row['tx_titulo'] ?></a></h4>
                                                <h5 class="media-heading text-gris"><?php echo trim($row['nomb_empl']); ?></h5>
                                              </div>
                                            </div>
                                        </div> 
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="spa-12">
                                        <div class="small-box ">
                                            <div class="inner centro">
                                                <h4>No hay Solicitudes</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php  
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="spa-7-5">
                        
                        <div class="helpdesk_solicitu_1">
                            <div class="header-fixed" style="width: 61.3%;">
                                <div class="list-group-header">
                                    <span class=" text-size-1-5 titulo-1"> <i class="fa fa-comments"></i> Comentarios </span> 
                                    <!--<a href="javascript:void(0)" onclick="modalAllSolicit(<?php echo trim($getDpto) ?>)" class="link">(Ver Todos)</a>-->
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div style="padding-top: 52px;">
                                
                                <div class="activity-list">
                                    <?php 
                                    
                                    if(isset($coment) && !empty($coment)){
                                        foreach($coment as $row){
                                            $site = 'img/fotos/'.trim($row['co_usuario']).'.JPG';
                                            $site = file_exists($site)?site_url('img/fotos/'.trim($row['co_usuario']).'.JPG'):site_url('img/fotos/user.png'); 
                                            if ((int)$row['fl_responder'] === 1){
                                                ?>
                                                <div class="activity">
                                                   
                                                    <img class="activity_img" src="<?php echo $site ?>" />
                                                    <div class="activity__message">
                                                        <div class="activity__message__header">
                                                            <h4><?php echo ucwords(strtolower($row['nombres']." ".$row['apellidos'])) ?> </h4>
                                                            <div style="color: #949494;font-size: 12px;">Ticket #<?php echo $row['co_solicitud']; ?> - <?php echo trim($row['tx_titulo']); ?></div>
                                                            <span class="span_right"><?php echo date('d', strtotime($row['fe_comentario']))." ".conveMes(date('n', strtotime($row['fe_comentario'])))." ".date('Y (h:i)', strtotime($row['fe_comentario'])); ?>
                                                            </span>
                                                        </div>
                                                      <p> <?php echo $row['tx_descripcion']; ?></p>
                                                      <?php
                                                        if (trim($row['co_usuario']) <> trim($_SESSION['c_e'])){
                                                            if(trim($row['co_estado']) <> 'F' && trim($row['co_estado']) <> 'C'){
                                                            ?>
                                                                <a href="javascript:void(0)" onclick="viewSolic('<?php echo $row['co_tipo']; ?>',<?php echo $row['co_solicitud']; ?>,'',0)">Responder</a>
                                                            <?php
                                                            }
                                                        }
                                                      ?>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="activity" style="font-weight: bold;min-height:0px;">
                                                    <div class="activity_img"><i class="fa fa-exclamation-circle"></i></div>
                                                    <div class="activity__message">
                                                        <div class="activity__message__header">
                                                            <span><?php echo date('d', strtotime($row['fe_comentario']))." ".conveMes(date('n', strtotime($row['fe_comentario'])))." ".date('Y (h:i)', strtotime($row['fe_comentario'])); ?></span>
                                                        </div>
                                                      <p> <?php echo $row['tx_descripcion']; ?></p>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }else {
                                        ?>
                                        <div class="spa-12">
                                            <div class="small-box ">
                                                <div class="inner centro">
                                                    <h4>No hay Comentarios</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  
                                    }
                                    
                                    ?>

                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
                <div class="clearfix"></div>
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalSolicit">
  <div class="modal-dialog">
      
    <div class="modal-header bg-red">
        <button type="button" onclick="resetSolici();" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 id="newTitle" class="modal-title">Nueva Solicitud</h3>
    </div>
        
    <div class="modal-content">
        <?php 
            $attributes = array('id' => 'form_Solicit','name'=>'form_Solicit');
            $hidden=array('txt_accion'=>'',
                          'txt_co_estado'=>'',
                          'id_co_solic'=>'',
                          'id_dpto'=>$getDpto);   
            echo form_open('procesos/helpdesk/co_101',$attributes,$hidden); 
        ?>   
        
        <div class="modal-body">
            
            <div class="spa-2">
                <label>Tipo de Solicitud</label>
                <?php echo $selectTipo; ?>
            </div>
            
            <div class="spa-10">
                <label>Titulo</label>
                <input type="text" class="form-control" id="txt_title" name="txt_title" maxlength="50">
            </div>
            
            <div class="spa-12">
                <label>Descripción</label>
                <textarea id="txt_contenido" rows="50"></textarea>
            </div>
            
            <div class="pull-right" style="margin-top: 15px;">
                <button type="reset" class="btn btn-danger resetForm"><i class="fa fa-close"></i> Cancelar</button>
                <button type="button" class="btn btn-danger saveForm" style=" display: none;">Guardar</button>
                <button type="button" class="btn btn-success sendForm"><i class="fa fa-send"></i> Enviar</button>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalViewSolicit">
  <div class="modal-dialog">
      
    <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('ViewSolicit','form_ViewSolicit'),window.location.reload()" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span class="titleSol"></span> - <i class="iconViewSolict" class="fa fa-times"></i>  <span class="titleViewSolict"></span></h3>
    </div>
      
    <div class="modal-content">
        
        <?php 
            $attributes = array('id' =>'form_ViewSolicit','name'=>'form_ViewSolicit');
            $hidden=array('id_Viewsolic'=>'','ti_Viewsolic'=>'');   
            echo form_open('procesos/helpdesk/co_101',$attributes,$hidden); 
        ?>   
        
        <div class="modal-body">
            <div  style="position: relative" class="row">
                
            <div class="spa-8">
                <div class="spa-12 line" style="margin-bottom: 15px; padding-bottom: 15px; position:relative;">
                    <div class="spa-6">
                        <b>Ingreso la solicitud: </b><br>
                        <span class="_ing_sol"></span>
                    </div>
                    <div class="spa-6">
                        <b>Centro de Costo: </b><br>
                        <span class="_ing_cco_sol"></span>
                    </div>
                    <div class="spa-6">
                        <b>Fecha de Solicitud: </b><br>
                        <span class="_ing_fec_sol"></span>
                    </div>
                </div>
                <div class="spa-12">
                    <div class="contViewSolict">
                    </div>
                </div>
            </div>
            
            <div class="spa-4" style="position: relative;">
                <div class="phone-containter">
                    <div id="idComentsSolic" class="phone"></div>
                    <div class="send-container">
                    <input id="msgInput" class="form-control" placeholder="Escribir un comentario..." type="text">
                    </div>
                    <div class="spa-12" style="margin-top: 15px;">
                        <button type="button" class="btn btn-danger cancelSolict pull-right">Anular Solicitud</button>
                        <button type="button" class="btn btn-success finSolict pull-right" style="margin-right: 10px;">Finalizar Solicitud</button>
                    </div>
                </div>
            </div>
                
            </div> 
            
            <div class="clearfix"></div>
        </div>
        
        <div class="clearfix"></div>
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalViewAllSolicit">
  <div class="modal-dialog">
    
      <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('ViewAllSolicit','form_ViewAllSolicit');" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title">Solicitudes</h3>
    </div>
      
    <div class="modal-content">
        
        <?php 
            $attributes = array('id' =>'form_ViewAllSolicit','name'=>'form_ViewAllSolicit');
            echo form_open('procesos/helpdesk/co_101',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div class="spa-2">
                <label>Fecha Inicio</label>
                <div class="input-group">
                    <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                    <input name="txt_fec_ini" value="<?php echo date('Y/m/d', strtotime('-1 day')) ?>" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                    <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="">
                </div>
            </div>

            <div class="spa-2">
                <label>Fecha Final</label>
                <div class="input-group">
                    <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                    <input name="txt_fec_fin" value="<?php echo date('Y/m/d') ?>" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                    <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="">
                </div>
            </div>
            
            <div class="spa-1">
                <label>Solicitud</label>
                <input type="text" id="se_solic" name="se_solic" class="form-control input-sm" value="" onkeypress="return isNumero(event,this)"/>
            </div>
            
            <div class="spa-2">
                <label class="out-text">Buscar</label>
                <button type="button" class="btn btn-primary" onclick="getAllSolici(<?php echo trim($getDpto); ?>)" ><i class="text-red fa fa-search"></i> Buscar</button>
            </div>
            <div class="spa-12">
                <div id="tableAllSolicit">

                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<script>
    
    CKEDITOR.replace('txt_contenido');
    
    $('#form_ViewSolicit').bind('submit', function() {
        var msgText = $('#msgInput').val();
        $('#msgInput').val('');
        if (msgText != '') addMsg(msgText);
        $('#idComentsSolic').animate({ scrollTop: $('#idComentsSolic').height() }, 600);
        return false;
    });

    function addMsg(msg) {
        var idSoli = $('#id_Viewsolic').val();
        saveComentSolic(msg,idSoli,1);
        var $_a = $('#idComentsSolic');
        var row = '<div class="activity">';
        row = row+'<img class="activity_img" src="<?php echo $siteGe; ?>">';
        row = row+'<div class="activity__message">';
        row = row+'<div class="activity__message__header">';
        row = row+'<h4><?php echo $_SESSION['nombre'] ?></h4>';
        row = row+'</span>';
        row = row+'</div>';
        row = row+'<p>'+msg+'</p>';
        row = row+'<span class="timeChat"><?php echo date("d")." ".conveMes(date("n"))." ".date("Y (h:i)"); ?></span>';
        row = row+'</div>';
        row = row+'<div>';
        $_a.append(row);
    }

</script>
