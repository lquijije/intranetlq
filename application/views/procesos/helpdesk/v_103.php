<script src="<?php echo site_url(); ?>js/plugins/chart/Chart.js" type="text/javascript"></script>
<?php 
    $siteGe = 'img/fotos/'.trim($_SESSION['c_e']).'.jpg';
    $siteGe = file_exists($siteGe)?$siteGe = site_url('img/fotos/'.trim($_SESSION['c_e']).'.jpg'):$siteGe = site_url('img/fotos/user.png');
?>
<style>
.col-sm-6,.col-sm-12,.col-sm-1 {
    padding: 0;
}
.doughnut-legend{
    list-style: none;
    position: relative;
}
</style>
<div class="demo-wrapper">
    <div class="clearfix" style="padding: 5em;">
        <div class="spa-12">
            <section class="content invoice">
                <button type="button" class="btn-out" onclick="window.location.reload();" title="Refrescar"><i class="fa fa-refresh"></i></button>
                <div class="row">
                    <div class="spa-4" style="max-width: 23.8%; width: auto;">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-ticket"></i> Helpdesk Atención</div>
                    </div>
                    
                    <div class="spa-2-5" style="max-width: 19.6333%; width: auto;">
                        <?php $attributes = array('id' => 'f_help','name' => 'f_help');   
                            echo form_open('procesos/helpdesk/co_103',$attributes); ?>
                            <ul class="nav navbar-nav">
                                <li class="user-menu">

                                    <?php 

                                    $data = array(
                                        'dpto' => $getDpto,
                                    );

                                    echo form_hidden($data);

                                    if (isset($dptos) && !empty($dptos)){

                                        $count = count($dptos);
                                        $arreglo = isset($_SESSION['master'][6]) && !empty($_SESSION['master'][6])?$_SESSION['master'][6]:$arreglo= null;
                                        $select_title="";
                                        $select_items="";
                                        for ($i=0; $i<$count; $i++){
                                            if (trim($getDpto) === trim($arreglo[0][$dptos[$i]]['cod_para'])) {
                                                if ($count === 1){
                                                    $select_title ='<a href="#" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.$arreglo[0][$dptos[$i]]['nom_para'].' </span></a><ul class="dropdown-menu">';
                                                } else {
                                                    $select_title ='<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 0px;"><span class="titulo-1 text-size-1-5">'.$arreglo[0][$dptos[$i]]['nom_para'].' <i class="caret"></i></span></a><ul class="dropdown-menu">';
                                                }
                                            } else {
                                                $select_items.="<li><a href='javascript:void(0)' onclick='passDpto(\"".trim($arreglo[0][$dptos[$i]]['cod_para'])."\")'>".$arreglo[0][$dptos[$i]]['nom_para']."</a></li>";
                                            }
                                        }
                                        echo $select_title.$select_items.'</ul>';   

                                    } else {
                                        ?>
                                            <a href="#" style="padding-top: 0px;">
                                                <span class="titulo-1 text-size-1-5" >No tiene Asignado Departamentos</span>
                                            </a>
                                        <?php
                                    }
                                    ?>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
                
                <div class="row">
                    <div class="spa-4-5">
                        <div class="box box-primary menu-color-6 text-blanco" style="padding: 5px 0 5px 0;">
                            <?php 
                                if(isset($estados) && !empty($estados)){
                                    foreach ($estados as $row){
                                        ?>
                                        <div class="spa-3">
                                            <div class="small-box ">
                                                <div class="inner centro" style="border-right:1px solid rgba(0,0,0,0.1)">
                                                    <i class="fa <?php echo $row['icon']; ?> text-size-1-5"></i>
                                                    <h3 class="centro"><?php echo $row['cantidad']; ?></h3>
                                                    <p><?php echo ucwords(strtolower(str_replace('DE APROBACION','',$row['nom_estado']))); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="spa-12">
                                        <div class="small-box ">
                                            <div class="inner centro">
                                                <h4>No hay Solicitudes</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php  
                                }
                            ?>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="helpdesk_solicitu">
                            <div class="header-fixed" style="width:36.3%;">
                                <div class="list-group-header">
                                    <span class=" text-size-1-5 titulo-1"> Mis Pendientes</span>
                                    <div><a href="javascript:void(0)" onclick="modalAllActivi(<?php echo trim($getDpto) ?>)" class="link">Ver Todas</a></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="list-group" style="padding-top: 69px;">
                                <?php 
                                if(isset($pendientes) && !empty($pendientes)){
                                    $bandera = false;
                                    foreach ($pendientes as $row){
                                        $bandera = (int)$row['fl_siendo_atendido'] === 1?true:false;
                                        $color = trim($row['co_tipo']) === 'R'?'menu-color-1':'menu-color-4';
                                        $funcion =  "viewSolic('".trim($row['co_tipo'])."',".$row['co_solicitud'].",'".$row['co_actividad']."//".$row['tx_detalle']."//".$row['co_unidad_tiempo']."//".(int)$row['nu_tiempo_estimado']."//".$row['va_progreso']."',1)";
                                        ?>
                                        <div class="bs-example list-group-item <?php echo $bandera == true?'enCurso':''; ?>">
                                            <div class="media">
                                              <div class="media-body">
                                                <span class="span_right">
                                                <?php echo date('d', strtotime($row['fe_ingreso']))." ".conveMes(date('n', strtotime($row['fe_ingreso'])))." ".date('Y', strtotime($row['fe_ingreso'])); ?>
                                                <input type="checkbox" class="radio" id="<?php echo $row['co_actividad']."-".$row['co_solicitud'] ?>" value="<?php echo $row['co_actividad']."-".$row['co_solicitud'] ?>" name="pendien[]" style="position: absolute; right: 0px; top: 15px;"/>
                                                </span>
                                                <div class="help-num text-blanco <?php echo $color; ?>"><i class="fa <?php echo $row['icon']; ?>"></i> <?php echo $row['co_solicitud'] ?></div>
                                                <h4 class="media-heading titulo-1"><a href="javascript:void(0)" onclick="<?php echo trim($funcion); ?>" class="link_1"><?php echo $row['tx_titulo'] ?></a></h4>
                                                <h5 class="media-heading text-gris"><?php echo trim($row['nomb_empl_ing']); ?></h5>
                                                <?php echo trim($row['tx_detalle']) <> ''?'<b>Actividad:'.trim($row['tx_detalle']).'</b>':''; ?>
                                              </div>
                                            </div>
                                        </div> 
                                        <?php

                                        if($bandera == true){
                                            ?>
                                                <script>
                                                    $('#<?php echo $row['co_actividad']."-".$row['co_solicitud'] ?>').prop("checked", true)
                                                </script>
                                            <?php
                                        }
                                    }

                                } else {
                                    ?>
                                    <div class="spa-12">
                                        <div class="small-box ">
                                            <div class="inner centro">
                                                <h4>No hay Actividades Pendientes</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php  
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="spa-4-5">
                        <div class="helpdesk_solicitu_1">
                            <div class="header-fixed" style="width:36.3%;">
                                <div class="list-group-header">
                                    <span class=" text-size-1-5 titulo-1"> <i class="fa fa-comments"></i> Comentarios</span>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div style="padding-top: 52px;">
                                
                                <div class="activity-list">
                                    <?php 
                                    if(isset($coment) && !empty($coment)){
                                        $co_actividad = $tx_detalle = $co_unidad_tiempo = $nu_tiempo_estimado = $va_progreso = '';
                                        foreach($coment as $row){
                                            $subArray = groupArray($pendientes,'co_solicitud');
                                            foreach ($subArray as $row1){
                                                if((int)$row1['co_solicitud'] === (int)$row['co_solicitud']){
                                                    $co_actividad = $row1['groupeddata'][0]['co_actividad'];
                                                    $tx_detalle = $row1['groupeddata'][0]['tx_detalle'];
                                                    $co_unidad_tiempo = $row1['groupeddata'][0]['co_unidad_tiempo'];
                                                    $nu_tiempo_estimado = $row1['groupeddata'][0]['nu_tiempo_estimado'];
                                                    $va_progreso = $row1['groupeddata'][0]['va_progreso'];
                                                }
                                            }
                                            $site = 'img/fotos/'.trim($row['co_usuario']).'.JPG';
                                            $site = file_exists($site)?site_url('img/fotos/'.trim($row['co_usuario']).'.JPG'):site_url('img/fotos/user.png');
                                            if ((int)$row['fl_responder'] === 1){
                                                ?>
                                                <div class="activity">
                                                    <img class="activity_img" src="<?php echo $site ?>" />
                                                    <div class="activity__message">
                                                        <div class="activity__message__header">
                                                            <h4><?php echo ucwords(strtolower($row['nombres']." ".$row['apellidos'])) ?> </h4>
                                                            <div style="color: #949494;font-size: 12px;">Ticket #<?php echo $row['co_solicitud']; ?> - <?php echo trim($row['tx_titulo']); ?></div>
                                                            <span class="span_right"><?php echo date('d', strtotime($row['fe_comentario']))." ".conveMes(date('n', strtotime($row['fe_comentario'])))." ".date('Y (h:i)', strtotime($row['fe_comentario'])); ?>
                                                            </span>
                                                        </div>
                                                      <p> <?php echo $row['tx_descripcion']; ?></p>
                                                      <?php
                                                        if (trim($row['co_usuario']) <> trim($_SESSION['c_e'])){
                                                            if(trim($row['co_estado']) <> 'F' && trim($row['co_estado']) <> 'C'){
                                                            ?>
                                                            <a href="javascript:void(0)" onclick="viewSolic('<?php echo $row['co_tipo']; ?>',<?php echo $row['co_solicitud']; ?>,'<?php echo $co_actividad."//".$tx_detalle."//".$co_unidad_tiempo."//".(int)$nu_tiempo_estimado."//".$va_progreso ?>',1)">Responder</a>
                                                            <?php
                                                            }
                                                        }
                                                      ?>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="activity" style="font-weight: bold;min-height:0px;">
                                                    <div class="activity_img"><i class="fa fa-exclamation-circle"></i></div>
                                                    <div class="activity__message">
                                                        <div class="activity__message__header">
                                                            <span><?php echo date('d', strtotime($row['fe_comentario']))." ".conveMes(date('n', strtotime($row['fe_comentario'])))." ".date('Y (h:i)', strtotime($row['fe_comentario'])); ?></span>
                                                        </div>
                                                      <p> <?php echo $row['tx_descripcion']; ?></p>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                        <div class="spa-12">
                                            <div class="small-box ">
                                                <div class="inner centro">
                                                    <h4>No hay Comentarios</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  
                                    }
                                    ?>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    
                    
                    <div class="spa-3" style="position: relative;">
                        <div class="helpdesk_solicitu_2">
                            <div style="padding: 20px;">
                                <span class="text-size-1-5 titulo-1"> <i class="fa fa-exclamation-circle"></i> Sin Asignar</span>
                            </div>
                            <div class="clear"></div>
                            <div class="list-group" style="padding-top: 10px;">
                                <?php 
                                if($SinAsignar <> 'null'){
                                    ?><canvas id="chart_s_g"/><?php
                                } else {
                                    ?>
                                    <div class="spa-12" style="padding-bottom: 20px;">
                                        <div class="small-box ">
                                            <div class="inner centro">
                                                <h4>No hay Solicitudes</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="spa-3" style="position: relative;">
                        <div class="helpdesk_solicitu_2">
                            <div style="padding: 20px;">
                                <span class="text-size-1-5 titulo-1" > <i class="fa fa-clock-o"></i> Por Vencer</span>
                            </div>
                            <div class="clear"></div>
                            <div class="list-group" style="padding-top: 10px;">
                                <?php 
                                if($PorVencer <> 'null'){
                                    ?><canvas id="chart_p_v" width="320"/><?php
                                } else {
                                    ?>
                                    <div class="spa-12" style="padding-bottom: 20px;">
                                        <div class="small-box ">
                                            <div class="inner centro">
                                                <h4>No hay Solicitudes</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="clearfix"></div>
                
            </section>
        </div>
    </div>
</div>
    
<div class="modal" id="modalViewSolicit">
  <div class="modal-dialog">
      
    <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('ViewSolicit','form_ViewSolicit'),window.location.reload()" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span class="titleSol"></span> - <span class="titleViewSolict"></span></h3>
    </div>
    <div class="modal-content">
        <span>
        <div class="modal-body">
            <div  style="position: relative" class="row">
                
            <div class="spa-8">
                <section>

                    <ul class="nav nav-tabs">
                        <li id="m_n1" class="active"><a href="#tab1" data-toggle="tab">Actividad</a></li>
                        <li id="m_n2"><a href="#tab2" data-toggle="tab">Solicitud</a></li>
                        <li id="m_n3"><a href="#tab3" data-toggle="tab">Asignaci&oacute;n</a></li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="tab1">
                            <div style="margin-bottom: 15px; padding-bottom: 15px; position:relative;" class="spa-12 line">

                                <div id="time_asig_s" class="spa-7">
                                    <h4>Porcentaje de Actividad</h4>
                                    <input type="text" id="porcentaje" value="" name="porcentaje" />
                                </div>

                                <div class="spa-3 time_asig">
                                    <h4>Tiempo Estimado</h4>
                                    <div class="spa-4">
                                        <label>Tiempo</label>
                                        <input id='txt_tiempo' type="text" class="form-control"/>
                                    </div>
                                    <div class="spa-8">
                                        <label>Unidad</label>
                                        <select id="cmb_unidad" class="form-control">
                                            <option value="H" selected="selected">Hora</option>
                                            <option value="D">Día(s)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="spa-2" style="position: absolute; bottom: 16px; right: 0px;">
                                    <button type="button" class="btn btn-primary savePorceTiempo"><i class="text-red fa fa-save"></i> Guardar</button>
                                </div>

                            </div>
                            <div style="margin-bottom: 15px; padding-bottom: 15px; position:relative;" class="spa-12">
                                <div class="spa-12">
                                    <b>Actividad: </b><span id='text_des_act'></span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">

                            <div class="spa-12 line" style="margin-bottom: 15px; padding-bottom: 15px; position:relative;">
                                <div class="spa-6">
                                    <b>Ingreso la solicitud: </b><br>
                                    <span class="_ing_sol"></span>
                                </div>
                                <div class="spa-6">
                                    <b>Centro de Costo: </b><br>
                                    <span class="_ing_cco_sol"></span>
                                </div>
                                <div class="spa-6">
                                    <b>Fecha de Solicitud: </b><br>
                                    <span class="_ing_fec_sol"></span>
                                </div>
                            </div>
                            
                            <div class="spa-12">
                                <div class="contViewSolict" class="spa-12">

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">

                            <div class="spa-12 line" style="margin-bottom: 15px; padding-bottom: 15px; position:relative;">
                                <div class="spa-12">
                                    <h4>Personas Asignadas a esta solicitud</h4>
                                    <ul class="list_asig">
                                        
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </section>
            </div>
            
            
            <div class="spa-4" style="position: relative;">
                  
                <?php 
                    $attributes = array('id' =>'form_ViewSolicit','name'=>'form_ViewSolicit');
                    $hidden=array('id_Viewsolic'=>'',
                                  'id_ViewActi'=>'',
                                  'tipo_sol'=>'',
                                  'id_dpto'=>$getDpto);   
                    echo form_open('procesos/helpdesk/co_103',$attributes,$hidden); 
                ?> 
                <div class="phone-containter">
                    <div id="idComentsSolic" class="phone">
                    </div>

                    <div class="send-container" style="width: 75%;top: 0;">
                      <input type="text" id="msgInput" class="form-control" placeholder="Escribir un comentario..." />
                    </div>
                    <div style="width: 20%;top: 0;position: absolute;right:0;">
                        <label>Publico</label><br>
                        <input type="checkbox" id="public_" name="public_"/>
                    </div>
                </div>
                
                 <?php echo form_close(); ?>
                
            </div>
                
            </div> 
            
            <div class="clearfix"></div>
        </div>
        </span> 
        
    </div>
  </div>
</div>

<div class="modal" id="modalSolicit">
  <div class="modal-dialog">
    <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('Solicit','form_Solicit'),window.location.reload()" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span id="titleDoc"></span></h3>
    </div>
    <div class="modal-content">
        <?php 
            $attributes = array('id' => 'form_Solicit','name'=>'form_Solicit');
            echo form_open('procesos/facturacion/co_102',$attributes); 
        ?>   
        <div class="modal-body">
            <div id="tableSolicit">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clear"></div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalAsigSolicit">
  <div class="modal-dialog">
    <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('AsigSolicit','form_AsigSolicit')" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title"><span id="titleAsigSol"></span> - <i id="iconAsigSolict" class="fa fa-times"></i>  <span id="titleAsigSolict"></span></h3>
    </div>
      
    <div class="modal-content">
        
        <?php 
            $attributes = array('id' => 'form_AsigSolicit','name'=>'form_AsigSolicit');
            echo form_open('procesos/facturacion/co_102',$attributes); 
        ?>   
        <input type="hidden" id="hid_cod_soli" name="hid_cod_soli" value="">
        <input type="hidden" id="hid_tip_soli" name="hid_tip_soli" value="">
        
        <div class="modal-body">
                
            <div class="spa-7" style="border-right: 1px solid #DDD;padding-right: 20px;">
                
                <div class="spa-12 line" style="margin-bottom: 15px; padding-bottom: 15px; position:relative;">
                    <div class="spa-6">
                        <b>Ingreso la solicitud: </b><br>
                        <span class="_ing_sol"></span>
                    </div>
                    <div class="spa-6">
                        <b>Centro de Costo: </b><br>
                        <span class="_ing_cco_sol"></span>
                    </div>
                    <div class="spa-6">
                        <b>Fecha de Solicitud: </b><br>
                        <span class="_ing_fec_sol"></span>
                    </div>
                </div>
                <div class="spa-12">
                    <div id="contAsigSolict">
                    </div>
                </div>
            </div>

            <div class="spa-5">
                <h3 class="modal-title centro" style="margin-bottom: 15px;">Asignación de Usuarios</h3>
                
                <div class="spa-11">
                    <input type="hidden" id="cod_Empl_Asig" name="cod_Empl_Asig" value="">
                    <?php echo $Select; ?>
                </div>
                
                <div class="spa-1">
                    <button type="button" class="btn btn-square btn-danger add_option"><i class="text-blanco fa fa-plus"></i></button>
                </div>
                
                <div class="spa-12">
                    <textarea id="ObsevaUsua" name="ObsevaUsua" class="form-control" style="margin-top: 5px;" placeholder="Observación"></textarea>
                </div>
                
                <div class="spa-12" style="margin-top: 10px;">
                    <table class="table table-bordered">
                        <thead class="bg-red">
                            <th width="95%">Asignación</th>
                            <th width="5%"></th>
                        </thead>
                        <tbody id="detalleAsigUsers">
                        </tbody>
                    </table>
                    <div id="cont_asig" class="pull-right" style="margin-top: 10px;">
                        
                    </div>
                </div>
                
            </div>
            
            <div class="clearfix"></div>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>
    
<div class="modal" id="modalViewAllSolicit">
  <div class="modal-dialog">
      
    <div class="modal-header bg-red">
        <button type="button" onclick="quitarmodalGeneral('ViewAllSolicit','form_ViewAllSolicit');" class="close">
            <i class="fa fa-times-circle"></i>
            <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title">Actividades Pendientes Asignadas</h3>
    </div>

    <div class="modal-content">
        <?php 
            $attributes = array('id' =>'form_ViewAllSolicit','name'=>'form_ViewAllSolicit');
            echo form_open('procesos/helpdesk/co_103',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div class="spa-2">
                <label>Fecha Inicio</label>
                <div class="input-group">
                    <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                    <input name="txt_fec_ini" value="<?php echo date('Y/m/d', strtotime('-1 day')) ?>" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                    <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="">
                </div>
            </div>

            <div class="spa-2">
                <label>Fecha Final</label>
                <div class="input-group">
                    <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                    <input name="txt_fec_fin" value="<?php echo date('Y/m/d') ?>" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                    <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="">
                </div>
            </div>
            
            <div class="spa-2">
                <label class="out-text">Buscar</label>
                <button type="button" class="btn btn-primary" onclick="getAllActiv(<?php echo trim($getDpto); ?>)" ><i class="text-red fa fa-search"></i> Buscar</button>
            </div>
            
            <div class="spa-12" style="margin-top: 15px;">
                <div id="tableAllSolicit">

                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<script>
    
    $('#form_ViewSolicit').bind('submit', function() { 
        var msgText = $('#msgInput').val();
        $('#msgInput').val('');
        if (msgText != '') addMsg(msgText);
        $('#idComentsSolic').animate({ scrollTop: $('#idComentsSolic').height() }, 600);
        return false;
    });

    function addMsg(msg) {
        var idSoli = $('#id_Viewsolic').val();
        var public = $("#public_").is(':checked')?1:0;
        saveComentSolic(msg,idSoli,public);
        var $_a = $('#idComentsSolic');
        var row = '<div class="activity">';
        row = row+'<img class="activity_img" src="<?php echo $siteGe; ?>">';
        row = row+'<div class="activity__message">';
        row = row+'<div class="activity__message__header">';
        row = row+'<h4><?php echo $_SESSION['nombre'] ?></h4>';
        row = row+'</span>';
        row = row+'</div>';
        row = row+'<p>'+msg+'</p>';
        row = row+'<span class="timeChat"><?php echo date("d")." ".conveMes(date("n"))." ".date("Y (h:i)"); ?></span>';
        row = row+'</div>';
        row = row+'<div>';
        $_a.append(row);
    }
    
    $(document).ready( 
        function () {
            
            $("input:checkbox[name='pendien[]']").on('click', function() {
                $('.list-group-item').removeClass('enCurso');
                var $box = $(this);
                var i = 0;
                if ($box.is(":checked")) {
                    var group = "input:checkbox[name='pendien[]']";
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                    $(this).parents("div:first").parents("div:first").parents("div:first").addClass('enCurso');
                    i=1;
                } else {
                    $box.prop("checked", false);
                    $(this).parents("div:first").parents("div:first").parents("div:first").removeClass('enCurso');
                    i=2;
                }
                var a = $box.attr("value").split('-');
                enCurso(i,a[0],a[1]);
            });
            
            var ancDiv = $(".spa-3-5").width();
            $('#chart_p_v').attr('width',parseInt(ancDiv)-10);
            $('#chart_s_g').attr('width',parseInt(ancDiv)-10);
            
            var dSinAsignar = eval('<?php echo $SinAsignar; ?>');
            if (dSinAsignar){
                
                var chart1 = new Chart(document.getElementById("chart_s_g").getContext("2d")).Doughnut(dSinAsignar);
                $("#chart_s_g").click(function(evt){
                    var array = chart1.getSegmentsAtEvent(evt);
                    if(!$.isEmptyObject(array)){
                        getSolicit('S',array[0].label,<?php echo $getDpto; ?>);
                    }
                }); 

                var legendHolder = document.createElement('div');
                legendHolder.innerHTML = chart1.generateLegend();
                chart1.chart.canvas.parentNode.parentNode.appendChild(legendHolder.firstChild);
                
            }
            
            var dPorVencer = eval ('<?php echo $PorVencer; ?>');
            if(dPorVencer){
                var chart2 = new Chart(document.getElementById("chart_p_v").getContext("2d")).Doughnut(dPorVencer);
                $("#chart_p_v").click(function(evt){
                    var array = chart2.getSegmentsAtEvent(evt);
                    if(!$.isEmptyObject(array)){
                        getSolicit('P',array[0].label+"s",<?php echo $getDpto; ?>);
                    }
                }); 

                var legendHolder1 = document.createElement('div');
                legendHolder1.innerHTML = chart2.generateLegend();
                chart2.chart.canvas.parentNode.parentNode.appendChild(legendHolder1.firstChild);
            }

        }
    );
    

</script>


