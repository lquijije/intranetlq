<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice5">
                
                <div class="row" style="margin-bottom: 2px;">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-2 text-blanco">Aprobar</span>
                        <small class="text-size-1 text-gris">Requisición</small>
                    </div>
                    
                    <?php $this->load->view('toolbar/toolbar'); ?>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">

                                <div class="spa-2">
                                    <label>Fecha Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_ini" value="<?php echo date('Y/m/d', strtotime('-7 day')); ?>" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                        <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="">
                                    </div>
                                </div>

                                <div class="spa-2">
                                    <label>Fecha Final</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_fin" value="<?php echo date('Y/m/d'); ?>" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                        <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="">
                                    </div>
                                </div>

                                <div class="spa-2">
                                    <label>Estado</label>
                                    <select id="cmb_estado" name="cmb_estado" class="form-control input-sm">
                                        <option value="G">Enviada a Aprobaci&oacute;n</option>
                                        <option value="A">Aprobada</option>
                                        <option value="N">Negada</option>
                                        <option value="GAN">Todos</option>
                                    </select>
                                </div>

                                <div class="spa-5">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchAprobar"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">
                        <div id="tableAprobar" class="row" style="padding-left: 17px; padding-right: 17px;">

                        </div>
                        <button type="button" class="btn btn-primary right-btn aprobar"><i class="text-red fa fa-check"></i> Guardar</button>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>
