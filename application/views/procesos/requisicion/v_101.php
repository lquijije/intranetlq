
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice5">
                
                <div class="row" style="margin-bottom: 4px;">
                    <div class="pull-left">
                        <span class="titulo-1 text-size-2 text-blanco">Ingreso</span>
                        <small class="text-size-1 text-gris">Requisición</small>
                    </div>
                    
                    <?php $this->load->view('toolbar/toolbar',$urlControlador); ?>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-2">
                                    <label>Fecha Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_ini" value="<?php echo date('Y/m/d', strtotime('-7 day')); ?>" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                        <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="">
                                    </div>
                                </div>

                                <div class="spa-2">
                                    <label>Fecha Final</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="txt_fec_fin" value="<?php echo date('Y/m/d'); ?>" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control input-sm" type="text">
                                        <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="">
                                    </div>
                                </div>
                                
                                <div class="spa-2">
                                    <label>Estado</label>
                                    <select id="cmb_estado" name="cmb_estado" class="form-control input-sm">
                                        <option value="I">Ingresada</option>
                                        <option value="C">Enviada a Cotizar</option>
                                        <option value="G">Enviada a Aprobaci&oacute;n</option>
                                        <option value="A">Aprobada</option>
                                        <option value="N">Negada</option>
                                        <option value="ICGAN">Todos</option>
                                    </select>
                                </div>

                                <div class="spa-5">
                                    <label class="out-text">Buscar</label>
                                    <button type="button" class="btn btn-primary searchRequi"><i class="text-red fa fa-search"></i> Buscar</button>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">
                        <div id="tableRequisicion" class="row" style="padding-left: 17px; padding-right: 17px;">

                        </div>
                        <button type="button" class="btn btn-primary right-btn aprueba"><i class="text-red fa fa-check"></i> Enviar a Cotizar</button>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>


<div class="modal" id="modalRequisicion">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" onclick="quitarmodalGeneral('Requisicion','form_ing_req');" class="close"><i class="fa fa-times-circle"></i><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Nueva Requisición</h4>
        </div>
        
        <?php 
            $attributes = array('id' => 'form_ing_req','name'=>'form_ing_req');
            echo form_open('procesos/requisicion/guardar',$attributes); 
        ?>   
        <div class="modal-body">
            <div class="spa-12">
                <label>Dispositivo</label>
                <textarea id="txt_dispositivo" name="txt_dispositivo" class="form-control" maxlength="1000"></textarea>
            </div>
            <div class="spa-12">
                <label>Destino</label>
                <textarea id="txt_destino" name="txt_destino" class="form-control"></textarea>
            </div>
            <div class="spa-3">
                <label>Cantidad</label>
                <input id="txt_cantidad" name="txt_cantidad" value="" onkeypress="return isNumero(event,this)" class="form-control input-sm" type="text">
            </div>
        </div>
        <div class="clear"></div>
        <div class="modal-footer">
          <button type="reset" class="btn btn-default resetRequi">Cancelar</button>
          <button type="button" class="btn btn-primary saveRequi">Guardar</button>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalAprobado">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" onclick="quitarmodalGeneral('modalAprobado','form_ing_req');" class="close"><i class="fa fa-times-circle"></i><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Nueva Requisición</h4>
        </div>
        
        <?php 
            $attributes = array('id'=>'form_env_apro','name'=>'form_env_apro');
            echo form_open('procesos/requisicion/guardar',$attributes); 
        ?>
        
        <div class="modal-body">
            <input type="hidden" id="txt_cod_cot" name="txt_cod_cot">
            <div class="spa-12">
                <label>Proveedor</label>
                <textarea id="txt_proveedor" name="txt_proveedor" class="form-control"></textarea>
            </div>
            <div class="spa-3">
                <label>Precio</label>
                <input id="txt_precio" name="txt_precio" value="" onkeypress="return isDecimal(event,this)" class="form-control input-sm" type="text">
            </div>
        </div>
        
        <div class="clear"></div>
        <div class="modal-footer">
          <button type="reset" class="btn btn-default resetAprobado">Cancelar</button>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>