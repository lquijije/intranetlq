<style>
    .thumbnail{
        position: relative;
        background: #ffffff;
        margin-bottom: 10px !important;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        width: 100%;
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
        border-top: 2px solid #FFF;
        display:block;
        padding:4px;
    }
    .img-thumbmail{
        height:100px;
        line-height:100px;
        margin:0px auto;
        text-align:center;
    }
    .img-thumbmail img {
        vertical-align: middle;
        height: 100px;
    }
    .thumbnail>img,.thumbnail a>img{margin-right:auto;margin-left:auto}
    a.thumbnail:hover,a.thumbnail:focus,a.thumbnail.active{border-color:#428bca}
    .thumbnail .caption{padding:9px;color:#333}
    .thumbnail .caption p {
        margin:0;
        font-size: 12px;
        border-top: 1px solid #ccc;
        white-space: nowrap;
        width: 100%;                   
        overflow: hidden;             
        text-overflow: ellipsis;
    }
    .thumbnail a > img {
        height: 100px !important;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <section class="content invoice8">
            <div class="row-fluid">
                <div class="col-xs-12" style="margin-bottom:10px;">
                    <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-upload"></i> Subir imagenes</div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="col-xs-12">
                            <div><i class="fa fa-search"></i> Buscar Imagenes</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="col-inside-lg time decor-primary color-1 sale">
                                <label>Tipo</label>
                                <select class="form-control" id="cmb_tipo">
                                    <option value="C">C&oacute;digo</option>
                                    <option value="N">Descripci&oacute;n</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <div class="col-inside-lg time decor-primary color-1 sale">
                                <label>Busqueda</label>
                                <div class="input-group">
                                    <input id="txt_busqueda" name="txt_busqueda" type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-danger searchInventar" type="button" style="height: 33px;"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-body">
                    <div class="col-xs-12">
                        <table id="contTable" class="table table-bordered TFtable"></table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
</div>

<script src="<?php echo site_url('js/plugins/lightbox/js/lightbox.js'); ?>" type="text/javascript"></script>

<div class="modal" id="modalArtFind">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-red">
                <button type="button" onclick="quitarmodalGeneral('ArtFind', '');" class="close">
                    <span aria-hidden="true"><i class="fa fa-times-circle"></i></span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title"><span id="desArt"></span></h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid" style="margin-top:5px; ">
                    <span class="btn btn-upload btn-success" style="border-radius: 12px;">
                        <input type="hidden" id="hiArt">
                        <input onchange="fileread(this.files)" name="files[]" id="file_img" type="file" multiple="multiple"/>
                        <i class="fa fa-plus"></i> A&ntilde;adir imagenes
                    </span>
                    <div class="clearfix"></div>
                </div>
                <div class="row-fluid">
                    <h4><i class="fa fa-img"></i>Imagenes subidas:</h4>
                    <div id="imgsArt"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

