<style>
    .thumbnail{
        position: relative;
        background: #ffffff;
        margin-bottom: 10px !important;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        width: 100%;
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
        border-top: 2px solid #FFF;
        display:block;
        padding:4px;
    }
    .img-thumbmail{
        height:100px;
        line-height:100px;
        margin:0px auto;
        text-align:center;
    }
    .img-thumbmail img {
        vertical-align: middle;
        height: 100px;
    }
    .thumbnail>img,.thumbnail a>img{margin-right:auto;margin-left:auto}
    a.thumbnail:hover,a.thumbnail:focus,a.thumbnail.active{border-color:#428bca}
    .thumbnail .caption{padding:9px;color:#333}
    .thumbnail .caption p {
        margin:0;
        font-size: 12px;
        border-top: 1px solid #ccc;
        white-space: nowrap;
        width: 100%;                   
        overflow: hidden;             
        text-overflow: ellipsis;
    }
    .thumbnail a > img {
        height: 100px !important;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <section class="content invoice">
            <div class="row-fluid">
                <div class="col-xs-12">
                    <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-upload"></i> Subir imagenes</div>
                </div>
            </div>
            <div class="row-fluid" style="padding-top: 10px;">
                <div class="col-xs-2">
                    <span class="btn btn-upload btn-danger" style="border-radius: 12px;">
                        <input onchange="fileread(this.files)" name="files[]" id="file_img" type="file" multiple="multiple"/>
                        <i class="fa fa-plus"></i> A&ntilde;adir imagenes
                    </span>
                    <div class="clearfix"></div>
                </div>
                <button class="btn btn-success" style="border-radius: 12px;"><i class="fa fa-times"></i>Eliminar Imagenes</button>
            </div>
            <div class="row-fluid" style="padding-top: 10px;">
                <div>
                    <?php
                    if (empty($imgs['error']) && !empty($imgs['data'])) {
                        echo $imgs['data'];
                    }
                    ?>
                </div>
                <div id="addFile" style="clear: both; margin-top: 10px;">
                </div>
            </div>
        </section>
    </div>
</div>
<script src="<?php echo site_url('js/plugins/lightbox/js/lightbox.js'); ?>" type="text/javascript"></script>

