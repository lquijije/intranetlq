<script>
    $('#bodyPage').css('background','url(<?php echo site_url(); ?>/img/back.jpg) top left no-repeat');
    $('#bodyPage').css('background-attachment','fixed');
    $('#bodyPage').css('background-size','cover');
    $('.header').css('background','none');
</script>

<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .fancybox-margin {
        margin-right: auto !important;
    }
</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                <div class="col-xs-2 col-xs-offset-5">
                    <a href="<?php echo site_url(); ?>">
                        <img src="<?php echo site_url('img/logo-precio-record.png'); ?>">
                    </a>
                </div>
                
                <div class="clearfix"></div>
                
                <div class="row" style="padding-top: 20px;">
                    <div class="box precio_record" style="padding: 0px 5px 0px 15px;">
                        <div class="box-body">
                            <div class="row">
                                <div class="spa-6" style="margin-bottom: 0px;">
                                    <div class="pull-left">
                                        <span class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-code"></i> Promociones</span>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-success newPromo" style="position: absolute; right: 0;margin: 0.2% 0.6%;"><i class="fa fa-save"></i> Nueva Promoci&oacute;n</button>
                            </div>

                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">   
                        <table class="table table-bordered" id="tab_promo">
                            <thead>
                                <tr>
                                    <td>Cod. Promoci&oacute;n</td>
                                    <td>Cod. Art&iacute;culo</td>
                                    <td>Nombre Art&iacute;culo</td>
                                    <td>Compartido</td>
                                    <td>Fecha Inicial</td>
                                    <td>Fecha Final</td>
                                    <td>Estado</td>
                                    <td>Modificar</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(isset($promos) && !empty($promos)){
                                foreach ($promos as $row){
                                    ?>
                                    <tr>
                                        <td><?php echo $row['co_pro']; ?></td>
                                        <td><?php echo $row['co_art']; ?></td>
                                        <td><?php echo utf8_encode($row['ds_art']); ?></td>
                                        <td class="centro"><?php echo $row['cn_sha']; ?></td>
                                        <td class="centro"><?php echo $row['fe_ini']; ?></td>
                                        <td class="centro"><?php echo $row['fe_fin']; ?></td>
                                        <td class="centro"><?php echo $row['es_pro']; ?></td>
                                        <td class="centro"><button type="button" class="btn btn-circle btn-warning" onclick="viewPro(<?php echo $row['co_pro']; ?>,'U');"><i class="text-blanco text-size-1 fa fa-edit"></i></button></td>
                                    </tr>
                                    <?php 
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalNewPro">
    <div class="modal-dialog">
        
        <div class="modal-header bg-red">
            <button type="button" onclick="quitarmodalGeneral('NewPro','form_NewPro');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title"><span id="name_pro"></span>Nueva Promoci&oacute;n</h3>
        </div>
        
        <div class="modal-content">
            <?php 
                $attributes = array('id' =>'form_NewPro','name'=>'form_NewPro');
                echo form_open('procesos/pyccacom/co_120',$attributes); 
                
                $data = array(
                    'co_acc' =>'',
                    'co_pro' =>''
                );

                echo form_hidden($data);
            ?>   
            
            <div class="modal-body">
                <div class="spa-12">
                    
                    <div class="spa-4">
                        <label>C&oacute;digo del Art&iacute;culo</label>
                        <input id="txt_co_art" name="txt_co_art" type="text" onblur="getArt(this.value);" value="" class="form-control">
                    </div>
                    
                    <div class="spa-8">
                        <label>Descripci&oacute;n del Art&iacute;culo</label>
                        <input id="txt_ds_art" type="text" value="" class="form-control">
                    </div>
                    
                    <div class="spa-4">
                        <label>Precio Inicial</label>
                        <input id="txt_pr_ini" name="txt_pr_ini" type="text" value="" class="form-control" onkeypress="return isNumero(event,this)">
                    </div>
                    
                    <div class="spa-4">
                        <label>Precio Tope</label>
                        <input id="txt_pr_top" name="txt_pr_top" type="text" value="" class="form-control" onkeypress="return isNumero(event,this)">
                    </div>
                    
                    <div class="spa-4">
                        <label>Cantidad de Share</label>
                        <input id="txt_cn_sha" name="txt_cn_sha" type="text" value="" class="form-control" onkeypress="return isNumero(event,this)">
                    </div>
                    
                    <div class="spa-4">
                        <label>Fecha Inicio</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input name="txt_fec_ini" value="" id="txt_fec_ini" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control" type="text">
                            <input type="hidden" id="val_fec_ini" name="val_fec_ini" value="">
                        </div>
                    </div>
                    
                    <div class="spa-4">
                        <label>Fecha Final</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input name="txt_fec_fin" value="" id="txt_fec_fin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control" type="text">
                            <input type="hidden" id="val_fec_fin" name="val_fec_fin" value="">
                        </div>
                    </div>
                    
                    <div class="spa-4">
                        <label>Estado</label>
                        <select id="cmb_est" class="form-control">
                            <option value="A">Activo</option>
                            <option value="I">Inactivo</option>
                        </select>
                    </div>
                    
                    <div class="spa-6" style="padding-top: 5px;">
                        <div id="post_img" class="targetLayer">Imagen</div>
                        <div class="uploadFormLayer">
                            <label>
                            Imagen de la Promoci&oacute;n
                            </label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary btn-file" style="padding: 7px;">
                                        <i class="text-red fa fa-search"></i>Buscar <input id="img_post" type="file" name="img_post" />
                                    </span>
                                </span>
                                <input id="img_post_name" type="text" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="spa-6" style="padding-top: 5px;">
                        <div id="face_img" class="targetLayer">Imagen</div>
                        <div class="uploadFormLayer">
                            <label>
                            Imagen de Facebook
                            </label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary btn-file" style="padding: 7px;">
                                        <i class="text-red fa fa-search"></i>Buscar <input id="img_face" type="file" name="img_face" />
                                    </span>
                                </span>
                                <input id="img_face_name" type="text" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    
                    <div class="spa-12" style="margin-top: 15px;">
                        <button type="button" class="btn btn-primary savPro pull-right"><i class="text-red fa fa-save"></i> Guardar</button>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            <?php echo form_close(); ?>
        </div>
        
    </div>
</div>

<script>
$('.fancybox').fancybox();
function archivo(input,tipo) {
    if (input.files[0]) {
        var reader = new FileReader();
        reader.onload = (function(theFile) {
            return function(e) {
                if (tipo.trim() === '1'){
                    document.getElementById("post_img").innerHTML = ['<a class="fancybox" href="', e.target.result,'" title="',escape(theFile.name),'"><img class="example-image" width="100px" height="100px" src="', e.target.result,'" title="', escape(theFile.name), '"/></a>'].join('');
                } else if (tipo.trim() === '2'){
                    document.getElementById("face_img").innerHTML = ['<a class="fancybox" href="', e.target.result,'" title="',escape(theFile.name),'"><img class="example-image" width="100px" height="100px" src="', e.target.result,'" title="', escape(theFile.name), '"/></a>'].join('');
                } 
            };
        })(input.files[0]);
        reader.readAsDataURL(input.files[0]);
    }
}

$("#img_post").change(function(){ archivo(this,'1'); });
$("#img_face").change(function(){ archivo(this,'2'); });
</script>