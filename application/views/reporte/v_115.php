
<style>
    
#cols {
    -webkit-column-count: 3;
    -moz-column-count: 3;
    -webkit-column-gap: 10px;
    -moz-column-gap: 10px;
    column-gap: 10px;
}


a:hover {
    color:#ccc;
}

.apa {
    width: 50px;
    height: 50px;
    text-align: center;
    line-height: 50px;
}

#myContent{
    -moz-column-count: 5;
    -moz-column-gap: 0px;
    -webkit-column-count: 5;
    -webkit-column-gap: 0px;
    column-count: 5;
    column-gap: 0px;
}

#myContent li{
    display: inline-block;
    margin-bottom: 0px;
    width: 100%;
    margin-top: 0px;
    padding: 10px;
}


#item{
    padding:10px;
    border:solid 4px transparent;
    background:#eee;
    -moz-background-clip: padding;
    -webkit-background-clip: padding;
    background-clip: padding-box;
}

@media only screen and (max-width : 1199px),
only screen and (max-device-width : 1199px){
    #myContent{
        -moz-column-count: 5;
        -moz-column-gap: 0px;
        -webkit-column-count: 5;
        -webkit-column-gap: 0px;
        column-count: 5;
        column-gap: 0px;
    }

}

@media only screen and (max-width : 999px),
only screen and (max-device-width : 999px){
    #myContent{
            -moz-column-count: 4;
            -moz-column-gap: 0px;
            -webkit-column-count: 4;
            -webkit-column-gap: 0px;
            column-count: 4;
            column-gap: 0px;
    }

}

@media only screen and (max-width : 799px),
only screen and (max-device-width : 799px){
    #myContent{
            -moz-column-count: 3;
            -moz-column-gap: 0px;
            -webkit-column-count: 3;
            -webkit-column-gap: 0px;
            column-count: 34;
            column-gap: 0px;
    }

}

@media only screen and (max-width : 599px),
only screen and (max-device-width : 599px){
    #myContent{
            -moz-column-count: 2;
            -moz-column-gap: 0px;
            -webkit-column-count: 2;
            -webkit-column-gap: 0px;
            column-count: 2;
            column-gap: 0px;
    }

}

@media only screen and (max-width : 399px),
only screen and (max-device-width : 399px){
    #myContent {
            -moz-column-count: 1;
            -moz-column-gap: 0px;
            -webkit-column-count: 1;
            -webkit-column-gap: 0px;
            column-count: 1;
            column-gap: 0px;
    }

}

</style>

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice1">
                
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-line-chart"></i> Reporting Services</div>
                    </div>
                </div>
                
                <ul id="myContent" style="margin-top: 10px;">
                    
                    <?php 
                    
                        if (!empty($reportes)){
                            
                            $abc = '';
                            $i = 0;
                            
                            foreach ($reportes as $row){
                               
                                $ini = strtoupper(substr(trim($row['tx_descripcion']),0,1));
                                
                                if ($abc <> $ini){  
                                    $i++;
                                    $i = $i >= 10?1:$i;
                                ?>
                                    
                                    <li class="item">
                                        <h1 class="text-blanco apa menu-color-<?php echo $i; ?>" style="font-weight: bold;"><?php echo $ini; ?></h1>

                                        <?php  foreach ($reportes as $raw){
                                            $link = trim($raw['tx_rutaejecutable_redirect']) <> ''?trim($raw['tx_rutaejecutable_redirect']):trim($raw['tx_rutaejecutable']);
                                            if (strtoupper(substr(trim($raw['tx_descripcion']),0,1)) === $ini){ ?>
                                                <a class="postlink" href="<?php echo $link ?>" target="_blank"><i class="fa fa-arrow-right"></i> <?php echo trim($raw['tx_descripcion']); ?></a><br>
                                            <?php }

                                        } ?>

                                    </li>

                                <?php } 

                                $abc = $ini;

                            }

                        }
                    
                    ?>
                    
                </ul>
                
                <div class="clearfix"></div>
                
            </section>
        </div>
    </div>
</div>
