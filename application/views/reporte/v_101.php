
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice1">
                
                <div class="row">
                    <div class="spa-12">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-bar-chart"></i> Contador de Personas</div>
                    </div>
                </div>
                
                <div class="row" style="margin-top: 10px;">
                    <div class="spa-2 box box-primary" style="padding:15px;">
                        <div class="spa-12">
                            <label>Almacén</label>
                            <?php echo $cmb_bodega; ?>
                        </div>
                        <div class="spa-12">
                            <label>Día</label>
                            <input name="txt_fec_ini" value="<?php echo date('Y/m/d',strtotime('-1 day')); ?>" id="txt_fec_ini" data-date-format="yyyy-mm-dd" class="form-control dp" type="text">
                        </div>
                        <div class="spa-12" style="margin-top: 10px;">
                            <button type="button" class="pull-right btn btn-primary btn-process search_chart"><i class="fa fa-search text-red"></i> Buscar</button>
                        </div>
                    </div>
                    <div class="spa-10" style="background: #FFF; padding:10px;box-shadow:0px 0px 0px 5px #DCDCDC inset;">
                        <div id="placeholder" style="height:500px;">
                            <div style="margin-left: auto;margin-right: auto; width: 300px; text-align: center;padding-top: 130px;">
                                <h1 id="textChart" style="font-weight: bold;color:#C9C9C9;">Area de Grafico</h1>
                                <i class="fa fa-bar-chart" style="color:#DCDCDC;font-size: 10em;"></i>
                            </div>
                        </div>
                        <div id="tableChart" class="spa-12" style="padding: 0 25px; font-size: 11px;">
                            
                        </div>
                    </div>
                        
                </div>
                
                <div class="clearfix"></div>
                
            </section>
        </div>
    </div>
</div>
