<style>
	#imaginary_container{
		/*margin-top:20%;  Don't copy this */
	}
	.stylish-input-group .input-group-addon{
		background: white !important; 
	}
	.stylish-input-group .form-control{
		border-right:0; 
		box-shadow:0 0 0; 
		border-color:#ccc;
	}
	.stylish-input-group button{
		border:0;
		background:transparent;
	}
	.glyphicon-refresh-animate {
		-animation: spin .7s infinite linear;
		-webkit-animation: spin2 .7s infinite linear;
	}

	@-webkit-keyframes spin2 {
		from { -webkit-transform: rotate(0deg);}
		to { -webkit-transform: rotate(360deg);}
	}

	@keyframes spin {
		from { transform: scale(1) rotate(0deg);}
		to { transform: scale(1) rotate(360deg);}
	}
	.close-modal-iv{
		float: right;
		font-size: 21px;
		font-weight: bold;
		line-height: 1;
		text-shadow: 0 1px 0 #ffffff;
		opacity: 0.5;
		filter: alpha(opacity=50);
		color: #FFFFFF
		/*padding-left:5px;*/
	}
	.close-modal-iv:hover,
	.close-modal-iv:focus {
		text-decoration: none;
		cursor: pointer;
		opacity: 0.9;
		filter: alpha(opacity=90);
		color: #FFFFFF
	}
	button.close-modal-iv {
		padding: 0;
		cursor: pointer;
		background: transparent;
		border: 0;
		-webkit-appearance: none;
	}
	.dropdown-menu.pull-left {
		left: 0;
		right: auto;
	}
	.div-flex{
		display: -webkit-flex;
		display: flex;
	}
	.div-flex-align-center{
		-webkit-align-items: center;
		align-items: center;
	}
	.div-flex-align-end{
		-webkit-align-items: flex-end;
		align-items: flex-end;
	}
	.div-flex-just-center{
		-webkit-justify-content: center;
		justify-content: center;
	}
	.div-flex-just-left{
		-webkit-justify-content: left;
		justify-content: left;
	}
}
</style>
<input type='hidden' id='hdMdlAct' name="txt_modal_acti" 		value="chkBySal" />
<input type='hidden' id='hdRadAct' name="txt_modal_acti" 		value="" />
<input type='hidden' id='hdFechSL' name="txt_codig_pedi" 		value=<?php echo $hoy?> />
<input type='hidden' id='hdModTrn' name="txt_modo_trnsp" 		value="" />

<input type='hidden' id='hdCodBod' name="txt_codig_bode" 		value="" />
<input type='hidden' id='hdCodPed' name="txt_codig_pedi" 		value="" />
<input type='hidden' id='hdBodLle' name="txt_bodeg_lleg" 		value="" />
<input type='hidden' id='hdNumIte' name="txt_numer_item" 		value="" />
<input type='hidden' id='hdStockB' name="txt_stock_basi" 		value="" />
<input type='hidden' id='hdTipBod' name="txt_tipo_bodeg" 		value="" />
<input type='hidden' id='hdBlomba' name="txt_codi_blomb" 		value="" />

<div class="demo-wrapper">
	<div class="dashboard">
		<div class="container" >
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
						<span class="glyphicon glyphicon-time">
						</span> Marcar Fecha Salida-LLegada de Camiones
					</h4>
				</div>
				<div class="modal-body" style="padding: 1%;">
					<div class="col-sm-12">
						<div class="row div-flex">
							<div class="col-sm-2 panel panel-primary">
								<div class="radio radio-primary" style="margin-top: 6%;">
									<input id="chkBySal" type="radio" name="radio-camion" checked="">
									<label for="chkBySal">Salida de Cami&oacuten</label>
								</div>
								<div class="radio radio-primary" style="margin-bottom: 2%;">
									<input id="chkByLle" type="radio" name="radio-camion">
									<label for="chkByLle">Llegada de Cami&oacuten</label>
								</div>
							</div>
							<div class="col-sm-10 panel panel-primary" style="padding-top: 6px;">
								<div class="row div-flex">
									<div class="col-sm-1">
										<label>Despacha:</label>
									</div>
									<div class="col-sm-3">
										<div class="input-group">
											<label class="form-control lbBodDesp" style="height: 25px"></label>
											<div class="input-group-addon bg-primary text-blanco no-margin no-padding" style="border-radius: 5px;">
												<a href="#" data-toggle="tooltip" title="Buscar Bodega" data-placement="right">
													<button type="button" class="btn btn-primary bg-primary btBDesp" style="height: 25px;padding-top: 1px;">
														<i class="fa fa-search-plus"></i>
													</button>
												</a>
											</div>											
										</div>
									</div>
									<div class="col-sm-8">
										<div class="col-sm-2">
											<label>Fecha Ped:</label>
										</div>
										<div class="col-sm-4">
											<label class="form-control lbFchPed" style="height: 25px"></label>
										</div>
										<div class="col-sm-2">
											<label>Pedido Por:</label>
										</div>
										<div class="col-sm-4">
											<label class="form-control lbPedPor" style="height: 25px"></label>
										</div>
									</div>
								</div>
								<div class="row div-flex">
									<div class="col-sm-1">
										<label>Nº Ped:</label>
									</div>
									<div class="col-sm-3">
										<div class="input-group">
											<label class="form-control lbNoPed text-bold" style="height: 25px;"></label>
											<div class="input-group-addon bg-primary text-blanco no-margin no-padding" style="border-radius: 5px;">
												<a href="#" data-toggle="tooltip" title="Buscar Pedido" >
													<button type="button" class="btn btn-primary bg-primary btNPed" style="height: 25px;padding-top: 1px;">
														<i class="fa fa-search-plus"></i>
													</button>
												</a>
											</div>											
										</div>
									</div>
									<div class="col-sm-8">
										<div class="col-sm-2">
											<label>Fecha Desp:</label>
										</div>
										<div class="col-sm-4">
											<label class="form-control lbFchDesp" style="height: 25px"></label>
										</div>
										<div class="col-sm-2">
											<label class="salCamCap">Salida Cam:</label>
										</div>
										<div class="col-sm-4">
											<label class="form-control lbFchSalCam" style="height: 25px"><?php echo $hoy?></label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row div-flex">
							<div class="col-sm-10 panel panel-primary" style="padding: 5px;">
								<div class="row panel panel-primary contDatos" style="padding: 5px;">
									<table id="table_grid_datos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">
										<thead>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Código</th>	
											<th style="background-color:#428bca; color:white; font-size:1em;">Grupo</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Artículo</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Cant.Ped.</th>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Cant.Desp.</th>
										</thead>
										<tbody>
										</tbody>
										<tfoot>										
										</tfoot>
									</table>
								</div>
								<div class="row panel panel-primary contPedidos" style="padding: 5px;">
									<table id="table_grid_pedido" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">
										<thead>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Nº Pedido</th>	
											<th style="background-color:#428bca; color:white; font-size:1em;">Cod.Bod.</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Nombre Bodega</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Pedido Por</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Items</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Nº Llegada</th>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Salida</th>
										</thead>
										<tbody>
										</tbody>
										<tfoot>										
										</tfoot>
									</table>
								</div>
							</div>
							<div class="col-sm-2 panel panel-primary">
								<div class="row text-center" style="padding-top: 5px;">
									<div class="col-sm-6">
										<div class="row">
											<button class="btn btn-primary bg-primary btn-xs fa fa-plus-square fa-2x btToolAgrPed"></button> 	
										</div>
										<div class="row">
											<label class="control-label text-size-0-5 text-bold">Agr. Pedido</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="row">
											<button class="btn btn-primary bg-primary btn-xs fa fa-paint-brush fa-2x btToolLimp"></button> 	
										</div>
										<div class="row">
											<label class="control-label text-size-0-5 text-bold">Limpiar</label>
										</div>
									</div>
								</div>
								<div class="row text-center" style="padding-top: 5px;">
									<div class="col-sm-6">
										<div class="row no-margin" style="padding-right:8px">
											<button class="btn btn-primary bg-primary btn-xs fa fa-print fa-2x btToolImpGuia"></button>
										</div>
										<div class="row no-margin no-margin">
											<label class="control-label text-size-0-5 text-bold">Impr. Pre Guía</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="row no-margin" style="padding-right:8px">
											<button class="btn btn-primary bg-primary btn-xs fa fa-print fa-2x btToolImpPed"></button> 	
										</div>
										<div class="row no-margin no-margin">
											<label class="control-label text-size-0-5 text-bold">Impr. Pedido</label>
										</div>
									</div>
								</div>
								<div class="row text-center" style="padding-top: 5px;">
									<div class="row no-margin" style="padding-right:8px">
										
									</div>
									<div class="row no-margin no-margin">
										
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 panel panel-primary no-margin no-padding">
								<div class="panel panel-heading text-center no-margin no-padding">
									Transportista
								</div>
								<div class="row"  style="padding-top: 4px;">
									<div class="col-sm-2" style="text-align: left;">
										<label>Nombre:</label>
									</div>
									<div class="col-sm-10">
										<div class="input-group">
											<label class="form-control lbTrNom" style="height: 25px"></label>
											<div class="input-group-addon bg-primary text-blanco no-margin no-padding" style="border-radius: 5px;">
												<a href="#" data-toggle="tooltip" title="Buscar Transaportista" >
													<button type="button" class="btn btn-primary bg-primary btTrBus" style="height: 25px;padding-top: 1px;">
														<i class="fa fa-search-plus"></i>
													</button>
												</a>
											</div>	
										</div>
									</div>
								</div>
								<div class="row" style="padding-top: 4px;">
									<div class="col-sm-2" style="text-align: left;">
										<label>RUC:</label>
									</div>
									<div class="col-sm-5">
										<label class="form-control lbTrRuc" style="height: 25px"></label>
									</div>
									<div class="col-sm-2" style="text-align: left;">
										<label>Placa:</label>
									</div>
									<div class="col-sm-3">
										<label class="form-control lbTrPla" style="height: 25px"></label>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-2" style="text-align: left;">
										<label>Dirección:</label>
									</div>
									<div class="col-sm-10">
										<label class="form-control lbTrDir" style="height: 25px"></label>
									</div>
								</div>
							</div>
							<div class="col-sm-1">
							</div>
							<div class="panel panel-primary col-sm-2 text-center">
								<label id="totItems"></label>
							</div>
						</div>
					</div>

				</div>  
				<div class="panel-footer" style="text-align: right; padding-bottom: 52px;padding-top: 5	px;">
					<div class="col-sm-11">
						<div class="row no-margin" style="padding-right:5px">
							<button class="btn btn-success btn-xs fa fa-check-square fa-2x btImp"></button> 	
						</div>
						<div class="row no-margin no-margin">
							<label class="control-label text-size-0-5 text-bold no-padding no-margin">Imprimir</label>
						</div>	
					</div>
					<div class="col-sm-1">
						<div class="row no-margin" style="padding-right:5px">
							<button class="btn btn-danger btn-xs fa fa-check-square fa-2x btSave"></button> 	
						</div>
						<div class="row no-margin no-margin">
							<label class="control-label text-size-0-5 text-bold no-padding no-margin">Grabar</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalBodPed">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvBdPBar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spBdPBar"><i class="fa fa-search-plus"></i> Escoja la bodega que despacha</span>
			</div>
			<div class="modal-body" id="dvBdPCont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableBdP" id="contTableBdP" >
								<?php echo $table_ped ?>
							</div>	
						</div>
					</div>

				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalPedPen">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvPedPenBar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spPedPenBar"><i class="fa fa-search-plus"></i> Pedidos Pendientes </span>
			</div>
			<div class="modal-body" id="dvPedPenCont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTablePedPen" id="contTablePedPen" >
								
							</div>	
						</div>
					</div>

				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalTransp">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvTranBar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spTranBar"><i class="fa fa-truck"></i> Escoja un Transportista </span>
			</div>
			<div class="modal-body" id="dvTranCont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<div class="row div-flex div-flex-align-end div-flex-just-center" id="dvTranSearch" style = "padding: 10px;">
						<input type="text" class="form-control txTranB"  placeholder="Ingrese Nombre, Placa, Cédula o RUC" >
						<a href="#" data-toggle="tooltip" title="Buscar">
							<button type="button" class="btn btn-primary bg-primary btTranB" style="height: 33px;margin:1px; border-radius: 3px">
								<i class="fa fa-search"></i>
							</button>
						</a>
						<a href="#" data-toggle="tooltip" title="Nuevo">
							<button type="button" class="btn btn-primary bg-primary btTranN" style="height: 33px;margin:1px; border-radius: 3px">
								<i class="fa fa-file-o"></i>
							</button>
						</a>
					</div>
					<div class="row div-flex div-flex-align-end div-flex-just-center">
						<div class="col-md-12">
							<div class="row well text-center contTableTran" id="contTableTran" >
								
							</div>	
						</div>
					</div>

				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalEdtTrn">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvEdtTrnBar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spEdtTrnBar"></i> </span>
			</div>
			
			<div class="modal-body" id="dvEdtTrnCont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<div class="row">
						<div class="col-md-12">
							<div class="row"  style="padding-top: 4px;">
								<div class="col-sm-2" style="text-align: left;">
									<label>RUC:</label>
								</div>
								<div class="col-sm-5">
									<input class="form-control txSvTrRuc" style="height: 25px"></input>
								</div>
								<div class="col-sm-2" style="text-align: left;">
									<label>Placa:</label>
								</div>
								<div class="col-sm-3">
									<input class="form-control txSvTrPla" style="height: 25px"></input>
								</div>
							</div>
							<div class="row" style="padding-top: 4px;">
								<div class="col-sm-2" style="text-align: left;">
									<label>Nombre:</label>
								</div>
								<div class="col-sm-10">
									<input class="form-control txSvTrNom" style="height: 25px"></input>
								</div>
							</div>
							<div class="row" style="padding-top: 4px;">
								<div class="col-sm-2" style="text-align: left;">
									<label>Dirección:</label>
								</div>
								<div class="col-sm-10">
									<input class="form-control txSvTrDir" style="height: 25px"></input>
								</div>
							</div>
						</div>
					</div>
				</div>    
				<div class="modal-footer" style="padding-bottom: 10px; text-align: right;">
					<button type="button" class="btn btn-primary bg-primary btSav" style="height: 25px;padding-top: 1px; border-radius: 3px;">
						<i class="fa fa-floppy-o"></i>
					</button>
					<button type="button" class="btn btn-default btCan" onclick="quitarModal();" style="height: 25px;padding-top: 1px; border-radius: 3px;">
						<i class="fa fa-times"></i> Cancelar
					</button>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="modal" id="confirmModal" style="display: none;z-index: 1050; width: 40%;top:35%">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary" id="dvBusBar">
				<button type="button" class="close-modal-iv" id="confirmClose" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-question-circle"></i> Confirmación</span> 
			</div>
			<div class="modal-body text-blue text-size-1-5" id="confirmMessage">
			</div>
			<div class="modal-footer well" style="padding: 10px;margin: 0px;">
				<button type="button" class="btn btn-primary" id="confirmOk">Ok</button>
				<button type="button" class="btn btn-default" id="confirmCancel">Cancel</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="inputModal" style="display: none;z-index: 1050; width: 40%;top:35%">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close-modal-iv" id="inputClose" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-file-o"></i> Entrada de datos</span> 
			</div>
			<div class="modal-body text-blue text-size-1-5">
				<label for="inputText" class="control-label" id="inputLabel"></label>
				<input type="text" id="inputText">
			</div>
			<div class="modal-footer well" style="padding: 10px;margin: 0px;">
				<button type="button" class="btn btn-primary" id="inputOk">Ok</button>
				<button type="button" class="btn btn-default" id="inputCancel">Cancel</button>
			</div>
		</div>
	</div>
</div>