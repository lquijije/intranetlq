<style>

	#imaginary_container{
		/*margin-top:20%;  Don't copy this */
	}
	.stylish-input-group .input-group-addon{
		background: white !important; 
	}
	.stylish-input-group .form-control{
		border-right:0; 
		box-shadow:0 0 0; 
		border-color:#ccc;
	}
	.stylish-input-group button{
		border:0;
		background:transparent;
	}
	.glyphicon-refresh-animate {
		-animation: spin .7s infinite linear;
		-webkit-animation: spin2 .7s infinite linear;
	}

	@-webkit-keyframes spin2 {
		from { -webkit-transform: rotate(0deg);}
		to { -webkit-transform: rotate(360deg);}
	}

	@keyframes spin {
		from { transform: scale(1) rotate(0deg);}
		to { transform: scale(1) rotate(360deg);}
	}
	.close-modal-iv{
		float: right;
		font-size: 21px;
		font-weight: bold;
		line-height: 1;
		text-shadow: 0 1px 0 #ffffff;
		opacity: 0.5;
		filter: alpha(opacity=50);
		color: #FFFFFF
		/*padding-left:5px;*/
	}
	.close-modal-iv:hover,
	.close-modal-iv:focus {
		text-decoration: none;
		cursor: pointer;
		opacity: 0.9;
		filter: alpha(opacity=90);
		color: #FFFFFF
	}
	button.close-modal-iv {
		padding: 0;
		cursor: pointer;
		background: transparent;
		border: 0;
		-webkit-appearance: none;
	}
	.dropdown-menu.pull-left {
		left: 0;
		right: auto;
	}
	.div-flex{
		display: -webkit-flex;
		display: flex;
	}
	.div-flex-align-center{
		-webkit-align-items: center;
		align-items: center;
	}
	.div-flex-align-end{
		-webkit-align-items: flex-end;
		align-items: flex-end;
	}
	.div-flex-just-center{
		-webkit-justify-content: center;
		justify-content: center;
	}
	.div-flex-just-left{
		-webkit-justify-content: left;
		justify-content: left;
	}
}
</style>

<input type='hidden' id='hdTipBus' name="txt_tipo_buscar" 		value="" />
<input type='hidden' id='hdCodigo' name="txt_codigo_articulo" 	value="" />
<input type='hidden' id='hdCoLike' name="txt_codigo_artilike" 	value="" />
<input type='hidden' id='hdDescri' name="txt_descri_articulo"	value="" />
<input type='hidden' id='hdCodFab' name="txt_codigo_fabrica" 	value="" />
<input type='hidden' id='hdCodUpc' name="txt_codigo_upc" 		value="" />
<input type='hidden' id='hdEstCom' name="txt_estruc_com" 		value="" />
<input type='hidden' id='hdFiltro' name="txt_filtros" 			value="" />
<input type='hidden' id='hdMdlAct' name="txt_modal_acti" 		value="" />
<input type='hidden' id='hdFacFDe' name="txt_fac_fdes" 			value="<?php echo $fIniFact; ?>">
<input type='hidden' id='hdFacFHa' name="txt_fac_fhas" 			value="<?php echo $fFinFact; ?>">
<input type='hidden' id='hdPedFDe' name="txt_ped_fdes" 			value="<?php echo $fIniPed; ?>">
<input type='hidden' id='hdPedFHa' name="txt_ped_fhas" 			value="<?php echo $fFinPed; ?>">
<input type='hidden' id='hdMovFDe' name="txt_ped_fdes" 			value="<?php echo $fIniMov; ?>">
<input type='hidden' id='hdBodVir' name="txt_bod_vir" 			value="0">
<input type='hidden' id='hdBodSal' name="txt_bod_sal" 			value="0">
<input type='hidden' id='hdAntigu' name="txt_antigue" 			value="0">
<input type='hidden' id='hdFilSql' name="txt_filtro_sql" 		value="">
<input type='hidden' id='hdClkHis' name="txt_click_historic"	value="0">
<input type='hidden' id='hdProvee' name="txt_cod_proveedor"		value="0">

<div class="demo-wrapper">
	<div class="dashboard">
		<div class="spa-12">
			<section id="secCa" class="content invoice3" style="padding-top: 0px;margin-top: 0px;">
				<div class="tab_st tab_color_2" >
					<div class="toolbar_tab" style="padding-top:5px; margin-bottom: 5px; padding-left: 5px;">
						<div class="col-sm-10">
							<span class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-search"></i> Consulta de Inventario</span>
						</div>
						<div class="col-sm-2 text-right">
							<a href="#" class="dropdown-toggle btn btn-sm" data-toggle="dropdown" style="color:white;">
								<span class="glyphicon glyphicon-chevron-down" style="opacity: 0.8;"> 
									
								</span>
							</a>
							<ul class="dropdown-menu pull-left" id="mnConInv">
								<li class="user-footer">
									<li>
										<a href="#" >
											<div class="checkbox checkbox-primary">
												<input id="chkByAnt" type="checkbox">
												<label for="chkByAnt">Ordenar por Antiguedad</label>
											</div>
										</a>
									</li>
									<li>
										<a href="#" >
											<div class="checkbox checkbox-primary">
												<input id="chkBdSal" type="checkbox">
												<label for="chkBdSal"> Solo Bodegas con Saldo</label>
											</div>
										</a>
									</li>
									<li>
										<a href="#" >
											<div class="checkbox checkbox-primary">
												<input id="chkBdVir" type="checkbox">
												<label for="chkBdVir"> Mostrar Bodega Virtual</label>
											</div>
										</a>
									</li>
								</li>
							</ul>
						</div>
					</div>
					<div class="content" style="margin-top: 2px;">
						<div class="row dvMainCont">
							<div class="col-md-9 dvInfCont" >
								<div class="row" >
									<div class="col-sm-2">
										<label for="txtArticulo" class="control-label text-black no-padding no-margin">Art&iacuteculo</label>
										<input type="text" class="form-control input-md text-blue text-size-1-25 txCodArt" placeholder="">
									</div>
									<div class="col-sm-8">
										<label for="txtDescripcion" class="control-label text-black no-padding no-margin">Descripci&oacuten</label>
										<input type="text" class="form-control input-md text-blue text-size-1-25 txDesArt" placeholder="">
									</div>
									<div class="col-sm-2">
										<label for="txtCodFabrica" class="control-label text-black no-padding no-margin">C&oacuted. F&aacutebrica</label>
										<input type="text" class="form-control input-md text-blue text-size-1-25 txCodFab" placeholder="" >
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-sm-2">
											<label for="cboGrupo" class="control-label text-black no-padding no-margin">Grupo</label>
											<select id="cboGrupo" class="form-control form-control-sm text-size-0-5 cbGru"><option></option></select>
										</div>
										<div class="col-sm-3">
											<label for="cboLinea" class="control-label text-black no-padding no-margin">L&iacutenea</label>
											<select id="cboLinea" class="form-control form-control-sm text-size-0-5 cbLin"><option></option></select>
										</div>
										<div class="col-sm-2">
											<label for="cboSubLinea" class="control-label text-black no-padding no-margin">SubL&iacutenea</label>
											<select id="cboSubLinea" class="form-control form-control-sm text-size-0-5 cbSbl"><option></option></select>
										</div>
										<div class="col-sm-3">
											<label for="cboCategoria" class="control-label text-black no-padding no-margin">Categor&iacutea</label>
											<select id="cboCategoria" class="form-control form-control-sm text-size-0-5 cbCat"><option></option></select>
										</div>
										<div class="col-sm-2">
											<label for="cboSubCategoria" class="control-label text-black no-padding no-margin">SubCategor&iacutea</label>
											<select id="cboSubCategoria" class="form-control form-control-sm text-size-0-5 cbSbc"><option></option></select>
										</div>
									</div>

								</div>
								<div class="row">
									<div class="form-group" >
										<div class="col-md-1">
											<div class="row">
												<label  class="control-label text-center text-black no-padding no-margin">Filtro</label>
											</div>
											<div class="row">
												<a href="#" data-toggle="tooltip" title="Quitar Filtro" >
													<button class="btn btn-warning btn-xs fa fa-close btFilQui"></button>
												</a>
												<a href="#" data-toggle="tooltip" title="Agregar Filtro" data-placement="right">
													<button class="btn btn-success btn-xs fa fa-plus btFilAgr"></button>
												</a>
											</div>
										</div>
										<div class="col-md-7">
											<div class="row">
												<label  class="control-label text-center text-black no-padding no-margin">Condici&oacuten</label>
											</div>
											<div class="row">
												<label class="control-label text-size-1 filCon" style="color:blue;"></label>
												<!--<input type="text"  class="form-control input-sm text-black txCon" placeholder="">-->
											</div>
										</div>
										<div class="col-md-4">
											<div class="row">
												<i class="fa fa-barcode"></i>
												<label  class="control-label text-center text-black no-padding no-margin">C&oacutedigo UPC</label>
											</div>
											<div class="row panel text-center">
												<!--<label id="lblUPC" class="control-label text-center"></label>-->
												<input type="text" class="form-control input-xs	 text-blue txUpc" style="height: 27px" placeholder="">
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-info" >
									<div class="col-md-12 panel">
										<div class="row" style="padding: 5px;">
											<div class="col-md-3">
												<div class="row" >
													<div class="col-xs-4" >
														<span class = "text-size-1 label label-info dvOfe">Oferta:</span>
													</div>
													<div class="col-xs-8 text-center" >	
														<!-- <p class="text-sixe-1 lbOfe" ></p> -->
														<span class="text-size-1-25 label text-blue lbOfe"></span>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="row">
													<div class="col-xs-3" >
														<span class = "text-size-1 label label-info">Desde:</span>
													</div>
													<div class="col-xs-9 text-center">	
														<span class="text-size-1 label text-blue" id="hOfeDes"></span>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="row" >
													<div class="col-xs-3" >
														<span class = "text-size-1 label label-info">Hasta:</span>
													</div>
													<div class="col-xs-9 text-center">	
														<span class="text-size-1 label text-blue" id="hOfeHas"></span>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="row">
													<div class="col-xs-3" >
														<span class = "text-size-1 label label-info">Desc:</span>
													</div>
													<div class="col-xs-9 text-center">	
														<span class="text-size-1 label text-blue" id="hPerDes"></span>
													</div>
												</div>
											</div>
										</div>
										<div class="row" style="display:flex" >
											<div class="col-md-6" >
												<div class="row">
													<div class="col-md-5 panel panel-info" >
														<div class="row">
															<label class="control-label text-danger no-padding no-margin">PVP</label>
														</div>
														<div class="row text-center">
															<p id="pPvp" class="text-primary text-size-2 no-padding no-margin"></p>
														</div>
													</div>
													<div class="col-md-5 panel panel-info">
														<div class="row">
															<label class="control-label text-danger no-padding no-margin">PVP+IVA</label>
														</div>
														<div class="row text-center">
															<p class="text-blue text-size-2 text-bold no-padding no-margin" id="pPvpIva"></p>

														</div>
													</div>
													<div class="col-md-2 panel panel-info">
														<div class="row">
															<label class="control-label text-danger no-padding no-margin">Ranking</label>
														</div>
														<div class="row text-center">
															<p class="text-success text-size-2 text-bold no-padding no-margin" id="pRank"></p>
														</div>
													</div>

												</div>
												<div class="row">
													<div class="col-md-8 ">
														<div class="row">
															<label class="control-label text-center text-primary text-bold no-padding no-margin">Fecha Creaci&oacuten</label>
															<!-- <h4><span class = "label label-info">Fecha Creaci&oacuten</span></h4> -->
														</div>
														<div class="row text-center">
															<label class="control-label text-center text-bold lbFecCre no-padding no-margin"></label>
														</div>
													</div>
													<div class="col-md-4">
														<div class="row">
															<label class="control-label text-center text-primary text-bold no-padding no-margin">Empaque</label>
															<!--<h4><span class = "label label-info">Empaque</span></h4>-->
														</div>
														<div class="row text-center">
															<label class="control-label text-center text-bold lbEmp no-padding no-margin"></label>
														</div>
													</div>	
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Ultima Factura</label>
															</div>
															<div class="col-md-6">
																<label class="control-label text-center text-size-0-5 text-bold lbUltFac no-padding no-margin"></label>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Ult. Unidades Ingresadas</label>
															</div>
															<div class="col-md-6">
																<label class="control-label text-center text-size-0-5 text-bold lbUndIng no-padding no-margin"></label>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Fecha Ultimo Ingreso</label>
															</div>
															<div class="col-md-6">
																<label class="control-label text-center text-size-0-5 text-bold lbFecUltIng no-padding no-margin"></label>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Ingresos Totales</label>
															</div>
															<div class="col-md-6">
																<label class="control-label text-center text-size-0-5 text-bold lbIngTot no-padding no-margin"></label>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Costo Moneda Extranjera</label>
															</div>
															<div class="col-md-6">
																<label class="control-label text-center text-size-0-5 text-bold lbCosMonExt no-padding no-margin"></label>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Ultimo Costo</label>
															</div>
															<div class="col-md-6">
																<label class="control-label text-center text-size-0-5 text-bold lbUltCos no-padding no-margin"></label>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Costo Promedio</label>
															</div>
															<div class="col-md-6">
																<label class="control-label text-center text-size-0-5 text-bold lbCosPro no-padding no-margin"></label>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Base Comercial</label>
															</div>
															<div class="col-md-6">
																<label class="control-label text-center text-size-0-5 text-bold lbBasCom no-padding no-margin"></label>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<label  class="control-label text-center text-navy text-size-0-5 text-bold no-padding no-margin" >Fabricante</label>
															</div>
															<div class="col-md-6" >
																<label class="control-label text-center text-size-0-5 text-bold lbFab no-padding no-margin"></label>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6" style="display:flex">
												<div class="row panel panel-info" style="width: 100%;text-align: center;vertical-align: bottom;">
													<img src="" class="imgArt" style="width: auto;height: auto;max-width: 100%;max-height: 275px;" alt="Imagen">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="row">
													<label  class="control-label text-center text-primary text-bold panel no-padding no-margin">Proveedor</label>
													<!--<h4><span class = "label label-info">Proveedor</span></h4>-->
												</div>
												<div class="row text-center">
													<label class="control-label text-center text-bold lbPro"></label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="row">
													<label  class="control-label text-center text-primary text-bold panel no-padding no-margin">Garantia</label>
													<!--<h4><span class = "label label-info">Garantia</span></h4>-->
												</div>
												<div class="row text-center">
													<label class="control-label text-center text-bold lbGar"></label>
												</div>
											</div>
										</div>
										<div class="row"> 
											<div class="row">
												<label  class="control-label text-center text-primary text-bold panel no-padding no-margin">Observaciones</label>
												<!--<h4><span class = "label label-info">Observaciones</span></h4>-->
											</div>
											<div class="row">
												<label  class="control-label text-bold lbObs"></label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 dvRotCont" style="padding:0px;">
								<ul class="nav nav-pills">
									<li class="active"><a href="#1a" data-toggle="tab"> Pycca </a></li>
									<li ><a href="#2a" data-toggle="tab"> Polipapel </a></li>
								</ul>
								<div class="tab-content clearfix">
									<div class="tab-pane active" id="1a">
										<div class="row well contTablePyRot" >
											<!-- AQUI VA LA TABLA DE CONSULTA DE ROTACION -->
											
										</div>										
									</div>
									<div class="tab-pane" id="2a">
										<div class="row well contTablePolRot" >
											<!-- AQUI VA LA TABLA DE CONSULTA DE ROTACION de POLIPAPEL -->
											<table id="table_rotacion_py" class="table table-bordered text-size-0-2 no-padding no-margin text-bold">
												<thead>
													<th style="background-color:#001f3f; color:white; font-size:1.2em;">Descripción</th>	
													<th style="background-color:#001f3f; color:white; font-size:1.2em;">Saldo</th>
													<th style="background-color:#001f3f; color:white; font-size:1.2em;">Rot360</th>
													<th style="background-color:#001f3f; color:white; font-size:1.2em;">Rot90</th>
												</thead>
											</table>
										</div>
									</div>
									<div class="row well bg-blue" style="padding: 1px;margin:0px">
										<div class="row">
											<div class="col-sm-5 text-left">
												<label  class="control-label text-center text-blanco text-bold text-size-0-5">Saldo</label>
											</div>
											<div class="col-sm-2 text-center">
												<label  class="control-label text-center text-blanco text-bold text-size-0-5 lbSalTo">0</label>
											</div>
											<div class="col-sm-2 text-center">
												<label  class="control-label text-center text-blanco text-bold text-size-0-5 lbRotTo360">0.00</label>
											</div>
											<div class="col-sm-2 text-center">
												<label  class="control-label text-center text-blanco text-bold text-size-0-5  lbRotTo90">0.00</label>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<label  class="control-label text-center text-blanco text-bold text-size-0-5">Rotaci&oacuten por mayor</label>
											</div>
											<div class="col-sm-2">
												<label  class="control-label text-center text-blanco text-bold text-size-0-5 lbRotMay">0.00</label>
											</div>
											<div class="col-sm-4">
												<label  class="control-label text-center text-blanco text-bold text-size-0-5">Rotaci&oacuten Cajas</label>
											</div>
										</div>
									</div>
									<div class="row well bg-light-lime dvPed"  style="padding: 1px;margin:0px;">
										<div class="row">
											<div class="col-sm-8 text-left">
												<label  class="control-label text-center text-blue text-bold text-size-1" style="font-weight: bold">Pedido Pendiente Total</label>
											</div>
											<div class="col-sm-4 text-right">
												<label  class="control-label text-center text-blue text-bold text-size-1 lbPedTot" style="font-weight: bold">0.00</label>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<label  class="control-label text-black text-bold text-size-0-5">Bodega Central</label>
											</div>
											<div class="col-sm-2 text-center">
												<label  class="control-label text-black text-bold text-size-0-5 lbBodPICA">0</label>
											</div>
											<div class="col-sm-4">
												<label  class="control-label text-black text-bold text-size-0-5">Almacenes</label>
											</div>
											<div class="col-sm-2 text-right">
												<label  class="control-label text-black text-bold text-size-0-5 lbTienPICA">0</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" >
							<div class="col-md-12" style="padding-bottom: 0px;">
								<div class="col-md-2">
									<div class="row text-left" >
										<a href="#" data-toggle="tooltip" title="Anterior">
											<button class="btn btn-primary btn-sm fa fa-arrow-left fa-2x  btAnt"></button>
										</a> 
										<a href="#" data-toggle="tooltip" title="Adelante">
											<button class="btn btn-primary btn-sm fa fa-arrow-right fa-2x btAde"></button> 
										</a>
									</div>
								</div>
								<div class="col-md-3">

								</div>
								<div class="col-md-1">
									<div class="row text-center">
										<button class="btn btn-success btn-xs fa fa-check-square fa-2x btSim"></button> 
									</div>
									<div class="row text-center">
										<label class="control-label text-size-0-5 text-bold no-padding no-margin">Similares</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="row text-center">
										<button class="btn btn-success btn-xs fa fa-check-square fa-2x btFic"></button> 
									</div>
									<div class="row text-center">
										<label class="control-label text-size-0-5 text-bold no-padding no-margin">Ficha T&eacutecnica</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="row text-center">
										<button class="btn btn-success btn-xs fa fa-check-square fa-2x btMov"></button> 
									</div>
									<div class="row text-center">
										<label class="control-label text-size-0-5 text-bold no-padding no-margin">Movimientos</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="row text-center">
										<button class="btn btn-success btn-xs fa fa-check-square fa-2x btFor"></button> 
									</div>
									<div class="row text-center">
										<label class="control-label text-size-0-5 text-bold no-padding no-margin">Forecast</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="row text-center">
										<button class="btn btn-success btn-xs fa fa-check-square fa-2x btPer"></button> 
									</div>
									<div class="row text-center">
										<label class="control-label text-size-0-5 text-bold no-padding no-margin">Percha</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="row text-center">
										<button class="btn btn-success btn-xs fa fa-check-square fa-2x btFac"></button> 
									</div>
									<div class="row text-center">
										<label class="control-label text-size-0-5 text-bold no-padding no-margin">Factura</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="row text-center">
										<button class="btn btn-success btn-xs fa fa-check-square fa-2x btPed"></button> 
									</div>
									<div class="row text-center">
										<label class="control-label text-size-0-5 text-bold no-padding no-margin">Pedido</label>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</section>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalFiltros">
	<div class="modal-dialog" id="dvDialog">
		<div class="modal-content" >
			<div class="modal-header bg-red" id="dvFilBar">

				<button type="button" onclick="quitarModal();" class="close-modal-iv">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spFilBar"><i class="fa fa-filter"></i> Condiciones de Selecci&oacuten</span>
			</div>
			<div class="modal-body" id="dvFilCont" style="padding-bottom: 1px">
				<div class="content">
					<div class="col-sm-12" style="margin-left: 10px;margin-right: 10px;" >
						<div class="row">
							<div class="col-sm-4">
								<h4 class = "text-blue">Campo</h4>
							</div>
							<div class="col-sm-2">
								<h4 class = "text-blue">Condici&oacuten</h4>
							</div>
							<div class="col-sm-3">
								<h4 class = "text-blue">Valor</h4>
							</div>
						</div>
						<div class="row panel panel-danger div-flex div-flex-align-end"  style="margin-top: 1px;padding: 4px; background-color: #FFEEEE">
							<div class="col-sm-4 div-flex div-flex-align-center">
								<label class="switch" style="margin-right: 3px;">
									<input id="chkFil1" type="checkbox" name ="a.pvpiva" >
									<div class="slider-danger round"></div>
								</label>
								<label for="chkFil1">PVP IVA</label>
							</div>
							<div class="col-sm-2" >
								<select class="form-control cbFil1" style="height: 32px;">
								</select>
							</div>
							<div class="col-sm-6" id="dvFilTx1a">
								<input type="text" class="form-control input-md txFil1a" style="height: 32px;">
							</div>
							<div class="col-sm-3 hide" id="dvFilTx1b">
								<input type="text" class="form-control input-md txFil1b" style="height: 32px;">
							</div>
						</div>
						<div class="row panel panel-danger div-flex div-flex-align-end"  style="margin-top: 1px;padding: 4px; background-color: #FFEEEE">
							<div class="col-sm-4 div-flex div-flex-align-center">
								<label class="switch" style="margin-right: 3px;">
									<input id="chkFil2" type="checkbox" name ="(r.rotdiaria*30.5)">
									<div class="slider-danger round"></div>
								</label>
								<label for="chkFil2">Rotaci&oacuten Mensual</label>
							</div>
							<div class="col-sm-2">
								<select class="form-control cbFil2" style="height: 32px;">
								</select>
							</div>
							<div class="col-sm-6" id="dvFilTx2a">
								<input type="text" class="form-control input-md txFil2a" style="height: 32px;">
							</div>
							<div class="col-sm-3 hide" id="dvFilTx2b">
								<input type="text" class="form-control input-md txFil2b" style="height: 32px;">
							</div>
						</div>
						<div class="row panel panel-danger div-flex div-flex-align-end"  style="margin-top: 1px;padding: 4px; background-color: #FFEEEE">
							<div class="col-sm-4 div-flex div-flex-align-center">
								<label class="switch" style="margin-right: 3px;">
									<input id="chkFil3" type="checkbox" name ="s.saldo">
									<div class="slider-danger round"></div>
								</label>
								<label for="chkFil3">Saldo Almac&eacuten</label>
							</div>
							<div class="col-sm-2">
								<select class="form-control cbFil3" style="height: 32px;">
								</select>
							</div>
							<div class="col-sm-6" id="dvFilTx3a">
								<input type="text" class="form-control input-md txFil3a" style="height: 32px;">
							</div>
							<div class="col-sm-3 hide" id="dvFilTx3b">
								<input type="text" class="form-control input-md txFil3b" style="height: 32px;">
							</div>
						</div>
						<div class="row panel panel-danger div-flex div-flex-align-end"  style="margin-top: 1px;padding: 4px; background-color: #FFEEEE">
							<div class="col-sm-4 div-flex div-flex-align-center">
								<label class="switch" style="margin-right: 3px;">
									<input id="chkFil4" type="checkbox" name ="DATEDIFF(NOW(),a.fe_ult_factura)">
									<div class="slider-danger round"></div>
								</label>
								<label for="chkFil4">Antiguedad</label>
							</div>
							<div class="col-sm-2">
								<select class="form-control cbFil4" style="height: 32px;">
								</select>
							</div>
							<div class="col-sm-6" id="dvFilTx4a">
								<input type="text" class="form-control input-md txFil4a" style="height: 32px;">
							</div>
							<div class="col-sm-3 hide" id="dvFilTx4b">
								<input type="text" class="form-control input-md txFil4b" style="height: 32px;">
							</div>
						</div>
						<div class="row panel panel-danger div-flex div-flex-align-end"  style="margin-top: 1px;padding: 4px; background-color: #FFEEEE">
							<div class="col-sm-4 div-flex div-flex-align-center">
								<label class="switch" style="margin-right: 3px;">
									<input id="chkFil5" type="checkbox" name ="a.saldo_total">
									<div class="slider-danger round"></div>
								</label>
								<label for="chkFil5">Saldo Total
								</label>
							</div>
							<div class="col-sm-2">
								<select class="form-control cbFil5" style="height: 32px;">
								</select>
							</div>
							<div class="col-sm-6" id="dvFilTx5a">
								<input type="text" class="form-control input-md txFil5a" style="height: 32px;">
							</div>
							<div class="col-sm-3 hide" id="dvFilTx5b">
								<input type="text" class="form-control input-md txFil5b" style="height: 32px;">
							</div>
						</div>
						<div class="row panel panel-danger div-flex div-flex-align-end"  style="margin-top: 1px;padding: 4px; background-color: #FFEEEE">
							<div class="col-sm-4 div-flex div-flex-align-center">
								<label class="switch" style="margin-right: 3px;">
									<input id="chkFil6" type="checkbox" name ="k.ds_ranking">
									<div class="slider-danger round"></div>
								</label>
								<label for="chkFil6">Ranking
								</label>
							</div>
							<div class="col-sm-2">
								<select class="form-control cbFil6" style="height: 32px;">
								</select>
							</div>
							<div class="col-sm-6" id="dvFilTx6a">
								<input type="text" class="form-control input-md txFil6a" style="height: 32px;">
							</div>
							<div class="col-sm-3 hide" id="dvFilTx6b">
								<input type="text" class="form-control input-md txFil6b" style="height: 32px;">
							</div>
						</div>
						<div class="row text-right" style ="padding:5px;">
							<button type="button" class="btn btn-success btAplFil">Aplicar <i class="fa fa-check-square"></i></button>
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalSimilares">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-red" id="dvSimBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-linode"></i> Consulta de los 15 anteriores Artículos+ 15 posteriores artículos</span> 
			</div>
			<div class="modal-body" id="dvSimCont">
				<div class="content" >
					<div class="row" id="dvSimTit">
						<div class="col-md-12">
							<div class="row text-center" >
								<label  class="control-label text-red text-size-2 text-bold" id="txSmTit"></label>
							</div>
						</div>
					</div>
					<div class="row" id="dvSimSearch">
						<div class="col-md-11">
							<input type="text" class="form-control txSimB"  placeholder="Ingrese código o descripción de articulo" >
						</div>
						<div class="col-md-1">
							<a href="#" data-toggle="tooltip" title="Buscar"><button type="button" class="btn btn-primary bg-red btSimB" style="height: 33px;border-radius: 3px"><i class="fa fa-search"></i></button></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableSimilares" >
								<!-- AQUI VA LA TABLA DE CONSULTA DE SIMILARES -->

							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modalFicha">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-navy" id="dvFicBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-clone"></i> Ficha T&eacutecnica</span>
			</div>
			<div class="modal-body" id="dvFicCont">
				<div class="content" >
					<div class="row" id="dvFicTit">
						<div class="col-md-12">
							<div class="row text-center" >
								<label  class="control-label text-blue text-size-1-25 text-bold" id="txFicTit"></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableFicha" >
								<!-- AQUI VA LA FICHA TECNICA -->

							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modalPercha">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-teal" id="dvPerBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spPerBar"></span>
			</div>
			<div class="modal-body" id="dvPerCont">
				<div class="content" >
					<div class="row" id="dvPerTit">
						<div class="col-md-12">
							<div class="row text-center" >
								<label  class="control-label text-blue text-size-1-25 text-bold" id="txPerTit"></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTablePercha" >
								<!-- AQUI VA LA PERCHA -->

							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modalForecast">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-orange" id="dvForBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spForBar"><i class="fa fa-map-o"></i> Consulta de Forecast Mensual.</span>
			</div>
			<div class="modal-body" id="dvForCont">
				<div class="content" >
					<div class="row" id="dvForTit">
						<div class="col-md-12">
							<div class="row text-center" >
								<div class="col-md-9 text-center">
									<label  class="control-label text-blue text-size-1-25 text-bold" id="txForTit"></label>
								</div>
								<div class="col-md-3 text-center" id="dvForImp">
									<label  class="control-label text-strong-red text-size-1-25 text-bold" >Importante!!</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableForecast" >
								<!-- AQUI VA EL FORECAST -->
							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalFactura">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-green" id="dvFacBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spFacBar"><i class="fa fa-newspaper-o"></i> Consulta de Factura.</span>
			</div>
			<div class="modal-body" id="dvFacCont">
				<div class="content" >
					<div class="row" id="dvFacTit">
						<div class="col-md-12">
							<div class="row text-center" >
								<label  class="control-label text-blue text-size-1-25 text-bold" id="txFacTit"></label>
							</div>
						</div>
					</div>
					<div class="row" id="dvFacBus">
						<!-- TODO BUSQUEDA -->
						<div class="col-sm-12">
							<div class="row div-flex div-flex-align-end div-flex-just-center">
								<div class="col-sm-2">
									<label class="control-label text-black no-padding no-margin">No. Factura</label>
									<div class="input-group">
										<div class="input-group-addon bg-blue">
											<i class="fa fa-hashtag"></i>
										</div>
										<input type="text" class="form-control input-md text-blue text-size-1 txFacBus" placeholder="">
									</div>								
								</div>
								<div class="col-sm-2">

									<label class="control-label text-black no-padding no-margin">Fecha Desde</label>
									<div class="input-group">
										<div class="input-group-addon bg-blue">
											<i class="fa fa-calendar"></i>
										</div>
										<input name="txFacFeIni" value="" id="txFacFeIni" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control text-blue" type="text">
									</div>
								</div>
								<div class="col-sm-2" >
									<label class="control-label text-black no-padding no-margin">Fecha hasta</label>
									<div class="input-group">
										<div class="input-group-addon bg-blue ">
											<i class="fa fa-calendar"></i>
										</div>
										<input name="txFacFeFin" value="" id="txFacFeFin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control text-blue" type="text">
									</div>
								</div>

								<a href="#" data-toggle="tooltip" title="Buscar" style="padding-bottom: 2px;margin-right: 2px;">
									<button type="button" class="btn btn-primary bg-blue btFacBus" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-search"></i>
									</button>
								</a>
								<a href="#" data-toggle="tooltip" title="Copiar en portapapeles" style="padding-bottom: 2px;">
									<button type="button" class="btn btn-primary bg-blue btFacCli" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-clipboard"></i>
									</button>
								</a>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableFactura" id="contTableFactura" >
								<!-- AQUI VA LA CONSULTA DE FACTURA -->

							</div>	
							<div id="list-popover" class="hide">
								<table class="display compact table-hover">
									<tbody>
										<tr>
											<td class="text-blue">No.Fact.</td>
											<td class="text-blue">:</td>
											<td id="tbRowFac1"></td>
										</tr>
										<tr>
											<td class="text-blue">Sec.Pedido</td>
											<td class="text-blue">:</td>
											<td id="tbRowFac2"></td>
										</tr>
										<tr>
											<td class="text-blue">Consolidado#</td>
											<td class="text-blue">:</td>
											<td id="tbRowFac3"></td>
										</tr>
										<tr>
											<td class="text-blue">Tipo Fact.</td>
											<td class="text-blue">:</td>
											<td id="tbRowFac4"></td>
										</tr>
										<tr>
											<td class="text-blue">Ped.PICA</td>
											<td class="text-blue">:</td>
											<td id="tbRowFac5"></td>
										</tr>
										<tr>
											<td class="text-blue">Bod.PICA</td>
											<td class="text-blue">:</td>
											<td id="tbRowFac6"></td>
										</tr>
										<tr>
											<td class="text-blue">Cliente PYCCA</td>
											<td class="text-blue">:</td>
											<td id="tbRowFac7"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalPedido">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-yellow" id="dvPedBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spPedBar"><i class="fa fa-list-alt"></i> Pedidos a Bodega.</span>
			</div>
			<div class="modal-body" id="dvPedCont">
				<div class="content" >
					<div class="row" id="dvPedTit">
						<div class="col-md-12">
							<div class="row text-center" >
								<label  class="control-label text-yellow2 text-size-1-25 text-bold" id="txPedTit"></label>
							</div>
						</div>
					</div>
					<div class="row" id="dvPedBus">
						<!-- TODO BUSQUEDA -->
						<div class="col-sm-12">
							<div class="row div-flex div-flex-align-end div-flex-just-center">
								<div class="col-sm-2">
									<label class="control-label text-black no-padding no-margin">No. Pedido</label>
									<div class="input-group">
										<div class="input-group-addon bg-yellow no-margin no-padding" style="border-radius: 4px;">
											<select class="bg-yellow cbPedBus" style="text-align-last: center;border:0;height: 31px;">
												<option value="1">=</option>
												<option value="2">></option>
												<option value="3">>=</option>
												<option value="4"><</option>
												<option value="5"><=</option>
												<option value="6">Entre</option>
											</select>
										</div>
										<input type="text" class="form-control input-md text-yellow2 text-size-1 txPedBus1" placeholder="">

									</div>								
								</div>
								<div class="col-sm-2 hide dvPedTxEnt">
									<div class="input-group">
										<input type="text" class="form-control input-md text-yellow2 text-size-1 txPedBus2" placeholder="">
									</div>
								</div>
								<div class="col-sm-2">
									<label class="control-label text-black no-padding no-margin">Fecha Desde</label>
									<div class="input-group">
										<div class="input-group-addon bg-yellow">
											<i class="fa fa-calendar"></i>
										</div>
										<input name="txPedFeIni" value="" id="txPedFeIni" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control text-yellow2" type="text">
									</div>
								</div>
								<div class="col-sm-2" >
									<label class="control-label text-black no-padding no-margin">Fecha Hasta</label>
									<div class="input-group">
										<div class="input-group-addon bg-yellow ">
											<i class="fa fa-calendar"></i>
										</div>
										<input name="txPedFeFin" value="" id="txPedFeFin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control text-yellow2" type="text">
									</div>
								</div>
								<div class="col-sm-2">
									<label class="control-label text-black no-padding no-margin">Almac&eacuten</label>
									<div class="input-group">
										<div class="input-group-addon bg-yellow">
											<i class="fa fa-shopping-bag"></i>
										</div>
										<?php echo $cmb_almacen_ped; ?>
									</div>
								</div>
								<a href="#" data-toggle="tooltip" title="Buscar" style = "padding-bottom: 2px;margin-right: 2px;">
									<button type="button" class="btn btn-primary bg-yellow btPedBus" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-search"></i>
									</button>
								</a>
								<a href="#" data-toggle="tooltip" title="Copiar en portapapeles" style = "padding-bottom: 2px;">
									<button type="button" class="btn btn-primary bg-yellow btPedCli" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-clipboard"></i>
									</button>
								</a>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTablePedido" id="contTablePedido" >
								<!-- AQUI VA LA CONSULTA DE PEDIDO -->

							</div>	
						</div>
					</div>

				</div>      	
			</div>
		</div>
	</div>
</div>


<div class="modal" id="modalDetPedido">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-yellow" id="dvDetPedBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spDetPedBar"><i class="fa fa-list"></i> Detalle del Pedido.</span>
			</div>
			<div class="modal-body" id="dvDetPedCont">
				<div class="content" >
					<div class="row" id="dvDetPedCab">
						<div class="row div-flex div-flex-align-end div-flex-just-left">
							<div class="col-sm-3">
								<div class="row">
									<label class="control-label text-black no-padding no-margin">No. Pedido:</label>
									<label class="control-label text-blue no-padding no-margin lbDetPedNP"></label>	
								</div>
								<div class="row">
									<label class="control-label text-black no-padding no-margin">Pedido por:</label>
									<label class="control-label text-blue no-padding no-margin lbDetPedPP"></label>	
								</div>
								<div class="row">
									<label class="control-label text-black no-padding no-margin">Fecha Despacho:</label>
									<label class="control-label text-blue no-padding no-margin lbDetPedFD"></label>	
								</div>
							</div>
							<div class="col-sm-9">
								<div class="row">
									<label class="control-label text-black no-padding no-margin">Requerimiento:</label>
									<label class="control-label text-blue no-padding no-margin lbDetPedRQ"></label>	
								</div>
								<div class="row">
									<label class="control-label text-blue no-padding no-margin lbDetPedPN"></label>	
									<label class="control-label text-blue no-padding no-margin lbDetPedCM"></label>	
								</div>
								<div class="row">
									<label class="control-label text-black no-padding no-margin">Despachador:</label>
									<label class="control-label text-blue no-padding no-margin lbDetPedDE"></label>	
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableDetPedido" id="contTableDetPedido" >
								<!-- AQUI VA LA CONSULTA DE DETALLE DE PEDIDO -->

							</div>	
						</div>
					</div>

				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalMovimiento">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-green2" id="dvMovBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spMovBar"><i class="fa fa-list-alt"></i> Consulta de Movimientos.</span>
			</div>
			<div class="modal-body" id="dvMovCont">
				<div class="content" >
					<div class="row" id="dvMovTit">
						<div class="col-md-12">
							<div class="row text-center" >
								<label  class="control-label text-green2 text-size-1-25 text-bold" id="txMovTit"></label>
							</div>
						</div>
					</div>
					<div class="row" id="dvMovBus">
						<!-- TODO BUSQUEDA -->
						<div class="col-sm-12">
							<div class="row div-flex div-flex-align-end div-flex-just-center">
								<div class="col-sm-2">
									<label class="control-label text-black no-padding no-margin">Almac&eacuten</label>
									<div class="input-group">
										<div class="input-group-addon bg-green2 text-blanco">
											<i class="fa fa-shopping-bag"></i>
										</div>
										<?php echo $cmb_almacen_mov; ?>
									</div>
								</div>
								<div class="col-sm-2">
									<label class="control-label text-black no-padding no-margin">Movimiento</label>
									<div class="input-group">
										<div class="input-group-addon bg-green2 text-blanco">
											<i class="fa fa-exchange"></i>
										</div>
										<?php echo $cmb_tipo_mov; ?>
									</div>
								</div>
								<div class="col-sm-2">
									<label class="control-label text-black no-padding no-margin">Fecha Desde</label>
									<div class="input-group">
										<div class="input-group-addon bg-green2 text-blanco">
											<i class="fa fa-calendar"></i>
										</div>
										<input name="txMovFeIni" value="" id="txMovFeIni" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control text-black" type="text">
									</div>
								</div>
								<div class="col-sm-2" >
									<label class="control-label text-black no-padding no-margin">Fecha Hasta</label>
									<div class="input-group">
										<div class="input-group-addon bg-green2 text-blanco">
											<i class="fa fa-calendar"></i>
										</div>
										<input name="txMovFeFin" value="" id="txMovFeFin" data-date-format="yyyy-mm-dd hh:ii" class="form_datetime form-control text-black" type="text">
									</div>
								</div>

								<a href="#" data-toggle="tooltip" title="Buscar" style = "padding-bottom: 2px;margin-right: 2px;">
									<button type="button" class="btn btn-primary bg-green2 btMovBus" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-search"></i>
									</button>
								</a>
								<a href="#" data-toggle="tooltip" title="Copiar en portapapeles" style = "padding-bottom: 2px;margin-right: 2px;">
									<button type="button" class="btn btn-primary bg-green2 btMovCli" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-clipboard"></i>
									</button>
								</a>
								<a href="#" data-toggle="tooltip" title="Histórico" style = "padding-bottom: 2px;margin-right: 2px;">
									<button type="button" class="btn btn-primary bg-green2 btMovHis" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-history"></i>
									</button>
								</a>
								<a href="#" data-toggle="tooltip" title="Imprimir PDF" style = "padding-bottom: 2px;">
									<button type="button" class="btn btn-primary bg-green2 btMovImp" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-file-pdf-o"></i>
									</button>
								</a>
								<div class="col-sm-1 div-flex div-flex-align-center div-flex-just-left">
									<label class="switch">
										<input id="chkMovTot" type="checkbox">
										<div class="slider-success round"></div>
									</label>
									<label for="chkMovTot">Totalizar</label>
								</div>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableMov" id="contTableMov" >
								<!-- AQUI VA LA CONSULTA DE PEDIDO -->

							</div>	
						</div>
					</div>

				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalCostoVta">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-aqua" id="dvCvtBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spCvtBar"><i class="fa fa-search-plus"></i> Documentos.</span>
			</div>
			<div class="modal-body" id="dvCvtCont">
				<div class="content" >
					<div class="row" id="dvCvtTit">
						<div class="col-md-12">
							<div class="row text-center" >
								<label  class="control-label text-aqua text-size-1-25 text-bold" id="txCvtTit"></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableCvt" id="contTableCvt" >
								<!-- AQUI VA LA CONSULTA DE PEDIDO -->

							</div>	
						</div>
					</div>

				</div>      	
			</div>
		</div>
	</div>
</div>