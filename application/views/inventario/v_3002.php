<style>
label {
    font-size: 24px;
}
.table {
    background: #FFF !important;
}
.pr {
    width: 0px;
    height: 0px;
    padding: 0;
    border: 1px solid #E5E5E5;
}
.form-control_g {
    font-size: 30px !important;
    height: 50px;
    text-transform: uppercase;
    text-align: center;
}
.form-control_p {
    font-size: 23px !important;
    height: 40px;
    text-transform: uppercase;
    text-align: center;
}
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="invoice12">
                <div class="row" style="margin-bottom:20px;">
                    <div class="spa-12 text-center">
                        <div class="titulo-1 text-size-1-5 text-blanco"><i class="fa fa-barcode"></i> Consultar UPC</div><button type="button" class="btn-out" onclick="window.location.reload();" title="Refrescar"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
                <div class="row">
                    <div class="spa-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                
                                <div class="col-xs-12 text-center">

                                    <div class="col-xs-12 f_2">
                                        <?php 
                                            echo form_open('inventario/co_3001',array('id'=>'form_upc','name'=>'form_upc'));
                                        ?>   
                                        <div class="col-xs-12">
                                            <label>Escanee el c&oacute;digo UPC</label>
                                        </div>
                                        <div class="col-xs-12">
                                            <input id="cod_upc" name="cod_upc" class="form-control" type="text">
                                            <img src="<?php echo site_url('img/1anima.gif'); ?>" width="20%">
                                        </div>
                                        <?php 
                                            echo form_close(); 
                                        ?>
                                    </div>
                                    
                                    <div class="col-xs-12" style="margin-top:10px;">
                                        <?php 
                                            echo form_open('inventario/co_3001',array('id'=>'send_upc','name'=>'send_upc','class'=>'cnt_art'));
                                        ?>
                                        <?php 
                                            echo form_close(); 
                                        ?>
                                    </div>
                                    
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<audio id="sp_2" src="<?php echo site_url('media/upc_esca.mp3'); ?>"></audio>
<audio id="so_0" src="<?php echo site_url('media/upc_correcto.mp3'); ?>"></audio>
<audio id="so_2" src="<?php echo site_url('media/upc_error.mp3'); ?>"></audio>
<audio id="t_2" src="<?php echo site_url('media/sin_datos.mp3'); ?>"></audio>
<audio id="err" src="<?php echo site_url('media/error.mp3'); ?>"></audio>