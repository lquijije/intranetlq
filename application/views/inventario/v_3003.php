<style>
    .TFtable td > span {
        position: absolute;
        z-index: -21;
    }
    .form-control {
        padding: 0 5px !important;
    }
</style>
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice">
                <div class="row" style="margin-bottom:10px;">
                    <div class="spa-12">
                        <h3 class="text-blanco"><i class="fa fa-cogs"></i> Productos Courier</h3>
                    </div>
                </div>
                <div class="row desPanel">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div id="tab_pa_1"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>
</div>