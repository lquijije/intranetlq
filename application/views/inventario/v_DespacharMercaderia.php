<style>
	.nav-tabs { border-bottom: 2px solid #DDD; }
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
	.nav-tabs > li > a { border: none; color: #666; }
	.nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #428bca !important; background: #DBECF5; }
	.nav-tabs > li > a::after { content: ""; background: #428bca; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
	.nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
	.tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
	.tab-pane { padding: 2px 0; }
	.tab-content{padding:2px}
	#imaginary_container{
		/*margin-top:20%;  Don't copy this */
	}
	.stylish-input-group .input-group-addon{
		background: white !important; 
	}
	.stylish-input-group .form-control{
		border-right:0; 
		box-shadow:0 0 0; 
		border-color:#ccc;
	}
	.stylish-input-group button{
		border:0;
		background:transparent;
	}
	.glyphicon-refresh-animate {
		-animation: spin .7s infinite linear;
		-webkit-animation: spin2 .7s infinite linear;
	}

	@-webkit-keyframes spin2 {
		from { -webkit-transform: rotate(0deg);}
		to { -webkit-transform: rotate(360deg);}
	}

	@keyframes spin {
		from { transform: scale(1) rotate(0deg);}
		to { transform: scale(1) rotate(360deg);}
	}
	.close-modal-iv{
		float: right;
		font-size: 21px;
		font-weight: bold;
		line-height: 1;
		text-shadow: 0 1px 0 #ffffff;
		opacity: 0.5;
		filter: alpha(opacity=50);
		color: #FFFFFF
		/*padding-left:5px;*/
	}
	.close-modal-iv:hover,
	.close-modal-iv:focus {
		text-decoration: none;
		cursor: pointer;
		opacity: 0.9;
		filter: alpha(opacity=90);
		color: #FFFFFF
	}
	button.close-modal-iv {
		padding: 0;
		cursor: pointer;
		background: transparent;
		border: 0;
		-webkit-appearance: none;
	}
	.dropdown-menu.pull-left {
		left: 0;
		right: auto;
	}
	.div-flex{
		display: -webkit-flex;
		display: flex;
	}
	.div-flex-align-center{
		-webkit-align-items: center;
		align-items: center;
	}
	.div-flex-align-end{
		-webkit-align-items: flex-end;
		align-items: flex-end;
	}
	.div-flex-just-center{
		-webkit-justify-content: center;
		justify-content: center;
	}
	.div-flex-just-left{
		-webkit-justify-content: left;
		justify-content: left;
	}
	.div-flex-just-right{
		-webkit-justify-content: flex-end;
		justify-content: flex-end;
	}
}
</style>

<input type='hidden' id='hdMdlAct' name="txt_modal_acti" 	value="" />
<input type='hidden' id='hdBodSal' name="txt_bodega_salida" value="" />
<input type='hidden' id='hdBodEnt' name="txt_bodega_entra" 	value="" />
<input type='hidden' id='hdCodBod' name="txt_codig_bode" 	value="" />

<div class="demo-wrapper">
	<div class="dashboard">
		<div class="container" >
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
						<span class="glyphicon glyphicon-list-alt">
						</span>   Despachar Mercader&iacutea
					</h4>
				</div>
				<div class="modal-body" style="padding: 1%;">
					<div class="col-sm-12">
						<div class="row div-flex">
							<div class="col-sm-2 panel panel-primary">
								<div class="radio radio-primary" style="margin-top: 6%;">
									<input id="rdPorDesp" type="radio" name="radio-pedido" checked="">
									<label for="rdPorDesp">Por Despachar</label>
								</div>
								<div class="radio radio-primary" style="margin-bottom: 2%;">
									<input id="rdModDesp" type="radio" name="radio-pedido">
									<label for="rdModDesp">Modificar Despachado</label>
								</div>
							</div>
							<div class="col-sm-5">
								<label>Bodega Despacha:</label>
								<!-- <select class="form-control form-control-sm text-size-0-5 cbAlm"><option></option></select> -->
								<label class="form-control lbBodDesp text-size-1-25" style="height: 33px;"></label>
							</div>
							<div class="col-sm-1 div-flex div-flex-align-end" style="padding-bottom: 10px;">
								<a href="#" data-toggle="tooltip" title="Consultar">
									<button type="button" class="btn btn-primary bg-primary btAplFil" style="height: 33px;border-radius: 3px">
										<i class="fa fa-search-plus"></i>
									</button>
								</a>
							</div>
						</div>
						<div class="row div-flex">
							<div class="col-sm-12">
								<div class="row panel panel-primary text-center contDatos" style="padding: 5px;">
									<table id="table_grid_datos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">
										<thead>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Zona#</th>	
											<th style="background-color:#428bca; color:white; font-size:1em;">Picking#</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Nº Ped</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Solicita</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Items</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Fecha Pedido</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Pedido Por</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Motivo</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Observacion</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Usuario</th>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Impreso</th>
										</thead>
										<tbody>
										</tbody>
										<tfoot>										
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>  
				<div class="panel-footer" style="text-align: right; padding-bottom: 52px;padding-top: 5px;">
					<div class="col-sm-7">

					</div>
					<div class="col-sm-5" style="text-align: right;">
						<div class="col-sm-1">
						</div>
						<div class="col-sm-3 text-center">
							<div class="row no-margin" style="padding-right:5px;">
								<button class="btn btn-danger btn-xs fa fa-check-square fa-2x btGene"></button> 	
							</div>
							<div class="row no-margin no-margin">
								<label class="control-label text-size-0-5 text-bold no-padding no-margin">Generar Pedido</label>
							</div>	
						</div>
						<div class="col-sm-2 text-center">
							<div class="row no-margin" style="padding-right:5px">
								<button class="btn btn-success btn-xs fa fa-check-square fa-2x btCopi"></button> 	
							</div>
							<div class="row no-margin no-margin">
								<label class="control-label text-size-0-5 text-bold no-padding no-margin">Copiar</label>
							</div>	
						</div>
						<div class="col-sm-2 text-center">
							<div class="row no-margin" style="padding-right:5px">
								<button class="btn btn-danger btn-xs fa fa-check-square fa-2x btProc"></button> 	
							</div>
							<div class="row no-margin no-margin">
								<label class="control-label text-size-0-5 text-bold no-padding no-margin">Procesar</label>
							</div>	
						</div>
						<div class="col-sm-2 text-center">
							<div class="row no-margin" style="padding-right:5px">
								<button class="btn btn-success btn-xs fa fa-check-square fa-2x btImpr"></button> 	
							</div>
							<div class="row no-margin no-margin">
								<label class="control-label text-size-0-5 text-bold no-padding no-margin">Imprimir</label>
							</div>	
						</div>
						<div class="col-sm-2 text-center">
							<div class="row no-margin" style="padding-right:5px">
								<button class="btn btn-danger btn-xs fa fa-check-square fa-2x btMod"></button> 	
							</div>
							<div class="row no-margin no-margin">
								<label class="control-label text-size-0-5 text-bold no-padding no-margin">Modificar</label>
							</div>	
						</div>

					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="modal" id="modalBodPed">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvBdPBar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spBdPBar"><i class="fa fa-search-plus"></i> Escoja la bodega que despacha</span>
			</div>
			<div class="modal-body" id="dvBdPCont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableBdP" id="contTableBdP" >
								<?php echo $table_ped ?>
							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalDetPed">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvDetPedBar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spDetPedBar"><i class="fa fa-truck"></i> Detalle del pedido </span>
			</div>
			<div class="modal-body" id="dvDetPedCont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<!-- <div class="row div-flex div-flex-align-end div-flex-just-center" id="dvDetPedSearch" style = "padding: 10px;">
						<input type="text" class="form-control txDetPedB"  placeholder="Ingrese Nombre, Placa, Cédula o RUC" >
						<a href="#" data-toggle="tooltip" title="Buscar">
							<button type="button" class="btn btn-primary bg-primary btDetPedB" style="height: 33px;margin:1px; border-radius: 3px">
								<i class="fa fa-search"></i>
							</button>
						</a>
						<a href="#" data-toggle="tooltip" title="Nuevo">
							<button type="button" class="btn btn-primary bg-primary btDetPedN" style="height: 33px;margin:1px; border-radius: 3px">
								<i class="fa fa-file-o"></i>
							</button>
						</a>
					</div> -->
					<div class="row div-flex div-flex-align-end div-flex-just-center">
						<div class="col-md-12">
							<div class="row well text-center contTableDetPed" >
								
							</div>	
						</div>
					</div>

				</div>      	
			</div>
			<!-- <div class="panel-footer" style="text-align: right; padding-bottom: 52px;padding-top: 5px;">
			</div> -->
		</div>
	</div>
</div>
