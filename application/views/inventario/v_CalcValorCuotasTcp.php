<style type="text/css">
	.stylish-input-group .input-group-addon{
		background: white !important; 
	}
	.stylish-input-group .form-control{
		border-right:0; 
		box-shadow:0 0 0; 
		border-color:#ccc;
	}
	.stylish-input-group button{
		border:0;
		background:transparent;
	}
	.glyphicon-refresh-animate {
		-animation: spin .7s infinite linear;
		-webkit-animation: spin2 .7s infinite linear;
	}

	@-webkit-keyframes spin2 {
		from { -webkit-transform: rotate(0deg);}
		to { -webkit-transform: rotate(360deg);}
	}

	@keyframes spin {
		from { transform: scale(1) rotate(0deg);}
		to { transform: scale(1) rotate(360deg);}
	}
	.close-modal-iv{
		float: right;
		font-size: 21px;
		font-weight: bold;
		line-height: 1;
		text-shadow: 0 1px 0 #ffffff;
		opacity: 0.5;
		filter: alpha(opacity=50);
		color: #FFFFFF
		/*padding-left:5px;*/
	}
	.close-modal-iv:hover,
	.close-modal-iv:focus {
		text-decoration: none;
		cursor: pointer;
		opacity: 0.9;
		filter: alpha(opacity=90);
		color: #FFFFFF
	}
	button.close-modal-iv {
		padding: 0;
		cursor: pointer;
		background: transparent;
		border: 0;
		-webkit-appearance: none;
	}
	.div-flex{
		display: -webkit-flex;
		display: flex;
	}
	.div-flex-align-center{
		-webkit-align-items: center;
		align-items: center;
	}
	.div-flex-align-end{
		-webkit-align-items: flex-end;
		align-items: flex-end;
	}
	.div-flex-just-center{
		-webkit-justify-content: center;
		justify-content: center;
	}
	.div-flex-just-left{
		-webkit-justify-content: left;
		justify-content: left;
	}

</style>
<input type='hidden' id='hdCodigo' name="txt_codigo_articulo" 	value="" />
<input type='hidden' id='hdMdlAct' name="txt_modal_acti" 		value="" />
<div class="demo-wrapper">
	<div class="dashboard">
		<div class="container" >
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
						<span class="glyphicon glyphicon-info-sign">
						</span> C&aacutelculo de Valor de Cuotas Cr&eacutedito Directo Pycca.
					</h4>
				</div>

				<div class="modal-body" style="padding: 13px;">
					<div class="col-sm-12">
						<div class="row div-flex">
							<div class="col-sm-9 panel panel-primary" style="padding: 15px;">
								<div class="row">
									<div class="col-sm-3">
										<div class="input-group">
											<div class="input-group-addon bg-primary text-blanco">
												<i class="fa fa-shopping-bag"></i>
											</div>
											<input class="form-control txCdAr" placeholder="Cód. artículo" type="text">
										</div>
									</div>
									<div class="col-sm-7">
										<label class="form-control text-size-1-25 lbDsArt" for=""></label>
									</div>
									<div class="col-sm-2">
										<a href="#" data-toggle="tooltip" title="Buscar" style = "margin-right: 2px;">
											<button type="button" class="btn btn-primary bg-primary btMainBus" style="width: 40px; height: 33px;border-radius: 3px;">
												<i class="fa fa-search-plus"></i>
											</button>
										</a>
										<a href="#" data-toggle="tooltip" title="Agregar" style = "margin-right: 2px;">
											<button type="button" class="btn btn-primary bg-primary btMainAgr" style="width: 40px; height: 33px;border-radius: 3px;">
												<i class="fa fa-plus-square-o"></i>
											</button>
										</a>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<div class="input-group">
											<div class="input-group-addon bg-primary text-blanco">
												<i class="fa fa-hashtag"></i>
											</div>
											<input class="form-control txCant" placeholder="Cantidad" type="text">
										</div>
									</div>
									<div class="col-sm-7">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<div class="input-group">
											<div class="input-group-addon bg-primary text-blanco">
												<i class="fa fa-money"></i>
											</div>
											<label class="form-control lbPvpIva" for="">PVP+IVA</label>
										</div>
									</div>
									<div class="col-sm-7">
										<label class="form-control text-size-1-25 lbOfert" for=""></label>
									</div>
								</div>
							</div>
							<div class="col-sm-3 div-flex div-flex-align-end" style="padding: 10px;" >
								<input class="form-control text-size-2 txCap" style="text-align: right;" placeholder="" type="text">
								<a href="#" data-toggle="tooltip" title="Limpiar" style = "padding-bottom: 2px;margin-left: 5px;">
									<button type="button" class="btn btn-primary bg-primary btMainLimp" style="height: 33px;border-radius: 3px;">
										<i class="fa fa-paint-brush"></i>
									</button>
								</a>
							</div>
						</div>
						
						<div class="row" style="display: flex;">
							<div class="col-sm-9 panel panel-primary contTableArtic" style="padding: 15px;" >
								<table id="table_grid_articulos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">
									<thead>
										<th style="background-color:#428bca; color:white; font-size:1.2em;">Código</th>	
										<th style="background-color:#428bca; color:white; font-size:1.2em;"></th>
										<th style="background-color:#428bca; color:white; font-size:1.2em;">Descripción</th>
										<th style="background-color:#428bca; color:white; font-size:1.2em;">Cant.</th>
										<th style="background-color:#428bca; color:white; font-size:1.2em;">PVP</th>
										<th style="background-color:#428bca; color:white; font-size:1.2em;">Subtotal</th>
									</thead>
									<tbody>
									</tbody>
									<tfoot>										
									</tfoot>
								</table>
							</div>
							<div class="col-sm-3 panel panel-primary contTableAmort" style="padding: 15px;">
								<table id="table_grid_cuotas" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">
									<thead>
										<th style="background-color:#428bca; color:white; font-size:1.2em;">#Cuota</th>	
										<th style="background-color:#428bca; color:white; font-size:1.2em;"> Valor </th>
									</thead>
									<tbody>
									</tbody>
									<tfoot>										
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>  
				<div class="panel-footer" style="text-align: right;">
					<div class="row no-margin" style="padding-right:8px;">
						<button class="btn btn-danger btn-xs fa fa-check-square fa-2x btProc"></button> 	
					</div>
					<div class="row no-margin no-margin">
						<label class="control-label text-size-0-5 text-bold no-padding no-margin">Procesar</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalBusqueda">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-primary" id="dvBusBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-search"></i> Consulta de Artículos</span> 
			</div>
			<div class="modal-body" id="dvBusCont" style="padding-top: 45px;padding-bottom: 15px;">
				<div class="content" style="margin-top: 0px;">
					<div class="row" id="dvBusSearch" style="margin:20px;">
						<div class="col-md-11">
							<input type="text" class="form-control txBusB"  placeholder="Ingrese código o descripción de articulo" >
						</div>
						<div class="col-md-1">
							<a href="#" data-toggle="tooltip" title="Buscar"><button type="button" class="btn btn-primary bg-primary btBusB" style="height: 33px;border-radius: 3px"><i class="fa fa-search"></i></button></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableBusqueda">
								<!-- AQUI VA LA TABLA DE CONSULTA DE ARTICULOS -->

							</div>	
						</div>
					</div>
				</div>   
			</div>
		</div>
	</div>
</div>

<div class="modal" id="confirmModal" style="display: none;z-index: 1050; width: 40%;top:35%">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary" id="dvBusBar">
				<button type="button" class="close-modal-iv" id="confirmClose" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-question-circle"></i> Confirmación</span> 
			</div>
			<div class="modal-body text-blue text-size-1-5" id="confirmMessage">
			</div>
			<div class="modal-footer well" style="padding: 10px;margin: 0px;">
				<button type="button" class="btn btn-primary" id="confirmOk">Ok</button>
				<button type="button" class="btn btn-default" id="confirmCancel">Cancel</button>
			</div>
		</div>
	</div>
</div>