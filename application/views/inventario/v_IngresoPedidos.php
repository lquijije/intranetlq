<style>
	.nav-tabs { border-bottom: 2px solid #DDD; }
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
	.nav-tabs > li > a { border: none; color: #666; }
	.nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #428bca !important; background: #DBECF5; }
	.nav-tabs > li > a::after { content: ""; background: #428bca; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
	.nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
	.tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
	.tab-pane { padding: 2px 0; }
	.tab-content{padding:2px}
	#imaginary_container{
		/*margin-top:20%;  Don't copy this */
	}
	.stylish-input-group .input-group-addon{
		background: white !important; 
	}
	.stylish-input-group .form-control{
		border-right:0; 
		box-shadow:0 0 0; 
		border-color:#ccc;
	}
	.stylish-input-group button{
		border:0;
		background:transparent;
	}
	.glyphicon-refresh-animate {
		-animation: spin .7s infinite linear;
		-webkit-animation: spin2 .7s infinite linear;
	}

	@-webkit-keyframes spin2 {
		from { -webkit-transform: rotate(0deg);}
		to { -webkit-transform: rotate(360deg);}
	}

	@keyframes spin {
		from { transform: scale(1) rotate(0deg);}
		to { transform: scale(1) rotate(360deg);}
	}
	.close-modal-iv{
		float: right;
		font-size: 21px;
		font-weight: bold;
		line-height: 1;
		text-shadow: 0 1px 0 #ffffff;
		opacity: 0.5;
		filter: alpha(opacity=50);
		color: #FFFFFF
		/*padding-left:5px;*/
	}
	.close-modal-iv:hover,
	.close-modal-iv:focus {
		text-decoration: none;
		cursor: pointer;
		opacity: 0.9;
		filter: alpha(opacity=90);
		color: #FFFFFF
	}
	button.close-modal-iv {
		padding: 0;
		cursor: pointer;
		background: transparent;
		border: 0;
		-webkit-appearance: none;
	}
	.dropdown-menu.pull-left {
		left: 0;
		right: auto;
	}
	.div-flex{
		display: -webkit-flex;
		display: flex;
	}
	.div-flex-align-center{
		-webkit-align-items: center;
		align-items: center;
	}
	.div-flex-align-end{
		-webkit-align-items: flex-end;
		align-items: flex-end;
	}
	.div-flex-just-center{
		-webkit-justify-content: center;
		justify-content: center;
	}
	.div-flex-just-left{
		-webkit-justify-content: left;
		justify-content: left;
	}
	.div-flex-just-right{
		-webkit-justify-content: flex-end;
		justify-content: flex-end;
	}
}
</style>

<input type='hidden' id='hdMdlAct' 	name="txt_modal_acti" 		value="" />
<input type='hidden' id='hdHoy' 	name="txt_fecha_hoy" 		value=<?php echo $hoy?> />
<input type='hidden' id='hdBodPid' 	name="txt_bodega_pide" 		value="" />
<input type='hidden' id='hdBodA' 	name="txt_bodega_a" 		value="" />
<input type='hidden' id='hdMotivo' 	name="txt_motivo_pedido" 	value="" />
<input type='hidden' id='hdTipBod' 	name="txt_tipo_bodega" 		value="" />
<input type='hidden' id='hdDspArt' 	name="txt_disponible_art" 	value="" />
<input type='hidden' id='hdSalArt' 	name="txt_saldo_art"	 	value="" />
<input type='hidden' id='hdCodFab' 	name="txt_codigo_fabrica"	value="" />
<input type='hidden' id='hdPedCon' 	name="txt_cont_pedidos" 	value="" />
<input type='hidden' id='hdPedida' 	name="txt_cant_pedida"	 	value="" />


<div class="demo-wrapper">
	<div class="dashboard">
		<div class="container" >
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
						<span class="glyphicon glyphicon-list-alt">
						</span>   Ingreso de Pedidos
					</h4>
				</div>
				<div class="modal-body" style="padding: 1%;">
					<div class="col-sm-12">
						<div class="row" >
							<div class="col-sm-7">
								<div class="row" style="margin-top: 17px;">
									<div class="col-sm-2" >
										<div class="col-sm-5">
											<label>Pide:</label>
										</div>
										<div class="col-sm-3">
											<label class="form-control lbPuedePed" style="height: 25px"></label>	
										</div>
										<div class="col-sm-3">
											<label class="form-control lbTiBodPid" style="height: 25px"></label>
										</div>
									</div>
									
									<div class="col-sm-10">
										<div class="input-group">
											<label class="form-control lbBodPide" style="height: 25px"></label>
											<div class="input-group-addon bg-primary text-blanco no-margin no-padding" style="border-radius: 5px;">
												<a href="#" data-toggle="tooltip" title="Buscar Bodega Pide"  data-placement="right">
													<button type="button" class="btn btn-primary bg-primary btBPide" style="height: 25px;padding-top: 1px;">
														<i class="fa fa-search-plus"></i>
													</button>
												</a>
											</div>											
										</div>
									</div>
								</div>
								<div class="row" style="margin-top: 6px;">
									<div class="col-sm-2">
										<div class="col-sm-5">
											<label>A:</label>
										</div>
										<div class="col-sm-3">
												
										</div>
										<div class="col-sm-3">
											<label class="form-control lbTiBodA" style="height: 25px"></label>
										</div>
									</div>
									<div class="col-sm-10">
										<div class="input-group">
											<label class="form-control lbBodA" style="height: 25px"></label>
											<div class="input-group-addon bg-primary text-blanco no-margin no-padding" style="border-radius: 5px;">
												<a href="#" data-toggle="tooltip" title="Buscar Bodega A" >
													<button type="button" class="btn btn-primary bg-primary btBodA" style="height: 25px;padding-top: 1px;">
														<i class="fa fa-search-plus"></i>
													</button>
												</a>
											</div>											
										</div>
									</div>
								</div>
								<div class="row" style="margin-top: 6px;">
									<div class="col-sm-2">
										<label>Motivo Tras:</label>
									</div>
									<div class="col-sm-10">
										<div class="input-group">
											<label class="form-control lbMotTra" style="height: 25px"></label>
											<div class="input-group-addon bg-primary text-blanco no-margin no-padding" style="border-radius: 5px;">
												<a href="#" data-toggle="tooltip" title="Buscar Motivo de Traspaso" >
													<button type="button" class="btn btn-primary bg-primary btMotTra" style="height: 25px;padding-top: 1px;">
														<i class="fa fa-search-plus"></i>
													</button>
												</a>
											</div>											
										</div>
									</div>
								</div>
								<div class="row" style="margin-top: 6px;">
									<div class="col-sm-2">
										<label>Pedido Por:</label>
									</div>
									<div class="col-sm-10">
										<input class="form-control txPedPor" type="text" style="height: 25px;">
									</div>
								</div>
							</div>
							<div class="col-sm-5" >
								<div class="row">
									<label>Observaci&oacuten: </label>
									<textarea class="form-control" id="taObs" style="resize: none;height: 85px;"></textarea>	
								</div>
								<div class="row hide" id="dvTipFact">
									<div class="col-sm-3" style="padding-top: 12px;">
										<label>Factura #: </label>									
									</div>
									<div class="col-sm-4"  style="padding-top: 12px;">
										<input class="form-control txFactNum" type="text" style="height: 25px;">
									</div>
									<div class="col-sm-4 text-center" style="padding-top: 13px;">
										<select name="tipoFactura" id="cbTiFac">
											<option value="1">FACTURA o GUIA DE HOY</option>
											<option value="2">FACTURA DIAS PASADOS</option>
											<option value="3">GUIA DE DIAS PASADOS</option>
											<option value="4">FACTURA MAYORISTA</option>
										</select>
									</div>
								</div>
								<div class="row" >
									<div class="col-sm-3" style="padding-top: 12px;">
										<label>Fecha Pedido: </label>									
									</div>
									<div class="col-sm-4"  style="padding-top: 12px;">
										<label class = "form-control" style="height: 25px;"><?php echo date('Y-m-d',strtotime($hoy))?></label>
									</div>
									<div class="col-sm-4 text-center" style="padding-top: 5px;">
										<div class="checkbox checkbox-primary">
											<input id="chkPre" type="checkbox" name="checkbox-prepedido" checked="" >
											<label for="chkPre" >Pre Pedido</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row panel panel-info" style="padding: 10px;background-color: #DBECF5;margin-left: 8px;margin-right:10px ">
							<div class="row">
								<!-- <div class="col-sm-2">
							</div> -->
							<div class="col-sm-2">
								<div class="col-xs-4">
									<label>Art&iacuteculo: </label>
								</div>
								<div class="col-xs-8">
									<input class="form-control txCodArt" type="text" style="text-align: center;height: 26px;" ></input>	
								</div>
							</div>
							<div class="col-sm-8">
								<div class="input-group">
									<label class="form-control lbDesArt" style="height: 25px"></label>
									<div class="input-group-addon bg-primary text-blanco no-margin no-padding" style="border-radius: 5px;">
										<a href="#" data-toggle="tooltip" title="Buscar Artículo" >
											<button type="button" class="btn btn-primary bg-primary btBusArt" style="height: 25px;padding-top: 1px;">
												<i class="fa fa-search-plus"></i>
											</button>
										</a>
									</div>											
								</div>
							</div>
							<div class="col-sm-2">
								<div class="col-xs-5">
									<label>Cantidad: </label>
								</div>
								<div class="col-xs-7">
									<input class="form-control txCant" type="text" style="text-align: center;height: 26px;"  ></input>
								</div>	
							</div>
						</div>
					</div>
					<div class="row panel" style="padding: 5px;">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item active">
								<a class="nav-link" data-toggle="tab" href="#pnlLstPed" role="tab">
									<i class="fa fa-check-circle"></i> Listar Pedido
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#pnlPed" role="tab">
									<i class="fa fa-check-circle"></i> Pedidos
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#pnlErrPed" role="tab">
									<i class="fa fa-check-circle"></i> Error de Pedido
								</a>
							</li>
							<div class="div-flex div-flex-align-center div-flex-just-right" style="height: 38px;padding-right: 2px;">
								<a href="#" data-toggle="tooltip" title="Ingresar" >
									<button type="button" class="btn btn-primary bg-primary btIng" style="border-radius: 3px;height: 28px; margin-left: 5px;">
										<i class="fa fa-plus"></i>
									</button>
								</a>
								<a href="#" data-toggle="tooltip" title="Faltante" >
									<button type="button" class="btn btn-primary bg-primary btFal" style="border-radius: 3px;height: 28px; margin-left: 5px;">
										<i class="fa fa-check-circle"></i>
									</button>
								</a>
							</div>
						</ul>
						<div class="tab-content" style="background-color: #DBECF5;">
							<div class="tab-pane active" id="pnlLstPed" role="tabpanel">
								<div class="row contLstPed" style="margin-left: 30px;margin-right: 30px;">
									<table id="table_lst_pedidos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">
										<thead>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Código</th>	
											<th style="background-color:#428bca; color:white; font-size:1em;"></th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Artículo</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Cantidad</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Error</th>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Fábrica</th>
										</thead>
										<tbody>
										</tbody>
										<tfoot>										
										</tfoot>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="pnlPed" role="tabpanel">
								<div class="row contPed" style="margin-left: 30px;margin-right: 30px;">
									<table id="table_pedidos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">
										<thead>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Nº_Pedido</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Entra</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Sale</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Fecha_Ped.</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Pedido Por.</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Motivo</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Observación</th>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Usuario</th>
										</thead>
										<tbody>
										</tbody>
										<tfoot>										
										</tfoot>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="pnlErrPed" role="tabpanel">
								<div class="row contErrPed" style="margin-left: 30px;margin-right: 30px;">
									<table id="table_err_pedidos" class="table table-bordered text-size-0-5 no-padding no-margin text-bold">
										<thead>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-left-radius: 5px;">Código</th>	
											<th style="background-color:#428bca; color:white; font-size:1em;"></th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Artículo</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Cantidad</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Nº Pedido</th>
											<th style="background-color:#428bca; color:white; font-size:1em;">Entgra</th>
											<th style="background-color:#428bca; color:white; font-size:1em;border-top-right-radius: 5px;">Sale</th>
										</thead>
										<tbody>
										</tbody>
										<tfoot>										
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>  
			</div>
			<div class="panel-footer" style="text-align: right; padding-bottom: 52px;padding-top: 5px;">
				<div class="col-sm-10">

				</div>
				<div class="col-sm-2" style="text-align: right;">
					<div class="col-sm-8">
						<div class="row no-margin" style="padding-right:5px">
							<button class="btn btn-danger btn-xs fa fa-check-square fa-2x btProc"></button> 	
						</div>
						<div class="row no-margin no-margin">
							<label class="control-label text-size-0-5 text-bold no-padding no-margin">Procesar</label>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="row no-margin" style="padding-right:5px">
							<button class="btn btn-success btn-xs fa fa-check-square fa-2x btApr"></button> 	
						</div>
						<div class="row no-margin no-margin">
							<label class="control-label text-size-0-5 text-bold no-padding no-margin">Aprobar</label>
						</div>	
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>

<div class="modal" id="modalAprPrePed">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-primary" id="dvAprobBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-pencil-square-o"></i> Consulta/Modificación de Pre-pedidos</span> 
			</div>
			<div class="modal-body" id="dvAprobCont">
				<div class="content">
					<div class="row" id="dvAprobSearch" style="margin-left: 5px;margin-right: 5px;">
						<div class="row">
							<div class="row">
								<div class="col-sm-7">
									<label >Pre-pedidos</label>
								</div>
								<div class="col-sm-5">
									<label >Autorizar pedidos reposición Fuera de Horario</label>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-7 panel panel-info" style="padding: 8px;background-color: #DBECF5;">
									<div class="col-md-8">
										<input type="text" class="form-control txAprobB text-size-0-5 txAprBus"  placeholder="Ingrese # de pedido o nombre de almacén" >
									</div>
									<div class="col-md-4" style="text-align: right;">
										<a href="#" data-toggle="tooltip" title="Filtrar">
											<button type="button" class="btn btn-primary bg-primary btAprFil" style="height: 33px;border-radius: 3px">
												<i class="fa fa-filter"></i>
											</button>
										</a>
										<a href="#" data-toggle="tooltip" title="Autorizar">
											<button type="button" class="btn btn-primary bg-primary btAprPed" style="height: 33px;border-radius: 3px">
												<i class="fa fa-check-square-o"></i>
											</button>
										</a>
										<a href="#" data-toggle="tooltip" title="Anular">
											<button type="button" class="btn btn-primary bg-primary btAnlPed" style="height: 33px;border-radius: 3px">
												<i class="fa fa-window-close-o"></i>
											</button>
										</a>
									</div>
								</div>	
								<div class="col-sm-5 panel panel-info" style="padding: 8px;background-color: #DBECF5;">
									<div class="col-sm-10">
										<select class="form-control form-control-sm text-size-0-5 cbAlm"><option></option></select>
									</div>
									<div class="col-sm-2">
										<a href="#" data-toggle="tooltip" title="Autorizar">
											<button type="button" class="btn btn-primary bg-primary btAprRep" style="height: 33px;border-radius: 3px">
												<i class="fa fa-check-square-o"></i>
											</button>
										</a>
									</div>
								</div>
							</div>							
						</div>
					</div>
					<div class="row"  style="padding-top: 5px;">
						<div class="col-md-12">
							<div class="row well text-center contTableAprPrePed" >
								<!-- AQUI VA LA TABLA DE CONSULTA DE AprPrePed -->

							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalDetPed">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-primary" id="dvDetPedBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-list"></i> Detalle del pedido</span> 
			</div>
			<div class="modal-body" id="dvDetPedCont">
				<div class="content">
					<div class="row" id="dvDetPedSearch" style="margin-left: 5px;margin-right: 5px;">
						<div class="col-sm-4">
							<label for="">Nº Pedido:</label>
							<label for="" class="form-control lbNoPed" style="height: 25px;"></label>
						</div>
						<div class="col-sm-4">
							<label for="">Despachador:</label>
							<label for="" class="form-control lbDespa" style="height: 25px;"></label>
						</div>
						<div class="col-sm-4">
							<label for="">Pedido Por:</label>
							<label for="" class="form-control lbPedPor" style="height: 25px;"></label>
						</div>
					</div>
					<div class="row"  style="padding-top: 5px;">
						<div class="col-md-12">
							<div class="row well text-center contTableDetPed" >
								<!-- AQUI VA LA TABLA DE CONSULTA DE DetPed -->
								
							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalBodPide">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvBdPBar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spBdPBar"><i class="fa fa-search-plus"></i> Escoja la bodega que Pide</span>
			</div>
			<div class="modal-body" id="dvBdPCont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableBdP" id="contTableBdP" >
								<?php echo $table_ped ?>
							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalMotivo">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvMotBar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spMotBar"><i class="fa fa-search-plus"></i> Escoja el motivo del pedido</span>
			</div>
			<div class="modal-body" id="dvMotCont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableMot">
								<?php echo $table_mot ?>
							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalBodA">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header" id="dvBdABar" style="background-color: #428bca;">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco" id="spBdABar"><i class="fa fa-search-plus"></i> Escoja la bodega a quien pedirá</span>
			</div>
			<div class="modal-body" id="dvBdACont" style="padding-top: 45px;padding-bottom: 5px; padding-left: 0px;padding-right: 0px;">
				<div class="content" >
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableBdA">
								<?php echo $table_bod ?>
							</div>	
						</div>
					</div>
				</div>      	
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalBusqArt">
	<div class="modal-dialog">
		<div class="modal-content" >
			<div class="modal-header bg-primary" id="dvBusBar">
				<button type="button" onclick="quitarModal();" class="close-modal-iv" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-search"></i> Consulta de Artículos</span> 
			</div>
			<div class="modal-body" id="dvBusCont" style="padding-top: 45px;padding-bottom: 15px;">
				<div class="content" style="margin-top: 0px;">
					<div class="row" id="dvBusSearch" style="margin:20px;">
						<div class="col-md-11">
							<input type="text" class="form-control txBusB"  placeholder="Ingrese código o descripción de articulo" >
						</div>
						<div class="col-md-1">
							<a href="#" data-toggle="tooltip" title="Buscar"><button type="button" class="btn btn-primary bg-primary btBusB" style="height: 33px;border-radius: 3px"><i class="fa fa-search"></i></button></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row well text-center contTableBusqueda">
								<!-- AQUI VA LA TABLA DE CONSULTA DE ARTICULOS -->

							</div>	
						</div>
					</div>
				</div>   
			</div>
		</div>
	</div>
</div>


<div class="modal" id="confirmModal" style="display: none;z-index: 1050; width: 40%;top:35%">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary" id="dvBusBar">
				<button type="button" class="close-modal-iv" id="confirmClose" style="color: #FFFFFF;">
					<i class="fa fa-times-circle"></i>
					<span class="sr-only">Close</span>
				</button>
				<span class="text-size-1-25 text-blanco"><i class="fa fa-question-circle"></i> Confirmación</span> 
			</div>
			<div class="modal-body text-blue text-size-1-5" id="confirmMessage">
			</div>
			<div class="modal-footer well" style="padding: 10px;margin: 0px;">
				<button type="button" class="btn btn-primary" id="confirmOk">Ok</button>
				<button type="button" class="btn btn-default" id="confirmCancel">Cancel</button>
			</div>
		</div>
	</div>
</div>