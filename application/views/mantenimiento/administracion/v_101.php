<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice8">
                
                <div class="row" style="margin-bottom:10px;">
                    
                    <div class="pull-left">
                        <span class="titulo-1 text-size-2 text-blanco">Perfiles de Usuario </span>
                    </div>
                    
                    <?php
                    //print_r($treearr);
                    $this->load->view('toolbar/toolbar'); ?>
                    
                </div>
                    
                <div class="row" style="margin-top: 2px;">
                    <div class="box box-primary">
                        <div class="box-body">
                            
                            <div id="contTablePerfil">
                                
                            </div>

                        <div class="clearfix"></div>
                            
                        </form>

                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalPerfiles">
  <div class="modal-dialog">
    <div class="modal-header bg-red">
        <button type="button" onclick="resetPerfil();" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
        </button>
        <h3 class="modal-title">Nuevo Perfiles de Usuario</h3>
    </div>
      
    <div class="modal-content">
        <?php 
            $attributes = array('id' => 'form_perfiles','name'=>'form_perfiles');
            $hidden=array('txt_accion'=>'',
                          'id_perfil'=>'');   
            echo form_open('mantenimiento/administracion/co_101',$attributes,$hidden); 
        ?>   
        <div class="modal-body">
            <div class="spa-5">
                <div class="spa-12">
                <label>Nombre</label>
                    <?php  $data= array('id'=>'txt_nombre','name'=>'txt_nombre','class'=>'form-control');
                    echo form_input($data,set_value('txt_nombre')); ?>
                </div>
                
                <div class="spa-12">
                    <label>Widgets</label>
                    <?php

                    if(isset($widget) && count($widget) > 0){

                        ?> <ul class="list-unstyled"> <?php

                            foreach ($widget AS $row){
                                ?> <li> <input id="cb_w<?php echo trim($row['codigo']) ?>" name="cb_w[]" type="checkbox" value="<?php echo trim($row['codigo']) ?>"> <?php echo utf8_encode($row['descripcion']); ?> </li><?php
                            }

                        ?> </ul> <?php

                    } else{
                        echo '<div>NO EXISTEN WIGGETS DISPONIBLES</div>';
                    }

                    ?>
                </div>  
            </div>
                <?php echo ''; 
                    $cantidad=count($treearr);
                    $char='';
                    for ( $i = 0; $i < $cantidad ; $i++) {
                        $char.= 'cb_'.$treearr[$i][1].'|';
                    }
                ?>
            <div class="spa-7">
                <label>Permisos</label>
                <input type="hidden" name="treview_valor" id="treview_valor" value="<?php echo $char; ?>" />
                <div id="TV">
                    <?php  echo $treeview; ?>
                </div>
            </div>  

            
        </div>
        <div class="clear"></div>
        <div class="modal-footer">
          <button type="reset" class="btn btn-default resetForm">Cancelar</button>
          <button type="button" class="btn btn-primary saveForm">Guardar</button>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalopciones_especiales">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" onclick="resetOption();" class="close"><i class="fa fa-times-circle"></i><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Opciones Especiales</h4>
        </div>
        
        <?php 
            $attributes = array('id' => 'form_opciones_especiales','name'=>'form_opciones_especiales');
            $hidden=array('id_optionMenu'=>'');   
            echo form_open('mantenimiento/administracion/co_101',$attributes,$hidden); 
        ?>   
        <div class="modal-body">
            <div class="spa-7">
                <input type="text" class="form-control" id="txt_op_id" name="txt_op_id" value="">
            </div>
            <div class="spa-3">
                <input type="text" class="form-control" id="txt_op_val" name="txt_op_val" value="">
            </div>
            <div class="spa-1">
                <button type="button" class="btn btn-primary add_option" style="height: 35px;"><i class="fa fa-plus"></i></button>
            </div>
            <div class="spa-12" style="margin-top: 10px;">
                <table class="table table-bordered"> <thead><th>Identificador</th><th>Valor</th></thead><tbody id="table_body_option"></tbody></table>
            </div>
            
        </div>
        <div class="clear"></div>
        <div class="modal-footer">
          <button type="reset" class="btn btn-default resetFormOpciones">Cancelar</button>
          <button type="button" class="btn btn-primary saveFormOpciones">Guardar</button>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>
                                    	
<script language="javascript">
        
        $('#table_Perfil').dataTable();
        function markChild(nodeId)
        {
                var status = document.getElementById("cb_" + nodeId ).checked;
                var childIds = treeview.getNode(nodeId).getChildIds();	
                for(var i in childIds)
        {
                        document.getElementById("cb_" + childIds[i] ).checked = status;
                        markChild(childIds[i]);	
        }		
        }

        function markChildConsulta(nodeId)
        {
                var status = document.getElementById("cb_" + nodeId ).checked;
                var childIds = treeview.getNode(nodeId).getChildIds();	
                for(var i in childIds)
        {
                        document.getElementById("cb_" + childIds[i] ).checked = status;
                        markParentConsulta(childIds[i]);	                                
                        markChildConsulta(childIds[i]);	
        }		
        }

        function markParentConsulta(nodeId) {   
           if (nodeId.indexOf(".root")<0) {
                var parentId = treeview.getNode(nodeId).getParentId();
                var siblingIds = treeview.getNode(parentId).getChildIds();
                var _parentSelected = false;
                for(var i in siblingIds)
                        if (document.getElementById("cb_" + siblingIds[i]).checked) 
                                _parentSelected = true;
                document.getElementById("cb_" + parentId).checked = _parentSelected;
                markParentConsulta(parentId);
            }

        }
        function DmarkChild(nodeId,enabledisable)
        {
                var childIds = treeview.getNode(nodeId).getChildIds();	
                for(var i in childIds)
        {
                        document.getElementById("cb_" + childIds[i] ).disabled = enabledisable;
                        DmarkChild(childIds[i],enabledisable);	
        }		
        }

        function DmarkParent(nodeId,enabledisable)
        {
                if (nodeId.indexOf(".root")<0)
                {
                        var parentId = treeview.getNode(nodeId).getParentId();
                        var siblingIds = treeview.getNode(parentId).getChildIds();
                        //var _parentSelected = true;
                        for(var i in siblingIds)
                                if (!document.getElementById("cb_" + siblingIds[i]).disabled!==enabledisable) 
                                        _parentSelected = false;
                        document.getElementById("cb_" + parentId).disabled = enabledisable;
                        DmarkParent(parentId,enabledisable);
                }		

        }                

        function toogle(nodeId)
        {
                markParentConsulta(nodeId);
                markChild(nodeId)
                //alert($("#treview_valor").val());                        
        }
        function toogleCarga(nodeId)
        {
                markChildConsulta(nodeId);
                //markParentConsulta(nodeId);
        }   
        function Deshabilitatoogle(nodeId,enabledisable) {
               document.getElementById("cb_" + nodeId).disabled = enabledisable;                
               DmarkChild(nodeId,enabledisable);
               DmarkParent(nodeId,enabledisable);// 

        }                

</script>