
<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice4">
                <div class="row" style="margin-bottom: 10px;">
                    <h3 class="text-blanco"><i class="fa fa-newspaper-o"></i> Noticias Pycca</h3>
                    <button type="button" class="btn btn-success btnN" style="position: absolute;top: 0;right: 0;"><i class="fa fa-plus"></i> Nuevo</button>
                </div>
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">                      
                            <div id="contTableNoticias" class="row" style="padding-left: 17px; padding-right: 17px;"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<div class="modal" id="modalNoticias">
    <div class="modal-dialog">
        <div class="modal-header bg-red">
            <button type="button" onclick="resetNoticia();" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Noticias</h3>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <?php
                $attributes = array('id' => 'form_noticias', 'name' => 'form_noticias');
                $hidden = array('txt_accion' => '', 'id_noticia' => '');
                echo form_open('mantenimiento/administracion/co_103', $attributes, $hidden);
                ?>   
                <div class="col-xs-12">
                    <label>Titulo</label>
                    <?php
                    $data = array('id' => 'txt_titulo', 'name' => 'txt_titulo', 'class' => 'form-control');
                    echo form_input($data, set_value('txt_titulo'));
                    ?>                 
                </div>
                <div class="col-xs-12">
                    <label>Descripci&oacute;n</label>
                    <textarea id="txt_descripcion" rows="50"></textarea>
                </div>
                <div class="col-xs-12"> 
                    <button type="button" class="btn btn-success pull-right saveForm" style="margin:10px 0px;"><i class="fa fa-save"></i> Guardar</button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<script>
    CKEDITOR.replace('txt_descripcion');
</script>