<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice5">
                
                <div class="row" style="margin-bottom:10px;">
                    
                    <div class="pull-left">
                        <span class="titulo-1 text-size-2 text-blanco">Administración de Usuarios </span>
                    </div>
                    
                    <?php
                    $this->load->view('toolbar/toolbar'); ?>
                    
                </div>
                    
                <div class="row" style="margin-top: 2px;">
                    <div class="box box-primary">
                        <div class="box-body">
                            
                            <div id="contTableUsers">
                                
                            </div>

                        <div class="clearfix"></div>
                            
                        </form>

                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>

<div class="modal" id="modalUsers">
  <div class="modal-dialog">
 
    <div class="modal-header bg-red">
            <button type="button" onclick="resetUsers();" class="close">
                    <i class="fa fa-times-circle"></i>
                    <span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title">Nuevo Usuario</h3>
    </div>

    <div class="modal-content">
        <?php 
            $attributes = array('id' => 'form_users','name'=>'form_users');
            $hidden=array('txt_accion'=>'',
                          'id_user'=>'',
                          'txt_cod_empresa'=>'',
                          'txt_cod_cc'=>'');   
            echo form_open('mantenimiento/administracion/co_102',$attributes,$hidden); 
        ?>   
        
        <div class="modal-body">
            
            <section>

                <ul class="nav nav-tabs">
                    <li class="active" ><a href="#tab1" data-toggle="tab" id="tab1_click">Informaci&oacute;n</a></li>
                    <li><a href="#tab2" data-toggle="tab">Permisos</a></li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="tab1">
                        
                        <div class="spa-2">
                            <label>Cod. Empleado</label>
                            <?php $data= array('id'=>'txt_cod_emp',
                                'name'=>'txt_cod_emp',
                                'class'=>'form-control',
                                'onblur'=>'consultaEmpleado(this.value)',
                                'onkeyPress'=>"return isNumero(event,this)");
                            echo form_input($data,set_value('txt_cod_emp')); ?>
                        </div>
                        
                        <div class="spa-5">
                            <label>Nombres</label>
                            <?php $data= array('id'=>'txt_nom_emp',
                                'name'=>'txt_nom_emp',
                                'class'=>'form-control',
                                'readonly'=>'readonly');
                            echo form_input($data,set_value('txt_cod_emp')); ?>
                        </div>
                        
                        <div class="spa-5">
                            <label>Apellidos</label>
                            <?php $data= array('id'=>'txt_ape_emp',
                                'name'=>'txt_ape_emp',
                                'class'=>'form-control',
                                'readonly'=>'readonly');
                            echo form_input($data,set_value('txt_cod_emp')); ?>
                        </div>
                        
                        <div class="spa-5">
                            <label>E-Mail</label>
                            <?php $data= array('id'=>'txt_email','name'=>'txt_email','class'=>'form-control');
                            echo form_input($data,set_value('txt_email')); ?>
                        </div>
                        
                        <div class="spa-4">
                            <label>Nombre de Usuario</label>
                            <?php $data= array('id'=>'txt_nom_user','name'=>'txt_nom_user','class'=>'form-control');
                            echo form_input($data,set_value('txt_nom_user')); ?>
                        </div>

                        <div class="spa-3">
                            <label>Clave</label>
                            <?php $data= array('id'=>'txt_pass','maxlength'=>'15','name'=>'txt_pass','class'=>'form-control');        
                            echo form_password($data,set_value('txt_pass')); ?>
                        </div>
                        
                        <div class="spa-2">
                            <label>Cod. Responsable</label>
                            <?php $data= array('id'=>'txt_cod_resp',
                                'name'=>'txt_cod_resp',
                                'class'=>'form-control',
                                'onblur'=>'consultaResponsable(this.value)',
                                'onkeyPress'=>"return isNumero(event,this)");
                            echo form_input($data,set_value('txt_cod_resp')); ?>
                        </div>
                        
                        <div class="spa-5">
                            <label>Nombres</label>
                            <?php $data= array('id'=>'txt_nom_resp',
                                'name'=>'txt_nom_resp',
                                'class'=>'form-control',
                                'readonly'=>'readonly');
                            echo form_input($data,set_value('txt_nom_resp')); ?>
                        </div>
                        
                        <div class="spa-5">
                            <label>Apellidos</label>
                            <?php $data= array('id'=>'txt_ape_resp',
                                'name'=>'txt_ape_resp',
                                'class'=>'form-control',
                                'readonly'=>'readonly');
                            echo form_input($data,set_value('txt_ape_resp')); ?>
                        </div>
                    </div>

                    <div class="tab-pane " id="tab2">
                        <div class="spa-12">
                            <label>Perfiles</label>
                            <?php

                            if(isset($perfiles) && count($perfiles) > 0){

                                ?> <ul class="list-unstyled"> <?php

                                    foreach ($perfiles AS $row){
                                        ?> <li> <input id="cb_w<?php echo trim($row['codigo']) ?>" name="cb_w[]" type="checkbox" value="<?php echo trim($row['codigo']) ?>"> <?php echo utf8_encode($row['descripcion']) ?> <a href="javascript:void(0)" onclick="getTreeview(<?php echo trim($row['codigo']) ?>)"><i class="fa fa-plus-circle"></a></i></li><?php
                                    }

                                ?> </ul> <?php

                            } else{
                                echo '<div>NO EXISTEN PERFILES DISPONIBLES</div>';
                            }

                            ?>
                        </div>  
                    </div>

                <div class="clear"></div>
                </div>
            </section>
            
        </div>
        
        <div class="clearfix"></div>
        
        <div class="modal-footer">
          <button type="reset" class="btn btn-default resetForm">Cancelar</button>
          <button type="button" class="btn btn-primary saveForm">Guardar</button>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<div class="modal" id="modalTreeview">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" onclick="quitarmodalGeneral('Treeview','form_Treeview');" class="close">
                <i class="fa fa-times-circle"></i>
                <span class="sr-only">Close</span></button>
            <h4 class="modal-title">Perfil</h4>
        </div>
        
        <?php 
            $attributes = array('id' =>'form_Treeview','name'=>'form_Treeview');
            echo form_open('mantenimiento/administracion/co_102',$attributes); 
        ?>   
        
        <div class="modal-body">
            <div id="TV">
                <?php  echo $treeview; ?>
            </div>
        </div>
        
        <?php echo form_close(); ?>
        
    </div>
  </div>
</div>

<style>

.defaultKTV label {
    background:none;
    color: #333;
    font-size: 13px;
    font-family: "open-sans-light" !important;
    line-height: 1.1;
    font-style: normal;
    font-weight: 100;
}
</style>