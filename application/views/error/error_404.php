<!--<aside class="right-side">

    <section class="content-header">
        <h1>
            404 Error Page
        </h1>
    </section>

    <section class="content">

        <div class="error-page">
            <h2 class="headline text-info"> 404</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-red"></i> Oops! Lo sentimos ....</h3>
                <p>&bull; La p&aacute;gina <strong> <?php echo $_SERVER["REQUEST_URI"]; ?> </strong> que est&aacute; buscando no se puede encontrar.</p>
                <p>&bull; Con frecuencia  es debido  a alg&uacute;n error al escribir la direcci&oacute;n  de la p&aacute;gina (URL). Compru&eacute;bela  de nuevo  para ver si es correcta.</p>
                <p>&bull; Si usted considera que esta URL  es correcta  por favor comun&iacute;quese con sistemas.</p>
                
                <h3><i class="fa fa-warning text-yellow"></i>Usted puede:</h3>

                <p>&bull; <a href="" onClick='history.back();'> Volver a la p&aacute;gina anterior</a></p>
                <p>&bull; <a href="<?php echo site_url(); ?>general/principal">Ir a la p&aacute;gina de inicio de SIAF</a></p>
            </div>
        </div>

    </section>
</aside>-->

<div class="demo-wrapper">
    <div class="dashboard clearfix">
        <div class="spa-12">
            <section class="content invoice8">
                <section class="content-header">
                    <h1>
                        404 Error Page
                        <small>Error</small>
                    </h1>
                </section>
                
                <div class="box box-primary bg-red-gradient">
                    <div class="box-body">
                        <div class="row">
                            <div class="error-content">
                                <h3><i class="fa fa-warning text-red"></i> Oops! Lo sentimos ....</h3>
                                <ul class="list-unstyled">
                                    <li>&bull; La p&aacute;gina <strong> <?php echo $_SERVER["REQUEST_URI"]; ?> </strong> que est&aacute; buscando no se puede encontrar.</li>
                                    <li>&bull; Con frecuencia  es debido  a alg&uacute;n error al escribir la direcci&oacute;n  de la p&aacute;gina (URL). Compru&eacute;bela  de nuevo  para ver si es correcta.</li>
                                    <li>&bull; Si usted considera que esta URL  es correcta  por favor comun&iacute;quese con sistemas.</li>
                                </ul>
                                
                                <h3><i class="fa fa-warning text-yellow"></i>Usted puede:</h3>
                                <ul class="list-unstyled">
                                    <li>&bull; <a href="" onClick='history.back();'> Volver a la p&aacute;gina anterior</a></li>
                                    <li>&bull; <a href="<?php echo site_url(); ?>general/principal">Ir a la p&aacute;gina de inicio</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
</div>