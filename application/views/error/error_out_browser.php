<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title> PYCCA | INTRANET</title>
        <link rel="icon" type="image/ico" href="<?php echo site_url() ?>img/favicon.ico" />
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo site_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>css/style.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>css/animation.css" rel="stylesheet" type="text/css" />
        
    </head>
   
    <body style="background: #00539F;">
        
    <div class="demo-wrapper">
        <div class="dashboard clearfix">
            <div class="spa-12"  style="text-align: center;">
                <section class="content invoice1">
                    <section class="content-header">
                        <h2 class="text-blanco" style="font-weight: bold;">
                            Oops !Lo sentimos navegador no compatible¡
                        </h2>
                        <p class="text-blanco" style="font-size: 18px;">
                        La versión de este navegador  
                        no es compatible con la nueva intranet.<br> 
                        Por favor actualice por alguna mas reciente ó 
                        seleccione otro navegador.<br><br>
                        Los navegadores compatibles son:<br>
                        <!--- Internet Explorer Versi&oacute;n 9 o superior<br>-->
                        - Google Chrome<br>
                        - Mozilla Firefox<br><br>
                        
                        Usted puede comunicarse a la Ext.: 2222 con soporte a usuario.
                        </p>
                        
                        <img src="<?php echo site_url(); ?>img/b.jpg">
                    </section>

                </section>
            </div>
        </div>
    </div>
        
    </body>
</html>