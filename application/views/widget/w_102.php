<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border:none;
    padding: 0px;
    line-height: 1.42857;
}
</style>


<script src="<?php echo site_url(); ?>js/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?php echo site_url(); ?>js/plugins/flot/jquery.flot.time.js" type="text/javascript"></script>

<div id="w_2" class="tile_py tile_py_2 tile_co_3" data-page-type="r-page" data-page-name="random-r-page">
    <span class="header-graph" style="padding-bottom: 0;">
        <h3 class="titulo-1">Ventas 
            <span style="position: absolute;right: 38px;top: 6px;">
                <i class="fa fa-calendar" style="font-size:17px;"></i> 
                <input name="txt_fec_Ge" readonly="readonly" value="<?php 

                if((int)date('H')>= 0 && date('H') < 10){
                    echo date('Y/m/d',strtotime('-1 day')).' | 23:00'; 
                } else {
                    echo date('Y/m/d').' | '.date('H').':00'; 
                }

                ?>" id="txt_fec_Ge" type="text" style="color: rgb(255, 255, 255); background: none repeat scroll 0% 0% transparent; border: medium none; cursor: pointer; font-size: 16px; width: 143px;">
                <b class="caret"></b>
            </span>
        </h3>
        <button type="button" class="btn-out" onclick="getVentas();" title="Refrescar"><i class="fa fa-refresh"></i></button>
    </span>
    <table class="table">
            <thead>
                <th width='40%'></th>
                <th></th>
                <th width='70'></th>
                <th width='40'></th>
            </thead>
            <tbody id="contW2">
            </tbody>
        </table>
    <span class="pull-right titulo-1" style="position: absolute; bottom: 0; margin: 10px; right: 15px; font-size: 13px; color: #FFF;">Ultima Consulta: <span id="fecConsultaVentas"></span></span>
</div>

<script>
$(function() {
    
    $('#txt_fec_Ge').datetimepicker({
        format:'Y/m/d | H:i',
        lang:'es',
        hour: 19,
        minute: 00 // default time
    });
    
}); 
</script>