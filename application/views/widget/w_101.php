<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border:none;
    padding: 0px;
    line-height: 1.42857;
}
</style>

<div id="w_1" class="tile_py tile_py_3 tile_co_1">
    
    <span class="header-graph">
        <h3 class="titulo-1">Facturación Electronica</h3>
        <div class="pull-left" id="reportrange" style="margin-top: 15px; font-size: 17px;">
            <i class="fa fa-calendar"></i> 
            <span><?php echo date("F j, Y", strtotime('-1 day')); ?> - <?php echo date("F j, Y"); ?></span> 
            <input type="hidden" id="fec_ini" name="fec_ini" value="<?php echo date('Y-m-d', strtotime('-1 day')); ?>">
            <input type="hidden" id="fec_fin" name="fec_fin" value="<?php echo date('Y-m-d'); ?>">
            <b class="caret"></b>
        </div>
        <button type="button" class="btn-out" onclick="getPorcentajes();" title="Refrescar"><i class="fa fa-refresh"></i></button>
        <div class="clearfix"></div>
    </span>
    
    <div class="spa-12" style="padding:20px;color: #0e585e; color: #FFF;">
        <table class="table">
            <thead>
                <th></th>
                <th width="90%"></th>
            </thead>
            <tbody id="graph_porcentaje">
            </tbody>
        </table>
    </div>
    
    <span class="pull-right titulo-1" style="position: absolute; bottom: 0px; margin: 10px; right: 15px; font-size: 13px; color: #FFF;">Ultima Consulta: <span id="fecConsulta"></span></span>
</div>

<script>
   
$(document).ready(function () {
   // getPorcentajes();
    $('#reportrange').daterangepicker({
            startDate: moment().subtract('days', 1),
            endDate: moment(),
            dateLimit: { days: 60 },
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Consultar',
                fromLabel: 'Desde',
                toLabel: 'Hasta',
                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                firstDay: 1
            }
        },
        
        function(start,end){
            $('#fec_ini').val(start.format("YYYY-MM-DD"));
            $('#fec_fin').val(end.format("YYYY-MM-DD"));
            $('#reportrange span').html(start.format('MMMM ,D YYYY') + ' - ' + end.format('MMMM ,D YYYY'));
            getPorcentajes();
        }
    );

});
    
</script>    