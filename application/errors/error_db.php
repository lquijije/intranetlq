<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title> PYCCA | INTRANET</title>
        <link rel="icon" type="image/ico" href="<?php echo site_url() ?>img/favicon.ico" />
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo site_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>css/style.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo site_url(); ?>css/animation.css" rel="stylesheet" type="text/css" />
        
    </head>
   
    <body style="background: #00539F;">
        
    <div class="demo-wrapper">
        <div class="dashboard clearfix">
            <div class="spa-12"  style="text-align: center;">
                <section class="content invoice1">
                    <section class="content-header text-blanco">
                        <img src="<?php echo site_url(); ?>img/b.jpg">
                        <h2 style="font-weight: bold;">
                            <?php echo $heading; ?>
                        </h2>
                        <div class="invoice12" style="border: 5px solid #FFF; padding: 15px; font-size: 15px; text-align: left;">
                            <?php echo $message; ?>
                        </div>
                    </section>

                </section>
            </div>
        </div>
    </div>
        
    </body>
</html>
		
		