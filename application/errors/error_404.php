
<div id="cont-pantalla">
<div class="contenido">

<div class="alert-404">
<img src="../images/alert-404.png" width="60px" style="float:left;"/>
<p>Lo sentimos ...</p>
<p>La página que está buscando no se puede encontrar</p>
<p>Con frecuencia  es debido  a algún error al escribir la dirección  de la página (URL). Compruébela  de nuevo  para ver si es correcta.</p>
</div>

<div class="alert-404">
<img src="../images/alert-404-ok.png" width="60px" style="float:left;"/>
<p><b>Usted puede:</b></p>
<p>Volver a la página anterior</p>
<p>Ir a la página de inicio de SIENLIT</p>
</div>

<div class="bg-404">
<img src="../images/error-404pg.png"/>
</div>


</div>
</div>
