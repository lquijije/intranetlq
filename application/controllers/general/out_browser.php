<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class out_browser extends CI_Controller {	
    
    function __construct()
    {		
        parent::__construct();
        
    }	

    public function _remap () {
        
        $this->_principal();
        
    }
    
    private function _principal(){
           
        $this->load->view('error/error_out_browser');  
        
    }
    
    
}