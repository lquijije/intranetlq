<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class principal extends CI_Controller {

    function __construct() {
        parent::__construct();
//        $this->load->model(array('proceso/evaluaciones/m_1191'));
        session_start();
    }

    public function _remap() {
        $tipo = $this->uri->segment(3);
        if ($tipo == 'retPorcentajes') {
            $this->_retPorcentajes();
        } else if ($tipo == 'retVentas') {
            $this->_retVentas();
        } else {
            $this->_principal();
        }
    }

    private function _principal() {

        clear_cache_php();

        if (isset($_SESSION['usuario'])) {

            $data['css'] = array('css/daterangepicker/daterangepicker-bs3.css',
                'css/datetimepicker/jquery.datetimepicker.css',
                'css/weather-icons.css',
                'css/weather-icons-wind.css');

            $data['js'] = array('js/jquery.min.js',
                'js/bootstrap.min.js',
                'js/general.js',
                'js/plugins/daterangepicker/daterangepicker.js',
                'js/plugins/datetimepicker/jquery.datetimepicker.js');

            $banderaEvaluaciones = false;

            $data['idFormulario'] = "f_principal";
            //$evalua = $this->m_1191->retEvaluAsignadas();
//            if(!empty($evalua)){
//                $banderaEvaluaciones = true;
//            }

            $data['bandEvalua'] = $banderaEvaluaciones;
            $data['noticias'] = $this->general_model->getNoticias();
            $data['widget'] = $this->general_model->getAccesoMenuWidget($_SESSION['c_e']);

            $this->load->view('barras/header', $data);
            $this->load->view('barras/menu');
            $this->load->view('principal/principal');
            $this->load->view('barras/footer');
        } else {
            redirect(site_url());
        }
    }

    private function _retPorcentajes() {

        //log_message('error','Inicio de la funcion:retPorcentajes'.date('d/m h:i:s').' ');
        $fec_ini = $this->input->post('fec_ini');
        $fec_fin = $this->input->post('fec_fin');
        $result = $this->general_model->retPoctDoc($fec_ini, $fec_fin);

        $arr = array();
        if (!empty($result)) {
            $arrError = isset($result['ERROR']) ? $result['ERROR'] : null;
            foreach ($result['OK'] as $row) {
                $total = $row['totales'];
                $aprobados = 100;
                $pendientes = (int) $row['pendientes'] * 100 / (int) $total;
                //$pendientes = round($pendientes);

                $errores = (int) $row['errores'] * 100 / (int) $total;
                //$errores = round($errores);

                if (number_format($pendientes, 2, '.', ',') > '0.00' && number_format($pendientes, 2, '.', ',') < '0.99') {
                    $pendientes = 1;
                } else {
                    $pendientes = round($pendientes);
                }

                if (number_format($errores, 2, '.', ',') > '0.00' && number_format($errores, 2, '.', ',') < '0.99') {
                    $errores = 1;
                } else {
                    $errores = round($errores);
                }

                if ($pendientes > 0) {
                    $aprobados = $aprobados - $pendientes;
                }
                if ($errores > 0) {
                    $aprobados = $aprobados - $errores;
                }

//                    $aprobados = $aprobados - $pendientes;
//                    $aprobados = $aprobados - $pendientes;
//                    $pendientes = $pendientes  - $errores;
                $arr [] = array('tipo' => $row['tipo'],
                    'totales' => $row['totales'],
                    'aprobados' => (int) $aprobados,
                    'pendientes' => (int) $pendientes,
                    'faltantes' => (int) $errores,
                    'error' => $arrError);
            }

            echo json_encode($arr);
            unset($row, $result);
        } else {
            echo json_encode(array(array('sms' => 'No existen Datos en la busqueda')));
        }
    }

    function _retVentas() {

        //log_message('error','Inicio de la funcion:retVentas'.date('d/m h:i:s').' ');
        $fec_ini = $this->input->post('f_i');
        $abc = explode('|', $fec_ini);
        $result = $this->general_model->retVentas($abc[0], $abc[1]);
        echo $result;
        unset($result);
        //log_message('error','Fin de la funcion:retVentas'.date('d/m h:i:s').' ');
    }

}
