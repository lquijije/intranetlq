<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class autocomplete extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model('autocomplete_model');
        session_start();
    }	

    public function _remap($method){      
        $method=$this->uri->segment(3);
        if($method=='retMenuAuto') {
            $this->_retMenuAuto();
        } 
    }
    
    private function _retMenuAuto(){
        $mensaje = $this->autocomplete_model->retMenuAuto();
        echo($mensaje);
    }

    
}
