<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_general_compras extends CI_Controller {	
	
    function __construct()
    {		
            parent::__construct();	
            $this->load->model('compras_model');
            session_start();
    }	

    public function _remap($method)
    {      
        $method=$this->uri->segment(3);
        if($method=='retDocCompra') {
            $this->_retDocCompra();
        } else if ($method=='updateDocCompra'){
            $this->_updateDocCompra();
        }  
    }
        
    private function _retDocCompra(){ 
        
        $id = $this->input->post('id');
        $tipo = $this->input->post('tp');
        $fec_ini = $this->input->post('f_i');
        $fec_fin = $this->input->post('f_f');
        
        $result = $this->compras_model->retDocCompras($id,$tipo,$fec_ini,$fec_fin);
        
        if (!empty($result)){
            $arreglo = array();
            foreach ($result as $row){  
                $arreglo[] = array("co_solicitud" => $row['co_solicitud'],
                                   "fe_ingreso" => $row['fe_ingreso'],
                                   "co_usuario" => $row['co_usuario'],
                                   "co_responsable" => $row['co_responsable'],
                                   "tx_dispositivo" => $row['tx_dispositivo'],
                                   "tx_destino" => $row['tx_destino'],
                                   "va_cantidad" => $row['va_cantidad'],
                                   "tx_proveedor" => (!empty($row['tx_proveedor']))? $row['tx_proveedor'] : '',
                                   "va_precio" => (!empty($row['va_precio']))? number_format($row['va_precio'],2,'.',',') : '',
                                   "bd_status" => $row['bd_status'],
                                   "nom_estado" => $row['nom_estado']); 
            }
            
            echo json_encode($arreglo);
            unset($row,$result);
        }
                  
    }
    
    private function _updateDocCompra(){

        $tipo = $this->input->post('tp');
        $tipo_change = $this->input->post('tc');
        $aprobar = $this->input->post('d_r');
       
        $doc = array('codigo' => $aprobar,'tipo' => $tipo,'tipo_change' => $tipo_change);
        $result = $this->compras_model->updateDocumentosCompras($doc);
        echo $result;
        
    }
    
        
}
