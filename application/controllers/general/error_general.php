<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class error_general extends CI_Controller {	
    
    function __construct()
    {		
        parent::__construct();
        session_start();
    }	

    public function _remap () {
        $tipo=$this->uri->segment(3);
        if($tipo=='valSess'){
            $this->_valSession();
        } else {
            $this->_principal();
        }
        
    }
    
    private function _principal(){
        
        $data['js'] = array('js/jquery.min.js',
                            'js/bootstrap.min.js',
                            'js/general.js');
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('error/error_general');  
        $this->load->view('barras/footer'); 
        
    }
    
    public function _valSession(){
        $msm = 0;
        if(!isset($_SESSION) || !empty($_SESSION) ){
            $msm = 1;
        }
        echo $msm;
    }
    
    
}