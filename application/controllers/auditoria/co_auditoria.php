<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_auditoria extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('mailer'));
        $this->load->model(array('auditoria/_auditoria'));
    }

    public function _enviarInforme($bodega, $listado, $tipo_informe, $email, $email_contenido) {

        ini_set('MAX_EXECUTION_TIME', -1);
        ini_set('memory_limit', '-1');
        $array = array('co_err' => 1, 'tx_err' => '');

        $email = explode(',', $email);
        $files_array = null;
        $email_array = null;
        $arrEm = null;

        if ($tipo_informe > 0) {

            $mensaje = 'Estimado(a) ';

            foreach ($email as $val) {
                $n_email = explode('@', $val);
                $arrEm[] = array(
                    'name' => $n_email[0],
                    'mail' => $val
                );
            }

            switch ($tipo_informe) {
                case 1:
                    $email_array = $arrEm;
                    $files_array = array(array(
                            'name' => 'TOMA-' . $bodega . '-' . $listado . '-' . date('m') . '.xls',
                            'fileString' => $this->_generaExcelAdministrador($bodega, $listado)
                    ));
                    $mensaje .= 'administrador(a) se adjunta el archivo de la toma ' . $listado . ".<br>" . $email_contenido;
                    break;
                case 2:
                    $email_array = $arrEm;
                    $files_array = array(array(
                            'name' => 'TOMA-' . $bodega . '-' . $listado . '-' . date('m') . '.xls',
                            'fileString' => $this->_generaExcelAuditor($bodega, $listado)
                    ));
                    $mensaje .= $_SESSION['nombre'] . ' se adjunta el archivo de ' . $listado . ".<br>" . $email_contenido;
                    break;
            }
            $res_mail = templ_mail('Auditoria Interna', $mensaje, 'Auditoria interna', 'Extranet', $email_array, $files_array);
            $array['co_err'] = $res_mail['co_err'];
            $array['tx_err'] = $res_mail['tx_err'];
        } else {
            $array['tx_err'] = 'PARAMETROS INCOMPLETOS';
        }

        echo json_encode($array);
    }

    private function _generaExcelAdministrador($bodega, $listado) {

//        error_reporting(E_ALL);
//        ini_set('display_errors', 1);

        $excel = '';
        $usu = $this->_auditoria->_retLiderToma($bodega, $listado);
        $co_usuario = empty($usu['err']) ? $usu['co_auditor'] : 0;
        $data = $this->_auditoria->_retDetalleListadoLiderEXTRANET($bodega, $listado, $co_usuario);

        $datos = $data['art'];

        if (!empty($datos)) {

            $TOTAL_POBLACION_PVP = 0;
            $TOTAL_POBLACION_COSTO = 0;
            $TOTAL_DIFERENCIA_PVP = 0;
            $TOTAL_DIFERENCIA_COSTO = 0;
            $TOTAL_ITEMS = 0;
            $TOTAL_UNIDADES = 0;

            $celdas = array(
                '0' => 'A', '1' => 'B', '2' => 'C', '3' => 'D', '4' => 'E', '5' => 'F', '6' => 'G', '7' => 'H', '8' => 'I', '9' => 'J', '10' => 'K', '11' => 'L', '12' => 'M', '13' => 'N', '14' => 'O', '15' => 'P', '16' => 'Q', '17' => 'R', '18' => 'S', '19' => 'T', '20' => 'U', '21' => 'V', '22' => 'W', '23' => 'X', '24' => 'Y', '25' => 'Z'
            );

            $claves = array_keys($datos[0]);
            $ClavesPermitidas = null;

            foreach ($claves as $row => $keys) {
                if (strpos($keys, 'ubica_') !== FALSE) {
                    $ClavesPermitidas[] = $keys;
                }
            }

            $this->load->library('PHPExcel');

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet();

            $objPHPExcel->
                    getProperties()
                    ->setCreator("AUDITORIA INTERNA")
                    ->setTitle("AUDITORIA INTERNA")
                    ->setSubject("AUDITORIA INTERNA")
                    ->setDescription("AUDITORIA INTERNA")
                    ->setKeywords("AUDITORIA INTERNA")
                    ->setCategory("AUDITORIA INTERNA");

            $borders1 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000'),
                    )
                ),
            );

            $borders = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000'),
                    )
                ),
            );

            $LetraINICIO = 3;
            $Letra = $celdas[($LetraINICIO + count($ClavesPermitidas) + 10)];
            $LetraINI = $LetraINICIO;
            $linea = 1;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('AUDITORIA INTERNA', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($data['inf']['no_alm'] . " - " . $data['inf']['ds_tip'], PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($data['inf']['ds_tom'], PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':D' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('#PAPELETA: ' . $bodega . '-' . $listado . '-' . date('m'), PHPExcel_Cell_DataType::TYPE_STRING);

            $linea = $linea + 2;
            $objPHPExcel->getActiveSheet()->mergeCells('E' . $linea . ':G' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea . ':G' . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea . ':G' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit('FISICO', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;

            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'CODIGO');
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $linea, 'FABRICA');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 'DESCRIPCION');
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $linea, 'ANT');

            foreach ($ClavesPermitidas as $raw) {
                $LetraINI++;
                $titulo = explode('|', $raw);
                $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, $titulo[1]);
            }

            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT FIS');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'SALDO SIST');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT DIF');
//            $LetraINI++;
//            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'BV');
            $LetraINI++;
//            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT POB COSTO');
//            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT POB PVP');
//            $LetraINI++;
//            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'COSTO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'PVP');
//            $LetraINI++;
//            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOTAL COSTO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOTAL  PVP');
            $LetraINI = $LetraINI + 2;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'SALDO SISTEMA HISTORICO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'BODEGA VIRTUAL HISTORICO');

            $lineaIniciaDatos = $linea + 1;

            foreach ($datos as $row) {
                $linea++;
                $LetraINI = $LetraINICIO;
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders);
                $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['co_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['co_fab'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['ds_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('D' . $linea)->setValueExplicit($row['va_ant'], PHPExcel_Cell_DataType::TYPE_NUMERIC);

                foreach ($ClavesPermitidas as $raw) {
                    $LetraINI++;
                    $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row[$raw], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                }
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_con'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_sis'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_dif'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//                $LetraINI++;
//                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_sal_vir'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//                $LetraINI++;
//                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//                $LetraINI++;
//                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//                $LetraINI++;
//                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_dif_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_dif_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI = $LetraINI + 2;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_sis_hist'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_vir_hist'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $TOTAL_POBLACION_PVP += $row['va_tot_pvp'];
                $TOTAL_POBLACION_COSTO += $row['va_tot_cos'];
                $TOTAL_DIFERENCIA_PVP += $row['va_tot_dif_pvp'];
                $TOTAL_DIFERENCIA_COSTO += $row['va_tot_dif_cos'];
            }

            $lineaFinDatos = $linea;
            $sumTotPVP = "=SUM(" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 6)] . $lineaIniciaDatos . ":" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 6)] . $lineaFinDatos . ")";
            //$sumTotCOS = "=SUM(" . $celdas[$LetraINI - 1] . $lineaIniciaDatos . ":" . $celdas[$LetraINI - 1] . $lineaFinDatos . ")";
            $sumTotDifPVP = "=SUM(" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $lineaIniciaDatos . ":" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $lineaFinDatos . ")";
            //$sumTotDifCOS = "=SUM(" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $lineaIniciaDatos . ":" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $lineaFinDatos . ")";
            $TOTAL_ITEMS = '=COUNTIF(' . $celdas[(count($ClavesPermitidas) + 4)] . $lineaIniciaDatos . ':' . $celdas[(count($ClavesPermitidas) + 4)] . $lineaFinDatos . ',"<>0")';
            $TOTAL_UNIDADES = '=SUM(' . $celdas[(count($ClavesPermitidas) + 4)] . $lineaIniciaDatos . ':' . $celdas[(count($ClavesPermitidas) + 4)] . $lineaFinDatos . ')';

            $linea = $linea + 2;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);

            $vaTotDifCOS = $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $linea;
            $vaTotDifPVP = $celdas[($LetraINICIO + count($ClavesPermitidas) + 6)] . $linea;
            $vaTotCOS = $celdas[$LetraINI - 1] . $linea;
            $vaTotPVP = $celdas[$LetraINI] . $linea;

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA');
            $objPHPExcel->getActiveSheet()->SetCellValue($celdas[3 + (count($ClavesPermitidas) + 5)] . $linea, $sumTotDifPVP);
            $objPHPExcel->getActiveSheet()->SetCellValue($celdas[3 + (count($ClavesPermitidas) + 6)] . $linea, $sumTotPVP);

            $linea = $linea + 2;


//            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':C' . $linea);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
//            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'RESUMEN INVENTARIO');
//            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 0);
//            $linea++;
//            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
//            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION PVP');
//            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, "=+" . $vaTotDifPVP);
//            $linea++;
//            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
//            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A PVP');
//            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, "=+" . $vaTotPVP);
//            $linea++;
//            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
//            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL ITEMS');
//            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $COUNTIFTotItems);
//            $linea++;
//            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
//            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
//            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL UNIDADES NETO');
//            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $SUMTotItems);


            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':C' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'RESUMEN INVENTARIO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 0);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_POBLACION_PVP);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION COSTO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_POBLACION_COSTO);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_DIFERENCIA_PVP);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A COSTO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_DIFERENCIA_COSTO);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL ITEMS');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_ITEMS);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL UNIDADES NETO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_UNIDADES);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('LISTA');

            @ob_start();
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $writer->save("php://output");
            $excel = @ob_get_contents();
            @ob_end_clean();
        } else {
            
        }
        return $excel;
    }

    private function _generaExcelAuditor($bodega, $listado) {

//        error_reporting(E_ALL);
//        ini_set('display_errors', 1);

        $excel = '';

        $usu = $this->_auditoria->_retLiderToma($bodega, $listado);
        $co_usuario = empty($usu['err']) ? $usu['co_auditor'] : 0;
        $data = $this->_auditoria->_retDetalleListadoLiderEXTRANET($bodega, $listado, $co_usuario);

        $datos = $data['art'];

        if (!empty($datos)) {

            $celdas = array(
                '0' => 'A', '1' => 'B', '2' => 'C', '3' => 'D', '4' => 'E', '5' => 'F', '6' => 'G', '7' => 'H', '8' => 'I', '9' => 'J', '10' => 'K', '11' => 'L', '12' => 'M', '13' => 'N', '14' => 'O', '15' => 'P', '16' => 'Q', '17' => 'R', '18' => 'S', '19' => 'T', '20' => 'U', '21' => 'V', '22' => 'W', '23' => 'X', '24' => 'Y', '25' => 'Z'
            );

            $claves = array_keys($datos[0]);
            $ClavesPermitidas = null;

            foreach ($claves as $row => $keys) {
                if (strpos($keys, 'ubica_') !== FALSE) {
                    $ClavesPermitidas[] = $keys;
                }
            }

            $this->load->library('PHPExcel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            //////////////////////////////////////////////////////////////////////////////
            ////// BEGIN SHEET 1 = 0
            //////////////////////////////////////////////////////////////////////////////

            $objPHPExcel->
                    getProperties()
                    ->setCreator("AUDITORIA INTERNA")
                    ->setTitle("AUDITORIA INTERNA")
                    ->setSubject("AUDITORIA INTERNA")
                    ->setDescription("AUDITORIA INTERNA")
                    ->setKeywords("AUDITORIA INTERNA")
                    ->setCategory("AUDITORIA INTERNA");


            $borders1 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000'),
                    )
                ),
            );

            $borders = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000'),
                    )
                ),
            );

            $TOTAL_POBLACION_PVP = 0;
            $TOTAL_POBLACION_COSTO = 0;
            $TOTAL_DIFERENCIA_PVP = 0;
            $TOTAL_DIFERENCIA_COSTO = 0;
            $vaTotCOS = 0;

            $LetraINICIO = 2;
            $LetraINI = $LetraINICIO;
            $Letra = $celdas[($LetraINI + count($ClavesPermitidas) + 11)];

            $linea = 1;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('AUDITORIA INTERNA', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($data['inf']['no_alm'] . " - " . $data['inf']['ds_tip'], PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($data['inf']['ds_tom'], PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':D' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('#PAPELETA: ' . $bodega . '-' . $listado . '-' . date('m'), PHPExcel_Cell_DataType::TYPE_STRING);

            $linea = $linea + 2;
            $objPHPExcel->getActiveSheet()->mergeCells('E' . $linea . ':G' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea . ':G' . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea . ':G' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit('FISICO', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'CODIGO');
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $linea, 'FABRICA');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 'DESCRIPCION');

            foreach ($ClavesPermitidas as $raw) {
                $LetraINI++;
                $titulo = explode('|', $raw);
                $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, $titulo[1]);
            }

            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT FIS');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'SALDO SIST');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT DIF');

            if ($data['inf']['co_tip'] == 2) {
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT DIF 2');
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'MOTIVO');
            }

            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT POB COSTO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT POB PVP');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'COSTO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'PVP');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOTAL COSTO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOTAL PVP');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'SALDO SISTEMA HISTORICO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'BODEGA VIRTUAL HISTORICO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'COMENTARIOS');

            $lineaIniciaDatos = $linea + 1;

            foreach ($datos as $row) {

                $linea++;
                $LetraINI = $LetraINICIO;
                //$objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders);
                $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['co_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['co_fab'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['ds_art'], PHPExcel_Cell_DataType::TYPE_STRING);

                foreach ($ClavesPermitidas as $raw) {
                    $LetraINI++;
                    $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row[$raw], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                }

                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_con'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_sis'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_dif'], PHPExcel_Cell_DataType::TYPE_NUMERIC);

                if ($data['inf']['co_tip'] == 2) {
                    $LetraINI++;
                    $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_dif2'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $LetraINI++;
                    $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['abr'], PHPExcel_Cell_DataType::TYPE_STRING);
                }

                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_dif_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getStyle($celdas[$LetraINI] . $linea)->getNumberFormat()->setFormatCode('_($ * #,##0.00_);_($ * (#,##0.00);_($ * "-"??_);_(@_)');
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_dif_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_sis_hist'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_vir_hist'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $comentarios = '';
                $observacion_arr = $row['obs'];
//                if ($_SERVER['REMOTE_ADDR'] == '130.1.39.49') {
//                    if ($row['co_art'] == 'A54304') {
                        if (!empty($observacion_arr)) {
                            $observacion_arr = json_decode('[' . $observacion_arr . ']', TRUE);
                            if (is_array($observacion_arr)) {
                                foreach ($observacion_arr as $ob) {
                                    $unObs = unserialize(str_replace("'", '"', $ob['obs']));
                                    $comentarios .= 'Usuario: ' . $ob['user'] . "\n";
                                    $comentarios .= 'Estado: ' . $ob['status'] . "\n";
                                    $comentarios .= 'Fecha: ' . $ob['date'] . "\n";
                                    $comentarios .= 'Comentarios: ';
                                    if (is_array($unObs) && !empty($unObs)) {
                                        foreach ($unObs as $uo) {
                                            $comentarios .= $uo . ",";
                                        }
                                    }
                                    $comentarios .= ',';
                                }
                            }
                        }
//                    }
//                }

                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($comentarios, PHPExcel_Cell_DataType::TYPE_STRING);
                $TOTAL_POBLACION_PVP += $row['va_tot_pvp'];
                $TOTAL_POBLACION_COSTO += $row['va_tot_cos'];
                $TOTAL_DIFERENCIA_PVP += $row['va_tot_dif_pvp'];
                $TOTAL_DIFERENCIA_COSTO += $row['va_tot_dif_cos'];
                $vaTotCOS += $row['va_tot_dif_cos'];
            }

            $lineaFinDatos = $linea;
            $sumTotDifCOS = "=SUM(" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 2)] . $lineaIniciaDatos . ":" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 2)] . $lineaFinDatos . ")";
            $sumTotDifPVP = "=SUM(" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 3)] . $lineaIniciaDatos . ":" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 3)] . $lineaFinDatos . ")";
            $sumTotCOS = "=SUM(" . $celdas[$LetraINI - 1] . $lineaIniciaDatos . ":" . $celdas[$LetraINI - 1] . $lineaFinDatos . ")";
            $sumTotPVP = "=SUM(" . $celdas[$LetraINI] . $lineaIniciaDatos . ":" . $celdas[$LetraINI] . $lineaFinDatos . ")";
            $COUNTIFTotItems = '=COUNTIF(' . $celdas[($LetraINICIO + count($ClavesPermitidas) + 1)] . $lineaIniciaDatos . ':' . $celdas[($LetraINICIO + count($ClavesPermitidas) + 1)] . $lineaFinDatos . ',"<>0")';
            $SUMTotItems = '=SUM(' . $celdas[($LetraINICIO + count($ClavesPermitidas) + 1)] . $lineaIniciaDatos . ':' . $celdas[($LetraINICIO + count($ClavesPermitidas) + 1)] . $lineaFinDatos . ')';

            $linea = $linea + 2;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);

            $vaTotDifCOS = $celdas[($LetraINICIO + count($ClavesPermitidas) + 4)] . $linea;
            $vaTotDifPVP = $celdas[($LetraINICIO + count($ClavesPermitidas) + 4)] . $linea;
            //$vaTotCOS = $celdas[$LetraINI - 1] . $linea;
            $vaTotPVP = $celdas[$LetraINI] . $linea;

//            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA');
//            $objPHPExcel->getActiveSheet()->SetCellValue($celdas[$LetraINICIO + (count($ClavesPermitidas) + 2)] . $linea, $sumTotDifCOS);
//            $objPHPExcel->getActiveSheet()->SetCellValue($celdas[$LetraINICIO + (count($ClavesPermitidas) + 3)] . $linea, $sumTotDifPVP);
//            if ($data['inf']['co_tip'] == 2) {
//                
//            }
//            $objPHPExcel->getActiveSheet()->SetCellValue($celdas[$LetraINICIO + (count($ClavesPermitidas) + 4)] . $linea, $sumTotDif2);

            $linea = $linea + 2;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':C' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'RESUMEN INVENTARIO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 0);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_POBLACION_PVP);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION COSTO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_POBLACION_COSTO);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, "=+" . $TOTAL_DIFERENCIA_PVP);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A COSTO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, "=+" . $TOTAL_DIFERENCIA_COSTO);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL ITEMS');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $COUNTIFTotItems);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL UNIDADES NETO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $SUMTotItems);

            //////////////////////////////////////////////////////////////////////////////////////////////////
            ///AVERIADOS QUE GENERAN SOBRANTE
            //////////////////////////////////////////////////////////////////////////////////////////////////

            $ave_tot_pvp = 0;
            $ave_tot_cos = 0;
            $ave_tot_items = 0;
            $ave_tot_neto = 0;

            $linea = $linea + 3;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':H' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':H' . $linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':H' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('AVERIADOS QUE GENERAN SOBRANTE', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea = $linea + 2;

            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':H' . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':H' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':H' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'CODIGO');
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $linea, 'FABRICA');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 'DESCRIPCION');
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $linea, 'TOT FIS');
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $linea, 'SALDO SIST');
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $linea, 'TOT DIF');
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $linea, 'PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('H' . $linea, 'TOTAL  PVP');

            if (!empty($data['sob'])) {
                foreach ($data['sob'] as $row) {
                    $linea++;
                    //$objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':H' . $linea)->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['co_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['co_fab'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['ds_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('D' . $linea)->setValueExplicit($row['va_can'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit($row['va_can'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->getCell('F' . $linea)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->getCell('G' . $linea)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->getCell('H' . $linea)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $ave_tot_pvp += $row['va_tot_pvp'];
                    $ave_tot_cos += $row['va_tot_cos'];
                    $ave_tot_items++;
                    $ave_tot_neto += $row['va_can'];
                }
            }

            $linea = $linea + 2;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':C' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'RESUMEN INVENTARIO');
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $ave_tot_pvp);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION COSTO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $ave_tot_cos);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL ITEMS');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $ave_tot_items);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL UNIDADES NETO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $ave_tot_neto);

            /////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////
//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('LISTA');

            //////////////////////////////////////////////////////////////////////////////
            ////// END SHEET 1 = 0
            //////////////////////////////////////////////////////////////////////////////

            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex(1);

            $LetraINICIO = 2;
            $LetraINI = $LetraINICIO;
            $linea = 1;
            $Letra = $celdas[5];

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('AUDITORIA INTERNA', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($listado, PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('INVENTARIO MERCADERIA AVERIADO ', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':D' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('#PAPELETA: ' . $bodega . '-' . $listado . '-' . date('m'), PHPExcel_Cell_DataType::TYPE_STRING);

            $linea = $linea + 2;

            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'CODIGO');
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $linea, 'FABRICA');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 'DESCRIPCION');
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $linea, 'CANT');
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $linea, 'PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $linea, 'TOTAL PVP');

            $total_dif_pvp = 0;
            $total_dif_cos = 0;
            $count_ave = 0;
            $sum_ave = 0;

            if (!empty($datos)) {
                foreach ($datos as $row) {
                    if ((isset($row['ubica_3|AVERIADO']) ? (int) $row['ubica_3|AVERIADO'] : 0) > 0) {
                        $linea++;
                        $count_ave++;
                        $total_pvp = (int) $row['ubica_3|AVERIADO'] * (float) $row['va_pvp'];
                        $total_cos = (int) $row['ubica_3|AVERIADO'] * (float) $row['va_cos'];
                        $total_dif_pvp += $total_pvp;
                        $total_dif_cos += $total_cos;
                        $sum_ave += (int) $row['ubica_3|AVERIADO'];
                        //$objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders);
                        $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['co_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['co_fab'], PHPExcel_Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['ds_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->getCell('D' . $linea)->setValueExplicit((int) $row['ubica_3|AVERIADO'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit($row['va_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->getCell('F' . $linea)->setValueExplicit($total_pvp, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    }
                }
            }

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL AVERIADOS');
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $linea, $total_dif_pvp);

            $linea = $linea + 3;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':C' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'RESUMEN INVENTARIO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 0);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $total_dif_pvp);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A COSTO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $total_dif_cos);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL ITEMS');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $count_ave);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL UNIDADES NETO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $sum_ave);

//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('AVERIADOS');

            //////////////////////////////////////////////////////////////////////////////
            ////// END SHEET 1 = 0 ///////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////

            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex(2);

            $LetraINICIO = 2;
            $LetraINI = $LetraINICIO;
            $linea = 1;
            $Letra = $celdas[9];

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('AUDITORIA INTERNA', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($listado, PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('F4 ', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':D' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('#PAPELETA: ' . $bodega . '-' . $listado . '-' . date('m'), PHPExcel_Cell_DataType::TYPE_STRING);

            $linea = $linea + 2;

            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'CODIGO');
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $linea, 'DESCRIPCION');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, '#RETIRO');
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $linea, 'CI');
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $linea, 'CLIENTE');
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $linea, 'MOTIVO');
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $linea, '#DOCUMENTO');
            $objPHPExcel->getActiveSheet()->setCellValue('H' . $linea, 'CANTIDAD');
            $objPHPExcel->getActiveSheet()->setCellValue('I' . $linea, 'FECHA REGISTRO');
            $objPHPExcel->getActiveSheet()->setCellValue('J' . $linea, 'FECHA COMPRA');

            if (!empty($data['f4'])) {

                foreach ($data['f4'] as $row) {
                    $linea++;
                    //$objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['co_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['ds_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['co_ret'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('D' . $linea)->setValueExplicit($row['co_ced'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit($row['no_cli'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('F' . $linea)->setValueExplicit($row['ds_mot'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('G' . $linea)->setValueExplicit($row['de_sri'] . $row['co_caj'] . $row['nu_doc'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('H' . $linea)->setValueExplicit((int) $row['va_can'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->getCell('I' . $linea)->setValueExplicit($row['fe_reg'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('J' . $linea)->setValueExplicit($row['fe_com'], PHPExcel_Cell_DataType::TYPE_STRING);
                }
            }

//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('F4');

            //////////////////////////////////////////////////////////////////////////////
            ////// END SHEET 1 = 0
            //////////////////////////////////////////////////////////////////////////////

            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex(3);

            $LetraINICIO = 2;
            $LetraINI = $LetraINICIO;
            $linea = 1;
            $Letra = $celdas[7];

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('AUDITORIA INTERNA', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($listado, PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('TRANSITO', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':D' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('#PAPELETA: ' . $bodega . '-' . $listado . '-' . date('m'), PHPExcel_Cell_DataType::TYPE_STRING);

            $linea = $linea + 2;

            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'CODIGO');
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $linea, 'DESCRIPCION');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, '#PEDIDO');
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $linea, 'DESPACHA');
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $linea, 'DESTINO');
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $linea, 'FECHA DESPACHO');
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $linea, 'CANT. PEDIDA');
            $objPHPExcel->getActiveSheet()->setCellValue('H' . $linea, 'CANT. DESPACHADA');

            if (!empty($data['tra'])) {

                foreach ($data['tra'] as $row) {
                    $linea++;
                    //$objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['co_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['ds_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['co_ped'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('D' . $linea)->setValueExplicit($row['tx_sal'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit($row['tx_des'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('F' . $linea)->setValueExplicit($row['fe_des'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('G' . $linea)->setValueExplicit($row['ca_ped'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->getCell('H' . $linea)->setValueExplicit($row['ca_des'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                }
            }

//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('TRANSITO');

            @ob_start();
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $writer->save("php://output");
            $excel = @ob_get_contents();
            @ob_end_clean();
        } else {
            
        }
        return $excel;
    }

}
