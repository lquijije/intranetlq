<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_5002 extends CI_Controller {

    public $procesar = array(
        'R' => array(
            'name' => 'REVISION'
        ),
        'C' => array(
            'name' => 'COBRAR'
        ),
        'A' => array(
            'name' => 'AJUSTAR'
        ),
        'N' => array(
            'name' => 'NO AJUSTAR'
        )
    );

    function __construct() {
        parent::__construct();
        $this->load->model(array('auditoria/_auditoria'));
        session_start();
    }

    public function _remap() {

        validaSession('auditoria/co_5002');
        $tipo = $this->uri->segment(3);

        $datos = $this->input->post();
        fnc_post($tipo, $datos, 'auditoria/co_5002');

        switch ($tipo) {
            case 'reToma':
                $this->_getToma();
                break;
            case 'savProductos':
                $this->_grabarArticulos();
                break;
            case 'savProductosAveriados':
                $this->_grabarArticulosAveriados();
                break;
            case 'proProductos':
                $this->_procesarArticulos();
                break;
            case 'snInforme':
                $this->_enviaInforme();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {

        $co_usuario = $_SESSION['c_e'];
        $_SESSION['f_dis'] = true;

        $data = null;
        $urlControlador = 'auditoria/co_5002';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css'
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/general.js',
            'js/config.js',
            'js/auditoria/j_5002.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js'
        );

        $result = $this->_auditoria->retTomasPendientes($co_usuario);
        $data['retPendientes'] = $result;
        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('auditoria/v_5002');
        $this->load->view('barras/footer');
    }

    private function _enviaInforme() {
        $ca = $this->load->library('../controllers/auditoria/co_auditoria');
        $ca = new co_auditoria();
        $bodega = $this->input->post('c_b');
        $listado = $this->input->post('c_l');
        $tipo_informe = $this->input->post('t_i');
        $email = $this->input->post('e_i');
        $email_contenido = $this->input->post('c_m');
        $data = $ca->_enviarInforme($bodega, $listado, $tipo_informe, $email, $email_contenido);
        print_r($data);
    }

    private function _procesarArticulos() {
        $cod_almacen = $this->input->post('c_b');
        $cod_listado = $this->input->post('c_l');
        $co_estado = "A";
        $result = $this->_auditoria->_actualizaToma($cod_almacen, $cod_listado, $co_estado);
        if ($result['co_err'] == 0) {
            $result['tx_err'] = 'Toma procesada correctamente.';
            if ($result['ca_pen'] > 0) {
                $result['tx_err'] = 'Se encuentran ' . $result['ca_pen'] . ' art&iacute;culos pendientes de procesar.';
            }
        }
        echo json_encode($result);
    }

    private function _grabarArticulos() {
        $data = $this->input->post('s_s');
        $cod_almacen = $this->input->post('c_b');
        $cod_listado = $this->input->post('c_l');
        $co_ususario = (int) $_SESSION['c_e'];
        $result = $this->_auditoria->registrarProductosGerente($data, $cod_almacen, $cod_listado, $co_ususario, 0);
        $error = 0;
        $menOK = $menER = $resultado = '';
        if ((int) $result['co_err'] == 0) {
            if (!empty($result['data'])) {
                for ($i = 0; $i < count($result['data']); $i++) {
                    if ((int) $result['data'][$i]['co_err'] == 0) {
                        $menOK .= $result['data'][$i]['co_art'] . "<br>";
                    } else {
                        $menER .= $result['data'][$i]['co_art'] . " - ERROR #" . trim($result['data'][$i]['co_err']) . ".<br>";
                    }
                }
                if (!empty($menOK)) {
                    $resultado .= 'Los siguientes productos han sido AJUSTADOS con exito.<br>' . $menOK;
                }
                if (!empty($menER)) {
                    $resultado .= '<span style="color: rgb(255, 0, 0);">Los siguientes productos han presentado ERROR al AJUSTAR.<br>' . $menER . '</span>';
                }
            } else {
                $resultado = 'Registros grabados correctamente.';
            }
        } else {
            $error = (int) $result['co_err'];
            $resultado = $result['tx_err'];
        }

        echo json_encode(array(
            'co_err' => $error,
            'tx_err' => $resultado
        ));
    }

    private function _grabarArticulosAveriados() {
        $data = $this->input->post('s_s');
        $cod_almacen = $this->input->post('c_b');
        $cod_listado = $this->input->post('c_l');
        $co_ususario = (int) $_SESSION['c_e'];
        $result = $this->_auditoria->registrarProductosGerente($data, $cod_almacen, $cod_listado, $co_ususario, 1);
        $error = 0;
        $menOK = $menER = $resultado = '';
        if ((int) $result['co_err'] == 0) {
            if (!empty($result['data'])) {
                for ($i = 0; $i < count($result['data']); $i++) {
                    if ((int) $result['data'][$i]['co_err'] == 0) {
                        $menOK .= $result['data'][$i]['co_art'] . "<br>";
                    } else {
                        $menER .= $result['data'][$i]['co_art'] . " - ERROR #" . trim($result['data'][$i]['co_err']) . ".<br>";
                    }
                }
                if (!empty($menOK)) {
                    $resultado .= 'Los siguientes productos han sido AJUSTADOS con exito.<br>' . $menOK;
                }
                if (!empty($menER)) {
                    $resultado .= '<span style="color: rgb(255, 0, 0);">Los siguientes productos han presentado ERROR al AJUSTAR.<br>' . $menER . '</span>';
                }
            } else {
                $resultado = 'Registros grabados correctamente.';
            }
        } else {
            $error = (int) $result['co_err'];
            $resultado = $result['tx_err'];
        }

        echo json_encode(array(
            'co_err' => $error,
            'tx_err' => $resultado
        ));
    }

    function _getToma() {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $articulos_contados = $this->input->post('c_a');
        $articulos_contados = (int) $articulos_contados;
        $co_bodega = $this->input->post('c_b');
        $co_listado = $this->input->post('c_l');
        $data = $this->_auditoria->_retDetalleListadoLider($co_bodega, $co_listado, $_SESSION['c_e'], 2);
        $data['inf']['btn_fil'] = $articulos_contados;
        if ($articulos_contados == 1) {
            if (empty($data['err']) && !empty($data['art'])) {
                $data_art = null;
                foreach ($data['art'] as $row) {
                    if ((int) $row['va_con'] > 0) {
                        $data_art[] = $row;
                    }
                }
                $data['art'] = $data_art;
            }
        }
        $array = $this->creaTablaArticulos($data, $co_bodega, $co_listado);
        $array['tab_ave'] = $this->creaTablaAveriados($data, $co_bodega, $co_listado, 'V3');
        echo json_encode($array);
    }

    private function creaTablaArticulos($data, $co_bodega, $co_listado) {

        $array = array('inf' => $data['inf'], 'art' => null, 'btn' => '', 'err' => $data['err']);

        $va_tot_pvp = 0;
        $va_tot_cos = 0;
        $va_tot_dif_pvp = 0;
        $va_tot_dif_cos = 0;
        $ubica_2 = 0;
        $countif = 0;
        $key = 0;

        if (empty($data['err'])) {
            if (!empty($data['art'])) {
                $datatable_data = null;

                $datatable_columns = array(
                    array('title' => 'F4', 'data' => 'html_f4'),
                    array('title' => 'C&oacute;digo', 'data' => 'co_art'),
                    array('title' => 'Descripci&oacute;n', 'data' => 'ds_art'),
                    array('title' => 'Averiado', 'data' => 'va_ave', 'className' => "text-center"),
                    array('title' => 'FIS', 'data' => 'va_con', 'className' => "text-center con_art_"),
                );

                array_push($datatable_columns, array('title' => 'SIST', 'data' => 'va_sis', 'className' => "text-center con_sis_"));
                array_push($datatable_columns, array('title' => 'DIF', 'data' => 'va_dif', 'className' => "text-center con_dif_"));

                if ($data['inf']['co_tip'] == 2) {
                    array_push($datatable_columns, array('title' => 'DIF 2', 'data' => 'va_dif2', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => 'MOTIVO', 'data' => 'motivo', 'className' => "text-center"));
                }

                array_push($datatable_columns, array('title' => 'COSTO', 'data' => 'va_costo', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => 'PVP', 'data' => 'va_pvp', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => 'T.COSTO', 'data' => 'va_tcosto', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => 'T.PVP', 'data' => 'va_tpvp', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => 'BV', 'data' => 'va_sal_vir', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => '<input name="ck_apro" id="ck_ajus" type="radio"> Ajustar', 'data' => 'html_ajus', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => '<input name="ck_apro" id="ck_cobr" type="radio"> Cobrar', 'data' => 'html_cobr', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => '<input name="ck_apro" id="ck_revi" type="radio"> Revisi&oacute;n', 'data' => 'html_revi', 'className' => "text-center"));

                foreach ($data['art'] as $row) {
                    //echo $row['fl_rev_aud'] . "*";

                    if (trim($row['fl_rev_aud']) <> 'R' && trim($row['fl_rev_aud']) <> 'N') {
                        //echo $row['fl_rev_aud'] . "-";
                        $va_tot_pvp += $row['va_tot_pvp'];
                        $va_tot_cos += $row['va_tot_cos'];
                        $va_tot_dif_pvp += $row['va_tot_dif_pvp'];
                        $va_tot_dif_cos += $row['va_tot_dif_cos'];
                        $countif += (isset($row['va_con']) ? (int) $row['va_con'] : 0) > 0 ? 1 : 0;
                        $ubica_2 += isset($row['va_con']) ? (int) $row['va_con'] : 0;

                        $datatable_data[$key] = array(
                            'DT_RowId' => 'ca_' . $row['co_art'],
                            'html_f4' => $row['html_f4'] . " " . $row['html_transito'],
                            'co_art' => $row['_link'],
                            'ds_art' => $row['ds_art'],
                            'va_ave' => isset($row['ubica_3|AVERIADO']) ? number_format($row['ubica_3|AVERIADO'], 0, '.', ',') : 0,
                            'va_con' => number_format($row['va_con'], 0, '.', ','),
                            'va_costo' => number_format($row['va_cos'], 2, '.', ','),
                            'va_pvp' => number_format($row['va_pvp'], 2, '.', ','),
                            'va_tcosto' => number_format($row['va_tot_dif_cos'], 2, '.', ','),
                            'va_tpvp' => number_format($row['va_tot_dif_pvp'], 2, '.', ',')
                        );

                        $html_ajus = $html_cobr = $html_revi = '';

                        if (!empty($row['fl_ger'])) {
                            switch (trim($row['fl_ger'])) {
                                case 'C':
                                    $html_cobr = 'POR COBRAR';
                                    break;
                                case 'A':
                                    $html_ajus = 'AJUSTADO';
                                    break;
                                case 'R':
                                    $html_ajus = '<center><input name="raB_' . $key . '" id="option_ajus[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    $html_cobr = '<center><input name="raB_' . $key . '" id="option_naju[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    $html_revi = '<center><input checked="" name="raB_' . $key . '" id="option_revi[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    break;
                            }
                        } else {
                            $aud_ajus = !empty($row['fl_rev_aud']) && trim($row['fl_rev_aud']) == 'A' ? '<span class="label label-success" style="padding: 3px 6px; border-radius: 100px;"><i class="fa fa-check"></i> ' . $row['fl_rev_user'] . '</span>' : '';
                            $html_ajus = '<center><input name="raB_' . $key . '" id="option_ajus[]" value="' . $row['co_art'] . '" type="radio"> ' . $aud_ajus . '</center>';
                            $html_cobr = '<center><input name="raB_' . $key . '" id="option_cobr[]" value="' . $row['co_art'] . '" type="radio"></center>';
                            $html_revi = '<center><input name="raB_' . $key . '" id="option_revi[]" value="' . $row['co_art'] . '" type="radio"></center>';
                        }

                        $datatable_data[$key]['html_ajus'] = $html_ajus;
                        $datatable_data[$key]['html_cobr'] = $html_cobr;
                        $datatable_data[$key]['html_revi'] = $html_revi;
                        $datatable_data[$key]['va_sis'] = number_format($row['va_sis'], 0, '.', ',');
                        $datatable_data[$key]['va_dif'] = number_format($row['va_dif'], 0, '.', ',');
                        if ($data['inf']['co_tip'] == 2) {
                            $datatable_data[$key]['motivo'] = $row['abr'];
                            $datatable_data[$key]['va_dif2'] = number_format($row['va_dif2'], 0, '.', ',');
                        }
                        $datatable_data[$key]['va_sal_vir'] = number_format($row['va_sal_vir'], 0, '.', ',');
                        $key++;
                    }
                }

                $btns = '<button type="button" class="btn btn-danger" onclick="grabarArticulos(' . $co_bodega . ',' . $co_listado . ');"><i class="fa fa-save"></i> Grabar</button>';
                $btns .= '<button type="button" class="btn btn-success" onclick="procesarArticulos(' . $co_bodega . ',' . $co_listado . ');" style="margin-left: 10px;"><i class="fa fa-cogs"></i> Procesar</button>';
                $btns .= '<div class="defaultKTV"><div class="ktvTop"><span class="ktvText"><input id="btn_check_f_art" type="checkbox" ' . ($data['inf']['btn_fil'] == 1 ? 'checked' : '') . '><label for="btn_check_f_art">Art&iacute;culos Contados</label></span></div></div>';

                $array['inf']['va_tot_pvp'] = number_format($va_tot_pvp, 2, '.', ',');
                $array['inf']['va_tot_cos'] = number_format($va_tot_cos, 2, '.', ',');
                $array['inf']['va_tot_dif_pvp'] = number_format($va_tot_dif_pvp, 2, '.', ',');
                $array['inf']['va_tot_dif_cos'] = number_format($va_tot_dif_cos, 2, '.', ',');
                $array['inf']['countif'] = number_format($countif, 0, '.', ',');
                $array['inf']['ubica_2'] = number_format($ubica_2, 0, '.', ',');
                $array['btn'] = $btns;

                $array['art'] = array(
                    'data' => $datatable_data,
                    'columns' => $datatable_columns
                );
            }
        }

        return $array;
    }

    private function creaTablaAveriados($data, $co_bodega, $co_listado, $co_estado) {
        $array = null;
        $va_tot_pvp = 0;
        $va_tot_cos = 0;
        $ubica_2 = 0;
        $countif = 0;
        $boton = 0;

        if (empty($data['err'])) {
            if (!empty($data['art'])) {

                $datatable_data = null;

                $datatable_columns = array(
                    array('title' => 'C&oacute;digo', 'data' => 'co_art'),
                    array('title' => 'Fabrica', 'data' => 'co_fab'),
                    array('title' => 'Descripci&oacute;n', 'data' => 'ds_art'),
                    array('title' => 'CANT', 'data' => 'va_ave', 'className' => "text-center"),
                    array('title' => 'PVP', 'data' => 'va_pvp', 'className' => "text-center"),
                    array('title' => 'TOT PVP', 'data' => 'to_ave', 'className' => "text-center")
                );

                if (trim($co_estado) == 'V2' || trim($co_estado) == 'V3') {
                    array_push($datatable_columns, array('title' => '<input name="ck_apro_ave" id="ck_ajus_ave" type="radio"> Ajustar', 'data' => 'html_ajus_ave', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => '<input name="ck_apro_ave" id="ck_naju_ave" type="radio"> Cobrar', 'data' => 'html_cobr_ave', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => '<input name="ck_apro_ave" id="ck_revi_ave" type="radio"> Revisi&oacute;n', 'data' => 'html_revi_ave', 'className' => "text-center"));
                } else {
                    array_push($datatable_columns, array('title' => 'Auditor', 'data' => 'html_flaudit_ave', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => 'Gerente', 'data' => 'html_fladmin_ave', 'className' => "text-center"));
                }
                $key = 0;
                foreach ($data['art'] as $row) {
                    if ((isset($row['ubica_3|AVERIADO']) ? (int) $row['ubica_3|AVERIADO'] : 0) > 0) {
                        if (trim($row['fl_rev_ave']) <> 'R' && trim($row['fl_rev_ave']) <> 'N') {
                            $total_pvp = (int) $row['ubica_3|AVERIADO'] * (float) $row['va_pvp'];
                            $va_tot_pvp += $total_pvp;
                            $va_tot_cos += $row['va_tot_cos'];
                            $countif += (isset($row['ubica_3|AVERIADO']) ? (int) $row['ubica_3|AVERIADO'] : 0) > 0 ? 1 : 0;
                            $ubica_2 += isset($row['ubica_3|AVERIADO']) ? (int) $row['ubica_3|AVERIADO'] : 0;
//
                            $datatable_data[$key] = array(
                                'DT_RowId' => 'ca_ave_' . $row['co_art'],
                                'co_art' => $row['co_art'],
                                'co_fab' => $row['co_fab'],
                                'ds_art' => $row['ds_art'],
                                'va_ave' => (int) $row['ubica_3|AVERIADO'],
                                'html_flaudit_ave' => !empty($row['fl_rev_ave']) ? $this->procesar[$row['fl_rev_ave']]['name'] : '',
                                'html_fladmin_ave' => !empty($row['fl_ger_ave']) ? $this->procesar[$row['fl_ger_ave']]['name'] : '',
                                'va_pvp' => number_format($row['va_pvp'], 0, '.', ','),
                                'to_ave' => number_format($total_pvp, 0, '.', ','),
                            );

                            if (trim($co_estado) == 'V2' || trim($co_estado) == 'V3') {

                                $html_ajus = $html_cobr = $html_revi = '';
                                $ck_cobr = !empty($row['fl_rev_ave']) && trim($row['fl_rev_ave']) == 'N' ? 1 : 0;

                                if (!empty($row['fl_ger_ave'])) {
                                    switch (trim($row['fl_rev_ave'])) {
                                        case 'N':
                                            $va_tot_pvp -= $row['va_tot_pvp'];
                                            $va_tot_cos -= $row['va_tot_cos'];
                                            $countif -= (isset($row['va_con']) ? (int) $row['va_con'] : 0) > 0 ? 1 : 0;
                                            $ubica_2 -= isset($row['va_con']) ? (int) $row['va_con'] : 0;
                                            $html_cobr = 'NO AJUSTAR';
                                            break;
                                        case 'A':
                                            if (trim($co_estado) == 'V2') {
                                                $html_ajus = '<center><input checked="" name="ave_raB_' . $key . '" id="option_ajus_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                                $html_cobr = '<center><input name="ave_raB_' . $key . '" id="option_naju_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                                $html_revi = '<center><input name="ave_raB_' . $key . '" id="option_revi_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                            } else if (trim($co_estado) == 'V3') {
                                                $html_ajus = 'AJUSTAR';
                                            }
                                            break;
                                        case 'R':
                                            $boton++;
                                            $html_ajus = '<center><input name="ave_raB_' . $key . '" id="option_ajus_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                            $html_cobr = '<center><input name="ave_raB_' . $key . '" id="option_naju_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                            $html_revi = '<center><input checked="" name="ave_raB_' . $key . '" id="option_revi_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                            break;
                                    }
                                } else {
                                    $boton++;
                                    $aud_ajus = !empty($row['fl_rev_ave']) && trim($row['fl_rev_ave']) == 'A' ? '<span class="label label-success" style="padding: 3px 6px; border-radius: 100px;"><i class="fa fa-check"></i> ' . $row['fl_rev_user_ave'] . '</span>' : '';
                                    $html_ajus = '<center><input name="ave_raB_' . $key . '" id="option_ajus_ave[]" value="' . $row['co_art'] . '" type="radio"> ' . $aud_ajus . '</center>';
                                    $html_cobr = '<center><input name="ave_raB_' . $key . '" id="option_cobr_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    $html_revi = '<center><input name="ave_raB_' . $key . '" id="option_revi_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                }

                                $datatable_data[$key]['html_ajus_ave'] = $html_ajus;
                                $datatable_data[$key]['html_cobr_ave'] = $html_cobr;
                                $datatable_data[$key]['html_revi_ave'] = $html_revi;
                            }
                            $key++;
                        }
                    }
                }

                $btns = '';
                if (trim($co_estado) == 'V2') {
                    $btns .= '<button type="button" class="btn btn-danger" onclick="grabarArticulosAveriados(' . $co_bodega . ',' . $co_listado . ');"><i class="fa fa-save"></i> Grabar</button>';
                } else if (trim($co_estado) == 'V3') {
                    if ($boton > 0) {
                        $btns .= '<button type="button" class="btn btn-danger" onclick="grabarArticulosAveriados(' . $co_bodega . ',' . $co_listado . ');"><i class="fa fa-save"></i> Grabar</button>';
                    }
                }

                $array['btn'] = $btns;
                $array['inf']['va_tot_pvp'] = number_format($va_tot_pvp, 2, '.', ',');
                $array['inf']['va_tot_cos'] = number_format($va_tot_cos, 2, '.', ',');
                $array['inf']['countif'] = number_format($countif, 0, '.', ',');
                $array['inf']['ubica_2'] = number_format($ubica_2, 0, '.', ',');

                $array['art'] = array(
                    'data' => $datatable_data,
                    'columns' => $datatable_columns
                );
            }
        }

        return $array;
    }

}
