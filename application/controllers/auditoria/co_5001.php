<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class co_5001 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('mailer'));
        $this->load->model(array('auditoria/_auditoria'));
        session_start();
    }

    public $procesar = array(
        'R' => array(
            'name' => 'REVISION'
        ),
        'C' => array(
            'name' => 'COBRAR'
        ),
        'A' => array(
            'name' => 'AJUSTAR'
        ),
        'N' => array(
            'name' => 'NO AJUSTAR'
        )
    );

    public function _remap() {

        validaSession('auditoria/co_5001');
        $tipo = $this->uri->segment(3);

        $datos = $this->input->post();
        fnc_post($tipo, $datos, 'auditoria/co_5001');

        switch ($tipo) {
            case 'reDas':
                $this->_getDashboard();
                break;
            case 'reLisT':
                $this->_getListadosToma();
                break;
            case 'reToma':
                $this->_getToma();
                break;
            case 'consultaArtToma':
                $this->_consultaArtToma();
                break;
            case 'savProductos':
                $this->_grabarArticulos();
                break;
            case 'savProductosAveriado':
                $this->_grabarArticulosAveriado();
                break;
            case 'proProductos':
                $this->_procesarArticulos();
                break;
            case 'delToma':
                $this->_eliminarToma();
                break;
            case 'snInforme':
                $this->_enviaInforme();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _enviaInforme() {
        $ca = $this->load->library('../controllers/auditoria/co_auditoria');
        $ca = new co_auditoria();
        $bodega = $this->input->post('c_b');
        $listado = $this->input->post('c_l');
        $tipo_informe = $this->input->post('t_i');
        $email = $this->input->post('e_i');
        $email_contenido = $this->input->post('c_m');
        $data = $ca->_enviarInforme($bodega, $listado, $tipo_informe, $email, $email_contenido);
    }

    private function _crud() {
        $_SESSION['f_dis'] = true;

        $data = null;
        $urlControlador = 'auditoria/co_5001';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'js/plugins/switchery/dist/switchery.css',
            'css/daterangepicker/daterangepicker-bs3.css'
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/general.js',
            'js/config.js',
            'js/auditoria/j_5001.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js',
            'js/plugins/switchery/dist/switchery.js',
            'js/plugins/flot/jquery.flot.min.js',
            'js/plugins/flot/jquery.flot.resize.min.js',
            'js/plugins/flot/jquery.flot.pie.min.js',
            'js/plugins/flot/Smooth-0.1.7.js',
            'js/plugins/daterangepicker/daterangepicker.js'
        );

        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('auditoria/v_5001');
        $this->load->view('barras/footer');
    }

    private function _eliminarToma() {
        $almacen = $this->input->post('a_l'); //ALMACEN
        $toma = $this->input->post('c_t'); //TOMA
        $data = $this->_auditoria->_deshabilitaToma($almacen, $toma);
        echo json_encode($data);
    }

    private function _procesarArticulos() {
        $cod_almacen = $this->input->post('c_b');
        $cod_listado = $this->input->post('c_l');
        $co_estado = "V3";
        $result = $this->_auditoria->_actualizaToma($cod_almacen, $cod_listado, $co_estado);
        $result_bdg = $this->_auditoria->_retGerenteRegional($cod_almacen);
        $result_toma = $this->_auditoria->_retDescripcionToma($cod_almacen, $cod_listado);
        if (empty($result_bdg['err']) && $result['co_err'] == 0 && empty($result_toma['err'])) {
            $email_array = array(array(
                    'name' => $result_bdg['no_ger'],
                    'mail' => $result_bdg['e_mail']
            ));
            $mensaje = 'Estimado(a) ' . $result_bdg['no_ger'] . ' la toma ' . $result_toma['no_toma'] . ' se encuentra en estado PROCESADO POR AUDITORIA.';
            $res_mail = templ_mail('Auditoria Interna', $mensaje, 'Auditoria interna', 'Intranet', $email_array, null);
        }

        echo json_encode($result);
    }

    private function _grabarArticulos() {
        $data = $this->input->post('s_s');
        $cod_almacen = $this->input->post('c_b');
        $cod_listado = $this->input->post('c_l');
        $co_ususario = (int) $_SESSION['c_e'];
        $result = $this->_auditoria->registrarProductos($data, $cod_almacen, $cod_listado, $co_ususario, 0);
        echo json_encode($result);
//        $bandera = 0;
//        $menOK = $menER = $valida = '';
//
//        if ((int) $result['co_err'] > 0) {
//            if (!empty($result['data'])) {
//                for ($i = 0; $i < count($result['data']); $i++) {
//                    if ((int) $result['data'][$i]['co_err'] == 0) {
//                        $menOK.= $result['data'][$i]['co_art'] . "<br>";
//                    } else {
//                        $menER.= $result['data'][$i]['co_art'] . " - (" . trim($result['data'][$i]['tx_err']) . ").<br>";
//                    }
//                }
//                if (!empty($menOK)) {
//                    $valida.= 'Los siguientes productos han sido procesados con exito.<br>' . $menOK;
//                }
//                if (!empty($menER)) {
//                    $bandera = 1;
//                    $valida.= 'Los siguientes productos han presentado ERROR al actualizar.<br>' . $menER;
//                }
//            }
//        } else {
//            $bandera = 1;
//            $valida = $result;
//        }
//
//        echo json_encode(array(
//            'co_err' => $bandera,
//            'tx_err' => $valida
//        ));
    }

    private function _grabarArticulosAveriado() {
        $data = $this->input->post('s_s');
        $cod_almacen = $this->input->post('c_b');
        $cod_listado = $this->input->post('c_l');
        $co_ususario = (int) $_SESSION['c_e'];
        $result = $this->_auditoria->registrarProductos($data, $cod_almacen, $cod_listado, $co_ususario, 1);
        echo json_encode($result);
    }

    function _getDashboard() {
        $fe_ini = $this->input->post('f_i');
        $fe_fin = $this->input->post('f_f');
        $data = $this->_auditoria->retDashboardAdministracion($fe_ini, $fe_fin);
        $array = array('err' => $data['error'], 'usu' => '', 'pie' => '');
        if (empty($data['error'])) {
            $array['pie'] = $data['data']['pie'];
            if (!empty($data['data']['aud'])) {
                foreach ($data['data']['aud'] as $i => $row) {

                    $tiempo = $to_art = $su_art = 0;
                    $html = '<div class="spa-4">';
                    $html .= '<div class="box">';
                    $html .= '<div class="box-header text-center">';
                    $html .= '<img src="' . site_url('lib/loadImgNominaCredencial.php?ce=' . $row['co_aud']) . '" class="img-circle" width="85" height="85">';
                    $html .= '</div>';
                    $html .= '<div class="box-body text-center">';
                    $html .= '<h6><b>' . $row['groupeddata'][0]['no_aud'] . '</b></h6>';
                    $html .= '<h6>' . $row['groupeddata'][0]['no_car'] . '</h6>';
                    $html .= '<div id="pie' . $row['co_aud'] . '" class="des_box" style="height:80px;"></div>';
                    $html .= '</div>';
                    $html .= '<div class="box-footer text-center">';

                    if (empty($row['res']['err']) && !empty($row['res']['dat'])) {
                        foreach ($row['res']['dat'] as $v) {
                            $tiempo += $v['ti_min'];
                            $to_art += $v['to_art'];
                            $su_art += $v['su_art'];
                        }
                    }

                    $html .= '<div class="col-xs-4">';
                    $html .= '<i class="fa fa-clock-o color_age"></i> <span class="text-blanco">' . toHours($tiempo) . '</span>';
                    $html .= '</div>';
                    $html .= '<div class="col-xs-4">';
                    $html .= '<i class="fa fa-exclamation color_age"></i> <span class="text-blanco">' . $to_art . ' Items</span>';
                    $html .= '</div>';
                    $html .= '<div class="col-xs-4">';
                    $html .= '<i class="fa fa-exclamation color_age"></i> <span class="text-blanco">' . $su_art . ' Und</span>';
                    $html .= '</div>';
                    $html .= '<div class="clearfix"></div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $array['usu'][$i]['c'] = $row['co_aud'];
                    $array['usu'][$i]['dat'] = $html;
                    $array['usu'][$i]['pie'] = $row['pie'];
                }
            }
        }
        echo json_encode($array);
    }

    function _getListadosToma() {
        $fe_ini = $this->input->post('f_i');
        $fe_fin = $this->input->post('f_f');
        $co_est = $this->input->post('c_e');
        $co_usu = $this->input->post('c_u');
        $data = $this->_auditoria->retListadosToma($fe_ini, $fe_fin, $co_est, $co_usu);
        if (!empty($data['data'])) {
            echo json_encode($data['data']);
        } else {
            echo json_encode(array(
                "sEcho" => 1,
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            ));
        }
    }

    function _getToma() {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $articulos_contados = $this->input->post('c_a');
        $articulos_contados = (int) $articulos_contados;
        $co_bodega = $this->input->post('c_b');
        $co_listado = $this->input->post('c_l');
        $co_estado = $this->input->post('c_e');
        $data = $this->_auditoria->_retDetalleListadoLider($co_bodega, $co_listado, $_SESSION['c_e'], 1);
        $data['inf']['btn_fil'] = $articulos_contados;
        if ($articulos_contados == 1) {
            if (empty($data['err']) && !empty($data['art'])) {
                $data_art = null;
                foreach ($data['art'] as $row) {
                    if ((int) $row['va_con'] > 0) {
                        $data_art[] = $row;
                    }
                }
                $data['art'] = $data_art;
            }
        }
        $array = $this->creaTablaArticulos($data, $co_bodega, $co_listado, $co_estado);
        $array['tab_ave'] = $this->creaTablaAveriados($data, $co_bodega, $co_listado, $co_estado);
        echo json_encode($array);
    }

    private function creaTablaArticulos($data, $co_bodega, $co_listado, $co_estado) {

        $array = array('inf' => $data['inf'], 'art' => null, 'btn' => '', 'err' => $data['err']);

        $va_tot_pvp = 0;
        $va_tot_cos = 0;
        $va_tot_dif_pvp = 0;
        $va_tot_dif_cos = 0;
        $ubica_2 = 0;
        $countif = 0;
        $boton = 0;

        if (empty($data['err'])) {
            if (!empty($data['art'])) {
                $datatable_data = null;

                $datatable_columns = array(
                    array('title' => 'F4', 'data' => 'html_f4'),
                    array('title' => 'C&oacute;digo', 'data' => 'co_art'),
                    array('title' => 'Descripci&oacute;n', 'data' => 'ds_art'),
                    array('title' => 'Averiado', 'data' => 'va_ave', 'className' => "text-center"),
                    array('title' => 'FIS', 'data' => 'va_con', 'className' => "text-center con_art_"),
                );

                if ((int) $data['inf']['da_ecv'] > 0 && $co_estado == 'S') {
                    array_push($datatable_columns, array('title' => '<div class="defaultKTV"><div class="ktvTop"><span class="ktvText"><input id="btn_check_vco" type="checkbox"><label for="btn_check_vco">VCO</label></span></div></div>', 'data' => 'to_est', 'className' => "text-center defaultKTV"));
                }

                array_push($datatable_columns, array('title' => 'SIST', 'data' => 'va_sis', 'className' => "text-center con_sis_"));
                array_push($datatable_columns, array('title' => 'DIF', 'data' => 'va_dif', 'className' => "text-center con_dif_"));

                if ($data['inf']['co_tip'] == 2) {
                    array_push($datatable_columns, array('title' => 'DIF 2', 'data' => 'va_dif2', 'className' => "text-center"));
                }

                array_push($datatable_columns, array('title' => 'COSTO', 'data' => 'va_costo', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => 'PVP', 'data' => 'va_pvp', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => 'T.COSTO', 'data' => 'va_tcosto', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => 'T.PVP', 'data' => 'va_tpvp', 'className' => "text-center"));
                array_push($datatable_columns, array('title' => 'BV', 'data' => 'va_sal_vir', 'className' => "text-center"));
                if ($data['inf']['co_tip'] == 2) {
                    array_push($datatable_columns, array('title' => 'MOT', 'data' => 'va_motivos', 'className' => "text-center"));
                }
                if (trim($co_estado) == 'V2' || trim($co_estado) == 'V3') {
                    array_push($datatable_columns, array('title' => '<input name="ck_apro" id="ck_ajus" type="radio"> Ajustar', 'data' => 'html_ajus', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => '<input name="ck_apro" id="ck_naju" type="radio"> No ajustar', 'data' => 'html_cobr', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => '<input name="ck_apro" id="ck_revi" type="radio"> Revisi&oacute;n', 'data' => 'html_revi', 'className' => "text-center"));
                } else {
                    array_push($datatable_columns, array('title' => 'Auditor', 'data' => 'html_flaudit', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => 'Gerente', 'data' => 'html_fladmin', 'className' => "text-center"));
                }

                foreach ($data['art'] as $key => $row) {
                    $va_tot_pvp += $row['va_tot_pvp'];
                    $va_tot_cos += $row['va_tot_cos'];
                    $va_tot_dif_pvp += $row['va_tot_dif_pvp'];
                    $va_tot_dif_cos += $row['va_tot_dif_cos'];
                    $countif += (isset($row['va_con']) ? (int) $row['va_con'] : 0) > 0 ? 1 : 0;
                    $ubica_2 += isset($row['va_con']) ? (int) $row['va_con'] : 0;

                    $datatable_data[$key] = array(
                        'DT_RowId' => 'ca_' . $row['co_art'],
                        'html_f4' => $row['html_f4'] . " " . $row['html_transito'],
                        'html_flaudit' => !empty($row['fl_rev']) ? $this->procesar[$row['fl_rev']]['name'] : '',
                        'html_fladmin' => !empty($row['fl_ger']) ? $this->procesar[$row['fl_ger']]['name'] : '',
                        'co_art' => $row['_link'],
                        'ds_art' => $row['ds_art'],
                        'va_ave' => isset($row['ubica_3|AVERIADO']) ? number_format($row['ubica_3|AVERIADO'], 0, '.', ',') : 0,
                        'va_con' => number_format($row['va_con'], 0, '.', ','),
                        'va_costo' => number_format($row['va_cos'], 2, '.', ','),
                        'va_pvp' => number_format($row['va_pvp'], 2, '.', ','),
                        'va_tcosto' => number_format($row['va_tot_dif_cos'], 2, '.', ','),
                        'va_tpvp' => number_format($row['va_tot_dif_pvp'], 2, '.', ',')
                    );

                    if (trim($co_estado) == 'V2' || trim($co_estado) == 'V3') {

                        $html_ajus = $html_cobr = $html_revi = '';
                        $ck_cobr = !empty($row['fl_rev']) && trim($row['fl_rev']) == 'N' ? 1 : 0;

                        if (!empty($row['fl_rev'])) {
                            switch (trim($row['fl_rev'])) {
                                case 'N':
                                    $va_tot_pvp -= $row['va_tot_pvp'];
                                    $va_tot_cos -= $row['va_tot_cos'];
                                    $va_tot_dif_pvp -= $row['va_tot_dif_pvp'];
                                    $va_tot_dif_cos -= $row['va_tot_dif_cos'];
                                    $countif -= (isset($row['va_con']) ? (int) $row['va_con'] : 0) > 0 ? 1 : 0;
                                    $ubica_2 -= isset($row['va_con']) ? (int) $row['va_con'] : 0;
                                    $html_cobr = 'NO AJUSTAR';
                                    break;
                                case 'A':
                                    if (trim($co_estado) == 'V2') {
                                        $html_ajus = '<center><input checked="" name="raB_' . $key . '" id="option_ajus[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                        $html_cobr = '<center><input name="raB_' . $key . '" id="option_naju[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                        $html_revi = '<center><input name="raB_' . $key . '" id="option_revi[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    } else if (trim($co_estado) == 'V3') {
                                        $html_ajus = 'AJUSTAR';
                                    }
                                    break;
                                case 'R':
                                    $boton++;
                                    $html_ajus = '<center><input name="raB_' . $key . '" id="option_ajus[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    $html_cobr = '<center><input name="raB_' . $key . '" id="option_naju[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    $html_revi = '<center><input checked="" name="raB_' . $key . '" id="option_revi[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    break;
                            }
                        } else {
                            if (trim($co_estado) == 'V2') {
                                $html_ajus = '<center><input name="raB_' . $key . '" id="option_ajus[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                $html_cobr = '<center><input name="raB_' . $key . '" id="option_naju[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                $html_revi = '<center><input name="raB_' . $key . '" id="option_revi[]" value="' . $row['co_art'] . '" type="radio"></center>';
                            }
                        }

                        $datatable_data[$key]['html_ajus'] = $html_ajus;
                        $datatable_data[$key]['html_cobr'] = $html_cobr;
                        $datatable_data[$key]['html_revi'] = $html_revi;
                    }

                    if ((int) $data['inf']['da_ecv'] > 0 && $co_estado == 'S') {
                        $datatable_data[$key]['to_est'] = $row['to_est'] > 0 ? '<div class="ktvTop"><span class="ktvText"><input id="vc_' . $row['co_art'] . '" name="option_vc[]" type="checkbox" value="' . $row['co_art'] . '"><label for="vc_' . $row['co_art'] . '">' . $row['to_est'] . '</label></span></div>' : 0;
                    }

                    $datatable_data[$key]['va_sis'] = number_format($row['va_sis'], 0, '.', ',');
                    $datatable_data[$key]['va_dif'] = number_format($row['va_dif'], 0, '.', ',');
                    //$datatable_data[$key]['va_sal_vir'] = number_format($row['va_sal_vir'], 0, '.', ',');
                    $datatable_data[$key]['va_sal_vir'] = $row['va_sal_vir'] . (!empty($row['va_vir_hist']) ? " <sup>[" . trim($row['va_vir_hist'], ';') . "]</sup>" : "");
                    if ($data['inf']['co_tip'] == 2) {
                        $datatable_data[$key]['va_dif2'] = number_format($row['va_dif2'], 0, '.', ',');
                        $datatable_data[$key]['va_motivos'] = $row['abr'];
                    }
                }

                $btns = '';

                if (trim($co_estado) == 'V2') {
                    $btns .= '<button type="button" class="btn btn-danger" onclick="grabarArticulos(' . $co_bodega . ',' . $co_listado . ');"><i class="fa fa-save"></i> Grabar</button>';
                    $btns .= '<button type="button" class="btn btn-success" onclick="procesarArticulos(' . $co_bodega . ',' . $co_listado . ');" style="margin-left: 10px;"><i class="fa fa-cogs"></i> Env&iacute;o ajuste</button>';
                } else if (trim($co_estado) == 'V3') {
                    if ($boton > 0) {
                        $btns .= '<button type="button" class="btn btn-danger" onclick="grabarArticulos(' . $co_bodega . ',' . $co_listado . ');"><i class="fa fa-save"></i> Grabar</button>';
                    }
                }

                $btns .= '<div class="defaultKTV"><div class="ktvTop"><span class="ktvText"><input id="btn_check_f_art" type="checkbox" ' . ($data['inf']['btn_fil'] == 1 ? 'checked' : '') . '><label for="btn_check_f_art">Art&iacute;culos Contados</label></span></div></div>';

                $array['inf']['va_tot_pvp'] = number_format($va_tot_pvp, 2, '.', ',');
                $array['inf']['va_tot_cos'] = number_format($va_tot_cos, 2, '.', ',');
                $array['inf']['va_tot_dif_pvp'] = number_format($va_tot_dif_pvp, 2, '.', ',');
                $array['inf']['va_tot_dif_cos'] = number_format($va_tot_dif_cos, 2, '.', ',');
                $array['inf']['countif'] = number_format($countif, 0, '.', ',');
                $array['inf']['ubica_2'] = number_format($ubica_2, 0, '.', ',');

                $array['btn'] = $btns;

                $array['art'] = array(
                    'data' => $datatable_data,
                    'columns' => $datatable_columns
                );
            }
        }

        return $array;
    }

    private function creaTablaAveriados($data, $co_bodega, $co_listado, $co_estado) {
        $array = null;
        $va_tot_pvp = 0;
        $va_tot_cos = 0;
        $ubica_2 = 0;
        $countif = 0;
        $boton = 0;

        if (empty($data['err'])) {
            if (!empty($data['art'])) {

                $datatable_data = null;

                $datatable_columns = array(
                    array('title' => 'C&oacute;digo', 'data' => 'co_art'),
                    array('title' => 'Fabrica', 'data' => 'co_fab'),
                    array('title' => 'Descripci&oacute;n', 'data' => 'ds_art'),
                    array('title' => 'CANT', 'data' => 'va_ave', 'className' => "text-center"),
                    array('title' => 'PVP', 'data' => 'va_pvp', 'className' => "text-center"),
                    array('title' => 'TOT PVP', 'data' => 'to_ave', 'className' => "text-center")
                );

                if (trim($co_estado) == 'V2' || trim($co_estado) == 'V3') {
                    array_push($datatable_columns, array('title' => '<input name="ck_apro_ave" id="ck_ajus_ave" type="radio"> Ajustar', 'data' => 'html_ajus_ave', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => '<input name="ck_apro_ave" id="ck_naju_ave" type="radio"> No ajustar', 'data' => 'html_cobr_ave', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => '<input name="ck_apro_ave" id="ck_revi_ave" type="radio"> Revisi&oacute;n', 'data' => 'html_revi_ave', 'className' => "text-center"));
                } else {
                    array_push($datatable_columns, array('title' => 'Auditor', 'data' => 'html_flaudit_ave', 'className' => "text-center"));
                    array_push($datatable_columns, array('title' => 'Gerente', 'data' => 'html_fladmin_ave', 'className' => "text-center"));
                }
                $key = 0;
                foreach ($data['art'] as $row) {
                    if ((isset($row['ubica_3|AVERIADO']) ? (int) $row['ubica_3|AVERIADO'] : 0) > 0) {

                        $total_pvp = (int) $row['ubica_3|AVERIADO'] * (float) $row['va_pvp'];
                        $total_cos = (int) $row['ubica_3|AVERIADO'] * (float) $row['va_cos'];
                        $va_tot_pvp += $total_pvp;
                        $va_tot_cos += $total_cos;
                        $countif += (isset($row['ubica_3|AVERIADO']) ? (int) $row['ubica_3|AVERIADO'] : 0) > 0 ? 1 : 0;
                        $ubica_2 += isset($row['ubica_3|AVERIADO']) ? (int) $row['ubica_3|AVERIADO'] : 0;
//
                        $datatable_data[$key] = array(
                            'DT_RowId' => 'ca_ave_' . $row['co_art'],
                            'co_art' => $row['co_art'],
                            'co_fab' => $row['co_fab'],
                            'ds_art' => $row['ds_art'],
                            'va_ave' => (int) $row['ubica_3|AVERIADO'],
                            'html_flaudit_ave' => !empty($row['fl_rev_ave']) ? $this->procesar[$row['fl_rev_ave']]['name'] : '',
                            'html_fladmin_ave' => !empty($row['fl_ger_ave']) ? $this->procesar[$row['fl_ger_ave']]['name'] : '',
                            'va_pvp' => number_format($row['va_pvp'], 2, '.', ','),
                            'to_ave' => number_format($total_pvp, 2, '.', ','),
                        );

                        if (trim($co_estado) == 'V2' || trim($co_estado) == 'V3') {

                            $html_ajus = $html_cobr = $html_revi = '';
                            $ck_cobr = !empty($row['fl_rev_ave']) && trim($row['fl_rev_ave']) == 'N' ? 1 : 0;

                            if (!empty($row['fl_rev_ave'])) {
                                switch (trim($row['fl_rev_ave'])) {
                                    case 'N':
//                                        $va_tot_pvp -= $row['va_tot_pvp'];
//                                        $va_tot_cos -= $row['va_tot_cos'];
//                                        $va_tot_dif_pvp -= $row['va_tot_dif_pvp'];
//                                        $va_tot_dif_cos -= $row['va_tot_dif_cos'];
//                                        $countif -= (isset($row['va_con']) ? (int) $row['va_con'] : 0) > 0 ? 1 : 0;
//                                        $ubica_2 -= isset($row['va_con']) ? (int) $row['va_con'] : 0;
                                        $html_cobr = 'NO AJUSTAR';
                                        break;
                                    case 'A':
                                        if (trim($co_estado) == 'V2') {
                                            $html_ajus = '<center><input checked="" name="ave_raB_' . $key . '" id="option_ajus_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                            $html_cobr = '<center><input name="ave_raB_' . $key . '" id="option_naju_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                            $html_revi = '<center><input name="ave_raB_' . $key . '" id="option_revi_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                        } else if (trim($co_estado) == 'V3') {
                                            $html_ajus = 'AJUSTAR';
                                        }
                                        break;
                                    case 'R':
                                        $boton++;
                                        $html_ajus = '<center><input name="ave_raB_' . $key . '" id="option_ajus_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                        $html_cobr = '<center><input name="ave_raB_' . $key . '" id="option_naju_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                        $html_revi = '<center><input checked="" name="ave_raB_' . $key . '" id="option_revi_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                        break;
                                }
                            } else {
                                if (trim($co_estado) == 'V2') {
                                    $html_ajus = '<center><input name="ave_raB_' . $key . '" id="option_ajus_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    $html_cobr = '<center><input name="ave_raB_' . $key . '" id="option_naju_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                    $html_revi = '<center><input name="ave_raB_' . $key . '" id="option_revi_ave[]" value="' . $row['co_art'] . '" type="radio"></center>';
                                }
                            }

                            $datatable_data[$key]['html_ajus_ave'] = $html_ajus;
                            $datatable_data[$key]['html_cobr_ave'] = $html_cobr;
                            $datatable_data[$key]['html_revi_ave'] = $html_revi;
                        }
                        $key++;
                    }
                }

                $btns = '';
                if (trim($co_estado) == 'V2') {
                    $btns .= '<button type="button" class="btn btn-danger" onclick="grabarArticulosAveriados(' . $co_bodega . ',' . $co_listado . ');"><i class="fa fa-save"></i> Grabar</button>';
                } else if (trim($co_estado) == 'V3') {
                    if ($boton > 0) {
                        $btns .= '<button type="button" class="btn btn-danger" onclick="grabarArticulosAveriados(' . $co_bodega . ',' . $co_listado . ');"><i class="fa fa-save"></i> Grabar</button>';
                    }
                }

                $array['btn'] = $btns;
                $array['inf']['va_tot_pvp'] = number_format($va_tot_pvp, 2, '.', ',');
                $array['inf']['va_tot_cos'] = number_format($va_tot_cos, 2, '.', ',');
                $array['inf']['countif'] = number_format($countif, 0, '.', ',');
                $array['inf']['ubica_2'] = number_format($ubica_2, 0, '.', ',');

                $array['art'] = array(
                    'data' => $datatable_data,
                    'columns' => $datatable_columns
                );
            }
        }

        return $array;
    }

    private function _consultaArtToma() {
        $articulo = $this->input->post('c_a');
        $parametros_tx = $this->input->post('c_p');
        $parametros = explode('-', $parametros_tx);
        $data = $this->_auditoria->retDetalleArticuloListado($parametros[0], $parametros[1], $parametros[3], $_SESSION['c_e'], $articulo);
        $array = array('data' => null, 'err' => $data['err']);

        if (empty($data['err'])) {

            $section = '';

            if (!empty($data['lis'])) {

                $table = '<table class="table table-bordered"><tbody id="tabUbicaciones">';
                foreach ($data['lis'] as $row) {
                    $table .= '<tr><td width="25%">' . $row['ds_ubi'] . '</td>';
                    $table .= '<td width="45%" style="font-size: 32px;" class="text-right"><div id="lTp_' . $row['co_ubi'] . '">' . $row['va_conso'] . '</div></td>';
                    $table .= '</tr>';
                }
                $table .= '</tbody></table>';
                //$table .= '<div class = "pull-right"><a href = "' . site_url('inventario/co_3002?c_a=' . $articulo . '&n_a=Articulo&c_b=' . $parametros[0]) . '" class = "btn btn-success" target = "_blank" style = "color:#FFF;margin-right: 10px;"><i class = "fa fa-search"></i> Ver inventario</a></div>';
                $section = $table;
            }

            if (!empty($data['his'])) {

                $section_tab = '';
                $section = '<section class="col-xs-12" style="margin-top: 10px;"><ul id="ubica_tab" class="nav nav-tabs"><li class="active"><a data-target="#tab_ub_1" data-toggle="tab">UBICACIONES</a></li>';

                foreach ($data['his'] as $row) {
                    $section .= '<li><a data-target="#tab_ub_' . $row['co_est'] . '" data-toggle="tab">' . $row['no_est'] . '</a></li>';
                    $section_tab .= '<div class="tab-pane" id="tab_ub_' . $row['co_est'] . '" style="margin-top: 15px;">';

                    foreach ($row['data'] as $dat) {

                        $section_tab .= '<div class="col-xs-12">' . $dat['no_aud'] . '<table class="table table-bordered"><thead><tr><th>Ubicaci&oacute;n</th><th>Valor</th><th>F&oacute;rmula</th><tr></thead><tbody>';

                        foreach ($dat['data'] as $est) {

                            $estFormula = unserialize($est['va_for']);
                            $stringForm = '';
                            if (!empty($estFormula['data'])) {
                                foreach ($estFormula['data'] as $for) {
                                    $stringForm .= $for['signo'] . $for['value'];
                                }
                                $stringForm = $stringForm . '=' . $estFormula['total'];
                            }

                            $section_tab .= '<tr>';
                            $section_tab .= '<td>' . $est['no_ubi'] . '</td>';
                            $section_tab .= '<td>' . $est['va_can'] . '</td>';
                            $section_tab .= '<td>';
                            $section_tab .= $stringForm;

                            $observacion_arr = unserialize($est['va_obs']);
                            if (!empty($observacion_arr)) {
                                $section_tab .= '</br><b>Comentarios:</b>';
                                $section_tab .= '<div class="col-xs-12" style="padding-left: 25px;">';
                                $section_tab .= '<ul>';
                                foreach ($observacion_arr as $row) {
                                    if (!empty($row)) {
                                        $section_tab .= '<li>' . $row . '</li>';
                                    }
                                }
                                $section_tab .= '</ul>';
                                $section_tab .= '</div>';
                            }

                            $section_tab .= '</td>';

                            $section_tab .= '</tr>';
                        }

                        $section_tab .= '</tbody></table></div>';
                    }

                    $section_tab .= '</div>';
                }

                $section .= '</ul>';
                $section .= '<div class="tab-content" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; padding-bottom: 8px;"><div class="tab-pane active" id="tab_ub_1" style="margin-top: 15px;">' . $table . '</div>';
                $section .= $section_tab;
                $section .= '</div></section>';
            }

            $array['data'] = $section;
        }

        echo json_encode($array);
    }

}
