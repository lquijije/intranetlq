<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_5003 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('auditoria/_auditoria', 'proceso/reclamos/m_111'));
        session_start();
    }

    public function _remap() {

        validaSession('auditoria/co_5003');
        $tipo = $this->uri->segment(3);

        switch ($tipo) {
            case 'retTom':
                $this->_retTomas();
                break;
            case 'snInforme':
                $this->_enviaInforme();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {
        $data = null;
        $data['permisos'] = '';
        $urlControlador = 'auditoria/co_5003';
        $data['urlControlador'] = $urlControlador;
        $data['fecha_inicial'] = date('Y/m/d', strtotime('-15 day'));
        $data['fecha_final'] = date('Y/m/d', strtotime('+15 day'));

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'css/datetimepicker/jquery.datetimepicker.css'
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/config.js',
            'js/general.js',
            'js/auditoria/j_5003.js',
            'js/plugins/datetimepicker/jquery.datetimepicker.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js'
        );

        $empresas = $this->m_111->retEmpresaAsignadas();

        $cmb_almacen = null;
        if (!empty($empresas)) {
            $cmb_almacen['NNN'] = ' -- ';
            foreach ($empresas as $row) {
                $cmb_almacen[$row['cod_bdg']] = $row['des_bdg'];
            }
        } else {
            $cmb_almacen['NNN'] = 'No tiene bodegas asignadas';
        }

        $data['cmb_almacen'] = form_dropdown('cmb_almacen', $cmb_almacen, '', "id='cmb_almacen' class='form-control'");

        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('auditoria/v_5003');
        $this->load->view('barras/footer');
    }

    private function _enviaInforme() {
        $ca = $this->load->library('../controllers/auditoria/co_auditoria');
        $ca = new co_auditoria();
        $bodega = $this->input->post('c_b');
        $listado = $this->input->post('c_l');
        $tipo_informe = $this->input->post('t_i');
        $email = $this->input->post('e_i');
        $email_contenido = $this->input->post('c_m');
        $data = $ca->_enviarInforme($bodega, $listado, $tipo_informe, $email, $email_contenido);
        print_r($data);
    }

    private function _retTomas() {
        $desde = $this->input->post('f_d');
        $hasta = $this->input->post('f_h');
        $co_bodega = $this->input->post('c_b');
        $data = $this->_auditoria->retTomasAlmacen($co_bodega, $desde, $hasta);
        $array = array('data' => null, 'err' => $data['err']);
        if (empty($data['err'])) {
            if (!empty($data['data'])) {
                $datatable_data = null;
                $datatable_columns = array(
                    array('title' => 'Almac&eacute;n', 'data' => 'no_bdg'),
                    array('title' => 'Solicitud', 'data' => 'co_lis'),
                    array('title' => 'Estado', 'data' => 'no_est', 'className' => "text-center"),
                    array('title' => 'Fecha', 'data' => 'fe_gen', 'className' => "text-center"),
                    array('title' => 'Origen', 'data' => 'or_lis', 'className' => "text-center"),
                    array('title' => 'Descripci&oacute;n', 'data' => 'ds_lis', 'className' => "text-center"),
                    array('title' => '', 'data' => 'html', 'className' => "text-center")
                );
                foreach ($data['data'] as $row) {
                    $datatable_data[] = array(
                        'no_bdg' => $row['no_bdg'],
                        'co_lis' => $row['co_lis'],
                        'no_est' => $row['no_est'],
                        'fe_gen' => $row['fe_gen'],
                        'or_lis' => $row['or_lis'],
                        'ds_lis' => $row['ds_lis'],
                        'html' => '<button type="button" onclick="fn_generarInforme(' . (int) $co_bodega . ',' . (int) $row['co_lis'] . ')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-send"></i></button>',
                    );
                }
                $array['data'] = array(
                    'data' => $datatable_data,
                    'columns' => $datatable_columns
                );
            }
        }
        echo json_encode($array);
    }

    private function _enviaInforma() {

        $array = array('co_err' => 1, 'tx_err' => '');
        $co_bodega = $this->input->post('c_b');
        $co_solicitud = $this->input->post('c_s');
        $email = $this->input->post('e_m');
        $co_usuario = $_SESSION['c_e'];
        $email_array = array(
            array(
                'name' => 'Administrador',
                'mail' => $email
            )
        );
        $files_array = array(
            array(
                'name' => 'TOMA-' . $co_bodega . '-' . $co_solicitud . '-' . date('m') . '.xls',
                'fileString' => $this->_generaExcelAdministrador($co_bodega, $co_solicitud)
            )
        );
        $mensaje = 'Estimado(a) administrador(a) se adjunta el archivo de la toma ';
        $res_mail = templ_mail('Auditoria Interna', $mensaje, 'Auditoria interna', 'Extranet', $email_array, $files_array);

        $array['co_err'] = $res_mail['co_err'];
        $array['tx_err'] = $res_mail['tx_err'];

        echo json_encode($array);
    }

    private function _generaExcelAdministrador($co_bodega, $co_solicitud) {

        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '-1');
        $excel = '';

        $usu = $this->_auditoria->_retLiderToma($co_bodega, $co_solicitud);
        $co_usuario = empty($usu['err']) ? $usu['co_auditor'] : 0;

        $data = $this->_auditoria->_retDetalleListadoLiderEXTRANET($co_bodega, $co_solicitud, $co_usuario);
        $datos = $data['art'];

        if (!empty($datos)) {

            $TOTAL_POBLACION_PVP = 0;
            $TOTAL_POBLACION_COSTO = 0;
            $TOTAL_DIFERENCIA_PVP = 0;
            $TOTAL_DIFERENCIA_COSTO = 0;
            $TOTAL_ITEMS = 0;
            $TOTAL_UNIDADES = 0;

            $celdas = array(
                '0' => 'A', '1' => 'B', '2' => 'C', '3' => 'D', '4' => 'E', '5' => 'F', '6' => 'G', '7' => 'H', '8' => 'I', '9' => 'J', '10' => 'K', '11' => 'L', '12' => 'M', '13' => 'N', '14' => 'O', '15' => 'P', '16' => 'Q', '17' => 'R', '18' => 'S', '19' => 'T', '20' => 'U', '21' => 'V', '22' => 'W', '23' => 'X', '24' => 'Y', '25' => 'Z'
            );

            $claves = array_keys($datos[0]);
            $ClavesPermitidas = null;

            foreach ($claves as $row => $keys) {
                if (strpos($keys, 'ubica_') !== FALSE) {
                    $ClavesPermitidas[] = $keys;
                }
            }

            $this->load->library('PHPExcel');

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet();

            $objPHPExcel->
                    getProperties()
                    ->setCreator("AUDITORIA INTERNA")
                    ->setTitle("AUDITORIA INTERNA")
                    ->setSubject("AUDITORIA INTERNA")
                    ->setDescription("AUDITORIA INTERNA")
                    ->setKeywords("AUDITORIA INTERNA")
                    ->setCategory("AUDITORIA INTERNA");

            $borders1 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000'),
                    )
                ),
            );

            $borders = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000'),
                    )
                ),
            );

            $LetraINICIO = 3;
            $Letra = $celdas[($LetraINICIO + count($ClavesPermitidas) + 10)];
            $LetraINI = $LetraINICIO;
            $linea = 1;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('AUDITORIA INTERNA', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($co_bodega . '-' . $co_solicitud . '-' . date('m'), PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('INVENTARIO FISICO ', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':D' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('#PAPELETA: ' . $co_bodega . '-' . $co_solicitud . '-' . date('m'), PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':D' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit("DESCRIPCION: " . $data['inf']['ds_tom'], PHPExcel_Cell_DataType::TYPE_STRING);


            $linea = $linea + 2;
            $objPHPExcel->getActiveSheet()->mergeCells('E' . $linea . ':G' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea . ':G' . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea . ':G' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit('FISICO', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;

            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'CODIGO');
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $linea, 'FABRICA');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 'DESCRIPCION');
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $linea, 'ANT');

            foreach ($ClavesPermitidas as $raw) {
                $LetraINI++;
                $titulo = explode('|', $raw);
                $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, $titulo[1]);
            }

            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT FIS');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'SALDO SIST');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT DIF');
//            $LetraINI++;
//            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'BV');
            $LetraINI++;
//            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT POB COSTO');
//            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOT POB PVP');
//            $LetraINI++;
//            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'COSTO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'PVP');
//            $LetraINI++;
//            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOTAL COSTO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'TOTAL  PVP');
            $LetraINI = $LetraINI + 2;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'SALDO SISTEMA HISTORICO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'BODEGA VIRTUAL HISTORICO');
            $LetraINI++;
            $objPHPExcel->getActiveSheet()->setCellValue($celdas[$LetraINI] . $linea, 'COMENTARIOS');

            $lineaIniciaDatos = $linea + 1;

            foreach ($datos as $row) {
                $linea++;
                $LetraINI = $LetraINICIO;
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders);
                $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['co_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['co_fab'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['ds_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('D' . $linea)->setValueExplicit($row['va_ant'], PHPExcel_Cell_DataType::TYPE_NUMERIC);

                foreach ($ClavesPermitidas as $raw) {
                    $LetraINI++;
                    $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row[$raw], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                }
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_con'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_sis'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_dif'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//                $LetraINI++;
//                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_sal_vir'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//                $LetraINI++;
//                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//                $LetraINI++;
//                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
//                $LetraINI++;
//                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_dif_cos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_tot_dif_pvp'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI = $LetraINI + 2;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_sis_hist'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['va_vir_hist'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $LetraINI++;
                $objPHPExcel->getActiveSheet()->getCell($celdas[$LetraINI] . $linea)->setValueExplicit($row['obs'], PHPExcel_Cell_DataType::TYPE_STRING);
                $TOTAL_POBLACION_PVP += $row['va_tot_pvp'];
                $TOTAL_POBLACION_COSTO += $row['va_tot_cos'];
                $TOTAL_DIFERENCIA_PVP += $row['va_tot_dif_pvp'];
                $TOTAL_DIFERENCIA_COSTO += $row['va_tot_dif_cos'];
            }

            $lineaFinDatos = $linea;
            $sumTotPVP = "=SUM(" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 6)] . $lineaIniciaDatos . ":" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 6)] . $lineaFinDatos . ")";
            //$sumTotCOS = "=SUM(" . $celdas[$LetraINI - 1] . $lineaIniciaDatos . ":" . $celdas[$LetraINI - 1] . $lineaFinDatos . ")";
            $sumTotDifPVP = "=SUM(" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $lineaIniciaDatos . ":" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $lineaFinDatos . ")";
            //$sumTotDifCOS = "=SUM(" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $lineaIniciaDatos . ":" . $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $lineaFinDatos . ")";
            $TOTAL_ITEMS = '=COUNTIF(' . $celdas[(count($ClavesPermitidas) + 4)] . $lineaIniciaDatos . ':' . $celdas[(count($ClavesPermitidas) + 4)] . $lineaFinDatos . ',"<>0")';
            $TOTAL_UNIDADES = '=SUM(' . $celdas[(count($ClavesPermitidas) + 4)] . $lineaIniciaDatos . ':' . $celdas[(count($ClavesPermitidas) + 4)] . $lineaFinDatos . ')';

            $linea = $linea + 2;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);

            $vaTotDifCOS = $celdas[($LetraINICIO + count($ClavesPermitidas) + 5)] . $linea;
            $vaTotDifPVP = $celdas[($LetraINICIO + count($ClavesPermitidas) + 6)] . $linea;
            $vaTotCOS = $celdas[$LetraINI - 1] . $linea;
            $vaTotPVP = $celdas[$LetraINI] . $linea;

            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA');
            $objPHPExcel->getActiveSheet()->SetCellValue($celdas[3 + (count($ClavesPermitidas) + 5)] . $linea, $sumTotDifPVP);
            $objPHPExcel->getActiveSheet()->SetCellValue($celdas[3 + (count($ClavesPermitidas) + 6)] . $linea, $sumTotPVP);

            $linea = $linea + 2;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':C' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'RESUMEN INVENTARIO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 0);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_POBLACION_PVP);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL POBLACION COSTO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_POBLACION_COSTO);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A PVP');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_DIFERENCIA_PVP);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL DIFERENCIA A COSTO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_DIFERENCIA_COSTO);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL ITEMS');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_ITEMS);
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':B' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':C' . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'TOTAL UNIDADES NETO');
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, $TOTAL_UNIDADES);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('LISTA');

            @ob_start();
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $writer->save("php://output");
            $excel = @ob_get_contents();
            @ob_end_clean();
        } else {
            
        }
        return $excel;
    }

}
