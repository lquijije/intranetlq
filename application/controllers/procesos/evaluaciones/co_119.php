<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_119 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/evaluaciones/m_119'));
        session_start();
    }	

    public function _remap() {

        ///validaSession('procesos/evaluaciones/co_119');  
        
        $tipo=$this->uri->segment(4);

        if($tipo=='reDoc'){
            $this->_reDocsProv();
        } else if($tipo=='updaEstd'){
            $this->_updaEstd();
        } else {
            $this->_crud(); 
        }
        
    }        

    private function _crud(){
        
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/evaluaciones/co_119';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                             'css/datetimepicker/jquery.datetimepicker.css',
                             'css/orgChart/jquery.orgchart.css');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/evaluaciones/j_119.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/countdown/jquery.countdown.js',
                            'js/jquery.orgchart.js');

        $data['idFormulario'] = 'co_119';
        
        $cod_node = 152;
        $padre = $this->m_119->retParent($cod_node);
        
        $hijos = $this->getOrga($cod_node);
        
        $orga = array_merge($padre[0],array('nodo'=>$hijos));
        
//        ECHO '<PRE>';
//        PRINT_R($hijos);
//        ECHO '</PRE>'; 
//        die;
        
        $hijosHTML = $this->getOrgaHTML($hijos);
        $orgaHTML  = '<ul id="organisation" class="hide">';
            $orgaHTML .= '<li id="'.$padre[0]['cod_orga'].'">';
                $orgaHTML .= '<b>'.$padre[0]['des_orga'].'</b>';
                //$orgaHTML .= $this->m_119->viewEmple($padre[0]['cod_orga']);
                    $orgaHTML .= '<ul>';
                        $orgaHTML .= $hijosHTML;
                    $orgaHTML .= '</ul>';
            $orgaHTML .= '</li>';
        $orgaHTML .= '</ul>';
        
        $data['orga'] = $orga;
        $data['orgaHTML'] = $orgaHTML;
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/evaluaciones/v_119_1');
        $this->load->view('barras/footer');            
    }
    
    function getOrgaHTML($array,$arr=''){
        if(!empty($array)){
            for($i=0;$i<count($array); $i++){
                $arr.= '<li>';
                $arr .= '<b>'.$array[$i]['des_orga'].'</b>';
                //$arr .= $this->m_119->viewEmple($array[$i]['cod_orga']);
                if(!empty($array[$i]['nodo'])){
                    $arr .= '<ul>';
                    $arr = $this->getOrgaHTML($array[$i]['nodo'],$arr);
                    $arr .= '</ul>';
                }
                $arr .= '</li>';
            }
        }
        return $arr;
    }
    
    function getOrga($node){
        
        $arr = array();
        $array = $this->m_119->retNodes($node);
        if(!empty($array)){
            for($i=0;$i<count($array); $i++){
                $arr[] = array_merge($array[$i],array('nodo'=>$this->getOrga($array[$i]['cod_orga'])));
            }
        }
        return $arr;
    }
    
}