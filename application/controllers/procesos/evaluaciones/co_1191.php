<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_1191 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/evaluaciones/m_1191'));
        session_start();
    }	

    public function _remap() {

        ///validaSession('procesos/evaluaciones/co_1191');  
        
        $tipo = $this->uri->segment(4);
        if($tipo=='takEva'){
            $this->_tomar_evaluacion();
        } else if($tipo=='sv_tra'){
            $this->_save_respuestas();
        } else if($tipo=='ckEva'){
            $this->_retRespCke();
        } else {
            $this->_crud(); 
        }
    }        

    private function _crud(){
        
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/evaluaciones/co_1191';
        $data['urlControlador'] = $urlControlador;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/evaluaciones/j_1191.js',
                            'js/general.js');

        $data['idFormulario'] = 'co_1191';
        
        $evalus = $this->m_1191->retEvaluAsignadas();
        $data['evaluaciones'] = $evalus;
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/evaluaciones/v_1191');
        $this->load->view('barras/footer');            
    }
    
    private function _tomar_evaluacion(){
        
        $cod_eva = $this->input->post('co_ev');
        $est_eva = $this->input->post('es_ev');
        
        if ((isset($cod_eva) && !empty($cod_eva)) && (isset($est_eva) && !empty($est_eva)) ){
            
            $data = null; 
            $data['permisos']='';   
            $urlControlador = 'procesos/evaluaciones/co_1191';
            $data['urlControlador'] = $urlControlador;

            $data['js'] = array('js/jquery-1.10.2.min.js',
                                'js/bootstrap.js',
                                'js/procesos/evaluaciones/j_1191.js',
                                'js/general.js');

            $data['idFormulario'] = 'co_1191';
            $data['ejescript'] = false;
            
            $dat_pu = $this->m_1191->retPuntaje();
            $data['_punt'] = $dat_pu;

            $datos = $this->m_1191->retEvaluaId($cod_eva);
            $data['_eval'] = $datos;

            if(trim($est_eva) === 'A'){
                $mensaje = $this->m_1191->saveInicial($cod_eva);
            } else {
                $data['ejescript'] = true;
            }
            
            $data['cod_evalu_preg'] = $cod_eva;  
            
            $this->load->view('barras/header',$data);
            $this->load->view('barras/menu');
            $this->load->view('procesos/evaluaciones/v_1195');
            $this->load->view('barras/footer'); 

        } else {
            redirect(site_url('procesos/evaluaciones/co_1191'));
        }
        
    }
    
    private function _save_respuestas(){
        
        $cod_eva = $this->input->post('c_e');
        $det_pre = $this->input->post('d_p');
        $array_resp = json_decode($det_pre,true);
        $mensaje = $this->m_1191->saveRespuestas($cod_eva,$array_resp);
        echo $mensaje;
    }
    
    private function _retRespCke(){
        $cod_eva = $this->input->post('c_e');
        $mensaje = null;
        if(isset($_SESSION['c_e']) && !empty($_SESSION['c_e'])){
            $mensaje = $this->m_1191->getRespuestas($cod_eva,$_SESSION['c_e']);
        }
        echo json_encode($mensaje);
    }
    
}