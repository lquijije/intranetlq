<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_1192 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/evaluaciones/m_1192','proceso/evaluaciones/m_119'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/evaluaciones/co_1192');  
        
        $tipo=$this->uri->segment(4);
        
        if($tipo == 'sv_tra'){
            $this->_save_pregunta();
        } else if($tipo=='retTrev'){
            $this->_retTreview();
        } else if($tipo=='retPre'){
            $this->_retPre();
        } else {
            $this->_crud(); 
        }
        
    }        

    private function _crud(){
        
        $id_met = 0;
        
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/evaluaciones/co_1192';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                             'css/datetimepicker/jquery.datetimepicker.css',
                             'css/treeview/default/style.css');
        
                            
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/evaluaciones/j_1192.js',
                            'js/general.js',
                            'js/plugins/treeview/jstree.min.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js');

        $data['idFormulario'] = 'co_1192';
        
        $metodos = $this->m_1192->retMetodos(); 
        
        $cod_m = 0;
        
        $datosParametro = null;
        if (!empty($metodos)){    
                $datosParametro['NNN'] = 'Seleccione un M&eacute;todo';
            foreach ($metodos as $row){ 
                $cod_m = (int)$metodos[0]['cod_met'];
                $datosParametro[$row['cod_met']] = $row['des_met'];                                     
            }
        } else {
            $datosParametro['NNN'] = 'No Existen M&eacute;todos';    
        }
        $atributosEspecial = "id='cmb_met' class='form-control'";
        $data['cmb_met'] = form_dropdown('cmb_met',$datosParametro,'NNN',$atributosEspecial);
        
        $id_met = isset($_POST['cmb_met_sea']) && !empty($_POST['cmb_met_sea'])?$_POST['cmb_met_sea']:$cod_m;
        
        $datosParametro = null;
        if (!empty($metodos)){    
            foreach ($metodos as $row){    
                $datosParametro[$row['cod_met']] = $row['des_met'];                                     
            }
        } else {
            $datosParametro['NNN'] = 'No Existen M&eacute;todos';    
        }
        $atributosEspecial = "id='cmb_met_sea' class='form-control' onchange='this.form.submit()' ";
        $data['cmb_met_sea'] = form_dropdown('cmb_met_sea',$datosParametro,$id_met,$atributosEspecial);
        
        
        $preguntas = $this->m_1192->retPreguntas('',$id_met); 
        $data['preguntas'] = $preguntas;
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/evaluaciones/v_1192');
        $this->load->view('barras/footer');            
    }
    
    function _retPre (){
        $cod_pre = $this->input->post('c_p');
        $preguntas = $this->m_1192->retPreguntas($cod_pre); 
        echo json_encode(array('data'=>$preguntas));
    }
    
    function getTreview($node){
        
        $arr = array();
        $array = $this->m_119->retNodes($node);
        if(!empty($array)){
            for($i=0;$i<count($array); $i++){
                $arr[] = array_merge($array[$i],array('nodo'=>$this->getOrga($array[$i]['cod_orga'])));
            }
        }
        return $arr;
    }
    
    private function _save_pregunta(){
        $datos = $this->input->post();
        $mensaje = $this->m_1192->save_pregunta($datos);
        echo $mensaje;
    }
    
    private function _retTreview(){
        $mensaje = $this->m_1192->retTreview();
        echo $mensaje;
    } 

}