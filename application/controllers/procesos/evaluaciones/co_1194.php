<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_1194 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/evaluaciones/m_1194','proceso/evaluaciones/m_119'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/evaluaciones/co_1194');  
        
        $tipo=$this->uri->segment(4);
        
        if($tipo == 'sv_tra'){
            $this->_save_evaluacion();
        } else if($tipo=='retTrev'){
            $this->_retTreview();
        } else if($tipo=='retPre'){
            $this->_retPre();
        } else if($tipo=='retEva'){
            $this->_retEva();
        } else {
            $this->_crud(); 
        }
        
    }        

    private function _crud(){
        
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/evaluaciones/co_1194';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                             'css/datetimepicker/jquery.datetimepicker.css',
                             'css/treeview/default/style.css',
                             'js/plugins/ckeditor/samples/sample.css');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/evaluaciones/j_1194.js',
                            'js/general.js',
                            'js/plugins/treeview/jstree.min.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/ckeditor/ckeditor.js');

        $data['idFormulario'] = 'co_1194';
        
        $evalua = $this->m_1194->retEvalua(); 
        $data['evaluaciones'] = $evalua;
        
        $metodos = $this->m_1194->retMetodos(); 
            
        $datosParametro = null;
        if (!empty($metodos)){    
                $datosParametro['NNN'] = 'Seleccione un M&eacute;todo';
            foreach ($metodos as $row){    
                $datosParametro[$row['cod_met']] = $row['des_met'];                                     
            }
        } else {
            $datosParametro['NNN'] = 'No Existen M&eacute;todos';    
        }
        $atributosEspecial = "id='cmb_met' class='form-control'";
        $data['cmb_met'] = form_dropdown('cmb_met',$datosParametro,'NNN',$atributosEspecial);   
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/evaluaciones/v_1194');
        $this->load->view('barras/footer');            
    }
    
    function _retPre(){
        $cod_pre = $this->input->post('i_m');
        $preguntas = $this->m_1194->retPreguntas($cod_pre); 
        echo json_encode($preguntas);
    }
    
    function _retEva(){
        $cod_eva = $this->input->post('c_e');
        $mensaje = $this->m_1194->retEvaluaId($cod_eva); 
        echo json_encode($mensaje);
    }
    
    function getTreview($node){
        
        $arr = array();
        $array = $this->m_119->retNodes($node);
        if(!empty($array)){
            for($i=0;$i<count($array); $i++){
                $arr[] = array_merge($array[$i],array('nodo'=>$this->getOrga($array[$i]['cod_orga'])));
            }
        }
        return $arr;
    }
    
    private function _save_evaluacion(){
        $datos = $this->input->post();
        $mensaje = $this->m_1194->save_evaluacion($datos);
        echo $mensaje;
    }
    
    private function _retTreview(){
        $mensaje = $this->m_1194->retTreview();
        echo $mensaje;
    } 

}