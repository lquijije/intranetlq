<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_1193 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/evaluaciones/m_1193','proceso/evaluaciones/m_119'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/evaluaciones/co_1193');  
        
        $tipo=$this->uri->segment(4);
        
        if($tipo == 'sv_tra'){
            $this->_save_meto();
        } else if($tipo=='retMet'){
            $this->_retMet();
        } else {
            $this->_crud(); 
        }
        
    }        

    private function _crud(){
        
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/evaluaciones/co_1193';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css');
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/evaluaciones/j_1193.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js');

        $data['idFormulario'] = 'co_1193';
        
        $metodos = $this->m_1193->retMetodos(); 
        $data['metodos'] = $metodos;
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/evaluaciones/v_1193');
        $this->load->view('barras/footer');            
    }
    
    function _retMet (){
        $cod_met = $this->input->post('c_m');
        $metodo = $this->m_1193->retMetodos($cod_met); 
        echo json_encode(array('data'=>$metodo));
    }
    
    private function _save_meto(){
        $datos = $this->input->post();
        $mensaje = $this->m_1193->save_pregunta($datos);
        echo $mensaje;
    }

}