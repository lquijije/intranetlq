<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_111 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        session_start();
        $this->load->helper(array('form'));
        $this->load->model(array('proceso/reclamos/m_111','proceso/reclamos/m_113'));
        $this->load->library(array('mailer'));
    }	
    public function _remap()  {    
        
        validaSession('procesos/reclamos/co_111'); 
        
        $tipo=$this->uri->segment(4);
        
        if($tipo=='retClient'){
            $this->_retCliente();
        } else if($tipo=='retFact'){
            $this->_retFact();
        } else if($tipo=='retDetFact'){
            $this->_retDetFact();
        } else if($tipo=='saveReclamo'){
            $this->_saveReclamo();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud($array=null){

        $data = null; 
        $urlControlador = 'procesos/reclamos/co_111';
        $data['urlControlador'] = $urlControlador;
        
        $data['array'] = $array;
        
        $monthNames = Array();
        $monthNames[1]= "Enero";
        $monthNames[2]= "Febrero";
        $monthNames[3]= "Marzo";
        $monthNames[4]= "Abril";
        $monthNames[5]= "Mayo";
        $monthNames[6]= "Junio";
        $monthNames[7]= "Julio";
        $monthNames[8]= "Agosto";
        $monthNames[9]= "Septiembre";
        $monthNames[10]= "Octubre";
        $monthNames[11]= "Noviembre";
        $monthNames[12]= "Diciembre";
        
        $data['css'] =  array('css/datatables/dataTables.bootstrap.css',
                            'js/plugins/select2/dist/css/select2.css',
                            'css/datetimepicker/jquery.datetimepicker.css');
            
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/reclamos/j_111.js',
                            'js/plugins/select2/dist/js/select2.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/general.js');
        
        
        $val_e_mail = $this->general_model->valid_e_mail($_SESSION['c_e']);

        if((int)trim($val_e_mail) === 1){
            $this->session->set_flashdata('error',"Para poder ingresar un reclamo debe actualizar su 'E-Mail'. ");
            redirect(site_url('login/c_pass?e_m=P'));
        }
        
        $empresas = $this->m_111->retEmpresaAsignadas();
        
        $data['empresas'] = $empresas;
        
        if (isset($empresas)){
                
            for ($i = 0; $i < count($empresas); $i++){
                if($i === 0) {
                    $p = $empresas[$i]['cod_bdg'];
                }
            }

        } else {
            $p = $q = '';
        }
        
        $get = $this->input->post('cod_emp');
        $cod_empr = !empty($get)?$get:$p;
        $_SESSION['cod_almacen'] = $cod_empr;
                
        $datosParametro=null;
        for($i = 1 ; $i <= 31; $i++){
            $datosParametro[$i]=$i;
        }
        
        $atributosEspecial="id='cmb_dia' class='form-control'";
        $data['cmb_dia']=form_dropdown('cmb_dia',$datosParametro,date('d'),$atributosEspecial); 
        
        $atributosEspecial="id='cmb_mes' class='form-control'";
        $data['cmb_mes']=form_dropdown('cmb_mes',$monthNames,date('m'),$atributosEspecial); 
        
        $datosParametro=null;
        for($i = 2000; $i <= (int)date('Y'); $i++){
            $datosParametro[$i]=$i;
        }
        
        $atributosEspecial="id='cmb_anio' class='form-control'";
        $data['cmb_anio']=form_dropdown('cmb_anio',$datosParametro,date('Y'),$atributosEspecial); 
        
        $datosParametro=null;
        $start = "00:00";
        $end = "24:00";

        $tStart = strtotime($start);
        $tEnd = strtotime($end);
        $tNow = $tStart;

        while($tNow <= $tEnd){
            $datosParametro[date("H:i",$tNow)]= date("H:i",$tNow); 
            $tNow = strtotime('+30 minutes',$tNow);
        }
        
        $atributosEspecial="id='cmb_hora' class='form-control'";
        $data['cmb_hora']=form_dropdown('cmb_hora',$datosParametro,date("H:i"),$atributosEspecial); 
                
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/reclamos/v_111'); 
        $this->load->view('barras/footer');
    }
    
    private function _retCliente(){
        $id = $this->input->post('c_c');
        $mensaje = $this->m_111->retCliente($id);
        echo json_encode($mensaje);
    }
    
    private function _retFact(){
        $datos = $this->input->post();
        $mensaje = $this->m_111->retFacturas($datos);
        echo json_encode($mensaje);
    }
    
    private function _retDetFact(){
        $datos = $this->input->post();
        $mensaje = $this->m_111->retDetFacturas($datos);
        echo json_encode($mensaje);
    }
    
    private function _saveReclamo(){
        $json = $this->input->post();
        $mensaje = $this->m_111->saveReclamo($json);
        $array_cab = json_decode($json['c_a'],true);
      
        if((int)$mensaje['data']['a'] === 2){
            $this->_MailAdminPycca($mensaje['data']['d']);
            if(trim($array_cab[0]['mail_cli']) <> ''){
                $this->_MailUserReclamo(trim($_SESSION['cod_almacen']),$mensaje['data']['c'],utf8_encode(trim($array_cab[0]['mail_cli'])),utf8_encode(trim($array_cab[0]['nom_cli'])));
            }
        }
        
        echo json_encode($mensaje);
    }
    
    private function _MailUserReclamo($cod_bdg,$cod_recl,$destinatario,$nombre){
        
        $datos = $this->m_113->retReclamos('',$cod_recl,$cod_bdg);
           
        $this->load->library('pdf');
        $pdf = new PDF();
        $pdf->fpdf('P','mm','A4');
        $pdf->SetMargins(5,5,5,5);
        $pdf->AliasNbPages();
        $pdf->setHeader('1.1.1');
        $pdf->AddPage();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetTextColor(0,0,0);

        $pdf->Ln(8);

        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Cell(160, 12, utf8_decode(" ALMACÉN ").utf8_encode($datos['data']['cab'][0]['nom_alm']),1, 0,'C','true');
        $pdf->Cell(40, 12, '# '.utf8_encode($datos['data']['cab'][0]['cod_rec']),1, 0,'C','true');
        $pdf->Ln(12);
        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(20, 5, utf8_decode(" Fecha:"),1, 0,'L','true');
        $pdf->Cell(15, 5, utf8_decode(" Día:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, date('d',strtotime($datos['data']['cab'][0]['fec_rec'])),1, 0,'C','true');

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(15, 5, utf8_decode(" Mes:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, date('m',strtotime($datos['data']['cab'][0]['fec_rec'])),1, 0,'C','true');

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(15, 5, utf8_decode(" Año:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, date('Y',strtotime($datos['data']['cab'][0]['fec_rec'])),1, 0,'C','true');

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(15, 5, utf8_decode(" Hora:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, date('H:i',strtotime($datos['data']['cab'][0]['fec_rec'])),1, 0,'C','true');

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(20, 5, utf8_decode(" Código:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, $datos['data']['cab'][0]['cod_alm'],1, 0,'C','true');

        $pdf->Ln(5); 

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(200, 8, utf8_decode(" 1. Identificación del Proveedor"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);

        $pdf->Ln(8);

        $pdf->Cell(120, 5, utf8_decode(" Nombre de la persona natural o razón social de la persona jurídica (proveedor): "),1, 0,'L','true');
        $pdf->Cell(80, 5, utf8_encode($datos['data']['cab'][0]['nom_alm']),1, 0,'L','true');

        $pdf->Ln(5); 

        $pdf->Cell(120, 5, utf8_decode(" Dirección del domicilio del establecimiento donde se coloca el libro de reclamos: "),1, 0,'L','true');
        $pdf->Cell(80, 5, utf8_encode($datos['data']['cab'][0]['dir_alm']),1, 0,'L','true');

        $pdf->Ln(5); 

        $pdf->Cell(120, 5, utf8_decode(" Número de R.U.C. o R.I.S.E. del proveedor: "),1, 0,'L','true');
        $pdf->Cell(80, 5, utf8_encode($datos['data']['cab'][0]['ruc_alm']),1, 0,'L','true');

        $pdf->Ln(5); 

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(200, 8, utf8_decode(" 2. Identificación de la persona consumidora"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);

        $pdf->Ln(8); 

        $pdf->Cell(35, 5, utf8_decode(" Nombre y Apellidos: "),1, 0,'L','true');
        $pdf->Cell(165, 5, utf8_encode($datos['data']['cab'][0]['nom_cli']),1, 0,'L','true');

        $pdf->Ln(5); 

        $pdf->Cell(35, 5, utf8_decode(" Dirección: "),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(165, 5, utf8_encode($datos['data']['cab'][0]['dir_cli']),1, 0,'L','true');

        $pdf->Ln(5); 

        $pdf->Cell(35, 5, utf8_decode(" Identificación: "),1, 0,'L','true');
        $pdf->Cell(65, 5, trim($datos['data']['cab'][0]['id_cli']),1, 0,'L','true');

        $pdf->Cell(35, 5, utf8_decode(" Teléfono Convencional: "),1, 0,'L','true');
        $pdf->Cell(65, 5, utf8_encode($datos['data']['cab'][0]['tel_con']),1, 0,'L','true');

        $pdf->Ln(5); 

        $pdf->Cell(35, 5, utf8_decode(" Teléfono Celular: "),1, 0,'L','true');
        $pdf->Cell(65, 5, utf8_encode($datos['data']['cab'][0]['tel_cel']),1, 0,'L','true');

        $pdf->Cell(35, 5, utf8_decode(" Dirección Electrónica: "),1, 0,'L','true');
        $pdf->Cell(65, 5, utf8_encode($datos['data']['cab'][0]['ema_cli']),1, 0,'L','true');

        $pdf->Ln(5); 

        $pdf->Cell(65, 5, utf8_decode(" Forma en que desea recibir la respuesta a este reclamo: "),1, 0,'L','true');
        $pdf->Cell(35, 5, utf8_decode(" En su dirección electronica"),1, 0,'C','true');
        $pdf->Cell(10, 5, (int)$datos['data']['cab'][0]['r_mai'] === 1 ?'X':'' ,1, 0,'C','true');
        $pdf->Cell(35, 5, utf8_decode(" En su teléfono convencional"),1, 0,'C','true');
        $pdf->Cell(10, 5, (int)$datos['data']['cab'][0]['r_con'] === 1 ?'X':'' ,1, 0,'C','true');
        $pdf->Cell(35, 5, utf8_decode(" En su teléfono celular "),1, 0,'C','true');
        $pdf->Cell(10, 5, (int)$datos['data']['cab'][0]['r_cel'] === 1 ?'X':'' ,1, 0,'C','true');

        $pdf->Ln(5); 

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(200, 8, utf8_decode(" 3. Identificación del bien o servicio"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);

        $pdf->Ln(8); 
        //
        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(40, 5, utf8_decode(" Servicio"),1, 0,'L','true');
        $pdf->Cell(10, 5, trim($datos['data']['cab'][0]['tip_rec']) === 'S' ?'X':'',1, 0,'C','true');
        $pdf->Cell(40, 5, utf8_decode(" Bien / Producto"),1, 0,'L','true');
        $pdf->Cell(10, 5, trim($datos['data']['cab'][0]['tip_rec']) === 'B' ?'X':'' ,1, 0,'C','true');
        $pdf->Cell(60, 5, utf8_decode(" No. De Factura / Nota de Venta"),1, 0,'L','true');
        $pdf->Cell(40, 5, trim(utf8_decode($datos['data']['cab'][0]['num_fac'])),1, 0,'C','true');
        $pdf->Ln(5); 

        if(trim($datos['data']['cab'][0]['tip_rec']) === 'S'){

            $pdf->SetFont('helvetica', '', 7);
            $pdf->Cell(200, 5, utf8_decode(" Descripción del servicio:"),1, 0,'L','true');
            $pdf->Ln(5);
            $pdf->SetFont('helvetica', '', 7);
            $pdf->MultiCell(200,5, utf8_decode($datos['data']['cab'][0]['des_abr']),1,1);   

        } else {

            $pdf->SetFont('helvetica', '', 7);
            $pdf->Cell(200, 5, utf8_decode(" Detalle de Productos"),1, 0,'C','true');
            $pdf->Ln(5);
            $pdf->Cell(20, 5, utf8_decode(" Código"),1, 0,'C','true');
            $pdf->Cell(60, 5, utf8_decode(" Nombre"),1, 0,'C','true');
            $pdf->Cell(120, 5, utf8_decode(" Descripción"),1, 0,'C','true');
            $pdf->Ln(5);

            for ($i = 0; $i<count($datos['data']['cab']); $i++){

                $pdf->Cell(20, 5, utf8_decode($datos['data']['cab'][$i]['cod_art']),1, 0,'C','true');
                $pdf->Cell(60, 5, utf8_decode($datos['data']['cab'][$i]['nom_art']),1, 0,'C','true');
                $pdf->Cell(120, 5, utf8_decode($datos['data']['cab'][$i]['des_art']),1, 0,'L','true');
                $pdf->Ln(5);

            }

        }

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(200, 8, utf8_decode(" 4. Identificación del reclamo"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);

        $pdf->Ln(8); 

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(60, 5, utf8_decode(" Fecha de Inconveniente:"),1, 0,'L','true');
        $pdf->Cell(15, 5, utf8_decode(" Día:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, date('d',strtotime($datos['data']['cab'][0]['fec_inc'])),1, 0,'C','true');

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(15, 5, utf8_decode(" Mes:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, date('m',strtotime($datos['data']['cab'][0]['fec_inc'])),1, 0,'C','true');

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(15, 5, utf8_decode(" Año:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, date('Y',strtotime($datos['data']['cab'][0]['fec_inc'])),1, 0,'C','true');

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(15, 5, utf8_decode(" Hora:"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(20, 5, date('H:i',strtotime($datos['data']['cab'][0]['fec_inc'])),1, 0,'C','true');

        $pdf->Ln(5);
        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(200, 5, utf8_decode(" Descripción clara y concisa del reclamo:"),1, 0,'L','true');
        $pdf->Ln(5);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->MultiCell(200,5, utf8_decode($datos['data']['cab'][0]['des_cla']),1,1);   
        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(200, 5, utf8_decode(" Resultado que pretende obtener con el reclamo:"),1, 0,'L','true');
        $pdf->Ln(5);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->MultiCell(200,5, utf8_decode($datos['data']['cab'][0]['dec_obj']),1,1);

        $pdf->SetFont('helvetica', 'B', 7);
        $pdf->Cell(200, 8, utf8_decode(" 5. Acciones adoptadas por el proveedor"),1, 0,'L','true');
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Ln(8); 

        if (!empty($datos['data']['det'])){

            for ($a = 0; $a<count($datos['data']['det']); $a++){
                $pdf->MultiCell(200,5, date('Y/m/d',strtotime($datos['data']['det'][$a]['fec_acc']))." - ".utf8_decode($datos['data']['det'][$a]['des_det']),1,1);
            }

        } else {

            $pdf->MultiCell(200,5, utf8_decode('Solicitud de Reclamo en espera de acciones.'),1,1);

        }

        $pdf->Ln(5); 
        $pdf->Cell(35, 20,'',0, 0,'L','true');
        $pdf->Cell(55, 20,'',1, 0,'L','true');
        $pdf->Cell(25, 20,'',0, 0,'L','true');
        $pdf->Cell(55, 20,'',1, 0,'L','true');
        $pdf->Ln(20);
        $pdf->Cell(35);
        $pdf->Cell(55, 5,'Firma de la persona Consumidora',1, 0,'C','true');
        $pdf->Cell(25);
        $pdf->Cell(55, 5,'Firma / Sello Proveedor',1, 0,'C','true');

        $attachment= $pdf->Output('Solicitud_reclamo_'.utf8_encode($datos['data']['cab'][0]['id_cli']).'.pdf', 'S');

	$host = "130.1.10.232";
	$puerto = "25";		
	$usuario = "sqlmail@pycca.com";		
	$clave = "Sqlma1l";
	$responder_a = "sqlmail@pycca.com";
        
        $asunto = "Solicitud de Reclamo";
        $cuerpo = '<table cellpadding="0" cellspacing="0" style="width:100%;border-top:5px solid #F0514A;padding:30px 0;">
            <tbody>
                <tr>
                    <td>
                        <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="ecxlayout">
                            <tbody>
                                <tr>
                                    <td height="10">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table border="0" width="121" align="left" cellpadding="0" cellspacing="0" class="ecxlogo">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="ecxlogo_place"><img src="http://www.pycca.com/images/logos/1/logo-pycca.png" alt="vive1" width="150" height="40"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                            <tbody>
                                                <tr>
                                                    <td class="ecxaction">
                                                        <table width="600" border="0" cellspacing="0" cellpadding="0" class="ecxaction_text" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-size:18px;color:#444;line-height:35px;font-weight:lighter;">
                                                                    <p>Estimado '.$nombre.'</p>
                                                                    <p>PYCCA se dirige a usted para enviarle su solicitud de reclamo y aprovechar la oportunidad para agradecerle profundamente la preferencia que ha mostrado por nuestros servicios. </p>
                                                                    <p>Nos despedimos de usted, no sin antes reiterarle nuestro agradecimiento y profundo compromiso con su bienestar.</p>
                                                                    <p>Saludos cordiales.</p> 
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="50" class="ecxspace">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#323f4e">
                            <tbody>
                                <tr>
                                    <td valign="middle" style="border-top:5px solid #F0514A;background-color:#00539F;">
                                        <table border="0" cellspacing="0" cellpadding="0" align="center" class="footer">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table width="250" border="0" cellspacing="0" cellpadding="0" align="left" class="ecxcopyright">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="40" valign="middle" style="font-size:14px;color:#FFF;">&copy; 2015 Webpycca</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>';
        
        $cabecera= "MIME-Version: 1.0\r\n";
        $cabecera .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
        $cabecera .= "From: Webpycca\r\n";
        
        $mail = new PHPMailer();
        $mail->IsSMTP(); 
        $mail->PluginDir = "";
        $mail->Host = $host;
        $mail->Port=$puerto;
        $mail->SMTPAuth = true;
        $mail->Username = $usuario;
        $mail->Password = $clave;
        $mail->From = $responder_a;
        $mail->FromName = "WebPycca";
        $mail->WordWrap = 50;
        $mail->Subject = $asunto;

        $mail->AddAddress($destinatario);
        $mail->addCC('fvera@pycca.com');
        $mail->addCC('gjalil@pycca.com');
        $mail->Body = $cuerpo;
        
        $mail->AddStringAttachment($attachment,'Solicitud_reclamo_'.$cod_recl.'.pdf');
        $mail->IsHTML(true);
        
        if(!$mail->Send()){
            echo "ERROR: " . $mail->ErrorInfo; 	
        } else {
           return "OK";	
        }	        
            
    }
    
    private function _MailAdminPycca($almacen){
        
	$host = "130.1.10.232";
	$puerto = "25";		
	$usuario = "sqlmail@pycca.com";		
	$clave = "Sqlma1l";
	$responder_a = "sqlmail@pycca.com";
        $array = $this->m_111->retGrupoUsuariosAlmacen($_SESSION['cod_almacen']);
        
        $asunto = "Solicitud de Reclamo";
        $cuerpo = '<table cellpadding="0" cellspacing="0" style="width:100%;border-top:5px solid #F0514A;padding:30px 0;">
            <tbody>
                <tr>
                    <td>
                        <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="ecxlayout">
                            <tbody>
                                <tr>
                                    <td height="10">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table border="0" width="121" align="left" cellpadding="0" cellspacing="0" class="ecxlogo">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="ecxlogo_place"><img src="http://www.pycca.com/images/logos/1/logo-pycca.png" alt="vive1" width="150" height="40"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                            <tbody>
                                                <tr>
                                                    <td class="ecxaction">
                                                        <table width="450" border="0" cellspacing="0" cellpadding="0" class="ecxaction_text" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-size:25px;color:#444;line-height:35px;font-weight:lighter;">&iexcl;Se ingres&oacute; una nueva Solicitud de reclamo en el almac&eacute;n '.$almacen.'.<br>Se espera sus acciones. En la siguiente direcci&oacute;n <a href="'.site_url('procesos/reclamos/co_112').'">'.site_url('procesos/reclamos/co_112').'</a>&iexcl;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="50" class="ecxspace">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#323f4e">
                            <tbody>
                                <tr>
                                    <td valign="middle" style="border-top:5px solid #F0514A;background-color:#00539F;">
                                        <table border="0" cellspacing="0" cellpadding="0" align="center" class="footer">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table width="250" border="0" cellspacing="0" cellpadding="0" align="left" class="ecxcopyright">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="40" valign="middle" style="font-size:14px;color:#FFF;">&copy; 2015 Webpycca</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>';
        
        $cabecera= "MIME-Version: 1.0\r\n";
        $cabecera .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
        $cabecera .= "From: Webpycca\r\n";
        
        $mail = new PHPMailer();
        $mail->IsSMTP(); 
        $mail->PluginDir = "";
        $mail->Host = $host;
        $mail->Port=$puerto;
        $mail->SMTPAuth = true;
        $mail->Username = $usuario;
        $mail->Password = $clave;
        $mail->From = $responder_a;
        $mail->FromName = "WebPycca";
        $mail->WordWrap = 50;
        $mail->Subject = $asunto;

        if (!empty($array)){
            foreach ($array as $row){
                $mail->AddAddress($row['mail']);
            }
        }
        
        $mail->Body = $cuerpo;
        $mail->IsHTML(true);
        
        if(!$mail->Send()){
            echo "ERROR: " . $mail->ErrorInfo; 	
        } else {
           return "OK";	
        }	        
            
    }
    
}