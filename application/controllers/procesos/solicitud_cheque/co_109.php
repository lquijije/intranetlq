<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_109 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        session_start();
        $this->load->helper(array('form'));
        $this->load->model(array('proceso/solicitud_cheque/m_109'));
        $this->load->library(array('mailer'));
    }	

    public function _remap()  {    
        
        validaSession('procesos/solicitud_cheque/co_109');  
        
        $tipo=$this->uri->segment(4);
        
        if($tipo=='retCeCosto'){
            $this->_retCeCosto();
        } else if($tipo=='retCuentas'){
            $this->_retCuentas();
        } else if($tipo=='retProvee'){
            $this->_retProvee();
        } else if($tipo=='retDocPen'){
            $this->_retDocPen();
        } else if($tipo=='retDoc'){
            $this->_retDoc();
        } else if($tipo=='retOc'){
            $this->_retOc();
        } else if($tipo=='saveSolc'){
            $this->_saveSolc();
        } else if($tipo=='retPlaImp'){
            $this->_retPlaImp();
        } else if($tipo=='retDisxm2'){
            $this->_retDisxm2();
        } else if($tipo=='retDisCeCos'){
            $this->_retDisCeCos();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud($array=null){
        
        $data = null; 
        $urlControlador = 'procesos/solicitud_cheque/co_109';
        $data['urlControlador'] = $urlControlador;
        $bandera = true;
        
        $data['array'] = $array;
        
        $data['css'] =  array('css/datatables/dataTables.bootstrap.css',
                            'js/plugins/select2/dist/css/select2.css',
                            'css/datetimepicker/jquery.datetimepicker.css');
            
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/solicitud_cheque/j_109.js',
                            'js/plugins/select2/dist/js/select2.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/general.js');
        
        $site = "temp/json/".$_SESSION['c_r']."-39.json";
        
        if (file_exists($site)) {
            
            $bandera = true;
            $string = file_get_contents($site);
            $json_array = json_decode($string, true);
            $empresas = isset($json_array['master']['39']['empresas'])?$json_array['master']['39']['empresas']:array();
            
            if (isset($empresas)){
                
                for ($i = 0; $i < count($empresas); $i++){
                    if($i === 0) {
                        $p = $empresas[$i]['codi_empr'];
                        $q = $empresas[$i]['server_solicitudes'];
                        $r = $empresas[$i]['base_dato'];
                        $s = $empresas[$i]['codi_empr_NSIP'];
                        $t = $empresas[$i]['indi_empr'];
                        $u = $empresas[$i]['nomb_empr'];
                    }
                }

            }
            
        } else {
            
            $bandera = false;
            $empresas = 'NN';
            $p = $q = $r = $s = $t = $u ='';
            
        }
        
        $get = $this->input->post('cod_emp');
        $get_id = $this->input->post('id_emp');
        
        if(!empty($get)){
            if (isset($empresas)){

                for ($i = 0; $i < count($empresas); $i++){
                    if($i === (int)$get_id) {
                        $get_srv = $empresas[$i]['server_solicitudes'];
                        $get_bas = $empresas[$i]['base_dato'];
                        $get_emp_nsip = $empresas[$i]['codi_empr_NSIP'];
                        $ind_emp = $empresas[$i]['indi_empr'];
                        $nomb_emp = $empresas[$i]['nomb_empr'];
                    }
                }

            }
        } else {
            $get_id =0;
        }   
        
        $getEmpre = !empty($get)?$get:$p;
        $getSrv = !empty($get_srv)?$get_srv:$q;
        $getBas = !empty($get_bas)?$get_bas:$r;
        $get_Emp_Nsip = !empty($get_emp_nsip)?$get_emp_nsip:$s;
        $get_IndEmp = !empty($ind_emp)?$ind_emp:$t;
        $nomb_IndEmp = !empty($nomb_emp)?$nomb_emp:$u;
        
        $data['empresas'] = $empresas; 
        
        $_SESSION['id_emp']=$get_id;
        $_SESSION['nomb_emp']=$nomb_IndEmp;
        $_SESSION['co_emp']=$getEmpre;
        $_SESSION['ba_emp']=$getBas;
        $_SESSION['sv_emp']=$getSrv;
        $_SESSION['ind_emp']=$get_IndEmp;
        $_SESSION['id_emp_sip']=$get_Emp_Nsip;
        
        $fecha = '';
        $hora = '';
        $data['fecha'] = $fecha;
        $data['hora'] = $hora;
        
        if($bandera){
            $validaOC = $this->m_109->valiOrdCom();
            $centr_cost = $this->_retCeCosto($getEmpre);  
            $activ_pasiv = $this->_retActivoPasivo($getEmpre);  
        } else {
            $validaOC= null;
            $centr_cost = null;
            $activ_pasiv = null;
        }
        
        $data['o_c']=$validaOC;
        
        $tip_docu = $this->m_109->retTipDocu(); 
        $tip_egre = $this->m_109->retTipEgre();
        $autorizadores = $this->m_109->retAutorizadores(); 
        $valida_aproba = $this->m_109->retAccSolCodEmp();
        
        $data['val_sin_aprob'] = $valida_aproba;
        $data['autorizadores'] = $autorizadores;
        //die;
        $datosParametro=null;
        if (!empty($tip_docu)){
                $datosParametro['NNN']='Seleccione un Tipo de Documento';
            foreach ($tip_docu as $row){
                $datosParametro[$row['cod_doc']]=$row['cod_doc']." - ".$row['nom_doc'];                                     
            }
        } else {
            $datosParametro['NNN']='No Existen Tipos de Documento';    
        }
        
        $atributosEspecial="id='cmb_tip_doc' class='form-control input-sm'";
        $data['cmb_tip_doc']=form_dropdown('cmb_tip_doc',$datosParametro,'NNN',$atributosEspecial); 
        
        $datosParametro=null;
        if (!empty($tip_egre)){
                $datosParametro['NNN']='Seleccione un Tipo de Egreso';
            foreach ($tip_egre as $row){
                $datosParametro[$row['cod_mvt']]=$row['nom_mvt'];                                     
            }
        } else {
            $datosParametro['NNN']='No Existen Tipos de Egreso';    
        }
        
        $atributosEspecial="id='cmb_tip_egre' class='form-control input-sm'";
        $data['cmb_tip_egre']=form_dropdown('cmb_tip_egre',$datosParametro,'NNN',$atributosEspecial); 
        
        $datosParametro=null;
        if (!empty($centr_cost)){
                $datosParametro['NNN']='Seleccione un Centro de Costo';
            foreach ($centr_cost as $row){
                $datosParametro[$row['CODI_CECO']]=$row['NOMB_CECO'];                                     
            }
        } else {
            $datosParametro['NNN']='No Existen Centro de Costo Asignadas';    
        }
        $atributosEspecial="id='cmb_cen_cost' class='form-control input-sm'";
        $data['cmb_cen_cost']=form_dropdown('cmb_cen_cost',$datosParametro,'NNN',$atributosEspecial);      
        
        $datosParametro=null;
        if (!empty($activ_pasiv)){    
                $datosParametro['NNN']='Seleccione un Cuenta';
            foreach ($activ_pasiv as $row){    
                $datosParametro[$row['cta_myor'].'-'.$row['codi_ceco'].'-'.$row['cta_scta']]=$row['actdescr'];                                     
            }
        } else {
            $datosParametro['NNN']='No Existen Cuentas';    
        }
        $atributosEspecial="id='cmb_activoPasivo' class='form-control'";
        $data['cmb_activoPasivo']=form_dropdown('cmb_activoPasivo',$datosParametro,'NNN',$atributosEspecial);      
                
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/solicitud_cheque/v_109'); 
        $this->load->view('barras/footer');
    }
    
    private function _retOc(){
        $data = $this->input->post();
        $mensaje = $this->m_109->retOrdCom($data);
        echo $mensaje;
    }
    
    private function _retCeCosto($cod_empresa){
        $string = file_get_contents("temp/json/".$_SESSION['c_r']."-39.json");
        $json_array = json_decode($string, true);
        $centro_costo = isset($json_array['master']['39']['centro_costo'])?$json_array['master']['39']['centro_costo']:array();
        $array = null;
        foreach($centro_costo as $row){
            if (trim($row['CODI_EMPR']) === trim($cod_empresa)){
                $array[] =$row;
            }
        }
        
        return $array;
    }
    
    private function _retActivoPasivo($cod_empr){
        $string = file_get_contents("temp/json/".$_SESSION['c_r']."-39.json");
        $json_array = json_decode($string, true);
        
        $activoPasivo = isset($json_array['master']['39']['activoPasivo'])?$json_array['master']['39']['activoPasivo']:array();
        $array = null;
        
        foreach($activoPasivo as $row){
            if (trim($row['codi_empr']) === trim($cod_empr)){
                $array[] = $row;
            }
        }
        
        return $array;
    }
    
    private function _retCuentas($get=null){
        
        $cod_empr =$this->input->post('c_e');
        $cod_ceco =$this->input->post('c_c');
        $todos =$this->input->post('t_s');
        $bandera = true;
        
        $string = file_get_contents("temp/json/".$_SESSION['c_r']."-39.json");
        $json_array = json_decode($string, true);
        
        $cuentas = isset($json_array['master']['39']['cuentas'])?$json_array['master']['39']['cuentas']:array();
        $activoPasivo = isset($json_array['master']['39']['activoPasivo'])?$json_array['master']['39']['activoPasivo']:array();
        
        $array = null;
        
        if (!empty($todos) || !empty($get)){
            $bandera = true;
        } else {
            $bandera = false;
        }
        
        if ($bandera === false){
            foreach($cuentas as $row){
                if (trim($row['codi_empr']) === trim($cod_empr) && trim($row['codi_ceco']) === trim($cod_ceco)){
                    $array[] =$row;
                }
            }
            
        } else {
            
            foreach($cuentas as $row){
                if (trim($row['codi_empr']) === trim($_SESSION['co_emp'])){
                    $array[] =$row;
                }
            }
            
            foreach($activoPasivo as $row){
                if (trim($row['codi_empr']) === trim($_SESSION['co_emp'])){
                    $array[] = $row;
                }
            }
            
        }
        if (empty($get)){
            echo json_encode(array('cuentas'=>$array));
        } else {
            return $array;
        }
    }
    
    private function _retProvee(){
        $data = $this->input->post();
        $mensaje = $this->m_109->retProveedor($data);
        echo $mensaje;
    }
    
    private function _retDocPen(){
        $data = $this->input->post();
        $mensaje = $this->m_109->retDocPen($data);
        echo $mensaje;
    }
    
    private function _retPlaImp(){
        $data = $this->input->post('p_i');
        $mensaje = $this->m_109->retPlanImp($data);
        echo $mensaje;
    }
    
    private function _retDisxm2(){
        $c_m = $this->input->post('c_m');
        
        if(!empty($c_m)){
            
            $array = explode('-',$c_m);
            $mensaje = $this->m_109->retDistribucionxm2($array[0],$array[2]);
            echo $mensaje;
        
        } else {
            echo 'No ha seleccionado una cuenta';
        }
    }
    
    private function _retDisCeCos(){
        $c_m = $this->input->post('c_m');
        
        if(!empty($c_m)){
            
            $array = explode('-',$c_m);
            $mensaje = $this->m_109->retDistribucioncc($array[0],$array[2]);
            echo $mensaje;
        
        } else {
            echo 'No ha seleccionado una cuenta';
        }
    }
    
    private function _saveSolc(){
        if(!empty($_SESSION)){
            $data = $this->input->post();
            $cuentas = $this->_retCuentas(1);
            $mensaje = $this->m_109->saveSolicitud($data,$cuentas);
            
            if ((int)$mensaje[0]['a'] === 0 && trim($mensaje[0]['c']) <> ''){
                $this->_enviarMail($mensaje[0]['c']);
            }
            
            echo json_encode($mensaje);
            
        }
    }
    
    private function _retDoc(){
        $data = $this->input->post();
        $array = $this->m_109->retDoc($data);
        
        $prod = $imp = $cab = '';
        $total = $totalImp_1 = $totalImp_2 = $totalGe = 0;
        
        for($i = 0; $i < count($array['cab']); $i++){    
            $cab.='<tr><td><b>Fecha Emisi&oacute;n</b></td>';
            $cab.='<td>'.date('Y/m/d',strtotime($array['cab'][$i]['e'])).'</td>';
            $cab.='<td><b>Estado</b></td>';
            $cab.='<td>'.$array['cab'][$i]['a'].'</td></tr>';
            
            $cab.='<tr><td><b>Documento</b></td>';
            $cab.='<td>'.$data['e_o']."-".$data['p_n']."-".$data['s_l'].'</td>';
            $cab.='<td><b>Numero Autorizaci&oacute;n</b></td>';
            $cab.='<td>'.$array['cab'][$i]['b'].'</td></tr>';
            
            $cab.='<tr><td><b>Mail</b></td>';
            $cab.='<td>'.$array['cab'][$i]['f'].'</td>';
            $cab.='<td><b>Fecha Autorizaci&oacute;n</b></td>';
            $cab.='<td>'.date('Y/m/d',strtotime($array['cab'][$i]['c'])).'</td></tr>';
            
            $cab.='<tr><td><b>Fecha Recepci&oacute;n</b></td>';
            $cab.='<td>'.date('Y/m/d',strtotime($array['cab'][$i]['g'])).'</td>';
            $cab.='<td><b>Clave Acceso</b></td>';
            $cab.='<td>'.$array['cab'][$i]['d'].'</td></tr>';
            
            $cab.='<tr><td><b>XML</b></td>';
            $cab.='<td>'.$array['cab'][$i]['i'].'</td>';
            $cab.='<td colspan="2"></td></tr>';
            
        }
        
        for($i = 0; $i < count($array['det']); $i++){    
            $total +=(float)$array['det'][$i]['h']; 
            $prod.='<tr><td>'.$array['det'][$i]['b'].'</td>';
            $prod.='<td>'.$array['det'][$i]['d'].'</td>';
            $prod.='<td class="derecha">'.(int)$array['det'][$i]['e'].'</td>';
            $prod.='<td class="derecha">'.$array['det'][$i]['f'].'</td>';
            $prod.='<td class="derecha">'.$array['det'][$i]['g'].'</td>';
            $prod.='<td class="derecha">'.$array['det'][$i]['h'].'</td></tr>';
        }
        
        $prod.='<tr><td colspan="5" class="derecha"><b>Total Productos</b></td><td class="derecha">'.$total.'</td></tr>';
       
        for($i = 0; $i < count($array['imp']); $i++){ 
            $totalImp_1 += (float)$array['imp'][$i]['f']; 
            $totalImp_2 += (float)$array['imp'][$i]['g']; 
            $imp.='<tr><td>'.$array['imp'][$i]['d']." ".$array['imp'][$i]['e'].'</td>';
            $imp.='<td class="derecha">'.$array['imp'][$i]['f'].'</td>';
            $imp.='<td class="derecha">'.$array['imp'][$i]['g'].'</td></tr>';
        }
        $totalGe = $totalImp_1 + $totalImp_2;
        $imp.='<tr><td class="derecha"><b>Total Impuestos</b></td><td class="derecha">'.(float)($totalImp_1).'</td><td class="derecha">'.(float)($totalImp_2).'</td></tr>';
        
        echo "OK-".json_encode(array(array('cab'=>$cab,'pro'=>$prod,'imp'=>$imp,'tot'=>$totalGe,'val_imp'=>$array['imp'])));
    }
    
    private function _enviarMail($destinatario){
        
	$host = "130.1.10.232";
	$puerto = "25";		
	$usuario = "sqlmail@pycca.com";		
	$clave = "Sqlma1l";
	$responder_a = "sqlmail@pycca.com";
        $asunto = "Solicitud de Cheque";
        $cuerpo = '<table cellpadding="0" cellspacing="0" style="width:100%;border-top:5px solid #F0514A;padding:30px 0;">
            <tbody>
                <tr>
                    <td>
                        <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="ecxlayout">
                            <tbody>
                                <tr>
                                    <td height="10">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table border="0" width="121" align="left" cellpadding="0" cellspacing="0" class="ecxlogo">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="ecxlogo_place"><img src="http://www.pycca.com/images/logos/1/logo-pycca.png" alt="vive1" width="150" height="40"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                            <tbody>
                                                <tr>
                                                    <td class="ecxaction">
                                                        <table width="450" border="0" cellspacing="0" cellpadding="0" class="ecxaction_text" align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-size:25px;color:#444;line-height:35px;font-weight:lighter;">&iexcl;Se ingres&oacute; una nueva Solicitud con caracter de URGENTE de la empresa '.$_SESSION['nomb_emp'].'.<br>Se espera su aprobaci&oacute;n. En la siguiente direcci&oacute;n <a href="'.site_url().'">'.site_url().'</a>&iexcl;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="50" class="ecxspace">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#323f4e">
                            <tbody>
                                <tr>
                                    <td valign="middle" style="border-top:5px solid #F0514A;background-color:#00539F;">
                                        <table border="0" cellspacing="0" cellpadding="0" align="center" class="footer">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table width="250" border="0" cellspacing="0" cellpadding="0" align="left" class="ecxcopyright">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="40" valign="middle" style="font-size:14px;color:#FFF;">&copy; 2015 Webpycca</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>';
        
        $cabecera= "MIME-Version: 1.0\r\n";
        $cabecera .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
        $cabecera .= "From: Frank\r\n";
            
        $mail = new PHPMailer();
        $mail->IsSMTP(); 
        $mail->PluginDir = "";
        $mail->Host = $host;
        $mail->Port=$puerto;
        $mail->SMTPAuth = true;
        $mail->Username = $usuario;
        $mail->Password = $clave;
        $mail->From = $responder_a;
        $mail->FromName = "WebPycca";
        $mail->WordWrap = 50;
        $mail->Subject = $asunto;

        $mail->AddAddress($destinatario,$_SESSION['nombre']);
        $mail->Body = $cuerpo;
        $mail->IsHTML(true);
        
        if(!$mail->Send()){
            echo "ERROR: " . $mail->ErrorInfo; 	
        } else {
           return "OK";	
        }	        
            
    }
    
}