<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_1101 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->model(array('proceso/solicitud_cheque/m_1101'));
        session_start();
    }	

    public function _remap()  {    
        
        $tipo  = $this->uri->segment(4);
        $datos = $this->input->post();
        
        validaSession('procesos/solicitud_cheque/co_1101');  
        fnc_post($tipo,$datos,'procesos/solicitud_cheque/co_1101');
        
        if($tipo == 'seDelg'){
            $this->_delegaUsuario();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud($array = null){
        
        $data = null; 
        $urlControlador = 'procesos/solicitud_cheque/co_1101';
        $data['urlControlador'] = $urlControlador;
        $data['array'] = $array;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/solicitud_cheque/j_1101.js',
                            'js/general.js');
        
        $delegados = $this->m_1101->retDelegados($_SESSION['c_e']); 
        $data['delegados'] = $delegados;
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/solicitud_cheque/v_1101');
        $this->load->view('barras/footer');
    }
    
    private function _delegaUsuario(){
        $cod_del = $this->input->post('c_d');
        $result = $this->m_1101->delegaUsuario((int)$_SESSION['c_e'],(int)$cod_del);
        echo json_encode($result);
    }

}