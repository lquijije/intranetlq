<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_110 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->model(array('proceso/solicitud_cheque/m_110'));
        session_start();
    }	

    public function _remap()  {    
        
        $tipo  = $this->uri->segment(4);
        $datos = $this->input->post();
        
        validaSession('procesos/solicitud_cheque/co_110');  
        fnc_post($tipo,$datos,'procesos/solicitud_cheque/co_110');
        
        if($tipo == 'reSol'){
            $this->_retSolicit();
        } else if ($tipo == 'viSol'){
            $this->_viewSolicit();
        } else if ($tipo == 'apSol'){
            $this->_aproSolicit();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud($array=null){
        
        $data = null; 
        $datosParametro=null;

        $urlControlador = 'procesos/solicitud_cheque/co_110';
        $data['urlControlador'] = $urlControlador;
        $data['array'] = $array;
        
        $data['css'] =  array('css/datatables/dataTables.bootstrap.css');
            
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/solicitud_cheque/j_110.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/general.js');
        
        $empresas = $this->m_110->retEmpresasAsignadas($_SESSION['c_e']); 
        
        $bandera = true;
        
        if(!isset($empresas['error'])){
            
            $data['reemplazo'] = $empresas['delegado'];
            if (!empty($empresas['empresas'])){    
                foreach ($empresas['empresas'] as $row){    
                    $datosParametro[trim($row['co_emp'])]=trim($row['no_emp']."(".$row['cant'].")");                                     
                }
            } else {
                $datosParametro['NNN']='No Existen Empresas';    
            }
            $atributosEspecial="id='cmb_empr' class='form-control'";
            $data['cmb_empr'] = form_dropdown('cmb_empr',$datosParametro,'NNN',$atributosEspecial);   
        
        } else {
            
            $bandera = false;
            $data['txt_error'] = trim($empresas['error']);
            
        }
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $bandera === true ?$this->load->view('procesos/solicitud_cheque/v_110'):$this->load->view('procesos/solicitud_cheque/v_110-REEMPLAZADO');
        $this->load->view('barras/footer');
    }
    
    private function _retSolicit(){
        $cod_emp = $this->input->post('cod_emp');
        $mensaje = $this->m_110->retSolicitudes($cod_emp);
        echo json_encode($mensaje);
    }
    
    private function _aproSolicit(){
        
        $data = $this->input->post();
        $result = $this->m_110->aprobarSolicit($data);
       
        $bandera = 0;
        $menOK = $menER = $valida = '';
        
        if(is_array($result)){
            if (!empty($result)){
                for($i=0;$i<count($result);$i++){
                    if((int)$result[$i]['d_e']['co_err'] == 0){
                        $menOK.= $result[$i]['c_s']."<br>";
                    } else {
                        $menER.= $result[$i]['c_s']." - (".trim($result[$i]['d_e']['tx_err']).").<br>";
                    }
                }
                if(!empty($menOK)){
                    $valida.= 'Las siguientes solicitudes han sido actualizadas con exito.<br>'.$menOK;
                }
                if(!empty($menER)){
                    $bandera = 1;
                    $valida.= 'Las siguientes solicitudes han presentado ERROR.<br>'.$menER;
                } 
            }
        } else {
            $bandera = 1;
            $valida = $result;
        }
        
        echo json_encode(array(
            'co_err'=>$bandera,
            'tx_err'=>$valida
        ));
    }

    private function _viewSolicit(){
        $data = $this->input->post();
        $mensaje = $this->m_110->viewSolicit($data);
        $array = null;
        
        if(empty($mensaje['err'])){
            
            $baseImp = (float)$mensaje['cab']['r'] + (float)$mensaje['cab']['s'] - (float)$mensaje['cab']['u'];
            $total = $baseImp + (float)$mensaje['cab']['v'] + (float)$mensaje['cab']['x'];

            $string ='<div class="row line" style="margin-bottom: 10px;padding-bottom: 10px;">';
            $string.='<div class="spa-6">';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Tipo</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="ab" name="ab" class="form-control input-sm" value="'.$mensaje['cab']['a'].' - '.$mensaje['cab']['b'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Concepto</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="d" name="d" class="form-control input-sm" value="'.$mensaje['cab']['d'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Fecha </label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="f" name="f" class="form-control input-sm" value="'.$mensaje['cab']['f'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>ID. Proveedor</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="h" name="h" class="form-control input-sm" value="'.$mensaje['cab']['h'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Nombre</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="j" name="j" class="form-control input-sm" value="'.$mensaje['cab']['j'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Centro Costo</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="l" name="l" class="form-control input-sm" value="'.$mensaje['cab']['l'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Cuentas</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="" name="" class="form-control input-sm" value="" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Observación</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<textarea id="o" name="o" class="form-control input-sm" readonly>'.$mensaje['cab']['o'].'</textarea>';
            $string.='</div>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="spa-6">';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>No. Documento</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="c" name="c" class="form-control input-sm" value="'.$mensaje['cab']['c'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Solicitante</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="e" name="e" class="form-control input-sm" value="'.$mensaje['cab']['e'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Fecha Venc </label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="q" name="q" class="form-control input-sm" value="'.$mensaje['cab']['q'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Beneficiario</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="i" name="i" class="form-control input-sm" value="'.$mensaje['cab']['i'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Moneda</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="" name="" class="form-control input-sm" value="Dólares" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Responsable</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<input id="n" name="n" class="form-control input-sm" value="'.$mensaje['cab']['n'].'" type="text" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>URGENTE</label>';
            $string.='</div>';
            $string.='<div class="spa-1">';
            $string.='<input id="" name="" class="form-control input-sm" value="" type="checkbox" readonly>';
            $string.='</div>';
            $string.='</div>';
            $string.='<div class="separador">';
            $string.='<div class="spa-3">';
            $string.='<label>Comentario Aprobacion o Negacion</label>';
            $string.='</div>';
            $string.='<div class="spa-8">';
            $string.='<textarea id="p" name="p" class="form-control input-sm">'.$mensaje['cab']['p'].'</textarea>';
            $string.='</div>';
            $string.='</div>';
            $string.='</div>';
            $string.='</div>';

            $string.='<div class="row line" style="margin-bottom: 10px;padding-bottom: 10px;">';

                $string.='<div class="spa-6">';
                    $string.='<div class="separador">';

                        $string.='<div class="spa-3">';
                        $string.='</div>';

                        $string.='<div class="spa-8"><h4 style="font-weight: bold;">Plan de Pago</h4>';

                        $string.='<table class="table table-bordered TFtable">';
                        $string.='<thead>';
                        $string.='<tr>';
                        $string.='<th>Fecha</th>';
                        $string.='<th>%</th>';
                        $string.='</tr>';
                        $string.='</thead>';
                        $string.='<tbody id="table_plan_pago">';

                        if(!empty($mensaje['pago'])){
                            foreach ($mensaje['pago'] as $row){
                                $string.='<tr>';
                                $string.='<td>'.$row['b'].'</td>';
                                $string.='<td class="centro">'.$row['c'].'</td>';
                                $string.='</tr>';
                            }
                        }

                        $string.='</tbody>';
                        $string.='</table>';

                        $string.='<h4 style="font-weight: bold;">Lista de Autorizadores</h4>';

                        $string.='<table class="table table-bordered TFtable">';
                        $string.='<thead>';
                        $string.='<tr>';
                        $string.='<th>Secu.</th>';
                        $string.='<th>Nombre</th>';
                        $string.='<th>Acción</th>';
                        $string.='<th>Fecha</th>';
                        $string.='</tr>';
                        $string.='</thead>';
                        $string.='<tbody id="table_plan_pago">';

                        if(!empty($mensaje['auto'])){
                            foreach ($mensaje['auto'] as $row){
                                $string.='<tr>';
                                $string.='<td>'.$row['a'].'</td>';
                                $string.='<td>'.$row['c'].'*'.$row['g'].'</td>';
                                $string.='<td class="centro">'.$row['d'].'</td>';
                                $string.='<td class="centro">'.$row['e'].'</td>';
                                $string.='</tr>';
                            }
                        }

                        $string.='</tbody>';
                        $string.='</table>';

                        $string.='</div>';

                    $string.='</div>';
                $string.='</div>';
                $string.='<div class="spa-6">';
                    $string.='<div class="separador">';

                    $string.='<div class="spa-3">';
                    $string.='<label>Subtotal/Prima</label>';
                    $string.='</div>';
                    $string.='<div class="spa-8">';
                    $string.='<input id="r" name="r" class="form-control input-sm derecha sta" value="'.$mensaje['cab']['r'].'" type="text" readonly>';
                    $string.='</div>';

                    $string.='<div class="spa-3">';
                    $string.='<label>Adicionales</label>';
                    $string.='</div>';
                    $string.='<div class="spa-8">';
                    $string.='<input id="s" name="s" class="form-control input-sm derecha sta" value="'.$mensaje['cab']['s'].'" type="text" readonly>';
                    $string.='</div>';

                    $string.='<div class="spa-3">';
                    $string.='<label>Descuento '.$mensaje['cab']['t'].' % </label>';
                    $string.='</div>';
                    $string.='<div class="spa-8">';
                    $string.='<input id="u" name="u" class="form-control input-sm derecha sta" value="'.$mensaje['cab']['u'].'" type="text" readonly>';
                    $string.='</div>';

                    $string.='<div class="spa-3">';
                    $string.='<label>Base Imponible</label>';
                    $string.='</div>';
                    $string.='<div class="spa-8">';
                    $string.='<input id="bi" name="bi" class="form-control input-sm derecha sta" value="'.number_format($baseImp,2,'.',',').'" type="text" readonly>';
                    $string.='</div>';

                    $string.='<div class="spa-3">';
                    $string.='<label>Varios</label>';
                    $string.='</div>';
                    $string.='<div class="spa-8">';
                    $string.='<input id="v" name="v" class="form-control input-sm derecha sta" value="'.$mensaje['cab']['v'].'" type="text" readonly>';
                    $string.='</div>';

                    $string.='<div class="spa-3">';
                    $string.='<label>I.V.A '.$mensaje['cab']['w'].'%</label>';
                    $string.='</div>';
                    $string.='<div class="spa-8">';
                    $string.='<input id="x" name="x" class="form-control input-sm derecha sta" value="'.$mensaje['cab']['x'].'" type="text" readonly>';
                    $string.='</div>';

                    $string.='<div class="spa-3">';
                    $string.='<label>Total Factura</label>';
                    $string.='</div>';
                    $string.='<div class="spa-8">';
                    $string.='<input id="to" name="to" class="form-control input-sm derecha sta" value="'.number_format($total,2,'.',',').'" type="text" readonly>';
                    $string.='</div>';

                    $string.='</div>';
                $string.='</div>';

                $string.='<div class="spa-12">';
                    $string.='<button type="button" class="btn btn-success " onclick="asigJsonSoli('."'".$data['i_e']."'".','."'".$data['i_s']."'".','."'".$mensaje['cab']['a']."'".','."'A'".')" style="position: absolute; left: 44%;">Aprobar</button>';
                    $string.='<button type="button" class="btn btn-danger " onclick="asigJsonSoli('."'".$data['i_e']."'".','."'".$data['i_s']."'".','."'".$mensaje['cab']['a']."'".','."'N'".')" style="position: absolute; left: 52%;">Negar</button>';
                $string.='</div>';

            $string.='</div>';

            $string.='<div class="row">';

                $string.='<div class="spa-12">';
                    $string.='<div class="separador">';

                        $string.='<div class="spa-1-5">';
                        $string.='</div>';

                        $string.='<div class="spa-9">';

                        $string.='<h4 style="font-weight: bold;">Asientos Contables</h4>';

                        $string.='<table class="table table-bordered TFtable">';
                        $string.='<thead>';
                        $string.='<tr>';
                        $string.='<th>Descripción</th>';
                        $string.='<th>Cuentas</th>';
                        $string.='<th>C.Costo</th>';
                        $string.='<th>Debe</th>';
                        $string.='<th>Haber</th>';
                        $string.='</tr>';
                        $string.='</thead>';
                        $string.='<tbody id="table_plan_pago">';
                        $sumD=0;
                        $sumH=0;

                        if(!empty($mensaje['cont'])){
                            foreach ($mensaje['cont'] as $row){
                                $sumD+=$row['g'];
                                $sumH+=$row['h'];
                                $string.='<tr>';
                                $string.='<td>'.$row['b'].'</td>';
                                $string.='<td>'.$row['c'].''.$row['d'].''.$row['e'].'</td>';
                                $string.='<td>'.$row['f'].'</td>';
                                $string.='<td class="derecha">'.$row['g'].'</td>';
                                $string.='<td class="derecha">'.$row['h'].'</td>';
                                $string.='</tr>';
                            }
                        }

                        $string.='<tr style="font-weight:bold;">';
                        $string.='<td colspan="3">Total</td>';
                        $string.='<td class="derecha">'.number_format($sumD,2,'.',',').'</td>';
                        $string.='<td class="derecha">'.number_format($sumH,2,'.',',').'</td>';
                        $string.='</tr>';

                        $string.='</tbody>';
                        $string.='</table>';

                        $string.='</div>';

                    $string.='</div>';
                $string.='</div>';

            $string.='</div>';
            
            $array = array(
                'empr'  =>  utf8_encode(trim($mensaje['cab']['y'])),
                'status'=>  trim($mensaje['cab']['z']),
                'soli'  =>  $string
            );
            
        } else {
            $array = array('Err'=>$mensaje['err']);
        }
        echo json_encode($array);
        
    }

}