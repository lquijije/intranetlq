<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//require('pdf_js.php');

class co_123 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/garantia/m_123','proceso/reclamos/m_111'));
        $this->load->library(array('mailer'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/garantia/co_123');  
        
        $tipo=$this->uri->segment(4);

        if($tipo=='retCon'){
            $this->_retContratos();
        } else if($tipo=='ge_pdf1'){
            $id_ga = $this->uri->segment(5);
            $tip_repo = $this->uri->segment(6);
            $datos = $this->m_123->retGarantia($id_ga,1);
            $datos = $datos['data'][0];
            $this->ge_pdf1($datos,$tip_repo);
        } else if($tipo=='ge_pdf2'){
            $id_ga = $this->uri->segment(5);
            $tip_repo = $this->uri->segment(6);
            $datos = $this->m_123->retGarantia($id_ga,1);
            $datos = $datos['data'][0];
            $this->ge_pdf2($datos,$tip_repo);
        } else {
            $this->_crud(); 
        }
        
    }        

    private function _crud(){
        
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/garantia/co_123';
        $data['urlControlador'] = $urlControlador;
        $data['css'] = array('css/datatables/dataTables.bootstrap.css','css/datetimepicker/jquery.datetimepicker.css');
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/garantia/j_123.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/mask/jquery.mask.js');

        $data['idFormulario'] = 'co_123';
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/garantia/v_123');
        $this->load->view('barras/footer');            
    }
    
    private function _retContratos(){
        $ci = $this->input->post('ci');
        $tipo = $this->input->post('ti');
        $contratos = null;
        if((int)$tipo == 3){            
            $arr = explode('-', $ci);
            $de_sri = isset($arr[0])?$arr[0]:0;
            $co_caja = isset($arr[1])?$arr[1]:0;
            $nu_doc = isset($arr[2])?$arr[2]:0;
            
            if((int)$de_sri > 0 && (int)$co_caja > 0 && (int)$nu_doc > 0){
                $contratos = $this->m_123->retGarantiaFact($de_sri,$co_caja,$nu_doc);
            } else {
                echo json_encode('Numero de factura incompleta.');
                return;
            }
            
        } else {
            $contratos = $this->m_123->retGarantia($ci,$tipo,$ci);
        }
        echo json_encode($contratos);
    }
    
    private function ge_pdf1($datos,$tipo){
        
            $this->load->library('pdf');
            $pdf = new PDF();
            $pdf->fpdf('P','mm','A4');
            $pdf->AliasNbPages();
            $pdf->AddPage();
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetTextColor(0,0,0);
            $pdf->IncludeJS("print(true);");

        if (!empty($datos['co_con'])){
            $pdf->Ln(5);
            $pdf->SetFont('helvetica', 'B', 15);
            $pdf->Cell(190, 12, utf8_decode("Certificado de Aceptación de Garantía Extendida "),0, 0,'C','true');
            $pdf->Ln(20);

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode("Nombre del cliente: "),0, 0,'L','true');
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Cell(150, 5, utf8_decode($datos['no_cli']),0, 0,'L','true');

            $pdf->Ln(5);

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode("CED/RUC: "),0, 0,'L','true');
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Cell(150, 5, utf8_decode($datos['id_cli']),0, 0,'L','true');

            $pdf->Ln(5);

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode("Número de Garantía: "),0, 0,'L','true');
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Cell(150, 5, $datos['co_con'],0, 0,'L','true');

            $pdf->Ln(5);

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode("Numero de Factura: "),0, 0,'L','true');
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Cell(150, 5, $datos['de_sri']."-".$datos['co_caj']."-".str_pad($datos['nu_doc'], 9, "0", STR_PAD_LEFT),0, 0,'L','true');

            $pdf->Ln(5);

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode("Fecha de Compra: "),0, 0,'L','true');
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Cell(150, 5, $datos['fe_fac'],0, 0,'L','true');

            $pdf->Ln(5);

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode("Descripción del Producto: "),0, 0,'L','true');
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Cell(150, 5, utf8_decode($datos['ds_art']),0, 0,'L','true');

            $pdf->Ln(5);

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode("Modelo: "),0, 0,'L','true');
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Cell(150, 5, utf8_decode($datos['ds_mod']),0, 0,'L','true');

            $pdf->Ln(5);

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode("Serie - IMEI: "),0, 0,'L','true');
            $pdf->SetFont('helvetica', '', 10);
            $pdf->Cell(150, 5, utf8_decode($datos['nu_ser']),0, 0,'L','true');

            $pdf->Ln(15);

            $parra1 = "Mediante el presente, consta la contratación del servicio de Garantía Extendida por parte del prestador Pycca S.A., los términos y condiciones de la mencionada extensión son idénticos a la garantía original del fabricante, los cuales el cliente declara conocer y entender al momento de la contratación	del servicio, a la vez estos están publicados en la siguiente página WEB: www.garantiaextendidapycca.com.ec.";
            $pdf->MultiCell(190,5, utf8_decode($parra1),0,'J'); 

            $pdf->Ln(5);

            $parra2 = "Principales Exclusiones: EI Mantenimiento, reparación o reemplazo necesario por perdida o daño que resulte de cualquier otra causa diferente al uso y operación normal del PRODUCTO de acuerdo con las especificaciones del fabricante, los servicios adicionales que el CLIENTE haya contratado directamente con el centro de servicio bajo cuenta y riesgo, desgaste propio por vejez o terminación de vida útil, los elementos de apariencia o estructurales tales como la envoltura, carcasas, la caja o sus partes decorativas, el chasis o el bastidor, o el marco. Los artículos de consumo, tales como los cartuchos de cinta o casetes en general, disquetes, discos compactos incluidos los de audio o de video, cristales, lentes, vidrios, cabezas y agujas.";
            $pdf->MultiCell(190,5, utf8_decode($parra2),0,'J'); 

            $pdf->Ln(5);

            $parra3 = "Uso del servicio: Para ejecutar esta extensión de garantía el cliente deberá comunicarse previamente a la central de Llamadas de la prestación del servicio.";
            $pdf->MultiCell(190,5, utf8_decode($parra3),0,'J'); 

            $pdf->Ln(5);

            $pdf->MultiCell(190,5, utf8_decode('1800 1 PYCCA (179222).'),0,'J'); 

            $pdf->Ln(5);

            $parra4 = "EI precio del servicio es de US$ ".  number_format($datos['va_art_gar'],2)." y tiene vigencia de ".((int)$datos['pr_art_gar']*12)." Meses a partir de que termine la garanta original del fabricante o proveedor.";
            $pdf->MultiCell(190,5, utf8_decode($parra4),0,'J'); 

            $pdf->Ln(5);

            $parra5 = "Servicio operado y respaldado por Ecuasistencia Compañía de Asistencia del Ecuador S.A.";
            $pdf->MultiCell(190,5, utf8_decode($parra5),0,'J'); 

            $pdf->SetFont('helvetica', 'B', 10);
            $pdf->Ln(15); 
            $pdf->Cell(55, 5, utf8_decode('___________________________'),0, 0,'L','true');
            $pdf->Ln(5); 
            $pdf->Cell(55, 5, utf8_decode('Firma de Aceptación del Cliente'),0, 0,'C','true');
            
            $pdf->Output();
            //$pdf->Output('CERTIFICADO_GARANTIA_EXT_'.$datos['co_con'].'.pdf','D');
        } else {
            $pdf->Ln(5);
            $pdf->SetFont('helvetica', 'B', 15);
            $pdf->Cell(190, 12, utf8_decode("NO HAY DATOS DEL CONTRATO A BUSCAR"),0, 0,'C','true');
            $pdf->Ln(20);
            $pdf->Output();
        }
        
    }
    
    private function ge_pdf2($datos,$tipo){
        
        $this->load->library('pdf');
        $pdf = new PDF();
        $pdf->fpdf('P','mm','A4');
        $pdf->SetMargins(20,10,20,10);
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetDrawColor(255,81,66);  
        $pdf->SetLineWidth(6);
        $pdf->Line(0,2,210,2);
        $pdf->Line(0,295,210,295);
        $pdf->Line(2,0,2,295);
        $pdf->Line(208,0,208,295);
        $pdf->IncludeJS("print(true);");
        
        if (!empty($datos['co_con'])){
            /***********************************************************************/
            
            $pdf->Image(site_url().'img/gar_ext.png',150,10.6,50);
            
            $pdf->SetFont('arial', '', 10);      
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(30, 5, utf8_decode("No. De Contrato: "),0, 0,'L','true');

            $pdf->SetFont('arial', 'B', 18);      
            $pdf->SetTextColor(238,58,67);
            $pdf->Cell(100,5, str_pad($datos['co_con'], 9, "0", STR_PAD_LEFT),0, 0,'L');

            $pdf->Ln(5);

            /***********************************************************************/
            $pdf->SetFont('arial', 'B', 10);      
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(130, 5, utf8_decode("CONTRATO DE GARANTÍA EXTENDIDA ELECTRODOMÉSTICO Y MUEBLES"),0, 0,'L');
            $pdf->Ln(5);
            $pdf->SetFont('arial', '', 7);     
            $pdf->Cell(130, 2, utf8_decode("FAVOR LLENAR EN LETRA IMPRENTA MAYUSCULAS SIN TACHADURAS"),0, 0,'C');

            $pdf->Ln(5);
            $pdf->SetFont('arial', '', 10);     
            $pdf->SetFillColor(238,58,67);    
            $pdf->SetTextColor(255,255,255);   
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(132.5,4, utf8_decode(" "),'T', 0,'C','true');     
            $pdf->Ln(8); 
            
            /***********************************************************************/

            $pdf->SetFont('arial', '', 9);     
            $pdf->SetFillColor(255,255,255);       
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(35, 4, utf8_decode("Fecha Del Contrato:"),0, 0,'L');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(100, 4,$datos['fe_fac'],'B', 0,'L');
            $pdf->Ln(5);  
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(30, 4, utf8_decode("No. De Factura:"),0, 0,'L');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(105, 4, $datos['de_sri']."-".$datos['co_caj']."-".str_pad($datos['nu_doc'], 9, "0", STR_PAD_LEFT),'B', 0,'L');
            $pdf->Ln(7);

            /***********************************************************************/

            $pdf->SetFont('arial', '', 9);     
            $pdf->SetFillColor(238,58,67);    
            $pdf->SetTextColor(255,255,255);   
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(170,4, utf8_decode("DATOS DEL PRODUCTO ADQUIRIDO"),'T', 0,'C','true');     
            $pdf->Ln(7); 
            
            /***********************************************************************/

            $pdf->SetFillColor(255,255,255); 
            $pdf->SetFont('arial', '', 9);      
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(25, 4, utf8_decode("Descripción:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(60, 4,$datos['ds_art'],'B', 0,'L','true');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(15, 4, utf8_decode("Marca:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(70, 4,$datos['ds_mar'],'B', 0,'L','true');
            $pdf->Ln(5);  
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(20, 4, utf8_decode("Serial No.:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(65, 4,$datos['nu_ser'],'B', 0,'L','true');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(15, 4, utf8_decode("Modelo:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(70, 4,$datos['ds_mod'],'B', 0,'L','true');
            $pdf->Ln(7);         
            $pdf->SetTextColor(238,58,67);
            $pdf->Cell(170, 4,utf8_decode("En caso que el producto tenga mas de un componenete, indicar el serial de cada uno."),0, 0,'C','true');
            $pdf->Ln(7);     

            /***********************************************************************/

            $pdf->SetFillColor(255,255,255); 
            $pdf->SetFont('arial', '', 9);      
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(35, 4, utf8_decode("Código del producto:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(50, 4,$datos['co_art'],'B', 0,'L','true');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(50, 4, utf8_decode("Precio de compra del producto:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(35, 4,"$". number_format($datos['va_art'],2),'B', 0,'L','true');
            $pdf->Ln(6);  
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(85, 4, utf8_decode("Periodo de Garantía del fabricante o del Almacén:"),0, 0,'L','true');
            $pdf->Cell(22, 4, utf8_decode("1 Año"),0, 0,'L','true');
            $pdf->Cell(22, 4, utf8_decode("2 Años"),0, 0,'L','true');
            $pdf->Cell(22, 4, utf8_decode("3 Años"),0, 0,'L','true');

            if($datos['pr_art'] == 12){
                $pdf->Image(site_url().'img/check_2.png',120,76.5,5);
                $pdf->Image(site_url().'img/check_1.png',142,76.5,5);
                $pdf->Image(site_url().'img/check_1.png',164,76.5,5);
            } else if($datos['pr_art'] == 24){
                $pdf->Image(site_url().'img/check_1.png',120,76.5,5);
                $pdf->Image(site_url().'img/check_2.png',142,76.5,5);
                $pdf->Image(site_url().'img/check_1.png',164,76.5,5);
            } else if($datos['pr_art'] == 36){
                $pdf->Image(site_url().'img/check_1.png',120,76.5,5);
                $pdf->Image(site_url().'img/check_1.png',142,76.5,5);
                $pdf->Image(site_url().'img/check_2.png',164,76.5,5);
            } else {
                $pdf->Image(site_url().'img/check_1.png',120,76.5,5);
                $pdf->Image(site_url().'img/check_1.png',142,76.5,5);
                $pdf->Image(site_url().'img/check_1.png',164,76.5,5);
            }

            $pdf->Ln(7);  

            /***********************************************************************/

            $pdf->SetFont('arial', '', 9);     
            $pdf->SetFillColor(238,58,67);    
            $pdf->SetTextColor(255,255,255);   
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(170,4, utf8_decode("GARANTÍA EXTENDIDA"),'T', 0,'C','true');     
            $pdf->Ln(7); 

            /***********************************************************************/

            $pdf->SetFillColor(255,255,255);  
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(85, 4, utf8_decode("Este contrato tiene validez exclusivamente para el producto descrito anteriormente."),0, 0,'L','true');
            $pdf->Ln(5); 
            
            $pdf->Cell(20, 4, utf8_decode("Duración:"),0, 0,'L','true'); 
            $pdf->Cell(30, 4, utf8_decode("12 Meses"),0, 0,'L','true');
            $pdf->Cell(30, 4, utf8_decode("24 Meses"),0, 0,'L','true');
            $pdf->Cell(30, 4, utf8_decode("36 Meses"),0, 0,'L','true');
            if($datos['pr_art_gar'] == 1){
                $pdf->Image(site_url().'img/check_2.png',60,95.5,5);
                $pdf->Image(site_url().'img/check_1.png',90,95.5,5);
                $pdf->Image(site_url().'img/check_1.png',120,95.5,5);
            } else if($datos['pr_art_gar'] == 2){
                $pdf->Image(site_url().'img/check_1.png',60,95.5,5);
                $pdf->Image(site_url().'img/check_2.png',90,95.5,5);
                $pdf->Image(site_url().'img/check_1.png',120,95.5,5);
            } else if($datos['pr_art_gar'] == 3){
                $pdf->Image(site_url().'img/check_1.png',60,95.5,5);
                $pdf->Image(site_url().'img/check_1.png',90,95.5,5);
                $pdf->Image(site_url().'img/check_2.png',120,95.5,5);
            } else {
                $pdf->Image(site_url().'img/check_1.png',60,95.5,5);
                $pdf->Image(site_url().'img/check_1.png',90,95.5,5);
                $pdf->Image(site_url().'img/check_1.png',120,95.5,5);
            }
            $pdf->Ln(7);
            $pdf->Cell(55, 4, utf8_decode("Precio de la garantía extendida:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(30, 4,"$". number_format($datos['va_art_gar'],2),'B', 0,'L','true');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);   
            $pdf->Cell(35, 4, utf8_decode("Código del producto:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(50, 4,$datos['co_art_ga'],'B', 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(5);

            /***********************************************************************/

            $pdf->SetTextColor(238,58,67);
            $pdf->SetFont('arial', '', 8);    
            $pdf->MultiCell(170,3, utf8_decode("IMPORTANTE: El presente contrato de SERVICIOS PREPAGAQOS DE REPARACION - GARANTÍA EXTENDIDA entra en vigencia en el momento en que se venza la garantía del fabricante, de la cual es Independiente."),0,'C');
            $pdf->Ln(5); 
            
            /***********************************************************************/
            
            $pdf->SetFont('arial', 'B', 15);      
            $pdf->SetTextColor(238,58,67);
            $pdf->Cell(50,0,"1800 1",0, 0,'R');
            $pdf->Image(site_url().'img/logo.png',63,116.3,30);
            $pdf->Cell(40,0,"(179222)",0, 0,'R');
            
            $pdf->Image(site_url().'img/logo.png',110,115,50);
            $pdf->Ln(5);    
            /***********************************************************************/
            
            
            $pdf->SetFont('arial', '', 9);     
            $pdf->SetFillColor(238,58,67);    
            $pdf->SetTextColor(255,255,255);   
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(170,4, utf8_decode("CLAUSULAS"),'T', 0,'C','true');     
            $pdf->Ln(7); 
            
            /***********************************************************************/
            
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('arial', '', 8.3);  
            
            $pdf->SetMargins(10,10,10,0);
            
            $pdf->MultiCell(170,3, utf8_decode("CONTRATO DE PRESTACIÓN DE SERVICIOS DE GARANTÍA EXTENDIDA PYCCA"),0,'C');
            $pdf->Ln(3);
            $pdf->y0 = 137;
            
            $P1 = utf8_decode("PYCCA S.A. en adelante el PRESTADOR DEL SERVICIO y quien aparece descrito en la carátula de este contrato, en adelante EL CLIENTE, acuerdan celebrar un Contrato de Prestación de Servicios de GARANTÍA EXTENDIDA que se rige por  las siguientes Cláusulas: \n\nPrimera. OBJETO. La prestación de los servicios de GARANTÍA EXTENDIDA, consiste en proporcionar durante el período de vigencia del presente contrato la mano de obra y los repuestos que sean necesarios para restablecer las condiciones normales de funcionamiento del PRODUCTO descrito en la carátula, siempre que dicha reparación sea necesaria por una falla mecánica, eléctrica o electrónica interna del PRODUCTO durante su uso normal, no comercial en la forma descrita por las instrucciones del fabricante, sin perjuicio de las exclusiones contenidas en este contrato. \nNota: Este plan es independiente y en ningún caso reemplaza la garantía original del fabricante y es así mismo, distinto e independiente de la garantía mínima presunta. En consecuencia, el fabricante del PRODUCTO no asume responsabilidad alguna derivada de este contrato.\n\nSegunda. TÉRMINOS Y DEFINICIONES: Para todos los efectos de este contrato se entenderá por:\na.	PRESTADOR DEL SERVICIO: Persona que se obliga a prestar los servicios descritos del presente contrato. A menos que se disponga en distinto  sentido en los términos y condiciones del contrato, la persona jurídica  obligada es PYCCA S.A., quien prestará el servicio a través de sus proveedores de servicios autorizados, quien para este evento será la compañía ECUASISTENCIA COMPAÑÍA DE ASISTENCIA DEL ECUADOR S.A.\nb.	CLIENTE: Persona Natural o Jurídica comprador del PRODUCTO cubierto bajo el plan de servicio o cualquier cesionario o apoderado del comprador.\nc.	PRODUCTO: Bien descrito en la carátula de este contrato debidamente identificado con su marca, número de serie, modelo o cualquier otro medio de identificación.\nd.	SERVICIO: Mano de obra y parte de repuesto necesarias para restablecer el funcionamiento del PRODUCTO a sus condiciones normales de operación de acuerdo con las especificaciones del fabricante. \ne.	VENDEDOR ORIGINAL: Persona jurídica responsable de la venta del PRODUCTO al cliente descrito en la carátula de este contrato. \nf.	NOTA DE  CAMBIO: Documento que puede ser utilizado por el CLIENTE para hacer compras por el valor que se consigne en el mismo documento; esta nota de cambio únicamente puede ser utilizada en las tiendas de PYCCA, vendedor original del PRODUCTO.\n\nTercera.  CONDICIONES DE LA PRESTACIÓN DEL SERVICIO. El prestador se obliga a prestar el Servicio cuantas veces se solicite durante la vigencia del presente contrato, siempre se cumplan los requisitos previstos en este documento; por lo tanto no existirá límite para la cantidad de reparaciones que se hagan al PRODUCTO.\n\nProcedimiento a seguir para obtener la prestación del servicio:\na.	El cliente no deberá estar en mora.\nb.	EL CLIENTE deberá comunicarse telefónicamente con la central de llamadas de PRESTADOR DEL SERVICIO, al número 1-800-1PYCCA (179222). de la ciudad de QUITO.\nc.	Producida la llamada, el CLIENTE deberá informar a la central de llamadas del PRESTADOR DEL SERVICIO, el número del contrato de GARANTÍA EXTENDIDA, nombres y apellidos del CLIENTE y documento de identidad, así como el motivo de la llamada.\nd.	EL PRESTADOR DEL SERVICIO orientará al CLIENTE y en caso requerido informará los datos del Centro de Servicios al cual debe trasladar el PRODUCTO.\ne.	EL CLIENTE deberá trasladar el PRODUCTO al centro de servicios informado por el PRESTADOR DEL SERVICIO; el personal del Centro de Servicio recibirá el PRODUCTO en depósito para realizar una inspección previa, determinar la falla y causa de la misma.");
            
            $pdf->PrintChapter(1,'',$P1);
            
            /*******************************************************************/
            
            $pdf->SetMargins(10,12,0,0);
            $pdf->AddPage();
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(238,58,67);  
            $pdf->SetLineWidth(6);
            $pdf->Line(0,2,210,2);
            $pdf->Line(0,295,210,295);
            $pdf->Line(2,0,2,295);
            $pdf->Line(208,0,208,295);
            
            $pdf->y0 = 12;
            $P2 = utf8_decode("El Servicio para lavadoras, secadoras, lavadora – secadora, televisores de más de (veinticinco) 25  pulgadas, campanas extractoras, fogones de hornillas eléctricos y a gas, calentadores, aires acondicionados empotrados y los demás equipos eléctricos que sean de difícil movilización por su tamaño o peso, será atendido por el PRESTADOR DEL SERVICIO  a domicilio en un término no mayor a treinta y seis (36) horas contadas desde la recepción de la formulación de la solicitud.\nf.      En el evento de que el PRODUCTO o el repuesto no se encuentre incluido dentro del objeto de este contrato, el PRESTADOR DE SERVICIO comunicará al CLIENTE esta circunstancia. EL CLIENTE se obliga a retirar el PRODUCTO dentro de los cinco (5) días hábiles siguientes contados a partir de la fecha de comunicación; transcurrido este plazo, el PRESTADOR DE SERVICIO no responderá por la pérdida o daños que sufra el PRODUCTO. EL CLIENTE puede autorizar expresamente al centro de servicio para que le sea reparado el equipo; en este caso los costos que generen por el arreglo serán cancelados directamente por el CLIENTE antes de retirar el PRODUCTO.\ng.       Verificada la procedencia de la cobertura, el PRESTADOR DEL SERVICIO se obliga a devolverlo reparado dentro de los treinta (30) días hábiles siguientes contados a partir del recibo del PRODUCTO por parte del personal del Centro de Servicio, salvo fuerza mayor o caso fortuito.\nh.        La obligación de entregar dentro del plazo señalado en el literal anterior no será exigible en el evento de que las piezas necesarias para la reparación no se encuentren disponibles en el mercado local y deban ser conseguidas en otras plazas. En este caso el PRESTADOR DE SERVICIO comunicará al CLIENTE esta circunstancia, ofreciéndole la posibilidad de préstamo de un PRODUCTO de similares características hasta tanto se entregue el PRODUCTO reparado.\ni.        Reparado el PRODUCTO, el CLIENTE  se obliga a retirar el PRODUCTO dentro de los cinco (5) días hábiles siguientes a la fecha de la comunicación en la que se informe que éste ha sido reparado. Si el PRODUCTO no es retirado por el CLIENTE, dentro del término establecido, cualquier pérdida o deterioro del mismo quedará bajo la exclusiva responsabilidad del CLIENTE, quien desde ahora y de manera anticipada se obliga a pagar al PRESTADOR DEL SERVICIO la suma equivalente al uno por ciento (1%) de valor de compra del PRODUCTO que conste en la factura de compra correspondiente, por cada día de demora en el retiro del PRODUCTO.\n\nEL PRESTADOR DEL SERVICIO se reserva el derecho de subcontratar el desarrollo de las actividades objeto del presente contrato.\n\nCuarta. OBLIGACIONES DEL PRESTADOR DEL SERVICIO. EL PRESTADOR DEL SERVICIO, se obliga a prestar el servicio de GARANTÍA EXTENDIDA  contemplado en el presente documento directamente ó a través de sus proveedores, teniendo como alternativas de prestación de este servicio, las siguientes:\n\nSERVICIO EN EL CENTRO DE SERVICIOS:\n\nEl CLIENTE deberá comunicarse con el PRESTADOR DEL SERVICIO al teléfono 1-800-1PYCCA (179222) – ECUADOR, para informar de la avería y solicitar el servicio.\n\nEl CLIENTE deberá trasladar el PRODUCTO al centro de servicios informado por el PRESTADOR DEL SERVICIO; el personal del Centro de Servicio recibirá el PRODUCTO en depósito para realizar una inspección previa, determinar la falla y causa de la misma. Verificada la procedencia de la cobertura, el PRESTADOR DEL SERVICIO se obliga a devolverlo reparado dentro de los treinta (30) días hábiles siguientes contados a partir del recibo del PRODUCTO por parte del personal del  Centro de Servicio, salvo fuerza mayor o caso fortuito. SERVICIO A DOMICILIO: \n\nSi la avería proviene de electrodomésticos como: lavadoras, secadoras, lavadora-secadora, televisores de más de (veinticinco) 25 pulgadas, campanas extractoras, fogones de hornillas eléctricos y a gas, calentadores, aires acondicionados empotrados y los demás equipos eléctricos que sean de difícil movilización por su tamaño o peso, el CLIENTE deberá comunicarse con el PRESTADOR DEL SERVICIO al teléfono 1800-1PYCCA (179222) – ECUADOR, para informar de la avería, solicitar el servicio y acordar la fecha en que se realizará la revisión del PRODUCTO por parte del personal designado para tal efecto por el PRESTADOR DEL SERVICIO. \n\nLa solicitud será atendida dentro de las siguientes treinta y seis (36) horas siguientes a la solicitud telefónica del servicio por parte del CLIENTE. EL PRESTADOR DEL SERVICIO no está autorizado para retirar elementos que estén fijados a la pared, a un mueble o a una estructura, que pudiera ocasionar daños a la propiedad en la que se encuentra el PRODUCTO. En consecuencia, el CLIENTE debe desfijar el PRODUCTO antes de la fecha programada para la visita. Si a juicio del PRESTADOR DEL SERVICIO; si lo anterior no resulta posible, el PRESTADOR DEL SERVICIO acordará con el CLIENTE el traslado del PRODUCTO al Centro de Servicio y el reembolso de los gastos razonables en que incurra el CLIENTE con ocasión del traslado. \n\nSUSTITUCIÓN PARCIAL DE EQUIPOS:\n\nPara los siguientes electrodomésticos: Nevera, Lavadoras y/o Televisores, el PRESTADOR DEL SERVICIO, sustituirá hasta por treinta (30) días, estos bienes con otros que cumplan con la funciones básicas del bien afectado.\n\nSUSTITUICIÓN DEL PRODUCTO:\n\nDurante la primera mitad del término de vigencia del presente contrato y por una vez, siempre y cuando el PRODUCTO ha sido reparado en dos oportunidades consecutivas por la misma falla y la misma se presenta a continuación por tercera vez comprobada, o si a juicio  del PRESTADOR DEL SERVICIO, el PRODUCTO no es susceptible de ser reparado, el PRESTADOR DE SERVICIO podrá sustituirlo por otro de iguales o superiores características y funciones, aunque no sea de la misma marca. En todos los casos de reemplazo el CLIENTE deberá entregar el PRODUCTO original al PRESTADOR DEL SERVICIO en el momento de recibir el PRODUCTO de reemplazo o en su defecto la NOTA DE CAMBIO. Los gastos de envío del PRODUCTO correrán por la cuenta del PRESTADOR DE SERVICIO.\n\nEn el evento de que al PRESTADOR DEL SERVICIO no le sea posible reemplazar el PRODUCTO dentro de un período de tiempo de treinta (30) días, podrá entregar al CLIENTE una NOTA DE CAMBIO por el valor original de la compra del PRODUCTO, sin incluir impuestos, sin indexación o incremento alguno; con esta NOTA DE CAMBIO el CLIENTE podrá adquirir cualquier bien  de iguales o superiores características y funciones dentro del establecimiento de comercio del Vendedor  Original del PRODUCTO. El CLIENTE al sustituir el PRODUCTO, recibirá por parte del Vendedor original, copia de la factura por el cambio realizado. \n\nDurante la segunda mitad del término de duración de este contrato, si a juicio del PRESTADOR DEL SERVICIO, el PRODUCTO no es susceptible de ser reparado, se entregará  al CLIENTE una NOTA DE CAMBIO por el valor original del PRODUCTO, sin incluir impuestos, ó se sustituirá por otro de iguales o superiores características y funciones, aunque no sea de la misma marca, sin indexación  o incremento alguno, y para los mismos fines esperados en el caso anterior.");
            $pdf->PrintChapter(2,'',$P2);
            
            /*******************************************************************/
            
            $pdf->SetMargins(10,12,0,0);
            $pdf->AddPage();
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(238,58,67);  
            $pdf->SetLineWidth(6);
            $pdf->Line(0,2,210,2);
            $pdf->Line(0,295,210,295);
            $pdf->Line(2,0,2,295);
            $pdf->Line(208,0,208,295);
            
            $pdf->y0 = 12;
            $P3 = utf8_decode("Quinta. EXCLUSIONES: No hace parte del objeto de este contrato y por lo tanto no habrá lugar a la prestación del Servicio en los siguientes casos:\n\na.       Los servicios que el CLIENTE haya concertado por su cuenta sin el previo consentimiento del PRESTADOR DEL SERVICIO.\nb.   Los servicios adicionales que el CLIENTE haya contratado directamente con el centro de servicio bajo cuenta y riesgo.\nc.       El Mantenimiento, reparación o reemplazo necesario por perdida o daño que resulte de cualquier otra causa diferente otra causa diferente al uso y operación normal del PRODUCTO de acuerdo con las especificaciones del fabricante, incluyendo pero no limitado a hurto, exposición a condiciones climáticas, negligencia, accidente, mal uso, abuso, suministro inadecuado de gas o corriente eléctrica, reparaciones no autorizadas, instalación o accesorios incorrectos, daños ocasionados durante el transporte (exceptuando daños sufridos durante el traslado autorizado del PRODUCTO), daños a gabinetes, falta del mantenimiento indicado por el fabricante, modificaciones, indebidas al PRODUCTO, vandalismo, plagas de animales o insectos, oxidación, polvo, corrosión baterías defectuosas, derrame de baterías, fósforo quemado (incluye imagen con espectros), píxeles fundidos por causas diferentes a la especificadas por el fabricante, o cualquier otro evento externo al PRODUCTO. \nd.    Los elementos de apariencia o estructurales; tales como la envoltura, carcazas, la caja o sus partes decorativas,  el chasis o el bastidor, o el marco, etc.\ne.   Los artículos adicionales como accesorios, interruptores, adaptadores, y cargadores de batería en general, las líneas y cables externos, bastidores, recipientes, botones, asideros, antenas, terminales, conectores, tomas, enchufes, ligas, bandas, cauchos, y partes desechables.\nf.       Los artículos de consumo, tales como los cartuchos de cinta o casetes en general, disquetes, discos compactos incluidos los de audio o de video, cristales, lentes, vidrios, cabezas y agujas.\ng.    Los daños causados por exposición a condiciones lumínicas, climáticas o ambientales, arena, golpes, caídas, maltrato, conexión inadecuada en general o tomacorrientes, adaptador, regulador, estabilizador, supresor de picos, a la(s) red(es) en general y/u otro equipo, o por fuerza mayor o caso fortuito.\nh.     Daños a tuberías de agua o gas y otras tuberías o instalaciones de plomería externos al PRODUCTO.\ni.       Los artículos que aún se encuentren cubiertos por la garantía  del fabricante.\nj.     Condiciones preexistentes, que hubieren ocurrido antes de la fecha de iniciación de la vigencia de este contrato.\nk.          Deterioro, demérito, depreciación y/o desgaste por el natural y normal uso o funcionamiento del PRODUCTO.\nl.          Desperfectos causados por fallas en unidades transformadoras o generadoras, servidores, etc., colocados en forma externa al PRODUCTO, excepto cuando ellas hayan sido provistas directamente por el fabricante del PRODUCTO y junto con éste.\nm.  El mantenimiento normal, limpieza, lubricación, ajuste o alineamiento y/o regulación.\nn.        Los problemas de transmisión o recepción en general.\no.       Cualquier problema o defecto no cubierto por la garantía original y por escrito del fabricante.\np.      Los desperfectos, daños o pérdida producidos a causa o como consecuencia de:\n\n-	Arreglos, reparaciones, modificaciones o desarme de la instalación o cualquier parte del PRODUCTO, por un técnico no autorizado por el fabricante o PRESTADOR DEL SERVICIO, o del incumplimiento o errores al seguir las instrucciones del fabricante para su instalación, operación o mantenimiento.\n\n-          Los causados por mala fe del cliente.\n-        Los causados por Incendio, inundaciones, terremoto, maremoto, granizo, vientos fuertes, erupciones volcánicas, tempestades, ciclónicas, caídas de cuerpos siderales y aerolitos, o cualquier otro fenómeno de la naturaleza de carácter catastrófico.\n-           Los que tuviesen origen o fueran una consecuencia directa o indirecta de guerra, guerra civil, conflictos armados, sublevación rebelión sedición, actos mal intencionados de terceros, motín, huelga, desorden popular y otros hechos que alteren la seguridad interior del Estado o el orden público, secuestro, confiscación, incautación o decomiso.\nq.      El presente contrato no cubre los programas de aplicación así como el software de operación o cualquier otro software, por lo 	que el PRESTADOR DEL SERVICIO no será responsable por la pérdida de datos o restauraciones de programas.\n\nEste contrato se refiere solo al PRODUCTO plenamente determinado en la carátula del mismo y por tanto, carecerá de toda validez, si desde un comienzo no se indica simultáneamente y en todos sus ejemplares: marca, modelo y número de serie que lo identifique, o cualquier otro medio de identificación. En caso de que se presenten inconsistencias en la información relacionada con la identificación del PRODUCTO consignada en la carátula, no habrá lugar a la prestación del Servicio.\n\nSexta. PRECIO. EL CLIENTE pagará la suma descrita en la carátula del contrato, que corresponderá al término de la Garantía Contratada, seleccionada por el CLIENTE, y por el cual podrá disponer del Servicio de GARANTÍA EXTENDIDA en los términos y condiciones establecidos en este contrato.\n\nSéptima.  VIGENCIA. El término de duración del presente contrato será el señalado en la carátula del contrato, y que en todos los casos se contará a partir del momento en que expire el período de garantía del fabricante. En consecuencia, no existirá en ningún caso solidaridad entre PRESTADOR DEL SERVICIO y el fabricante y/o el Vendedor Original o Distribuidor del PRODUCTO por la garantía original.\n\nOctava. OBLIGACIONES DEL CLIENTE. Además de las obligaciones a cargo del CLIENTE previstas en otras cláusulas de este contrato, el CLIENTE  se obliga a:\na.        Observar estrictamente las instrucciones impartidas por el fabricante en los manuales de operación del PRODUCTO y a velar porque el PRODUCTO permanezca en óptimas condiciones de conservación y funcionamiento.\nb.            Comunicarse previamente con la central de llamadas,  del PRESTADOR DEL SERVICIO, para reportar la avería. \nc.          No efectuar y ni permitir que se efectúe cualquier reparación, intervención manipulación o uso no autorizado del PRODUCTO,  sin consentimiento previo y escrito del PRESTADOR DEL SERVICIO.\n\nNovena. CESIÓN DEL CONTRATO. Los derechos del CLIENTE derivados de este contrato son transferibles en cualquier persona en cualquier momento durante de la vigencia. \nPara que la cesión productos efectos, el CLIENTE debe avisar por escrito al PRESTADOR DEL SERVICIO  a la dirección consignada en la Cláusula Décima Tercera de este contrato.  El aviso debe incluir el número de referencia del contrato, la fecha de cesión del PRODUCTO, el nombre del nuevo propietario con su dirección completa y su número telefónico. La cesión producirá efectos desde el momento en que el PRESTADOR DEL SERVICO  reciba la comunicación escrita. EL PRESTADOR DEL SERVICIO no pagará ni reembolsará en ningún caso, los gastos en que incurra el CLIENTE en ocasión de la Cesión de este contrato.\n\nDécima. TERMINACIÓN DEL CONTRATO. El presente contrato terminará automáticamente al vencimiento del término  de duración pactado. Así mismo, son causales de terminación:");
            $pdf->PrintChapter(2,'',$P3);
            
            /*******************************************************************/
            
            $pdf->SetMargins(10,12,0,0);
            $pdf->AddPage();
            $pdf->SetFillColor(255,255,255);
            $pdf->SetDrawColor(238,58,67);  
            $pdf->SetLineWidth(6);
            $pdf->Line(0,2,210,2);
            $pdf->Line(0,295,210,295);
            $pdf->Line(2,0,2,295);
            $pdf->Line(208,0,208,295);
            
            $pdf->y0 = 12;
            
            $pdf->SetY(12);
            
            $P4 = utf8_decode("a.           Solicitud del CLIENTE.\nb.           Mutuo acuerdo entre las partes.\nc.       La falta de pago del valor estipulado como precio de este contrato faculta al PRESTADOR DEL SERVICIO para darlo por terminado desde el mismo día en que se produzca el incumplimiento de la obligación por parte del CLIENTE, sin necesidad de aviso o requerimiento alguno.\nd.         Incumplimiento de cualquiera de las obligaciones a cargo del CLIENTE. \ne.          Cuando el CLIENTE permita que persona no autorizada revise o repare el PRODUCTO o ejecute actos que motiven la extinción de la Garantía Original del Fabricante. \nf.         En caso de pérdida o destrucción del PRODUCTO o por la rotura de sellos de garantía. \ng.        Fuerza mayor o caso fortuito que haga imposible el cumplimiento al PRESTADOR DEL SERVICIO de las obligaciones objeto del presente contrato.\n\nDécima Primera. CANCELACIÓN DEL CONTRATO. En virtud de la prerrogativa que el Vendedor Original confiere a (sus) CLIENTE(S) de devolver el PRODUCTO dentro de la oportunidad establecida, en el mismo momento de devolver el PRODUCTO, el CLIENTE deberá entregar al Vendedor el original de este contrato, debidamente cancelado, para obtener la devolución del importe del precio pagado por la prestación de los servicios de GARANTÍA EXTENDIDA, sin que en ningún caso haya lugar al reconocimiento de intereses, indexación o concepto similar.\n\nDécima Segunda. SUBROGACIÓN. En el caso en que el PRODUCTO sea reparado o reemplazado bajo los términos y condiciones de este contrato, el CLIENTE acepta ceder y subrogar sus derechos de recuperación, si lo hubiere, al PRESTADOR DEL SERVICIO.\n\nDécima Tercera. LIMITACIÓN DE RESPONSABILIDAD. El PRESTADOR DEL SERVICIO y sus representantes, NO asumirán responsabilidad alguna por daños consecuenciales relacionados con el Servicio prestado en virtud de este contrato, diferente a la establecida este documento y en las disposiciones legales que resulten aplicables.\n\n\n\nDécima Cuarta. LEY APLICABLE. El presente contrato se regirá e interpretará de conformidad con las leyes de la República del Ecuador. En el evento de presentarse conflicto entre el Contrato y la información suministrada en forma oral o por escrito por el PRESTADOR DEL SERVICIO, sus empleados, proveedores o distribuidores, el Vendedor Original, sus empleados, agentes, o distribuidores regirán las condiciones consignadas en el texto de este contrato.\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nDécima Quinta. DOMICILIO Y NOTIFICACIONES. Las partes señalan los siguientes domicilios para todos los efectos relacionados con el presente contrato:\n\nPRESTADOR DEL SERVICIO/ VENDEDOR ORIGINAL:\n\nPYCCA S.A.\nTeléfono: 04-2592001\nFax: 04-2531899\nDirección: Calle Boyacá 1205 y 9 de Octubre.\nGuayaquil-Ecuador\n\nCLIENTE: Datos que aparecen en la carátula de este contrato.\n\nCualquier cambio en la información relacionado con el domicilio del CLIENTE,  deberá ser comunicada de manera anticipada o a más tardar dentro de los siguientes cinco (5) días hábiles al momento en que se produzca el cambio.\n\nDécima Sexta. ARREGLO DIRECTO Y CLÁUSULA COMPROMISORA. Las Partes convienen en resolver sus diferencias mediante conciliación ante la Cámara de Comercio de Guayaquil; para tal efecto se dispondrá de un término  de 15 días comunes contados a partir de la fecha en que cualquiera de ellas requiera por escrito a la otra en tal sentido. Las comunicaciones podrán enviarse por correo, fax o telegrama a las direcciones señaladas de en este contrato. Evacuada la etapa de arreglo directo, las partes someterán cualquier diferencia que surja en relación con este contrato, exclusivamente  a un Tribunal de Arbitraje de conformidad con las reglas del Centro de Arbitraje de la Cámara de Comercio de Guayaquil. El Tribunal actuará conforme a las siguientes normas:\n1.El tribunal estará integrado por un (3) tres árbitros, seleccionamos de común acuerdo entre las partes. Si no hubiera acuerdo, serán designados mediante sorteo entre los árbitros  inscritos en las listas que se lleven en el Centro de Arbitraje de la Cámara de Comercio de Guayaquil.\n2.	El Tribunal decidirá en derecho y su fallo será final y obligatorio para las Partes.\n3.	El Tribunal funcionará en la ciudad de Guayaquil en el Centro de Arbitrajes de la Cámara de Comercio.\n4.	Los costos y gastos del arbitraje serán de cuenta de la parte que resultare vencida.\n\nPara constancia de lo anterior se firma en la ciudad de Guayaquil, a los…..días del mes de………………del año 20_, en tres ejemplares, uno para cada una de la partes y otro para el Vendedor Original.\n\nDe conformidad y para constancia las partes celebran el presente Contrato y firman 2 ejemplares de un solo tenor y a un solo efecto, en la fecha en el encabezamiento.");
            $pdf->PrintChapter(4,'',$P4);
            
            $pdf->Ln(5);    
            
            /***********************************************************************/
            
            $pdf->SetFont('arial', '', 9);     
            $pdf->SetFillColor(238,58,67);    
            $pdf->SetTextColor(255,255,255);   
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(190,4, utf8_decode("CONOCEMOS Y ACEPTAMOS LAS CONDICIONES EXPRESADAS AL DORSO DEL PRESENTE CONTRATO"),'T', 0,'C','true');     
            $pdf->Ln(4); 
              
            $pdf->SetFillColor(255,255,255);  
            $pdf->Cell(190, 50,'',1, 0,'L','true');
            
            $pdf->SetMargins(14,12,0,0);
            $pdf->Ln(4);
            
            $pdf->SetFillColor(255,255,255); 
            $pdf->SetFont('arial', '', 10);      
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Ln(15); 
            $pdf->Cell(30, 4, utf8_decode("Firma del Cliente:"),0, 0,'L');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(60, 4,"",'B', 0,'L');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(25, 4, utf8_decode("PYCCA S.A.:"),0, 0,'L');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(65, 4,"",'B', 0,'L');
            
            $pdf->Ln(5);  
            
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(20, 4, utf8_decode("Nombres:"),0, 0,'L');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(70, 4,$datos['no_cli'],'B', 0,'L');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(40, 4, utf8_decode("Nombre del Vendedor:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(50, 4,$datos['no_usu_pos'],'B', 0,'L','true');
            
            $pdf->Ln(5);  
            
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(20, 4, utf8_decode("Apellidos:"),0, 0,'L');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(70, 4,"",'B', 0,'L');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(40, 4, utf8_decode("Código del Vendedor:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(50, 4,$datos['co_usu_pos'],'B', 0,'L','true');
            
            $pdf->Ln(5);  
            
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(20, 4, utf8_decode("C. I. Nº:"),0, 0,'L');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(70, 4,$datos['id_cli'],'B', 0,'L');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(45, 4, utf8_decode("Establecimiento Comercial:"),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(45, 4,$datos['ds_bdg'],'B', 0,'L','true');
            
            $pdf->Ln(5);  
            
            $pdf->SetTextColor(0,66,126);
            $pdf->Cell(20, 4, utf8_decode("Teléfono:"),0, 0,'L');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(70, 4,$datos['tl_cli'],'B', 0,'L');
            $pdf->SetTextColor(0,66,126);        
            $pdf->SetLineWidth(0.1);  
            $pdf->Cell(2, 4, utf8_decode(""),0, 0,'L','true');
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(88, 4,"",'B', 0,'L','true');
            
            $pdf->SetMargins(10,0,0,0);
            $pdf->Ln(8);  
            
            $pdf->SetFont('arial', '', 9);     
            $pdf->SetFillColor(238,58,67);    
            $pdf->SetTextColor(255,255,255);   
            $pdf->Cell(190,4,utf8_decode("En caso de reclamo, presente este CONTRATO de Garantía Extendida y su correspondiente FACTURA DE COMPRA"),'B',0,'C','true');
            $pdf->SetTextColor(0,0,0);
            
            $pdf->Image(site_url().'img/1800.png',75,280,60);
            //$pdf->Output('CONTRATO_GARANTIA_EXT_'.$datos['co_con'].'.pdf','D');
            $pdf->Output();
        } else {
            $pdf->Ln(5);
            $pdf->SetFont('helvetica', 'B', 15);
            $pdf->Cell(190, 12, utf8_decode("NO HAY DATOS DEL CONTRATO A BUSCAR"),0, 0,'C','true');
            $pdf->Ln(20);
            $pdf->Output();
        }
    }
    
}

