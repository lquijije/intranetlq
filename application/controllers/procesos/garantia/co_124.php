<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_124 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/garantia/m_124'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/garantia/co_124');  
        $this->_crud(); 
        
    }        

    private function _crud(){
        
        $monthNames = Array();
        $monthNames[1]= "Enero";
        $monthNames[2]= "Febrero";
        $monthNames[3]= "Marzo";
        $monthNames[4]= "Abril";
        $monthNames[5]= "Mayo";
        $monthNames[6]= "Junio";
        $monthNames[7]= "Julio";
        $monthNames[8]= "Agosto";
        $monthNames[9]= "Septiembre";
        $monthNames[10]= "Octubre";
        $monthNames[11]= "Noviembre";
        $monthNames[12]= "Diciembre";
        
        $data = null; 
        $data['data_search'] = null;
        $data['permisos']='';   
        $data['cod_user']='';  
        $id_mes = $id_anio =  0;
        
        $urlControlador = 'procesos/garantia/co_124';
        $data['urlControlador'] = $urlControlador;
        $data['css'] = array('css/datatables/dataTables.bootstrap.css','css/datetimepicker/jquery.datetimepicker.css');
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js');

        $data['idFormulario'] = 'co_124';
        
        $post = $this->input->post();
        if(!empty($post)){
            $data['cod_user'] = $post['c_e'];
            $id_mes = $post['cmb_mes'];
            $id_anio = $post['cmb_anio'];
            $array = $this->m_124->retColaborador($post);
            $data['data_search'] = $array;
        }
        
        $id_mes = $id_mes == 0?date('m'):$id_mes;
        $id_anio = $id_anio == 0?date('Y'):$id_anio;
        
        $atributosEspecial="id='cmb_mes' class='form-control'";
        $data['cmb_mes']=form_dropdown('cmb_mes',$monthNames,$id_mes,$atributosEspecial); 
        
        $datosParametro=null;
        for($i = (int)date('Y')-2; $i <= (int)date('Y'); $i++){
            $datosParametro[$i]=$i;
        }
        
        $atributosEspecial="id='cmb_anio' class='form-control'";
        $data['cmb_anio']=form_dropdown('cmb_anio',$datosParametro,$id_anio,$atributosEspecial); 
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/garantia/v_124');
        $this->load->view('barras/footer');            
    }
    
}