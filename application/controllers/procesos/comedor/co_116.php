<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_116 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('proceso/comedor/m_116'));
        session_start();
    }

    public function _remap() {

        clear_cache_php();
        validaSession('procesos/comedor/co_116');

        $tipo = $this->uri->segment(4);
        $datos = $this->input->post();
        fnc_post($tipo, $datos, 'procesos/comedor/co_116');

        if ($tipo == 's_tic') {
            $this->_save();
        } else {
            $this->_crud();
        }
    }

    private function _crud() {
        $data = null;
        $data['permisos'] = '';
        $urlControlador = 'procesos/comedor/co_116';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array('css/timeline/timeline.css', 'css/fonts.css');

        $data['js'] = array('js/jquery-1.10.2.min.js',
            'js/general.js',
            'js/fechas.js',
            'js/procesos/comedor/j_116.js',
            'js/bootstrap.min.js');

        $data['idFormulario'] = 'co_116';

        $valida = $this->m_116->ValidaComedor();
        $bandera = false;

        if (isset($valida['status']) && $valida['status'] == "A") {
            $bandera = true;
        }

        if ($bandera == true && isset($valida['extra']) && (int) $valida['extra'] == 1) {
            $bandera = true;
        }

        $data['valida'] = $bandera;
        $comedor = $this->m_116->getComedor($valida['extra']);
        if ($_SERVER['REMOTE_ADDR'] == '130.1.39.49') {
//            print_r($comedor);
//            DIE;
        }
        $data['comedor'] = $comedor;
        $fec_ini = isset($comedor[0]['fecha']) && !empty($comedor[0]['fecha']) ? $comedor[0]['fecha'] : date('Y-m-d');
        $data['calendar'] = $this->m_116->ticketxDia(date('Y-m-d', strtotime('-10 day', strtotime($fec_ini))), date('Y-m-d', strtotime('-1 day', strtotime($fec_ini))));
        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/comedor/v_116');
        $this->load->view('barras/footer');
    }

    private function _save() {
        $datos = $this->input->post();
        $personas = $this->m_116->saveComedor($datos);
        echo json_encode($personas);
    }

}
