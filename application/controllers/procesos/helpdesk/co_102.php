<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_102 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation','mailer'));
        $this->load->model(array('proceso/helpdesk/m_102','proceso/helpdesk/m_101'));
        session_start();
    }

    public function _remap()  {
        
        clear_cache_php();
        
        if(empty($_SESSION)){
            redirect(site_url('procesos/helpdesk/co_102'));
        } 
        
        $tipo = $this->uri->segment(4);
        $datos = $this->input->post();
        
        fnc_post($tipo,$datos,'procesos/helpdesk/co_102');
        
        if($tipo=='retTipoSolicit'){
            $this->_retTipoSolicit();
        } else if($tipo=='saveAsignaciones'){
            $this->_saveAsignaciones();
        } else if($tipo=='retSoliAsig'){
            $this->_retSoliAsig();
        } else if($tipo=='retActiUser'){
            $this->_retActiUser();
        } else if($tipo=='UpdateAsig'){
            $this->_UpdateAsig();
        } else if($tipo=='actualizaPrioridadActividad'){
            $this->_ActualizaPrioridadActividad();
        } else if($tipo=='exportExcel'){
            $this->_ExportExcel();
        } else {
            $this->_crud();
        }
        
    }
    
    private function _crud(){
        
        $data = null; 
        $urlControlador = 'procesos/helpdesk/co_102';
        $data['urlControlador'] = $urlControlador;
        
        $opcion = BuscaIdMenu($data['urlControlador'],$_SESSION['menu']);
        $depar = $p = null;
        
        $fec_ini = date('Y-m-d', strtotime('-15 day'));
        $fec_fin = date('Y-m-d');
        
        if(!empty($opcion)){
            if (isset($_SESSION['menu'][$opcion]['opciones']) && !empty($_SESSION['menu'][$opcion]['opciones'])){
                $arrayBusq = $_SESSION['menu'][$opcion]['opciones'];
                foreach ($arrayBusq AS $row){
                    if (isset($row['DEPARTAMENTOS']) && !empty($row['DEPARTAMENTOS'])){
                        $depar = $row['DEPARTAMENTOS'];
                        $p = $depar[0];
                    }
                }  
            } else {
                $_SESSION['tit_ge'] = 'Helpdesk';
                $_SESSION['des_ge'] = 'No posee departamentos asignados <br>Usted puede comunicarse a la Ext.: 2222 con soporte a usuario.';
                redirect(site_url().'general/error_general');
            }
            
        }
        
        $get = $this->input->post('dpto');
        $f_i = $this->input->post('f_i');
        $f_f = $this->input->post('f_f');
        
        $getDpto = !empty($get)?$get:$p;
        $getFec_ini = !empty($f_i)?$f_i:$fec_ini;
        $getFec_fin = !empty($f_f)?$f_f:$fec_fin;
        
        $coment = $this->m_102->retComents($getDpto);
        $select = $this->m_102->retUserArea($getDpto);
        $SinAsignar = $this->m_102->retVenceAsignar('S',$getDpto);
        $PorVencer = $this->m_102->retVenceAsignar('P',$getDpto);
//        print_r($PorVencer);
//        die;
        $tablaMonitor = $this->m_102->retTableMonitor($getDpto,$getFec_ini,$getFec_fin);
        
        $data['fec_ini'] = $getFec_ini;
        $data['fec_fin'] = $getFec_fin;
        $data['getDpto'] = $getDpto;
        $data['dptos'] = $depar;
        $data['SinAsignar'] = $SinAsignar;
        $data['PorVencer'] = $PorVencer;
        $data['Select'] = $select;
        $data['tablaMonitor'] = $tablaMonitor;
        $data['coment'] = $coment;

        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                            'css/datetimepicker/jquery.datetimepicker.css',
                            'js/plugins/select2/dist/css/select2.css',
                            'css/daterangepicker/daterangepicker-bs3.css',
                            'css/ionslider/ion.rangeSlider.css',
                            'css/star.css',
                            'css/ionslider/ion.rangeSlider.skinFlat.css');

        $data['js'] = array('js/jquery-1.11.1.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/helpdesk/j_102.js',
                            'js/general.js',
                            'js/config.js',
                            'js/general_helpdesk.js',
                            'js/plugins/select2/dist/js/select2.js',
                            'js/plugins/select2/dist/js/i18n/es.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/daterangepicker/daterangepicker.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/ionslider/ion.rangeSlider.js');

        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/helpdesk/v_102'); 
        $this->load->view('barras/footer');
    }
    
    function _retTipoSolicit(){
        $datos = $this->input->post();
        $data = $this->m_102->retTipoSolicit($datos);
        echo $data;
    }
    
    
    function _saveAsignaciones(){
        $datos = $this->input->post();
        $data = $this->m_102->saveAsignaciones($datos);
        $da = $this->m_102->UpdateFecTop($datos);
        
        if (empty($datos['d_e'])){
            echo $da;
            die;
        }
        
        if (isset($data['co_error'])){

            if((int)$data['co_error'] === 0){
                
                $tipo = trim($datos['t_s']) === 'T'?'TICKET':'REQUERIMIENTO';
                $titulo= 'M&oacute;dulo Helpdesk';
                $descripcion = $tipo.' #'.trim($datos['c_s']).' ha sido asignado';
                $descripcion.= '<br>';
                $asunto= $tipo.' #'.trim($datos['c_s']).' ha sido asignado';
                $remitente= 'Helpdesk Intranet';
                $array = $this->m_101->getMailIngreSolic(trim($datos['c_s']));
                templ_mail($titulo,$descripcion,$asunto,$remitente,$array);
                
                $array = $this->m_101->getMailAsignado(trim($datos['c_s']),trim($datos['t_s']),trim($datos['i_d']));
                $descripcion = $tipo.' #'.trim($datos['c_s']).' ha sido asignado a los usuarios:<br>';
                $descripcion.='<ul>';
                foreach ($array as $row){
                    $descripcion.= '<li>'.$row['name'].'</li>';
                }
                $descripcion.='</ul>';
                templ_mail($titulo,$descripcion,$asunto,$remitente,$array); 
                
                echo 'Grabado Correctamente';
                
            } else {
                echo $data['tx_error'];
            }
            
        } else {
            echo $data;
        }
        
        
    }
    
    function _UpdateAsig(){
        $datos = $this->input->post();
        $data = $this->m_102->UpdateAsignacion($datos);
        echo $data;
    }
    
    function _retSoliAsig(){
        $cod_solici = $this->input->post('c_s');
        $data = $this->m_102->RetSolictAsig($cod_solici);
        echo $data;
    }
    
    function _retActiUser(){
        $cod_emple = $this->input->post('c_e');
        $id_dpto = $this->input->post('i_d');
        $data = $this->m_102->retActiUser($cod_emple,$id_dpto);
        echo $data;
    }
    
    function _ActualizaPrioridadActividad(){
        $orden = $this->input->post('order');
        $data = $this->m_102->ActualizaPrioridadActividad($orden);
        echo $data;
    }
    
    function _ExportExcel(){
        
        $data = $this->input->post();
        $datos = $this->m_102->retReportActiUser($data);
        
        if (!empty($datos)){
            $this->load->library('PHPExcel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->
            getProperties()
            ->setCreator("HELPDESK INTRANET")
            ->setTitle("REPORTE DE ACTIVIDADES")
            ->setSubject("REPORTE DE ACTIVIDADES")
            ->setDescription("REPORTE DE ACTIVIDADES")
            ->setKeywords("REPORTE DE ACTIVIDADES")
            ->setCategory("REPORTE DE ACTIVIDADES"); 

            $borders1 = array (
                'borders' => array (
                  'allborders' => array (
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array ('rgb' => '000000'),
                  )
               ),
            );

            $borders = array (
                'borders' => array (
                  'allborders' => array (
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array ('rgb' => '000000'),
                  )
               ),
            );
            
            $Letra = "L";
            $linea = 1; 

            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':'.$Letra.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('HELPDESK INTRANET', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':'.$Letra.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('REPORTE DE ACTIVIDADES', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':'.$Letra.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('DEPARTAMENTO - CARPINTERIA', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':'.$Letra.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('REPORTE DE ACTIVIDADES', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':D'.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('FECHA DEL REPORTE: '.date('Y/m/d h:i:s'), PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea = $linea + 4;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':F'.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':F'.$linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':F'.$linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':F'.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('SOLICITUD', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $objPHPExcel->getActiveSheet()->mergeCells('G'.$linea.':L'.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('G'.$linea.':L'.$linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('G'.$linea.':L'.$linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('G'.$linea.':L'.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('G'.$linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('G'.$linea)->setValueExplicit('ACTIVIDAD', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$linea, 'FECHA TOPE')
                            ->setCellValue('B'.$linea, 'FECHA DE INGRESO')
                            ->setCellValue('C'.$linea, 'TIPO')
                            ->setCellValue('D'.$linea, 'CODIGO')
                            ->setCellValue('E'.$linea, 'TITULO')
                            ->setCellValue('F'.$linea, 'ESTADO')
                            ->setCellValue('G'.$linea, 'STAFF')
                            ->setCellValue('H'.$linea, 'FECHA ASIGNACION')
                            ->setCellValue('I'.$linea, 'COD. ACTIVIDAD')
                            ->setCellValue('J'.$linea, 'TITULO')
                            ->setCellValue('K'.$linea, 'ATENDIDO')
                            ->setCellValue('L'.$linea, '% PROGRESO');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(12);

            foreach ($datos as $row){
                $linea++;
                $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->applyFromArray($borders);
                $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit($row['fe_tope'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('B'.$linea)->setValueExplicit($row['fe_ingreso'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('C'.$linea)->setValueExplicit($row['tipo'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('D'.$linea)->setValueExplicit($row['co_solicitud'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('E'.$linea)->setValueExplicit($row['tx_titulo'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('F'.$linea)->setValueExplicit($row['co_estado'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('G'.$linea)->setValueExplicit($row['nombre'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('H'.$linea)->setValueExplicit($row['fe_asignacion'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('I'.$linea)->setValueExplicit($row['co_actividad'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('J'.$linea)->setValueExplicit($row['tx_detalle'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('K'.$linea)->setValueExplicit($row['fl_siendo_atendido'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('L'.$linea)->setValueExplicit($row['va_progreso'], PHPExcel_Cell_DataType::TYPE_STRING);
            }

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

            $hoy = date("Y_m_d_h_i");  
            $objPHPExcel->getActiveSheet()->setTitle('LISTA'); // Renombrando el worksheet            
            $filename='REPORTE_DE_ACTIVIDADES_'.$hoy.'.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=1'); //no cache
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('temp/'.$filename);
            header("location: ".site_url()."temp/$filename"); 
            unset($detalle,$datos);
            ///exit;

        } else {
            echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
            echo " <script> window.close();</script> ";
        }

        
   }
    
    
}