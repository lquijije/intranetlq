<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_101 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation','mailer'));
        $this->load->model(array('proceso/helpdesk/m_101'));
        session_start();
    }	

    public function _remap()  {    
        
        clear_cache_php();

        if(empty($_SESSION)){
            redirect(site_url('procesos/helpdesk/co_102'));
        } 
        
        $tipo = $this->uri->segment(4);
        $datos = $this->input->post();
        
        fnc_post($tipo,$datos,'procesos/helpdesk/co_101');
        
        if($tipo=='guardar'){
            $this->_guardar();
        } else if($tipo == 'retCodSolicit'){
            $this->_retCodSolicit();
        } else if($tipo == 'guardarComent'){
            $this->_guardarComent();
        } else if($tipo == 'retCodSolictComent'){
            $this->_retCodSolictComent();
        } else if($tipo == 'getAllSolicit'){
            $this->_getAllSolicit();
        } else if($tipo == 'cancelSolicit'){
            $this->_cancelSolicit();
        } else if($tipo == 'finSolicit'){
            $this->_finSolicit();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud(){
        $data = null; 
        $urlControlador = 'procesos/helpdesk/co_101';
        $data['urlControlador'] = $urlControlador;
        
        $opcion = BuscaIdMenu($data['urlControlador'],$_SESSION['menu']);
        $depar = null;
        
        if(!empty($opcion)){
            
            if(isset($_SESSION['menu'][$opcion]['opciones']) && !empty($_SESSION['menu'][$opcion]['opciones'])){
               
                $arrayBusq = $_SESSION['menu'][$opcion]['opciones'];
                foreach ($arrayBusq AS $row){

                    if (isset($row['DEPARTAMENTOS']) && !empty($row['DEPARTAMENTOS'])){
                        $depar = $row['DEPARTAMENTOS'];
                        $p = $depar[0];
                    }

                } 
                
            } else {
                $_SESSION['tit_ge'] = 'Helpdesk';
                $_SESSION['des_ge'] = 'No posee departamentos asignados <br>Usted puede comunicarse a la Ext.: 2222 con soporte a usuario.';
                redirect(site_url().'general/error_general');
            }
        }
        
        $get = $this->input->post('dpto');
        $getDpto = !empty($get)?$get:$p;
        $selectTipo = $this->m_101->solicitudSelect($getDpto);
        
        $sel = '<select id="cmb_tipo" name="cmb_tipo" class="form-control" onchange="ch_tp(this.value)">';
        $sel .= '<option value="S">Seleccionar</option>';
        if (!empty($selectTipo)){
            foreach ($selectTipo as $fila){
                $sel .= '<option value="'.$fila['co_tipo'].'">'.$fila['tx_descripcion'].'</option>';
            }
        }
        $sel .= '</select>';
        
        $TopSolic = $this->m_101->retTopSolicit($_SESSION['c_e'],$getDpto);
        $arraySolici = '';
        if(isset($TopSolic) && !empty($TopSolic)){
            foreach ($TopSolic as $row){
                $arraySolici.=$row['co_solicitud'].",";
            }
        }
        $coment = $this->m_101->retComents(trim($arraySolici,','));
        $estados = $this->m_101->retEstadoAgrupados($getDpto);
        $data['getDpto'] = $getDpto;
        $data['dptos'] = $depar;
        $data['coment'] = $coment;
        $data['TopSolic'] = $TopSolic;
        $data['estados'] = $estados;
        $data['selectTipo'] = $sel;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                            'css/datetimepicker/jquery.datetimepicker.css',
                            'js/plugins/ckeditor/samples/sample.css');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/helpdesk/j_101.js',
                            'js/general.js',
                            'js/general_helpdesk.js',
                            'js/config.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/ckeditor/ckeditor.js');
                      
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/helpdesk/v_101'); 
        $this->load->view('barras/footer');
    }
    
    private function _retCodSolicit(){
        $datos = $this->input->post();
        $data = $this->m_101->retCodSolict($datos);
        echo $data;
    }
    
    private function _guardarComent(){
        $datos = $this->input->post();
        $data = $this->m_101->guardarComent($datos);
        echo $data;
    }
    
    private function _retCodSolictComent(){
        $datos = $this->input->post();
        $data = $this->m_101->retCodSolictComent($datos);
        echo $data;
    }
    
    private function _getAllSolicit(){
        $datos = $this->input->post();
        $data = $this->m_101->getAllSolicit($datos);
        echo $data;
    }
    
    private function _cancelSolicit(){
        $datos = $this->input->post();
        $data = $this->m_101->cancelSolicit($datos);
        if($data = 'OK'){
            $tipo = trim($datos['t_s']) === 'T'?'TICKET':'REQUERIMIENTO';
            $titulo= 'M&oacute;dulo Helpdesk';
            $descripcion = $tipo.' #'.trim($datos['c_s']).' ha sido cancelado por el usuario '.utf8_encode($_SESSION['nombre']).' ';
            $descripcion.= '<br>';
            $asunto= $tipo.' #'.trim($datos['c_s']).' ha sido cancelado';
            $remitente= 'Helpdesk Intranet';
            $array = $this->m_101->getMailAsignado(trim($datos['c_s']),trim($datos['t_s']),trim($datos['i_d']));
            $arr = $array;
            templ_mail($titulo,$descripcion,$asunto,$remitente,$arr); 
        }
        echo $data;
        
    }
    
    private function _finSolicit(){
        $datos = $this->input->post();
        $data = $this->m_101->finSolicit($datos);
        if($data = 'OK'){
            $tipo = trim($datos['t_s']) === 'T'?'TICKET':'REQUERIMIENTO';
            $titulo= 'M&oacute;dulo Helpdesk';
            $descripcion = $tipo.' #'.trim($datos['c_s']).' ha sido finalizado por el usuario '.utf8_encode($_SESSION['nombre']).' ';
            $descripcion.= '<br>';
            $asunto= $tipo.' #'.trim($datos['c_s']).' ha sido finalizado';
            $remitente= 'Helpdesk Intranet';
            $array = $this->m_101->getMailAsignado(trim($datos['c_s']),trim($datos['t_s']),trim($datos['i_d']));
            $arr = $array;
            templ_mail($titulo,$descripcion,$asunto,$remitente,$arr); 
        }
        echo $data;
    }
    
    private function _guardar(){
        $datos = $this->input->post();
        $mensaje = $this->m_101->guardar($datos);
        if (substr($mensaje,0,2) === 'OK'){
            if (trim($datos['b_a']) === 'A'){
                $men_array = explode('|',$mensaje);
                if((int)$men_array[1] <> 1){
                    $array = $this->m_101->getMailDpto($men_array[1],$men_array[2]);
                    $this->_enviarMail($men_array[2],$array,$men_array[3],$men_array[4],trim(utf8_decode(str_replace("'",'"',$datos['t_o']))),trim($datos['d_n']));
                }
            }
            echo 'OK';
        } else {
            echo $mensaje;
        }
    }
    
    private function _enviarMail($tipo,$arr,$c_s,$f_i,$t_o,$d_n){
        
        $tipo = $tipo === 'T'?'TICKET':'REQUERIMIENTO';
	$host = "130.1.10.232";
	$puerto = "25";		
	$usuario = "sqlmail@pycca.com";		
	$clave = "Sqlma1l";
	$responder_a = "sqlmail@pycca.com";
        $asunto = $tipo." #".$c_s." - ".$t_o;
        $cuerpo = '
        <center style="background-color:#E1E1E1;">
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
            	<tr>
                    <td align="center" valign="top" id="bodyCell">
                    	<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="900" id="emailBody">
                            <tr>
                            	<td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:5px solid #F0514A;background-color:#00539F; position:relative;" bgcolor="#00539F">
                                    	<tr>
                                            <td align="center" valign="top">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="900" class="flexibleContainer">
                                                    <tr>
                                                    	<td align="center" valign="top" width="900" class="flexibleContainerCell">
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="center" valign="top" class="textContent">
                                                                        <img src="http://www.pycca.com/images/logo-hover.png" width="105" height="32" style="position: absolute;left: 25px;"/>
                                                                        <h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">M&oacute;dulo Helpdesk</h1>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                            	<td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
                                    	<tr>
                                            <td align="center" valign="top">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="900" class="flexibleContainer">
                                                    <tr>
                                                    	<td align="center" valign="top" width="900" class="flexibleContainerCell">
                                                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="center" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left" valign="top" class="flexibleContainerBox">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                                        <tr>
                                                                                            <td align="left" class="textContent">
                                                                                                <h3 style="line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Ingreso la Solicitud:</h3>
                                                                                                <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;line-height:135%;color:#333;">'.trim($_SESSION['nombre']).'</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td align="right" valign="top" class="flexibleContainerBox">
                                                                                    <table class="flexibleContainerBoxNext" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                                        <tr>
                                                                                            <td align="left" class="textContent">
                                                                                                <h3 style="line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Fecha de Ingreso</h3>
                                                                                                <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;line-height:135%;color:#333;">'.date('d/m/Y (h:i)',strtotime($f_i)).'</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="textContent">
                                                                                    <h3 style="line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;margin-top: 15px;">T&iacute;tulo:</h3>
                                                                                    <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;line-height:135%;color:#333;">'.$t_o.'</div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="textContent">
                                                                                    <h3 style="line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;margin-top: 15px;">Descripci&oacute;n</h3>
                                                                                    <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;line-height:135%;color:#333;">'.$d_n.'</div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                     
                        <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="900" id="emailFooter">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="900" class="flexibleContainer">
                                                    <tr>
                                                        <td align="center" valign="top" width="900" class="flexibleContainerCell">
                                                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" bgcolor="#E1E1E1">
                                                                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                        <div>Copyright &#169; 2015 Intranet Pycca</div>
                                                                        <div>Este email ha sido generado autom&aacute;ticamente. Por favor no escribir a esta cuenta. </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>';
        
        $cabecera= "MIME-Version: 1.0\r\n";
        $cabecera .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
        $cabecera .= "From: Helpdesk\r\n";
            
        $mail = new PHPMailer();
        $mail->IsSMTP(); 
        $mail->PluginDir = "";
        $mail->Host = $host;
        $mail->Port=$puerto;
        $mail->SMTPAuth = true;
        $mail->Username = $usuario;
        $mail->Password = $clave;
        $mail->From = $responder_a;
        $mail->FromName = "Helpdesk Intranet";
        $mail->WordWrap = 50;
        $mail->Subject = $asunto;
        //$mail->AddAddress('frankcisvm@gmail.com','FVERA');
        foreach($arr as $row){
            if(!empty($row['mail'])){
                $mail->AddAddress($row['mail'],$row['nom_empl']);
            }
        }
        
        $mail->Body = $cuerpo;
        $mail->IsHTML(true);
        
        if(!$mail->Send()){
            echo "ERROR: " . $mail->ErrorInfo; 	
        } else {
           return "OK";	
        }	        
            
    }

}