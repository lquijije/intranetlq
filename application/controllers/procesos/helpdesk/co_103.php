<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_103 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation','mailer'));
        $this->load->model(
                array(
                    'proceso/helpdesk/m_102',
                    'proceso/helpdesk/m_103',
                    'proceso/helpdesk/m_101'
                )
        );
        session_start();
    }	

    public function _remap()  {    
        
        clear_cache_php();
            
        if(empty($_SESSION)){
            redirect(site_url('procesos/helpdesk/co_102'));
        } 
        
        $tipo = $this->uri->segment(4);
        $datos = $this->input->post();
        
        fnc_post($tipo,$datos,'procesos/helpdesk/co_103');
        
        if($tipo=='getAllActivt'){
            $this->_getAllActivt();
        } else if($tipo=='enCurso'){
            $this->_enCurso();
        } else if($tipo=='savePorcTiempActi'){
            $this->_savePorcTiempActi();
        } else if($tipo=='retTipoSolicit'){
            $this->_retTipoSolicit();
        } else if($tipo=='retSoliAsig'){
            $this->_retSoliAsig();
        } else if($tipo=='saveAsignaciones'){
            $this->_saveAsignaciones();
        } else if($tipo=='UpdateAsig'){
            $this->_UpdateAsig();
        } else {
            $this->_crud();
        }
    }
        
    private function _crud(){
        $data = null; 
        $urlControlador = 'procesos/helpdesk/co_103';
        $data['urlControlador'] = $urlControlador;
        
        $opcion = BuscaIdMenu($data['urlControlador'],$_SESSION['menu']);
        $depar = null;
        
        if(!empty($opcion)){
            if (isset($_SESSION['menu'][$opcion]['opciones']) && !empty($_SESSION['menu'][$opcion]['opciones'])){
                $arrayBusq = $_SESSION['menu'][$opcion]['opciones'];
                foreach ($arrayBusq AS $row){
                    if (isset($row['DEPARTAMENTOS']) && !empty($row['DEPARTAMENTOS'])){
                        $depar = $row['DEPARTAMENTOS'];
                        $p = $depar[0];
                    }
                }
                
            } else {
                $_SESSION['tit_ge'] = 'Helpdesk';
                $_SESSION['des_ge'] = 'No posee departamentos asignados <br>Usted puede comunicarse a la Ext.: 2222 con soporte a usuario.';
                redirect(site_url().'general/error_general');
            }
            
        }
        
        $get = $this->input->post('dpto');
        $getDpto = !empty($get)?$get:$p;
        
        $solicitudes = '';
        $salto = '';
        $pendientes = $this->m_103->retActiUser($getDpto,$_SESSION['c_e'],'','');
        for($i = 0; $i < count($pendientes); $i++){
            if($salto <> (int)$pendientes[$i]['co_solicitud']){
                $solicitudes.= $pendientes[$i]['co_solicitud'].",";
            }
            $salto = (int)$pendientes[$i]['co_solicitud'];
        }
        
        $coment = $this->m_103->retComents(trim($solicitudes,','));
        $estados  = $this->m_103->retEstadoAgrupados($_SESSION['c_e']);
        $select = $this->m_103->retUserArea($getDpto);
        $data['getDpto'] = $getDpto;
        $data['dptos'] = $depar;
        $data['coment'] = $coment;
        $data['pendientes'] = $pendientes;
        $data['estados'] = $estados;
        $data['Select'] = $select;
        
        $SinAsignar = $this->m_102->retVenceAsignar('S',$getDpto);
        $PorVencer = $this->m_102->retVenceAsignar('P',$getDpto);
        $data['SinAsignar'] = $SinAsignar;
        $data['PorVencer'] = $PorVencer;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                            'css/datetimepicker/jquery.datetimepicker.css',
                            'js/plugins/select2/dist/css/select2.css',
                            'css/ionslider/ion.rangeSlider.css',
                            'css/ionslider/ion.rangeSlider.skinFlat.css');

        $data['js'] = array('js/jquery-1.11.1.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/helpdesk/j_103.js',
                            'js/general.js',
                            'js/config.js',
                            'js/general_helpdesk.js',
                            'js/plugins/select2/dist/js/select2.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/ionslider/ion.rangeSlider.js');
                      
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/helpdesk/v_103'); 
        $this->load->view('barras/footer');
    }
    
    private function _retTipoSolicit(){
        $datos = $this->input->post();
        $data = $this->m_102->retTipoSolicit($datos);
        echo $data;
    }
    
    private function _retSoliAsig(){
        $cod_solici = $this->input->post('c_s');
        $data = $this->m_102->RetSolictAsig($cod_solici);
        echo $data;
    }
    
    private function _saveAsignaciones(){
        $datos = $this->input->post();
        $data = $this->m_102->saveAsignaciones($datos);
        
        if (isset($data['co_error'])){

            if((int)$data['co_error'] === 0){
                
                $tipo = trim($datos['t_s']) === 'T'?'TICKET':'REQUERIMIENTO';
                $titulo= 'M&oacute;dulo Helpdesk';
                $descripcion = $tipo.' #'.trim($datos['c_s']).' ha sido asignado';
                $descripcion.= '<br>';
                $asunto= $tipo.' #'.trim($datos['c_s']).' ha sido asignado';
                $remitente= 'Helpdesk Intranet';
                
                $a = array(
                    'name'=>$_SESSION['nombre'],
                    'e_mail'=>$_SESSION['e_mail']
                );
                
                templ_mail($titulo,$descripcion,$asunto,$remitente,$a);
                
                $tipo = trim($datos['t_s']) === 'T'?'TICKET':'REQUERIMIENTO';
                $array = $this->m_101->getMailAsignado(trim($datos['c_s']),trim($datos['t_s']),trim($datos['i_d']));
                $titulo= 'M&oacute;dulo Helpdesk';
                $descripcion = $tipo.' #'.trim($datos['c_s']).' ha sido asignado a los usuarios:<br>';
                $descripcion.='<ul>';
                foreach ($array as $row){
                    $descripcion.= '<li>'.$row['name'].'</li>';
                }
                $descripcion.='</ul>';
                $asunto= $tipo.' #'.trim($datos['c_s']).' ha sido asignado';
                $remitente= 'Helpdesk Intranet';
                templ_mail($titulo,$descripcion,$asunto,$remitente,$array); 
                
                echo 'Grabado Correctamente';
                
            } else {
                echo $data['tx_error'];
            }
            
        } else {
            echo $data;
        }
        
    }
    
    private function _UpdateAsig(){
        $datos = $this->input->post();
        $data = $this->m_102->UpdateAsignacion($datos);
        echo $data;
    }
    
    private function _getAllActivt(){
        $fec_ini = $this->input->post('f_i');
        $fec_fin = $this->input->post('f_f');
        $cod_dpto = $this->input->post('c_d');
        $data = $this->m_103->retActiUser($cod_dpto,$_SESSION['c_e'],$fec_ini,$fec_fin);
        echo json_encode($data);
    }
    
    private function _enCurso(){
        $id = $this->input->post('i_d');
        $id_solici = $this->input->post('i_s');
        $tipo = $this->input->post('t_p');
        
        $data = $this->m_103->enCurso($id_solici,$id,$tipo);
        echo $data;
    }
    
    private function _savePorcTiempActi(){
        $datos = $this->input->post();
        $data = $this->m_103->savePorcTiempActi($datos);
        
        if((int)$data['co_error'] === 0){
            
            if ((int)$data['tx_comprobar'] === 1){
                $tipo = trim($datos['t_s']) === 'T'?'TICKET':'REQUERIMIENTO';
                $titulo= 'M&oacute;dulo Helpdesk';
                $descripcion = $tipo.' #'.trim($datos['c_s']).' ha pasado de estado a comprobar ';
                $descripcion.= '<br>Por favor verifique si el '.$tipo.' se encuentra completamente realizado';
                $descripcion.= '<br>Luego de verificar si se encuentra comprobado lo puede finalizar.';
                $asunto= $tipo.' #'.trim($datos['c_s']).' ha pasado de estado a comprobar';
                $remitente= 'Helpdesk Intranet';
                $array = $this->m_101->getMailAsignado(trim($datos['c_s']),trim($datos['t_s']),trim($datos['i_d']));
                $arr = $array;
                templ_mail($titulo,$descripcion,$asunto,$remitente,$arr); 
            }
            
            echo 'OK';
            
        } else {
            echo $data['tx_error'];
        }

    }   
    
}