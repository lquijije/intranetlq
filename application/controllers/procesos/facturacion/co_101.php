<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_101 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation'));
        $this->load->model(array('proceso/facturacion/m_101'));
        session_start();
    }	

    public function _remap()  {    
        
        validaSession('procesos/facturacion/co_101');  
        
        $tipo=$this->uri->segment(4);

        if($tipo=='retDocuErrorDetalle'){
            $this->_retDocuErrorDetalle();
        } else if($tipo=='retDocuError'){
            $this->_retDocuError();
        } else if($tipo=='modificaDoc'){
            $this->_modificaDoc();
        } else {
            $this->_crud();
        }
    }
        
    private function _crud(){
        
        $data = null; 
        $urlControlador = 'procesos/facturacion/co_101';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css','css/datetimepicker/jquery.datetimepicker.css');
                
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/facturacion/f_101.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js');
        
        $tipo = $this->input->get('tp', TRUE);
        $fecha_inicial = $this->input->get('fi', TRUE);
        $fecha_final = $this->input->get('ff', TRUE);
        
        $data['tipoDoc'] = $tipo;
        $data['fecha_inicial'] = $fecha_inicial;
        $data['fecha_final'] = $fecha_final;
        $data['fecha_final'] = $fecha_final;
                                    
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/facturacion/detalle-documentos'); 
        $this->load->view('barras/footer');
    }
    
    private function _retDocuError(){
        $datos = $this->input->post();
        $data = $this->m_101->retDetDocElectronicos($datos);
        echo $data;
        
    }
    
    private function _retDocuErrorDetalle(){
        
        $fec_ini = $this->input->post('f_i');
        $fec_fin = $this->input->post('f_f');
        $tipo = $this->input->post('tp');
        $almacen = $this->input->post('a_l');
        $ip = $this->input->post('i_p');
        $estado = $this->input->post('est');
        $tipo_conexion = $this->input->post('t_c');
        
        $result = $this->m_101->GetDetalleDocumentosELectronicos($fec_ini,$fec_fin,$tipo,$almacen,$ip,$estado,$tipo_conexion);
        
        if (!empty($result)){
            
            $nomTipo = '';
            if (trim($tipo) === 'F'){
                $nomTipo = 'FACTURA';
            } else if (trim($tipo) === 'V'){
                $nomTipo = 'NOTA DE CRÉDITO';
            } else if (trim($tipo) === 'E'){
                $nomTipo = 'RETENCIONES';
            } else if (trim($tipo) === 'G'){
                $nomTipo = 'GUIAS DE REMISIÓN';
            } 
            
            $clase = '';
            $datos = null;

            foreach ($result['data'] as $row){  

                if((trim($row['estado']) === 'RECHAZADO POR CIMA') || (trim($row['estado']) === 'NOVEDAD CON XML - COMUNIQUE A SISTEMAS') || (trim($row['estado']) === 'XML NO GENERADO - COMUNIQUE A SISTEMAS') || (trim($row['estado']) === 'INESPERADO - COMUNIQUE A SISTEMAS')){
                    $clase ='negada';
                } else if((trim($row['estado']) === 'NO AUTORIZADO') || (trim($row['estado']) === 'CONTINGENCIA')){
                    $clase ='estado_3';
                } else if((trim($row['estado']) === 'ENCOLADO EN CIMA')){
                    $clase ='estado_2';
                } else if((trim($row['estado']) === 'ESPERANDO AUTORIZACION SRI')){
                    $clase ='aprobada';
                } else if((trim($row['estado']) === 'AUTORIZADO')){
                    $clase ='estado_1';
                }
                
                $datos[] = array(
                    'tip_doc'=>trim($nomTipo),           
                    'fec_tra'=> $row['fe_transaccion'],
                    'cod_bdg'=> trim($row['co_bodega']),
                    'cod_caj'=> trim($row['co_caja']),
                    'tip_doc'=> trim($row['ti_documento']),
                    'num_doc'=> trim($row['nu_documento']),
                    'det_xml'=> utf8_encode(trim($row['de_xml'])),
                    'cod_est'=> trim($row['estado']),
                    'det_con'=> trim($row['de_consulta']),
                    'num_aut'=> trim($row['nu_autorizacion']),
                    'doc_doc'=> trim($row['documento']),
                    'val_tra'=> trim($row['va_totaltransaccion']),
                    'cod_cli'=> trim($row['co_cliente']),
                    'cod_cla'=> trim($clase),
                    'fec_env'=> $row['fe_envio'],
                    'det_env'=> trim($row['de_envio']),
                    'fec_con'=> $row['fe_consulta']
                );

            }
            echo "OK-".json_encode(array('data'=>$datos,'error'=>$result['error'])); 
            
        }
    }
    
    private function _modificaDoc(){
        require_once('lib/nusoap.php');
        $serverURL = $this->config->item('web_service'); 
        $serverScript = 'server.php'; 
        
        $xml= $this->input->post('x_l');
        $data= $this->input->post('data');
        
        $arr = explode('/*/', $data);
        $num_doc = $arr[0];
        $ip = $arr[1];
        $tipo = $arr[2];
        
        $client = new nusoap_client($serverURL."/".$serverScript."?wsdl", 'wsdl');

        $err = $client->getError();
        if ($err) {
            echo '<h2>Constructor error</h2>' . $err;
            exit();
        }
        
        $result = $client->call('updateDocElect', 
            array('ip' => $ip,'num_doc' => $num_doc,'xml' => $xml,'tipo' => $tipo)
        );
        
        if ($client->fault) {
            echo '<h2>Fault</h2><pre>';
            print_r($result);
            die;
            echo '</pre>';
        } else {
            $err = $client->getError();
            if ($err) {
                echo "ERROR-".$err;
                die;
            } 
        }
        
        print_r($result);
        die;
    }
    
    
}