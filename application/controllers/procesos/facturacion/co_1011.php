<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_1011 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/facturacion/m_1011'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/facturacion/co_1011');  
        $this->_crud(); 
        
    }        

    private function _crud(){
        
        $post = $this->input->post();
        if(empty($post['c_u'])){
            $_SESSION['verifica'] = 0;
        }
        $data = null; 
        $data['permisos']='';   
        $data['cod_ci']='';  
        $data['message'] = '';
        $data['dataFec'] = null;
        
        $urlControlador = 'procesos/facturacion/co_1011';
        $data['urlControlador'] = $urlControlador;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/general.js');

        $data['idFormulario'] = 'co_1011';

        $this->form_validation->set_rules('c_u', 'C&eacute;dula', 'trim|required|max_length[13]|xss_clean'); 
        
        if ($this->form_validation->run() == TRUE){
            if(!empty($post['c_u'])){
                $data['cod_ci'] = $post['c_u'];
                if((int)$_SESSION['verifica'] == 0){
                    $data['dataFec'] = $this->m_1011->retHistPass($post['c_u']);
                    $_SESSION['verifica'] = 1;
                } else {
                    $mensaje = $this->resetPassFactElec($post['c_u']);   
                    $data['cod_ci']=''; 
                    $data['message'] = $mensaje;
                    $_SESSION['verifica'] = 0;
                }
            }
        }
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/facturacion/v_1011');
        $this->load->view('barras/footer');            
    }
    
    private function resetPassFactElec($ci){
        
        validaSession('procesos/facturacion/co_1011'); 
        
        if(!empty($ci)){
            
            $ban = false;
            
            if(strlen($ci) == 10){
                $result = validaCedula($ci);
                if((int)$result['co_err'] == 0){
                    $ban = true;
                } else {
                    return $result['tx_err'];
                }
            } else {
                $ban = true;
            }
            
            if($ban){
                return $this->_wsdlPassFactElec($ci);
            }
        }
        
    }
    
    private function _wsdlPassFactElec($ci){
        require_once('lib/nusoap.php');
        $mensaje = '';
        $client = new nusoap_client("http://130.1.80.70:8080/WebServicesFacturacion/WsFacturacionComprobantes?wsdl", 'wsdl');
        $respuesta = $client->call('cliente.restaurarClave',array('rucEmpresa'=>'0990000530001','usuario'=>$ci,'nuevaClave'=>''));
        
        $err = $client->getError();
        if ($err) {
            $mensaje = "ERROR: ".$err;
        } else {
            //$mensaje = (string)$client->response;
            //$pos = strpos($mensaje,'<soap:Envelope');
            //$string = substr($mensaje,$pos);
            $mensaje = $respuesta['return'];
            $mensaje = trim($mensaje) === ''?'Usuario reseteado con exito':$respuesta['return'];
            $this->m_1011->saveHistPass($ci);
        }
        return $mensaje;
    }
    
    
}