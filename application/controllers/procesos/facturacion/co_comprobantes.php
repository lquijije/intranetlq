<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_comprobantes extends CI_Controller {

    protected $url_controlador;

    function __construct() {
        parent::__construct();
        $this->url_controlador = 'procesos/facturacion/co_comprobantes';
        $this->load->model(array('proceso/facturacion/m_comprobantes'));
        session_start();
    }

    public function _remap() {

        $datos = $this->input->post();
        $tipo = $this->uri->segment(4);

        validaSession($this->url_controlador);
        //fnc_post($tipo, $datos, $this->url_controlador);

        switch ($tipo) {
            case 'reEnviados':
                $this->_consultaDocumentosEnviados();
                break;
            case 'reRecibidos':
                $this->_consultaDocumentosRecibidos();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {

        $data = null;

        $data['css'] = array(
            'js/plugins/tab/tab.css',
            'css/datatables/dataTables.bootstrap.css',
            'js/plugins/perfect-scrollbar/css/perfect-scrollbar.css',
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/general.js',
            'js/config.js',
            'js/plugins/tab/tab.js',
            'js/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
            'js/plugins/tablefixed/jquery.floatThead.js',
            'js/procesos/facturacion/j_comprobantes.js',
            'js/plugins/flot/jquery.flot.min.js',
            'js/plugins/flot/jquery.flot.resize.min.js',
            'js/plugins/flot/jquery.flot.pie.min.js',
            'js/plugins/flot/Smooth-0.1.7.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js'
        );

        $cmb_anios = $cmb_meses = $cmb_dias = $cmb_estados = $cmb_documentos = null;

//        $estados = $this->m_comprobantes->retEstadosDocumentos();
//        if (!empty($estados)) {
        $cmb_estados['NULL'] = "";
        $cmb_estados['AT'] = 'AUTORIZADO';
        $cmb_estados['CT'] = 'CONTINGENCIA';
        $cmb_estados['DV'] = 'DEVUELTO';
        $cmb_estados['NA'] = 'NO AUTORIZADO';
        $cmb_estados['RS'] = 'RECIBIDO';
//            foreach ($estados as $row) {
//                $cmb_estados[$row['co_est']] = $row['no_est'];
//            }
//        } else {
//            $cmb_estados['NNN'] = "Sin estados";
//        }

        for ($i = 2014; $i <= date('Y'); $i++) {
            $cmb_anios[$i] = $i;
        }

        for ($i = 1; $i <= 12; $i++) {
            $cmb_meses[$i] = conveMes($i);
        }

        for ($i = 1; $i <= 31; $i++) {
            $cmb_dias[$i] = $i;
        }

        $cmb_documentos['NULL'] = '';
        $cmb_documentos['01'] = 'FACTURA';
        $cmb_documentos['04'] = 'NOTA DE CREDITO';
        $cmb_documentos['05'] = 'NOTA DE DEBITO';
        $cmb_documentos['06'] = 'GUIA DE REMISION';
        $cmb_documentos['07'] = 'COMPROBANTE DE RETENCION';

        $cmb_documentosRec['NULL'] = '';
        $cmb_documentosRec['01'] = 'FACTURA';
        $cmb_documentosRec['04'] = 'NOTA DE CREDITO';
        $cmb_documentosRec['05'] = 'NOTA DE DEBITO';
        $cmb_documentosRec['07'] = 'GUIA DE REMISION';
        $cmb_documentosRec['06'] = 'COMPROBANTE DE RETENCION';

        $data['cmb_anio'] = form_dropdown('cmb_anio', $cmb_anios, date('Y'), "id='cmb_anio' class='form-control form-control-sm'");
        $data['cmb_meses'] = form_dropdown('cmb_meses', $cmb_meses, date('m'), "id='cmb_meses' class='form-control form-control-sm'");
        $data['cmb_dia'] = form_dropdown('cmb_dia', $cmb_dias, date('d'), "id='cmb_dia' class='form-control form-control-sm'");
        $data['cmb_estados'] = form_dropdown('cmb_estados', $cmb_estados, date('m'), "id='cmb_estados' class='form-control form-control-sm'");
        $data['cmb_documentos'] = form_dropdown('cmb_documentos', $cmb_documentos, "NULL", "id='cmb_documentos' class='form-control form-control-sm'");

        $data['cmb_anio_rec'] = form_dropdown('cmb_anio_rec', $cmb_anios, date('Y'), "id='cmb_anio_rec' class='form-control form-control-sm'");
        $data['cmb_meses_rec'] = form_dropdown('cmb_meses_rec', $cmb_meses, date('m'), "id='cmb_meses_rec' class='form-control form-control-sm'");
        $data['cmb_dia_rec'] = form_dropdown('cmb_dia_rec', $cmb_dias, date('d'), "id='cmb_dia_rec' class='form-control form-control-sm'");
        $data['cmb_estados_rec'] = form_dropdown('cmb_estados_rec', $cmb_estados, date('m'), "id='cmb_estados_rec' class='form-control form-control-sm'");
        $data['cmb_documentos_rec'] = form_dropdown('cmb_documentos_rec', $cmb_documentosRec, "NULL", "id='cmb_documentos_rec' class='form-control form-control-sm'");

        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/facturacion/v_comprobantes');
        $this->load->view('barras/footer');
    }

    private function _consultaDocumentosEnviados() {
        ini_set('memory_limit', '-1');
        $tot = '';
        $co_anio = $this->input->post('c_an');
        $co_mes = $this->input->post('c_me');
        $co_dia = $this->input->post('c_di');
        $co_autorizacion = $this->input->post('c_au');
        $co_documento = $this->input->post('c_do');
        $co_identificacion = $this->input->post('c_id');
        $co_establecimiento = $this->input->post('c_es');
        $co_puntoemision = $this->input->post('c_pe');
        $co_secuencial = $this->input->post('c_se');

        $result = $this->m_comprobantes->retDocumentosEmitidos($co_anio, $co_mes, $co_dia, $co_autorizacion, $co_documento, $co_identificacion, $co_establecimiento, $co_puntoemision, $co_secuencial);
        $array = null;
        if (empty($result['err'])) {
            if (!empty($result['dat'])) {
                $array['columns'] = array(
                    array('title' => 'Emisi&oacute;n', 'data' => 'fe_emi'),
                    array('title' => 'Docum.', 'data' => 'tp_doc'),
                    array('title' => 'Cliente/Prov./Receptor', 'data' => 'ra_soc'),
                    array('title' => 'Identificaci&oacute;n', 'data' => 'id_com'),
                    array('title' => 'Nro. Documento', 'data' => 'nu_fac'),
                    array('title' => 'Fecha Autorizaci&oacute;n', 'data' => 'fe_aut'),
                    array('title' => 'Tipo Autorizaci&oacute;n', 'data' => 'no_es_aut'),
                    array('title' => 'Nro. Autorizaci&oacute;n', 'data' => 'nu_aut'),
                    array('title' => 'Clave Acceso', 'data' => 'cl_acc'),
                    array('title' => 'Base 12', 'data' => 'sub_12'),
                    array('title' => 'Base 0', 'data' => 'sub_0'),
                    array('title' => 'Iva', 'data' => 'iva_12'),
                    array('title' => 'Importe Total', 'data' => 'im_tot')
                );

                foreach ($result['dat'] as $row) {
                    $array['data'][] = array(
                        'fe_emi' => $row['fe_emi'],
                        'tp_doc' => $row['tp_doc'],
                        'ra_soc' => $row['ra_soc'],
                        'id_com' => $row['id_com'],
                        'nu_fac' => $row['nu_fac'],
                        'fe_aut' => $row['fe_aut'],
                        'no_es_aut' => $row['no_es_aut'],
                        'nu_aut' => $row['nu_aut'],
                        'cl_acc' => $row['cl_acc'],
                        'sub_12' => $row['sub_12'],
                        'sub_0' => $row['sub_0'],
                        'iva_12' => $row['iva_12'],
                        'im_tot' => $row['im_tot']
                    );
                }
                $pieDoc = $pieAut = null;
                if (!empty($result['doc'])) {
                    foreach ($result['doc'] as $doc) {
                        $pieDoc[] = array(
                            'label' => $doc['co_doc'] . '*|*' . $doc['no_doc'] . ' <b>' . number_format($doc['ca_doc'], 0) . '</b>',
                            'data' => (int) $doc['ca_doc']
                                //'color' => $this->estados[trim($row['co_est'])]['color'],
                        );
                    }
                }
                $result['doc'] = $pieDoc;

                if (!empty($result['aut'])) {
                    foreach ($result['aut'] as $aut) {
                        $pieAut[] = array(
                            'label' => $aut['co_aut'] . '*|*' . $aut['no_aut'] . ' <b>' . number_format($aut['ca_aut'], 0) . '</b>',
                            'data' => (int) $aut['ca_aut']
                                //'color' => $this->estados[trim($row['co_est'])]['color'],
                        );
                    }
                }
                $result['aut'] = $pieAut;

                $tot .= '<table class="table table-bordered TFtable" style="margin-bottom:0;"><tbody>';
                $tot .= '<tr>';
                $tot .= '<td colspan=2><b>TOTALES</b></td>';
                $tot .= '</tr>';
                $tot .= '<tr>';
                $tot .= '<td>Subtotal 12</td>';
                $tot .= '<td class="text-right"><b>' . number_format($result['tot']['sub_12'], 2, '.', ',') . '</b></td>';
                $tot .= '</tr>';
                $tot .= '<tr>';
                $tot .= '<td>Subtotal 0</td>';
                $tot .= '<td class="text-right"><b>' . number_format($result['tot']['sub_0'], 2, '.', ',') . '</b></td>';
                $tot .= '</tr>';
                $tot .= '<tr>';
                $tot .= '<td>Iva</td>';
                $tot .= '<td class="text-right"><b>' . number_format($result['tot']['iva_12'], 2, '.', ',') . '</b></td>';
                $tot .= '</tr>';
                $tot .= '<tr>';
                $tot .= '<td>Importe Total</td>';
                $tot .= '<td class="text-right"><b>' . number_format($result['tot']['im_tot'], 2, '.', ',') . '</b></td>';
                $tot .= '</tr>';
                $tot .= '</tbody></table>';
            }

            $result['dat'] = $array;
            $result['tot'] = $tot;

            echo json_encode($result);
        }
    }

    private function _consultaDocumentosRecibidos() {
        $tot = '';
        $co_anio = $this->input->post('c_an');
        $co_mes = $this->input->post('c_me');
        $co_dia = $this->input->post('c_di');
        $co_autorizacion = $this->input->post('c_au');
        $co_documento = $this->input->post('c_do');
        $co_identificacion = $this->input->post('c_id');
        $co_establecimiento = $this->input->post('c_es');
        $co_puntoemision = $this->input->post('c_pe');
        $co_secuencial = $this->input->post('c_se');
        $contabilizado = $this->input->post('c_co');
        $result = $this->m_comprobantes->retDocumentosRecibidos($co_anio, $co_mes, $co_dia, $co_documento, $co_identificacion, $co_establecimiento, $co_puntoemision, $co_secuencial, $contabilizado);
        $array = null;
        if (empty($result['err'])) {
            if (!empty($result['dat'])) {
                $array['columns'] = array(
                    array('title' => 'Emisi&oacute;n', 'data' => 'fe_emi'),
                    array('title' => 'Docum.', 'data' => 'tp_doc'),
                    array('title' => 'Cliente/Prov./Receptor', 'data' => 'ra_soc'),
                    array('title' => 'Identificaci&oacute;n', 'data' => 'id_com'),
                    array('title' => 'Nro. Documento', 'data' => 'nu_fac'),
                    array('title' => 'Fecha Autorizaci&oacute;n', 'data' => 'fe_aut'),
                    array('title' => 'Tipo Autorizaci&oacute;n', 'data' => 'no_es_aut'),
                    array('title' => 'Nro. Autorizaci&oacute;n', 'data' => 'nu_aut'),
                    array('title' => 'Clave Acceso', 'data' => 'cl_acc'),
                    array('title' => 'Base 12', 'data' => 'sub_12', 'className' => "text-right"),
                    array('title' => 'Base 0', 'data' => 'sub_0', 'className' => "text-right"),
                    array('title' => 'Iva', 'data' => 'iva_12', 'className' => "text-right"),
                    array('title' => 'Importe Total', 'data' => 'im_tot', 'className' => "text-right"),
                    array('title' => 'Contabilizado', 'data' => 'contab', 'className' => "text-center")
                );

                foreach ($result['dat'] as $row) {
                    $array['data'][] = array(
                        'fe_emi' => $row['fe_emi'],
                        'tp_doc' => $row['tp_doc'],
                        'ra_soc' => $row['ra_soc'],
                        'id_com' => $row['id_com'],
                        'nu_fac' => $row['nu_fac'],
                        'fe_aut' => $row['fe_aut'],
                        'no_es_aut' => $row['no_es_aut'],
                        'nu_aut' => $row['nu_aut'],
                        'cl_acc' => $row['cl_acc'],
                        'sub_12' => $row['sub_12'],
                        'sub_0' => $row['sub_0'],
                        'iva_12' => $row['iva_12'],
                        'im_tot' => $row['im_tot'],
                        'contab' => $row['contab'] == 1 ? 'SI' : 'NO'
                    );
                }
                $pieDoc = $pieAut = null;
                if (!empty($result['doc'])) {
                    foreach ($result['doc'] as $doc) {
                        $pieDoc[] = array(
                            'label' => $doc['co_doc'] . '*|*' . $doc['no_doc'] . ' <b>' . number_format($doc['ca_doc'], 0) . '</b>',
                            'data' => (int) $doc['ca_doc']
                                //'color' => $this->estados[trim($row['co_est'])]['color'],
                        );
                    }
                }
                $result['doc'] = $pieDoc;

                if (!empty($result['con'])) {
                    foreach ($result['con'] as $aut) {
                        $pieAut[] = array(
                            'label' => $aut['co_con'] . '*|*' . $aut['no_con'] . ' <b>' . number_format($aut['ca_con'], 0) . '</b>',
                            'data' => (int) $aut['ca_con']
                                //'color' => $this->estados[trim($row['co_est'])]['color'],
                        );
                    }
                }
                $result['con'] = $pieAut;

                $tot .= '<table class="table table-bordered TFtable" style="margin-bottom:0;"><tbody>';
                $tot .= '<tr>';
                $tot .= '<td colspan=2><b>TOTALES</b></td>';
                $tot .= '</tr>';
                $tot .= '<tr>';
                $tot .= '<td>Subtotal 12</td>';
                $tot .= '<td class="text-right"><b>' . number_format($result['tot']['sub_12'], 2, '.', ',') . '</b></td>';
                $tot .= '</tr>';
                $tot .= '<tr>';
                $tot .= '<td>Subtotal 0</td>';
                $tot .= '<td class="text-right"><b>' . number_format($result['tot']['sub_0'], 2, '.', ',') . '</b></td>';
                $tot .= '</tr>';
                $tot .= '<tr>';
                $tot .= '<td>Iva</td>';
                $tot .= '<td class="text-right"><b>' . number_format($result['tot']['iva_12'], 2, '.', ',') . '</b></td>';
                $tot .= '</tr>';
                $tot .= '<tr>';
                $tot .= '<td>Importe Total</td>';
                $tot .= '<td class="text-right"><b>' . number_format($result['tot']['im_tot'], 2, '.', ',') . '</b></td>';
                $tot .= '</tr>';
                $tot .= '</tbody></table>';
            }
            $result['dat'] = $array;
            $result['tot'] = $tot;
        }

        echo json_encode($result);
    }

}
