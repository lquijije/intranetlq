<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_132 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->model(array('proceso/ventas/m_132'));
        $this->load->library(array('mailer'));
        session_start();
    }	

    public function _remap()  {    
        
        validaSession('procesos/ventas/co_132');  
        
        $tipo=$this->uri->segment(4);
                
        if ($tipo == 'reSol'){
            $this->_retSolicitudes();
        } else if ($tipo == 'aprSol'){
            $this->_aproSolicit();
        } else if ($tipo == 'viFac'){
            $this->_viewFactura();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud(){
        $data = null; 
        $urlControlador = 'procesos/ventas/co_132';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css');
            
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/config.js',
                            'js/bootstrap.min.js',
                            'js/procesos/ventas/j_132.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/general.js');
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/ventas/v_132');
        $this->load->view('barras/footer');
    }
    
    private function _viewFactura(){
        $co_bodega = $this->input->post('c_b');
        $co_caja = $this->input->post('c_c');
        $nu_documento = $this->input->post('n_d');
        $factura = $this->m_132->retDetalleFactura($co_bodega, $co_caja, $nu_documento);
        echo json_encode($factura);
    }
    
    private function _retSolicitudes(){
        $solicitudes = $this->m_132->retSolicitudes($_SESSION['c_e']); 
        if (!empty($solicitudes)){
            echo json_encode($solicitudes);
        } else {
            echo json_encode(array(
                "sEcho" => 1,
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            ));
        }
    }
    
    private function _aproSolicit(){
        $data = $this->input->post();
        $result = $this->m_132->aprobarSolicit($data);
       
        $bandera = 0;
        $menOK = $menER = $valida = '';
        
        if(is_array($result)){
            if (!empty($result)){
                for($i=0;$i<count($result);$i++){
                    if((int)$result[$i]['d_e']['co_err'] == 0){
                        $this->_MailSolicitud($result[$i]['c_s']);
                        $menOK.= $result[$i]['c_s']."<br>";
                    } else {
                        $menER.= $result[$i]['c_s']." - (".trim($result[$i]['d_e']['tx_err']).").<br>";
                    }
                }
                if(!empty($menOK)){
                    $valida.= 'Las siguientes solicitudes han sido actualizadas con exito.<br>'.$menOK;
                }
                if(!empty($menER)){
                    $bandera = 1;
                    $valida.= 'Las siguientes solicitudes han presentado ERROR.<br>'.$menER;
                } 
            }
        } else {
            $bandera = 1;
            $valida = $result;
        }
        
        echo json_encode(array(
            'co_err'=>$bandera,
            'tx_err'=>$valida
        ));
    }
    
    private function _MailSolicitud($cod_sol){
        
//        error_reporting(E_ALL);
//        ini_set('display_errors', 1);

        $arrSolic = $this->m_132->retSolicitud($cod_sol);
        
        $host = $this->config->item('host_mail');
        $puerto = $this->config->item('port_mail');	
        $usuario = $this->config->item('user_mail');		
        $clave = $this->config->item('pass_mail');	
        
	$responder_a = $usuario;
        
        if(!empty($arrSolic)){
            $asunto = "Autorización POS".$arrSolic['no_es_sol'];
            $cuerpo = '<table cellpadding="0" cellspacing="0" style="width:100%;border-top:5px solid #F0514A;padding:30px 0;">
                <tbody>
                    <tr>
                        <td>
                            <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="ecxlayout">
                                <tbody>
                                    <tr>
                                        <td height="10">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="middle">
                                            <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table border="0" width="121" align="left" cellpadding="0" cellspacing="0" class="ecxlogo">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="ecxlogo_place"><img src="http://www.pycca.com/images/logos/1/logoPycca.png" width="180" height="40"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="middle">
                                            <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                                <tbody>
                                                    <tr>
                                                        <td class="ecxaction">
                                                            <table width="600" border="0" cellspacing="0" cellpadding="0" class="ecxaction_text" align="left">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="font-size:18px;color:#444 !important;line-height:25px;font-weight:lighter;">
                                                                        <p>Estimado(a) la solicitud de autorización #'.$cod_sol.' con documento #'.$arrSolic['nu_fac'].' ha sido '.$arrSolic['no_es_sol'].' por '.$_SESSION['nombre'].'.</p>
                                                                        <p># De Autorización: '.$arrSolic['co_aut'].'</p>
                                                                        <p>'.$arrSolic['de_obs'].'</p>
                                                                        <p>Saludos cordiales.</p> 
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="50" class="ecxspace">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#323f4e">
                                <tbody>
                                    <tr>
                                        <td valign="middle" style="border-top:5px solid #F0514A;background-color:#00539F;">
                                            <table border="0" cellspacing="0" cellpadding="0" align="center" class="footer">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="250" border="0" cellspacing="0" cellpadding="0" align="left" class="ecxcopyright">
                                                                <tbody>
                                                                    <tr>
                                                                        <td height="40" valign="middle" style="font-size:14px;color:#FFF;">&copy; 2015 PYCCA S.A.</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>';

            $cabecera= "MIME-Version: 1.0\r\n";
            $cabecera .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
            $cabecera .= "From: PYCCA S.A.\r\n";

            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->IsSMTP(); 
            $mail->PluginDir = "";
            $mail->Host = $host;
            $mail->Port = $puerto;
            $mail->SMTPAuth = true;
            $mail->Username = $usuario;
            $mail->Password = $clave;
            $mail->From = $responder_a;
            $mail->FromName = "PYCCA S.A.";
            $mail->WordWrap = 50;
            $mail->Subject = $asunto;

            $mail->AddAddress($_SESSION['e_mail']);
            $mail->AddAddress($arrSolic['em_gea']);
            $mail->AddBCC('fvera@pycca.com');
            $mail->Body = $cuerpo;
            $mail->IsHTML(true);

            if(!$mail->Send()){
                return "ERROR: " . $mail->ErrorInfo; 	
            } else {
                return "OK";	
            }	
        }
    }
}