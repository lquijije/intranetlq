<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_129 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('proceso/ventas/m_129', 'jobs'));
        session_start();
    }

    public function _remap() {
        validaSession('procesos/ventas/co_129');
        $tipo = $this->uri->segment(4);
        switch ($tipo) {
            case 'retReg':
                $this->_retRegla();
                break;
            case 'retCanales':
                $this->_retCanales();
                break;
            case 'retIvrTot':
                $this->_retIvrTot();
                break;
            case 'up_prioridad':
                $this->_up_prioridad();
                break;
            case 'sv_reg':
                $this->_save_regla();
                break;
            case 'retCalendar':
                $this->_retCalendario();
                break;
            case 'up_job':
                $this->_up_estRobot();
                break;
            case 'in_job':
                $this->_in_job();
                break;
            case 'st_job':
                $this->_st_job();
                break;
            case 'snExcel':
                $this->_grabarExcel();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {
        $data = null;
        $urlControlador = 'procesos/ventas/co_129';
        $data['permisos'] = '';
        $data['urlControlador'] = $urlControlador;
        $data['idFormulario'] = 'co_129';
        $data['r1_DonaData'] = null;
        $data['r1_ValGes'] = 0;
        $data['r1_CosGes'] = 0;

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'css/daterangepicker/daterangepicker-bs3.css',
            'js/plugins/select2/dist/css/select2.css',
            'js/plugins/switchery/dist/switchery.css',
            'css/datetimepicker/jquery.datetimepicker.css'
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.js',
            'js/general.js',
            'js/plugins/switchery/dist/switchery.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js',
            'js/plugins/daterangepicker/daterangepicker.js',
            'js/procesos/ventas/j_129.js',
            'js/plugins/select2/dist/js/select2.js',
            'js/plugins/datetimepicker/jquery.datetimepicker.js'
        );

        $Reglas = $this->m_129->retReglas();
        $Canales = $this->m_129->retCanales();
        $Parametros = $this->m_129->retParametros();
        $Prioridades = $this->m_129->retPrioridades();
        $data['estadoRobot'] = $this->m_129->retEstadoRobot();
        $actividad = $this->jobs->Actividad('ROBOT_GENERACION');
        $data['activi_job'] = $actividad;
        $data['Reglas'] = $Reglas;

        $datosParametro = null;

        if (!empty($Prioridades)) {
            foreach ($Prioridades as $row) {
                $datosParametro[$row['co_pri']] = $row['co_pri'];
            }
        } else {
            $datosParametro['NNN'] = 'Sin datos';
        }

        $data['cmb_pri_pla'] = form_dropdown('cmb_pri_pla', $datosParametro, 'NNN', "id='cmb_pri_pla' class='form-control' multiple=''");

        /*         * ******************************************************************* */

        $datosParametro = null;

        if (!empty($Parametros)) {
            $datosParametro['NNN'] = "Seleccionar";
            foreach ($Parametros as $row) {
                $datosParametro[$row['co_par']] = $row['ds_par'];
            }
        } else {
            $datosParametro['NNN'] = 'Sin datos';
        }

        $data['cmb_param'] = form_dropdown('cmb_param', $datosParametro, 'NNN', "id='cmb_param' class='form-control'");

        /*         * ******************************************************************* */

        $datosParametro = null;

        if (!empty($Canales)) {
            foreach ($Canales as $row) {
                $datosParametro[$row['co_can']] = $row['ds_can'];
            }
        } else {
            $datosParametro['NNN'] = 'Sin datos';
        }

        $this->canales = $datosParametro;

        $data['cmb_can_pla'] = form_dropdown('cmb_can_pla', $datosParametro, 'NNN', "id='cmb_can_pla' class='form-control' multiple=''");

        /*         * ******************************************************************* */

        $datosParametro = null;

        if (!empty($Canales)) {
            foreach ($Canales as $row) {
                $datosParametro[$row['co_can']] = $row['ds_can'];
            }
        } else {
            $datosParametro['NNN'] = 'Sin datos';
        }

        $data['cmb_canal'] = form_dropdown('cmb_canal', $datosParametro, 'NNN', "id='cmb_canal' class='form-control'");

        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/ventas/v_129');
        $this->load->view('barras/footer');
    }

    private function _up_job() {
        $onoff = $this->input->post('ck');
        $id_job = $this->input->post('i_j');
        $mensaje = $this->jobs->Actualizar($onoff, $id_job);
        echo json_encode($mensaje);
    }

    private function _up_estRobot() {
        $onoff = $this->input->post('ck');
        $mensaje = $this->m_129->updEstadoRobot($onoff);
        echo json_encode($mensaje);
    }

    private function _in_job() {
        $mensaje = $this->jobs->Iniciar('ROBOT_GENERACION');
        echo $mensaje;
    }

    private function _st_job() {
        $actividad = $this->jobs->Actividad('ROBOT_GENERACION');
        echo json_encode($actividad);
    }

    private function _retCalendario() {
        $co_anio = $this->input->post('c_a');
        $co_mes = $this->input->post('c_m');
        $data = $this->m_129->retCalendario($co_anio, $co_mes);
        $data = groupArray($data, 'id_fec');
        echo json_encode($data);
    }

    private function _retRegla() {
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        $cp_reg = $this->input->post('c_r');
        $mensaje = $this->m_129->retReglaRobot($cp_reg);
        if (empty($mensaje['err'])) {

            $res_2 = $res_3 = $res_4 = $res_5 = '';
            if (!empty($mensaje['res_2'])) {
                foreach ($mensaje['res_2'] as $row) {
                    $res_2.= '<tr id="opt_para_' . $row['co_par'] . '">';
                    $res_2.= '<td><p class="tab_co_para">' . $row['co_par'] . '</p></td>';
                    $res_2.= '<td>' . $row['ds_par'] . '</td>';
                    $res_2.= '<td class="derecha"><p class="tab_va_para">' . $row['va_par'] . '</p></td>';
                    $res_2.= '<td><center><button type="button" onclick="removeOptionParametros(' . $row['co_par'] . ')" class="btn btn-circle btn-danger add_option"><i class="text-blanco fa fa-minus"></i></button></center></td>';
                    $res_2.= '</tr>';
                }
            }

            if (!empty($mensaje['res_3'])) {
                foreach ($mensaje['res_3'] as $row) {
                    $res_3.= '<li class="ui-state-default" id="opt_cana_' . $row['co_can'] . '">';
                    $res_3.= '<div class="external-event">';
                    $res_3.= '<div style="position: relative;">';
                    $res_3.= '<button title="Eliminar Canal" type="button" class="btn btn-circle btn-danger" onclick="removeOptionCanales(' . $row['co_can'] . ')" style="position: absolute; right: 0;"><i class="text-blanco text-size-1 fa fa-close"></i></button>';
                    $res_3.= '<b>' . $row['ds_can'] . '</b>';
                    $res_3.= '<textarea name="txt_men_can_' . $row['co_can'] . '" value="" id="txt_men_can_' . $row['co_can'] . '" class="form-control" style="height:50px;">' . $row['me_can'] . '</textarea>';
                    $res_3.= '</div>';
                    $res_3.= '<div class="clearfix"></div>';
                    $res_3.= '</div>';
                    $res_3.= '</li>';
                }
            }
            if (!empty($mensaje['res_4'])) {
                $c = $this->m_129->retCanales();
                foreach ($mensaje['res_4'] as $row) {
                    $canal = explode('|', $row['ca_det']);
                    $txCanal = '';
                    if (!empty($canal)) {
                        foreach ($canal as $ca) {
                            $k = array_search($ca, array_map(function($e) {
                                        return $e['co_can'];
                                    }, $c));
                            $txCanal .= $c[$k]['ds_can'] . ",";
                        }
                    }
                    $res_4.= '<tr id="opt_plan_' . $row['di_det'] . '">';
                    $res_4.= '<td><p class="tab_tx_pla">' . $row['di_det'] . '</p></td>';
                    $res_4.= '<td>' . trim($txCanal, ',') . '<input type="hidden" class="tab_cm_can" value="' . $row['ca_det'] . '"></td>';
                    $res_4.= '<td><p class="tab_cm_pri">' . $row['pr_det'] . '</p></td>';
                    $res_4.= '<td><p class="tab_es_pla">' . $row['es_det'] . '</p></td>';
                    $res_4.= '<td><center><button type="button" onclick="removeOptionPlanificacion(' . $row['di_det'] . ')" class="btn btn-circle btn-danger add_option"><i class="text-blanco fa fa-minus"></i></button></center></td>';
                    $res_4.= '</tr>';
                }
            }

            if (!empty($mensaje['res_5'])) {

                foreach ($mensaje['res_5'] as $row) {
                    $res_5.= '<tr id="opt_fech_' . $row['fe_man'] . '">';
                    $res_5.= '<td>' . $row['fe_man'] . '</td>';
                    $res_5.= '<td>' . $row['to_man'] . '</td>';
                    $res_5.= '<td><center><button type="button" onclick="viewListFech(' . $row['fe_man'] . ')" class="btn btn-circle btn-success"><i class="text-blanco fa fa-plus"></i></button></center></td>';
                    $res_5.= '</tr>';
                }
            }

            $mensaje['res_2'] = $res_2;
            $mensaje['res_3'] = $res_3;
            $mensaje['res_4'] = $res_4;
            $mensaje['res_5'] = $res_5;
        }
        echo json_encode($mensaje);
    }

    private function _up_prioridad() {
        $datos = $this->input->post();
        $mensaje = $this->m_129->updatePrioridadCanalesRegla($datos);
        echo json_encode($mensaje);
    }

    private function _save_regla() {
        $datos = $this->input->post();
        $mensaje = $this->m_129->saveReglas($datos);
        echo json_encode($mensaje);
    }

    private function _grabarExcel() {
        $d_c = $this->input->post('d_c');
        $d_f = $this->input->post('d_f');
        $c_c = $this->input->post('c_c');
        $datos = json_decode($d_c);
        $mensaje = $this->m_129->grabarExcel($datos, $d_f, $c_c);
        echo json_encode($mensaje);
    }

}
