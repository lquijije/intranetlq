<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_131 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('proceso/ventas/m_131', 'proceso/reclamos/m_111'));
        $this->load->library(array('mailer', 'Encrypt'));
        session_start();
    }

    public function _remap() {

        validaSession('procesos/ventas/co_131');

        $tipo = $this->uri->segment(4);

        if ($tipo == 'retSol') {
            $this->_retSolicitudes();
        } else if ($tipo == 'retDetFact') {
            $this->_retDetFact();
        } else if ($tipo == 'senSo') {
            $this->_enviarSoli();
        } else {
            $this->_crud();
        }
    }

    private function _crud() {

        $_SESSION['cg_r'] = 0;
        $_SESSION['mg_r'] = '';
        $data = null;
        $data['permisos'] = '';
        $urlControlador = 'procesos/ventas/co_131';
        $data['urlControlador'] = $urlControlador;
        $data['css'] = array('css/datatables/dataTables.bootstrap.css', 'css/datetimepicker/jquery.datetimepicker.css');
        $data['js'] = array('js/jquery-1.10.2.min.js',
            'js/bootstrap.js',
            'js/procesos/ventas/j_131C.js',
            'js/general.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js',
            'js/plugins/datetimepicker/jquery.datetimepicker.js');

        $data['idFormulario'] = 'co_131';
        $empresas = $this->m_111->retEmpresaAsignadas();

        $data['empresas'] = $empresas;

        if (isset($empresas)) {
            $p = $empresas[0]['cod_bdg'];
            $q = $empresas[0]['de_sri'];
        } else {
            $p = $q = '';
        }

        $getCo_alm = $this->input->post('co_alm');
        $getCo_sr = $this->input->post('co_sr');

        $cod_alm = !empty($getCo_alm) ? $getCo_alm : $p;
        $co_sr = !empty($getCo_sr) ? $getCo_sr : $q;

        $permisos = $this->m_131->retPermSolic($_SESSION['c_e']);
        //$permisos_solicitud = $this->m_131->retPermisosSolicitud($_SESSION['c_e']);

        $datosParametro = null;
        if (!empty($permisos)) {
            foreach ($permisos as $row) {
                $datosParametro[$row['ti_aut']] = $row['no_ti_aut'];
            }
            $_SESSION['cg_r'] = $permisos[0]['co_ger'];
            $_SESSION['mg_r'] = $permisos[0]['em_ger'];
        } else {
            $datosParametro['NNN'] = 'No tiene asignado tipos';
        }
        $atributosEspecial = "id='cmb_tip' class='form-control'";
        $data['cmb_tip'] = form_dropdown('cmb_tip', $datosParametro, 'NNN', $atributosEspecial);

        $_SESSION['cod_almacen'] = $cod_alm;
        $_SESSION['cod_sri'] = $co_sr;

        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/ventas/v_131');
//        if ($_SERVER['REMOTE_ADDR'] == '130.1.39.49') {
//        } else {
//            $this->load->view('error/app_update');
//        }
        $this->load->view('barras/footer');
    }

    private function _retSolicitudes() {
        $co_alm = (int) $this->input->post('c_a');
        $co_emp = (int) $_SESSION['c_e'];
        $contratos = $this->m_131->retSolicitudes($co_alm, $co_emp);
        if (!empty($contratos)) {
            echo json_encode($contratos);
        } else {
            echo json_encode(array(
                "sEcho" => 1,
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            ));
        }
    }

    private function _retDetFact() {
        $datos = $this->input->post();
        $data = $this->m_131->retFactNTPYCCA($datos);
        $array = array('err' => '', 'dat' => '');
        $cantidad = 0;
        $devuelto = 0;
        $total = 0;

        if (empty($data['err'])) {
            $html = '<div class="separador">';
            $html.= '<table class="table table-bordered TFtable">';
            $html.= '<thead>';
            $html.= '<th>C&oacute;digo</th>';
            $html.= '<th>Descripci&oacute;n</th>';
            $html.= '<th>Cantidad</th>';
            $html.= '<th>Valor</th>';
            $html.= '<th>Cant. Devuelta/Cambiada</th>';
            $html.= '<th></th>';
            $html.= '</thead>';
            $html.= '<tbody id="det_articulos">';

            if (!empty($data['dt_2'])) {
                foreach ($data['dt_2'] as $key => $row) {
                    $cantidad+=(int) $row['can_art'];
                    $devuelto+=(int) $row['can_dyc'];
                    $c_dyc = ((int) $row['can_art'] - (int) $row['can_dyc']) == 0 ? 'danger' : '';
                    $d_dyc = ((int) $row['can_art'] - (int) $row['can_dyc']) == 0 ? 'fa-close' : 'fa-check';
                    $html.= '<tr id="option_' . $key . '" class="' . $c_dyc . '">';
                    $html.= '<td>' . $row['cod_art'] . '</td>';
                    $html.= '<td>' . $row['des_art'] . '</td>';
                    $html.= '<td class="derecha">' . $row['can_art'] . '</td>';
                    $html.= '<td class="derecha">' . $row['pvp_art'] . '</td>';
                    $html.= '<td class="derecha">' . $row['can_dyc'] . '</td>';
                    $html.= '<td class="centro"><i class="fa ' . $d_dyc . '"></i></td>';
                    $html.= '</tr>';
                }
                $total = $cantidad - $devuelto;
            }

            $html.= '</tbody>';
            $html.= '</table>';
            $html.= '</div>';
            $html.= '<div class="separador">';
            $html.= '<label>Observaci&oacute;n</label>';
            $html.= '<textarea id="tx_obs" name="tx_obs" class="form-control"></textarea>';
            $html.= '</div>';
            if ($total > 0) {
                $html.= '<div class="separador">';
                $html.= '<button type="button" class="btn btn-success pull-right" onclick="sendSolis();" style="margin-top: 10px;"><i class="fa fa-check"></i> Enviar Solicitud</button>';
                $html.= '</div>';
            }
            $array['dat'] = !empty($data['dt_2']) ? $html : '';
        } else {
            $array['err'] = $data['err'];
        }
        echo json_encode($array);
    }

    private function _enviarSoli() {

        $datos = $this->input->post();
        $array = $this->m_131->saveSoliciAut($datos);
        if ((int) $array['co_err'] == 0) {
            $this->_MailSolicitud($datos, $_SESSION['mg_r'], $_SESSION['e_mail']);
        }
        echo json_encode(array('data' => $array));
    }

    private function _MailSolicitud($data, $e_mail_ge, $e_mail_us) {

        $host = $this->config->item('host_mail');
        $puerto = $this->config->item('port_mail');
        $usuario = $this->config->item('user_mail');
        $clave = $this->config->item('pass_mail');

        $responder_a = $usuario;
        if (!empty($e_mail_ge) && !empty($e_mail_us)) {
            $asunto = "Solicitud de Autorización POS";
            $cuerpo = '<table cellpadding="0" cellspacing="0" style="width:100%;border-top:5px solid #F0514A;padding:30px 0;">
                <tbody>
                    <tr>
                        <td>
                            <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="ecxlayout">
                                <tbody>
                                    <tr>
                                        <td height="10">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="middle">
                                            <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table border="0" width="121" align="left" cellpadding="0" cellspacing="0" class="ecxlogo">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="ecxlogo_place"><img src="http://www.pycca.com/images/logos/1/logoPycca.png" width="180" height="40"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="middle">
                                            <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                                <tbody>
                                                    <tr>
                                                        <td class="ecxaction">
                                                            <table width="600" border="0" cellspacing="0" cellpadding="0" class="ecxaction_text" align="left">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="font-size:18px;color:#444 !important;line-height:25px;font-weight:lighter;">
                                                                        <p>Estimado(a) se solicita su autorización para el documento # ' . $data['c_s'] . '-' . $data['c_c'] . '-' . $data['c_d'] . '.</p>
                                                                        <p>' . $data['t_o'] . '</p>
                                                                        <p>Saludos cordiales.</p> 
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="50" class="ecxspace">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#323f4e">
                                <tbody>
                                    <tr>
                                        <td valign="middle" style="border-top:5px solid #F0514A;background-color:#00539F;">
                                            <table border="0" cellspacing="0" cellpadding="0" align="center" class="footer">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="250" border="0" cellspacing="0" cellpadding="0" align="left" class="ecxcopyright">
                                                                <tbody>
                                                                    <tr>
                                                                        <td height="40" valign="middle" style="font-size:14px;color:#FFF;">&copy; 2015 PYCCA S.A.</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>';

            $cabecera = "MIME-Version: 1.0\r\n";
            $cabecera .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $cabecera .= "From: PYCCA S.A.\r\n";

            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->IsSMTP();
            $mail->PluginDir = "";
            $mail->Host = $host;
            $mail->Port = $puerto;
            $mail->SMTPAuth = true;
            $mail->Username = $usuario;
            $mail->Password = $clave;
            $mail->From = $responder_a;
            $mail->FromName = "PYCCA S.A.";
            $mail->WordWrap = 50;
            $mail->Subject = $asunto;

            $mail->AddAddress($e_mail_ge);
            $mail->AddAddress($e_mail_us);
            $mail->AddBCC('fvera@pycca.com');
            $mail->Body = $cuerpo;
            $mail->IsHTML(true);

            if (!$mail->Send()) {
                echo "ERROR: " . $mail->ErrorInfo;
            } else {
                return "OK";
            }
        }
    }

}
