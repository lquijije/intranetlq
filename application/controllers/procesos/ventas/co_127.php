<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_127 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/ventas/m_127'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/ventas/co_127');  

        $tipo = $this->uri->segment(4);

        if($tipo == 've_fac'){
            $this->_verificaFactura();
        } else if($tipo == 'va_cup'){
            $this->_ingresaCupon();
        } else {
            $this->_crud(); 
        }
        
    }        

    private function _crud(){
       
        $data = null; 
        $data['permisos']='';   
        $data['cod_ci']='';  
        $data['message'] = '';
        $data['dataFec'] = null;
        
        $urlControlador = 'procesos/ventas/co_127';
        $data['urlControlador'] = $urlControlador;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/general.js',
                            'js/procesos/ventas/j_127.js');

        $data['idFormulario'] = 'co_127';
        
        //$data['dataFec'] = $this->m_127->retHistPass($post['c_u']);
             
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/ventas/v_127');
        $this->load->view('barras/footer');            
    }
    
    public function _verificaFactura(){
        $data = $this->input->post();
        $mensaje = $this->m_127->verFactura($data);
        echo json_encode($mensaje);
    }
    
    public function _ingresaCupon(){
        $data = $this->input->post();
        $mensaje = $this->m_127->ingCupon($data);
        echo json_encode($mensaje);
    }
    
}