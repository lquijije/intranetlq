<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_108 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation'));
        $this->load->model(array('proceso/ventas/m_108'));
        session_start();
    }	

    public function _remap()  {    
        
        validaSession('procesos/ventas/co_108');  
        
        $tipo=$this->uri->segment(4);

        if($tipo=='reDatVentas'){
            $this->_reDatVentas();
        } else {
            $this->_crud();
        }
    }
        
    private function _crud(){
        
        $data = null; 
        $urlControlador = 'procesos/facturacion/co_108';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] = array('css/datetimepicker/jquery.datetimepicker.css');
                
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/ventas/j_108.js',
                            'js/general.js',
                            'js/fechas.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js');
        
        $fecha = $this->input->get('f', TRUE);
        $hora = $this->input->get('h', TRUE);
        $data['fecha'] = $fecha;
        $data['hora'] = $hora;
                                    
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/ventas/v_108'); 
        $this->load->view('barras/footer');
    }
    
    private function _reDatVentas(){
        $fec_ini = $this->input->post('f_i');
        $abc = explode('|',$fec_ini);
        $result = $this->m_108->reDatVentas($abc[0],$abc[1]);
        echo $result;
        unset($result);
    }
    
}