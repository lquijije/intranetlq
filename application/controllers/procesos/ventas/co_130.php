<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_128 extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('proceso/ventas/m_128','jobs'));
        session_start();
    }

    public function _remap() {

        validaSession('procesos/ventas/co_128');

        $tipo=$this->uri->segment(4);
        
        if ($tipo=='retCanales'){
            $this->_retCanales();
        } else if ($tipo=='up_job'){
            $this->_up_job();
        } else if ($tipo=='in_job'){
            $this->_in_job();
        } else if ($tipo=='st_job'){
            $this->_st_job();
        } else if ($tipo=='exportExcel'){
            $this->_exportXls();
        } else if ($tipo=='up_prov'){
            $this->_up_prov();
        } else if ($tipo=='up_camp'){
            $this->_up_camp();
        } else if ($tipo=='retReg'){
            $this->_retRegla();
        } else if ($tipo=='seaNum'){
            $this->_seaNumCed();
        } else if ($tipo=='retCalls'){
            $this->_retCalls();
        } else {
            $this->_crud();
        }
        
    }        
        
    private function _crud(){
        
        $data                   =  null; 
        $urlControlador         =  'procesos/ventas/co_128';
        $data['permisos']       =  '';   
        $data['urlControlador'] =  $urlControlador;
        $data['idFormulario']   =  'co_128';
        $data['r1_DonaData']    =  null;
        $data['r1_ValGes']      =  0;
        $data['r1_CosGes']      =  0;
        
        $opcion = BuscaIdMenu($urlControlador,$_SESSION['menu']);
        $update = false;
        
        if(!empty($opcion)){
            
            if(isset($_SESSION['menu'][$opcion]['opciones']) && !empty($_SESSION['menu'][$opcion]['opciones'])){
               
                $arrayBusq = $_SESSION['menu'][$opcion]['opciones'];
                foreach ($arrayBusq AS $row){

                    if (isset($row['UPADTE']) && !empty($row['UPADTE'])){
                        $row['UPADTE'][0] == 1 ?$update = true:$update = false;
                    }

                }
                
            }
            
        }
        
        $data['update'] = $update;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                            'js/plugins/switchery/dist/switchery.css',
                            'css/datetimepicker/jquery.datetimepicker.css');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/switchery/dist/switchery.js',
                            'js/plugins/flot/jquery.flot.min.js',
                            'js/plugins/flot/jquery.flot.resize.min.js',
                            'js/plugins/flot/jquery.flot.pie.min.js',
                            'js/plugins/flot/Smooth-0.1.7.js',
                            'js/plugins/flot/jquery.flot.time.js',
                            'js/plugins/flot/jquery.flot.curvedlines-0.2.3.min.js',
                            'js/procesos/ventas/j_128.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js'
            
        );
        
        $fec_ini = date('Y-m-d',strtotime('-1 day'));
        $fec_fin = date('Y-m-d');
        
        $f_i = $this->input->post('f_i');
        $f_f = $this->input->post('f_f');
        
        $getFec_ini = !empty($f_i)?$f_i:$fec_fin;
        $getFec_fin = !empty($f_f)?$f_f:$fec_fin;
        
        $data['fec_ini'] = $getFec_ini;
        $data['fec_fin'] = $getFec_fin;
        
        $monitor      = $this->m_128->retMonitor($getFec_ini,$getFec_fin);
        $chartLineCmb = $this->m_128->retCanalesReporte($getFec_fin,'');
        $chartLineCmb = $chartLineCmb['res_1'];
        
        /***********************************************************************/
        $datosParametro = null;
        
        $nameChart = '';
        
        if (!empty($chartLineCmb)){    
            foreach ($chartLineCmb as $row){ 
                $datosParametro[$row['grupo2']] = $row['grupo2'];                                     
            }
        } else {
            $datosParametro['NNN'] = 'Sin datos';    
        }
        
        $atributosEspecial = "id='cmb_chart_lines' class='form-control'";
        
        $data['cmb_chart_lines'] = form_dropdown('cmb_chart_lines',$datosParametro,'NNN',$atributosEspecial);
        
        /**********************************************************************/
        
        if(!empty($monitor)){
            
            if(!empty($monitor['res_2'])){
                
                $data['r2_Data'] = $monitor['res_2'];

            }
            
            if(!empty($monitor['res_3'])){
                
                $data['r3_Data'] = $monitor['res_3'];

            }
            
        }
        
        $data['reglas'] = $monitor;

        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/ventas/v_128');
        $this->load->view('barras/footer');   
        
    }

    private function _retRegla(){
        $cp_reg = $this->input->post('c_r');
        $mensaje = $this->m_128->retReglaRobot($cp_reg);
        echo json_encode($mensaje);
    }
    
    private function _retCalls(){
        
        $detalle = null;
        $fecha = $this->input->post('f_f');
        $arrayRes = $this->m_128->retDetLlamada($fecha);
       
        if(empty($arrayRes['err'])){
            
            $detalle = $this->retChartCalls($arrayRes['res_1']);
        }
        
        echo json_encode(array('ch_1'=>$detalle,'re_2'=>$arrayRes['res_2']));
        
    }
    
    function retChartCalls($arrayRes){
        
        $arrayHora = null;
        
        $color = array (
            'ANSWERED_LIVE' => array(
                'color'=>'#1DC096'
            ),
            'ANSWERED_MACHINE' => array(
                'color'=>'#1896A1'
            ),
            'ANSWERED_NORINGBACK' => array(
                'color'=>'#F39719'
            )
        );
        
        $data = null;
        
        if(!empty($arrayRes)){
        
            $grupoArray = groupArray($arrayRes,'ou_dial');
            
            foreach ($grupoArray as $raw){
                
                $ar = null;
                
                foreach ($raw['groupeddata'] as $rew){
                   $arrayHora[] = $rew['ho_dial'];
                   $ar[] = array($rew['ca_dial']); 
                }
                
                $data[] = array (
                    'label' => $raw['ou_dial'],
                    'fillColor' => 'rgba(172,194,132,0.1)',
                    'strokeColor' => $color[$raw['ou_dial']]['color'],
                    'pointColor' => $color[$raw['ou_dial']]['color'],
                    'pointStrokeColor' => $color[$raw['ou_dial']]['color'],
                    'data' => $ar
                );
                
            }
            
            $arrayHora = array_unique($arrayHora);
            return array('datos'=>$data,'ticks'=>$arrayHora);
            
        }
        
    }
    
    private function _retCanales(){
        
        $cod_ca = $this->input->post('c_a');
        $fecha = $this->input->post('f_f');
        
        $arrayRes = $this->m_128->retCanalesReporte($fecha,$cod_ca);
        
        if(empty($arrayRes['err'])){
            
            $detalle = $this->retChartDetalle($arrayRes['res_1']);
            $total = $this->retChartTotal($arrayRes['res_2']);
        }
        
        echo json_encode(array('ch_1'=>$detalle,'ch_2'=>$total,'ch_3'=>$arrayRes['res_3']));
    }
    
    function retChartTotal($arrayRes){
        
        $color = array (
            'CLARO' => array(
                'color'=>'#1DC096'
            ),
            'CNT' => array(
                'color'=>'#1896A1'
            ),
            'MOVISTAR' => array(
                'color'=>'#F39719'
            ),
            'SETEL' => array(
                'color'=>'#2E8BEF'
            )
        );
        
        $data = $ar = $arrayHora = null;
        
        if(!empty($arrayRes)){
            
            foreach ($arrayRes as $raw){
                $arrayHora[] = $raw['hora_sin'];
                $ar[] = array($raw['call']); 
            }
            
            $data[] = array (
                'label' => $arrayRes[0]['grupo2'],
                'fillColor' => 'rgba(172,194,132,0.1)',
                'strokeColor' => $color[$arrayRes[0]['grupo2']]['color'],
                'pointColor' => $color[$arrayRes[0]['grupo2']]['color'],
                'pointStrokeColor' => $color[$arrayRes[0]['grupo2']]['color'],
                'data' => $ar
            );
            
            $arrayHora = array_unique($arrayHora);
            return array('datos'=>$data,'ticks'=>$arrayHora);
            
        }
        
    }
    
    function retChartDetalle($arrayRes){
        
        $arrayHora = null;
        
        $color = array (
            'Claro(A)' => array(
                'color'=>'#1DC096'
            ),
            'Claro(B)' => array(
                'color'=>'#1896A1'
            ),
            'Claro(C)' => array(
                'color'=>'#F39719'
            ),
            'Claro(D)' => array(
                'color'=>'#2E8BEF'
            ),
            'Claro(E)' => array(
                'color'=>'#633EBE'
            ),
            'CNT(3700)' => array(
                'color'=>'#F0514A'
            ),
            'Movistar(A)' => array(
                'color'=>'#F04A96'
            ),
            'Movistar(B)' => array(
                'color'=>'#1DC096'
            ),
            'SETEL(600)' => array(
                'color'=>'#1896A1'
            )
        );
        
        $data = null;
        
        if(!empty($arrayRes)){
        
            $grupoArray = groupArray($arrayRes,'name');
            
            foreach ($grupoArray as $raw){
                
                $ar = null;
                
                foreach ($raw['groupeddata'] as $rew){
                   $arrayHora[] = $rew['hora_sin'];
                   $ar[] = array($rew['call']); 
                }
                
                $data[] = array (
                    'label' => $raw['name'],
                    'fillColor' => 'rgba(172,194,132,0.1)',
                    'strokeColor' => $color[$raw['name']]['color'],
                    'pointColor' => $color[$raw['name']]['color'],
                    'pointStrokeColor' => $color[$raw['name']]['color'],
                    'data' => $ar
                );
                
            }
            
            $arrayHora = array_unique($arrayHora);
            return array('datos'=>$data,'ticks'=>$arrayHora);
            
        }
        
    }
    
    private function _up_prov(){
        $onoff = $this->input->post('ck');
        $id_pro = $this->input->post('c_p');
        $mensaje = $this->m_128->updateEstadoProveedor($onoff,$id_pro);
        echo json_encode(array('data'=>$mensaje));
    }
    
    private function _up_camp(){
        $onoff = $this->input->post('ck');
        $id_pro = $this->input->post('c_c');
        $mensaje = $this->m_128->updateEstadoCampania($onoff,$id_pro);
        echo json_encode(array('data'=>$mensaje));
    }
    
    private function _up_job(){
        $onoff = $this->input->post('ck');
        $id_job = $this->input->post('i_j');
        $mensaje = $this->jobs->Actualizar($onoff,$id_job);
        echo json_encode($mensaje);
    }
    
    private function _in_job(){
        $mensaje = $this->jobs->Iniciar('ROBOT_GENERACION');
        echo $mensaje;
    }
    
    private function _st_job(){
        $actividad = $this->jobs->Actividad('ROBOT_GENERACION');
        echo json_encode($actividad);
    }
    
    function _seaNumCed (){
        $co_bus = $this->input->post('c_b');
        $fe_bus = $this->input->post('f_f');
        $mensaje = $this->m_128->BuscaCedulaNumero($fe_bus,$co_bus);
        echo json_encode($mensaje);
    }
    
    function _exportXls(){
        
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '-1');
        
        $data = $this->input->post();
        $datos = $this->m_128->retReport($data);
        
        if (!empty($datos)){
            
            $tab  = '<table>';
                $tab .= '<tr>';
                    $tab .= '<th>COD. CLIENTE</th>';
                    $tab .= '<th>CEDULA</th>';
                    $tab .= '<th>NOMBRES COMPLETOS</th>';
                    $tab .= '<th>DIAS VENCIDOS</th>';
                    $tab .= '<th>PAGO MINIMO</th>';
                    $tab .= '<th>ESTADO</th>';
                    $tab .= '<th>PROVEEDOR</th>';
                    $tab .= '<th>FECHA ENVIO</th>';
                    $tab .= '<th>CONTACTO</th>';
                    $tab .= '<th>MENSAJE</th>';
                    $tab .= '<th>RESPUESTA</th>';
                    $tab .= '<th>DURACION</th>';
                    $tab .= '<th>ESTADO</th>';
                $tab .= '</tr>';
            foreach ($datos as $row){
               
                $tab .= '<tr>';
                    $tab .= '<td>'.$row['co_cli'].'</td>';
                    $tab .= '<td>'.$row['cc_cli'].'</td>';
                    $tab .= '<td>'.$row['ap_cli']." ".$row['no_cli'].'</td>';
                    $tab .= '<td>'.$row['va_dia'].'</td>';
                    $tab .= '<td>'.$row['va_min'].'</td>';
                    $tab .= '<td>'.$row['no_est_cab'].'</td>';
                    $tab .= '<td>'.$row['no_pro'].'</td>';
                    $tab .= '<td>'.$row['fe_env'].'</td>';
                    $tab .= '<td>'.$row['contac'].'</td>';
                    $tab .= '<td>'.$row['tx_men'].'</td>';
                    $tab .= '<td>'.$row['tx_res'].'</td>';
                    $tab .= '<td>'.$row['va_dur'].'</td>';
                    $tab .= '<td>'.$row['no_est_det'].'</td>';
                $tab .= '</tr>';
                
            }
            
            $tab .= '</table>';

            $hoy = date("Y_m_d_h_i");  
            $filename='REPORTE_ROBOT_'.$hoy.'.xls'; //save our workbook as this file name
//            header('Content-Type: application/vnd.ms-excel'); //mime type
//            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
//            header('Cache-Control: max-age=1'); //no cache
//            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//            $objWriter->save('temp/'.$filename);
            //header("location: ".site_url()."temp/$filename"); 
            header ( "Content-type: application/vnd.ms-excel" );
            header ( "Content-Disposition: attachment; filename=$filename" );
            echo $tab;
            
            //unset($datos);

        } else {
            echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
            echo " <script> window.close();</script> ";
        }

        
    }
   
    function _exportExcel(){
        
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '-1');
        
        $data = $this->input->post();
        $datos = $this->m_128->retReport($data);
       
        if (!empty($datos)){
            $this->load->library('PHPExcel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->
            getProperties()
            ->setCreator("ROBOT")
            ->setTitle("ROBOT")
            ->setSubject("ROBOT")
            ->setDescription("ROBOT")
            ->setKeywords("ROBOT")
            ->setCategory("ROBOT"); 

            $borders1 = array (
                'borders' => array (
                  'allborders' => array (
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array ('rgb' => '000000'),
                  )
               ),
            );

            $borders = array (
                'borders' => array (
                  'allborders' => array (
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array ('rgb' => '000000'),
                  )
               ),
            );
            
            $Letra = "M";
            $linea = 1; 

            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':'.$Letra.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('ROBOT', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':'.$Letra.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('REPORTE DE GESTIONES', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':N'.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('FECHA DEL REPORTE: '.date('Y/m/d h:i:s'), PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea = $linea + 2;
            $linea++;
//            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->applyFromArray($borders1);
//            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setBold(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$linea, 'COD. CLIENTE')
                            ->setCellValue('B'.$linea, 'CEDULA')
                            ->setCellValue('C'.$linea, 'NOMBRES COMPLETOS')
                            ->setCellValue('D'.$linea, 'DIAS VENCIDOS')
                            ->setCellValue('E'.$linea, 'PAGO MINIMO')
                            ->setCellValue('F'.$linea, 'ESTADO')
                            ->setCellValue('G'.$linea, 'PROVEEDOR')
                            ->setCellValue('H'.$linea, 'FECHA ENVIO')
                            ->setCellValue('I'.$linea, 'CONTACTO')
                            ->setCellValue('J'.$linea, 'MENSAJE')
                            ->setCellValue('K'.$linea, 'RESPUESTA')
                            ->setCellValue('L'.$linea, 'DURACION')
                            ->setCellValue('M'.$linea, 'ESTADO');
//            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(12);
            
            $cod_cliente = 0;
            
            foreach ($datos as $row){
                $linea++;
//                $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->applyFromArray($borders);
                if($cod_cliente <> (int)$row['co_cli']){
                    $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit($row['co_cli']);
                    $objPHPExcel->getActiveSheet()->getCell('B'.$linea)->setValueExplicit($row['cc_cli']);
                    $objPHPExcel->getActiveSheet()->getCell('C'.$linea)->setValueExplicit($row['ap_cli']." ".$row['no_cli']);
                    $objPHPExcel->getActiveSheet()->getCell('D'.$linea)->setValueExplicit($row['va_dia']);
                    $objPHPExcel->getActiveSheet()->getCell('E'.$linea)->setValueExplicit($row['va_min']);
                    $objPHPExcel->getActiveSheet()->getCell('F'.$linea)->setValueExplicit($row['no_est_cab']);
                }
                
                $objPHPExcel->getActiveSheet()->getCell('G'.$linea)->setValueExplicit($row['no_pro']);
                $objPHPExcel->getActiveSheet()->getCell('H'.$linea)->setValueExplicit($row['fe_env']);
                $objPHPExcel->getActiveSheet()->getCell('I'.$linea)->setValueExplicit($row['contac']);
                $objPHPExcel->getActiveSheet()->getCell('J'.$linea)->setValueExplicit($row['tx_men']);
                $objPHPExcel->getActiveSheet()->getCell('K'.$linea)->setValueExplicit($row['tx_res']);
                $objPHPExcel->getActiveSheet()->getCell('L'.$linea)->setValueExplicit($row['va_dur']);
                $objPHPExcel->getActiveSheet()->getCell('M'.$linea)->setValueExplicit($row['no_est_det']);
                
                $cod_cliente = (int)$row['co_cli'];
                
            }
            
//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

            $hoy = date("Y_m_d_h_i");  
            $objPHPExcel->getActiveSheet()->setTitle('LISTA'); // Renombrando el worksheet            
            $filename='REPORTE_ROBOT_'.$hoy.'.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=1'); //no cache
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('temp/'.$filename);
            header("location: ".site_url()."temp/$filename"); 
            unset($datos);

        } else {
            echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
            echo " <script> window.close();</script> ";
        }

   }
   
}