<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_128 extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('proceso/ventas/m_128','jobs'));
        session_start();
    }

    public function _remap() {

        validaSession('procesos/ventas/co_128');

        $tipo=$this->uri->segment(4);
        
        if ($tipo=='exportExcel'){
            $this->_exportXls();
        } else if ($tipo=='seaNum'){
            $this->_seaNumCed();
        } else if ($tipo=='retCalls'){
            $this->_retCalls();
        } else {
            $this->_crud();
        }
        
    }        
        
    private function _crud(){
        
        $data                   =  null; 
        $urlControlador         =  'procesos/ventas/co_128';
        $data['permisos']       =  '';   
        $data['urlControlador'] =  $urlControlador;
        $data['idFormulario']   =  'co_128';
        
        $opcion = BuscaIdMenu($urlControlador,$_SESSION['menu']);
        $update = false;
        
        if(!empty($opcion)){
            
            if(isset($_SESSION['menu'][$opcion]['opciones']) && !empty($_SESSION['menu'][$opcion]['opciones'])){
               
                $arrayBusq = $_SESSION['menu'][$opcion]['opciones'];
                foreach ($arrayBusq AS $row){

                    if (isset($row['UPADTE']) && !empty($row['UPADTE'])){
                        $row['UPADTE'][0] == 1 ?$update = true:$update = false;
                    }

                }
                
            }
            
        }
        
        $data['update'] = $update;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                            'css/daterangepicker/daterangepicker-bs3.css',
                            'js/plugins/switchery/dist/switchery.css');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/daterangepicker/daterangepicker.js',
                            'js/plugins/switchery/dist/switchery.js',
                            'js/plugins/flot/jquery.flot.min.js',
                            'js/plugins/flot/jquery.flot.resize.min.js',
                            'js/plugins/flot/jquery.flot.pie.min.js',
                            'js/plugins/flot/Smooth-0.1.7.js',
                            'js/plugins/flot/jquery.flot.time.js',
                            'js/plugins/flot/jquery.flot.curvedlines-0.2.3.min.js',
                            'js/procesos/ventas/j_128.js'
            
        );
        
        //$fec_ini = date('Y-m-d',strtotime('-1 day'));
        $fec_ini = date('Y-m-d');
        $fec_fin = date('Y-m-d');
        
        $f_i = $this->input->post('f_i');
        $f_f = $this->input->post('f_f');
        
        $getFec_ini = !empty($f_i)?$f_i:$fec_ini;
        $getFec_fin = !empty($f_f)?$f_f:$fec_fin;
        
        $data['fec_ini'] = $getFec_ini;
        $data['fec_fin'] = $getFec_fin;
        
        $monitor      = $this->m_128->retMonitor($getFec_ini,$getFec_fin);
        
        /**********************************************************************/
        
        if(!empty($monitor)){
            
            if(!empty($monitor['res_2'])){
                
                $data['r2_Data'] = $monitor['res_2'];

            }
            
            if(!empty($monitor['res_3'])){
                
                $data['r3_Data'] = $monitor['res_3'];

            }
            
        }
        
        $data['reglas'] = $monitor;

        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/ventas/v_128');
        $this->load->view('barras/footer');   
        
    }

    private function _retCalls(){
        
        $detalle = null;
        $fe_i = $this->input->post('f_i');
        $fe_f = $this->input->post('f_f');
        $arrayRes = $this->m_128->retDetLlamada($fe_i,$fe_f);
        if(empty($arrayRes['err'])){
            
            $detalle = $this->retChartCalls($arrayRes['res_1']);
        }
        
        echo json_encode(array('ch_1'=>$detalle,'re_2'=>$arrayRes['res_2']));
        
    }
    
    function retChartCalls($arrayRes){
        
        $arrayHora = null;
        
        $color = array (
            '1' => array(
                'color'=>'#f0a30a'
            ),
            '2' => array(
                'color'=>'#F56954'
            ),
            '3' => array(
                'color'=>'#00A65A'
            ),
            '4' => array(
                'color'=>'#2E8BEF'
            ),
            '5' => array(
                'color'=>'#633EBE'
            ),
            '6' => array(
                'color'=>'#1DC096'
            ),
            '7' => array(
                'color'=>'#f04a96'
            )
        );
        
        $data = null;
        
        if(!empty($arrayRes)){
        
            $grupoArray = groupArray($arrayRes,'di_dial');
            
            //print_r($grupoArray);die;
            
            foreach ($grupoArray as $raw){
                
                $ar = null;
                
                foreach ($raw['groupeddata'] as $rew){
                   $arrayHora[] = $rew['ho_dial'];
                   $ar[] = array($rew['ca_dial']); 
                }
                
                $data[] = array (
                    'label' => $raw['groupeddata'][0]['tx_di_dial'],
                    'fillColor' => 'rgba(172,194,132,0.1)',
                    'strokeColor' => $color[$raw['di_dial']]['color'],
                    'pointColor' => $color[$raw['di_dial']]['color'],
                    'pointStrokeColor' => $color[$raw['di_dial']]['color'],
                    'data' => $ar
                );
                
            }
            
            $arrayHora = array_unique($arrayHora);
            return array('datos'=>$data,'ticks'=>$arrayHora);
            
        }
        
    }
    
    function _seaNumCed (){
        $co_bus = $this->input->post('c_b');
        $fe_ini = date('Y-m-d',strtotime('-2 month'));
        $fe_fin = date('Y-m-d');
        $mensaje = $this->m_128->BuscaCedulaNumero($fe_ini,$fe_fin,$co_bus);
        echo json_encode($mensaje);
    }
    
    function _exportXls(){
        
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '-1');
        
        $data = $this->input->post();
        $datos = $this->m_128->retReport($data);
        
        if (!empty($datos)){
            
//            'fe_gen'=> trim($row['fe_generacion']),
//            'co_cam'=> (int)$row['co_campania'],
//            'ds_cam'=> trim($row['ds_campania']),
//            'co_cli'=> (int)$row['co_cliente'],
//            'cc_cli'=> trim($row['co_cedula']),
//            'ap_cli'=> trim(utf8_encode($row['cl_apellidos'])),
//            'no_cli'=> trim(utf8_encode($row['cl_nombres'])),
//            'va_dia'=> (int)$row['va_dias_vencidos'],
//            'va_min'=> (float)$row['va_pago_minimo'],
//            'co_est_cab'=> trim($row['co_estado_cab']),
//            'no_est_cab'=> $this->estados[$row['co_estado_cab']]['name'],
//            'co_sec'=> (int)$row['co_secuencia'],
//            'co_can'=> (int)$row['co_canal'],
//            'co_pro'=> (int)$row['co_proveedor'],
//            'no_pro'=> trim($row['tx_descripcion']),
//            'fe_env'=> trim($row['fe_envio']),
//            'contac'=> trim($row['contacto']),
//            'tx_men'=> trim(utf8_encode($row['tx_mensaje'])),
//            'tx_res'=> trim(utf8_encode($row['tx_respuesta_envio'])),
//            'va_dur'=> trim($row['va_duracion']),
//            'co_est_det'=> trim($row['co_estado_det']),
//            'no_est_det'=> $this->estados[$row['co_estado_det']]['name']
            
//            $tab  = '<table>';
//                $tab .= '<tr>';
//                    $tab .= '<th>FECHA</th>';
//                    $tab .= '<th>TRAMO</th>';
//                    $tab .= '<th>COD. CLIENTE</th>';
//                    $tab .= '<th>CEDULA</th>';
//                    $tab .= '<th>NOMBRES COMPLETOS</th>';
//                    $tab .= '<th>DIAS VENCIDOS</th>';
//                    $tab .= '<th>PAGO MINIMO</th>';
//                    $tab .= '<th>ESTADO</th>';
//                    $tab .= '<th>PROVEEDOR</th>';
//                    $tab .= '<th>FECHA ENVIO</th>';
//                    $tab .= '<th>CONTACTO</th>';
//                    $tab .= '<th>MENSAJE</th>';
//                    $tab .= '<th>RESPUESTA</th>';
//                    $tab .= '<th>DURACION</th>';
//                    $tab .= '<th>ESTADO</th>';
//                $tab .= '</tr>';
//            foreach ($datos as $row){
//               
//                $tab .= '<tr>';
//                    $tab .= '<td>'.$row['fe_gen'].'</td>';
//                    $tab .= '<td>'.$row['ds_cam'].'</td>';
//                    $tab .= '<td>'.$row['co_cli'].'</td>';
//                    $tab .= '<td>'.$row['cc_cli'].'</td>';
//                    $tab .= '<td>'.$row['ap_cli']." ".$row['no_cli'].'</td>';
//                    $tab .= '<td>'.$row['va_dia'].'</td>';
//                    $tab .= '<td>'.$row['va_min'].'</td>';
//                    $tab .= '<td>'.$row['no_est_cab'].'</td>';
//                    $tab .= '<td>'.$row['no_pro'].'</td>';
//                    $tab .= '<td>'.$row['fe_env'].'</td>';
//                    $tab .= '<td>'.$row['contac'].'</td>';
//                    $tab .= '<td>'.utf8_encode($row['tx_men']).'</td>';
//                    $tab .= '<td>'.$row['tx_res'].'</td>';
//                    $tab .= '<td>'.$row['va_dur'].'</td>';
//                    $tab .= '<td>'.$row['no_est_det'].'</td>';
//                $tab .= '</tr>';
//                
//            }
//            
//            $tab .= '</table>';
            
            $hoy = date("Y_m_d_h_i");  
            $filename='REPORTE_ROBOT_'.$hoy.'.csv'; //save our workbook as this file name
//            header('Content-Type: application/vnd.ms-excel'); //mime type
//            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
//            header('Cache-Control: max-age=1'); //no cache
//            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//            $objWriter->save('temp/'.$filename);
            //header("location: ".site_url()."temp/$filename"); 
            header ( "Content-type: text/csv" );
            header ( "Content-Disposition: attachment; filename=$filename" );
            header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
            header("Pragma: no-cache"); // HTTP 1.0
            header("Expires: 0"); // Proxies
            
            echo $tab  = 'FECHA,TRAMO,COD. CLIENTE,CEDULA,NOMBRES COMPLETOS,DIAS VENCIDOS,PAGO MINIMO,ESTADO,PROVEEDOR,FECHA ENVIO,CONTACTO,MENSAJE,RESPUESTA,DURACION,ESTADO\n';
            //    $tab .= '</tr>';
            foreach ($datos as $row){
                $tab = $row['fe_gen'].',';
                $tab .= $row['ds_cam'].',';
                $tab .= $row['co_cli'].',';
                $tab .= $row['cc_cli'].',';
                $tab .= $row['ap_cli']." ".$row['no_cli'].',';
                $tab .= $row['va_dia'].',';
                $tab .= $row['va_min'].',';
                $tab .= $row['no_est_cab'].',';
                $tab .= $row['no_pro'].',';
                $tab .= $row['fe_env'].',';
                $tab .= $row['contac'].',';
                $tab .= utf8_encode($row['tx_men']).',';
                $tab .= $row['tx_res'].',';
                $tab .= $row['va_dur'].',';
                echo $tab .= $row['no_est_det'].'\n';
            }

            //unset($datos);
            
            
            

        } else {
            echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
            echo " <script> window.close();</script> ";
        }

        
    }
   
    function _exportExcel(){
        
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '-1');
        
        $data = $this->input->post();
        $datos = $this->m_128->retReport($data);
       
        if (!empty($datos)){
            $this->load->library('PHPExcel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->
            getProperties()
            ->setCreator("ROBOT")
            ->setTitle("ROBOT")
            ->setSubject("ROBOT")
            ->setDescription("ROBOT")
            ->setKeywords("ROBOT")
            ->setCategory("ROBOT"); 

            $borders1 = array (
                'borders' => array (
                  'allborders' => array (
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array ('rgb' => '000000'),
                  )
               ),
            );

            $borders = array (
                'borders' => array (
                  'allborders' => array (
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array ('rgb' => '000000'),
                  )
               ),
            );
            
            $Letra = "M";
            $linea = 1; 

            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':'.$Letra.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('ROBOT', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':'.$Letra.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('REPORTE DE GESTIONES', PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$linea.':N'.$linea);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);         
            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.'')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit('FECHA DEL REPORTE: '.date('Y/m/d h:i:s'), PHPExcel_Cell_DataType::TYPE_STRING);
            
            $linea = $linea + 2;
            $linea++;
//            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->applyFromArray($borders1);
//            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setBold(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$linea, 'COD. CLIENTE')
                            ->setCellValue('B'.$linea, 'CEDULA')
                            ->setCellValue('C'.$linea, 'NOMBRES COMPLETOS')
                            ->setCellValue('D'.$linea, 'DIAS VENCIDOS')
                            ->setCellValue('E'.$linea, 'PAGO MINIMO')
                            ->setCellValue('F'.$linea, 'ESTADO')
                            ->setCellValue('G'.$linea, 'PROVEEDOR')
                            ->setCellValue('H'.$linea, 'FECHA ENVIO')
                            ->setCellValue('I'.$linea, 'CONTACTO')
                            ->setCellValue('J'.$linea, 'MENSAJE')
                            ->setCellValue('K'.$linea, 'RESPUESTA')
                            ->setCellValue('L'.$linea, 'DURACION')
                            ->setCellValue('M'.$linea, 'ESTADO');
//            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//            $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->getFont()->setSize(12);
            
            $cod_cliente = 0;
            
            foreach ($datos as $row){
                $linea++;
//                $objPHPExcel->getActiveSheet()->getStyle('A'.$linea.':'.$Letra.$linea)->applyFromArray($borders);
                if($cod_cliente <> (int)$row['co_cli']){
                    $objPHPExcel->getActiveSheet()->getCell('A'.$linea)->setValueExplicit($row['co_cli']);
                    $objPHPExcel->getActiveSheet()->getCell('B'.$linea)->setValueExplicit($row['cc_cli']);
                    $objPHPExcel->getActiveSheet()->getCell('C'.$linea)->setValueExplicit($row['ap_cli']." ".$row['no_cli']);
                    $objPHPExcel->getActiveSheet()->getCell('D'.$linea)->setValueExplicit($row['va_dia']);
                    $objPHPExcel->getActiveSheet()->getCell('E'.$linea)->setValueExplicit($row['va_min']);
                    $objPHPExcel->getActiveSheet()->getCell('F'.$linea)->setValueExplicit($row['no_est_cab']);
                }
                
                $objPHPExcel->getActiveSheet()->getCell('G'.$linea)->setValueExplicit($row['no_pro']);
                $objPHPExcel->getActiveSheet()->getCell('H'.$linea)->setValueExplicit($row['fe_env']);
                $objPHPExcel->getActiveSheet()->getCell('I'.$linea)->setValueExplicit($row['contac']);
                $objPHPExcel->getActiveSheet()->getCell('J'.$linea)->setValueExplicit($row['tx_men']);
                $objPHPExcel->getActiveSheet()->getCell('K'.$linea)->setValueExplicit($row['tx_res']);
                $objPHPExcel->getActiveSheet()->getCell('L'.$linea)->setValueExplicit($row['va_dur']);
                $objPHPExcel->getActiveSheet()->getCell('M'.$linea)->setValueExplicit($row['no_est_det']);
                
                $cod_cliente = (int)$row['co_cli'];
                
            }
            
//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

            $hoy = date("Y_m_d_h_i");  
            $objPHPExcel->getActiveSheet()->setTitle('LISTA'); // Renombrando el worksheet            
            $filename='REPORTE_ROBOT_'.$hoy.'.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=1'); //no cache
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('temp/'.$filename);
            header("location: ".site_url()."temp/$filename"); 
            unset($datos);

        } else {
            echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
            echo " <script> window.close();</script> ";
        }

   }
   
}