<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_118 extends CI_Controller {	
	
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/ventas/m_118'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/ventas/co_118');  
        
        $tipo=$this->uri->segment(4);

        if($tipo=='reDoc'){
            $this->_reDocsProv();
        } else if($tipo=='updaEstd'){
            $this->_updaEstd();
        } else {
            $this->_crud(); 
        }
        
    }        

    private function _crud(){
        
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/ventas/co_118';
        $data['urlControlador'] = $urlControlador;
        $data['css'] = array('css/datatables/dataTables.bootstrap.css','css/datetimepicker/jquery.datetimepicker.css');
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/ventas/j_118.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js');

        $data['idFormulario'] = 'co_118';

        $getProvee = $this->m_118->getProvIndu();
        
        $datosParametro=null;
        if (!empty($getProvee)){
                $datosParametro['NNN']='Seleccione un proveedor';
            foreach ($getProvee as $row){
                $datosParametro[$row['cod_pro']] = $row['nom_pro'];                                     
            }
        } else {
            $datosParametro['NNN']='No Existen Proveedores';    
        }
        
        $atributosEspecial="id='cmb_prov' class='form-control input-sm'";
        $data['cmb_prov']=form_dropdown('cmb_prov',$datosParametro,'NNN',$atributosEspecial); 
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/ventas/v_118');
        $this->load->view('barras/footer');            
    }

    private function _reDocsProv(){
        $datos = $this->input->post();
        $mensaje = $this->m_118->reDocsProv($datos);
        echo json_encode($mensaje);
    }
    
    private function _updaEstd(){
        $datos = $this->input->post();
        $mensaje = $this->m_118->updateEstado($datos);
        echo $mensaje;
    }

}