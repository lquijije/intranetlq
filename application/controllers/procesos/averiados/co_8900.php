<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_8900 extends CI_Controller {	
	
	function __construct()
	{		
		parent::__construct();	
                $this->load->model(array('proceso/averiados/m_8900'));
                session_start();
        }	
        
        public function _remap() {
            
            validaSession('procesos/averiados/co_8900'); 
            
            $tipo=$this->uri->segment(4);
            
            if($tipo=='retReclamos'){
                $this->_retReclamos();
            } else if($tipo=='viewReclamo'){
                $this->_viewReclamo();
            } else if($tipo=='viewPDF'){
                $a = $this->uri->segment(5);
                $b = $this->uri->segment(6);
                $this->_viewPDF($a,$b);
            } else if($tipo=='saveAcciones'){
                $this->_saveAcciones();
            } else if($tipo=='finReclamo'){
                $this->_finReclamo();
            } else {
                $this->_crud(); 
            }
        }        
        
        private function _crud(){
            $data=null; 
            $data['permisos']='';   
            $urlControlador = 'procesos/averiados/co_8900';
            $data['urlControlador'] = $urlControlador;
            
            $data['css'] = array('css/datatables/dataTables.bootstrap.css');
            
            $data['js']=array('js/jquery-1.10.2.min.js',
                              'js/general.js',
                              'js/procesos/averiados/j_8900.js',
                              'js/plugins/datatables/jquery.dataTables.js',
                              'js/plugins/datatables/dataTables.bootstrap.js',
                              'js/bootstrap.min.js');
            
            $data['idFormulario'] = 'co_8900';
            
            $reclamos =$this->m_8900->retReclamos();
            
            $data['reclamos'] = $reclamos;
            
            $this->load->view('barras/header',$data);
            $this->load->view('barras/menu');
            $this->load->view('procesos/averiados/v_8900');
            $this->load->view('barras/footer');            
        }
        
        private function _saveAcciones(){
            $data =$this->input->post();
            $reclamos =$this->m_8900->saveAcciones($data);
            echo json_encode($reclamos);
        }
        
        private function _finReclamo(){
            $data =$this->input->post();
            $reclamos =$this->m_8900->finReclamo($data);
            echo json_encode($reclamos);
        }
        
        private function _retReclamos(){
            $ci =$this->input->post('c_c');
            $reclamos =$this->m_8900->retReclamos($ci);
            echo json_encode($reclamos);
        }
        
        private function _viewReclamo(){
            
            $cod_recl =$this->input->post('c_r');
            $cod_bdg =$this->input->post('c_b');
            $view =$this->m_8900->retReclamos('',$cod_recl,$cod_bdg);
            $tipo_recl = $des_cla = $des_pre = '';
            
            if (!empty($view['data']['cab'])){
                
                $urlControlador = 'procesos/averiados/co_8900';

                $opcion = BuscaIdMenu($urlControlador,$_SESSION['menu']);

                $finalizan = null;

                if(!empty($opcion)){
                    if(isset($_SESSION['menu'][$opcion]['opciones']) && !empty($_SESSION['menu'][$opcion]['opciones'])){
                        $arrayBusq = $_SESSION['menu'][$opcion]['opciones'];
                        foreach ($arrayBusq AS $row){
                            if (isset($row['FINALIZA']) && !empty($row['FINALIZA'])){
                                $finalizan = $row['FINALIZA'];
                            }
                        } 
                    }
                }
                
                for ($a = 0; $a<count($view['data']['cab']); $a++){

                    if ($a === 0){
                        
                        $tipo_recl = $view['data']['cab'][$a]['tip_rec'];
                        $cod_alm = $view['data']['cab'][$a]['cod_alm'];
                        $des_cla = trim($view['data']['cab'][$a]['des_cla']);
                        $des_pre = trim(utf8_encode($view['data']['cab'][$a]['dec_obj']));
                        
                        $string ='<div class="spa-12">';
                        $string.='<h4 style="font-weight:bold;">Identificaci&oacute;n del Bien o servicio</h4>';
                        $string.='</div>';
                            
                        if (trim($tipo_recl) === 'S'){

                            $string.='<div class="spa-12">';
                                $string.='<label>Descripci&oacute;n del Servicio</label>';
                                $string.='<textarea id="g" name="g" class="form-control" readonly>'.trim(utf8_encode($view['data']['cab'][$a]['des_abr'])).'</textarea>';
                            $string.='</div>';
                            
                        } else {
                            
                            $string.='<div class="spa-12"><table class="table table-bordered TFtable">';
                            $string.='<thead>';
                            $string.='<tr>';
                            $string.='<th class="centro" colspan="3">DETALLE DE PRODUCTOS</th>';
                            $string.='</tr>';
                            $string.='<tr>';
                            $string.='<th>C&oacute;digo</th>';
                            $string.='<th>Nombre</th>';
                            $string.='<th>Descripci&oacute;n</th>';
                            $string.='</tr>';
                            $string.='</thead>';
                            $string.='<tbody>';
                            
                        }
                        
                    }
                    
                    if (trim($view['data']['cab'][$a]['tip_rec']) === 'B'){

                        $string.='<tr>';
                        $string.='<td>'.$view['data']['cab'][$a]['cod_art'].'</td>';
                        $string.='<td>'.trim(utf8_encode($view['data']['cab'][$a]['nom_art'])).'</td>';
                        $string.='<td>'.trim(utf8_encode($view['data']['cab'][$a]['des_art'])).'</td>';
                        $string.='</tr>';
                        
                    }
                    
                }
                
                if (trim($tipo_recl) === 'B'){
                    
                    $string.='</tbody>';
                    $string.='</table></div>';
                    
                }
                $string.='<div class="spa-12">';
                $string.='<h4 style="font-weight:bold;">Descripci&oacute;n del Reclamo</h4>';
                $string.='<div class="spa-12">';
                    $string.='<label>Descripci&oacute;n clara y concisa del reclamo</label>';
                    $string.='<textarea id="g" name="g" class="form-control" readonly>'.$des_cla.'</textarea>';
                $string.='</div>';
                $string.='<div class="spa-12">';
                    $string.='<label>Resultado que pretende obtener del reclamo</label>';
                    $string.='<textarea id="g" name="g" class="form-control" readonly>'.$des_pre.'</textarea>';
                $string.='</div>';
                $string.='</div>';
                
                $string.='<div class="spa-12" style="padding: 4px; margin-top: 8px;">';
                
                foreach ($finalizan as $row){
                    if (trim($row) === trim($_SESSION['c_e'])){
                        $string.='<button type="button" class="btn btn-danger pull-left" onclick="endReclamo('."'".$cod_alm."'".','."'".$cod_recl."'".');"><i class="fa fa-check"></i> Finalizar Reclamo</button>';
                    }
                }
                
                $string.='<button type="button" class="btn btn-success pull-left" onclick="getPDF('."'".$cod_alm."'".','."'".$cod_recl."'".');"  style="margin-left: 10px;"><i class="fa fa-file-pdf-o"></i> PDF</button>';
                $string.='<div class="clearfix"></div>';
                $string.='</div>';
                
                $string.='<div class="clearfix"></div>';

                echo json_encode(array('cab'=>$string,'det'=>$view['data']['det']));
                
            } else {
                echo json_encode($view);
            }
            
        }
        
        private function _viewPDF($cod_bdg,$cod_recl){
            
            $datos = $this->m_8900->retReclamos('',$cod_recl,$cod_bdg);
            
            if (!empty($datos['data']['cab'])) {
                
                $this->load->library('pdf');
                $pdf = new PDF();
                $pdf->fpdf('P','mm','A4');
                $pdf->SetMargins(5,5,5,5);
                $pdf->AliasNbPages();
                $pdf->setHeader('1.1.1');
                $pdf->AddPage();
                $pdf->SetFillColor(255,255,255);
                $pdf->SetDrawColor(0,0,0);
                $pdf->SetTextColor(0,0,0);
                
                $pdf->Ln(8);
                        
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(160, 12, utf8_decode(" ALMACÉN ").utf8_encode($datos['data']['cab'][0]['nom_alm']),1, 0,'C','true');
                $pdf->Cell(40, 12, '# '.utf8_encode($datos['data']['cab'][0]['cod_rec']),1, 0,'C','true');
                $pdf->Ln(12);
                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(20, 5, utf8_decode(" Fecha:"),1, 0,'L','true');
                $pdf->Cell(15, 5, utf8_decode(" Día:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, date('d',strtotime($datos['data']['cab'][0]['fec_rec'])),1, 0,'C','true');

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(15, 5, utf8_decode(" Mes:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, date('m',strtotime($datos['data']['cab'][0]['fec_rec'])),1, 0,'C','true');

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(15, 5, utf8_decode(" Año:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, date('Y',strtotime($datos['data']['cab'][0]['fec_rec'])),1, 0,'C','true');

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(15, 5, utf8_decode(" Hora:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, date('H:i',strtotime($datos['data']['cab'][0]['fec_rec'])),1, 0,'C','true');

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(20, 5, utf8_decode(" Código:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, $datos['data']['cab'][0]['cod_alm'],1, 0,'C','true');

                $pdf->Ln(5); 

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(200, 8, utf8_decode(" 1. Identificación del Proveedor"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);

                $pdf->Ln(8);

                $pdf->Cell(120, 5, utf8_decode(" Nombre de la persona natural o razón social de la persona jurídica (proveedor): "),1, 0,'L','true');
                $pdf->Cell(80, 5, utf8_encode($datos['data']['cab'][0]['nom_alm']),1, 0,'L','true');

                $pdf->Ln(5); 

                $pdf->Cell(120, 5, utf8_decode(" Dirección del domicilio del establecimiento donde se coloca el libro de reclamos: "),1, 0,'L','true');
                $pdf->Cell(80, 5, utf8_encode($datos['data']['cab'][0]['dir_alm']),1, 0,'L','true');

                $pdf->Ln(5); 

                $pdf->Cell(120, 5, utf8_decode(" Número de R.U.C. o R.I.S.E. del proveedor: "),1, 0,'L','true');
                $pdf->Cell(80, 5, utf8_encode($datos['data']['cab'][0]['ruc_alm']),1, 0,'L','true');

                $pdf->Ln(5); 

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(200, 8, utf8_decode(" 2. Identificación de la persona consumidora"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);

                $pdf->Ln(8); 

                $pdf->Cell(35, 5, utf8_decode(" Nombre y Apellidos: "),1, 0,'L','true');
                $pdf->Cell(165, 5, utf8_encode($datos['data']['cab'][0]['nom_cli']),1, 0,'L','true');

                $pdf->Ln(5); 

                $pdf->Cell(35, 5, utf8_decode(" Dirección: "),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(165, 5, utf8_encode($datos['data']['cab'][0]['dir_cli']),1, 0,'L','true');

                $pdf->Ln(5); 

                $pdf->Cell(35, 5, utf8_decode(" Identificación: "),1, 0,'L','true');
                $pdf->Cell(65, 5, trim($datos['data']['cab'][0]['id_cli']),1, 0,'L','true');

                $pdf->Cell(35, 5, utf8_decode(" Teléfono Convencional: "),1, 0,'L','true');
                $pdf->Cell(65, 5, utf8_encode($datos['data']['cab'][0]['tel_con']),1, 0,'L','true');

                $pdf->Ln(5); 

                $pdf->Cell(35, 5, utf8_decode(" Teléfono Celular: "),1, 0,'L','true');
                $pdf->Cell(65, 5, utf8_encode($datos['data']['cab'][0]['tel_cel']),1, 0,'L','true');

                $pdf->Cell(35, 5, utf8_decode(" Dirección Electrónica: "),1, 0,'L','true');
                $pdf->Cell(65, 5, utf8_encode($datos['data']['cab'][0]['ema_cli']),1, 0,'L','true');

                $pdf->Ln(5); 

                $pdf->Cell(65, 5, utf8_decode(" Forma en que desea recibir la respuesta a este reclamo: "),1, 0,'L','true');
                $pdf->Cell(35, 5, utf8_decode(" En su dirección electronica"),1, 0,'C','true');
                $pdf->Cell(10, 5, (int)$datos['data']['cab'][0]['r_mai'] === 1 ?'X':'' ,1, 0,'C','true');
                $pdf->Cell(35, 5, utf8_decode(" En su teléfono convencional"),1, 0,'C','true');
                $pdf->Cell(10, 5, (int)$datos['data']['cab'][0]['r_con'] === 1 ?'X':'' ,1, 0,'C','true');
                $pdf->Cell(35, 5, utf8_decode(" En su teléfono celular "),1, 0,'C','true');
                $pdf->Cell(10, 5, (int)$datos['data']['cab'][0]['r_cel'] === 1 ?'X':'' ,1, 0,'C','true');

                $pdf->Ln(5); 

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(200, 8, utf8_decode(" 3. Identificación del bien o servicio"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);

                $pdf->Ln(8); 
                //
                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(40, 5, utf8_decode(" Servicio"),1, 0,'L','true');
                $pdf->Cell(10, 5, trim($datos['data']['cab'][0]['tip_rec']) === 'S' ?'X':'',1, 0,'C','true');
                $pdf->Cell(40, 5, utf8_decode(" Bien / Producto"),1, 0,'L','true');
                $pdf->Cell(10, 5, trim($datos['data']['cab'][0]['tip_rec']) === 'B' ?'X':'' ,1, 0,'C','true');
                $pdf->Cell(60, 5, utf8_decode(" No. De Factura / Nota de Venta"),1, 0,'L','true');
                $pdf->Cell(40, 5, trim(utf8_decode($datos['data']['cab'][0]['num_fac'])),1, 0,'C','true');
                $pdf->Ln(5); 
                
                if(trim($datos['data']['cab'][0]['tip_rec']) === 'S'){
                    
                    $pdf->SetFont('helvetica', '', 7);
                    $pdf->Cell(200, 5, utf8_decode(" Descripción del servicio:"),1, 0,'L','true');
                    $pdf->Ln(5);
                    $pdf->SetFont('helvetica', '', 7);
                    $pdf->MultiCell(200,5, utf8_decode($datos['data']['cab'][0]['des_abr']),1,1);   

                } else {
                    
                    $pdf->SetFont('helvetica', '', 7);
                    $pdf->Cell(200, 5, utf8_decode(" Detalle de Productos"),1, 0,'C','true');
                    $pdf->Ln(5);
                    $pdf->Cell(20, 5, utf8_decode(" Código"),1, 0,'C','true');
                    $pdf->Cell(60, 5, utf8_decode(" Nombre"),1, 0,'C','true');
                    $pdf->Cell(120, 5, utf8_decode(" Descripción"),1, 0,'C','true');
                    $pdf->Ln(5);
                    
                    for ($i = 0; $i<count($datos['data']['cab']); $i++){
                        
                        $pdf->Cell(20, 5, utf8_decode($datos['data']['cab'][$i]['cod_art']),1, 0,'C','true');
                        $pdf->Cell(60, 5, utf8_decode($datos['data']['cab'][$i]['nom_art']),1, 0,'C','true');
                        $pdf->Cell(120, 5, utf8_decode($datos['data']['cab'][$i]['des_art']),1, 0,'L','true');
                        $pdf->Ln(5);

                    }
                
                }

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(200, 8, utf8_decode(" 4. Identificación del reclamo"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);

                $pdf->Ln(8); 

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(60, 5, utf8_decode(" Fecha de Inconveniente:"),1, 0,'L','true');
                $pdf->Cell(15, 5, utf8_decode(" Día:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, date('d',strtotime($datos['data']['cab'][0]['fec_inc'])),1, 0,'C','true');

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(15, 5, utf8_decode(" Mes:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, date('m',strtotime($datos['data']['cab'][0]['fec_inc'])),1, 0,'C','true');

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(15, 5, utf8_decode(" Año:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, date('Y',strtotime($datos['data']['cab'][0]['fec_inc'])),1, 0,'C','true');

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(15, 5, utf8_decode(" Hora:"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Cell(20, 5, date('H:i',strtotime($datos['data']['cab'][0]['fec_inc'])),1, 0,'C','true');

                $pdf->Ln(5);
                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(200, 5, utf8_decode(" Descripción clara y concisa del reclamo:"),1, 0,'L','true');
                $pdf->Ln(5);
                $pdf->SetFont('helvetica', '', 7);
                $pdf->MultiCell(200,5, utf8_decode($datos['data']['cab'][0]['des_cla']),1,1);   
                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(200, 5, utf8_decode(" Resultado que pretende obtener con el reclamo:"),1, 0,'L','true');
                $pdf->Ln(5);
                $pdf->SetFont('helvetica', '', 7);
                $pdf->MultiCell(200,5, utf8_decode($datos['data']['cab'][0]['dec_obj']),1,1);

                $pdf->SetFont('helvetica', 'B', 7);
                $pdf->Cell(200, 8, utf8_decode(" 5. Acciones adoptadas por el proveedor"),1, 0,'L','true');
                $pdf->SetFont('helvetica', '', 7);
                $pdf->Ln(8); 
                
                if (!empty($datos['data']['det'])){
                    
                    for ($a = 0; $a<count($datos['data']['det']); $a++){
                        $pdf->MultiCell(200,5, date('Y/m/d',strtotime($datos['data']['det'][$a]['fec_acc']))." - ".utf8_decode($datos['data']['det'][$a]['des_det']),1,1);
                    }
                    
                } else {
                    
                    $pdf->MultiCell(200,5, utf8_decode('Solicitud de Reclamo en espera de acciones.'),1,1);
                    
                }
                
                $pdf->Ln(5); 
                $pdf->Cell(35, 20,'',0, 0,'L','true');
                $pdf->Cell(55, 20,'',1, 0,'L','true');
                $pdf->Cell(25, 20,'',0, 0,'L','true');
                $pdf->Cell(55, 20,'',1, 0,'L','true');
                $pdf->Ln(20);
                $pdf->Cell(35);
                $pdf->Cell(55, 5,'Firma de la persona Consumidora',1, 0,'C','true');
                $pdf->Cell(25);
                $pdf->Cell(55, 5,'Firma / Sello Proveedor',1, 0,'C','true');
                $pdf->SetFont('helvetica', 'B', 7);
                
                unset($datos);
                $pdf->Output();

            } else {
               echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
               echo " <script> window.close();</script> ";
            }
            
        }

}