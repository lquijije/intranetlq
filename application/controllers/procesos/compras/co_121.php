<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_121 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->model(array('proceso/compras/m_121'));
        session_start();
    }	

    public function _remap()  {    
        
        validaSession('procesos/compras/co_121');  
        
        $tipo=$this->uri->segment(4);
        
        if ($tipo == 'aprSol'){
            $this->_aproSolicit();
        } else if ($tipo == 'viSol'){
            $this->_viewSolicit();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud(){
        
        $data = null; 
        $datosParametro=null;

        $urlControlador = 'procesos/compras/co_121';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] =  array('css/datatables/dataTables.bootstrap.css');
            
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/procesos/compras/j_121.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/general.js');
        $cod_empr = 27;
        $data['cod_empr'] = $cod_empr;
        $solicitudes = $this->m_121->retSolicitudes($cod_empr); 
        $data['solicitudes'] = $solicitudes;
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/compras/v_121');
        $this->load->view('barras/footer');
    }
    
    private function _aproSolicit(){
        $data = $this->input->post();
        $mensaje = $this->m_121->aprobarSolicit($data);
        echo json_encode($mensaje);
    }

    private function _viewSolicit(){
        $data = $this->input->post();
        $mensaje = $this->m_121->retDetSolicitudes($data['i_e'],$data['i_s']);

        if(isset($mensaje['dat'])){
            $total = 0;

            $string ='<div class="row" style="margin-bottom: 10px; padding-bottom: 10px; padding-left: 30px; padding-right: 30px;">';

            $string.='<table id="detSol" class="table table-bordered TFtable">';
            $string.='<thead>';
            $string.='<tr>';
            $string.='<th>Cod. Secuencia</th>';
            $string.='<th>Cod. Item</th>';
            $string.='<th>Cod. Fabrica</th>';
            $string.='<th>Descripci&oacute;n</th>';
            $string.='<th>Valor FOB</th>';
            $string.='<th>Valor Pedido</th>';
            $string.='<th>Total</th>';
            $string.='</tr>';
            $string.='</thead>';
            $string.='<tbody>';

            foreach ($mensaje['dat'] as $row){
                $total+=(float)$row['val_tot'];
                $string.='<tr>';
                $string.='<td>'.$row['cod_sec'].'</td>';
                $string.='<td>'.$row['cod_ite'].'</td>';
                $string.='<td>'.$row['cod_fab'].'</td>';
                $string.='<td>'.$row['des_ite'].'</td>';
                $string.='<td class="derecha">'.number_format($row['val_fob'],2).'</td>';
                $string.='<td class="derecha">'.number_format($row['val_ped'],0).'</td>';
                $string.='<td class="derecha">'.number_format($row['val_tot'],2).'</td>';
                $string.='</tr>';
            }

            $string.='<tr style="font-weight:bold;">';
            $string.='<td colspan="6">Total</td>';
            $string.='<td class="derecha">'.number_format($total,2).'</td>';
            $string.='</tr>';

            $string.='</tbody>';
            $string.='</table>';

            $string.='</div>';

            echo json_encode(array(array('text'=>$string)));
        }
    }

}