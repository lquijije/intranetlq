<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_117 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        session_start();
    }	

    public function _remap()  {    
        $this->load->model(array('proceso/personal/m_117'));
        $this->_crud();
    }
        
    private function _crud(){
        
        validaSession('procesos/personal/co_117');  
        
        $data = null; 
        $urlControlador = 'procesos/personal/co_117';
        $data['urlControlador'] = $urlControlador;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/general.js');
        
        if ((int)$_SESSION['co_empresa'] === 1){
            $fechas = $this->_fechaPICA();
        } else {
            $fechas = $this->_fechaPYCCA();
        }
        
        $ID_FECH = $this->input->post('cmb_fechas');
        $ID_F = isset($ID_FECH) && !empty($ID_FECH)?$ID_FECH:$fechas[0]['id'];
        $in_array = in_array_r($ID_F,$fechas);
        $ID_F_1 = $in_array['fec_1'];
        $ID_F_2 = $in_array['fec_2'];
        
        $datosParametro=null;
        if (!empty($fechas)){
            foreach ($fechas as $row){
                $datosParametro[$row['id']]=$row['des'];                                     
            }
        } else {
            $datosParametro['NNN']='No Existen fechas';    
        }
        
        $marcaciones = $this->m_117->retMarcaciones($ID_F_1,$ID_F_2);
        $data['marcaciones'] = $marcaciones;
        $horario = $this->m_117->retHorario();
        $data['horario'] = $horario;
        
        $atributosEspecial="id='cmb_tipo' class='form-control' onchange='this.form.submit()'";
        $data['cmb_fechas']=form_dropdown('cmb_fechas',$datosParametro,trim($ID_F),$atributosEspecial); 
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/personal/v_117'); 
        $this->load->view('barras/footer');
    }
    
    private function _fechaPYCCA(){
        
//        $fechaAct = new DateTime('2015-05-06');
//        $fechaSistema = new DateTime('2015-05-06');
//        $fechaAct = date_format($fechaAct,'Y-m-d');
//        $fechaSistema = date_format($fechaSistema,'Y-m-d');
        
        $fechaAct = date('Y-m-d');
        $fechaSistema = date('Y-m-d');
        
        if(($fechaAct > date('Y-m',strtotime($fechaSistema))."-05") && ($fechaAct < date('Y-m',strtotime($fechaSistema))."-21")){
            $a = date('Y-m',strtotime($fechaSistema))."-05";
        } else if($fechaAct >= date('Y-m',strtotime($fechaSistema))."-21"){
            $a = date('Y-m',strtotime($fechaSistema))."-21";
        } else {
            $a = date('Y-m',strtotime('-1 month',strtotime($fechaSistema)))."-21";
        }
        
        $mes = getUltimoDiaMes(date('Y',strtotime($a)),date('m',strtotime($a)));
        $dia = date('d',strtotime($a));
        $suma = (int)$dia >= 5 ? 15 :((int)$mes - (int)$dia + 5);
        
        $b = date('Y-m-d',strtotime('+'.$suma.' day',strtotime($a)));
        $c = date('Y-m-d',strtotime('-1 month',strtotime($b)));
        $d = date('Y-m-d',strtotime('-1 month',strtotime($a)));
        $e = date('Y-m-d',strtotime('-1 month',strtotime($c)));
        $aa = (int)$dia === 5 ?date('Y-m-d',strtotime('+1 day',strtotime($a))):$a;
        $bb = (int)$dia === 5 ?date('Y-m-d',strtotime($a)):date('Y-m-d',strtotime('-1 day',strtotime($a)));
        $cc = (int)$dia === 5 ?date('Y-m-d',strtotime('+1 day',strtotime($d))):$d;
        $dd = (int)$dia === 5 ?date('Y-m-d',strtotime($d)):date('Y-m-d',strtotime('-1 day',strtotime($d)));
        
        $array = array(
            array(
                'id'=>'A',
                'fec_1'=>$aa,
                'fec_2'=>$b,
                'des'=>date('d/m/Y',strtotime($aa))." al ".date('d/m/Y',strtotime($b))
            ),
            array(
                'id'=>'B',
                'fec_1'=>date('Y-m-d',strtotime('+1 day',strtotime($c))),
                'fec_2'=>$bb,
                'des'=>date('d/m/Y',strtotime('+1 day',strtotime($c)))." al ".date('d/m/Y',strtotime($bb))
            ),
            array(
                'id'=>'C',
                'fec_1'=>$cc,
                'fec_2'=>$c,
                'des'=>date('d/m/Y',strtotime($cc))." al ".date('d/m/Y',strtotime($c))
            ),
            array(
                'id'=>'D',
                'fec_1'=>date('Y-m-d',strtotime('+1 day',strtotime($e))),
                'fec_2'=>$dd,
                'des'=>date('d/m/Y',strtotime('+1 day',strtotime($e)))." al ".date('d/m/Y',strtotime($dd))
            )
            
        );
        
        return $array;
        
    }
    
    private function _fechaPICA(){
        
        $fechaAct = date('Y-m-d');
        $fechaSistema = date('Y-m-d');
        
        $a = $fechaAct <= date('Y-m',strtotime($fechaSistema)).'-05'?date('Y-m-d',strtotime('-1 month',strtotime(date('Y-m',strtotime($fechaSistema)).'-5'))):date('Y-m',strtotime($fechaSistema)).'-05';
        $mes = getUltimoDiaMes(date('Y',strtotime($a)),date('m',strtotime($a)));
        $b = date('Y-m-d',strtotime('+'.$mes.' day',strtotime($a)));
        $c = date('Y-m-d',strtotime('-1 month',strtotime($a)));
        $d = date('Y-m-d',strtotime('-1 month',strtotime($c)));
       
        $array = array(
            array(
                'id'=>'A',
                'fec_1'=>date('Y-m-d',strtotime('+1 day',strtotime($a))),
                'fec_2'=>$b,
                'des'=>date('d/m/Y',strtotime('+1 day',strtotime($a)))." al ".date('d/m/Y',strtotime($b))
            ),
            array(
                'id'=>'B',
                'fec_1'=>date('Y-m-d',strtotime('+1 day',strtotime($c))),
                'fec_2'=>$a,
                'des'=>date('d/m/Y',strtotime('+1 day',strtotime($c)))." al ".date('d/m/Y',strtotime($a))
            ),
            array(
                'id'=>'C',
                'fec_1'=>date('Y-m-d',strtotime('+1 day',strtotime($d))),
                'fec_2'=>$c,
                'des'=>date('d/m/Y',strtotime('+1 day',strtotime($d)))." al ".date('d/m/Y',strtotime($c))
            )
            
        );
        
        return $array;
            
    }
    
}