<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_114 extends CI_Controller {	
	
    function __construct() {		
            parent::__construct();	
            $this->load->library(array('encrypt'));
            $this->load->model(array('proceso/personal/m_114'));
            session_start();
    }	

    public function _remap() {

        validaSession('procesos/personal/co_114'); 
        $tipo=$this->uri->segment(4);

        if($tipo=='retROL'){
            $this->_retROL();
        } else {
            $this->_crud();
        }

    }        

    private function _crud(){

        $data=null; 
        $data['permisos']='';   
        $urlControlador = 'publico/co_114';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array('css/datatables/dataTables.bootstrap.css');

        $data['js']=array('js/jquery-1.10.2.min.js',
                          'js/general.js',
                          'js/plugins/datatables/jquery.dataTables.js',
                          'js/plugins/datatables/dataTables.bootstrap.js',
                          'js/bootstrap.min.js');

        $data['idFormulario'] = 'co_114';

        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');

        $this->form_validation->set_rules('txt_clave', 'Clave', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE){ 

            $this->load->view('procesos/personal/v_114');

        } else {

            $devuelto = $this->_autenticacion($this->input->post('txt_clave'));
            
            if($devuelto=='true'){

                redirect(site_url('procesos/personal/co_114/retROL'));  

            } else {
                $this->session->set_flashdata('error',$devuelto);
                redirect(site_url('procesos/personal/co_114'));  
            }

        }

        $this->load->view('barras/footer');            
    }

    private function _retROL(){

        $data=null; 
        $data['permisos']='';   
        $urlControlador = 'publico/co_114';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array('css/datatables/dataTables.bootstrap.css');

        $data['js']=array('js/jquery-1.10.2.min.js',
                          'js/general.js',
                          'js/plugins/datatables/jquery.dataTables.js',
                          'js/plugins/datatables/dataTables.bootstrap.js',
                          'js/bootstrap.min.js');

        $data['idFormulario'] = 'co_114';

        $monthNames = Array();
        $monthNames[1]= "Enero";
        $monthNames[2]= "Febrero";
        $monthNames[3]= "Marzo";
        $monthNames[4]= "Abril";
        $monthNames[5]= "Mayo";
        $monthNames[6]= "Junio";
        $monthNames[7]= "Julio";
        $monthNames[8]= "Agosto";
        $monthNames[9]= "Septiembre";
        $monthNames[10]= "Octubre";
        $monthNames[11]= "Noviembre";
        $monthNames[12]= "Diciembre";

        $tipo_rol = array(
            array('id'=>'1','des'=>'Rol de Pago'),
            array('id'=>'37','des'=>'Fondo de Reserva'),
            array('id'=>'5,24','des'=>'Decimo Cuarto'),
            array('id'=>'3','des'=>'Decimo Tercero'),
            array('id'=>'8','des'=>'Utilidades'),
            array('id'=>'13,16','des'=>'Vacaciones')
        );
        
        $valida = $this->m_114->valiNomina();
        $empresas = $this->m_114->retEmpre();
        $data['empresas'] = $empresas;

        if (!empty($empresas)){
            
            $bono = $this->m_114->retBono();
           
            $data['bono'] = $bono;

            $Pool = $this->m_114->retlinePool();
            $data['linePool'] = $Pool;

            //--------------------------------------------------------------------------------------------
            //--------------------------------------------------------------------------------------------
            //--------------------------------------------------------------------------------------------

            $ID_TIPO = $this->input->post('cmb_tipo');
            $ID_EMPR = $this->input->post('cmb_emp');
            $ID_FECH = $this->input->post('cmb_fech_rol');

            $abc = explode('|*|',$ID_EMPR);
            $ID_T = isset($ID_TIPO) && !empty($ID_TIPO)?$ID_TIPO:$tipo_rol[0]['id'];
            $ID_E = isset($abc[0]) && !empty($abc[0])?$abc[0]:$empresas[0]['cod_emp'];
            $ID_R = isset($abc[1]) && !empty($abc[1])?$abc[1]:$empresas[0]['tip_rol'];
            $paso = (int)$ID_E == 27 ?$valida[0]['bd_pycca']:$valida[0]['bd_plast'];
            $data['valida'] = $paso == 0 ? false:true;
            
            if(in_array($_SERVER['REMOTE_ADDR'],$this->config->item('rol_pago'))){
                $data['valida'] = true;
            }
            
            $DesTip = in_array_r($ID_T,$tipo_rol);
            $data['tipo_compro'] = $DesTip['des'];

            $DesCco = in_array_r($ID_E,$empresas);
            $data['centro'] = $DesCco['des_cco'];
            $data['idd_emp'] = $ID_E."|*|".$ID_R;
            $data['idd_tip'] = $ID_T;

            //--------------------------------------------------------------------------------------------
            //-----------------------------  TIPO  DE ROL DE PAGO   --------------------------------------
            //--------------------------------------------------------------------------------------------

            $datosParametro=null;
            if (!empty($tipo_rol)){
                foreach ($tipo_rol as $row){
                    $datosParametro[$row['id']]=$row['des'];                                     
                }
            } else {
                $datosParametro['NNN']='No Existen Tipos';    
            }

            $atributosEspecial="id='cmb_tipo' class='form-control' onchange='this.form.submit()'";
            $data['cmb_tipo']=form_dropdown('cmb_tipo',$datosParametro,$ID_T,$atributosEspecial); 

            //--------------------------------------------------------------------------------------------
            //--------------------------------------------------------------------------------------------
            //--------------------------------------------------------------------------------------------

            $fechas = $this->m_114->retFechRol($ID_E,$ID_T,$ID_R);
            
            $ID_F = isset($ID_FECH) && !empty($ID_FECH)?$ID_FECH:$fechas[0]['fec_rol_1'];

            $DesFec = in_array_r($ID_F,$fechas);
            $data['fecha'] = $DesFec['est_sul']." (".$monthNames[(int)date('m',strtotime($ID_F))]." / ".date('Y',strtotime($ID_F)).")";

            $datosParametro=null;
            if (!empty($fechas)){
                foreach ($fechas as $row){
                    $datosParametro[$row['fec_rol_1']]=$row['fec_rol'];                                     
                }
            } else {
                $datosParametro['NNN']='No Existen fechas';    
            }

            $atributosEspecial="id='cmb_fech_rol' class='form-control' onchange='this.form.submit()' ";
            $data['cmb_fech_rol']=form_dropdown('cmb_fech_rol',$datosParametro,$ID_F,$atributosEspecial); 

            $datosParametro=null;
            if (!empty($empresas)){
                foreach ($empresas as $row){

                    $datosParametro[$row['cod_emp']."|*|".$row['tip_rol']]=$row['des_emp'];                                     
                }
            } else {
                $datosParametro['NNN']='No Existen empresas';    
            }

            $atributosEspecial="id='cmb_emp' class='form-control' onchange='this.form.submit()' ";
            $data['cmb_emp']=form_dropdown('cmb_emp',$datosParametro,$ID_E."|*|".$ID_R,$atributosEspecial); 
            //echo $ID_F."---".$fechas[0]['fec_rol_1'];

            $data['rolPago'] = $this->m_114->retRolPago($ID_E,$ID_F);
            

        } else {

            $this->session->set_flashdata('error','Usuario no tiene empresas.');
            redirect(site_url('procesos/personal/co_114')); 

        }

        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/personal/v_114_OK');
        $this->load->view('barras/footer');

    }

    private function _autenticacion($clave){
        
        if (!empty($_SESSION['c_e'])){
            if (!empty($clave)){
                
                $result = $this->m_114->valUser($_SESSION['c_e']);
               
                if(isset($result['co_error']) && empty($result['co_error']) ){
                    if (trim($clave) === $this->encrypt->decode(trim($result['data']['clave']))){
                        return true;
                    } else {
                        return 'Clave ingresada es incorrecta';
                    }
                } else {
                    return $result['co_error'];
                }
            }
        }

    }
        
}