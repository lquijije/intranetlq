<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_102 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation'));
        $this->load->model('compras_model');
        session_start();
    }	

    public function _remap()  {  
        validaSession('procesos/requisicion/c_102');  

        $tipo=$this->uri->segment(4);

        if($tipo=='guardar'){
            $this->_guardar();
        } else {
            $this->_crud();
        }
    }
        
    private function _crud(){
        
        $data = null; 
        $urlControlador = 'procesos/requisicion/c_102';
        $data['urlControlador'] = $urlControlador;
        
        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                            'css/datetimepicker/jquery.datetimepicker.css');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/general.js',
                            'js/bootstrap.min.js',
                            'js/procesos/j_102.js',
                            'js/procesos/compras.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js');
       
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/requisicion/v_102',$data); 
        $this->load->view('barras/footer');
    }
    
    private function _guardar (){

        $status = $this->input->post('act');
        $codigo = $this->input->post('id');
        $proveedor = $this->input->post('pro');
        $precio = $this->input->post('pre');
        
        $doc = array('status' => $status,'codigo' => $codigo,'proveedor' => $proveedor,'precio' => $precio);
        $result = $this->compras_model->saveRequisicionCotizar($doc);
        echo $result;
        
    }
}