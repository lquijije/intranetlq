<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_uploadVtex extends CI_Controller {

    var $urlControlador = 'procesos/pyccacom/co_uploadVtex';
    protected $url_file = '/var/www/html/pycca/images/vtex/';
    protected $url_pycca = 'http://www.pycca.com/images/vtex/';

    function __construct() {
        parent::__construct();
        $this->load->model(array('proceso/pyccacom/m_uploadvtex'));
        $this->load->helper(array('form'));
        session_start();
    }

    public function _remap() {
        $datos = $this->input->post();
        $tipo = $this->uri->segment(4);

        validaSession($this->urlControlador);
        //fnc_post($tipo, $datos, $this->urlControlador);

        switch ($tipo) {
            case 'consultaArt':
                $this->_getArticulo();
                break;
            case 'retDArt':
                $this->_retDataArt();
                break;
            case 'saveImgs':
                $this->_grabarImagenes();
                break;
            case 'dFil':
                $this->_eliminarImagen();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {

        $urlControlador = $this->urlControlador;
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'js/plugins/lightbox/css/lightbox.css'
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/config.js',
            'js/general.js',
            'js/jquery-ui.min.js',
            'js/procesos/pyccacom/j_uploadVtex.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js'
        );
        $data['imgs'] = $this->listFilesDirectory();
        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/pyccacom/v_uploadVtex');
        $this->load->view('barras/footer');
    }

    private function _getArticulo() {
        $tipo = $this->input->post('c_t');
        $busqueda = $this->input->post('c_b');
        $data = $this->m_uploadvtex->_getInventar($tipo, $busqueda);
        if (!empty($data['dat'])) {
            echo json_encode($data);
        } else {
            echo json_encode(array(
                "sEcho" => 1,
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            ));
        }
    }

    private function _retDataArt() {
        $co_articulo = $this->input->post('c_art');
        $data = $this->listFilesDirectory('', $co_articulo);
        echo json_encode($data);
    }

    private function listFilesDirectory($directory = '', $co_articulo = '') {
        $array = array('err' => '', 'dat' => '');
        try {
            $connection = ssh2_connect('130.1.80.65', 22);
            ssh2_auth_password($connection, 'root', 'clearlog');
            $sftp = ssh2_sftp($connection);
            $directory = !empty($directory) ? $directory : '';
            $files = scandir('ssh2.sftp://' . $sftp . $this->url_file . $directory);
            //arsort($files);
            if (!empty($files)) {
                foreach ($files as $file) {
                    if ($file != '.' && $file != '..') {
                        $img = pathinfo($this->url_file . $directory . $file);
                        if (isset($img['filename']) && !empty($img['filename'])) {
                            $expArt = trim($img['filename']);
                            $expArt = explode('-', $expArt);
                            if (trim($expArt[0]) == trim($co_articulo)) {
                                $array['dat'] .= "<div class='col-xs-3'>";
                                $array['dat'] .= "<div class='thumbnail'>";
                                $array['dat'] .= "<div class='img-thumbmail'>";
                                $array['dat'] .= "<a href='" . $this->url_pycca . $directory . $file . "' class='fancybox' data-lightbox='example-set' title='" . $file . "'>";
                                $array['dat'] .= "<img class='example-image' src='" . $this->url_pycca . $directory . $file . "' title='" . $file . "'/>";
                                $array['dat'] .= "</a>";
                                $array['dat'] .= "</div>";
                                $array['dat'] .= "<div class='caption text-gris'>";
                                $array['dat'] .= "<p><b>Nombre:</b> " . $file . "</p>";
                                $array['dat'] .= "<p><b>Ruta:</b> images/" . $directory . $file . "</p>";
                                $array['dat'] .= "<div class='text-center' style='margin-top:5px;'><button class='btn btn-danger' onclick='delFil(" . '"' . $file . '"' . ");'><i class='fa fa-close'></i>Eliminar</button></div>";
                                $array['dat'] .= "</div>";
                                $array['dat'] .= "</div>";
                                $array['dat'] .= "</div>";
                            }
                        }
                    }
                }
                if (!empty($array['dat'])) {
                    $array['dat'] .= '<div class="clearfix"></div>';
                }
            }
        } catch (Exception $exc) {
            $array['err'] = $exc->getMessage();
        }
        return $array;
    }

    private function _eliminarImagen() {
        $img = $this->input->post('fl');
        $data = array('err' => '');
        try {
            $connection = ssh2_connect('130.1.80.65', 22);
            ssh2_auth_password($connection, 'root', 'clearlog');
            $sftp = ssh2_sftp($connection);
            ssh2_sftp_unlink($sftp, $this->url_file . $img);
        } catch (Exception $exc) {
            $data['err'] = $exc->getMessage();
        }
        echo json_encode($data);
    }

    private function _grabarImagenes() {
        $co_articulo = $this->input->post('co_art');
        $extensiones_permitidas = array('PNG', 'JPEG', 'JPG', 'GIF');
        $folder = $_SERVER['DOCUMENT_ROOT'] . '/ipycca/img/pyccacom';
        $files = isset($_FILES['fi_up']) ? $_FILES['fi_up'] : null;
        $bit = 1024;
        $bandera = true;
        $arr_imagen = null;
        $co_usuario = $_SESSION['c_e'];
        if (!empty($files['name'])) {
            if (!file_exists($folder)) {
                if (!mkdir($folder, 777)) {
                    $bandera = false;
                } else {
                    chmod($folder, 0777);
                }
            }
            if ($bandera) {
                for ($i = 0; $i < count($files['name']); $i++) {
                    list($ancho, $alto, $type, $attr) = getimagesize($files['tmp_name'][$i]);
                    $name = $co_articulo . "-" . ($i + 1);
                    $type = $files['type'][$i];
                    $size = $files['size'][$i];
                    $error_img = $files['error'][$i];
                    if ($error_img == 0) {
                        $size_mb = (int) round(($size / $bit) / $bit);
                        $ext_ = explode('/', trim($type));
                        $extImagen = trim(strtoupper($ext_[1]));
                        $extImagen == 'JPEG' ? 'JPG' : $extImagen;
                        if (in_array($extImagen, $extensiones_permitidas)) {
                            $arr_imagen[$i] = array(
                                'co_error' => 0,
                                'tx_error' => '',
                                'type' => $extImagen,
                                'name' => $name,
                                'folder' => $folder,
                            );
                            try {
                                if (!move_uploaded_file($files['tmp_name'][$i], $folder . "/" . $name . "." . $extImagen)) {
                                    throw new Exception('ERROR AL SUBIR IMAGEN ' . $name);
                                }
                            } catch (Exception $e) {
                                $arr_imagen[$i]['co_error'] = 1;
                                $arr_imagen[$i]['tx_error'] = $e->getMessage();
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($this->copyDeleteImage($arr_imagen));
    }

    private function copyDeleteImage($array) {
        $data = array('error' => '');
        if (!empty($array)) {
            try {
                $folder = $_SERVER['DOCUMENT_ROOT'] . '/ipycca/img/pyccacom/';
                $connection = ssh2_connect('130.1.80.65', 22);
                ssh2_auth_password($connection, 'root', 'clearlog');
                $sftp = ssh2_sftp($connection);
                foreach ($array as $row) {
                    if ($row['co_error'] == 0) {
                        if (ssh2_scp_send($connection, $folder . $row['name'] . "." . $row['type'], $this->url_file . "/" . $row['name'] . "." . $row['type'], 0644)) {
                            //ssh2_sftp_unlink($sftp, $folder . $row['name']);
                        }
                    }
                }
            } catch (Exception $exc) {
                $data['error'] = $exc->getMessage();
            }
        }
        return $data;
    }

}
