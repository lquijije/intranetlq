<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_upload extends CI_Controller {

    var $urlControlador = 'procesos/pyccacom/co_upload';
    protected $url_file = '/var/www/html/pycca/images/mail/';
    protected $url_pycca = 'http://www.pycca.com/images/mail/';

    function __construct() {
        parent::__construct();
        session_start();
        $this->load->helper(array('form'));
        //$this->load->model(array('proceso/pyccacom/m_upload'));
    }

    public function _remap() {
        $datos = $this->input->post();
        $tipo = $this->uri->segment(4);

        validaSession($this->urlControlador);
        //fnc_post($tipo, $datos, $this->urlControlador);

        switch ($tipo) {
            case 'saveImgs':
                $this->_grabarImagenes();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {

        $urlControlador = $this->urlControlador;
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'js/plugins/lightbox/css/lightbox.css'
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/config.js',
            'js/general.js',
            'js/jquery-ui.min.js',
            'js/procesos/pyccacom/j_upload.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js'
        );
        $data['imgs'] = $this->listFilesDirectory();
        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/pyccacom/v_upload');
        $this->load->view('barras/footer');
    }

    private function listFilesDirectory($directory = '') {
//        error_reporting(E_ALL);
//        ini_set('display_errors', TRUE);
        $array = array('error' => '', 'data' => '');
        set_error_handler(function($errno, $errstr, $errfile, $errline ) {
            throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
        });
        try {
            $connection = ssh2_connect('130.1.80.65', 22);
            ssh2_auth_password($connection, 'root', 'clearlog');
            $sftp = ssh2_sftp($connection);
            $directory = !empty($directory) ? $directory : '';
            $files = scandir('ssh2.sftp://' . $sftp . $this->url_file . $directory);
            //arsort($files);
            if (!empty($files)) {
                foreach ($files as $file) {
                    if ($file != '.' && $file != '..') {
                        $array['data'].= "<div class='col-xs-2'>";
                        $array['data'].= "<div class='thumbnail'>";
                        $array['data'].= "<div class='img-thumbmail'>";
                        $array['data'].= "<a href='" . $this->url_pycca . $directory . $file . "' class='fancybox' data-lightbox='example-set' title='" . $file . "'>";
                        $array['data'].= "<img class='example-image' src='" . $this->url_pycca . $directory . $file . "' title='" . $file . "'/>";
                        $array['data'].= "</a>";
                        $array['data'].= "</div>";
                        $array['data'].= "<div class='caption text-gris'>";
                        $array['data'].= "<p><b>Nombre:</b> " . $file . "</p>";
                        $array['data'].= "<p><b>Ruta:</b> images/mail/" . $directory . $file . "</p>";
                        $array['data'].= "</div>";
                        $array['data'].= "</div>";
                        $array['data'].= "</div>";
                    }
                }
            }
        } catch (Exception $exc) {
            $array['error'] = $exc->getMessage();
        }
        return $array;
    }

    private function _grabarImagenes() {
        $extensiones_permitidas = array('PNG', 'JPEG', 'JPG', 'GIF');
        $folder = $_SERVER['DOCUMENT_ROOT'] . '/ipycca/img/pyccacom';
        $files = isset($_FILES['fi_up']) ? $_FILES['fi_up'] : null;
        $bit = 1024;
        $bandera = true;
        $arr_imagen = null;
        $co_usuario = $_SESSION['c_e'];
        if (!empty($files['name'])) {
            if (!file_exists($folder)) {
                if (!mkdir($folder, 777)) {
                    $bandera = false;
                } else {
                    chmod($folder, 0777);
                }
            }
            if ($bandera) {
                for ($i = 0; $i < count($files['name']); $i++) {
                    list($ancho, $alto, $type, $attr) = getimagesize($files['tmp_name'][$i]);
                    $name = $files['name'][$i];
                    $type = $files['type'][$i];
                    $size = $files['size'][$i];
                    $error_img = $files['error'][$i];
                    if ($error_img == 0) {
                        $size_mb = (int) round(($size / $bit) / $bit);
                        $ext_ = explode('/', trim($type));
                        $extImagen = trim(strtoupper($ext_[1]));
                        if (in_array($extImagen, $extensiones_permitidas)) {
                            $arr_imagen[$i] = array(
                                'co_error' => 0,
                                'tx_error' => '',
                                'type' => $extImagen,
                                'name' => $name,
                                'folder' => $folder,
                            );
                            try {
                                if (!move_uploaded_file($files['tmp_name'][$i], $folder . "/" . $name)) {
                                    throw new Exception('ERROR AL SUBIR IMAGEN ' . $name);
                                }
                            } catch (Exception $e) {
                                $arr_imagen[$i]['co_error'] = 1;
                                $arr_imagen[$i]['tx_error'] = $e->getMessage();
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($this->copyDeleteImage($arr_imagen));
    }

    private function copyDeleteImage($array) {
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        $data = array('error' => '');
        if (!empty($array)) {
            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });
            try {
                $folder = $_SERVER['DOCUMENT_ROOT'] . '/ipycca/img/pyccacom/';
                $connection = ssh2_connect('130.1.80.65', 22);
                ssh2_auth_password($connection, 'root', 'clearlog');
                $sftp = ssh2_sftp($connection);
                foreach ($array as $row) {
                    if ($row['co_error'] == 0) {
                        if (ssh2_scp_send($connection, $folder . $row['name'], $this->url_file . "/" . $row['name'], 0644)) {
                            //ssh2_sftp_unlink($sftp, $folder . $row['name']);
                        }
                    }
                }
            } catch (Exception $exc) {
                $data['error'] = $exc->getMessage();
            }
        }
        return $data;
    }

//ssh2_sftp_mkdir($sftp, $directory);
}
