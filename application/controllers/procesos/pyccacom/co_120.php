<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_120 extends CI_Controller {	

    function __construct()
    {		
        parent::__construct();	
        $this->load->model(array('proceso/pyccacom/m_120'));
        session_start();
    }	

    public function _remap() {

        validaSession('procesos/pyccacom/co_120'); 

        $tipo=$this->uri->segment(4);
        
        if($tipo=='retArt'){
            $this->_retArt();
        } else if ($tipo=='sv_tra'){
            $this->_sv_tra();
        } else if ($tipo=='retPro'){
            $this->_retPro();
        } else {
            $this->_crud(); 
        }
    }        

    private function _crud(){
        
        $file = '';
        //unlink("http://www.localhost/dir/" . $file );
        
        $data=null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/pyccacom/co_120';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array('css/datatables/dataTables.bootstrap.css',
                             'css/datetimepicker/jquery.datetimepicker.css',
                             'js/plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/pyccacom/j_120.js',
                            'js/general.js',
                            'js/plugins/datatables/jquery.dataTables.js',
                            'js/plugins/datatables/dataTables.bootstrap.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/fancyBox/source/jquery.fancybox.js?v=2.1.5');

        $data['idFormulario'] = 'co_120';

        $promos =$this->m_120->retLisPro();
        $data['promos'] = $promos;

        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/pyccacom/v_120');
        $this->load->view('barras/footer');            
    }

    private function _retArt(){
        $cod_art = $this->input->post('c_a');
        $data = $this->m_120->retArt($cod_art);
        echo json_encode($data);
    }
    
    function _nuevorand($lista) {
        srand ((double) microtime( )*1000000);
        do {
           $random_number = rand(10000,70000);
        } while (in_array($random_number,$lista));
        return $random_number;
    }
    
    private function _sv_tra(){
        
        $usados = array();
        $codes = $this->m_120->codeImgs();
        
        if(!empty($codes['data'])){
            foreach ($codes['data'] as $row){
                $usados[] = $row["code"];
            }
        }
        
        $array = $this->input->post();
        $array = json_decode($array['data'],true);
        $valid = 0;
        $error = $nameImgRand = $nameFacRand = '';
        
        $ftp_server = "130.1.1.15";
        $ftp_user_name = "ftprecio";
        $ftp_user_pass = "pr2015";

        $web_dir='../../media/Data/preciorecord/img/articulos/';

        $conn_id = ftp_connect($ftp_server);
        ftp_pasv($conn_id, true); 

        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 

        if ((!$conn_id) || (!$login_result)) { 
            
            $error = "Conexión FTP ha fallado.";
            
        } else {
            
            if(!empty($_FILES)){

                if(isset($_FILES['pro_img']) && isset($_FILES['fac_img'])){         
                    
                    $resultArt = ftp_nlist($conn_id,'../../media/Data/preciorecord/img/');
                    $resultFac = ftp_nlist($conn_id,'../../media/Data/preciorecord/img/articulos/');
                        
                    if (!in_array('../../media/Data/preciorecord/img/articulos',$resultArt)) {  
                        $directory = '../../media/Data/preciorecord/img/articulos';
                        if (!ftp_mkdir($conn_id, $directory)) {
                            $error = "Carpeta no creada: <b>$directory</b>";
                            die;
                        }
                    } 
                    
                    if (!in_array('../../media/Data/preciorecord/img/articulos/face',$resultFac)) {  
                        $directory = '../../media/Data/preciorecord/img/articulos/face';
                        if (!ftp_mkdir($conn_id, $directory)) {
                            $error = "Carpeta no creada: <b>$directory</b>";
                            die;
                        }
                    } 
                    
                    $num_rand = $this->_nuevorand($usados);
                    $nameImg = $_FILES['pro_img']['name'];
                    $tempImg = $_FILES['pro_img']['tmp_name'];
                    $extImg = explode('.', basename($nameImg));
                    $nameImgRand = $num_rand."_img.".$extImg[count($extImg)-1];
                    
                    $web_location=$web_dir.$num_rand."_img.".$extImg[count($extImg)-1]; 
                    $upload = ftp_put($conn_id, $web_location,$tempImg, FTP_BINARY); 

                    if (!$upload) { 
                        $error.= '&bull; '.$_FILES['pro_img']['tmp_name'].' <br>';
                    } else {
                        $valid++;
                    }

                    $nameFac = $_FILES['fac_img']['name'];
                    $tempFac = $_FILES['fac_img']['tmp_name'];
                    $extFac = explode('.', basename($nameFac));
                    $nameFacRand = $num_rand."_fac.".$extFac[count($extFac)-1];
                    
                    $web_location=$web_dir."face/".$num_rand."_fac.".$extFac[count($extFac)-1]; 
                    $upload = ftp_put($conn_id, $web_location,$tempFac, FTP_BINARY); 

                    if (!$upload) { 
                        $error.= '&bull; '.$_FILES['fac_img']['tmp_name'].' <br>';
                    } else {
                        $valid++;
                    }
                }

                ftp_close($conn_id);

                if ($valid == 2){
                    $mensaje = $this->m_120->savePromo($array,$nameImgRand,$nameFacRand);
                    echo json_encode($mensaje);
                } else {
                    echo "Error al subir imagenes <br>".$error;
                }

            }
        }
        
        echo $error;
        
    }
    
    private function _retPro(){
        $cp_pro = $this->input->post('c_p');
        $mensaje = $this->m_120->retPromo($cp_pro);
        echo json_encode($mensaje);
    }
}