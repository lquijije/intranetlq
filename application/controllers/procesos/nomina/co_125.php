<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_125 extends CI_Controller {	
    
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/nomina/m_125','proceso/reclamos/m_111'));
        $this->load->library(array('mailer','Encrypt'));
        session_start();
    }	
    
    public function _remap() {

        validaSession('procesos/nomina/co_125'); 
        
        $tipo = $this->uri->segment(4);
        if($tipo == 'saveTu'){
            $this->_saveTurnos();
        } else if($tipo == 'deleTu'){
            $this->_deleTurnos();
        } else if($tipo == 'updaTu'){
            $this->_updaTurnos();
        } else if($tipo == 'getTu'){
            $this->_getTurnos();
        } else if($tipo == 'ge_pdf'){
            $para1 = $this->uri->segment(5);
            $para2 = $this->uri->segment(6);
            $para3 = $this->uri->segment(7);
            $this->ge_pdf_turnos($para1,$para2,$para3);
        } else if($tipo == 'ge_pdfUlt'){
            $para1 = $this->uri->segment(5);
            $this->ge_pdf_ultimo($para1);
        } else {
            $this->_crud(); 
        }
        
    }      

    private function _crud(){
        
        $bandera = true;
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/nomina/co_125';
        $data['urlControlador'] = $urlControlador;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/nomina/j_125.js',
                            'js/general.js');
        
        $data['idFormulario'] = 'co_125';
        
        $cod_empl = (int)$_SESSION['c_e'];
        $ip = $_SERVER['REMOTE_ADDR'];
        
        $fecActual = $this->m_125->retHoraSql();
        $data['fecActual'] = $fecActual;
        
        $turnos = $this->m_125->retTurnos();
        $data['turnos'] = $turnos;
        
        $fechas = $this->m_125->retFecAutorizada();
        $data['fechas'] = $fechas;
        
        $ccosto = $this->m_125->retCcostoIP($cod_empl,$ip);
        $data['ccosto'] = $ccosto;
        
        empty($ccosto)?$bandera = false:true;
        $data['bandera'] = $bandera;
        
        if (isset($ccosto)){
            $p = $ccosto[0]['co_bdg'];
            $q = $ccosto[0]['co_cco'];
        } else {
            $p = $q = '';
        }
        
        $getCo_alm = $this->input->post('co_bdg');
        $getCo_cco = $this->input->post('co_cco');
        
        $cod_alm = !empty($getCo_alm)?$getCo_alm:$p;
        $cod_cco = !empty($getCo_cco)?$getCo_cco:$q;
        
        $_SESSION['co_alm_125'] = $cod_alm;
        $_SESSION['co_cco_125'] = $cod_cco;
        
        $emple_ccosto = $this->m_125->retUsuariosCcosto($fechas['fe_des'],$fechas['fe_has'],$cod_cco);
        $data['emple_ccosto'] = $emple_ccosto;
        
        $turnosActuales = $this->groupTurnos($emple_ccosto,'co_turn');
        $data['turnosActuales'] = $turnosActuales;
        
        $turnosUltimos = $this->groupTurnos($emple_ccosto,'ul_turn');
        $data['turnosUltimos'] = $turnosUltimos;
        
        $varVer = 0;
        
        for ($i=0;$i < count($emple_ccosto); $i++){
            if($emple_ccosto[$i]['co_turn']> 0)
                $varVer++;
        }
        
        $data['imprimir'] = false;
        
        if(count($emple_ccosto) == $varVer)
                $data['imprimir'] = true;
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/nomina/v_125');
        $this->load->view('barras/footer');            
    }
    
    public function groupTurnos($array,$groupkey) {
        if (count($array)>0)
        {
            $keys = array_keys($array[0]);
            $removekey = array_search($groupkey, $keys);	

            if ($removekey === false){
                return array("Clave \"$groupkey\" no existe");
            } else {
                unset($keys[$removekey]);
            }
            
            $groupcriteria = array();
            $return = array();
            
            foreach($array as $value) {
                
                $item = null;
                foreach ($keys as $key)
                {
                    $item[$key] = $value[$key];
                }
                
//                echo '<pre>';
//                echo $groupkey;
//                    print_r($value[$groupkey]);
//                echo '</pre>';
                
                if($value[$groupkey] > 0){

                    $busca = array_search($value[$groupkey], $groupcriteria);

                    if ($busca === false)
                    {
                       $groupcriteria[] = $value[$groupkey];

                       $return[]=array($groupkey=>$value[$groupkey],'groupeddata'=>array());
                       $busca=count($return)-1;
                    }
                    $return[$busca]['groupeddata'][]=$item;
                }
                
            }
            return $return;
        } else {
            return array();
        }
    }
    
    public function _retIpAlmc(){
        validaSession('procesos/nomina/co_125');
        $cnn = $_SESSION['cod_ip'];
        $cnn = $this->encrypt->decode(trim($cnn));
        return $cnn;
    }
    
    private function _saveTurnos(){
        $datos = $this->input->post();
        $det = json_decode($datos['d_d'],true);
        $array = $this->m_125->saveTurnos($datos,$det);
        echo json_encode(array('data'=>$array));
    }
    
    private function _deleTurnos(){
        $datos = $this->input->post();
        $mensaje = $this->m_125->deleTurnos($datos);
        echo json_encode($mensaje);
    }
    
    private function _updaTurnos(){
        $datos = $this->input->post();
        $mensaje = $this->m_125->updaTurnos($datos);
        echo json_encode($mensaje);
    }
    
    public function _getTurnos(){
        $mensaje = $this->m_125->retTurnos();
        echo json_encode($mensaje);
    }
    
    private function ge_pdf_turnos($fec_ini,$fec_fin,$cod_cco){

        $this->load->library('pdf_ge');
        $pdf = new pdf_ge();
        $pdf->fpdf('P','mm','A4');
        $pdf->setTitulo('PLANIFICACIÓN DE TURNOS');
        
        if(!empty($fec_ini) && !empty($fec_fin) && !empty($cod_cco)){
            
            $datos = $this->m_125->retTurnosConsulta($fec_ini,$fec_fin,$cod_cco);
            
            if(!empty($datos)){
                //$turnosActuales = $this->groupTurnos($datos,'co_turn');
                $turnosActuales = $datos;
                $pdf->setSubTitulo("ALMACÉN ".$turnosActuales[0]['no_ceco']);
                $pdf->setGrupo('co_125');
                $pdf->Campo_1('DESDE: '.date('d', strtotime($fec_ini))."/".conveMes(date('n', strtotime($fec_ini)))."/".date('Y ', strtotime($fec_ini)));
                $pdf->Campo_4('HASTA: '.date('d', strtotime($fec_fin))."/".conveMes(date('n', strtotime($fec_fin)))."/".date('Y ', strtotime($fec_fin)));
                $pdf->AddPage();
                
                $pdf->SetFont('helvetica', '', 7);
               
                for($i=0;$i < count($turnosActuales); $i++){
                    //print_r($turnosActuales);
                    $pdf->Cell(10, 5, $i+1,1, 0,'L');
                    $pdf->Cell(30, 5, $turnosActuales[$i]['co_empl'],1, 0,'L');
                    $pdf->Cell(75, 5, utf8_decode($turnosActuales[$i]['ap_empl']." ".$turnosActuales[$i]['no_empl']),1, 0,'L');
                    $pdf->Cell(75, 5, utf8_decode($turnosActuales[$i]['no_turn']),1, 0,'L');
                    $pdf->Ln(5);   
                }
                
            }
            
        } else {
            $pdf->Ln(5);
            $pdf->SetFont('helvetica', 'B', 15);
            $pdf->Cell(190, 12, utf8_decode("NO HAY DATOS DEL CONTRATO A BUSCAR"),0, 0,'C','true');
            $pdf->Ln(20);
        }
        
        $pdf->Output();

        
    }
    
    private function ge_pdf_ultimo($cod_cco){

        $this->load->library('pdf_ge');
        $pdf = new pdf_ge();
        $pdf->fpdf('P','mm','A4');
        $pdf->setTitulo('PLANIFICACIÓN DE TURNOS');
        
        if(!empty($cod_cco)){
            
            $datos = $this->m_125->retTurnosConsultaUltimo($cod_cco);
            
            if(!empty($datos)){
                $turnosActuales = $datos;
                $pdf->setSubTitulo("ALMACÉN ".$turnosActuales[0]['no_ceco']);
                $pdf->setGrupo('co_125');
                $pdf->Campo_1('DESDE: '.date('d', strtotime($turnosActuales[0]['fe_des']))."/".conveMes(date('n', strtotime($turnosActuales[0]['fe_des'])))."/".date('Y ', strtotime($turnosActuales[0]['fe_des'])));
                $pdf->Campo_4('HASTA: '.date('d', strtotime($turnosActuales[0]['fe_has']))."/".conveMes(date('n', strtotime($turnosActuales[0]['fe_has'])))."/".date('Y ', strtotime($turnosActuales[0]['fe_has'])));
                $pdf->AddPage();
                
                $pdf->SetFont('helvetica', '', 7);
               
                for($i=0;$i < count($turnosActuales); $i++){
                    $pdf->Cell(10, 5, $i+1,1, 0,'L');
                    $pdf->Cell(30, 5, $turnosActuales[$i]['co_empl'],1, 0,'L');
                    $pdf->Cell(75, 5, utf8_decode($turnosActuales[$i]['ap_empl']." ".$turnosActuales[$i]['no_empl']),1, 0,'L');
                    $pdf->Cell(75, 5, utf8_decode($turnosActuales[$i]['no_turn']),1, 0,'L');
                    $pdf->Ln(5);   
                }
                
            }
            
        } else {
            $pdf->Ln(5);
            $pdf->SetFont('helvetica', 'B', 15);
            $pdf->Cell(190, 12, utf8_decode("NO HAY DATOS DEL CONTRATO A BUSCAR"),0, 0,'C','true');
            $pdf->Ln(20);
        }
        
        $pdf->Output();

    }
}