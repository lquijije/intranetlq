<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_133 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('proceso/nomina/m_133'));
        $this->load->helper(array('form'));
        session_start();
    }

    public function _remap() {
        validaSession('procesos/nomina/co_133');
        $tipo = $this->uri->segment(4);
        switch ($tipo) {
            case 'geCr':
                $co_cre = $this->uri->segment(5);
                $this->_retPDFCredencial($co_cre);
                break;
            case 'geCo':
                $this->_retColaborador();
                break;
            case 'geCi':
                $this->_retCiudad();
                break;
            case 'svCr':
                $this->_saveCredencial();
                break;
            case 'geRe':
                $this->_retReporte();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {

        $data = null;
        $urlControlador = 'procesos/nomina/co_133';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array(
            'js/plugins/switchery/dist/switchery.css',
            'js/plugins/tab/tab.css',
            'css/datatables/dataTables.bootstrap.css',
            'css/datetimepicker/jquery.datetimepicker.css'
        );
        $data['js'] = array('js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/plugins/switchery/dist/switchery.js',
            'js/procesos/nomina/j_133.js',
            'js/config.js',
            'js/general.js',
            'js/plugins/tab/tab.js',
            'js/pdfobject.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js',
            'js/plugins/datetimepicker/jquery.datetimepicker.js');

        $ciudades = $this->m_133->retCiudades(0);

        $datosParametro = null;
        if (!empty($ciudades)) {
            foreach ($ciudades as $row) {
                $datosParametro[$row['co_ciu']] = $row['no_ciu'];
            }
        } else {
            $datosParametro['NNN'] = 'Sin Datos';
        }
        $atributosEspecial = "id='ed_ciu' class='form-control'";
        $data['ed_ciu'] = form_dropdown('ed_ciu', $datosParametro, 'NNN', $atributosEspecial);

        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/nomina/v_133');
        $this->load->view('barras/footer');
    }

    private function _retPDFCredencial($co_empl) {

        $array = null;
        $ini_filename = '';
        $w = $y = 0;
        $Px = 9.5;
        $Py = 22.8;

        if (!empty($co_empl)) {

            $array = $this->m_133->retColaboradorCr($co_empl);

            if (!empty($array['res_2'])) {

                $ini_filename = '/media/credenciales/' . trim($co_empl) . '.jpg';

                if (!empty($ini_filename)) {
                    try {

                        $tamaño = getimagesize($ini_filename);
                        $porc = 0;

                        if (isset($tamaño[0]) && isset($tamaño[1])) {
                            if ((int) $tamaño[0] > (int) $tamaño[1]) {
                                $porc = (float) ((int) $tamaño[0] * 100 / 36.8);
                            } else {
                                $porc = (float) ((int) $tamaño[1] * 100 / 36.8);
                            }

                            $w = (int) $tamaño[0] * 100 / $porc;
                            $y = (int) $tamaño[1] * 100 / $porc;

                            if ((int) $tamaño[0] > (int) $tamaño[1]) {
                                $res = ($w - $y) / 2;
                                $Py = $Py + $res;
                            } else {
                                $res = ($y - $w) / 2;
                                $Px = $Px + $res;
                            }
                        }
                    } catch (Exception $e) {
                        
                    }
                }

                $this->load->library('pdf_cr');
//                require_once $_SERVER['DOCUMENT_ROOT'] . '/ipycca/application/libraries/fpdf/makefont/makefont.php'; 
//                MakeFont('/media/Data/produccion/ipycca/fonts/OratorStd.ttf');

                $pdf = new pdf_cr();
                $pdf->AddFont('oratorstd', '', 'oratorstd.php');

                $pdf->fpdf('P', 'mm', 'credencial');
                $pdf->SetMargins(0, 0, 0, 0);
                $pdf->setTitulo('CREDENCIALES');
                $pdf->AddPage();
                $pdf->IncludeJS("print(true);");
                $empresa = 'PYCCA S.A.';

                if ($array['res_1']['co_emp'] == 1 || $array['res_1']['co_emp'] == 10) {
                    $pdf->Image(site_url() . 'img/credencial_1.jpg', 0, 0, 56);
                } else if ($array['res_1']['co_emp'] == 27 || $array['res_1']['co_emp'] == 11) {
                    $pdf->Image(site_url() . 'img/credencial_27.jpg', 0, 0, 56);
                }

                $pdf->Image('/media/credenciales/' . trim($co_empl) . '.jpg', $Px, $Py, $w, $y);
                $pdf->Ln(65);

                if ($array['res_1']['co_emp'] == 1 || $array['res_1']['co_emp'] == 10) {

                    $pdf->SetFont('oratorstd', '', 11);
                    $empresa = 'PLASTICOS INDUSTRIALES';
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->Cell(0, 7, utf8_decode($array['res_2']['ta_nom']), 0, 0, 'C');
                    $pdf->Ln(8);

                    $pdf->SetTextColor(255, 255, 255);
                    $pdf->SetFont('oratorstd', '', 9);
                    $pdf->Cell(0, 7, '', 0, 0, 'C');

                    $pdf->Ln(4);
                    $pdf->Cell(0, 7, "COD: " . trim($co_empl), 0, 0, 'C');
                } else if ($array['res_1']['co_emp'] == 27 || $array['res_1']['co_emp'] == 11) {

                    $pdf->SetFont('helvetica', '', 11);
                    $pdf->SetTextColor(255, 255, 255);
                    $pdf->Cell(0, 7, utf8_decode($array['res_2']['ta_nom']), 0, 0, 'C');

                    $pdf->Ln(8);
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->SetFont('helvetica', '', 9);
                    $pdf->Cell(0, 7, utf8_decode($array['res_2']['ta_car']), 0, 0, 'C');

                    $pdf->Ln(4);
                    $pdf->Cell(0, 7, "COD: " . trim($co_empl), 0, 0, 'C');
                }

                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetMargins(5, 8, 5, 5);
                $pdf->Ln(10);

                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->MultiCell(0, 5, utf8_decode("Esta tarjeta es personal e Intransferible."), 0, 'C');
                $pdf->Ln(5);
                $pdf->MultiCell(0, 5, utf8_decode($empresa . " no se responsabiliza por el mal uso de esta tarjeta."), 0, 'C');
                $pdf->Ln(5);
                $pdf->MultiCell(0, 5, utf8_decode("Si encuentra estar tarjeta, favor devolverla en nuestras oficinas."), 0, 'C');
                $pdf->Ln(5);
                $pdf->MultiCell(0, 5, utf8_decode($array['res_2']['di_ciu'] . " " . $array['res_2']['te_ciu']), 0, 'C');
                $pdf->Output();
            }
        }
    }

    private function _retCiudad() {
        $c_c = $this->input->post('c_c');
        $array = $this->m_133->retCiudades($c_c);
        echo json_encode($array);
    }

    private function _retReporte() {
        $fe_ini = $this->input->post('f_i');
        $fe_fin = $this->input->post('f_f');
        $co_des = $this->input->post('c_d');

        $data = $this->m_133->retReporte($fe_ini, $fe_fin, $co_des);
        $array = array(
            'err' => $data['err'],
            'dat' => '',
            'sum' => '',
        );
        $htmlSum = '';

        if (empty($data['err'])) {
            if (!empty($data['dat'])) {
                $empresas = groupArray($data['dat'], 'co_emp');
                foreach ($empresas as $e) {
                    $suma = 0;
                    $htmlSum .= '<tr><td><b><h3 class="text-right">' . $e['groupeddata'][0]['no_emp'] . '</h3></b></td>';
                    if (!empty($e['groupeddata'])) {
                        foreach ($e['groupeddata'] as $g) {
                            $suma += $g ['va_mon'];
                        }
                    }
                    $htmlSum .= '<td><b><h3 class="text-right">$' . number_format($suma, 2, '.', ',') . '</h3></b></td></tr>';
                }

                $array['sum'] = '<table class="table"><tbody>' . $htmlSum . '</tbody></table>';

                $datatable_columns = array(
                    array('title' => 'Empresa', 'data' => 'no_emp'),
                    array('title' => 'C&oacute;digo', 'data' => 'co_usu'),
                    array('title' => 'Colaborador', 'data' => 'no_usu'),
                    array('title' => 'Fecha Emisi&oacute;n', 'data' => 'fe_cre', 'className' => "text-center"),
                    array('title' => 'Fecha Eliminado', 'data' => 'fe_eli', 'className' => "text-center"),
                    array('title' => 'Valor a descontar', 'data' => 'va_mon', 'className' => "text-right")
                );
                $array['dat'] = array(
                    'data' => $data['dat'],
                    'columns' => $datatable_columns
                );
            }
        }

        echo json_encode($array);
    }

    private function _saveCredencial() {
        $data = $this->input->post('c_a');
        $array = $this->m_133->grabarCredencial($data);
        echo json_encode($array);
    }

    private function _retColaborador() {
        $c_e = $this->input->post('c_e');
        $array = $this->m_133->retColaboradorCr($c_e);

        if (!empty($array['res_1'])) {

            $w = $y = 0;

            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });

            try {
                $ini_filename = '/media/credenciales/' . $c_e . '.jpg';
                $tamaño = getimagesize($ini_filename);
                $porc = 0;
                if (isset($tamaño[0]) && isset($tamaño[1])) {
                    if ((int) $tamaño[0] > (int) $tamaño[1]) {
                        $porc = (float) ((int) $tamaño[0] * 100 / 192);
                    } else {
                        $porc = (float) ((int) $tamaño[1] * 100 / 192);
                    }

                    $w = (int) $tamaño[0] * 100 / $porc;
                    $y = (int) $tamaño[1] * 100 / $porc;
                }
            } catch (Exception $e) {
                $array['err'] = $e;
            }

            $array['res_1']['x'] = $w;
            $array['res_1']['y'] = $y;
        }

        echo json_encode($array);
    }

}
