<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_126 extends CI_Controller {	
    
    function __construct(){		
        parent::__construct();	
        $this->load->model(array('proceso/nomina/m_126','proceso/reclamos/m_111','ccosto'));
        $this->load->library(array('mailer'));
        session_start();
    }	
    
    public function _remap() {

        validaSession('procesos/nomina/co_126'); 
        
        $tipo = $this->uri->segment(4);
        if($tipo == 'saveTu'){
            $this->_saveTurnos();
        } else if($tipo == 'procTu'){
            $this->_procTurnos();
        } else if($tipo == 'deleTu'){
            $this->_deleTurnos();
        } else if($tipo == 'getEmplBg'){
            $this->_getTurnosEmpleados();
        } else if($tipo == 'getTu'){
            $this->_getTurnos();
        } else if($tipo=='ge_pdf'){
            $para1 = $this->uri->segment(5);
            $para2 = $this->uri->segment(6);
            $this->ge_pdf_turnos($para1,$para2);
        } else {
            $this->_crud(); 
        }
        
    }

    private function _getTurnos(){
        $mensaje = $this->m_126->retTurnos();
        echo json_encode($mensaje);
    }
    private function _crud(){
        
        $bandera = true;
        $data = null; 
        $data['permisos']='';   
        $urlControlador = 'procesos/nomina/co_126';
        $data['urlControlador'] = $urlControlador;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/procesos/nomina/j_126.js',
                            'js/general.js');
        
        $data['idFormulario'] = 'co_126';
        
        $cod_empl = (int)$_SESSION['c_e'];
        
        $fechas = $this->m_126->retFecAutorizada();
        
        if(!empty($fechas)){
            $p = $fechas[0]['fe_des']."|".$fechas[0]['fe_has'];
        }
        
        $getCmb = $this->input->post('cmb_fec');
        $cod_cmb = !empty($getCmb)?$getCmb:$p;
        
        $datosParametro = null;
        if (!empty($fechas)){    
            foreach ($fechas as $row){    
                $datosParametro[$row['fe_des']."|".$row['fe_has']] = "Desde el ".date('d',strtotime($row['fe_des']))."/".conveMes(date('m',strtotime($row['fe_des'])))."/".date('Y',strtotime($row['fe_des']))." Hasta el ".date('d',strtotime($row['fe_has']))."/".conveMes(date('m',strtotime($row['fe_has'])))."/".date('Y',strtotime($row['fe_has']));
            }
        } else {
            $datosParametro['NNN'] = 'No Existen Fecha';    
        }
        $atributosEspecial = "id='cmb_fec' class='form-control' onchange='this.form.submit()'";
        $data['cmb_fec'] = form_dropdown('cmb_fec',$datosParametro,$getCmb,$atributosEspecial);
        
        $cod_cmb = explode('|',$cod_cmb);
        
        $ccosto = $this->m_126->retCcosto($cod_empl,$cod_cmb[0],$cod_cmb[1]);
        $data['ccosto'] = $ccosto;
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('procesos/nomina/v_126');
        $this->load->view('barras/footer');            
    }
    private function _saveTurnos(){
        $datos = $this->input->post();
        $det = json_decode($datos['d_d'],true);
        $array = $this->m_126->saveTurnos($datos,$det);
        echo json_encode(array('data'=>$array));
    }
    private function _deleTurnos(){
        $datos = $this->input->post();
        $mensaje = $this->m_126->deleTurnos($datos);
        echo json_encode($mensaje);
    }
    
    private function _getTurnosEmpleados(){
        $f_i = $this->input->post('f_i');
        $f_f = $this->input->post('f_f');
        $c_c = $this->input->post('c_c');
        $mensaje = $this->m_126->retUsuariosCcosto($f_i,$f_f,$c_c);
        echo json_encode($mensaje);
    }
    
    private function _procTurnos(){
        
        $datos = $this->input->post();
        $array = $this->m_126->procTurnos($datos);
        if((int)$datos['m_l'] == 1 && $array['co_err'] == 0){
            $response = $this->mailProcesados($datos['f_i'],$datos['f_f']);
            if (trim($response) <> 'OK'){
                $array['co_err'] = 1;
                $array['tx_err'] = 'Datos procesados con exito, error al envio de correo '.$response;
            }
        }
        
        echo json_encode(array('data'=>$array));
        
    }
    
    private function ge_pdf_pro($fec_ini,$fec_fin){
        
        $datos = $this->m_126->retCecoProce($fec_ini,$fec_fin);
        
        $attachment = '';
        
        if (isset($datos['data']) && !empty($datos['data'])){
            
            $this->load->library('pdf_ge');
            $pdf = new pdf_ge();
            $pdf->fpdf('P','mm','A4');
            $pdf->setTitulo('PLANIFICACIÓN DE TURNOS INTRANET');
            $pdf->setSubTitulo('ALMACENES PROCESADOS');
            $pdf->setGrupo('co_126');
            $pdf->Campo_1('DESDE: '.date('d', strtotime($fec_ini))."/".conveMes(date('n', strtotime($fec_ini)))."/".date('Y ', strtotime($fec_ini)));
            $pdf->Campo_4('HASTA: '.date('d', strtotime($fec_fin))."/".conveMes(date('n', strtotime($fec_fin)))."/".date('Y ', strtotime($fec_fin)));
            $pdf->AddPage();

            $pdf->SetFont('helvetica', '', 7);

            for($i=0;$i < count($datos['data']); $i++){
                $pdf->Cell(10, 5, $i+1,1, 0,'L');
                $pdf->Cell(40, 5, utf8_decode($datos['data'][$i]['no_ceco']),1, 0,'L');
                $pdf->Cell(20, 5, $datos['data'][$i]['co_empl'],1, 0,'R');
                $pdf->Cell(65, 5, utf8_decode($datos['data'][$i]['ap_empl']." ".$datos['data'][$i]['no_empl']),1, 0,'L');
                $pdf->Cell(55, 5, utf8_decode($datos['data'][$i]['no_turn']),1, 0,'L');
                $pdf->Ln(5);   
            }
            
            //$pdf->Output();
            $attachment = $pdf->Output('TURNOS_PROCESADOS_'.date('Ymd').'.pdf', 'S');
        }
        return $attachment;
        
    }
    
    private function mailProcesados($fec_ini,$fec_fin){
        
        $adjunt = $this->ge_pdf_pro($fec_ini,$fec_fin);
        
        $host = $this->config->item('host_mail');
        $puerto = $this->config->item('port_mail');	
        $usuario = $this->config->item('user_mail');		
        $clave = $this->config->item('pass_mail');	
        
	$responder_a = $usuario;
        
        if(!empty($adjunt)){
            
            $asunto = "TURNOS PROCESADOS";
            $cuerpo = '<table cellpadding="0" cellspacing="0" style="width:100%;border-top:5px solid #F0514A;padding:30px 0;">
                <tbody>
                    <tr>
                        <td>
                            <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="ecxlayout">
                                <tbody>
                                    <tr>
                                        <td height="10">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="20">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="middle">
                                            <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="ecxcontainer">
                                                <tbody>
                                                    <tr>
                                                        <td class="ecxaction">
                                                            <table width="600" border="0" cellspacing="0" cellpadding="0" class="ecxaction_text" align="left">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="font-size:18px;color:#444 !important;line-height:25px;font-weight:lighter;">
                                                                        <p>Adjunto archivo PDF con los turnos procesados desde el '.$fec_ini.' hasta el '.$fec_fin.'. </p>
                                                                        <p>Saludos cordiales.</p> 
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="50" class="ecxspace">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#323f4e">
                                <tbody>
                                    <tr>
                                        <td valign="middle" style="border-top:5px solid #F0514A;background-color:#00539F;">
                                            <table border="0" cellspacing="0" cellpadding="0" align="center" class="footer">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="250" border="0" cellspacing="0" cellpadding="0" align="left" class="ecxcopyright">
                                                                <tbody>
                                                                    <tr>
                                                                        <td height="40" valign="middle" style="font-size:14px;color:#FFF;">&copy; 2015 PYCCA S.A.</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>';

            $cabecera= "MIME-Version: 1.0\r\n";
            $cabecera .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
            $cabecera .= "From: PYCCA S.A.\r\n";
            
            try {
                $mail = new PHPMailer();
                $mail->IsSMTP(); 
                $mail->PluginDir = "";
                $mail->Host = $host;
                $mail->Port = $puerto;
                $mail->SMTPAuth = true;
                $mail->Username = $usuario;
                $mail->Password = $clave;
                $mail->From = $responder_a;
                $mail->FromName = "PYCCA S.A.";
                $mail->WordWrap = 50;
                $mail->Subject = $asunto;

                $mail->AddAddress('jmora@pycca.com');
                $mail->AddBCC('fvera@pycca.com');
                $mail->AddBCC('gjalil@pycca.com');
                $mail->Body = $cuerpo;

                $mail->AddStringAttachment($adjunt,'TURNOS_PROCESADOS_'.date('Ymd').'.pdf');
                $mail->IsHTML(true);
                $mail->Send();
                
            } catch (Exception $e){
                return "ERROR: " . $e->getMessage(); 	
            }
            
            return "OK";	
        }
    }
    
    private function ge_pdf_turnos($fec_ini,$fec_fin){

        $this->load->library('pdf_ge');
        $pdf = new pdf_ge();
        $pdf->fpdf('P','mm','A4');
        $pdf->setTitulo('PLANIFICACIÓN DE TURNOS INTRANET');

        if(!empty($fec_ini) && !empty($fec_fin)){

            $datos = $this->m_126->retCecoProce($fec_ini,$fec_fin);

            if (isset($datos['data']) && !empty($datos['data'])){

                $this->load->library('pdf_ge');
                $pdf = new pdf_ge();
                $pdf->fpdf('P','mm','A4');
                $pdf->setTitulo('PLANIFICACIÓN DE TURNOS INTRANET');
                $pdf->setSubTitulo('ALMACENES PROCESADOS');
                $pdf->setGrupo('co_126');
                $pdf->Campo_1('DESDE: '.date('d', strtotime($fec_ini))."/".conveMes(date('n', strtotime($fec_ini)))."/".date('Y ', strtotime($fec_ini)));
                $pdf->Campo_4('HASTA: '.date('d', strtotime($fec_fin))."/".conveMes(date('n', strtotime($fec_fin)))."/".date('Y ', strtotime($fec_fin)));
                $pdf->AddPage();

                $pdf->SetFont('helvetica', '', 7);

                for($i=0;$i < count($datos['data']); $i++){
                    $pdf->Cell(10, 5, $i+1,1, 0,'L');
                    $pdf->Cell(40, 5, utf8_decode($datos['data'][$i]['no_ceco']),1, 0,'L');
                    $pdf->Cell(20, 5, $datos['data'][$i]['co_empl'],1, 0,'R');
                    $pdf->Cell(65, 5, utf8_decode($datos['data'][$i]['ap_empl']." ".$datos['data'][$i]['no_empl']),1, 0,'L');
                    $pdf->Cell(55, 5, utf8_decode($datos['data'][$i]['no_turn']),1, 0,'L');
                    $pdf->Ln(5);   
                }
            }
            
        } else {
            $pdf->Ln(5);
            $pdf->SetFont('helvetica', 'B', 15);
            $pdf->Cell(190, 12, utf8_decode("NO HAY DATOS EN LA BUSQUEDA REALIZADA"),0, 0,'C','true');
            $pdf->Ln(20);
        }

        $pdf->Output();

    }
    
}