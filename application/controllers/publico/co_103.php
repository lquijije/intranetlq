<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_103 extends CI_Controller {	
	
	function __construct()
	{		
		parent::__construct();	
                $this->load->model(array('publico/m_103'));
                session_start();
        }	
        
        public function _remap() {
            
            $tipo=$this->uri->segment(3);
            
            if($tipo=='retPersonas'){
                $this->_retPersonas();
            } else {
                $this->_crud(); 
            }
        }        
        
        private function _crud(){
            $data=null; 
            $data['permisos']='';   
            $urlControlador = 'publico/co_103';
            $data['urlControlador'] = $urlControlador;
            
            $data['css'] = array('css/timeline/timeline.css','css/fonts.css');
            
            $data['js']=array('js/jquery-1.10.2.min.js',
                              'js/general.js',
                              'js/bootstrap.min.js');
            
            $data['idFormulario'] = 'co_103';
            
            $data['comedor'] = $this->m_103->getComedor();
            
            $this->load->view('barras/header',$data);
            $this->load->view('barras/menu');
            $this->load->view('publico/v_103');
            $this->load->view('barras/footer');            
        }
        
        private function _retPersonas(){
            $datos =$this->input->post();
            $personas =$this->m_103->getPersonas($datos);
            echo ($personas);
        }

}