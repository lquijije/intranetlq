<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_102 extends CI_Controller {	
	
    function __construct()
    {		
            parent::__construct();	
            $this->load->model(array('publico/m_102'));
            session_start();
    }	

    public function _remap() {

        $tipo=$this->uri->segment(3);

        if($tipo=='retPersonas'){
            $this->_retPersonas();
        } else {
            $this->_crud(); 
        }
    }        

    private function _crud(){
        $data=null; 
        $data['permisos']='';   
        $urlControlador = 'publico/co_102';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array('css/timeline/timeline_1.css');

        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/general.js',
                            'js/bootstrap.min.js');

        $data['idFormulario'] = 'co_102';

        $data['cumpleanios'] =$this->m_102->getCumpleaños();

        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('publico/v_102');
        $this->load->view('barras/footer');            
    }

       

}