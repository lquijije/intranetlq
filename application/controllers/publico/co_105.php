<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_105 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation'));
        $this->load->model(array('publico/m_105'));
        session_start();
    }

    public function _remap() {

        $tipo = $this->uri->segment(3);

        if ($tipo == 'getInventar') {
            $this->_getInventar();
        } else if ($tipo == 'retDataArt') {
            $this->_retDataArt();
        } else {
            $this->_crud();
        }
    }

    private function _crud() {

        $data = null;
        $data['permisos'] = '';
        $urlControlador = 'publico/co_105';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array('css/datatables/dataTables.bootstrap.css');

        $data['js'] = array('js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/config.js',
            'js/general.js',
            'js/publico/js_105.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js');

        $this->load->view('barras/header', $data);
        $this->load->view('publico/v_105');
        $this->load->view('barras/footer');
    }

    private function _getInventar() {
        $datos = $this->input->post();
        $mensaje = $this->m_105->_getInventar($datos);
        echo $mensaje;
    }

    private function _retDataArt() {
        $co_articulo = $this->input->post('i_art');
        $data = $this->m_105->consultaArticulo($co_articulo);
        $array = array('err' => '', 'dat' => '');
        if (empty($data['err'])) {
            $html = '<section>';
            
            $html .= '<ul class="nav nav-tabs">';
            $html .= '<li class="active"><a href="#tab1" data-toggle="tab">ART&Iacute;CULO</a></li>';
            $html .= '</ul>';

            $html .= '<div class="tab-content">';
            $html .= '<div class="tab-pane active" id="tab1">';
            
            $html .= '<div class="col-xs-4" style="margin-bottom: 5px;padding-bottom: 10px;">';
            $html .= '<div style="position:relative;">';
            if ($data['inf']['oferta'] == 'S') {
                $resultD = 100 - (($data['inf']['pvp_ofe'] * 100) / $data['inf']['pvp']);
                $html .= '<div class="oferta">' . number_format($resultD, 0) . '%</div>';
            }
            $html .= '</div>';
            $html .= '<div>';
            
            $html .= '<img style="max-width: 100% ! important;height: 250px;" src="' . site_url('lib/loadImg.php?codigo=' . $co_articulo) . '"></div></div><div class="col-xs-8" style="margin-bottom: 5px;padding-bottom: 10px;"><div class="spa-12"><h5><b>Ruta: </b>' . $data['inf']['des_gru'] . '/' . $data['inf']['des_lin'] . '/' . $data['inf']['des_sub'] . '/' . $data['inf']['des_cat'] . '</h5><h5><b>Fabrica: </b>' . $data['inf']['cod_fab'] . '</h5><h5><b>Precio: </b>';
            if ($data['inf']['oferta'] == 'S') {
                $html .= '<span style="text-decoration: line-through; color: grey;">$' . number_format($data['inf']['pvp'], 2, '.', ',') . '</span> <span style="color:red;">$' . number_format($data['inf']['pvp_ofe'], 2, ".", ",") . '</span>, <span>oferta desde ' . $data['inf']['desde'] . ' hasta ' . $data['inf']['hasta'] . '</span>';
            } else {
                $html .= '<span>$' . $data['inf']['pvp'] . '</span>';
            }
            $html .= '</div>';
            
            $html .= '<div class="clearfix">';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="col-xs-12">';
            $html .= '<div>';
            
            $html .= '<div class="shadow-z-1" style="margin-bottom: 20px;">';
            $html .= '<table id="detArt" class="table table-bordered table-striped table-mc-teal">';
            $html .= '<thead>';
            $html .= '<th>Bodega</th>';
            $html .= '<th width="10%">Saldo</th>';
            $html .= '<th width="10%">Reser.</th>';
            $html .= '</thead>';
            $html .= '<tbody>';
            if (!empty($data['sal'])) {
                foreach ($data['sal'] as $val) {
                    $html .= '<tr>';
                    $html .= '<td>' . $val['descripcion'] . '</td>';
                    $html .= '<td>' . $val['saldo'] . '</td>';
                    $html .= '<td>' . $val['reservado'] . '</td>';
                    $html .= '</tr>';
                }
            }
            $html .= '<tbody>';
            $html .= '</table>';
            $html .= '</div>';
            
            $html .= '</div></section>';
            $array['dat'] = $html;
        } else {
            $array['err'] = $data['err'];
        }
        echo json_encode($array);
    }

}
