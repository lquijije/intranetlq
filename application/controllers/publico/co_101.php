<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_101 extends CI_Controller {	
	
	function __construct()
	{		
		parent::__construct();	
                $this->load->model(array('publico/m_101'));
                session_start();
        }	
        
        public function _remap() {
            
            $tipo=$this->uri->segment(3);
            
            if($tipo=='retPersonas'){
                $this->_retPersonas();
            } else {
                $this->_crud(); 
            }
        }        
        
        private function _crud(){
            $data=null; 
            $data['permisos']='';   
            $urlControlador = 'publico/co_101';
            $data['urlControlador'] = $urlControlador;
            
            $data['css'] = array('css/datatables/dataTables.bootstrap.css');
            
            $data['js']=array('js/jquery-1.10.2.min.js',
                              'js/general.js',
                              'js/publico/js_101.js',
                              'js/plugins/datatables/jquery.dataTables.js',
                              'js/plugins/datatables/dataTables.bootstrap.js',
                              'js/bootstrap.min.js');
            
            $data['idFormulario'] = 'co_101';
            
            $this->load->view('barras/header',$data);
            $this->load->view('barras/menu');
            $this->load->view('publico/v_101');
            $this->load->view('barras/footer');            
        }
        
        private function _retPersonas(){
            $datos =$this->input->post();
            $personas =$this->m_101->getPersonas($datos);
            echo ($personas);
        }

}