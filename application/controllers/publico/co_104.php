<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class co_104 extends CI_Controller {	
	
	function __construct()
	{		
		parent::__construct();	
                $this->load->model(array('publico/m_104'));
                session_start();
        }	
        
        public function _remap($method) {
            
            $tipo=$this->uri->segment(3);
            
            if($tipo=='retAutos'){
                $this->_retAutos();
            } else {
                $this->_crud(); 
            }
        }        
        
        private function _crud(){
            $data=null; 
            $data['permisos']='';   
            $urlControlador = 'publico/co_104';
            $data['urlControlador'] = $urlControlador;
            
            $data['css'] = array('css/datatables/dataTables.bootstrap.css');
            
            $data['js'] = array('js/jquery-1.10.2.min.js',
                                'js/general.js',
                                'js/publico/js_104.js',
                                'js/plugins/datatables/jquery.dataTables.js',
                                'js/plugins/datatables/dataTables.bootstrap.js',
                                'js/bootstrap.min.js');
            
            $data['idFormulario'] = 'co_104';
            
            $this->load->view('barras/header',$data);
            $this->load->view('barras/menu');
            $this->load->view('publico/v_104');
            $this->load->view('barras/footer');            
        }
        
        private function _retAutos(){
            $tipo =$this->input->post('t_p');
            $buscar =$this->input->post('b_r');
            $Autos =$this->m_104->getAutos($tipo,$buscar);
            echo ($Autos);
        }

}