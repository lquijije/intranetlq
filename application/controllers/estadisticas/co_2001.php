<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_2001 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->library(array('mailer'));
        $this->load->model(array('estadisticas/m_2001'));
        session_start();
    }	

    public function _remap()  {    
        
        $tipo=$this->uri->segment(4);
        validaSession('estadisticas/co_2001');  
        
        //$datos = $this->input->post();
        //fnc_post($tipo,$datos,'estadisticas/co_2001');

        if ($tipo == 'reSol'){
            $this->_retSolicitudes();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud($array = null){
        
        $data = null; 
        $urlControlador = 'estadisticas/co_2001';
        $data['urlControlador'] = $urlControlador;
        $data['array'] = $array;
        
        $data['reem'] ='';
        $data['empr'] = true;
        
        $data['css'] = array('css/weather-icons.css',
                            'css/weather-icons-wind.min.css',
                            'css/datetimepicker/jquery.datetimepicker.css');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.js',
                            'js/general.js',
                            'js/estadisticas/j_2001.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js');
        
        $fec_ini = date('Y-m-d');
        $monto = 'VENTA';
                
        $datF = $this->input->post('txt_fec');
        $datM = $this->input->post('cmb_mon');
        
        if(!empty($datF) && !empty($datM)){
            $fec_ini = $datF;
            $monto = $datM;
        }
        
        $datosParametro = array(
            "VENTA"=>"VENTA TOTAL",
            "VtaTPYCCA"=>"CR&Eacute;DITO PERSONAL",
            "CobTPYCCA"=>"COBROS",
            "Empresarial"=>"EMPRESARIAL",
            "Supercredito"=>"SUPER CR&Eacute;DITO",
        );
        
        $atributosEspecial = "id='cmb_mon' class='form-control'";
        $data['cmb_mon'] = form_dropdown('cmb_mon',$datosParametro,$monto,$atributosEspecial);
        
        $data['fec'] = $fec_ini;
        $data['mon'] = $monto;
        
        $data['dataVtaDia'] = $this->m_2001->retVentasPorDia($fec_ini,$monto);
        $data['dataVtaGru'] = $this->m_2001->retVentasPorGrupo($fec_ini);
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('estadisticas/v_2001');
        $this->load->view('barras/footer');
    }

    private function _retSolicitudes(){
        $cod_empr = $this->input->post('c_e');
        $solicitudes = $this->m_2001->retSolicitudesPrestamos($cod_empr); 
        if (!empty($solicitudes)){
            echo json_encode($solicitudes);
        } else {
            echo json_encode(array(
                "sEcho" => 1,
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            ));
        }
    }

    private function _retSolicitud(){
        
        $cod_empr = $this->input->post('c_e');
        $cod_empr = $cod_empr == 1 ?'XYZ':'PYCCA';
        $cod_pre = $this->input->post('c_p');
        $solicitudes = $this->m_2001->retSolicitud($cod_empr,$cod_pre);
        $novedades = $this->m_2001->retSolicitudNovedades($cod_pre);
        
        $string = $title = '';
        
        if(empty($solicitudes['err'])){
            
            if(!empty($solicitudes['data'])){
                $title = 'Solicitud # '.$solicitudes['data']['a']." (".$solicitudes['data']['j'].") Empresa: ".$solicitudes['data']['c'];
                $string ='<div style="margin-top:15px">';
                $string.='<div class="col-md-6">';
                    $string.='<table class="table table-bordered">';
                        $string.='<tbody>';
                            $string.='<tr><td class="topTr" colspan="2">DATOS DEL PRESTAMO</td></tr>';
                            $string.='<tr>';
                                $string.='<td width="40%"><b>Motivo</b></td>';
                                $string.='<td>'.$solicitudes['data']['v'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Descripción</b></td>';
                                $string.='<td>'.$solicitudes['data']['h'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Fecha de Inicio</b></td>';
                                $string.='<td>'.$solicitudes['data']['s'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Monto</b></td>';
                                $string.='<td>$'.$solicitudes['data']['l'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Número de Cuotas</b></td>';
                                $string.='<td>'.$solicitudes['data']['t']." ".$solicitudes['data']['a6'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Valor Cuota</b></td>';
                                $string.='<td>$'.$solicitudes['data']['n'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Tasa de interes</b></td>';
                                $string.='<td>'.$solicitudes['data']['u'].'%</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Ingresado por</b></td>';
                                $string.='<td>'.$solicitudes['data']['a5'].'</td>';
                            $string.='</tr>';
                        $string.='</tbody>';
                    $string.='</table>';
                $string.='</div>';
                
                $string.='<div class="col-md-6">';
                    $string.='<table class="table table-bordered">';
                        $string.='<tbody>';
                            $string.='<tr><td class="topTr" colspan="2">DATOS DEL DEUDOR</td></tr>';
                            $string.='<tr>';
                                $string.='<td width="40%"><b>Cédula</b></td>';
                                $string.='<td>'.$solicitudes['data']['e'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Código</b></td>';
                                $string.='<td>'.$solicitudes['data']['g'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Nombre</b></td>';
                                $string.='<td>'.$solicitudes['data']['f'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Fecha de Ingreso</b></td>';
                                $string.='<td>'.$solicitudes['data']['k'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Deuda vigente</b></td>';
                                $string.='<td>$'.$solicitudes['data']['o'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Sueldo actual</b></td>';
                                $string.='<td>$'.$solicitudes['data']['p'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Prom. Total a recibir</b></td>';
                                $string.='<td>$'.$solicitudes['data']['q'].'</td>';
                            $string.='</tr>';
                            $string.='<tr>';
                                $string.='<td><b>Prom. Cupo disponible</b></td>';
                                $string.='<td>$'.$solicitudes['data']['r'].'</td>';
                            $string.='</tr>';
                        $string.='</tbody>';
                    $string.='</table>';
                $string.='</div>';
                
                $string.='<div class="col-md-6">';
                    $string.='<table class="table table-bordered">';
                        $string.='<tbody>';
                            $string.='<tr><td class="topTr">NOVEDADES</td></tr>';
                            if(empty($novedades['err'])){
                                if(!empty($novedades['data'])){
                                    foreach ($novedades['data'] as $row){
                                        $string.='<tr>';
                                            $string.='<td>'.$row['desc_nov'].'</td>';
                                        $string.='</tr>';
                                    }
                                }
                            } 
                        $string.='</tbody>';
                    $string.='</table>';
                $string.='</div>';
                
                $string.='<div class="col-md-6">';
                    $string.='<table class="table table-bordered">';
                        $string.='<tbody>';
                            $string.='<tr><td class="topTr">COMENTARIOS DEPARTAMENTALES</td></tr>';
                            if(!empty($solicitudes['data']['w'])){
                                $string.='<tr>';
                                    $string.='<td><b>NIVEL 1:</b> '.$solicitudes['data']['w']." - ".$solicitudes['data']['x'].'</td>';
                                $string.='</tr>';
                            }
                            if(!empty($solicitudes['data']['y'])){
                                $string.='<tr>';
                                    $string.='<td><b>NIVEL 2:</b> '.$solicitudes['data']['y']." - ".$solicitudes['data']['z'].'</td>';
                                $string.='</tr>';
                            }
                            if(!empty($solicitudes['data']['a1'])){
                                $string.='<tr>';
                                    $string.='<td><b>NIVEL 3:</b> '.$solicitudes['data']['a1']." - ".$solicitudes['data']['a2'].'</td>';
                                $string.='</tr>';
                            }
                        $string.='</tbody>';
                    $string.='</table>';
                $string.='</div>';
                $string.='<div class="col-md-6"></div>';
                $string.='<div class="col-md-6">';
                    $string.='<label><b>Comentario (opcional)<b></label>';
                    $string.='<textarea class="form-control" id="tx_obs" name="tx_obs" style="margin-bottom:10px;"></textarea>';
                    $string.='<button type="button" class="btn btn-danger pull-right" onclick="asigJsonSoli('."'".$solicitudes['data']['a']."'".','."'N'".')" style="margin-left:10px;">Negar</button>';
                    $string.='<button type="button" class="btn btn-success pull-right" onclick="asigJsonSoli('."'".$solicitudes['data']['a']."'".','."'A'".')">Aprobar</button>';
                $string.='</div>';
                
                $string.='</div>';
                
            }

        }
        
        echo json_encode(array('data'=>$string,'tite'=>$title,'err'=>$solicitudes['err']));
        
    }
    
    private function _aprobacionSolicitudes(){
        
        $data = $this->input->post();
        $obs = $data['o_s'];
        $cod_empr = $data['c_e'];
        $data = json_decode($data['s_s'],true);
        $bandera = 0;
        $men = $menOK = $menER = $valida = '';
        $arr = null;
        
        $arr[] = array(
           'name'=>'adelacruz',
//           'mail'=>'adelacruz@pycca.com',
           'mail'=>'fvera@pycca.com'
        );
        
        if(!empty($data)){
        
            foreach ($data as $row) {

                $res = $this->m_2001->saveAprobacion($cod_empr,(int)$row['c_s'],$row['e_s'],$obs);

                if (empty($res['err'])){

                    if($res['data']['co_err'] == 0){

                        $estado = trim($row['e_s']) == 'A' ? 'APROBADO':'NEGADO';
                        $men = trim($row['e_s']) == 'A' ? ' Favor ingrese al Sistema de Préstamos para procesarla.':'.';
                        $mensaje = "Este mail es para notificarle que se ha ".$estado." la solicitud de préstamo No. ".(int)$row['c_s'].$men;
                        $title = 'Solicitud de Prestamo '.$estado;
                        $array = templ_mail($title,$mensaje,'Solicitud de Prestamo','jfayad@pycca.com',$arr);
                        $menOK.= $row['c_s']."<br>";

                    } else {
                        $menER.= $row['c_s']." - (".trim($res['data']['tx_err']).").<br>";
                    }

                } else {
                    $bandera = 1;
                    $valida = trim($res['err']);
                    break;
                }

            }

            if(!empty($menOK)){
                $valida.= 'Las siguientes solicitudes han sido actualizadas con exito.<br>'.$menOK;
            }
            if(!empty($menER)){
                $valida.= 'Las siguientes solicitudes han presentado ERROR.<br>'.$menER;
            } 

        } else {
            $bandera = 1;
            $valida = 'Debe seleccionar las solicitudes';
        }
        
        echo json_encode(array(
            'co_err'=>$bandera,
            'tx_err'=>$valida
        ));
        
    }
    
}