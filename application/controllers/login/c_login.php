<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class c_login extends CI_Controller {
    //$dblxmaster = $CI->load->database('db_lx_master', true);
    function __construct() {
        parent::__construct();
        $this->load->library(array('Encrypt'));
        $this->load->model(array('autocomplete_model'));
        session_start();
        
    }

    public function _remap() {
        $method = $this->uri->segment(3);
        if ($method == 'cerrar_session') {
            $this->_cerrar_session();
        } else {
            $this->inicio();
        }
    }

    private function inicio() {

        //$web = get_browser(null, true);

        //if (trim($web['browser']) === 'IE' && (int) $web['version'] <= 8) {
        //    redirect(site_url() . 'general/out_browser');
        //}

        if (isset($_SESSION['usuario']) && !empty($_SESSION['usuario'])) {
            redirect(site_url() . 'general/principal');
        }

        $options = null;
        $data = null;

        $data['css'] = array('css/animation.css',
            'css/weather-icons.css',
            'css/weather-icons-wind.css');

        $data['noticias'] = $this->general_model->getNoticias();

        $data['js'] = array('js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/general.js');

        $this->form_validation->set_rules('txt_usuario', 'Usuario', 'trim|required|min_length[3]|max_length[25]|xss_clean');
        $this->form_validation->set_rules('txt_clave', 'Contrase&ntilde;a', 'trim|required|min_length[3]|max_length[12]|xss_clean');
        $formulario = $this->form_validation->run();
        $data['err_login'] = validation_errors();
        $this->load->view('barras/header_login', $data);
        if ($formulario) {
            $validaUsuario = $this->autenticacion($this->input->post('txt_usuario'), $this->input->post('txt_clave'));
            if ($validaUsuario['co_err'] == 0) {
                redirect(site_url('general/principal'));
            } else {
                $this->session->set_flashdata('error', $validaUsuario['tx_err']);
                session_destroy();
                $_SESSION = null;
                redirect(site_url());
            }
        } else {
            $this->load->view('principal/principal', $data);
        }

        $this->load->view('barras/footer');
    }

    private function autenticacion($txt_usuario, $txt_clave) {

        $response = array('co_err' => 0, 'tx_err' => '');

        if (!empty($txt_usuario) && !empty($txt_clave)) {

            $row = $this->general_model->login($txt_usuario);

            if (!empty($row)) {

                if (trim($txt_clave) == $this->encrypt->decode(trim($row['clave']))) {

                    $_SESSION['c_e'] = trim($row['cod_empleado']);
                    $_SESSION['c_r'] = trim($row['cod_encargado']);
                    $_SESSION['co_ccosto'] = trim($row['cod_centro_costo']);
                    $_SESSION['co_empresa'] = trim($row['cod_empresa']);
                    $_SESSION['nombre'] = trim($row['nombre']);
                    $_SESSION['e_mail'] = trim($row['e_mail']);
                    $_SESSION['usuario'] = $txt_usuario;
                    $_SESSION['id_emp'] = '';
                    $_SESSION['nomb_emp'] = '';
                    $_SESSION['co_emp'] = '';
                    $_SESSION['ba_emp'] = '';
                    $_SESSION['sv_emp'] = '';
                    $_SESSION['ind_emp'] = '';
                    $_SESSION['id_emp_sip'] = '';
                    $_SESSION['chang_pass'] = trim($row['cambio_pass']);

                    $menuComple = $this->createMenu();
                    $botones = $this->general_model->getBotones();
                    $almacenes = $this->getAlmacenes();
                    $arrMenu = $this->autocomplete_model->retMenuAuto();
                    $countMenu = count($arrMenu);
                    $arrPrinc = null;
                    if ($countMenu > 0) {
                        for ($i = 0; $i < $countMenu; $i++) {
                            if (!empty($arrMenu[$i]['urlinvocar'])) {
                                $CI = &get_instance();
                                $Sql = " SELECT DISTINCT b.tipo,b.menu,b.opcion ";
                                $Sql .= " FROM tb_intranet_users_perfil a ";
                                $Sql .= " INNER JOIN tb_intranet_perfil_opciones b ";
                                $Sql .= " ON b.perfil = a.perfil ";
                                $Sql .= " AND b.menu = " . $arrMenu[$i]['id'];
                                $Sql .= " WHERE a.cod_empl = '" . $_SESSION['c_e'] . "' ";
                                $dblxmaster = $CI->load->database('db_lx_master',true);
                                $query = $dblxmaster->query($Sql);

                                //$query = $CI->db->query($Sql);
                                $aaa = null;
                                if (count($query->result_array()) > 0) {

                                    $salto = $salto1 = '';
                                    $opcion = null;
                                    foreach ($query->result_array() AS $row) {
                                        if ($salto !== $row['tipo']) {
                                            foreach ($query->result_array() AS $rq) {
                                                if ($row['tipo'] === $rq['tipo'] && $row['menu'] === $rq['menu']) {
                                                    $opcion[] = $rq['opcion'];
                                                }
                                                $salto1 = $rq['tipo'];
                                            }
                                            $aaa[] = array($row['tipo'] => $opcion);
                                            $opcion = null;
                                        }
                                        $salto = $row['tipo'];
                                    }
                                    $arrPrinc[] = array_push_assoc($arrMenu[$i], 'opciones', $aaa);
                                } else {
                                    $arrPrinc[] = $arrMenu[$i];
                                }
                            } else {
                                $arrPrinc[] = $arrMenu[$i];
                            }
                        }
                    }
                    $_SESSION['menu'] = $arrPrinc;
                    $_SESSION['menuCompl'] = $menuComple;
                    $_SESSION['master'] = $botones;
                    $_SESSION['almacenes'] = $almacenes;
                } else {
                    $response['co_err'] = 1;
                    $response['tx_err'] = 'Clave ingresada es incorrecta.';
                }
            } else {
                $response['co_err'] = 1;
                $response['tx_err'] = 'Usuario no existe.';
            }
        } else {
            $response['co_err'] = 1;
            $response['tx_err'] = 'No enviado los parametros necesarios.';
        }

        return $response;
    }

    private function _cerrar_session() {
        session_destroy();
        $_SESSION = null;
        redirect(site_url());
    }

    private function createMenu() {

        $menuArm = '<ul id="sidebar-menu" class="sidebar-menu">';
        $menuArm.='<ul class="nav list-menu">';

        if (!isset($_SESSION)) {
            session_start();
        }

        $CI = &get_instance();

        $sqlPadre = " SELECT DISTINCT b.menu AS codigo,c.nombre,c.urlinvocar,c.icono,c.padre ";
        $sqlPadre .= " FROM tb_intranet_users_perfil a ";
        $sqlPadre .= " INNER JOIN tb_intranet_perfil_accesos b ";
        $sqlPadre .= " ON b.perfil = a.perfil ";
        $sqlPadre .= " AND b.tipo = 1  ";
        $sqlPadre .= " INNER JOIN tb_intranet_menu c ";
        $sqlPadre .= " ON c.codigo = b.menu ";
        $sqlPadre .= " AND c.padre = 0 ";
        $sqlPadre .= " WHERE a.cod_empl = '" . $_SESSION['c_e'] . "' ";
        $sqlPadre .= " ORDER BY b.menu ASC ";
        //$CI->db = $CI->load->database('desarrollo', true);
        $dblxmaster = $CI->load->database('db_lx_master', true);
        $query = $dblxmaster->query($sqlPadre);

        $iCount = 0;
        foreach ($query->result_array() as $row) {
            $iCount++;
            $iCount = $iCount >= 10 ? 1 : $iCount;
            $url = trim($row['urlinvocar']);

            if (!empty($url)) {
                $menuArm.='<li><a href="' . base_url($url) . '"><i class="fa ' . $row['icono'] . ' me-list menu-color-' . $iCount . ' "></i> <span>' . ($row['nombre']) . '</span></a></li>';
            } else {
                $menuArm.='<li class="treeview active">';
                $menuArm.='<a href="#">';
                $menuArm.='<i class="fa ' . $row['icono'] . ' me-list menu-color-' . $iCount . ' "></i> <span>' . ($row['nombre']) . '</span>';
                $menuArm.='</a>';
            }

            $padre = trim($row['codigo']);

            $sqlHijo = " SELECT DISTINCT b.menu AS codigo,c.nombre,c.urlinvocar,c.icono,c.padre ";
            $sqlHijo .= " FROM tb_intranet_users_perfil a ";
            $sqlHijo .= " INNER JOIN tb_intranet_perfil_accesos b ";
            $sqlHijo .= " ON b.perfil = a.perfil ";
            $sqlHijo .= " AND b.tipo = 1  ";
            $sqlHijo .= " INNER JOIN tb_intranet_menu c ";
            $sqlHijo .= " ON c.codigo = b.menu ";
            $sqlHijo .= " AND c.padre = " . $padre . " ";
            $sqlHijo .= " WHERE a.cod_empl = '" . $_SESSION['c_e'] . "' ";
            $sqlHijo .= " ORDER BY b.menu ASC ";
            //$CI->db = $CI->load->database('desarrollo', true);
            //$query1 = $CI->db->query($sqlHijo);
            $dblxmaster = $CI->load->database('db_lx_master', true);
            $query1 = $dblxmaster->query($sqlHijo);

            if (count($query1->result_array()) > 0) {
                $ul = 1;
                $menuArm.='<ul class="treeview-menu">';
            }

            foreach ($query1->result_array() as $rowHijo) {
                $urlHijo = trim($rowHijo['urlinvocar']);

                if (!empty($urlHijo)) {
                    $menuArm.='<li style="margin-top:0;"><a href="' . base_url($urlHijo) . '"><i class="fa fa-angle-double-right"></i>' . ($rowHijo['nombre']) . '</a></li>';
                } else {
                    $menuArm.='<li class="treeview active">';
                    $menuArm.='<a href="#">';
                    $menuArm.='<i class="fa fa-file-text-o"></i><span>' . ($rowHijo['nombre']) . '</span>';
                    $menuArm.='</a>';
                }

                $sqlSubHijo = " SELECT DISTINCT b.menu AS codigo,c.nombre,c.urlinvocar,c.icono,c.padre ";
                $sqlSubHijo .= " FROM tb_intranet_users_perfil a ";
                $sqlSubHijo .= " INNER JOIN tb_intranet_perfil_accesos b ";
                $sqlSubHijo .= " ON b.perfil = a.perfil ";
                $sqlSubHijo .= " AND b.tipo = 1  ";
                $sqlSubHijo .= " INNER JOIN tb_intranet_menu c ";
                $sqlSubHijo .= " ON c.codigo = b.menu ";
                $sqlSubHijo .= " AND c.padre = " . $rowHijo["codigo"] . " ";
                $sqlSubHijo .= " WHERE a.cod_empl = '" . $_SESSION['c_e'] . "' ";
                $sqlSubHijo .= " ORDER BY b.menu  ASC ";
                //$CI->db = $CI->load->database('desarrollo', true);

                //$query2 = $CI->db->query($sqlSubHijo);
                $dblxmaster = $CI->load->database('db_lx_master', true);
                $query2 = $dblxmaster->query($sqlSubHijo);

                if (count($query2->result_array()) > 0) {
                    $ulHijo = 1;
                    $menuArm.='<ul class="treeview-menu">';
                }

                foreach ($query2->result_array() as $rowSubHijo) {
                    $urlSubHijo = trim($rowSubHijo['urlinvocar']);

                    if (!empty($urlSubHijo)) {
                        $menuArm.='<li style="margin-top:0;"><a href="' . base_url($urlSubHijo) . '"><i class="fa fa-angle-double-right"></i>' . ($rowSubHijo['nombre']) . '</a></li>';
                    } else {
                        $menuArm.='<li class="treeview active">';
                        $menuArm.='<a href="#">';
                        $menuArm.='<i class="fa fa-file-text-o"></i><span>' . ($rowSubHijo['nombre']) . '</span>';
                        $menuArm.='</a>';

                        $sqlSub1Hijo = " SELECT DISTINCT b.menu AS codigo,c.nombre,c.urlinvocar,c.icono,c.padre ";
                        $sqlSub1Hijo .= " FROM tb_intranet_users_perfil a ";
                        $sqlSub1Hijo .= " INNER JOIN tb_intranet_perfil_accesos b ";
                        $sqlSub1Hijo .= " ON b.perfil = a.perfil ";
                        $sqlSub1Hijo .= " AND b.tipo = 1  ";
                        $sqlSub1Hijo .= " INNER JOIN tb_intranet_menu c ";
                        $sqlSub1Hijo .= " ON c.codigo = b.menu ";
                        $sqlSub1Hijo .= " AND c.padre = " . $rowSubHijo["codigo"] . " ";
                        $sqlSub1Hijo .= " WHERE a.cod_empl = '" . $_SESSION['c_e'] . "' ";
                        $sqlSub1Hijo .= " ORDER BY b.menu  ASC ";
                        //$CI->db = $CI->load->database('desarrollo', true);

                        //$query3 = $CI->db->query($sqlSub1Hijo);
                        $dblxmaster = $CI->load->database('db_lx_master', true);
                        $query3 = $dblxmaster->query($sqlSub1Hijo);

                        if (count($query3->result_array()) > 0) {
                            $ulSubHijo = 1;
                            $menuArm.='<ul class="treeview-menu">';
                        }

                        foreach ($query3->result_array() as $rowSub1Hijo) {
                            $urlSub1Hijo = trim($rowSub1Hijo['urlinvocar']);
                            if (!empty($urlSub1Hijo)) {
                                $menuArm.='<li style="margin-top:0;"><a href="' . base_url($urlSub1Hijo) . '"><i class="fa fa-angle-double-right"></i>' . ($rowSub1Hijo['nombre']) . '</a></li>';
                            } else {
                                $menuArm.='<li class="treeview active">';
                                $menuArm.='<a href="#">';
                                $menuArm.='<i class="fa fa-file-text-o"></i><span>' . ($rowSub1Hijo['nombre']) . '</span>';
                                $menuArm.='</a>';

                                $sqlSub2Hijo = " SELECT DISTINCT b.menu AS codigo,c.nombre,c.urlinvocar,c.icono,c.padre ";
                                $sqlSub2Hijo .= " FROM tb_intranet_users_perfil AS a ";
                                $sqlSub2Hijo .= " INNER JOIN tb_intranet_perfil_accesos AS b ";
                                $sqlSub2Hijo .= " ON b.perfil = a.perfil ";
                                $sqlSub2Hijo .= " AND b.tipo = 2  ";
                                $sqlSub2Hijo .= " INNER JOIN tb_intranet_menu AS c ";
                                $sqlSub2Hijo .= " ON c.codigo = b.menu ";
                                $sqlSub2Hijo .= " AND c.padre = " . $rowSub1Hijo["codigo"] . " ";
                                $sqlSub2Hijo .= " WHERE a.cod_empl = '" . $_SESSION['c_e'] . "' ";
                                $sqlSub2Hijo .= " ORDER BY b.menu ASC ";
                                //$CI->db = $CI->load->database('desarrollo', true);
                                //$query4 = $CI->db->query($sqlSub2Hijo);
                                $dblxmaster = $CI->load->database('db_lx_master', true);
                                $query4 = $dblxmaster->query($sqlSub2Hijo);
                                if (count($query4->result_array()) > 0) {
                                    $ulSub1Hijo = 1;
                                    $menuArm.='<ul class="treeview-menu">';
                                }

                                foreach ($query4->result_array() as $rowSub2Hijo) {
                                    $urlSub2Hijo = trim($rowSub2Hijo['urlinvocar']);

                                    if (!empty($urlSub2Hijo)) {
                                        $menuArm.='<li style="margin-top:0;"><a href="' . base_url($urlSub2Hijo) . '"><i class="fa fa-angle-double-right"></i>' . ($rowSub2Hijo['nombre']) . '</a></li>';
                                    } else {
                                        $menuArm.='<li class="treeview active">';
                                        $menuArm.='<a href="#">';
                                        $menuArm.='<i class="fa fa-file-text-o"></i><span>' . ($rowSub2Hijo['nombre']) . '></span>';
                                        $menuArm.='</a>';
                                    }
                                }
                                if (count($query4->result_array()) > 0) {
                                    if ($ulSub1Hijo === 1) {
                                        $menuArm.='</ul>';
                                    }
                                }
                            }
                        }

                        if (count($query3->result_array()) > 0) {
                            if ($ulSubHijo === 1) {
                                $menuArm.='</ul>';
                            }
                        }
                    }
                }

                if (count($query2->result_array()) > 0) {
                    if ($ulHijo === 1) {
                        $menuArm.='</ul>';
                    }
                }
                $menuArm.='</li>';
            }
            if (count($query1->result_array()) > 0) {
                if ($ul === 1) {
                    $menuArm.='</ul>';
                }
            }
            $menuArm.='</li>';
        }

        $menuArm.='</ul>';
        $menuArm.='</ul>';
        return $menuArm;
    }

    private function jsonModule($id, $sp, $cod_emp, $cnn) {

        if (!empty($sp)) {
            $CI = &get_instance();
            $abc = $CI->config->item('conexion');
            $conn = odbc_connect($abc[$cnn], "pica", "");
            $Sql = "$sp $cod_emp";

            $resultset = odbc_exec($conn, $Sql);

            $a = '';
            $pass = 0;
            $arrCa = null;
            $arrDe = null;
            $arrItems = null;

            while ($row = odbc_fetch_array($resultset)) {
                $pass++;
                $a = trim($row[odbc_field_name($resultset, 1)]);
                for ($b = 2; $b <= odbc_num_fields($resultset); $b++) {
                    $arrItems[odbc_field_name($resultset, $b)] = utf8_encode(trim($row[odbc_field_name($resultset, $b)]));
                }
                $arrDe[] = $arrItems;
            }
            $arrCa[$a] = $arrDe;

            while (odbc_next_result($resultset)) {
                $arrDe = null;
                $arrItems = null;
                while ($row = odbc_fetch_array($resultset)) {
                    $pass++;
                    $a = trim($row[odbc_field_name($resultset, 1)]);
                    for ($b = 2; $b <= odbc_num_fields($resultset); $b++) {
                        $arrItems[odbc_field_name($resultset, $b)] = utf8_encode(trim($row[odbc_field_name($resultset, $b)]));
                    }
                    $arrDe[] = $arrItems;
                }
                $arrCa[$a] = $arrDe;
            }

            if ($pass > 0) {
                $fp = fopen('temp/json/' . $cod_emp . "-" . $id . '.json', 'w');
                fwrite($fp, json_encode(array('master' => array($id => $arrCa))));
                fclose($fp);
            }
        }
    }

    private function xmlModule($id, $sp, $cod_emp) {
        if (!empty($sp)) {
            $conn = odbc_connect("SQLNSIP-P", "pica", "");
            $Sql = "$sp $cod_emp";
            $resultset = odbc_exec($conn, $Sql);

            $dom = new DOMDocument("1.0");
            $a = '';
            $p = odbc_num_fields($resultset);

            while ($row = odbc_fetch_array($resultset)) {
                if ($a !== trim($row[odbc_field_name($resultset, 1)])) {
                    $node = $dom->createElement(trim($row[odbc_field_name($resultset, 1)]));
                    $dom->appendChild($node);
                }

                $items = $dom->createElement('data');

                for ($b = 2; $b <= $p; $b++) {
                    $items->setAttribute(odbc_field_name($resultset, $b), utf8_encode(trim($row[odbc_field_name($resultset, $b)])));
                    $node->appendChild($items);
                }

                $a = trim($row[odbc_field_name($resultset, 1)]);
            }

            while (odbc_next_result($resultset)) {
                $p = odbc_num_fields($resultset);
                while ($row = odbc_fetch_array($resultset)) {
                    if ($a !== trim($row[odbc_field_name($resultset, 1)])) {
                        $node = $dom->createElement(trim($row[odbc_field_name($resultset, 1)]));
                        $dom->appendChild($node);
                    }

                    $items = $dom->createElement('data');

                    for ($b = 2; $b <= $p; $b++) {
                        $items->setAttribute(odbc_field_name($resultset, $b), utf8_encode(trim($row[odbc_field_name($resultset, $b)])));
                        $node->appendChild($items);
                    }

                    $a = trim($row[odbc_field_name($resultset, 1)]);
                }
            }

            $dom->save('temp/xml/' . $cod_emp . "-" . $id . '.xml');
        }
    }

    private function getAlmacenes() {
        $Sql = " podbpos.dbo.posGetServersMonitor ";
        $query = $this->db->query($Sql);
        if ($this->db->_error_message()) {
            $this->db->trans_rollback();
            return "ERROR AL OBTENER ALMACENES: " . $this->db->_error_number() . " " . $this->db->_error_message();
        }

        if (count($query->result_array()) > 0) {
            return $query->result_array();
            unset($result);
        }
    }

}
