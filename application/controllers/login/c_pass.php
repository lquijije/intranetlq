<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_pass extends CI_Controller {	
	
    function __construct()
    {		
            parent::__construct();	
            $this->load->library(array('encrypt'));
            session_start();          
    }	

    public function _remap($method)
    {      
        $method = $this->uri->segment(3);

        if ($method == 'cambio_pass') {
            $this->cambio_pass();
        } else {
            $this->inicio();
        }

    }

    private function inicio(){

        if(isset($_SESSION) && empty($_SESSION)){
            redirect(site_url());
        }

        $bandera = $this->input->get('e_m',true);

        $data['js'] = array('js/jquery.min.js',
                            'js/bootstrap.min.js',
                            'js/general.js');

        $this->load->view('barras/header',$data);   
        $this->load->view('barras/menu');  

        $this->form_validation->set_rules('txt_usuario', 'Usuario', 'trim|required|min_length[3]|max_length[30]|xss_clean'); 
        $this->form_validation->set_rules('txt_clave', 'Clave Actual', 'trim|required|min_length[3]|max_length[30]|xss_clean');

        if(!empty($bandera) && $bandera == 'P'){
            $data['form_pass'] = 'login/c_pass?e_m=P';
            $bandera = TRUE;
            $this->form_validation->set_rules('txt_nueva_clave', 'Clave Nueva','trim|min_length[3]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txt_rep_clave', 'Repetir Clave','trim|min_length[3]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txt_mail','Email','valid_email|required');
        } else {
            $bandera = FALSE;
            $data['form_pass'] = 'login/c_pass';
            $this->form_validation->set_rules('txt_nueva_clave', 'Clave Nueva','trim|required|min_length[3]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txt_rep_clave', 'Repetir Clave','trim|required|min_length[3]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txt_mail','Email','valid_email');
        }

        $data['band'] = $bandera;

        //////////////////////////////////////
        if ($this->form_validation->run() == FALSE){
            $this->load->view('login/v_pass',$data); 
        } else {   
            $usuario = $this->input->post('txt_usuario');
            $clave_actual = $this->input->post('txt_clave');
            $clave_nueva = $this->input->post('txt_nueva_clave');
            $clave_rep = $this->input->post('txt_rep_clave');
            $txt_mail = $this->input->post('txt_mail');

            if($bandera == false){

                if (trim($clave_nueva) <> trim($clave_rep)){
                    $this->session->set_flashdata('error','Las Contraseñas no coinciden');
                    redirect(site_url('login/c_pass'));
                }

            }

            $validaUsuario = $this->autenticacion($usuario,$clave_actual);
            
            if($validaUsuario['co_err'] == 0){
                $clave_nueva = !empty($clave_nueva)?$this->encrypt->encode($clave_nueva):'';
                $this->general_model->actualiza_clave($usuario,$clave_nueva,$txt_mail);
                $this->session->set_flashdata('error','Sus datos han sido modificados exitosamente');
                session_unset();
                session_destroy(); 
                redirect(site_url());  
            } else {
                $this->session->set_flashdata('error',$validaUsuario['tx_err']);
                $_SESSION = null;
                session_destroy();
                if($bandera == false){
                    redirect(site_url('login/c_pass'));
                } else {
                    redirect(site_url('login/c_pass?e_m=P'));
                }
            }   

        }

        $this->load->view('barras/footer');
    }
        
    private function autenticacion($txt_usuario,$txt_clave){
        
        $response = array('co_err'=>0,'tx_err'=>'');
        
        if(!empty($txt_usuario) && !empty($txt_clave)){
           
            $row = $this->general_model->login($txt_usuario);
            
            if(!empty($row)){
                
                if (trim($txt_clave) <> $this->encrypt->decode(trim($row['clave']))){
                    $response['co_err'] = 1;
                    $response['tx_err'] = 'Clave ingresada es incorrecta.';
                }
                
            } else {
                $response['co_err'] = 1;
                $response['tx_err'] = 'Usuario no existe.';
            }
                        
        } else {
            $response['co_err'] = 1;
            $response['tx_err'] = 'No enviado los parametros necesarios.';
        }
        
        return $response;
        
    }

}