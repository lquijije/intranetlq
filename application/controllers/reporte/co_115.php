<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_115 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        session_start();
    }	

    public function _remap()  {    
        
        validaSession('reporte/co_115');  
        $this->_crud();
    }
        
    private function _crud(){
        $data = null; 
        $urlControlador = 'reporte/co_115';
        $data['urlControlador'] = $urlControlador;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/general.js');
        
        $site = "temp/json/".$_SESSION['c_e'].".json";
        $reportes = null;
        
        if (file_exists($site)) {
            $string = file_get_contents($site);
            $json_array = json_decode($string, true);
            $reportes = isset($json_array['master']['47']['reporting_services']) && !empty($json_array['master']['47']['reporting_services'])?$json_array['master']['47']['reporting_services']:null;
        }
        
        $data['reportes'] = $reportes;
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('reporte/v_115'); 
        $this->load->view('barras/footer');
    }
    
}