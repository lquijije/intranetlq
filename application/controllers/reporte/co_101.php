<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_101 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->library(array('form_validation'));
        $this->load->model(array('reporte/m_101'));
        session_start();
    }	

    public function _remap()  {    
        
        validaSession('reporte/co_101');  
        $tipo=$this->uri->segment(3);
        if($tipo=='retChart'){
            $this->_retChart();
        } else {
            $this->_crud();
        }
    }
        
    private function _crud(){
        $data = null; 
        $urlControlador = 'reporte/co_101';
        $data['urlControlador'] = $urlControlador;
        $data['css'] = array('css/datetimepicker/jquery.datetimepicker.css');
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/reporte/j_101.js',
                            'js/general.js',
                            'js/plugins/datetimepicker/jquery.datetimepicker.js',
                            'js/plugins/flot/jquery.flot.min.js',
                            'js/plugins/flot/jquery.flot.orderBars.js',
                            'js/plugins/flot/jquery.flot.time.min.js');
 
        $combo = $this->m_101->retCombo();
        $datosCombo = null;
        if(count($combo)>0) {
            foreach($combo as $row){
                $datosCombo[trim($row['co_bodega'])]=$row['descripcion'];
            }
        } else {
            $datosCombo[]='No hay Datos';   
        } 
        
        $atributosEspecial="id='cmb_bodega' class='form-control'";
        $data['cmb_bodega']=form_dropdown('cmb_bodega',$datosCombo,0,$atributosEspecial);
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('reporte/v_101'); 
        $this->load->view('barras/footer');
    }
    
    private function _retChart(){
        $datos = $this->input->post();
        $mensaje = $this->m_101->retChart($datos);
        echo $mensaje;
    }
    
}