<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class IngresoPedidos extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('inventario/m_IngresoPedidos');
		$this->load->model('inventario/m_ConsultaArticulo');
		session_start();
	}
	public function _remap(){
		validaSession('inventario/IngresoPedidos'); 
		$tipo=$this->uri->segment(3);

		if($tipo == 'retBusqArt'){
			$this->_retBusqArt();
		}elseif ($tipo == 'retTraslados'){
			$this->_retTraslados();
		}elseif ($tipo == 'retConsArt'){
			$this->_retConsArt();
		}elseif ($tipo == 'retStockBasico'){
			$this->_retStockBasico();
		}elseif ($tipo == 'retConsBodeGyeUio'){
			$this->_retConsBodeGyeUio();
		}elseif ($tipo == 'retExistenciaItem'){
			$this->_retExistenciaItem();
		}elseif ($tipo == 'grabaPedido'){
			$this->_grabaPedido();
		}else{			
			$this->_index();
		}
	}

	private function _index(){
		$data=null;
		$data['permisos']='';   
		$urlControlador = 'inventario/IngresoPedidos';
		$data['urlControlador'] = $urlControlador;

		$data['hoy'] = date('Y-m-d H:m:s', strtotime('+0 day'));

		$data['css'] = array(
			'js/plugins/tab/tab.css',
			'css/datatables/dataTables.bootstrap.css',
			'css/datetimepicker/jquery.datetimepicker.css',
			'css/bootstrap-checkbox-radio/checkbox-radio.css',
			'js/plugins/perfect-scrollbar/css/perfect-scrollbar.css');


		$data['js']=array('js/jquery-1.10.2.min.js',
			'js/general.js',
			'js/plugins/tab/tab.js',
			'js/plugins/datatables/dataTables.bootstrap.js',
			'js/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
			'js/inventario/j_ingresopedidos.js',
			'js/plugins/datatables/jquery.dataTables.js',
			'js/plugins/datetimepicker/jquery.datetimepicker.js',
			'js/bootstrap.min.js');
		
		$data['table_ped'] = $this->m_IngresoPedidos->retTableBodPide();
		$data['table_mot'] = $this->m_IngresoPedidos->retTableMotivo();
		$data['table_bod'] = $this->m_IngresoPedidos->retTableBodeA();

		$this->load->view('barras/header',$data);
		$this->load->view('barras/menu');
		$this->load->view('inventario/v_IngresoPedidos');
		$this->load->view('barras/footer');
	}

	private function _retBusqArt(){
		$strBus = $this->input->post('c_bu');
		$strBus = str_replace("||","%",$strBus);
		$datos  = $strBus;
		$mensaje   = $this->m_IngresoPedidos->retTablaBusqArt($datos);             
		echo($mensaje);
	}

	private function _retTraslados(){
		$bodent = $this->input->post('c_be');
		$mensaje   = $this->m_IngresoPedidos->retTablaTraslados($bodent);             
		echo($mensaje);
	}

	private function _retConsArt(){
		$datos = $this->input->post('c_cd');
		$mensaje   = $this->m_ConsultaArticulo->retConsArt($datos);             
		echo($mensaje);
	}

	private function _retExistenciaItem(){
		$datos  = array(
			"c_codart" =>$this->input->post('c_ca'),
			"c_bodsal" =>$this->input->post('c_bs'),
			"c_bodent" =>$this->input->post('c_be')
			);
		$existencia   = $this->m_IngresoPedidos->retExistenciaItem($datos);  
		if(is_array($existencia)){
			echo("OK-".json_encode($existencia));	
		}else{
			echo($existencia);
		}		
	}

	private function _retStockBasico(){
		$bodent = $this->input->post('c_be');
		$det_bod   = $this->m_IngresoPedidos->retStockBasico($bodent);             
		if(is_array($det_bod)){
			echo("OK-".json_encode($det_bod));
		}else{
			echo($det_bod);
		}		
	}

	private function _retConsBodeGyeUio(){
		$bodent = $this->input->post('c_be');
		$det_bod   = $this->m_IngresoPedidos->retConsBodeGyeUio($bodent);  
		if(is_array($det_bod)){
			echo("OK-".json_encode($det_bod));
		}else{
			echo($det_bod);
		}           
	}

	private function _grabaPedido(){
		$datos = array('detPedidos'=>json_decode($_POST['detPedidos']),
						   'cabPedidos'=>json_decode($_POST['cabPedidos']));
		$mensaje = 	$this->m_IngresoPedidos->grabaPedido($datos); 
		echo $mensaje;
    }
    
	
}