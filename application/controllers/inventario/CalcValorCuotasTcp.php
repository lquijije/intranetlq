<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CalcValorCuotasTcp extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('inventario/m_CalcValorCuotasTcp');
		$this->load->model('inventario/m_ConsultaArticulo');
		session_start();
	}

	public function _remap(){
		validaSession('inventario/ConsultaArticulo'); 
		$tipo=$this->uri->segment(3);

		if ($tipo == 'retTablaBusqueda'){
			$this->_retTablaBusqueda();
		}elseif ($tipo == 'retConsArt'){
			$this->_retConsArt();
		}elseif ($tipo == 'retTablaAmortiza'){
			$this->_retTablaAmortiza();
		}else {
			$this->_index(); 
		}
	}

	private function _index(){
		$data=null; 
		$data['permisos']='';   
		$urlControlador = 'inventario/CalcValorCuotasTcp';
		$data['urlControlador'] = $urlControlador;

		$data['css'] = array(
			'js/plugins/tab/tab.css',
			'css/datatables/dataTables.bootstrap.css',
			'css/datetimepicker/jquery.datetimepicker.css',
			'css/bootstrap-checkbox-radio/checkbox-radio.css',
			'js/plugins/perfect-scrollbar/css/perfect-scrollbar.css');


		$data['js']=array('js/jquery-1.10.2.min.js',
			'js/general.js',
			'js/plugins/tab/tab.js',
			'js/plugins/datatables/dataTables.bootstrap.js',
			'js/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
			'js/inventario/j_calcvalorcuotastcp.js',
			'js/plugins/datatables/jquery.dataTables.js',
			'js/plugins/datetimepicker/jquery.datetimepicker.js',
			'js/bootstrap.min.js');


		$this->load->view('barras/header',$data);
		$this->load->view('barras/menu');
		$this->load->view('inventario/v_CalcValorCuotasTcp');
		$this->load->view('barras/footer');
	}

	private function _retTablaBusqueda(){
		$strBus = $this->input->post('c_bu');
		$strBus = str_replace("||","%",$strBus);
		$datos  = $strBus;
		$mensaje   = $this->m_CalcValorCuotasTcp->retTablaBusqueda($datos);             
		echo($mensaje);
	}

	private function _retConsArt(){
		$datos = $this->input->post('c_cd');
		$mensaje   = $this->m_ConsultaArticulo->retConsArt($datos);             
		echo($mensaje);
	}

	private function _retTablaAmortiza(){
		$monto = $this->input->post('c_mt');
		$mensaje   = $this->m_CalcValorCuotasTcp->retTablaAmortiza($monto);             
		echo($mensaje);
	}
}