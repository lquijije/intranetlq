<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MarcaSalidaLlegadaCamiones extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('inventario/m_MarcaSalidaLlegadaCamiones');
		session_start();
	}

	public function _remap(){
		validaSession('inventario/MarcaSalidaLlegadaCamiones'); 
		$tipo=$this->uri->segment(3);
		if ($tipo == 'retTablaPedidosPendientes') {
			$this->_retTablaPedidosPendientes();
		}elseif($tipo == 'retTablaSalidaEntradaPed'){
			$this->_retTablaSalidaEntradaPed();
		}elseif($tipo == 'retTablaTransportistas'){
			$this->_retTablaTransportistas();
		}elseif($tipo == 'guardarTransp'){
			$this->_guardarTransp();
		}elseif($tipo == 'retBlomba'){
			$this->_retBlomba();
		}elseif($tipo == 'retPedValidaLlegadaPrevia'){
			$this->_retPedValidaLlegadaPrevia();
		}elseif($tipo == 'savPedidoLlegada'){
			$this->_savPedidoLlegada();
		}else{
			$this->_index();
		}
	}

	private function _index(){
		$data=null;
		$data['permisos']='';   
		$urlControlador = 'inventario/MarcaSalidaLlegadaCamiones';
		$data['urlControlador'] = $urlControlador;

		$data['hoy'] = date('Y-m-d H:m:s', strtotime('+0 day'));

		$data['css'] = array(
			'js/plugins/tab/tab.css',
			'css/datatables/dataTables.bootstrap.css',
			'css/datetimepicker/jquery.datetimepicker.css',
			'css/bootstrap-checkbox-radio/checkbox-radio.css',
			'js/plugins/perfect-scrollbar/css/perfect-scrollbar.css');


		$data['js']=array('js/jquery-1.10.2.min.js',
			'js/general.js',
			'js/plugins/tab/tab.js',
			'js/plugins/datatables/dataTables.bootstrap.js',
			'js/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
			'js/inventario/j_marcasalidallegadacamiones.js',
			'js/plugins/datatables/jquery.dataTables.js',
			'js/plugins/datetimepicker/jquery.datetimepicker.js',
			'js/bootstrap.min.js');

		$data['table_ped'] = $this->m_MarcaSalidaLlegadaCamiones->retTableBodDesp();
		
		$this->load->view('barras/header',$data);
		$this->load->view('barras/menu');
		$this->load->view('inventario/v_MarcaSalidaLlegadaCamiones');
		$this->load->view('barras/footer');
	}

	private function _retTablaPedidosPendientes(){
		$datos = array(
			"c_bod_sa"=>$this->input->post('c_cb'),
			"c_es_sal"=>$this->input->post('c_sl'),
			"c_es_cho"=>$this->input->post('c_ch'),
			"c_id_ped"=>$this->input->post('c_cp')
			);
		$mensaje   = $this->m_MarcaSalidaLlegadaCamiones->retTablaPedidosPendientes($datos);             
		echo($mensaje);
	}

	private function _retTablaSalidaEntradaPed(){
		$datos = array(
			"c_bod_se"=>$this->input->post('c_se'),
			"c_no_ped"=>$this->input->post('c_cp'),
			"c_s_esta"=>$this->input->post('c_es'),
			"c_s_tipo"=>$this->input->post('c_tp'),
			"c_idpick"=>$this->input->post('c_pk')
			);
		$mensaje   = $this->m_MarcaSalidaLlegadaCamiones->retTablaSalidaEntradaPed($datos);             
		echo($mensaje);
	}

	private function _retTablaTransportistas(){
		$strBus = $this->input->post('c_bus');
		$strBus = str_replace("||","%",$strBus);
		$mensaje   = $this->m_MarcaSalidaLlegadaCamiones->retTablaTransportistas($strBus);             
		echo($mensaje);
	}

	private function _guardarTransp(){
		$datos = array(
			"c_ced"=>$this->input->post('c_id'),
			"c_nom"=>$this->input->post('c_no'),
			"c_pla"=>$this->input->post('c_pl'),
			"c_dir"=>$this->input->post('c_di')
			);
		$mensaje   = $this->m_MarcaSalidaLlegadaCamiones->guardarTransp($datos);             
		echo($mensaje);
	}

	private function _retBlomba(){
		$datos = array(
			"c_pe"=>$this->input->post('c_pe'),
			"c_be"=>$this->input->post('c_be'),
			"c_bs"=>$this->input->post('c_bs')
			);
		$blomba   = $this->m_MarcaSalidaLlegadaCamiones->retBlomba($datos);
		echo $blomba;
	}

	private function _retPedValidaLlegadaPrevia(){
		$datos = array(
			"c_pd"=>$this->input->post('c_pd'),
			"c_bs"=>$this->input->post('c_bs'),
			"c_bl"=>$this->input->post('c_bl'),
			"c_pk"=>$this->input->post('c_pk')
			);
		$mensaje   = $this->m_MarcaSalidaLlegadaCamiones->retPedValidaLlegadaPrevia($datos);
		echo $mensaje;
	}

	private function _savPedidoLlegada(){
		$datos = array(
			"c_ch"=>$this->input->post('c_ch'),
			"c_pd"=>$this->input->post('c_pd'),
			"c_bs"=>$this->input->post('c_bs'),
			"c_bl"=>$this->input->post('c_bl'),
			"c_pk"=>$this->input->post('c_pk'),
			"c_sl"=>$this->input->post('c_sl'),
			"c_bm"=>$this->input->post('c_bm')
			);
		$mensaje   = $this->m_MarcaSalidaLlegadaCamiones->ped5000_llegada($datos);
		echo $mensaje;
	}
}