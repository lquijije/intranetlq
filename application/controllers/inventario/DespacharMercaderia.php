<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class DespacharMercaderia extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('inventario/m_DespacharMercaderia');
		session_start();
	}
	public function _remap(){
		validaSession('inventario/DespacharMercaderia'); 
		$tipo=$this->uri->segment(3);
		if ($tipo == 'retTablaPedidosPorDesp'){
			$this->_retTablaPedidosPorDespachar();
		}elseif($tipo == 'retTablaDetallePedido'){
			$this->_retTablaDetallePedido();
		}else{
			$this->_index();
		}
	}
	private function _index(){
		$data=null;
		$data['permisos']='';   
		$urlControlador = 'inventario/DespacharMercaderia';
		$data['urlControlador'] = $urlControlador;

		$data['hoy'] = date('Y-m-d H:m:s', strtotime('+0 day'));

		$data['css'] = array(
			'js/plugins/tab/tab.css',
			'css/datatables/dataTables.bootstrap.css',
			'css/datetimepicker/jquery.datetimepicker.css',
			'css/bootstrap-checkbox-radio/checkbox-radio.css',
			'js/plugins/perfect-scrollbar/css/perfect-scrollbar.css');


		$data['js']=array('js/jquery-1.10.2.min.js',
			'js/general.js',
			'js/plugins/tab/tab.js',
			'js/plugins/datatables/dataTables.bootstrap.js',
			'js/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
			'js/inventario/j_despacharmercaderia.js',
			'js/plugins/datatables/jquery.dataTables.js',
			'js/plugins/datetimepicker/jquery.datetimepicker.js',
			'js/bootstrap.min.js');
		$data['table_ped'] = $this->m_DespacharMercaderia->retTableBodDesp();

		$this->load->view('barras/header',$data);
		$this->load->view('barras/menu');
		$this->load->view('inventario/v_DespacharMercaderia');
		$this->load->view('barras/footer');
	}

	private function _retTablaPedidosPorDespachar(){
		$datos  = array(
			"c_bodsal" =>$this->input->post('c_bs'),
			"c_estado" =>$this->input->post('c_es'),
			"c_zebra"  =>$this->input->post('c_zb'),
			"c_impres" =>$this->input->post('c_im'),
			"c_codped" =>$this->input->post('c_pd')
			);
		$mensaje   = $this->m_DespacharMercaderia->retTablaPedidosPorDespachar($datos);       
		echo($mensaje);
	}

	private function _retTablaDetallePedido(){
		$datos  = array(
			"c_codped" =>$this->input->post('c_cp'),
			"c_bodsal" =>$this->input->post('c_bs'),
			"c_bodent" =>$this->input->post('c_be'),
			"c_pickin" =>$this->input->post('c_pk')
			);
		$mensaje   = $this->m_DespacharMercaderia->retTabladetallePedido($datos);       
		echo($mensaje);
	}
}