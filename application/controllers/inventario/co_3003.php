<?php

if (!defined('BASEPATH'))
    exit
            ('No direct script access allowed');

class co_3003 extends CI_Controller {

    function

    __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model(array('inventario/m_3003'));
        session_start();
    }

    public function _remap() {

        $tipo = $this->uri->segment(3);
        $datos = $this->input->post();

        validaSession('inventario/co_3003');

        switch ($tipo) {
            case 'artCou':
                $this->_consultaArticulosCourier();
                break;
            case 'proArts':
                $this->_procesarArticulos();
                break;
            case 'geExc':
                $this->_generarExcel();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {

        $data = null;

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/general.js',
            'js/config.js',
            'js/inventario/j_3003.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js'
        );

        $data['articulos'] = null;
        //$data['articulos'] = $this->_consultaArticulosCourier();
        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('inventario/v_3003.php');
        $this->load->view('barras/footer');
    }

    private function _generarExcel() {

        $articulosMysql = $this->m_3003->_retArtitulosMysql();
        if ($articulosMysql['co_err'] == 0) {
            $articulosSql = $this->m_3003->_retArtitulosSql($articulosMysql);
            if ($articulosSql['co_err'] == 0) {

                $this->load->library('PHPExcel');
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getActiveSheet();

                $objPHPExcel->
                        getProperties()
                        ->setCreator("PRODUCTOS COURIER")
                        ->setTitle("PRODUCTOS COURIER")
                        ->setSubject("PRODUCTOS COURIER")
                        ->setDescription("PRODUCTOS COURIER")
                        ->setKeywords("PRODUCTOS COURIER")
                        ->setCategory("PRODUCTOS COURIER");

                $borders1 = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                            'color' => array('rgb' => '000000'),
                        )
                    ),
                );

                $borders = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000'),
                        )
                    ),
                );

                $Letra = 'G';
                $linea = 1;
                $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(13);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('PRODUCTOS COURIER', PHPExcel_Cell_DataType::TYPE_STRING);
                $linea++;
                $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('FECHA ' . date('Y-m-d H:i:s'), PHPExcel_Cell_DataType::TYPE_STRING);
                $linea++;
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders1);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $linea, 'CODIGO');
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $linea, 'DESCRIPCION');
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $linea, 'PRECIO');
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $linea, 'PESO');
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $linea, 'ALTO');
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $linea, 'LARGO');
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $linea, 'ANCHO');
                $linea++;
                foreach ($articulosSql['data'] as $row) {
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['co_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['ds_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['va_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('D' . $linea)->setValueExplicit($row['pe_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit($row['al_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('F' . $linea)->setValueExplicit($row['la_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getCell('G' . $linea)->setValueExplicit($row['an_art'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $linea++;
                }

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->setTitle('LISTA');

                $hoy = date("Y_m_d_h_i");
                $objPHPExcel->getActiveSheet()->setTitle('REPORTE');
                $filename = 'PRODUCTOS_COURIER_' . $hoy . '.xls';
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=1');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('temp/' . $filename);
                header("location: " . site_url() . "temp/$filename");
                unlink(site_url("temp/$filename"));
            }
        }
    }

    private function _consultaArticulosCourier1() {

        $articulosMysql = $this->m_3003->_retArtitulosMysql();
        $array = array('co_err' => $articulosMysql['co_err'], 'tx_err' => $articulosMysql['tx_err'], 'data' => null);

        if ($articulosMysql['co_err'] == 0) {
            $articulosSql = $this->m_3003->_retArtitulosSql($articulosMysql);
//            print_r($articulosSql);
//            die;
            if ($articulosSql['co_err'] == 0) {

                $datatable_data = array('data' => null);

                foreach ($articulosSql['data'] as $row) {

                    $dimension = !empty($row['di_art']) ? json_decode($row['di_art'], true) : null;
                    $largo = isset($dimension['LARGO']) ? $dimension['LARGO'] : '';
                    $ancho = isset($dimension['ANCHO']) ? $dimension['ANCHO'] : '';
                    $alto = isset($dimension['ALTO']) ? $dimension['ALTO'] : '';
                    $medida = isset($dimension['UNIDAD']) ? $dimension['UNIDAD'] : '';

                    $peso = !empty($row['pe_art']) ? explode(' ', $row['pe_art']) : null;
                    $va_peso = isset($peso[0]) ? $peso[0] : '';
                    $cmb_peso = isset($peso[1]) ? $peso[1] : '';

                    $array_peso = array(
                        'g' => 'g = GRAMO',
                        'lb' => 'lb = LIBRA',
                        'kg' => 'kg = KILOGRAMO',
                    );

                    $array_longitud = array(
                        'mm' => 'mm = MILIMETRO',
                        'cm' => 'cm = CENTIMETRO',
                        'dm' => 'dm = DECIMETRO',
                        'm' => 'm = METRO',
                    );

                    $cmb_pe = form_dropdown('cmb_pe_' . $row['co_art'], $array_peso, $cmb_peso, "id='cmb_pe_" . $row['co_art'] . "' name='cmb_pe_" . $row['co_art'] . "' class='selectpicker form-control cmb_pe_' style='width: 65px;' ");
                    $cmb_lo = form_dropdown('cmb_lo_' . $row['co_art'], $array_longitud, $medida, "id='cmb_lo_" . $row['co_art'] . "' name='cmb_lo_" . $row['co_art'] . "' class='form-control cmb_lo_' style='width: 72px;' ");

                    $datatable_data['data'][] = array(
                        '<center><input id="btn_check_art" type="checkbox" name="option_art[]" value="' . $row['co_art'] . '"></center>',
                        $row['co_art'],
                        $row['ds_art'],
                        $row['va_art'],
                        '<div class="col-xs-12"><b>Fondo:</b> ' . utf8_encode($row['fo_art']) . '</div><div class="col-xs-12"><b>Medida:</b> ' . utf8_encode($row['me_art']) . '</div><div class="col-xs-12"><b>Profundidad:</b> ' . utf8_encode($row['pr_art']) . '</div>',
                        $cmb_lo,
                        '<span>' . $largo . '</span><input id="la_' . $row['co_art'] . '" name="la_' . $row['co_art'] . '" class="form-control larg_" type="text" onkeypress="return isNumeric(event, this);" value="' . $largo . '" style="width: 80px;"  />',
                        '<span>' . $ancho . '</span><input id="an_' . $row['co_art'] . '" name="an_' . $row['co_art'] . '" class="form-control anch_" type="text" onkeypress="return isNumeric(event, this);" value="' . $ancho . '" style="width: 80px;" />',
                        '<span>' . $alto . '</span><input id="al_' . $row['co_art'] . '" name="al_' . $row['co_art'] . '" class="form-control alto_" type="text" onkeypress="return isNumeric(event, this);" value="' . $alto . '" style="width: 80px;" />',
                        '<span>' . $va_peso . '</span><div class="input-group my-group" style="width: 155px;"> <input id="pe_' . $row['co_art'] . '" name="pe_' . $row['co_art'] . '" class="form-control peso_" type="text" onkeypress="return isNumeric(event, this);" value="' . $va_peso . '" style="width: 80px;" />' . $cmb_pe . '</div></div>',
                        '$0.00',
                        '$0.00',
                        '$0.00',
                        '$0.00',
                        '$0.00'
                    );
                }
            } else {
                $array['co_err'] = $articulosSql['co_err'];
                $array['tx_err'] = $articulosSql['tx_err'];
            }
        }
        echo json_encode($datatable_data);
    }

    private function _consultaArticulosCourier() {

        $articulosMysql = $this->m_3003->_retArtitulosMysql();
        $array = array('co_err' => $articulosMysql['co_err'], 'tx_err' => $articulosMysql['tx_err'], 'data' => null);

        if ($articulosMysql['co_err'] == 0) {
            $articulosSql = $this->m_3003->_retArtitulosSql($articulosMysql);
//            print_r($articulosSql);
//            die;
            if ($articulosSql['co_err'] == 0) {
                $datatable_data = null;
                $datatable_columns = array(
                    array('title' => '<input name="ck_art" id="ck_art" type="checkbox">', 'data' => 'html_se'),
                    array('title' => 'C&oacute;digo', 'data' => 'co_art'),
                    array('title' => 'Descripci&oacute;n', 'data' => 'ds_art'),
                    array('title' => 'PVP', 'data' => 'va_art', 'className' => "text-right"),
                    array('title' => 'Ficha Tecnica', 'data' => 'fi_html'),
                    array('title' => 'U. Medida', 'data' => 'html_ul'),
                    array('title' => 'Largo', 'data' => 'la_html'),
                    array('title' => 'Ancho', 'data' => 'an_html'),
                    array('title' => 'Alto', 'data' => 'al_html'),
                    array('title' => 'Peso', 'data' => 'pe_html'),
                    array('title' => 'Local', 'data' => 'lo_cal', 'className' => "text-right"),
                    array('title' => 'Principal', 'data' => 'pr_inc', 'className' => "text-right"),
                    array('title' => 'Especial', 'data' => 'es_pec', 'className' => "text-right"),
                    array('title' => 'Oriente', 'data' => 'or_ien', 'className' => "text-right"),
                    array('title' => 'Gal&aacute;pagos', 'data' => 'ga_lap', 'className' => "text-right")
                );

                foreach ($articulosSql['data'] as $row) {

                    $dimension = !empty($row['di_art']) ? json_decode($row['di_art'], true) : null;
                    $largo = isset($dimension['LARGO']) ? $dimension['LARGO'] : '';
                    $ancho = isset($dimension['ANCHO']) ? $dimension['ANCHO'] : '';
                    $alto = isset($dimension['ALTO']) ? $dimension['ALTO'] : '';
                    $medida = isset($dimension['UNIDAD']) ? $dimension['UNIDAD'] : '';

                    $peso = !empty($row['pe_art']) ? explode(' ', $row['pe_art']) : null;
                    $va_peso = isset($peso[0]) ? $peso[0] : '';
                    $cmb_peso = isset($peso[1]) ? $peso[1] : '';

                    $array_peso = array(
                        'g' => 'g = GRAMO',
                        'lb' => 'lb = LIBRA',
                        'kg' => 'kg = KILOGRAMO',
                    );

                    $array_longitud = array(
                        'mm' => 'mm = MILIMETRO',
                        'cm' => 'cm = CENTIMETRO',
                        'dm' => 'dm = DECIMETRO',
                        'm' => 'm = METRO',
                    );

                    $cmb_pe = form_dropdown('cmb_pe_' . $row['co_art'], $array_peso, $cmb_peso, "id='cmb_pe_" . $row['co_art'] . "' name='cmb_pe_" . $row['co_art'] . "' class='selectpicker form-control cmb_pe_' style='width: 65px;' ");
                    $cmb_lo = form_dropdown('cmb_lo_' . $row['co_art'], $array_longitud, $medida, "id='cmb_lo_" . $row['co_art'] . "' name='cmb_lo_" . $row['co_art'] . "' class='form-control cmb_lo_' style='width: 72px;' ");

                    $datatable_data[] = array(
                        'DT_RowId' => 'ca_' . $row['co_art'],
                        'co_art' => $row['co_art'],
                        'ds_art' => $row['ds_art'],
                        'va_art' => $row['va_art'],
                        'fi_html' => '<div class="col-xs-12"><b>Fondo:</b> ' . utf8_encode($row['fo_art']) . '</div><div class="col-xs-12"><b>Medida:</b> ' . utf8_encode($row['me_art']) . '</div><div class="col-xs-12"><b>Profundidad:</b> ' . utf8_encode($row['pr_art']) . '</div>',
                        'la_html' => '<span>' . $largo . '</span><input id="la_' . $row['co_art'] . '" name="la_' . $row['co_art'] . '" class="form-control larg_" type="text" onkeypress="return isNumeric(event, this);" value="' . $largo . '" style="width: 80px;"  />',
                        'an_html' => '<span>' . $ancho . '</span><input id="an_' . $row['co_art'] . '" name="an_' . $row['co_art'] . '" class="form-control anch_" type="text" onkeypress="return isNumeric(event, this);" value="' . $ancho . '" style="width: 80px;" />',
                        'al_html' => '<span>' . $alto . '</span><input id="al_' . $row['co_art'] . '" name="al_' . $row['co_art'] . '" class="form-control alto_" type="text" onkeypress="return isNumeric(event, this);" value="' . $alto . '" style="width: 80px;" />',
                        'pe_html' => '<span>' . $va_peso . '</span><div class="input-group my-group" style="width: 155px;"> <input id="pe_' . $row['co_art'] . '" name="pe_' . $row['co_art'] . '" class="form-control peso_" type="text" onkeypress="return isNumeric(event, this);" value="' . $va_peso . '" style="width: 80px;" />' . $cmb_pe . '</div></div>',
                        'lo_cal' => '$0.00',
                        'pr_inc' => '$0.00',
                        'es_pec' => '$0.00',
                        'or_ien' => '$0.00',
                        'ga_lap' => '$0.00',
                        'html_b' => '<button type="button" class="btn btn-circle btn-success"><i class="text-blanco fa fa-save"></i></button>',
                        'html_ul' => $cmb_lo,
                        'html_se' => '<center><input id="btn_check_art" type="checkbox" name="option_art[]" value="' . $row['co_art'] . '"></center>'
                    );
                }

                $btn = '<a class="btn btn-success" href="' . site_url('inventario/co_3003/geExc') . '" target="_blank" style="position: absolute; top: 15px; right: 122px;"><i class="fa fa-file-excel-o"></i> Excel</a>';
                $btn.= '<button type="button" class="btn btn-danger" onclick="sav_art();" style="position: absolute; top: 15px; right: 15px;"><i class="fa fa-save"></i> Guardar</button>';

                $array['data'] = array(
                    'data' => $datatable_data,
                    'columns' => $datatable_columns,
                    'btn' => $btn
                );
            } else {
                $array['co_err'] = $articulosSql['co_err'];
                $array['tx_err'] = $articulosSql['tx_err'];
            }
        }

        echo json_encode($array);
    }

    function _procesarArticulos() {
        $data = $this->input->post('s_s');
        $data = json_decode($data, true);
        $array = $this->m_3003->_actualizaArticulos($data);
        echo json_encode($array);
    }

}
