<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ConsultaArticulo extends CI_Controller{

	function __construct(){
		parent::__construct();
    $this->load->model('inventario/m_ConsultaArticulo');
    session_start();
  }

  public function _remap(){

    validaSession('inventario/ConsultaArticulo'); 
    $tipo=$this->uri->segment(3);

    if ($tipo == 'retGrupo'){
      $this->_retGrupo();
    } elseif ($tipo == 'retEstructCom') {
      $this->_retEstructCom();
    }elseif ($tipo == 'retFiltros2') {
      $this->_retFiltros2();
    }elseif ($tipo == 'retConsArt') {
      $this->_retConsArt();
    }elseif ($tipo == 'retAdelante') {
      $this->_retAdelante();
    }elseif ($tipo == 'retAnterior') {
      $this->_retAnterior();
    }elseif ($tipo == 'retRotacion') {
      $this->_retRotacion();
    }elseif ($tipo == 'retTablaSimilares') {
      $this->_retTablaSimilares();
    }elseif ($tipo == 'retBuscarSimilares') {
      $this->_retBuscarSimilares();
    }elseif ($tipo == 'retTablaFicha') {
      $this->_retTablaFicha();
    }elseif ($tipo == 'retTablaPercha') {
      $this->_retTablaPercha();
    }elseif ($tipo == 'retTablaForecast') {
      $this->_retTablaForecast();
    }elseif ($tipo == 'retTablaFactura') {
      $this->_retTablaFactura();
    }elseif ($tipo == 'retTablaPedido') {
      $this->_retTablaPedido();
    }elseif ($tipo == 'retTablaDetallePedido') {
      $this->_retTablaDetallePedido();
    }elseif ($tipo == 'retTablaMovimiento') {
      $this->_retTablaMovimiento();
    }elseif ($tipo == 'retTablaHisMov') {
      $this->_retTablaHisMov();
    }elseif ($tipo == 'retTablaXlsCostoVentasPvp') {
      $this->_retTablaXlsCostoVentasPvp();
    }elseif ($tipo == 'genMovPDF') {
      $this->_genMovPDF();
    }elseif ($tipo == 'genIngFacPDF') {
      $cf=$this->uri->segment(4);
      $pv=$this->uri->segment(5);
      $this->_genIngFacPDF($cf,$pv);
    }else {
      $this->_index();
    }
  }

  private function _index(){
    $data=null; 
    $data['permisos']='';   
    $urlControlador = 'inventario/ConsultaArticulo';
    $data['urlControlador'] = $urlControlador;
    $fchini = $this->m_ConsultaArticulo->retMinFechaIngresoFactura();
    $data['fIniFact'] = date('Y-m-d', strtotime($fchini));
    $data['fFinFact'] = date('Y-m-d', strtotime('+0 day'));
    $data['fIniPed']  = date('Y-m-d', strtotime('-30 day'));//-30 day
    $data['fFinPed']  = date('Y-m-d', strtotime('+30 day'));//+30 day
    $data['fIniMov']  = date('Y-m-d', strtotime('-90 day'));

    $data['css'] = array(
      'js/plugins/tab/tab.css',
      'css/datatables/dataTables.bootstrap.css',
      'css/datetimepicker/jquery.datetimepicker.css',
      'css/bootstrap-checkbox-radio/checkbox-radio.css',
      'js/plugins/perfect-scrollbar/css/perfect-scrollbar.css');


    $data['js']=array('js/jquery-1.10.2.min.js',
      'js/general.js',
      'js/plugins/tab/tab.js',
      'js/plugins/datatables/dataTables.bootstrap.js',
      'js/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
      'js/inventario/j_consultaarticulo.js',
      'js/plugins/datatables/jquery.dataTables.js',
      'js/plugins/datetimepicker/jquery.datetimepicker.js',
      'js/bootstrap.min.js');

    $bodegas    = $this->m_ConsultaArticulo->retBodegasAuth();
    $cmb_almacen_ped = null;
    $cmb_almacen_ped['0'] = ' TODOS ';
    if (!empty($bodegas)) {
      foreach ($bodegas as $row) {
        $cmb_almacen_ped[$row['co_bodega']] = $row['ds_bodega'];
      }
    }

    $data['cmb_almacen_ped'] = form_dropdown('cmb_almacen_ped', $cmb_almacen_ped, '', "class='form-control text-yellow2 cbPedAlm'");
    $data['cmb_almacen_mov'] = form_dropdown('cmb_almacen_mov', $cmb_almacen_ped, '', "class='form-control text-green2 cbMovAlm'");

    $movimiento = $this->m_ConsultaArticulo->retTipoMovimiento(0);
    $cmb_tipo_mov = null;
    $cmb_tipo_mov['0'] = ' TODOS ';
    if (!empty($movimiento)) {
      foreach ($movimiento as $row) {
        $cmb_tipo_mov[$row['co_tipo']] = $row['ds_descripcion'];
      }
    }

    $data['cmb_tipo_mov'] = form_dropdown('cmb_tipo_mov', $cmb_tipo_mov, '', "class='form-control text-green2 cbMovTip'");

    $this->m_ConsultaArticulo->getEstructComercial();  
    $this->load->view('barras/header',$data);
    $this->load->view('barras/menu');
    $this->load->view('inventario/v_ConsultaArticulo');
    $this->load->view('barras/footer');
  }

  private function _retFiltros2(){

    $strBus = $this->input->post('c_vl');
    $strBus = str_replace("||","%",$strBus);
    $datos = array( 
                    "c_tipo"=>$this->input->post('c_tp'),
                    "c_valr"=>$strBus,
                    "c_anti"=>$this->input->post('c_an'),
                    "c_filt"=>$this->input->post('c_fl')
                  );
    $mensaje   = $this->m_ConsultaArticulo->retFiltros2($datos);
    echo($mensaje);
  }

  private function _retEstructCom(){
    $datos = array(
                    "c_grupo"=>$this->input->post('c_gr'),
                    "c_linea"=>$this->input->post('c_li'),
                    "c_subli"=>$this->input->post('c_sl'),
                    "c_categ"=>$this->input->post('c_ca'),
                    "c_subca"=>$this->input->post('c_sc')
                  );
    $mensaje   = $this->m_ConsultaArticulo->retECArray($datos);             
    echo($mensaje);
  }
  private function _retConsArt(){
    $datos = $this->input->post('c_cd');
    $mensaje   = $this->m_ConsultaArticulo->retConsArt($datos);             
    echo($mensaje);
  }
  private function _retAdelante(){
    $datos = array( 
                    "c_tipbus"=>$this->input->post('c_tb'),
                    "c_cdart" =>$this->input->post('c_ca'),
                    "c_cdlik" =>$this->input->post('c_cl'),
                    "c_dsart" =>$this->input->post('c_da'),
                    "c_cdfab" =>$this->input->post('c_cf'),
                    "c_cdupc" =>$this->input->post('c_cu'),
                    "c_escom" =>$this->input->post('c_ec'),
                    "c_antig" =>$this->input->post('c_an'),
                    "c_filtr" =>$this->input->post('c_fl')
                  );
    $mensaje   = $this->m_ConsultaArticulo->retAdelante($datos);             
    echo($mensaje);
  }
  private function _retAnterior(){
    $datos = array(
                    "c_tipbus"=>$this->input->post('c_tb'),
                    "c_cdart" =>$this->input->post('c_ca'),
                    "c_cdlik" =>$this->input->post('c_cl'),
                    "c_dsart" =>$this->input->post('c_da'),
                    "c_cdfab" =>$this->input->post('c_cf'),
                    "c_cdupc" =>$this->input->post('c_cu'),
                    "c_escom" =>$this->input->post('c_ec'),
                    "c_antig" =>$this->input->post('c_an'),
                    "c_filtr" =>$this->input->post('c_fl')
                  );
    $mensaje   = $this->m_ConsultaArticulo->retAnterior($datos);             
    echo($mensaje);
  }
  private function _retRotacion(){
    $datos = array( "c_cd"=>$this->input->post('c_cd'));
    $mensaje   = $this->m_ConsultaArticulo->retRotacion($datos);             
    echo($mensaje);
  }

  private function _retTablaSimilares(){
    $ec = $this->input->post('c_ec');
    $ec = str_replace("WHERE","",$ec);
    $datos = array(
                    "c_tipbus"=>$this->input->post('c_tb'),
                    "c_cdart"=>$this->input->post('c_ca'),
                    "c_dsart"=>$this->input->post('c_da'),
                    "c_cdfab"=>$this->input->post('c_cf'),
                    "c_cdupc"=>$this->input->post('c_cu'),
                    "c_escom"=>$ec,
                    "c_filtr"=>$this->input->post('c_fl')
                  );
    $mensaje   = $this->m_ConsultaArticulo->retTablaSimilares($datos);       
    echo($mensaje);
  }

  private function _retBuscarSimilares(){
    $strBus = $this->input->post('c_bu');
    $strBus = str_replace("||","%",$strBus);
    $datos  = $strBus;
    $mensaje   = $this->m_ConsultaArticulo->retBuscarSimilares($datos);             
    echo($mensaje);
  }

  private function _retTablaFicha(){
    $datos   = $this->input->post('c_ca');
    $mensaje = $this->m_ConsultaArticulo->retTablaFicha($datos);       
    echo($mensaje);
  }

  private function _retTablaPercha(){
    $datos   = $this->input->post('c_ca');
    $mensaje = $this->m_ConsultaArticulo->retTablaPercha($datos);       
    echo($mensaje);
  }

  private function _retTablaForecast(){
    $datos   = $this->input->post('c_ca');
    $mensaje = $this->m_ConsultaArticulo->retTablaForecast($datos);       
    echo($mensaje);
  }

  private function _retTablaFactura(){
    $strBus = $this->input->post('c_cf');
    $strBus = str_replace("||","%",$strBus);
    $datos  = array(
                      "c_codfac"=>$strBus,
                      "c_codart"=>$this->input->post('c_ca'),
                      "c_desde"=>$this->input->post('c_fd'),
                      "c_hasta"=>$this->input->post('c_fh')
                    );
    $mensaje   = $this->m_ConsultaArticulo->retTablaFactura($datos);       
    echo($mensaje);
  }

  private function _retTablaPedido(){
    $datos  = array(
                      "c_codbod"=>$this->input->post('c_cb'),
                      "c_codart"=>$this->input->post('c_ca'),
                      "c_fdesde"=>$this->input->post('c_fd'),
                      "c_fhasta"=>$this->input->post('c_fh'),
                      "c_condic"=>$this->input->post('c_cn')
                    );
    $mensaje   = $this->m_ConsultaArticulo->retTablaPedido($datos);       
    echo($mensaje);
  }

  private function _retTablaDetallePedido(){
    $datos  = array(
                      "c_codped"=>$this->input->post('c_cp'),
                      "c_bodent"=>$this->input->post('c_be'),
                      "c_bodsal"=>$this->input->post('c_bs'),
                      "c_pickin"=>$this->input->post('c_pk')
                    );
    $mensaje   = $this->m_ConsultaArticulo->retTablaDetallePedido($datos);       
    echo($mensaje);
  }

  private function _retTablaMovimiento(){
    $datos  = array(
                      "c_codart"=>$this->input->post('c_ca'),
                      "c_fdesde"=>$this->input->post('c_fd'),
                      "c_fhasta"=>$this->input->post('c_fh'),
                      "c_condic"=>$this->input->post('c_cn')
                    );
    $mensaje   = $this->m_ConsultaArticulo->retTablaMovimiento($datos);       
    echo($mensaje);
  }

  private function _retTablaHisMov(){
    $codigo   = $this->input->post('c_ca');
    $mensaje  = $this->m_ConsultaArticulo->retTablaHistMov($codigo);       
    echo($mensaje);
  }

  private function _retTablaXlsCostoVentasPvp(){
    $datos  = array(
                      "c_fdesde"=>$this->input->post('c_fd'),
                      "c_fhasta"=>$this->input->post('c_fh'),
                      "c_codart"=>$this->input->post('c_ca'),
                      "c_codbod"=>$this->input->post('c_cb'),
                      "c_codcaj"=>$this->input->post('c_cc'),
                      "c_tipmov"=>$this->input->post('c_tm')
                    );
    $mensaje   = $this->m_ConsultaArticulo->retTablaXlsCostoVentasPvp($datos);       
    echo($mensaje);
  }

  private function _genMovPDF(){
    $fpp = 0 ;
    $params = json_decode($_POST['parameter']); 
    $datos  = (array)$params[0];
    $histo  = $datos['c_histor']; 
    $mov1   = $this->m_ConsultaArticulo->retArrayMovimiento($datos);  

    if($histo=='1'){
      $codigo = $datos['c_codart']; 
      $mov2   = $this->m_ConsultaArticulo->retArrayHistMov($codigo);   
      $data   = array_merge($mov1,$mov2);
    }else{
      $data   = $mov1; 
    }

    if (!empty($data)) { 
         $this->load->library('pdf');
         $pdf = new PDF();
         $pdf->fpdf('L','mm','A4');
         foreach ($data as $row) { 
            if($fpp==0 || $fpp==39){
              $pdf->SetMargins(5,5,5,5);
              $pdf->AliasNbPages();
              $pdf->setHeader('1.1.1');

              $pdf->AddPage();
              $pdf->SetFillColor(255,255,255);
              $pdf->SetDrawColor(0,0,0);
              $pdf->SetTextColor(0,0,0);
              $pdf->Ln(6); 
              
              $pdf->SetFont('helvetica', 'B', 12);
              $pdf->Cell(285, 8,'Movimientos de '.$datos['c_codart'].' desde '.$datos['c_fdesde'].' hasta '.$datos['c_fhasta'],0, 0,'C','false');

              $pdf->Ln(8); 
              $pdf->SetFont('helvetica', 'B', 10);
              $pdf->Cell(10, 8,'Tipo','TB', 0,'C','false');
              $pdf->Cell(35, 8,'Movimiento','TB', 0,'C','false');
              $pdf->Cell(10, 8,'Cant.','TB', 0,'C','false');
              $pdf->Cell(20, 8,'Fecha','TB', 0,'C','false');
              $pdf->Cell(25, 8,'Caja/Doc.','TB', 0,'L','false');
              $pdf->Cell(35, 8,'Entra','TB', 0,'L','false');
              $pdf->Cell(35, 8,'Sale','TB', 0,'L','false');
              $pdf->Cell(15, 8,'Usuario','TB', 0,'C','false');
              $pdf->Cell(70, 8,'Nombre Usuario','TB', 0,'C','false');
              $pdf->Cell(30, 8,'Cargo','TB', 0,'C','false');
              $pdf->Ln(8);  
              $fpp = 0;
            }
            $pdf->SetFont('helvetica', '', 7);
            $pdf->Cell(10, 4,trim($row['co_tipo_mov']),'0', 0,'C','false');
            $pdf->Cell(35, 4,trim($row['ds_tipo_mov']),'0', 0,'L','false');
            $pdf->Cell(10, 4,trim($row['cantidad']),'0', 0,'R','false');
            $pdf->Cell(20, 4,trim($row['fe_movto']),'0', 0,'C','false');
            $pdf->Cell(25, 4,trim($row['co_nota']),'0', 0,'L','false');
            $pdf->Cell(35, 4,trim($row['bode_entra']),'0', 0,'L','false');
            $pdf->Cell(35, 4,trim($row['bode_sale']),'0', 0,'L','false');
            $pdf->Cell(15, 4,trim($row['co_usuario']),'0', 0,'C','false');
            $pdf->Cell(70, 4,utf8_decode(trim($row['tx_nombre'])),'0', 0,'L','false');
            $pdf->Cell(30, 4,trim($row['tx_cargo']),'0', 0,'L','false');
            $pdf->Ln(4);  
            $fpp = $fpp +1;
         }
         unset($data);
         $pdf->Output();
     }else {
         echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
         echo " <script> window.close();</script> ";
     }
  }

  private function _genIngFacPDF($cf,$pv){

      $fpp = 1 ;
      $data   = $this->m_ConsultaArticulo->retTablaIngFacturas($cf,$pv);
      if (!empty($data)) { 
         $this->load->library('pdf');
         $pdf = new PDF();
         $pdf->fpdf('P','mm','A4');
         foreach ($data as $row) { 
            if($fpp==1 || $fpp==26){
                $pdf->SetMargins(8,5,5,5);
                $pdf->AliasNbPages();
                $pdf->setHeader('1.1.1');

                $pdf->AddPage();
                $pdf->SetFillColor(255,255,255);
                $pdf->SetDrawColor(0,0,0);
                $pdf->SetTextColor(0,0,0);
                $pdf->Ln(6); 
                
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->Cell(190, 8,'INGRESO DE FACTURAS','0', 0,'C','false');
                $pdf->Ln(8); 

                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(30, 6,'Factura:','0', 0,'L','false');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(50, 6,trim($row['co_factura']),'0', 1,'L','false');

                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(30, 6,'Proveedor:','0', 0,'L','false');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(50, 6,trim($row['nomb_proveedor']),'0', 1,'L','false');

                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(30, 6,'Moneda:','0', 0,'L','false');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->Cell(50, 6,trim($row['nomb_mone']),'0', 1,'L','false');

                $pdf->Cell(185, 8,'','T', 0,'C','false');
                $pdf->Ln(1); 
                $pdf->SetFont('helvetica', 'B', 10);
                $pdf->Cell(10, 8,'#','0', 0,'R','false');
                $pdf->Cell(14, 8,utf8_decode('Código'),'0', 0,'C','false');
                $pdf->Cell(21, 8,utf8_decode('Fábrica'),'0', 0,'R','false');
                $pdf->Cell(50, 8,utf8_decode('Artículo'),'0', 0,'C','false');
                $pdf->Cell(10, 8,'Cant.','0', 0,'R','false');
                $pdf->Cell(20, 8,'FOB','0', 0,'R','false');
                $pdf->Cell(20, 8,'Sub. FOB','0', 0,'R','false');
                $pdf->Cell(20, 8,'Costo','0', 0,'R','false');
                $pdf->Cell(20, 8,'Subtotal','0', 0,'R','false');
                
                $pdf->Ln(8);  
                $pdf->Cell(185, 8,'','T', 0,'C','false');
                $pdf->Ln(1);  
              }
            $pdf->SetFont('helvetica', '', 7);
            $pdf->Cell(10, 4,$fpp,'0', 0,'R','false');
            $pdf->Cell(14, 4,trim($row['co_articulo']),'0', 0,'C','false');
            $pdf->Cell(21, 4,trim($row['co_fabrica']),'0', 0,'R','false');
            $pdf->Cell(50, 4,trim($row['ds_articulo']),'0', 0,'L','false');
            $pdf->Cell(10, 4,trim($row['cantidad']),'0', 0,'R','false');
            $pdf->Cell(20, 4,trim($row['costo_m_e']),'0', 0,'R','false');
            $pdf->Cell(20, 4,trim($row['sb_costo_m_e']),'0', 0,'R','false');
            $pdf->Cell(20, 4,trim($row['costo_suc']),'0', 0,'R','false');
            $pdf->Cell(20, 4,trim($row['sb_costo_suc']),'0', 0,'R','false');
            $pdf->Ln(4);
            $fpp = $fpp + 1;
         }
         $pdf->Cell(185, 8,'','T', 0,'C','false');
         $pdf->Ln(4);  
         $pdf->SetFont('helvetica', 'B', 10);
         $pdf->Cell(125, 8,'Total Factura','0', 0,'L','false');
         $pdf->Cell(20, 8,$data[(sizeof($data)-1)]['sm_costo_m_e'],'TB', 0,'R','false');
         $pdf->Cell(20, 8,'','0', 0,'R','false');
         $pdf->Cell(20, 8,$data[(sizeof($data)-1)]['sm_costo_suc'],'TB', 0,'R','false');
         $pdf->Ln(8);  
         $pdf->Cell(45, 8,'Factor (pvp/costo):','0', 0,'L','false');
         $pdf->Cell(15, 8,$data[(sizeof($data)-1)]['sm_factor'],'LRTB', 0,'R','false');
         unset($data);
         $pdf->Output();
      }else{
        echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
        echo " <script> window.close();</script> ";
      }
  }
  
}