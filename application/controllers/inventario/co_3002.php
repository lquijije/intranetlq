<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_3002 extends CI_Controller {	
	
    function __construct() {	
        parent::__construct();	
        $this->load->helper(array('form'));
        $this->load->model(array('inventario/m_3001'));
        session_start();
    }	

    public function _remap()  {    
        
        $tipo = $this->uri->segment(3);
        validaSession('inventario/co_3002');  
        
        $datos = $this->input->post();
        fnc_post($tipo,$datos,'inventario/co_3002');

        if ($tipo == 'reArt'){
            $this->_conArticulo();
        } else {
            $this->_crud();
        }
        
    }
        
    private function _crud(){
        
        $data = null; 
        $urlControlador = 'inventario/co_3002';
        $data['urlControlador'] = $urlControlador;
        
        $data['js'] = array('js/jquery-1.10.2.min.js',
                            'js/bootstrap.min.js',
                            'js/config.js',
                            'js/general.js',
                            'js/inventario/j_3002.js');
        
        $this->load->view('barras/header',$data);
        $this->load->view('barras/menu');
        $this->load->view('inventario/v_3002');
        $this->load->view('barras/footer');
    }

    private function _conArticulo(){
        
        $cod_art = $this->input->post('c_a');
        $data = $this->m_3001->_retArtitulo($cod_art); 
        $string = '';
        
        if(!empty($data['data'])){
            
            $string.= '<table class="table table-bordered">';
                $string.= '<tbody>';
                    $string.= '<tr>';
                        $string.= '<td><b>C&oacute;digo:</b></td>';
                        $string.= '<td>'.$data['data']['cod_art'].'</td>';
                    $string.= '</tr>';
                    $string.= '<tr>';
                        $string.= '<td><b>Descripc&oacute;n:</b></td>';
                        $string.= '<td>'.$data['data']['des_art'].'</td>';
                    $string.= '</tr>';
                    $string.= '<tr>';
                        $string.= '<td><b>UPC:</b></td>';
                        $string.= '<td>'.trim($data['data']['cod_upc']).'</td>';
                    $string.= '</tr>';
                    $string.= '<tr>';
                        $string.= '<td><b>Imagen:</b></td>';
                        $string.= '<td class="text-center"><img src="'.site_url("lib/loadImg.php?codigo=".trim($data['data']['cod_art'])).'" style="height: 150px;"></td>';
                    $string.= '</tr>';
                $string.= '</tbody>';
            $string.= '</table>';
            $string.= '<input type="hidden" id="co_art" name="co_art" value="'.trim($data['data']['cod_art']).'">';
            $data['data'] = $string;

        }
        
        echo json_encode($data);
    }
    
}