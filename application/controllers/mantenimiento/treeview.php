<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class treeview extends CI_Controller {	
	
	function __construct()
	{		
		parent::__construct();	
                $this->load->model(array('mantenimiento/administracion/m_101'));
                session_start();
        }	
        
         public function _remap($method)
        
        {    
            $this->_treview();
        }
        
        private function  _treview() {
        require 'KoolControls/KoolTreeView/kooltreeview.php'; 
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        $treearr =$this->m_101->menu();
        
        
        array ("support","update","Update",false);
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$_node_template = "<input type='checkbox' id='cb_{id}' name='cb_{name}' {check} onclick='toogle(\"{id}\")'><label for='cb_{id}' >{text}</label>";
	
	$treeview = new KoolTreeView("treeview");
	$treeview->scriptFolder = site_url().'KoolControls/KoolTreeView';
	$treeview->imageFolder=   site_url().'KoolControls/KoolTreeView/icons';
	$treeview->styleFolder = "default";
	$root = $treeview->getRootNode();
	
	$root->text = str_replace("{id}","treeview.root",$_node_template);
	$root->text = str_replace("{name}","treeview_root",$root->text);
	$root->text = str_replace("{text}","Menu",$root->text);
	$root->text = str_replace("{check}",isset($_POST["cb_treeview_root"])?"checked":"",$root->text);
	$root->expand=true;
        $tamanio=count($treearr);
	for ( $i = 0 ; $i <$tamanio  ; $i++)
	{
		$_text = str_replace("{id}",$treearr[$i][1],$_node_template);
		$_text = str_replace("{name}",$treearr[$i][1],$_text);
		$_text = str_replace("{text}",$treearr[$i][2],$_text);
		$_text = str_replace("{check}",isset($_POST["cb_".$treearr[$i][1]])?"checked":"",$_text);
		$treeview->Add($treearr[$i][0],$treearr[$i][1],$_text,$treearr[$i][3]);
	}
	$treeview->showLines = true;
	$treeview->selectEnable = false;
	$treeview->keepState = "onpage";	
        ?>
 
        <form id="form1" method="post">
                <div style="padding:10px;">
                        <?php echo $treeview->Render();?>
                </div>

                <script language="javascript">

                        function markChild(nodeId)
                        {
                                var status = document.getElementById("cb_" + nodeId ).checked;
                                var childIds = treeview.getNode(nodeId).getChildIds();	
                                for(var i in childIds)
                        {
                                        document.getElementById("cb_" + childIds[i] ).checked = status;
                                        markChild(childIds[i]);	
                        }		
                        }

                        function markChildConsulta(nodeId)
                        {
                                var status = document.getElementById("cb_" + nodeId ).checked;
                                var childIds = treeview.getNode(nodeId).getChildIds();	
                                for(var i in childIds)
                        {
                                        document.getElementById("cb_" + childIds[i] ).checked = status;
                                        markParentConsulta(childIds[i]);	                                
                                        markChildConsulta(childIds[i]);	
                        }		
                        }

                        function markParentConsulta(nodeId) {   
                           if (nodeId.indexOf(".root")<0) {
                                var parentId = treeview.getNode(nodeId).getParentId();
                                var siblingIds = treeview.getNode(parentId).getChildIds();
                                var _parentSelected = false;
                                for(var i in siblingIds)
                                        if (document.getElementById("cb_" + siblingIds[i]).checked) 
                                                _parentSelected = true;
                                document.getElementById("cb_" + parentId).checked = _parentSelected;
                                markParentConsulta(parentId);
                            }

                        }
                        function DmarkChild(nodeId,enabledisable)
                        {
                                var childIds = treeview.getNode(nodeId).getChildIds();	
                                for(var i in childIds)
                        {
                                        document.getElementById("cb_" + childIds[i] ).disabled = enabledisable;
                                        DmarkChild(childIds[i],enabledisable);	
                        }		
                        }

                        function DmarkParent(nodeId,enabledisable)
                        {
                                if (nodeId.indexOf(".root")<0)
                                {
                                        var parentId = treeview.getNode(nodeId).getParentId();
                                        var siblingIds = treeview.getNode(parentId).getChildIds();
                                        for(var i in siblingIds)
                                                if (!document.getElementById("cb_" + siblingIds[i]).disabled!==enabledisable) 
                                                        _parentSelected = false;
                                        document.getElementById("cb_" + parentId).disabled = enabledisable;
                                        DmarkParent(parentId,enabledisable);
                                }		

                        }                

                        function toogle(nodeId)
                        {
                                markParentConsulta(nodeId);
                                markChild(nodeId)
                                //alert($("#treview_valor").val());                        
                        }
                        function toogleCarga(nodeId)
                        {
                                markChildConsulta(nodeId);
                                //markParentConsulta(nodeId);
                        }   
                        function Deshabilitatoogle(nodeId,enabledisable) {
                               document.getElementById("cb_" + nodeId).disabled = enabledisable;                
                               DmarkChild(nodeId,enabledisable);
                               DmarkParent(nodeId,enabledisable);// 

                        }                

                </script>


                </form>  
            
        <?php
       }

}
