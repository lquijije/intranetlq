<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_102 extends CI_Controller {	
	
    	function __construct()
    	{		
    		parent::__construct();	
    		$this->load->helper(array('form'));
            $this->load->library(array('form_validation','encrypt'));
            $this->load->model(array('mantenimiento/administracion/m_102','mantenimiento/administracion/m_101'));
            session_start();
        }	
        
        public function _remap() {
            
            validaSession('mantenimiento/administracion/co_102');     
            $tipo=$this->uri->segment(4);
            
            if($tipo == 'guardar'){
                $this->_guardar();
            } else if ($tipo == 'consulta'){
                $this->_consulta();
            } else if ($tipo == 'consulta_menuPerfil'){
                $this->_consulta_menuPerfil();
            } else if ($tipo == 'getTreeview'){
                $this->_getTreeview();
            } else if ($tipo == 'getUsers'){
                $this->_getUsers();
            } else if ($tipo == 'retEmpleado'){
                $this->_retEmpleado();
            } else if ($tipo == 'retResponsable'){
                $this->_retResponsable();
            } else {
                $this->_crud(); 
            }
        }        
        
        private function _crud(){
            $data=null; 
            $data['permisos']='';   
            $urlControlador = 'mantenimiento/administracion/co_102';
            $data['urlControlador'] = $urlControlador;
            
            $data['css'] = array('css/datatables/dataTables.bootstrap.css');
            
            $data['js']=array('js/jquery-1.10.2.min.js',
                              'js/general.js',
                              'js/mantenimiento/administracion/j_102.js',
                              'js/plugins/datatables/jquery.dataTables.js',
                              'js/plugins/datatables/dataTables.bootstrap.js',
                              'js/bootstrap.min.js');
            
            $data['idFormulario'] = 'co_102';
            $perfiles = $this->m_102->getPerfil();
            $data['perfiles'] = $perfiles;
            
            require 'KoolControls/KoolTreeView/kooltreeview.php'; 
            $treearr =$this->m_102->menu(9);
            
            array ("support","update","Update",false);
            $_node_template = "<label for='cb_{id}'>{text}</label>";
            $treeview = new KoolTreeView("treeview");
            $treeview->scriptFolder = site_url().'KoolControls/KoolTreeView';
            $treeview->imageFolder = site_url().'KoolControls/KoolTreeView/icons';
            $treeview->styleFolder = "default";
            $root = $treeview->getRootNode();

            $root->text = str_replace("{id}","treeview.root",$_node_template);
            $root->text = str_replace("{name}","treeview_root",$root->text);
            $root->text = str_replace("{text}","Menu",$root->text);
            $root->text = str_replace("{check}",isset($_POST["cb_treeview_root"])?"checked":"",$root->text);
            $root->expand=true;
            $tamanio=count($treearr);
            for ( $i = 0 ; $i <$tamanio; $i++) {
                    $_text = str_replace("{id}",$treearr[$i][1],$_node_template);
                    $_text = str_replace("{name}",$treearr[$i][1],$_text);
                    $_text = str_replace("{text}",$treearr[$i][2],$_text);
                    $_text = str_replace("{check}",isset($_POST["cb_".$treearr[$i][1]])?"checked":"",$_text);
                    $treeview->Add($treearr[$i][0],$treearr[$i][1],$_text,$treearr[$i][3]);
            }
            $treeview->showLines = true;
            $treeview->selectEnable = false;
            $treeview->keepState = "onpage";
            $data['treeview']=$treeview->Render();
            $data['treearr']=$treearr;
           
            $this->load->view('barras/header',$data);
            $this->load->view('barras/menu');
            $this->load->view('mantenimiento/administracion/v_102');
            $this->load->view('barras/footer');            
        }
        
        private function _getTreeview(){
            
            require 'KoolControls/KoolTreeView/kooltreeview.php'; 
            $id =$this->input->post('id');
            $treearr =$this->m_102->menu($id);
            array ("support","update","Update",false);
            $_node_template = "<label for='cb_{id}'>{text}</label>";
            $treeview = new KoolTreeView("treeview");
            $treeview->scriptFolder = site_url().'KoolControls/KoolTreeView';
            $treeview->imageFolder=   site_url().'KoolControls/KoolTreeView/icons';
            $treeview->styleFolder = "default";
            $root = $treeview->getRootNode();

            $root->text = str_replace("{id}","treeview.root",$_node_template);
            $root->text = str_replace("{name}","treeview_root",$root->text);
            $root->text = str_replace("{text}","Menu",$root->text);
            $root->text = str_replace("{check}",isset($_POST["cb_treeview_root"])?"checked":"",$root->text);
            $root->expand=true;
            $tamanio=count($treearr);
            for ( $i = 0 ; $i <$tamanio  ; $i++)
            {
                    $_text = str_replace("{id}",$treearr[$i][1],$_node_template);
                    $_text = str_replace("{name}",$treearr[$i][1],$_text);
                    $_text = str_replace("{text}",$treearr[$i][2],$_text);
                    $_text = str_replace("{check}",isset($_POST["cb_".$treearr[$i][1]])?"checked":"",$_text);
                    $treeview->Add($treearr[$i][0],$treearr[$i][1],$_text,$treearr[$i][3]);
                    //echo ",".$treearr[$i][0]."--";                
            }
            $treeview->showLines = true;
            $treeview->selectEnable = false;
            $treeview->keepState = "onpage";
            echo $treeview->Render();
            
        }
        
        private function _getUsers(){
            $Perfiles =$this->m_102->getUsers();
            echo ($Perfiles);
        }

        private function _guardar(){
            
            $datos =$this->input->post();
            $mensaje = $this->m_102->grabar($datos);             
            echo($mensaje);  
            
        }
                   
        private function _consulta(){
                     
            $id= $this->input->post('id');
            if ((!empty($id))){    
                $datos=$this->m_102->retUser(trim($id));
                echo json_encode($datos);
            }   
            
        }
        
        private function _deletePerfil(){
            $datos =$this->input->post();
            $mensaje = $this->m_102->delete($datos);             
            echo($mensaje);
        }
        
        private function _retEmpleado(){
            $cod_emp =$this->input->post('c_p');
            $mensaje = $this->m_102->retEmpleado($cod_emp);             
            echo($mensaje);
        }
        
        private function _retResponsable(){
            $cod_emp =$this->input->post('c_r');
            $mensaje = $this->m_102->retResponsable($cod_emp);             
            echo($mensaje);
        }
        
        
        
}   