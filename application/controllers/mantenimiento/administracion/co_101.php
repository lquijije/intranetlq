<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class co_101 extends CI_Controller {	
	
	function __construct()
	{		
		parent::__construct();	
                $this->load->model(array('mantenimiento/administracion/m_101'));
                session_start();
        }	
        
        public function _remap() {
            
            validaSession('mantenimiento/administracion/co_101');     
            
            $tipo=$this->uri->segment(4);
            
            if($tipo=='guardar'){
                $this->_guardar();
            } else if($tipo=='consulta'){
                $this->_consulta();
            } else if($tipo=='consulta_menuPerfil'){
                $this->_consulta_menuPerfil();
            } else if ($tipo == 'deletePerfil'){
                $this->_deletePerfil();
            } else if ($tipo == 'getPerfiles'){
                $this->_getPerfiles();
            } else {
                $this->_crud(); 
            }
        }        
        
        private function _crud(){
            $data=null; 
            $data['permisos']='';   
            $urlControlador = 'mantenimiento/administracion/co_101';
            $data['urlControlador'] = $urlControlador;
            
            $data['css'] =  array('css/datatables/dataTables.bootstrap.css');
            
            $data['js']=array('js/jquery-1.10.2.min.js',
                              'js/general.js',
                              'js/mantenimiento/administracion/j_101.js',
                              'js/plugins/datatables/jquery.dataTables.js',
                              'js/plugins/datatables/dataTables.bootstrap.js',
                              'js/bootstrap.min.js');
            
            $data['idFormulario'] = 'co_101';
            $widget = $this->general_model->getWidget();
            $data['widget'] = $widget;
            
            ////// treeview  //////////////////////////////////////////////////////////
            require 'KoolControls/KoolTreeView/kooltreeview.php'; 
            $treearr =$this->m_101->menu();
            //echo $treearr;        
            array ("support","update","Update",false);
            $_node_template = "<input type='checkbox' id='cb_{id}'name='cb_{name}' {check} onclick='toogle(\"{id}\")'><label for='cb_{id}'>{text}</label>{link}";
            $treeview = new KoolTreeView("treeview");
            $treeview->scriptFolder = site_url().'KoolControls/KoolTreeView';
            $treeview->imageFolder=   site_url().'KoolControls/KoolTreeView/icons';
            $treeview->styleFolder = "default";
            $root = $treeview->getRootNode();

            $root->text = str_replace("{id}","treeview.root",$_node_template);
            $root->text = str_replace("{name}","treeview_root",$root->text);
            $root->text = str_replace("{text}","Menu",$root->text);
            $root->text = str_replace("{link}","",$root->text);
            $root->text = str_replace("{check}",isset($_POST["cb_treeview_root"])?"checked":"",$root->text);
            $root->expand=true;
            $tamanio=count($treearr);
			
			
            for ( $i = 0 ; $i <$tamanio  ; $i++)
            {
                    $_text = str_replace("{id}",$treearr[$i][1],$_node_template);
                    $_text = str_replace("{name}",$treearr[$i][1],$_text);
                    $_text = str_replace("{text}",$treearr[$i][2],$_text);
                    $_text = str_replace("{link}",isset($treearr[$i][3]) && !empty($treearr[$i][3])?'<a href="javascript:void(0)" class="especialLink" onclick="openEspecialOption('.$treearr[$i][1].')">Opciones: </a><input id="iesp_'.$treearr[$i][1].'" name="iesp_'.$treearr[$i][1].'" type="text" class="form-especialLink">':'',$_text);
                    $_text = str_replace("{check}",isset($_POST["cb_".$treearr[$i][1]])?"checked":"",$_text);
                    $treeview->Add($treearr[$i][0],$treearr[$i][1],$_text,$treearr[$i][3]);
            }

            $treeview->showLines = true;
            $treeview->selectEnable = false;
            $treeview->keepState = "onpage";

            $data['treeview']=$treeview->Render();
            $data['treearr']=$treearr;
            
            $this->load->view('barras/header',$data);
            $this->load->view('barras/menu');
            $this->load->view('mantenimiento/administracion/v_101');
            $this->load->view('barras/footer');            
        }
        
        private function _getPerfiles(){
            $Perfiles =$this->general_model->getPerfiles($_SESSION);
            print_r($Perfiles);
        }

        private function _guardar(){
            
            $datos =$this->input->post();
            $mensaje = $this->m_101->grabar($datos);             
            echo($mensaje);  
            
        }
                   
        private function _consulta(){
                     
            $id= $this->input->post('id');
            
            if ((!empty($id))){    
                $datos=$this->m_101->retPerfilWidget(trim($id));
            }   
                
        }
        
        private function _consulta_menuPerfil(){
            $id= $this->input->post('id');
            
            if ((!empty($id))){  
                $datos=$this->m_101->menu_perfil(trim($id));
            }     
            if(isset($datos)&& !empty($datos)){
                echo $datos;
            } else {
                echo 'No data';
            }
       }        
       
        private function _deletePerfil(){
            $datos =$this->input->post();
            $mensaje = $this->m_101->delete($datos);             
            echo($mensaje);
        }
}