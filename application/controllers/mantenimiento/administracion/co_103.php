<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_103 extends CI_Controller {

    var $url_control;

    function __construct() {
        parent::__construct();
        $this->load->model(array('mantenimiento/administracion/m_103'));
        $this->url_control = $this->router->directory . $this->router->class;
        session_start();
    }

    public function _remap() {

        $tipo = $this->uri->segment(4);
        $datos = $this->input->post();

        validaSession($this->url_control);
        //fnc_post($tipo, $datos, $this->url_control);

        switch ($tipo) {
            case 'guardar':
                $this->_guardar();
                break;
            case 'consulta':
                $this->_consulta();
                break;
            case 'consulta_menuPerfil':
                $this->_consulta_menuPerfil();
                break;
            case 'deleteNoticia':
                $this->_deleteNoticia();
                break;
            case 'publicarNoticia':
                $this->_publicarNoticia();
                break;
            case 'getNoticias':
                $this->_getNoticias();
                break;
            default:
                $this->_crud();
                break;
        }
    }

    private function _crud() {
        $data = null;
        $data['permisos'] = '';
        $urlControlador = $this->url_control;
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'js/plugins/ckeditor/samples/sample.css'
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/config.js',
            'js/general.js',
            'js/mantenimiento/administracion/j_103.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js',
            'js/bootstrap.min.js',
            'js/plugins/ckeditor/ckeditor.js'
        );

        $data['idFormulario'] = 'co_103';

        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('mantenimiento/administracion/v_103');
        $this->load->view('barras/footer');
    }

    private function _getNoticias() {
        $data = $this->m_103->getNoticias();
        $html = '<table id="table_Noticias" class="table table-bordered TFtable">';
        $html .= '<thead>';
        $html .= '<th>Titulo</th>';
        $html .= '<th>Fecha</th>';
        $html .= '<th>Estado</th>';
        $html .= '<th>Publicar</th>';
        $html .= '<th>Modificar</th>';
        $html .= '<th>Eliminar</th>';
        $html .= '</thead>';
        $html .= '<tbody>';

        if (empty($data['err']) && !empty($data['dat'])) {
            foreach ($data['dat'] as $row) {
                $html .= '<tr>';
                $html .= '<td>' . $row['ds_not'] . '</td>';
                $html .= '<td>' . $row['fe_not'] . '</td>';
                $html .= '<td>' . $row['ds_est'] . '</td>';
                $html .= '<td><center><input type="checkbox" id="ck_' . $row['co_not'] . '" name="ck_' . $row['co_not'] . '" onclick="publicarNoticia(' . $row['co_not'] . ');" ' . ($row['fl_est'] == 'P' ? 'checked' : '') . ' ></center></td>';
                $html .= '<td><center><a href="javascript:void(0)" onclick="consulNoticia(' . "'U'" . ',' . $row['co_not'] . ')">Modificar</a></center></td>';
                $html .= '<td><center><a href="javascript:void(0)" onclick="deleteNoticia(' . $row['co_not'] . ')">Eliminar</a></center></td>';
            }
        }

        $html .= '</tbody>';
        $html .= '</table>';
        $data['dat'] = $html;
        echo json_encode($data);
    }

    private function _guardar() {
        $accion = $this->input->post('acc');
        $co_not = $this->input->post('id');
        $ti_noti = $this->input->post('tit');
        $ds_noti = $this->input->post('des');
        $data = $this->m_103->grabar($accion, $co_not, $ti_noti, $ds_noti, $_SESSION['c_e'], 'I');
        echo json_encode($data);
    }

    private function _consulta() {
        $id = $this->input->post('id');
        $data = $this->m_103->retNoticia($id);
        echo json_encode($data);
    }

    private function _deleteNoticia() {
        $id = $this->input->post('id');
        $data = $this->m_103->delete($id);
        echo json_encode($data);
    }

    private function _publicarNoticia() {
        $id = $this->input->post('id');
        $acc = $this->input->post('acc');
        $data = $this->m_103->publicar($id, $acc);
        echo json_encode($data);
    }

}
