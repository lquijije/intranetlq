<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_4001 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('call_center/m_4001'));
        session_start();
    }

    public function _remap() {

        validaSession('call_center/co_4001');
        $tipo = $this->uri->segment(3);

        $datos = $this->input->post();
        fnc_post($tipo, $datos, 'call_center/co_4001');

        if ($tipo == 'reAge') {
            $this->_getAgente();
        } else if ($tipo == 'reCli') {
            $this->_getClientes();
        } else if ($tipo == 'reTel') {
            $this->_getTelefonos();
        } else if ($tipo == 'svTel') {
            $this->_grabaTelefono();
        } else if ($tipo == 'upTel') {
            $this->_actualizarTelefono();
        } else if ($tipo == 'upCli') {
            $this->_actualizarCliente();
        } else {
            $this->_crud();
        }
    }

    private function _crud() {

        $_SESSION['usuario_syscards'] = '';

        $data = null;
        $urlControlador = 'call_center/co_4001';
        $data['urlControlador'] = $urlControlador;

        $dataJS['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'js/plugins/switchery/dist/switchery.css',
            'css/datetimepicker/jquery.datetimepicker.css'
        );

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'js/plugins/switchery/dist/switchery.css',
            'css/datetimepicker/jquery.datetimepicker.css'
        );

        $dataJS['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/general.js',
            'js/config.js',
            'js/call_center/j_4001.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js',
            'js/plugins/switchery/dist/switchery.js',
            'js/plugins/flot/jquery.flot.min.js',
            'js/plugins/flot/jquery.flot.resize.min.js',
            'js/plugins/flot/jquery.flot.pie.min.js',
            'js/plugins/flot/Smooth-0.1.7.js',
            'js/plugins/datetimepicker/jquery.datetimepicker.js'
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/general.js',
            'js/config.js'
        );

        $valida = $this->m_4001->validaUsusarioSyscards($_SESSION['c_e']);

        if (!empty($valida)) {

            $_SESSION['usuario_syscards'] = $valida;
            $tiTelf = $this->m_4001->retTipoTelf();

            $datosParametro = null;
            if (!empty($tiTelf)) {
                foreach ($tiTelf as $row) {
                    $datosParametro[$row['co_tip']] = $row['ds_tip'];
                }
            } else {
                $datosParametro['NNN'] = 'No hay datos';
            }

            $atributosEspecial = "id='cmb_tip' class='form-control form-select'";
            $dataJS['cmb_tip'] = form_dropdown('cmb_tip', $datosParametro, 'NNN', $atributosEspecial);

            $this->load->view('barras/header', $dataJS);
            $this->load->view('barras/menu');
            $this->load->view('call_center/v_4001');
        } else {

            $this->load->view('barras/header', $data);
            $this->load->view('barras/menu');
            $this->load->view('call_center/v_4001_PERMISOS');
        }

        $this->load->view('barras/footer');
    }

    function _getAgente() {
        $usuario = (int) $_SESSION['c_e'];
        $data = $this->m_4001->retDashboardAgente($usuario);
        echo json_encode($data);
    }

    function _getClientes() {
        $cod_est = $this->input->post('c_e');
        $clientes = $this->m_4001->retClientesEstado($cod_est);
        if (!empty($clientes)) {
            echo json_encode($clientes);
        } else {
            echo json_encode(array(
                "sEcho" => 1,
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            ));
        }
    }

    function _getTelefonos() {
        $retData = null;
        $data = $this->input->post('dt');

        if (!empty($data['co_cli'])) {
            $retData = $this->m_4001->retTelefonoClientes($data['co_cli']);

            if (trim($data['co_est']) == 'P') {
                $this->m_4001->actualizaGestion($data['co_cam'], $data['co_cli'], $data['fe_car'], 'E', '', $_SESSION['c_e'], '1', '');
            }
        }
        echo json_encode($retData);
    }

    function _actualizarTelefono() {

        $retData = null;
        $data = $this->input->post('dt');
//        print_r($data);
//        die;
        if (!empty($data['co_cli'])) {

            switch ($data['co_est']) {
                case 'D':
                    $retData = $this->m_4001->eliminaTelefeno($data['co_reg'], $data['co_cli'], $data['ti_tel'], $data['co_sec'], $_SESSION['usuario_syscards'],$data['se_ref']);
                    break;
                default:
                    if($data['co_reg'] == 'CL'){
                        $retData = $this->m_4001->actualiza_insertaTelefeno($data['co_reg'], $data['co_cli'], $data['ti_tel'], $data['nu_tel'], $data['ex_tel'], $data['ar_tel'], $_SESSION['usuario_syscards'], $data['co_sec']);
                    
                    } else if($data['co_reg'] == 'RF'){
                        $retData = $this->m_4001->actualizaReferencia($data['co_reg'], $data['co_cli'], $data['ti_tel'], $data['nu_tel'], $data['ex_tel'], $data['ar_tel'], $_SESSION['usuario_syscards'], $data['co_sec'], $data['no_ref'], $data['ap_ref'], $data['co_par'], $data['co_ciu'], $data['co_pro'],$data['se_ref']);
                    }
                    break;
            }
        }
        echo json_encode($retData);
    }

    function _actualizarCliente() {
        $data = $this->input->post('dt');
        $retData = $this->m_4001->actualizaGestion($data['co_cam'], $data['co_cli'], $data['fe_car'], $data['co_est'], $data['tx_hor'], $_SESSION['c_e'], $data['co_ci'], $data['tx_com']);
        echo json_encode($retData);
    }

    function _grabaTelefono() {
        $data = $this->input->post();
        $data = $data['dt'];
        $array = $this->m_4001->actualiza_insertaTelefeno($data['co_reg'], $data['co_cli'], $data['cb_tip'], $data['nu_tel'], $data['ex_tel'], $data['ar_tel'], $_SESSION['usuario_syscards']);
        echo json_encode($array);
    }

}
