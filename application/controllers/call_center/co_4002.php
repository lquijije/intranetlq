<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class co_4002 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('call_center/m_4001', 'call_center/m_4002'));
        session_start();
    }

    public function _remap() {

        validaSession('call_center/co_4002');
        $tipo = $this->uri->segment(3);

        $datos = $this->input->post();
        fnc_post($tipo, $datos, 'call_center/co_4002');

        if ($tipo == 'reDas') {
            $this->_getDashboard();
        } else if ($tipo == 'svAsig') {
            $this->_saveAsignacion();
        } else if ($tipo == 'svCambioAsig') {
            $this->_saveCambioAsignacion();
        } else if ($tipo == 'exExc') {
            $this->_exportExcel();
        } else {
            $this->_crud();
        }
    }

    private function _crud() {

        $data = null;
        $urlControlador = 'call_center/co_4002';
        $data['urlControlador'] = $urlControlador;

        $data['css'] = array(
            'css/datatables/dataTables.bootstrap.css',
            'js/plugins/switchery/dist/switchery.css',
            'css/daterangepicker/daterangepicker-bs3.css',
        );

        $data['js'] = array(
            'js/jquery-1.10.2.min.js',
            'js/bootstrap.min.js',
            'js/general.js',
            'js/config.js',
            'js/call_center/j_4002.js',
            'js/plugins/datatables/jquery.dataTables.js',
            'js/plugins/datatables/dataTables.bootstrap.js',
            'js/plugins/switchery/dist/switchery.js',
            'js/plugins/flot/jquery.flot.min.js',
            'js/plugins/flot/jquery.flot.resize.min.js',
            'js/plugins/flot/jquery.flot.pie.min.js',
            'js/plugins/flot/Smooth-0.1.7.js',
            'js/plugins/daterangepicker/daterangepicker.js',
        );

        $data['usuarios'] = $this->m_4002->retUsuaariosAsignar();
        $data['fe_cargas'] = $this->m_4002->retListaFechaDeCarga();

        $this->load->view('barras/header', $data);
        $this->load->view('barras/menu');
        $this->load->view('call_center/v_4002');
        $this->load->view('barras/footer');
    }

    function _getDashboard() {
        $fe_ini = $this->input->post('f_i');
        $fe_fin = $this->input->post('f_f');
        $data = $this->m_4002->retDashboardAdministracion($fe_ini, $fe_fin);
        echo json_encode($data);
    }

    function _saveAsignacion() {
        $usuarios = $this->input->post('d_t');
        $cantidad = $this->input->post('c_a');
        $tipo = $this->input->post('t_p');
        $usuarios = json_decode($usuarios, true);
        $data = $this->m_4002->asignarUsuarios($usuarios, $cantidad,$tipo);
        echo json_encode($data);
    }
    
    function _saveCambioAsignacion() {
        $usuario = $this->input->post('c_u');
        $usuario_a_cambio = $this->input->post('u_c');
        $estado = $this->input->post('e_s');
        $fe_ini = $this->input->post('f_i');
        $fe_fin = $this->input->post('f_f');
        $data = $this->m_4002->cambioUsuarioAsignacion($fe_ini, $fe_fin,$usuario, $usuario_a_cambio,$estado);
        echo json_encode($data);
    }

    function _exportExcel() {
        $fe_ini = $this->input->post('f_i');
        $fe_fin = $this->input->post('f_f');
        $datos = $this->m_4002->retReporteAsignado($fe_ini, $fe_fin);
        if (!empty($datos)) {
            $this->load->library('PHPExcel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->
                    getProperties()
                    ->setCreator("CALL CENTER INTRANET")
                    ->setTitle("GESTION DE ACTUALIZACION DE DATOS")
                    ->setSubject("GESTION DE ACTUALIZACION DE DATOS")
                    ->setDescription("GESTION DE ACTUALIZACION DE DATOS")
                    ->setKeywords("GESTION DE ACTUALIZACION DE DATOS")
                    ->setCategory("GESTION DE ACTUALIZACION DE DATOS");

            $borders1 = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000'),
                    )
                ),
            );

            $borders = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000'),
                    )
                ),
            );

            $Letra = "M";
            $linea = 1;

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('CALL CENTER INTRANET', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':' . $Letra . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('GESTION DE ACTUALIZACION DE DATOS', PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $linea . ':D' . $linea);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit('FECHA DEL REPORTE: ' . date('Y/m/d h:i:s'), PHPExcel_Cell_DataType::TYPE_STRING);

            $linea++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders1);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $linea, 'FECHA CARGA')
                    ->setCellValue('B' . $linea, 'TIPO DE GESTION')
                    ->setCellValue('C' . $linea, 'CODIGO USUARIO')
                    ->setCellValue('D' . $linea, 'NOMBRE USUARIO')
                    ->setCellValue('E' . $linea, 'CODIGO CLIENTE')
                    ->setCellValue('F' . $linea, 'CEDULA CLIENTE')
                    ->setCellValue('G' . $linea, 'NOMBRE CLIENTE')
                    ->setCellValue('H' . $linea, 'DEUDA CLIENTE')
                    ->setCellValue('I' . $linea, 'FECHA ASIGNADA')
                    ->setCellValue('J' . $linea, 'HORA INICIO')
                    ->setCellValue('K' . $linea, 'HORA FINAL')
                    ->setCellValue('L' . $linea, 'ESTADO')
                    ->setCellValue('M' . $linea, 'OBSERVACION');
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->getFont()->setSize(12);

            foreach ($datos as $row) {
                $linea++;
                //$objPHPExcel->getActiveSheet()->getStyle('A' . $linea . ':' . $Letra . $linea)->applyFromArray($borders);
                $objPHPExcel->getActiveSheet()->getCell('A' . $linea)->setValueExplicit($row['fe_car'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('B' . $linea)->setValueExplicit($row['ti_ges'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('C' . $linea)->setValueExplicit($row['de_usu'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('D' . $linea)->setValueExplicit($row['no_usu'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('E' . $linea)->setValueExplicit($row['co_cli'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('F' . $linea)->setValueExplicit($row['ce_cli'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('G' . $linea)->setValueExplicit($row['no_cli'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('H' . $linea)->setValueExplicit($row['va_deu'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('I' . $linea)->setValueExplicit($row['fe_asg'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('J' . $linea)->setValueExplicit($row['ho_ini'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('K' . $linea)->setValueExplicit($row['ho_fin'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('L' . $linea)->setValueExplicit($row['de_est'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('M' . $linea)->setValueExplicit($row['de_obs'], PHPExcel_Cell_DataType::TYPE_STRING);
            }

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

            $hoy = date("Y_m_d_h_i");
            $objPHPExcel->getActiveSheet()->setTitle('REPORTE');
            $filename = 'REPORTE_DE_GESTIONES_' . $hoy . '.xls';
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=1');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('temp/' . $filename);
            header("location: " . site_url() . "temp/$filename");
            unlink(site_url("temp/$filename"));
        } else {
            echo " <script> alert('No hay datos disponibles en la busqueda realizada');</script> ";
            echo " <script> window.close();</script> ";
        }
    }

}
