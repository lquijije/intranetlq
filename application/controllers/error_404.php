<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class error_404 extends CI_Controller {	
	
	function __construct()
	{		
		parent::__construct();	
                session_start();          
        }	
	
        public function _remap($method)
        {      
             $this->_error404();
        }
        
        private function _error404(){
            $data['css'] =  array('css/animation.css');

            $data['js'] = array('js/jquery.min.js',
                                'js/bootstrap.min.js',
                                'js/general.js',
                                'js/jquery-ui-1.10.3.js');

            
            $data['idFormulario'] = "f_principal";
            $this->load->view('barras/header',$data);
            $this->load->view('barras/menu');
            $this->load->view('error/error_404');
            $this->load->view('barras/footer');
        }
}
