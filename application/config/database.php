<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = "default";
$active_record = TRUE;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////   CONEXION A SQL SERVER    //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$db['default']['hostname'] = 'MASTER-D';
$db['default']['username'] = 'pica';
$db['default']['password'] = '';
$db['default']['database'] = 'WEBPYCCA';
$db['default']['dbdriver'] = 'odbc';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$db['desarrollo']['hostname'] = 'MASTER-D';
$db['desarrollo']['username'] = 'pica';
$db['desarrollo']['password'] = '';
$db['desarrollo']['database'] = 'WEBPYCCA';
$db['desarrollo']['dbdriver'] = 'odbc';
$db['desarrollo']['dbprefix'] = '';
$db['desarrollo']['pconnect'] = FALSE;
$db['desarrollo']['db_debug'] = TRUE;
$db['desarrollo']['cache_on'] = FALSE;
$db['desarrollo']['cachedir'] = '';
$db['desarrollo']['char_set'] = 'utf8';
$db['desarrollo']['dbcollat'] = 'utf8_general_ci';
$db['desarrollo']['swap_pre'] = '';
$db['desarrollo']['autoinit'] = TRUE;
$db['desarrollo']['stricton'] = FALSE;

$db['desarrollo_report']['hostname'] = 'DESARROLLO-REPORT';
$db['desarrollo_report']['username'] = 'LINVENT';
$db['desarrollo_report']['password'] = 'LINVENT';
$db['desarrollo_report']['database'] = 'WEBPYCCA';
$db['desarrollo_report']['dbdriver'] = 'odbc';
$db['desarrollo_report']['dbprefix'] = '';
$db['desarrollo_report']['pconnect'] = FALSE;
$db['desarrollo_report']['db_debug'] = TRUE;
$db['desarrollo_report']['cache_on'] = FALSE;
$db['desarrollo_report']['cachedir'] = '';
$db['desarrollo_report']['char_set'] = 'utf8';
$db['desarrollo_report']['dbcollat'] = 'utf8_general_ci';
$db['desarrollo_report']['swap_pre'] = '';
$db['desarrollo_report']['autoinit'] = TRUE;
$db['desarrollo_report']['stricton'] = FALSE;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////   CONEXION A SQL NOMINA    //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$db['nomina']['hostname'] = 'NOMINA-P';
$db['nomina']['username'] = 'pica';
$db['nomina']['password'] = '';
$db['nomina']['database'] = 'PERSONAL_PI';
$db['nomina']['dbdriver'] = 'odbc';
$db['nomina']['dbprefix'] = '';
$db['nomina']['pconnect'] = FALSE;
$db['nomina']['db_debug'] = TRUE;
$db['nomina']['cache_on'] = FALSE;
$db['nomina']['cachedir'] = '';
$db['nomina']['char_set'] = 'utf8';
$db['nomina']['dbcollat'] = 'utf8_general_ci';
$db['nomina']['swap_pre'] = '';
$db['nomina']['autoinit'] = TRUE;
$db['nomina']['stricton'] = FALSE;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// CONEXION A SQL SQLNSIP //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$db['sqlnsip']['hostname'] = 'SQLNSIP-P';
$db['sqlnsip']['username'] = 'pica';
$db['sqlnsip']['password'] = '';
$db['sqlnsip']['database'] = 'PRDBPROCESO';
$db['sqlnsip']['dbdriver'] = 'odbc';
$db['sqlnsip']['dbprefix'] = '';
$db['sqlnsip']['pconnect'] = FALSE;
$db['sqlnsip']['db_debug'] = TRUE;
$db['sqlnsip']['cache_on'] = FALSE;
$db['sqlnsip']['cachedir'] = '';
$db['sqlnsip']['char_set'] = 'utf8';
$db['sqlnsip']['dbcollat'] = 'utf8_general_ci';
$db['sqlnsip']['swap_pre'] = '';
$db['sqlnsip']['autoinit'] = TRUE;
$db['sqlnsip']['stricton'] = FALSE;

$db['sqlnsip-clon']['hostname'] = 'SQLNSIP-P';
$db['sqlnsip-clon']['username'] = 'pica';
$db['sqlnsip-clon']['password'] = '';
$db['sqlnsip-clon']['database'] = 'PRDBPROCESO';
$db['sqlnsip-clon']['dbdriver'] = 'odbc';
$db['sqlnsip-clon']['dbprefix'] = '';
$db['sqlnsip-clon']['pconnect'] = FALSE;
$db['sqlnsip-clon']['db_debug'] = TRUE;
$db['sqlnsip-clon']['cache_on'] = FALSE;
$db['sqlnsip-clon']['cachedir'] = '';
$db['sqlnsip-clon']['char_set'] = 'utf8';
$db['sqlnsip-clon']['dbcollat'] = 'utf8_general_ci';
$db['sqlnsip-clon']['swap_pre'] = '';
$db['sqlnsip-clon']['autoinit'] = TRUE;
$db['sqlnsip-clon']['stricton'] = FALSE;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// CONEXION A MYSQL //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$db['db_preciorecord']['hostname'] = '130.1.1.15';
$db['db_preciorecord']['username'] = 'root';
$db['db_preciorecord']['password'] = 'Pycca5689';
$db['db_preciorecord']['database'] = 'dbpreciorecord';
$db['db_preciorecord']['dbdriver'] = 'mysql';
$db['db_preciorecord']['dbprefix'] = '';
$db['db_preciorecord']['pconnect'] = FALSE;
$db['db_preciorecord']['db_debug'] = TRUE;
$db['db_preciorecord']['cache_on'] = FALSE;
$db['db_preciorecord']['cachedir'] = '';
$db['db_preciorecord']['char_set'] = 'utf8';
$db['db_preciorecord']['dbcollat'] = 'utf8_general_ci';
$db['db_preciorecord']['swap_pre'] = '';
$db['db_preciorecord']['autoinit'] = TRUE;
$db['db_preciorecord']['stricton'] = FALSE;

$db['db_lx_master']['hostname'] = '130.1.40.136'; 
$db['db_lx_master']['username'] = 'root';
$db['db_lx_master']['password'] = 'Pycca5689';
$db['db_lx_master']['database'] = 'WEBPYCCA';
$db['db_lx_master']['dbdriver'] = 'mysqli'; 
$db['db_lx_master']['dbprefix'] = '';
$db['db_lx_master']['pconnect'] = FALSE; 
$db['db_lx_master']['db_debug'] = TRUE;
$db['db_lx_master']['cache_on'] = FALSE;
$db['db_lx_master']['cachedir'] = '';
$db['db_lx_master']['char_set'] = 'utf8';
$db['db_lx_master']['dbcollat'] = 'utf8_general_ci';
$db['db_lx_master']['swap_pre'] = '';
$db['db_lx_master']['autoinit'] = TRUE;
$db['db_lx_master']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./system/application/config/database.php */
?>
