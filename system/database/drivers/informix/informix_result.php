<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2009, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// --------------------------------------------------------------------

/**
 * MySQL Result Class
 *
 * This class extends the parent result class: CI_DB_result
 *
 * @category	Database
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/database/
 */
class CI_DB_informix_result extends CI_DB_result {

	/**
	 * Number of rows in the result set
	 *
	 * implantada dlg  19feb2011
	 * @access	public
	 * @return	integer
	 */
	function num_rows()
	{  $treg = ifx_affected_rows($this->result_id);

    //echo "<br> INFOMRIX_RESULT->num_rows()  treg = ".$treg;
   //echo "teg = ".$treg;

   return  $treg;
		//return @ifx_affected_rows($this->result_id);
	}
	
	// --------------------------------------------------------------------

	/**
	 * Number of fields in the result set
	 *
	 * @access	public
	 * @return	integer
	 */
	function num_fields()
	{
		//return @mysql_num_fields($this->result_id);
  return @ifx_num_fields($this->result_id);
	}
	
	// --------------------------------------------------------------------

	/**
	 * Fetch Field Names
	 * implantada dlg  19feb2011
	 * Generates an array of column names
	 *
	 * @access	public
	 * @return	array
	 */
	function list_fields()
	{
		$field_names = array();

  $types = ifx_fieldtypes ($this->result_id);
  for ($i = 0; $i < count($types); $i++) 
   { $fname = key($types);
     $field_names[] = $fname;
     next($types);
   }
		
		return $field_names;
	}

	// --------------------------------------------------------------------

	/**
	 * Field data
	 *
	 * implantada dlg  19feb2011
	 * Generates an array of objects containing field meta-data
	 *
	 * @access	public
	 * @return	array
	 */
	function field_data()
	{
		$retval = array();
  $properties = ifx_fieldproperties($this->result_id);
  foreach ($properties as $fname => $val) 
   { $arr = explode(";",$val);
  			$F				= new stdClass();
    	$F->name 		= $fname;
  			$F->type 		= $arr[0];
  			$F->default		= "";
  			$F->max_length	= $arr[1];
  			$F->primary_key = "";
			
	   	$retval[] = $F;
      
   }

/*
		while ($field = mysql_fetch_field($this->result_id))
		{	
			$F				= new stdClass();
			$F->name 		= $field->name;
			$F->type 		= $field->type;
			$F->default		= $field->def;
			$F->max_length	= $field->max_length;
			$F->primary_key = $field->primary_key;
			
			$retval[] = $F;
		}
		*/

		return $retval;
	}
	
	// --------------------------------------------------------------------

	/**
	 * Free the result
	 *
	 * @return	null
	 */		
	function free_result()
	{
		//echo "<br> estoy aqui  resulid = ".$this->result_id;
        if (is_resource($this->result_id))
		{
		   // echo "voy a free";
			ifx_free_result($this->result_id);
			$this->result_id = FALSE;
            
           /* $c =  fopen("a.log","a+");
            fwrite($c , $this->result_id);
            fclose($c);   */       
		}
       // exit;
	}

	// --------------------------------------------------------------------

	/**
	 * Data Seek
	 *
	 * Moves the internal pointer to the desired offset.  We call
	 * this internally before fetching results to make sure the
	 * result set starts at zero
	 *
	 * @access	private
	 * @return	array
	 */
	function _data_seek($n = 0)
	{ // DLg  si falla  habria que implantar  ifx_fetch_row($this->result_id,"FIRST")

		//return mysql_data_seek($this->result_id, $n);
   // NO IMPLEMENT
	}

	// --------------------------------------------------------------------

	/**
	 * Result - associative array
	 *
	 * Returns the result set as an array
	 *
	 * @access	private
	 * @return	array
	 */
	function _fetch_assoc()
	{ //echo "<br>  esoty en INFORMIX_RESULT->fetch _assoc()  RESULT_ID = " . $this->result_id;
		//return mysql_fetch_assoc($this->result_id);
   //$arr = ifx_fetch_row($this->result_id,'FIRST');
   //$arr = ifx_fetch_row($this->result_id,'NEXT');
   //$arr = ifx_fetch_row($this->result_id);
   
    $arr = ifx_fetch_row($this->result_id);

  return $arr;
  //return ifx_fetch_row($this->result_id);
	}
	
	// --------------------------------------------------------------------

	/**
	 * Result - object
	 *
	 * Returns the result set as an object
	 *
	 * @access	private
	 * @return	object
	 */
	function _fetch_object()
	{
		//return mysql_fetch_object($this->result_id);
  // NO IMPLEMENT
	}
	
}


/* End of file mysql_result.php */
/* Location: ./system/database/drivers/mysql/mysql_result.php */
