<?php
$directorio_fotos_articulos="/media/PRODUCTOS/";
$directorio_app="/media/Data/desarrollo/picking/img/";

function CargarJpeg($imagen)
{
    $im = @imagecreatefromjpeg($imagen);

    if(!$im){
        
        $im  = imagecreatetruecolor(150, 30);
        $fondo = imagecolorallocate($im, 255, 255, 255);
        $ct  = imagecolorallocate($im, 0, 0, 0);

        imagefilledrectangle($im, 0, 0, 150, 30, $fondo);
        imagestring($im, 1, 5, 5, 'Error cargando ' . $imagen, $ct);
    }

    return $im;
}

$existe=false;
$codigo = $_GET["codigo"];

if ($codigo!="") {
    $filename=$directorio_fotos_articulos.substr($codigo,0,5).".jpg";
    if (file_exists($filename)) {
        $existe=true;
    }
}

if (!$existe) {
    $filename=$directorio_app."imagen_no_disponible.jpg";
}

header('Content-Type: image/jpeg');
$img = CargarJpeg($filename);
imagejpeg($img);
imagedestroy($img);

?>
