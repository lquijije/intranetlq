<?php

date_default_timezone_set('America/Guayaquil');
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '-1');

$datos = retReport($_GET);

$hoy = date("Y_m_d_h_i");  
$filename='REPORTE_ROBOT_'.$hoy.'.csv'; //save our workbook as this file name
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=$filename");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

echo $tab = 'FECHA,TRAMO,COD. CLIENTE,CEDULA,NOMBRES COMPLETOS,DIAS VENCIDOS,PAGO MINIMO,ESTADO,PROVEEDOR,FECHA ENVIO,CONTACTO,MENSAJE,RESPUESTA,DURACION,ESTADO'.chr(13);

foreach ($datos as $row){
    $tab = '"'.$row['fe_gen'].'",';
    $tab .= '"'.$row['ds_cam'].'",';
    $tab .= '"'.$row['co_cli'].'",';
    $tab .= '"'.$row['cc_cli'].'",';
    $tab .= '"'.$row['ap_cli']." ".$row['no_cli'].'",';
    $tab .= $row['va_dia'].',';
    $tab .= $row['va_min'].',';
    $tab .= $row['no_est_cab'].',';
    $tab .= $row['no_pro'].',';
    $tab .= '"'.$row['fe_env'].'",';
    $tab .= '"'.$row['contac'].'",';
    $tab .= '"'.utf8_encode($row['tx_men']).'",';
    $tab .= '"'.$row['tx_res'].'",';
    $tab .= $row['va_dur'].',';
    $tab .= $row['no_est_det'];
    echo $tab.chr(13);
}

function retReport($data){
        
    $estados = array (
        'C' => array(
            'name'=>'Ya pago',
            'color'=>'#f0a30a'
        ),
        'P' => array(
            'name'=>'Pendiente',
            'color'=>'#F56954'
        ),
        'E' => array(
            'name'=>'Contactado',
            'color'=>'#00A65A'
        ),
        'X' => array(
            'name'=>'Error',
            'color'=>'#2E8BEF'
        ),
        'L' => array(
            'name'=>'IVR Encolado',
            'color'=>'#633EBE'
        )
    );
    $array = null;

    if(!empty($data['c_c']) && !empty($data['f_i']) && !empty($data['f_f'])){

        $Sql = " NTS_INTERCAMBIO.._sp_robot_reporte ".(int)$data['c_c'].",'".trim($data['f_i'])."','".trim($data['f_f'])."' ";

        $conn = odbc_connect("MASTER-P", "LINVENT", "LINVENT");
        $resultset = odbc_exec($conn,$Sql);
            
        if (odbc_error()){
            echo "ERROR: AL RETORNAR DETALLE LLAMADAS ".odbc_errormsg($conn);
        }

        while($row = odbc_fetch_array($resultset)){

            $array[] = array(
                'fe_gen'=> trim($row['fe_generacion']),
                'co_cam'=> (int)$row['co_campania'],
                'ds_cam'=> trim($row['ds_campania']),
                'co_cli'=> (int)$row['co_cliente'],
                'cc_cli'=> trim($row['co_cedula']),
                'ap_cli'=> trim(utf8_encode($row['cl_apellidos'])),
                'no_cli'=> trim(utf8_encode($row['cl_nombres'])),
                'va_dia'=> (int)$row['va_dias_vencidos'],
                'va_min'=> (float)$row['va_pago_minimo'],
                'co_est_cab'=> trim($row['co_estado_cab']),
                'no_est_cab'=> $estados[$row['co_estado_cab']]['name'],
                'co_sec'=> (int)$row['co_secuencia'],
                'co_can'=> (int)$row['co_canal'],
                'co_pro'=> (int)$row['co_proveedor'],
                'no_pro'=> trim($row['tx_descripcion']),
                'fe_env'=> trim($row['fe_envio']),
                'contac'=> trim($row['contacto']),
                'tx_men'=> trim(utf8_encode($row['tx_mensaje'])),
                'tx_res'=> trim(utf8_encode($row['tx_respuesta_envio'])),
                'va_dur'=> trim($row['va_duracion']),
                'co_est_det'=> trim($row['co_estado_det']),
                'no_est_det'=> $estados[$row['co_estado_det']]['name']
            );

        }

    }

    return $array;

}

?>
