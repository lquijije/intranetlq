<?php
$directorio_fotos_articulos="/media/credenciales/";
$directorio_app="/media/Data/desarrollo/ipycca/img/";

function CargarJpeg($imagen)
{
    $img = imagecreatefromjpeg($imagen);
    $width = imagesx( $img );
    $height = imagesy( $img );

    $new_width = 200;
    $new_height = floor( $height * ( $new_width / $width ) );

    $tmp_img = imagecreatetruecolor( $new_width, $new_height );

    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
    imagejpeg($tmp_img);
    imagedestroy($tmp_img);
    
}

$existe=false;
$codigo = $_GET["ce"];

if ($codigo!="") {
    $filename=$directorio_fotos_articulos.substr($codigo,0,5).".jpg";
    if (file_exists($filename)) {
        $existe=true;
    }
}

if (!$existe) {
    $filename = $directorio_app."user.jpg";
}

header('Content-Type: image/jpeg');
CargarJpeg($filename);

?>
