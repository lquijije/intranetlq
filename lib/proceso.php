<?php

ini_set('max_execution_time', 0);
error_reporting(E_ALL);

//////////////////////////////////////////////////////////////////
/// TIPO DE FUNCIONES                                           //
/// --------------------------------------------------------------
/// 1 = NOTICIAS                                                //
/// 2 = JSON USUARIO Y JSON SOLICITUD DE CHEQUE                 //
//////////////////////////////////////////////////////////////////

if (isset($_GET['fn'])) {
    if ($_GET['fn'] === "1") {
        getRSS();
    } else if ($_GET['fn'] === "2") {
        jsonUser();
        jsonSqlnsip($_GET['c_r']);
    }
}

function getRSS() {

    $co_error = 0;

    $url = "http://www.eluniverso.com/rss/noticias.xml";
    $xml = file_get_contents($url);

    if (!empty($xml)) {

        $fp = fopen('../temp/xml/noticias.xml', 'w');
        fwrite($fp, $xml);
        fclose($fp);
    } else {

        $co_error = 1;
    }

    echo $co_error;
}

function jsonUser() {

    $CNN = odbc_connect('MASTER-P', 'pica', '');

    $Sql = " intranet_master_json '47' ";

    $RESULT = odbc_exec($CNN, $Sql);
    $array = null;
    while ($raw = odbc_fetch_array($RESULT)) {
        $array[] = $raw;
    }

    $empl = '';
    $arr = null;

    foreach ($array AS $rew) {
        if ($empl <> trim($rew['cod_empl'])) {
            foreach ($array AS $row) {
                if (trim($rew['cod_empl']) === trim($row['cod_empl'])) {
                    if ((int) trim($row['menu']) === 47) {
                        $Sql = 'webpycca.._get_reportes_usuario ' . trim($rew['cod_empl']);
                        $report = getArray($Sql, $CNN);
                        $arr[trim($row['menu'])] = $report;
                    }
                }
            }
        }

        $empl = trim($rew['cod_empl']);

        $fp = fopen('../temp/json/' . trim($rew['cod_empl']) . '.json', 'w');
        fwrite($fp, json_encode(array('master' => $arr)));
        fclose($fp);
    }

    odbc_close($CNN);
}

function jsonSqlnsip($cod_resp) {

    $CNN = odbc_connect('MASTER-P', 'pica', '');
    $NSIP = odbc_connect('SQLNSIP-P', 'pica', '');

    if (!empty($cod_resp)) {

        $Sql = '_get_maestras_solcheque ' . trim($cod_resp);
        $sqlnsip = getArray($Sql, $NSIP);

        if (!empty($sqlnsip)) {
            $fp = fopen('../temp/json/' . trim($cod_resp) . '-39.json', 'w');
            fwrite($fp, json_encode(array('master' => array(39 => $sqlnsip))));
            fclose($fp);
        }
    } else {

        $Sql = ' SELECT c.cod_encargado ';
        $Sql .= ' FROM tb_intranet_perfil_accesos AS a ';
        $Sql .= ' INNER JOIN tb_intranet_users_perfil AS b ON a.perfil = b.perfil ';
        $Sql .= ' INNER JOIN tb_intranet_users AS c ON b.cod_empl = c.cod_empleado AND c.estado = 1 ';
        $Sql .= ' WHERE a.menu IN(39) ';
        $Sql .= ' GROUP BY c.cod_encargado ';
        $Sql .= ' ORDER BY 1 ';

        $RESULT = odbc_exec($CNN, $Sql);

        while ($r = odbc_fetch_array($RESULT)) {
            $Sql = '_get_maestras_solcheque ' . trim($r['cod_encargado']);
            $sqlnsip = getArray($Sql, $NSIP);
            if (!empty($sqlnsip)) {
                $fp = fopen('../temp/json/' . trim($r['cod_encargado']) . '-39.json', 'w');
                fwrite($fp, json_encode(array('master' => array(39 => $sqlnsip))));
                fclose($fp);
            }
        }
    }

    odbc_close($CNN);
    odbc_close($NSIP);
}

function getArray($Sql, $Con) {

    $RESULT = odbc_exec($Con, $Sql);

    $a = '';
    $pass = 0;
    $arrCa = null;
    $arrDe = null;
    $arrItems = null;

    while ($row = odbc_fetch_array($RESULT)) {
        $pass++;
        $a = trim($row[odbc_field_name($RESULT, 1)]);
        for ($b = 2; $b <= odbc_num_fields($RESULT); $b++) {
            $arrItems[odbc_field_name($RESULT, $b)] = utf8_encode(trim($row[odbc_field_name($RESULT, $b)]));
        }
        $arrDe[] = $arrItems;
    }

    $arrCa[$a] = $arrDe;

    while (odbc_next_result($RESULT)) {
        $arrDe = null;
        $arrItems = null;
        while ($row = odbc_fetch_array($RESULT)) {
            $pass++;
            $a = trim($row[odbc_field_name($RESULT, 1)]);
            for ($b = 2; $b <= odbc_num_fields($RESULT); $b++) {
                $arrItems[odbc_field_name($RESULT, $b)] = utf8_encode(trim($row[odbc_field_name($RESULT, $b)]));
            }
            $arrDe[] = $arrItems;
        }
        $arrCa[$a] = $arrDe;
    }

    return $arrCa;
}
?>

